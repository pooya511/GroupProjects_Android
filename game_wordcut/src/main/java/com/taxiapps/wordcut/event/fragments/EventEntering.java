package com.taxiapps.wordcut.event.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.event.EventDialogFragment;
import com.taxiapps.wordcut.model.Coin;
import com.taxiapps.wordcut.model.Event;
import com.taxiapps.wordcut.model.GameConfig;
import com.taxiapps.wordcut.model.Level;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;
import com.taxiapps.wordcut.ui.dialog.Dialogs;
import com.taxiapps.wordcut.utils.AppModules;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import de.cketti.mailto.EmailIntentBuilder;
import modules.PublicModules;

public class EventEntering extends Fragment {

    private Button enterBtn, okBtn;
    private TextView enrolDescription, progressText, wantToBeSponsor, sponsorRewardTitle;
    private ImageView sponsorLogo, prizeImage;
    private LinearLayout loadingLayout;
    private ConstraintLayout dataLayout;

    private Event event;
    private EnterTypes enterType;

    public enum EnterTypes {

        NOT_STARTED("NotStarted"),
        NOT_ENROLLED("NotEnrolled"),
        ENROLLED("Enrolled"),
        FINISHED("Finished");

        private final String name;

        EnterTypes(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }

    }

    public static EventEntering newInstance(Event event, EnterTypes enterType) {

        Bundle args = new Bundle();

        EventEntering fragment = new EventEntering();
        fragment.setArguments(args);
        fragment.event = event;
        fragment.enterType = enterType;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.pop_event_enter, container, false);

        // set Layout Font
        Typeface targetTypeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + "yekan.ttf");
        AppModules.setTypeface((ViewGroup) view, targetTypeFace);

        //  find components
        enterBtn = view.findViewById(R.id.pop_event_enter_enter_btn);
        okBtn = view.findViewById(R.id.pop_event_enter_ok_btn);
        enrolDescription = view.findViewById(R.id.pop_event_enter_header_description);
        sponsorLogo = view.findViewById(R.id.pop_event_enter_sponsor_logo);
        prizeImage = view.findViewById(R.id.pop_event_enter_prize_image);
        loadingLayout = view.findViewById(R.id.pop_event_enter_loading_container);
        dataLayout = view.findViewById(R.id.pop_event_enter_data_container);
        progressText = view.findViewById(R.id.pop_event_enter_download_progress_text);
        wantToBeSponsor = view.findViewById(R.id.pop_event_enter_want_be_sponsor);
        sponsorRewardTitle = view.findViewById(R.id.pop_event_enter_t_sponsor_rewards);

        // end event date
        Calendar endEventDate = Calendar.getInstance();
        endEventDate.setTime(new Date(event.getEventEnd()));

        // start event date
        Calendar startEventDate = Calendar.getInstance();
        startEventDate.setTime(new Date(event.getEventStart()));

        // current date
        Calendar todayDate = Calendar.getInstance();

        if (enterType.equals(EnterTypes.NOT_STARTED)) {
            int dayCount = startEventDate.get(Calendar.DAY_OF_YEAR) - todayDate.get(Calendar.DAY_OF_YEAR);
            int hourCount = startEventDate.get(Calendar.HOUR_OF_DAY) - todayDate.get(Calendar.HOUR_OF_DAY);

            enrolDescription.setTextColor(getResources().getColor(R.color.red));

            if (dayCount > 0) { // age rooz munde bashe ta beresim be shoru e event
                if (hourCount > 0) { // age bish az 1 saat ham munde bashe
                    enrolDescription.setText(PublicModules.toPersianDigit(String.valueOf(dayCount)) + " روز و " + PublicModules.toPersianDigit(String.valueOf(hourCount)) + " ساعت تا شروع");
                } else { // age kamtar az 1 saat munde bashe
                    enrolDescription.setText(PublicModules.toPersianDigit(String.valueOf(dayCount)) + " روز تا شروع");
                }
            } else { // age emruz gharare shoru she
                if (hourCount > 0) { // age chand saat munde be shoru
                    enrolDescription.setText(PublicModules.toPersianDigit(String.valueOf(hourCount)) + " ساعت تا شروع");
                } else { // age 1 saat ya kamtar az 1 saat munde bashe be shoru
                    enrolDescription.setText("کم تر از ۱ ساعت تا شروع");
                }
            }

            enterBtn.setBackgroundResource(R.drawable.slc_event_exit_btn);
        }
        if (enterType.equals(EnterTypes.NOT_ENROLLED)) {
            enrolDescription.setText(event.getEnrolDescription());
            enrolDescription.setTextColor(getResources().getColor(R.color.black));
            enterBtn.setText("ورود " + PublicModules.toPersianDigit(String.valueOf(event.getEnrolCost())));
            enterBtn.setBackgroundResource(R.drawable.slc_event_enter_btn);
        }
        if (enterType.equals(EnterTypes.ENROLLED)) {
            enrolDescription.setTextColor(getResources().getColor(R.color.black));
            enrolDescription.setText(event.getEnrolDescription());
            enterBtn.setBackgroundResource(R.drawable.slc_btn_green_event_enter);
        }
        if (enterType.equals(EnterTypes.FINISHED)) {
            enrolDescription.setText("این رویداد به پایان رسیده است.\n منتظر رویداد های بعدی باشید!");
            sponsorRewardTitle.setText("مشاهده اهدا جوایز و برنده ها");
            sponsorRewardTitle.setTextColor(getResources().getColor(R.color.blue));
            enrolDescription.setTextColor(getResources().getColor(R.color.red));
            enterBtn.setBackgroundResource(R.drawable.slc_event_exit_btn);
        }

        //  init components
        enterBtn.setOnClickListener(v ->
                {
                    if (enterType.equals(EnterTypes.NOT_STARTED)) {
                        EventDialogFragment.getInstance().dismiss();
                    }
                    if (enterType.equals(EnterTypes.NOT_ENROLLED)) {
                        // kam kardan e seke
                        int coinCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.coin));
                        if (coinCount < event.getEnrolCost()) {
                            Dialogs.payment(getActivity(), false, MainActivity.getInstance().parent);
                        } else {
                            downloadEventData();
                        }

                    }
                    if (enterType.equals(EnterTypes.ENROLLED)) {
                        String eventLevelString = PreferencesHelper.getEventLevels(event.getName());
                        String eventSeasonString = PreferencesHelper.getEventSeasons(event.getName());
                        GameConfig game_config = new GameConfig();
                        try {
                            game_config = new GameConfig(event, AppModules.levelJsonToHashMap(new JSONArray(eventLevelString)), AppModules.seasonJsonToHashMap(new JSONArray(eventSeasonString)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MainActivity.Last_Config_Created = game_config;
                        MainActivity.getInstance().showMain(false);
                        MainActivity.getInstance().showLevels(true, game_config);
                        MainActivity.getInstance().makeIndicators(game_config);
                        EventDialogFragment.getInstance().dismiss();
                    }
                    if (enterType.equals(EnterTypes.FINISHED)) {
                        EventDialogFragment.getInstance().dismiss();
                    }

                }
        );

        wantToBeSponsor.setOnClickListener(v -> sponsoringRequest(getActivity()));

        sponsorRewardTitle.setOnClickListener(v -> {
            if (enterType.equals(EnterTypes.FINISHED)) {
                try {
                    Uri uri = Uri.parse(event.getSponsorLink());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (Exception e) {

                }
            }
        });

        sponsorLogo.setOnClickListener(v -> {
            try {
                Uri uri = Uri.parse(event.getSponsorLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            } catch (Exception e) {

            }
        });

        Glide.with(getActivity()).load(event.getSponsorLogo()).into(sponsorLogo);
        Glide.with(getActivity()).load(event.getSponsorPrizeImageUrl()).into(prizeImage);

        return view;
    }

    private void downloadInProgressUI() {

        dataLayout.setVisibility(View.GONE);
        enterBtn.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);

    }

    private void downloadComplete(boolean success) {

        if (success) {

            // user enrol karde ro save mikonim
            PreferencesHelper.setEventSettings(event.getName(), PreferencesHelper.enrolIsTrue, "1");

            enterType = EnterTypes.ENROLLED;
            enterBtn.callOnClick();

            EventDialogFragment.getInstance().dismiss();
        } else {
            okBtn.setVisibility(View.VISIBLE);
            okBtn.setOnClickListener(v ->
                    EventDialogFragment.getInstance().dismiss()
            );
            progressText.setText("دریافت با خطا مواجه شد !");
            progressText.setTextColor(MainActivity.getInstance().getResources().getColor(R.color.red));
        }

    }

    private void downloadEventData() {

        File destinationFile = new File(getActivity().getCacheDir(), "DownloadedLevelsFromServer");
        if (!destinationFile.exists()) {
            try {
                destinationFile.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        // kam kardan e seke
        int coinCount = Integer.parseInt(Coin.getCoinNumber());
        if (coinCount < event.getEnrolCost()) {
            Dialogs.payment(getActivity(), false, MainActivity.getInstance().parent).show();
        } else {
            PRDownloader.download(event.getLevelsUrl_android(), getActivity().getCacheDir().getAbsolutePath(), "DownloadedLevelsFromServer")
                    .build()
                    .setOnProgressListener(progress -> {
                        downloadInProgressUI();
                    })
                    .start(new OnDownloadListener() {
                        @Override
                        public void onDownloadComplete() {

                            // Extract in App's cache Directory
                            unzip(destinationFile);

                            // kam kardan e seke
                            Coin.decreaseCoins(event.getEnrolCost());

                            // load data from json file
                            String json = loadJSONFromFile();

                            // save levels in shared
                            saveDataInShared(event, json);

                            downloadComplete(true);
                        }

                        @Override
                        public void onError(Error error) {
                            downloadComplete(false);
                        }
                    });
        }

    }

    private void unzip(File zipFile) {
        try {
            byte[] buffer = new byte[1024];
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                String fileName = zipEntry.getName();
                File newFile = new File(getActivity().getCacheDir(), fileName);
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            zipFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String loadJSONFromFile() {
        String json;
        try {
            InputStream is = new FileInputStream(new File(getActivity().getCacheDir(), "config.txt"));
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;

    }

    private void saveDataInShared(Event event, String json) {

        try {
            JSONObject eventData = new JSONObject(json);

            // shuffle event levels
            JSONArray levelsArray = new JSONArray(eventData.getString("levels"));
            if (event.isShuffleLevels()) {
                shuffleLevels(levelsArray);
                sortLevels(levelsArray);
            }

            PreferencesHelper.initEventSettings(event.getName());
            PreferencesHelper.setEventSeasons(event.getName(), eventData.getString("seasons"));
            PreferencesHelper.setEventLevels(event.getName(), levelsArray.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void shuffleLevels(JSONArray levelsArray) {

        // Implementing Fisher–Yates shuffle
        Random rnd = new Random();
        for (int i = levelsArray.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            // Simple swap
            Object object;
            try {
                object = levelsArray.get(j);
                levelsArray.put(j, levelsArray.get(i));
                levelsArray.put(i, object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void sortLevels(JSONArray levelsArray) {

        for (int i = 0; i < levelsArray.length(); i++) {

            try {
                JSONObject jsonObject = (JSONObject) levelsArray.get(i);
                jsonObject.put("id", (i + 1));
                jsonObject.put("season_id", (i / 30) + 1);

                levelsArray.put(i, jsonObject);// add index ba id e jadid

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void sponsoringRequest(Context context) {
        Intent emailIntent = EmailIntentBuilder.from(context)
                .to("support@taxiapps.org")
                .subject("درخواست اسپانسرینگ | نیم کلمه")
                .body("توضیحات در مورد فعالیت شغلی و نوع مارکتینگ:" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "version: " + Application.appVersion + "\n" +
                        "device: Android")
                .build();
        context.startActivity(emailIntent);
    }

}