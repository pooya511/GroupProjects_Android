package com.taxiapps.wordcut.event.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.event.EventDialogFragment;
import com.taxiapps.wordcut.model.Event;
import com.taxiapps.wordcut.model.Level;
import com.taxiapps.wordcut.utils.AppModules;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EventExit extends Fragment {

    private Button exit;
    private TextView remainingScoreText;
    private ImageView prizeImage, sponsorLogo;

    private Event event;

    public static EventExit newInstance(Event event) {

        Bundle args = new Bundle();

        EventExit fragment = new EventExit();
        fragment.setArguments(args);
        fragment.event = event;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.pop_event_exit, container, false);

        // set Layout Font
        Typeface targetTypeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + "yekan.ttf");
        AppModules.setTypeface((ViewGroup) view, targetTypeFace);

        // find Components
        exit = view.findViewById(R.id.pop_event_exit_exit_btn);
        remainingScoreText = view.findViewById(R.id.pop_event_exit_remaining_score);
        prizeImage = view.findViewById(R.id.pop_event_exit_prize_image);
        sponsorLogo = view.findViewById(R.id.pop_event_exit_sponsor_logo);

        // init Components
        exit.setOnClickListener(v -> {
            EventDialogFragment.getInstance().dismiss();
        });

        ArrayList<String> ids = PreferencesHelper.getStringArrayPrefEvent(event.getName(), PreferencesHelper.passedLevelsKey);
        int currentScore = Integer.parseInt(ids.get(ids.size() - 1));
        int remainingScore;
        try {
            remainingScore = AppModules.levelJsonToHashMap(new JSONArray(PreferencesHelper.getEventLevels(event.getName()))).size();
            remainingScoreText.setText(String.valueOf(remainingScore - currentScore));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Glide.with(getActivity()).load(event.getSponsorPrizeImageUrl()).into(prizeImage);
        Glide.with(getActivity()).load(event.getSponsorLogo()).into(sponsorLogo);
        sponsorLogo.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(event.getSponsorLink()));
            startActivity(browserIntent);
        });

        return view;
    }
}