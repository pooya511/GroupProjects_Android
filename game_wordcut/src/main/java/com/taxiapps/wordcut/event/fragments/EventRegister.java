package com.taxiapps.wordcut.event.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.bumptech.glide.Glide;
import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.event.EventDialogFragment;
import com.taxiapps.wordcut.model.Event;
import com.taxiapps.wordcut.utils.AppModules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import modules.PublicModules;

public class EventRegister extends Fragment {

    private Button register;
    private TextView userScore, description, errorValidation;
    private EditText name, phone, email;
    private ImageView sponsorLogo;

    private Event event;
    private int score;

    public static EventRegister newInstance(Event event) {

        Bundle args = new Bundle();

        EventRegister fragment = new EventRegister();
        fragment.setArguments(args);
        fragment.event = event;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.pop_event_register, container, false);

        // set Layout Font
        Typeface targetTypeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + "yekan.ttf");
        AppModules.setTypeface((ViewGroup) view, targetTypeFace);

        // find components
        register = view.findViewById(R.id.pop_event_register_register_btn);
        userScore = view.findViewById(R.id.pop_event_register_user_score_text);
        description = view.findViewById(R.id.pop_event_register_score_description);
        name = view.findViewById(R.id.pop_event_register_name_edittext);
        phone = view.findViewById(R.id.pop_event_register_phone_edittext);
        email = view.findViewById(R.id.pop_event_register_email_edittext);
        errorValidation = view.findViewById(R.id.pop_event_register_validation_text);
        sponsorLogo = view.findViewById(R.id.pop_event_register_sponsor_logo);

        if (PreferencesHelper.getEventSettings(event.getName(), PreferencesHelper.isUserSubmitted).equals("1")) {
            name.setText(PreferencesHelper.getEventSettings(event.getName(), PreferencesHelper.eventUserName));
            phone.setText(PreferencesHelper.getEventSettings(event.getName(), PreferencesHelper.eventUserPhone));
            phone.setFocusable(false);
            phone.setFocusableInTouchMode(false);
            phone.setClickable(false);
            phone.setBackgroundResource(R.drawable.sh_event_register_editttext_disabled);
            phone.setTextColor(getResources().getColor(R.color.grayS));
            email.setText(PreferencesHelper.getEventSettings(event.getName(), PreferencesHelper.eventUserMail));
        }

        // init components
        Glide.with(getActivity()).load(event.getSponsorLogo()).into(sponsorLogo);
        sponsorLogo.setOnClickListener(v -> {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(event.getSponsorLink()));
                startActivity(browserIntent);
            } catch (Exception e) {

            }
        });
        ArrayList<String> ids = PreferencesHelper.getStringArrayPrefEvent(event.getName(), PreferencesHelper.passedLevelsKey);
        score = Integer.valueOf(ids.get(ids.size() - 1));
        userScore.setText(PublicModules.toPersianDigit(String.valueOf(score)));
        description.setText("شما " + PublicModules.toPersianDigit(String.valueOf(score)) + " امتیاز دارید. برای شرکت در قرعه کشی مشخصات خود را وارد کنید.\n تمام کردن بیشتر مراحل = شانس بیشتر !");
        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                errorValidation.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                errorValidation.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        register.setOnClickListener(v -> {
            if (inputsIsValid()) {
                String phoneNumber = null;
                if (PreferencesHelper.getEventSettings(event.getName(), PreferencesHelper.isUserSubmitted).equals("0")) {
                    phoneNumber = toEnglishDigits(phone.getText().toString());
                }
                HashMap<String, String> map = new HashMap<>();
                map.put("eventName", event.getName());
                map.put("userName", name.getText().toString());
                map.put("userPhone", phoneNumber);
                map.put("userMail", email.getText().toString());
                map.put("userScore", String.valueOf(score));
                saveUserEventData(map);
                Application.backendlessHandling(event.getName(), new AsyncCallback<Map>() {
                    @Override
                    public void handleResponse(Map response) {
                         PreferencesHelper.setEventSettings(event.getName(), PreferencesHelper.eventUserPendingData, "0"); // yani movafagh bud va datay pending nadare
                        PreferencesHelper.setEventSettings(event.getName(), PreferencesHelper.isUserSubmitted, "1");
                        EventDialogFragment.getInstance().initTarget(EventDialogFragment.EXIT, event);

                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        PreferencesHelper.setEventSettings(event.getName(), PreferencesHelper.eventUserPendingData, "1"); // yani na movafagh bud va bayad har vaght net dashtim dobare befrestim
                        PreferencesHelper.setEventSettings(event.getName(), PreferencesHelper.isUserSubmitted, "1");
                        EventDialogFragment.getInstance().initTarget(EventDialogFragment.EXIT, event);

                    }
                });

            }
        });

        return view;
    }

    private boolean inputsIsValid() {

        String nameText = name.getText().toString();
        String phoneText = phone.getText().toString();
        String emailText = email.getText().toString();

        if (TextUtils.isEmpty(nameText)) {
            errorValidation.setVisibility(View.VISIBLE);
            errorValidation.setText("فیلد نام نمی تواند خالی باشد");
            return false;
        }

        if (TextUtils.isEmpty(phoneText)) {
            return false;
        } else if (!PublicModules.toEnglishDigit(phoneText).matches("(^[0][9][0-9]{9}$)|(^[9][0-9]{9}$)")) {
            errorValidation.setVisibility(View.VISIBLE);
            errorValidation.setText("شماره تلفن وارد شده صحیح نیست");
            return false;
        }

        if (!emailText.equals("")) {
            if (!emailText.matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}") || emailText.contains("www.")) {
                errorValidation.setVisibility(View.VISIBLE);
                errorValidation.setText("ایمیل وارد شده صحیح نیست");
                return false;
            }
        }

        if (score == 0) {
            errorValidation.setVisibility(View.VISIBLE);
            errorValidation.setText("شما هنوز هیچ امتیازی ندارید");
            return false;
        }

        errorValidation.setVisibility(View.INVISIBLE);
        return true;
    }

    private void saveUserEventData(HashMap<String, String> map) {
        PreferencesHelper.setEventSettings(map.get("eventName"), PreferencesHelper.eventUserScore, map.get("userScore"));
        PreferencesHelper.setEventSettings(map.get("eventName"), PreferencesHelper.eventUserName, map.get("userName"));
        if (map.get("userPhone") != null) {
            PreferencesHelper.setEventSettings(map.get("eventName"), PreferencesHelper.eventUserPhone, map.get("userPhone"));
        }
        PreferencesHelper.setEventSettings(map.get("eventName"), PreferencesHelper.eventUserMail, map.get("userMail"));
    }

    private String toEnglishDigits(String str) {
        return str.replace("١", "1").replace("۱", "1")
                .replace("٢", "2").replace("۲", "2")
                .replace("٣", "3").replace("۳", "3")
                .replace("٤", "4").replace("۴", "4")
                .replace("٥", "5").replace("۵", "5")
                .replace("٦", "6").replace("۶", "6")
                .replace("٧", "7").replace("۷", "7")
                .replace("٨", "8").replace("۸", "8")
                .replace("٩", "9").replace("۹", "9")
                .replace("٠", "0").replace("۰", "0");
    }
}
