package com.taxiapps.wordcut.event;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.event.fragments.EventEntering;
import com.taxiapps.wordcut.event.fragments.EventExit;
import com.taxiapps.wordcut.event.fragments.EventRegister;
import com.taxiapps.wordcut.model.Event;

import java.util.Calendar;
import java.util.Date;

public class EventDialogFragment extends DialogFragment {

    public static final int ENTER = 1;
    public static final int REGISTER = 2;
    public static final int EXIT = 3;

    private FrameLayout fragmentContainer;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private static EventDialogFragment instance;
    private Event event;
    private int fragmentType;
    private PorterShapeImageView eventParentHeader;

    public static EventDialogFragment getInstance() {
        return instance;
    }

    public static EventDialogFragment newInstance(Event event, int fragmentType) {

        Bundle bundle = new Bundle();
        EventDialogFragment eventFragmentParent = new EventDialogFragment();
        eventFragmentParent.setArguments(bundle);

        eventFragmentParent.event = event;
        eventFragmentParent.fragmentType = fragmentType;

        return eventFragmentParent;
    }

    @SuppressLint("ResourceType")
    public void initTarget(int targetFragmentID, Event event) {

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.start_slide_left, R.anim.end_slide_left);

        switch (targetFragmentID) {
            case ENTER:
                Calendar endEventDate = Calendar.getInstance();
                endEventDate.setTime(new Date(event.getEventEnd()));

                Calendar startEventDate = Calendar.getInstance();
                startEventDate.setTime(new Date(event.getEventStart()));

                Calendar todayDate = Calendar.getInstance();

                if (startEventDate.getTimeInMillis() > todayDate.getTimeInMillis()) {
                    fragmentTransaction.replace(fragmentContainer.getId(), EventEntering.newInstance(event , EventEntering.EnterTypes.NOT_STARTED), "ENTER_FRAGMENT");
                }
                if (startEventDate.getTimeInMillis() < todayDate.getTimeInMillis()&& endEventDate.getTimeInMillis() > todayDate.getTimeInMillis() && PreferencesHelper.getEventSettings(event.getName() , PreferencesHelper.enrolIsTrue).equals("")){
                    fragmentTransaction.replace(fragmentContainer.getId(), EventEntering.newInstance(event , EventEntering.EnterTypes.NOT_ENROLLED), "ENTER_FRAGMENT");
                }
                if (startEventDate.getTimeInMillis() < todayDate.getTimeInMillis()&& endEventDate.getTimeInMillis() > todayDate.getTimeInMillis() && PreferencesHelper.getEventSettings(event.getName() , PreferencesHelper.enrolIsTrue).equals("1")){
                    fragmentTransaction.replace(fragmentContainer.getId(), EventEntering.newInstance(event , EventEntering.EnterTypes.ENROLLED), "ENTER_FRAGMENT");
                }
                if (endEventDate.getTimeInMillis() < todayDate.getTimeInMillis()){
                    fragmentTransaction.replace(fragmentContainer.getId(), EventEntering.newInstance(event , EventEntering.EnterTypes.FINISHED), "ENTER_FRAGMENT");
                }
                break;
            case REGISTER:
                fragmentTransaction.replace(fragmentContainer.getId(), EventRegister.newInstance(event), "REGISTER_FRAGMENT");
                break;
            case EXIT:
                fragmentTransaction.replace(fragmentContainer.getId(), EventExit.newInstance(event), "EXIT_FRAGMENT");
                break;

        }

        fragmentTransaction.addToBackStack(null);
        if (getActivity() != null)
            if (!getActivity().isFinishing())
                fragmentTransaction.commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.pop_event_parent, container, false);
        eventParentHeader = view.findViewById(R.id.pop_event_parent_header_image);

        fragmentContainer = view.findViewById(R.id.pop_event_parent_fragment_container);

        instance = this;
        fragmentManager = getChildFragmentManager();
        switch (fragmentType) {

            case ENTER:
                initTarget(EventDialogFragment.ENTER, event);
                break;
            case REGISTER:
                initTarget(EventDialogFragment.REGISTER, event);
                break;
            case EXIT:
                initTarget(EventDialogFragment.EXIT, event);
                break;

        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window dialogWindow = getDialog().getWindow();
        dialogWindow.setBackgroundDrawableResource(android.R.color.transparent);
        getDialog().setCanceledOnTouchOutside(true);

        Glide.with(getActivity()).load(event.getImageUrlEventHeader()).into(eventParentHeader);

        int width;
        int height;
        switch (screenSize(getActivity())) {
            case 1:
                width = 80;
                height = 75;
                break;
            case 2:
                width = 75;
                height = 70;
                break;
            case 3:
                width = 65;
                height = 65;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }

        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        //
        int ScreenSizeResults[] = getWidthAndHeight(getActivity(), width, height);
        lp.width = ScreenSizeResults[0];
        lp.height = ScreenSizeResults[1];

        dialogWindow.setAttributes(lp);
    }

    private int screenSize(Context context) {

        int screenSize = context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return 1;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return 2;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return 3;
            default:
                return 4; // alaki
        }
    }

    private int[] getWidthAndHeight(Context context, int widthPercent, int heightPercent) {

        int widthAndHeight[] = new int[2];
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        widthAndHeight[0] = widthPercent * displayMetrics.widthPixels / 100;
        widthAndHeight[1] = heightPercent * displayMetrics.heightPixels / 100;
        return widthAndHeight;
    }
}