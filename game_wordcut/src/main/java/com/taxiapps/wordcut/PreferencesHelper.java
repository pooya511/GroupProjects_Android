package com.taxiapps.wordcut;

import android.content.Context;
import android.content.SharedPreferences;

import com.taxiapps.wordcut.model.json.AchievementJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Parsa on 2018-03-15.
 */

public class PreferencesHelper {

    private static final String sharedName = "SharedPreferences";

    public static final String passedLevelsKey = "passedLevelsKey";
    public static final String unLockedLevels = "unLockedLevels";
    public static final String passedSeasons = "passedSeasons";
    public static final String unLockedSeasons = "unLockedSeasons";

    public static final String musicIsEnabled = "musicIsEnabled";
    public static final String sfxIsEnabled = "sfxIsEnabled";

    public static final String coin = "coin";

    public static final String eyeUsedCount = "eyeUsedCount";
    public static final String dicUsedCount = "dicUsedCount";
    public static final String skipUsedCount = "skipUsedCount";
    public static final String playTimeValue = "playTimeValue";
    public static final String oneShotCount = "oneShotCount";
    public static final String shareCount = "shareCount";
    public static final String tapLogoCount = "tapLogoCount";
    public static final String listAppearCount = "listAppearCount";

    public static final String unlockedAchievement = "unlockedAchievement";
    public static final String collectedAchievement = "collectedAchievement";

    public static final String playCount = "playCount";
    public static final String fastestComplete = "fastestComplete";
    public static final String wrongAnswer = "wrongAnswer";
    public static final String purchaseCount = "purchaseCount";

    public static final String isUserRated = "isUserRated";
    public static final String levelsToRate = "levelsToRate";

    public static final String gamePlayLevelOneFirstTime = "gamePlayLevelOneFirstTime";
    public static final String gamePlayLevelTwoFirstTime = "gamePlayLevelTwoFirstTime";

    public static final String eventLevels = "eventLevels";
    public static final String enrolIsTrue = "enrolIsTrue";

    public static final String eventUserName = "eventUserName";
    public static final String eventUserPhone = "eventUserPhone";
    public static final String eventUserMail = "eventUserMail";
    public static final String eventUserScore = "eventUserScore";
    public static final String eventUserPendingData = "eventUserPendingData";

    public static final String isUserSubmitted = "isUserSubmitted";
    public static final String lastLevelAd = "lastLevelAd";

    public static final String eyeAdTimeLimit = "eyeAdTimeLimit";
    public static final String dicAdTimeLimit = "dicAdTimeLimit";
    public static final String skipAdTimeLimit = "skipAdTimeLimit";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public static void init(Context context) {

        sharedPreferences = context.getSharedPreferences(sharedName, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (!sharedPreferences.contains(passedLevelsKey)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("0");
            setStringArrayPref(passedLevelsKey, defaultValues);
        }

        if (!sharedPreferences.contains(unLockedLevels)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("1");
//            defaultValues.add("2");
//            defaultValues.add("3");
//            defaultValues.add("4");
//            defaultValues.add("5");
//            defaultValues.add("6");
//            defaultValues.add("7");
//            defaultValues.add("8");
//            defaultValues.add("9");
//            defaultValues.add("10");
//            defaultValues.add("11");
//            defaultValues.add("12");
//            defaultValues.add("13");
//            defaultValues.add("14");
//            defaultValues.add("15");
//            defaultValues.add("16");
//            defaultValues.add("17");
//            defaultValues.add("18");
//            defaultValues.add("19");
//            defaultValues.add("20");
//            defaultValues.add("21");
//            defaultValues.add("22");
//            defaultValues.add("23");
//            defaultValues.add("24");
//            defaultValues.add("25");
//            defaultValues.add("26");
//            defaultValues.add("27");
//            defaultValues.add("28");
//            defaultValues.add("29");
//            defaultValues.add("30");
            setStringArrayPref(unLockedLevels, defaultValues);
        }

        if (!sharedPreferences.contains(levelsToRate)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("3");
            defaultValues.add("18");
            defaultValues.add("33");
            defaultValues.add("60");
            defaultValues.add("90");
            defaultValues.add("110");
            defaultValues.add("130");
            defaultValues.add("150");
            defaultValues.add("180");
            defaultValues.add("220");
            defaultValues.add("250");
            defaultValues.add("288");

            setStringArrayPref(levelsToRate, defaultValues);
        }

        if (!sharedPreferences.contains(passedSeasons)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("0");

            setStringArrayPref(passedSeasons, defaultValues);
        }

        if (!sharedPreferences.contains(unLockedSeasons)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("1");

            setStringArrayPref(unLockedSeasons, defaultValues);
        }

        if (!sharedPreferences.contains(unlockedAchievement)) {
            ArrayList<String> defaultValues = new ArrayList<>(); // default values should be empty
            setStringArrayPref(unlockedAchievement, defaultValues);
        }

        if (!sharedPreferences.contains(collectedAchievement)) {
            ArrayList<String> defaultValues = new ArrayList<>(); // default values should be empty
            setStringArrayPref(collectedAchievement, defaultValues);
        }

        if (!sharedPreferences.contains(musicIsEnabled)) {
            editor.putString(musicIsEnabled, "1");
            editor.apply();
        }

        if (!sharedPreferences.contains(sfxIsEnabled)) {
            editor.putString(sfxIsEnabled, "1");
            editor.apply();
        }

        if (!sharedPreferences.contains(coin)) {
            editor.putString(coin, "100");
            editor.apply();
        }

        if (!sharedPreferences.contains(eyeUsedCount)) {
            editor.putString(eyeUsedCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(dicUsedCount)) {
            editor.putString(dicUsedCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(skipUsedCount)) {
            editor.putString(skipUsedCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(playTimeValue)) {
            editor.putString(playTimeValue, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(oneShotCount)) {
            editor.putString(oneShotCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(shareCount)) {
            editor.putString(shareCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(tapLogoCount)) {
            editor.putString(tapLogoCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(listAppearCount)) {
            editor.putString(listAppearCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(gamePlayLevelOneFirstTime)) {
            editor.putString(gamePlayLevelOneFirstTime, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(gamePlayLevelTwoFirstTime)) {
            editor.putString(gamePlayLevelTwoFirstTime, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(eventLevels)) {
            editor.putString(eventLevels, "");
            editor.apply();
        }

        if (!sharedPreferences.contains(isUserRated)) {
            editor.putString(isUserRated, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(lastLevelAd)) {
            editor.putString(lastLevelAd, "2"); // az 1 ta 3 nabayad ad neshun bede. default value ro mizarim 2 ke age khast neshun bede, az 4 neshun bede
            editor.apply();
        }

        if (!sharedPreferences.contains(eyeAdTimeLimit)) {
            editor.putString(eyeAdTimeLimit, "0");
        }

        if (!sharedPreferences.contains(dicAdTimeLimit)) {
            editor.putString(dicAdTimeLimit, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(skipAdTimeLimit)) {
            editor.putString(skipAdTimeLimit, "0");
            editor.apply();
        }

        // Achievements
        initAchievements();

        // Reports
        initReports();

    }

    private static void initAchievements() {

        if (!sharedPreferences.contains(AchievementJson.Achievements.levels_1.value)) {
            editor.putString(AchievementJson.Achievements.levels_1.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.levels_5.value)) {
            editor.putString(AchievementJson.Achievements.levels_5.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.levels_20.value)) {
            editor.putString(AchievementJson.Achievements.levels_20.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.levels_50.value)) {
            editor.putString(AchievementJson.Achievements.levels_50.value, "0");
            editor.apply();
        }

//        if (!sharedPreferences.contains(AchievementJson.Achievements.levels_100.value)) {
//            editor.putString(AchievementJson.Achievements.levels_100.value, "0");
//            editor.apply();
//        }
//
//        if (!sharedPreferences.contains(AchievementJson.Achievements.levels_200.value)) {
//            editor.putString(AchievementJson.Achievements.levels_200.value, "0");
//            editor.apply();
//        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.record_5s.value)) {
            editor.putString(AchievementJson.Achievements.record_5s.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.record_10s.value)) {
            editor.putString(AchievementJson.Achievements.record_10s.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.record_30s.value)) {
            editor.putString(AchievementJson.Achievements.record_30s.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.share_1.value)) {
            editor.putString(AchievementJson.Achievements.share_1.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.share_10.value)) {
            editor.putString(AchievementJson.Achievements.share_10.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.eye_1.value)) {
            editor.putString(AchievementJson.Achievements.eye_1.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.eye_3.value)) {
            editor.putString(AchievementJson.Achievements.eye_3.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.eye_10.value)) {
            editor.putString(AchievementJson.Achievements.eye_10.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.dic_1.value)) {
            editor.putString(AchievementJson.Achievements.dic_1.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.dic_3.value)) {
            editor.putString(AchievementJson.Achievements.dic_3.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.dic_10.value)) {
            editor.putString(AchievementJson.Achievements.dic_10.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.skip1.value)) {
            editor.putString(AchievementJson.Achievements.skip1.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.skip3.value)) {
            editor.putString(AchievementJson.Achievements.skip3.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.skip10.value)) {
            editor.putString(AchievementJson.Achievements.skip10.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.listenMusic.value)) {
            editor.putString(AchievementJson.Achievements.listenMusic.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.playtime_1h.value)) {
            editor.putString(AchievementJson.Achievements.playtime_1h.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.playtime_3h.value)) {
            editor.putString(AchievementJson.Achievements.playtime_3h.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.playtime_10h.value)) {
            editor.putString(AchievementJson.Achievements.playtime_10h.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.oneshot_1.value)) {
            editor.putString(AchievementJson.Achievements.oneshot_1.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.oneshot_10.value)) {
            editor.putString(AchievementJson.Achievements.oneshot_10.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.oneshot_20.value)) {
            editor.putString(AchievementJson.Achievements.oneshot_20.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.oneshot_50.value)) {
            editor.putString(AchievementJson.Achievements.oneshot_50.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.buyCoin.value)) {
            editor.putString(AchievementJson.Achievements.buyCoin.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.masterWord.value)) {
            editor.putString(AchievementJson.Achievements.masterWord.value, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(AchievementJson.Achievements.tapLogo.value)) {
            editor.putString(AchievementJson.Achievements.tapLogo.value, "0");
            editor.apply();
        }

    }

    private static void initReports() {

        if (!sharedPreferences.contains(playCount)) {
            editor.putString(playCount, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(fastestComplete)) {
            editor.putString(fastestComplete, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(wrongAnswer)) {
            editor.putString(wrongAnswer, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(purchaseCount)) {
            editor.putString(purchaseCount, "0");
            editor.apply();
        }

    }

    public static void initEventSettings(String eventName) {

        String newKeyNames = eventName + "_";

        if (!sharedPreferences.contains(newKeyNames + passedLevelsKey)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("0");
//            defaultValues.add("1");
//            defaultValues.add("2");
//            defaultValues.add("3");
//            defaultValues.add("4");
//            defaultValues.add("5");
//            defaultValues.add("6");
//            defaultValues.add("7");
//            defaultValues.add("8");
//            defaultValues.add("9");
//            defaultValues.add("10");
//            defaultValues.add("11");
//            defaultValues.add("12");
//            defaultValues.add("13");
//            defaultValues.add("14");
//            defaultValues.add("15");
//            defaultValues.add("16");
//            defaultValues.add("17");
//            defaultValues.add("18");
//            defaultValues.add("19");
//            defaultValues.add("20");
//            defaultValues.add("21");
//            defaultValues.add("22");
//            defaultValues.add("23");
//            defaultValues.add("24");
//            defaultValues.add("25");
//            defaultValues.add("26");
//            defaultValues.add("27");
//            defaultValues.add("28");
//            defaultValues.add("29");
//            defaultValues.add("30");
            setStringArrayPref(newKeyNames + passedLevelsKey, defaultValues);
        }

        if (!sharedPreferences.contains(newKeyNames + unLockedLevels)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("1");
            setStringArrayPref(newKeyNames + unLockedLevels, defaultValues);
        }

        if (!sharedPreferences.contains(newKeyNames + passedSeasons)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("0");
            setStringArrayPref(newKeyNames + passedSeasons, defaultValues);
        }

        if (!sharedPreferences.contains(newKeyNames + unLockedSeasons)) {
            ArrayList<String> defaultValues = new ArrayList<>();
            defaultValues.add("1");
            setStringArrayPref(newKeyNames + unLockedSeasons, defaultValues);
        }

        if (!sharedPreferences.contains(newKeyNames + enrolIsTrue)) {
            editor.putString(newKeyNames + enrolIsTrue, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(newKeyNames + eventUserName)) {
            editor.putString(newKeyNames + eventUserName, "");
            editor.apply();
        }

        if (!sharedPreferences.contains(newKeyNames + eventUserPhone)) {
            editor.putString(newKeyNames + eventUserPhone, "");
            editor.apply();
        }

        if (!sharedPreferences.contains(newKeyNames + eventUserMail)) {
            editor.putString(newKeyNames + eventUserMail, "");
            editor.apply();
        }

        if (!sharedPreferences.contains(newKeyNames + eventUserScore)) {
            editor.putString(newKeyNames + eventUserScore, "");
            editor.apply();
        }

        if (!sharedPreferences.contains(newKeyNames + eventUserPendingData)) {
            editor.putString(newKeyNames + eventUserPendingData, "0");
            editor.apply();
        }

        if (!sharedPreferences.contains(newKeyNames + isUserSubmitted)) {
            editor.putString(newKeyNames + isUserSubmitted, "0");
            editor.apply();
        }
    }

    public static void setString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public static void setStringArrayPref(String key, ArrayList<String> values) {
        JSONArray a = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            a.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, a.toString());
        } else {
            editor.putString(key, null);
        }
        editor.apply();
    }

    public static ArrayList<String> getStringArrayPref(String key) {
        String json = sharedPreferences.getString(key, null);
        ArrayList<String> urls = new ArrayList<>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public static ArrayList<String> getStringArrayPrefEvent(String eventName, String key) {
        String json = sharedPreferences.getString(eventName + "_" + key, null);
        ArrayList<String> urls = new ArrayList<>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public static void setStringArrayPrefEvent(String eventName, String key, ArrayList<String> values) {
        key = eventName + "_" + key;
        JSONArray a = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            a.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, a.toString());
        } else {
            editor.putString(key, null);
        }
        editor.apply();
    }

    public static void setEventSettings(String eventName, String key, String value) {
        editor.putString(eventName + "_" + key, value);
        editor.apply();
    }

    public static String getEventSettings(String eventName, String key) {
        return sharedPreferences.getString(eventName + "_" + key, "");
    }

    public static void setEventLevels(String eventName, String levels) {
        editor.putString("levels_" + eventName, levels);
        editor.apply();
    }

    public static String getEventLevels(String eventName) {
        return sharedPreferences.getString("levels_" + eventName, "");
    }

    public static void setEventSeasons(String eventName, String seasons) {
        editor.putString("seasons_" + eventName, seasons);
        editor.apply();
    }

    public static String getEventSeasons(String eventName) {
        return sharedPreferences.getString("seasons_" + eventName, "");
    }

}