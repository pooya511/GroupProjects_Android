package com.taxiapps.wordcut.model.json;

import com.taxiapps.wordcut.model.Level;

import java.util.HashMap;

/**
 * Created by Parsa on 2018-03-14.
 */

public class LevelJson {

    public static HashMap<String, Level> levels = new HashMap<>();

    public static void initLevels() {

        levels.put("1", new Level(1, 1, true, true, true, "پایتخت آن فرونزه است ", "قرقیزستان"));
        levels.put("2", new Level(2, 1, true, true, true, "معافیت", "بخشودگی"));
        levels.put("3", new Level(3, 1, true, true, true, "مستخدم", "خدمتگزار"));
        levels.put("4", new Level(4, 1, true, true, true, "سوغات یا ره آورد سفر", "ارمغان"));
        levels.put("5", new Level(5, 1, true, true, true, "با کسی چیزی بدل کردن", "مبادله"));
        levels.put("6", new Level(6, 1, true, true, true, "شبکه رایانه ای فراگیر", "اینترنت"));
        levels.put("7", new Level(7, 1, true, true, true, "شرح حال", "زندگی نامه"));
        levels.put("8", new Level(8, 1, true, true, true, "ماهر بودن در کاری", "مهارت"));
        levels.put("9", new Level(9, 1, true, true, true, "آنکه کارها را با عجله انجام می دهد.", "عجول"));
        levels.put("10", new Level(10, 1, true, true, true, "محل ریختن خاک روبه", "خاکدان"));
        levels.put("11", new Level(11, 1, true, true, true, "پاک و بی گناه", "معصوم"));
        levels.put("12", new Level(12, 1, true, true, true, "اندوهگین و عزادار بودن به خاطر از دست دادن کسی", "عزاداری"));
        levels.put("13", new Level(13, 1, true, true, true, "صبر؛ تحمل؛ بردباری", "حوصله"));
        levels.put("14", new Level(14, 1, true, true, true, "گرما؛ گرمی", "حرارت"));
        levels.put("15", new Level(15, 1, true, true, true, "واکسن زده شده", "واکسینه"));
        levels.put("16", new Level(16, 1, true, true, true, "بسیاری و افزونی", "اکثریت"));
        levels.put("17", new Level(17, 1, true, true, true, "سست؛ بی‌حال؛ شل", "لمس"));
        levels.put("18", new Level(18, 1, true, true, true, "نام شبه جزیره ای در شمال غربی امریکای شمالی متعلق بدول متحده امریکای شمالی ، دارای 55000 مردم", "آلاسکا"));
        levels.put("19", new Level(19, 1, true, true, true, "وسیله‌ای به شکل چند صفحۀ مدرج برای اندازه‌گیری ارتفاع ستارگان و مشخص کردن مکان آن‌ها", "اسطرلاب"));
        levels.put("20", new Level(20, 1, true, true, true, "محفظه ای ویژه ی حمل بار با کامیون یا کشتی به شکل مکعب مستطیل", "کانتینر"));
        levels.put("21", new Level(21, 1, true, true, true, "کودکی و خردسالی", "طفولیت"));
        levels.put("22", new Level(22, 1, true, true, true, "شب را در جایی به سر بردن", "بیتوته"));
        levels.put("23", new Level(23, 1, true, true, true, "کج شدن از راه راست", "منحرف"));
        levels.put("24", new Level(24, 1, true, true, true, "کار و فعالیت روی زمین برای تولید گیاهان و دانه های خوراکی", "کشاورزی"));
        levels.put("25", new Level(25, 1, true, true, true, "هریک از قسمت‌های سختی که اسکلت مهره‌داران را تشکیل می‌دهد.", "استخوان"));
        levels.put("26", new Level(26, 1, true, true, true, "از رشته‌های ورزشی که از حرکات هماهنگ بر روی زمین یا با وسایلی همچون بارفیکس، پارالل، خرک، تاب، و امثال آن‌ها انجام می‌شود.", "ژیمناستیک"));
        levels.put("27", new Level(27, 1, true, true, true, "به رنگ نقره", "نقره گون"));
        levels.put("28", new Level(28, 1, true, true, true, "کتابی که ادعا می شود به کمک آن می توان حوادث آینده زندگی خود را تشخیص داد", "فال نامه"));
        levels.put("29", new Level(29, 1, true, true, true, "عضو استوانه‌شکل گیاه و درخت که به خلاف ریشه در هوا به سمت بالا نمو می‌کند و حامل شاخه‌ها و برگ‌ها و گل‌ها است", "ساقه"));
        levels.put("30", new Level(30, 1, true, true, true, "عنوان مشهورسلسله ای از سلاطین مسلمان خوارزم", "خوارزمشاهیان"));
        levels.put("31", new Level(31, 2, true, true, true, "آن چه از روی منطق و تعقل باشد", "منطقی"));
        levels.put("32", new Level(32, 2, true, true, true, "مجموعه عمل ها و وسیله هایی که ارتباط را برقرار می کنند", "ارتباطات"));
        levels.put("33", new Level(33, 2, true, true, true, "جایی که گرداگرد آن را دیواری احاطه کرده باشد", "محوطه"));
        levels.put("34", new Level(34, 2, true, true, true, "مجموعه آثار مکتوب منظوم یا منثور که بیانگر عواطف است.", "ادبیات"));
        levels.put("35", new Level(35, 2, true, true, true, "روشن و درخشان", "مشعشع"));
        levels.put("36", new Level(36, 2, true, true, true, "مرد سال‌خورده و کهن‌سال", "پیرمرد"));
        levels.put("37", new Level(37, 2, true, true, true, "سیرشده از آب", "سیراب"));
        levels.put("38", new Level(38, 2, true, true, true, "مال و اثاثیۀ گرانبها داشتن", "تجمل"));
        levels.put("39", new Level(39, 2, true, true, true, "باهوش؛ هوشمند؛ زرنگ", "هوشیار"));
        levels.put("40", new Level(40, 2, true, true, true, "بو کشیدن", "استشمام"));
        levels.put("41", new Level(41, 2, true, true, true, "از بر خواندن شعر در موضوع هایی مشخص و یا خواندن بیت هایی که با حرفی معین آغاز شود", "مشاعره"));
        levels.put("42", new Level(42, 2, true, true, true, "کاغذ نسبتاً ضخیم و معمولاً دارای نقش و نگار که برای پوشاندن یا آرایش دیوارهای اتاق به کار می رود", "کاغذ دیواری"));
        levels.put("43", new Level(43, 2, true, true, true, "نشانه ها", "آثار"));
        levels.put("44", new Level(44, 2, true, true, true, "اقوام کوچ‌نشین که از طریق دامداری امرام معاش می‌کنند", "عشایر"));
        levels.put("45", new Level(45, 2, true, true, true, "خدا را به پاکی یاد کردن", "تسبیح"));
        levels.put("46", new Level(46, 2, true, true, true, "جمع نام ها", "اسامی"));
        levels.put("47", new Level(47, 2, true, true, true, "کسی که علم یا هنری را به دیگران تعلیم می‌دهد", "استاد"));
        levels.put("48", new Level(48, 2, true, true, true, "در خیاطی تکۀ کوچک سه‌گوش از پارچه", "مرغک"));
        levels.put("49", new Level(49, 2, true, true, true, "چیزی که با دلیل و مدرک همراه باشد", "مستند"));
        levels.put("50", new Level(50, 2, true, true, true, "رفتاری عمدی که برای آزار و اذیت کسی به تلافی کار بد او انجام می‌شود", "انتقام"));
        levels.put("51", new Level(51, 2, true, true, true, "بنا و ساختمان", "عمارت"));
        levels.put("52", new Level(52, 2, true, true, true, "کسی که در هر کار و هر هنری دست دارد.", "همه فن حریف"));
        levels.put("53", new Level(53, 2, true, true, true, "غلاف فشنگ بی سرب و باروت", "پوکه"));
        levels.put("54", new Level(54, 2, true, true, true, "کسی که نزد دیگری علم یا هنر می‌آموزد", "شاگرد"));
        levels.put("55", new Level(55, 2, true, true, true, "حل کننده مشکلات", "کارگشا"));
        levels.put("56", new Level(56, 2, true, true, true, "هر چیزی که با کاغذ کادو بسته بندی شده است", "کادوپیچ"));
        levels.put("57", new Level(57, 2, true, true, true, "بالا و پایین رفتن آب دریا", "جزر و مد"));
        levels.put("58", new Level(58, 2, true, true, true, "لبریز", "سرشار"));
        levels.put("59", new Level(59, 2, true, true, true, "خواندن کتاب", "تلاوت"));
        levels.put("60", new Level(60, 2, true, true, true, "آنچه باعث تکمیل و تمام شدن چیز دیگر باشد", "متمم"));
        levels.put("61", new Level(61, 3, true, true, true, "ورزش کار یا بازیکنی که به ورزش بسکتبال می پردازد.", "بسکتبالیست"));
        levels.put("62", new Level(62, 3, true, true, true, "آهسته و بی سر و صدا راه رفتن", "پاورچین"));
        levels.put("63", new Level(63, 3, true, true, true, "فن آواز خواندن و نواختن ساز", "موسیقی"));
        levels.put("64", new Level(64, 3, true, true, true, "نوعی فرش که با نخ پنبه‌ای یا پشمی", "گلیم"));
        levels.put("65", new Level(65, 3, true, true, true, "وسیله گردنده بازی و سواری کودکان", "چرخ و فلک"));
        levels.put("66", new Level(66, 3, true, true, true, "گشادگی؛ گشادی؛ فراخی جا؛ پهنه", "وسعت"));
        levels.put("67", new Level(67, 3, true, true, true, "استاد دانشگاه", "پروفسور"));
        levels.put("68", new Level(68, 3, true, true, true, "هر کس از شدت ناراحتی بلند حرف می زند و ممکن است با خشونت رفتار کند", "عصبانی"));
        levels.put("69", new Level(69, 3, true, true, true, "ذات هر شخص", "شخصیت"));
        levels.put("70", new Level(70, 3, true, true, true, "نوعی کرم حلقوی‌شکل با زندگی انگلی که در رودۀ انسان و حیوان زندگی می‌کند", "آسکاریس"));
        levels.put("71", new Level(71, 3, true, true, true, "واژه ای است که برای آگاهی دادن و هشدار  کار می رود", "زنهار"));
        levels.put("72", new Level(72, 3, true, true, true, "دور شدن یا جدا شدن از راه‌ها", "انحراف"));
        levels.put("73", new Level(73, 3, true, true, true, "جایی که ستاره‌شناسان در آن با‌ ابزارهای مخصوص ستارگان و اجرام نجومی را مشاهده و بررسی می‌کنند", "رصدخانه"));
        levels.put("74", new Level(74, 3, true, true, true, "جای کشت‌وزرع", "مزرعه"));
        levels.put("75", new Level(75, 3, true, true, true, "دارای شوق و اشتیاق", "مشتاق"));
        levels.put("76", new Level(76, 3, true, true, true, "کسی که به او اشاره شده", "مشارالیه"));
        levels.put("77", new Level(77, 3, true, true, true, "پاک دلی", "اخلاص"));
        levels.put("78", new Level(78, 3, true, true, true, "ارمغان و پیشکش", "هدایا"));
        levels.put("79", new Level(79, 3, true, true, true, "فایده گرفتن", "استفاده"));
        levels.put("80", new Level(80, 3, true, true, true, "کامیابی", "پیروزی"));
        levels.put("81", new Level(81, 3, true, true, true, "احداث، ایجاد، بنیان، بنیانگذاری", "تاسیس"));
        levels.put("82", new Level(82, 3, true, true, true, "اقدام کردن به ‌کاری", "مبادرت"));
        levels.put("83", new Level(83, 3, true, true, true, "ماده ای رنگی که در نقاشی برای رنگ آمیزی استفاده می شود", "آبرنگ"));
        levels.put("84", new Level(84, 3, true, true, true, "مجموعه آداب و سنن و دینی و ملی یک سرزمین", "فرهنگ"));
        levels.put("85", new Level(85, 3, true, true, true, "اولین", "نخستین"));
        levels.put("86", new Level(86, 3, true, true, true, "بناها و موانعی که برای دفع دشمنان می سازند", "استحکامات"));
        levels.put("87", new Level(87, 3, true, true, true, "توجه نمودن", "عنایت"));
        levels.put("88", new Level(88, 3, true, true, true, "آن بخش از لباس که دور گردن قرار دارد", "یقه"));
        levels.put("89", new Level(89, 3, true, true, true, "رستنی قدیمی که بزرگ و ستبری که دارای ریشه و تنه و شاخه باشد", "درخت کهن"));
        levels.put("90", new Level(90, 3, true, true, true, "هر بچه ای که به شدت مورد علاقه و توجه پدر و مادر و اطرافیانش باشد", "عزیزدردانه"));
        levels.put("91", new Level(91, 4, true, true, true, "گسترده شدن چیزی", "انتشار"));
        levels.put("92", new Level(92, 4, true, true, true, "سازمان یا انجمنی که برای اداره کردن رشته‌ای ورزشی تشکیل شود", "فدراسیون"));
        levels.put("93", new Level(93, 4, true, true, true, "کار خود را خدا واگذار کردن", "توکل"));
        levels.put("94", new Level(94, 4, true, true, true, "شایستگی", "قابلیت"));
        levels.put("95", new Level(95, 4, true, true, true, "حالت گرفتگی روحی که در گفتار و چهره نمایان می ود", "بغض"));
        levels.put("96", new Level(96, 4, true, true, true, "بزرگی خیلی زیاد", "عظمت"));
        levels.put("97", new Level(97, 4, true, true, true, "نوعی قندیل بزرگ که چراغ ها یا شمع های فراوان در آن قرار می دهند", "چلچراغ"));
        levels.put("98", new Level(98, 4, true, true, true, "انبساط، خرمی، خوشی", "نشاط"));
        levels.put("99", new Level(99, 4, true, true, true, "وسیله ای در روزگار گذشته برای شمارش اعداد از آن استفاده می کردند", "چرتکه"));
        levels.put("100", new Level(100, 4, true, true, true, "کار و کوشش", "سعی"));
        levels.put("101", new Level(101, 4, true, true, true, " استقامت", "ایستادگی"));
        levels.put("102", new Level(102, 4, true, true, true, "پسماندها", "فضولات"));
        levels.put("103", new Level(103, 4, true, true, true, "آمیخته‌شده با چیزی", "آغشته"));
        levels.put("104", new Level(104, 4, true, true, true, "نیازمند", "محتاج"));
        levels.put("105", new Level(105, 4, true, true, true, "پریشان‌خاطر", "آشفته خاطر"));
        levels.put("106", new Level(106, 4, true, true, true, "آبگیر مصنوعی برای تزیین یا نگه‌داشتن آب", "حوض"));
        levels.put("107", new Level(107, 4, true, true, true, "واقعی؛ راستین", "حقیقی"));
        levels.put("108", new Level(108, 4, true, true, true, "حصیری که از نی می بافند", "بوریا"));
        levels.put("109", new Level(109, 4, true, true, true, "متداول شدن", "معمول"));
        levels.put("110", new Level(110, 4, true, true, true, "واژه ای است برای اشاره به شخص غیر معمول", "فلان"));
        levels.put("111", new Level(111, 4, true, true, true, "رساندن پیام یا خیر یا مطلبی به مردم", "تبلیغ"));
        levels.put("112", new Level(112, 4, true, true, true, "معاینة مجراها و حفره های داخلی بدن با درون بین", "آندوسکوپی"));
        levels.put("113", new Level(113, 4, true, true, true, "داوری در مورد امر مورد دعوا.", "قضاوت"));
        levels.put("114", new Level(114, 4, true, true, true, "قصدکننده بر انجام کاری", "عازم"));
        levels.put("115", new Level(115, 4, true, true, true, "سرانجام", "عاقبت"));
        levels.put("116", new Level(116, 4, true, true, true, "اندرز، پند", "نصیحت"));
        levels.put("117", new Level(117, 4, true, true, true, "هرچیز گرد و دایره‌شکل", "حلقه"));
        levels.put("118", new Level(118, 4, true, true, true, "اسم کردی به معنی روشنایی می باشد", "روناک"));
        levels.put("119", new Level(119, 4, true, true, true, "کنایه از محروم ماندن", "تهی ماندن"));
        levels.put("120", new Level(120, 4, true, true, true, "بزرگ‌ترین جشن ملی ایرانیان که از روز اول فروردین آغاز می‌شود", "نوروز"));
        levels.put("121", new Level(121, 5, true, true, true, "فرزند حاج میرزا علی اکبر قوام الملک شیرازی ", "فتحعلیخان"));
        levels.put("122", new Level(122, 5, true, true, true, "تواضع و فروتنی", "افتادگی"));
        levels.put("123", new Level(123, 5, true, true, true, "قاعده و معیار", "هنجار"));
        levels.put("124", new Level(124, 5, true, true, true, "یک قسمت از چهار قسمت روز که در آن چیزی بخورند", "چاشت"));
        levels.put("125", new Level(125, 5, true, true, true, "در جایی ماندن", "اقامت"));
        levels.put("126", new Level(126, 5, true, true, true, "چیزی که با چیز دیگر پیوستگی و ارتباط داشته باشد", "مرتبط"));
        levels.put("127", new Level(127, 5, true, true, true, "غیر واقعی و جعلی", "مصنوعی"));
        levels.put("128", new Level(128, 5, true, true, true, "خیر اندیشی", "مصلحت"));
        levels.put("129", new Level(129, 5, true, true, true, "گروهی از جانوران خزنده، چابک، و شبیه سوسمار با بدن فلس‌دار و دُمِ بلند که قادرند دُم ازدست‌رفتۀ خود را ترمیم کنند.", "مارمولک"));
        levels.put("130", new Level(130, 5, true, true, true, "نامعلوم", "ناشناخته"));
        levels.put("131", new Level(131, 5, true, true, true, "پادزهر", "نوش دارو"));
        levels.put("132", new Level(132, 5, true, true, true, "احساس دریغ، حسرت، و اندوه", "افسوس"));
        levels.put("133", new Level(133, 5, true, true, true, "برومند", "حاصل خیز"));
        levels.put("134", new Level(134, 5, true, true, true, "اصطلاحاً به روزنامه ها و مجلاّت گفته می شود", "مطبوعات"));
        levels.put("135", new Level(135, 5, true, true, true, "آراسته و پاکیزه", "پیراسته"));
        levels.put("136", new Level(136, 5, true, true, true, "به دست آوردن", "دستیابی"));
        levels.put("137", new Level(137, 5, true, true, true, "آزادی داشتن؛ به آزادی کاری کردن؛ بدون مداخلۀ کسی کار خود را اداره کردن", "استقلال"));
        levels.put("138", new Level(138, 5, true, true, true, "سستی؛ بی‌حالی", "کسالت"));
        levels.put("139", new Level(139, 5, true, true, true, "چشم‌انداز", "منظره"));
        levels.put("140", new Level(140, 5, true, true, true, "حقیقت و ذات", "ماهیت"));
        levels.put("141", new Level(141, 5, true, true, true, "هدهد", "پوپک"));
        levels.put("142", new Level(142, 5, true, true, true, "آنکه بدنی نیرومند دارد", "رویین تن"));
        levels.put("143", new Level(143, 5, true, true, true, "پشت خود را به چیزی نهادن", "تکیه"));
        levels.put("144", new Level(144, 5, true, true, true, "افتاده، خاشع، خاضع، خاکی", "متواضع"));
        levels.put("145", new Level(145, 5, true, true, true, "ریشه‌دار بودن", "اصالت"));
        levels.put("146", new Level(146, 5, true, true, true, "دانش بررسی روش‌های کسب درآمد", "اقتصاد"));
        levels.put("147", new Level(147, 5, true, true, true, "شغل و عمل بازیگر", "بازیگری"));
        levels.put("148", new Level(148, 5, true, true, true, "سرد و بی روح", "نژند"));
        levels.put("149", new Level(149, 5, true, true, true, "درنگ کردن", "مکث"));
        levels.put("150", new Level(150, 5, true, true, true, "شعری که هر دو مصراع آن قافیه داشته باشد و قافیۀ مصراع دوم آن نظیر قافیۀ مصراع اول باشد", "مثنوی"));
        levels.put("151", new Level(151, 6, true, true, true, "بهره‌گیری از یافته‌های علمی برای بهبود عملکرد در زمینه‌های صنعتی، کشاورزی، آموزشی، اقتصادی، و مانند آن‌ها", "تکنولوژی"));
        levels.put("152", new Level(152, 6, true, true, true, "صاف و هموار", "مسطح"));
        levels.put("153", new Level(153, 6, true, true, true, "صفات چیزی را بیان کردن", "توصیف کردن"));
        levels.put("154", new Level(154, 6, true, true, true, "وزارتخانه یا اداره‌ای که به امور حقوقی و جزایی مردم رسیدگی می‌کند", "دادگستری"));
        levels.put("155", new Level(155, 6, true, true, true, "شاد و خوشحال", "مسرور"));
        levels.put("156", new Level(156, 6, true, true, true, "دست یافتن بر کسی یا چیزی", "تسلط"));
        levels.put("157", new Level(157, 6, true, true, true, "شغل و عمل راننده", "رانندگی"));
        levels.put("158", new Level(158, 6, true, true, true, "معناها و مفهوم ها", "مضامین"));
        levels.put("159", new Level(159, 6, true, true, true, "شغل و عمل رختشو", "رختشویی"));
        levels.put("160", new Level(160, 6, true, true, true, "جانوری از ردۀ دوزیستان با پاهای قوی و پره‌دار که در آب تخم می‌ریزد", "قورباغه"));
        levels.put("161", new Level(161, 6, true, true, true, "نام قدیمی توالت یا دستشویی", "آبریزگاه"));
        levels.put("162", new Level(162, 6, true, true, true, "آسودگی و راحتی", "آسایش"));
        levels.put("163", new Level(163, 6, true, true, true, "پذیرنده عذر خواهی", "پوزش پذیر"));
        levels.put("164", new Level(164, 6, true, true, true, "ضد شوخی و از روی حقیقت و راستی و جداً", "جدی"));
        levels.put("165", new Level(165, 6, true, true, true, "در بر گیرنده", "مشتمل"));
        levels.put("166", new Level(166, 6, true, true, true, "گفتار", "گویش"));
        levels.put("167", new Level(167, 6, true, true, true, "دستور یا هر مطلب دیگری که از طرف وزارتخانه یا اداره‌ای در چندین نسخه نوشته شده و برای شعبه‌ها، ادارات، یا کارمندان فرستاده می‌شود", "بخشنامه"));
        levels.put("168", new Level(168, 6, true, true, true, "قنات", "کاریز"));
        levels.put("169", new Level(169, 6, true, true, true, "با نظم‌وترتیب", "منظم"));
        levels.put("170", new Level(170, 6, true, true, true, "گوش دهنده", "مستمع"));
        levels.put("171", new Level(171, 6, true, true, true, "ویژگی آنچه دارای سنجیدگی و استواری است", "محکم"));
        levels.put("172", new Level(172, 6, true, true, true, "سختی و رنج", "مصیبت"));
        levels.put("173", new Level(173, 6, true, true, true, "وقار", "متانت"));
        levels.put("174", new Level(174, 6, true, true, true, "قفل چوبی که پست در نصب می کنند و با آن در را باز و بسته می کنند", "کلون"));
        levels.put("175", new Level(175, 6, true, true, true, "آنچه انسان به آن عقیده دارد", "معتقد"));
        levels.put("176", new Level(176, 6, true, true, true, "گردیدن جسمی بر روی جسم دیگر به‌پهنا یا به‌پهلو‌", "غلتیدن"));
        levels.put("177", new Level(177, 6, true, true, true, "عمل تحریک ماهیچه‌های بدن به‌وسیلۀ انگشت یا وسیلۀ دیگر به‌طوری‌که موجب خنده شود", "قلقلک"));
        levels.put("178", new Level(178, 6, true, true, true, "سفارش شده و اندرز دهنده", "وصی"));
        levels.put("179", new Level(179, 6, true, true, true, "روانی و شیوایی", "فصاحت"));
        levels.put("180", new Level(180, 6, true, true, true, "فایده و سود بردن از چیزی", "غنیمت شمردن"));
        levels.put("181", new Level(181, 7, true, true, true, "توانایی ذهنی و فطری برای انجام دادن یا فراگرفتن کاری", "استعداد"));
        levels.put("182", new Level(182, 7, true, true, true, "ابزار تیر اندازی قدیمی", "تیر و کمان"));
        levels.put("183", new Level(183, 7, true, true, true, "تلاش برای یافتن و به‌دست آوردن چیزی", "جست و جو"));
        levels.put("184", new Level(184, 7, true, true, true, "آبگیر مصنوعی", "استخر"));
        levels.put("185", new Level(185, 7, true, true, true, "پشیمانی", "ندامت"));
        levels.put("186", new Level(186, 7, true, true, true, "شکلی از زبان  گفتار که با نشانه هایی از زبان محلی تکلم شود", "لهجه"));
        levels.put("187", new Level(187, 7, true, true, true, "به وظیفه و شغل امدادرسان گفته می شود.", "امداد رسانی"));
        levels.put("188", new Level(188, 7, true, true, true, "ابتدایی و اولیه", "مقدماتی"));
        levels.put("189", new Level(189, 7, true, true, true, "استفاده کردن", "استعمال"));
        levels.put("190", new Level(190, 7, true, true, true, "نهج البلاغه کتابی شامل خطبه ها، سخنان و نامه های حضرت علی (ع) می باشد که توسط سید رضی در قرن چهارم هجری قمری جمع آوری شده است", "نهج البلاغه"));
        levels.put("191", new Level(191, 7, true, true, true, "لوله ی چاه یا تونلی که آب های کثیف را زا محل سکونت دور می کند", "فاضلاب"));
        levels.put("192", new Level(192, 7, true, true, true, "طلب آمرزش و مغفرت برای مرده", "ترحیم"));
        levels.put("193", new Level(193, 7, true, true, true, "رای عالم دینی در حکم شرعی", "فتوا"));
        levels.put("194", new Level(194, 7, true, true, true, "", "نیم کلمه"));
        levels.put("195", new Level(195, 7, true, true, true, "فراودۀ لبنی که از مایه زدن شیر گرم و بستن آن در اثر گرم ماندن تولید می‌شود", "ماست"));
        levels.put("196", new Level(196, 7, true, true, true, "گرانبها", "ثمین"));
        levels.put("197", new Level(197, 7, true, true, true, "آذین، آرایش، زیب", "تزیین"));
        levels.put("198", new Level(198, 7, true, true, true, "ترحم، دلجویی، دلسوزی", "شفقت"));
        levels.put("199", new Level(199, 7, true, true, true, "سندی که در آن حدود، شرایط و اجاره بها نوشته می شود.", "اجاره نامه"));
        levels.put("200", new Level(200, 7, true, true, true, "یک مصوت یا رشته‌ای از چند صامت و مصوت که در آخرین کلمۀ ابیات یک قطعه شعر یا در آخرین کلمۀ مصراع‌های یک بیت تکرار شود", "قافیه"));
        levels.put("201", new Level(201, 7, true, true, true, "چین‌خوردگی‌های طاق‌مانند زمین", "طاقدیس"));
        levels.put("202", new Level(202, 7, true, true, true, "آب زیادی که در طبیعت از جایی بلند به پایین می ریزد", "آبشار"));
        levels.put("203", new Level(203, 7, true, true, true, "ستونی مرکب از 33 قطعه استخوان به نام مهره یا فقره که به طور عمودی بر روی هم در پشت انسان قرار دارند", "ستون فقرات"));
        levels.put("204", new Level(204, 7, true, true, true, "هر چیزی که دور از انتظار و پیش بینی باشد", "غیرمنتظره"));
        levels.put("205", new Level(205, 7, true, true, true, "شغل و عمل باغبان", "باغبانی"));
        levels.put("206", new Level(206, 7, true, true, true, "پایانیافته، بهآخررسیده", "سپری"));
        levels.put("207", new Level(207, 7, true, true, true, "تاریکی؛ سیاهی", "تیرگی"));
        levels.put("208", new Level(208, 7, true, true, true, "شتربان", "ساربان"));
        levels.put("209", new Level(209, 7, true, true, true, "محل قرار  گرفتن چاقو و شمشیر", "غلاف"));
        levels.put("210", new Level(210, 7, true, true, true, "قسمتی از جهان که ساختۀ دست بشر نیست", "طبیعت"));
        levels.put("211", new Level(211, 8, true, true, true, "مجموعه از چند جزیره که در کنار یکدیگر قرار دارند", "مجمع الجزایر"));
        levels.put("212", new Level(212, 8, true, true, true, "بدنامی و بی آبرویی", "افتضاح"));
        levels.put("213", new Level(213, 8, true, true, true, "زیاده‌روی کردن در توصیف کسی یا چیزی", "اغراق"));
        levels.put("214", new Level(214, 8, true, true, true, "کاروکوشش بسیار", "فعالیت"));
        levels.put("215", new Level(215, 8, true, true, true, "دیوانه", "مجنون"));
        levels.put("216", new Level(216, 8, true, true, true, "تراکم و قیض", "فشردگی"));
        levels.put("217", new Level(217, 8, true, true, true, "گروهی از مردم که با هم وجه اشتراک داشته باشند و در سرزمینی واحد زندگی کنند.", "جامعه"));
        levels.put("218", new Level(218, 8, true, true, true, "استقلال، استقلال داخلی، خودگردانی", "خودمختاری"));
        levels.put("219", new Level(219, 8, true, true, true, "پزشک", "حکیم"));
        levels.put("220", new Level(220, 8, true, true, true, "نشناختن وجه صواب در امری", "غلط"));
        levels.put("221", new Level(221, 8, true, true, true, "خوراک بادمجان و گوجه فرنگی و گوشت و سیب زمینی و ادویه", "تاس کباب"));
        levels.put("222", new Level(222, 8, true, true, true, "هنر کندن نقش و نگار روی فلز", "قلم زنی"));
        levels.put("223", new Level(223, 8, true, true, true, "اندازه یا وسیله ای که با آن چیزی را بسنجند", "معیار"));
        levels.put("224", new Level(224, 8, true, true, true, "حملۀ ناگهانی؛ یورش", "هجوم"));
        levels.put("225", new Level(225, 8, true, true, true, "کسی که تحت فرمان حاکم ستمگر کار می کند", "مزدور"));
        levels.put("226", new Level(226, 8, true, true, true, "لبخند زدن.", "ابتسام"));
        levels.put("227", new Level(227, 8, true, true, true, "سخت گیری کردن", "مضیقه"));
        levels.put("228", new Level(228, 8, true, true, true, "فعلی که بر زمان گذشته دلالت می‌کند", "ماضی"));
        levels.put("229", new Level(229, 8, true, true, true, "بن، بیخ، پایه", "بنیاد"));
        levels.put("230", new Level(230, 8, true, true, true, "نسبت دادن گمان و کار بد و ناروا به کسی", "تهمت"));
        levels.put("231", new Level(231, 8, true, true, true, "تنبلی", "کاهلی"));
        levels.put("232", new Level(232, 8, true, true, true, "آزردگی", "بیزاری"));
        levels.put("233", new Level(233, 8, true, true, true, "علم به حقایق موجودات به اندازه توانایی بشر", "فلسفه"));
        levels.put("234", new Level(234, 8, true, true, true, "کدوی بزرگ زرد یا نارنجی رنگ تخمه دار که نیمی از تنۀ آن قطورتر از نیم دیگر است", "کدو حلوایی"));
        levels.put("235", new Level(235, 8, true, true, true, "رجوع به شکوه شود", "شکوهمندی"));
        levels.put("236", new Level(236, 8, true, true, true, "قومی زرد‌پوست ساکن آسیای مرکزی", "مغول"));
        levels.put("237", new Level(237, 8, true, true, true, "مٲمور نیروی انتظامی و شهربانی که وظیفه‌اش حفظ نظم و آرامش شهر است.", "پاسبان"));
        levels.put("238", new Level(238, 8, true, true, true, "ثروتمندان", "گنجوران"));
        levels.put("239", new Level(239, 8, true, true, true, "آن‌که یا آنچه به چیزی پاک یا ناپاک آغشته شده", "آلوده"));
        levels.put("240", new Level(240, 8, true, true, true, "آفت رسیده", "آفت زده"));
        levels.put("241", new Level(241, 9, true, true, true, "به لرزه درآمدن", "ارتعاش"));
        levels.put("242", new Level(242, 9, true, true, true, "تربیت", "پرورش"));
        levels.put("243", new Level(243, 9, true, true, true, "زیبا و مرتّب شده", "آراسته"));
        levels.put("244", new Level(244, 9, true, true, true, "سهش ها", "احساسات"));
        levels.put("245", new Level(245, 9, true, true, true, "فرمان‌بردار", "مطیع"));
        levels.put("246", new Level(246, 9, true, true, true, "چراغی از جنس شیشه، چینی، یا فلز که دارای چندین حباب و تزئینات است و از سقف آویزان می‌شود.", "لوستر"));
        levels.put("247", new Level(247, 9, true, true, true, "آلودگی", "آلایش"));
        levels.put("248", new Level(248, 9, true, true, true, "به‌ یکدیگر نامه نوشتن", "مکاتبه"));
        levels.put("249", new Level(249, 9, true, true, true, "عاشق؛ دلباخته", "شیفته"));
        levels.put("250", new Level(250, 9, true, true, true, "نیکوکار", "صالح"));
        levels.put("251", new Level(251, 9, true, true, true, "پدید آورنده نقش", "صورتگر"));
        levels.put("252", new Level(252, 9, true, true, true, "کسی که نظریه‌ای فلسفی را ارائه دهد یا دانش کامل دربارۀ فلسفه داشته باشد", "فیلسوف"));
        levels.put("253", new Level(253, 9, true, true, true, "توضیح خواستن گروهی از نمایندگان مجلس از یکی از وزیران یا رئیس دولت.", "استیضاح"));
        levels.put("254", new Level(254, 9, true, true, true, "آن سو", "ماورا"));
        levels.put("255", new Level(255, 9, true, true, true, "فرزندان آدم", "بنی آدم"));
        levels.put("256", new Level(256, 9, true, true, true, "نام یکی از پادشاهان ایران", "طهماسب"));
        levels.put("257", new Level(257, 9, true, true, true, "کاری به زور بر عهدۀ کسی گذاشتن", "تحمیل"));
        levels.put("258", new Level(258, 9, true, true, true, "سؤال کردن از دیگری به منظور سنجش اطلاعات او", "امتحان"));
        levels.put("259", new Level(259, 9, true, true, true, "بذل‌ مال", "بخشش"));
        levels.put("260", new Level(260, 9, true, true, true, "بروز دادن", "اقرار"));
        levels.put("261", new Level(261, 9, true, true, true, "جار و جنجال ، داد و فریاد", "قشقرق"));
        levels.put("262", new Level(262, 9, true, true, true, "پاک و پاکیزه", "مقدس"));
        levels.put("263", new Level(263, 9, true, true, true, "وقت مناسب برای کاری", "فرصت"));
        levels.put("264", new Level(264, 9, true, true, true, "اسف، اضطراب، بیآرامی، بیقراری", "اندوه"));
        levels.put("265", new Level(265, 9, true, true, true, "واحد اندازه‌گیری طول در کشورهای انگلیسی‌زبان برابر با ۱۶۰۹ متر", "مایل"));
        levels.put("266", new Level(266, 9, true, true, true, "بالاآمده؛ برجسته", "ورقلمبیده"));
        levels.put("267", new Level(267, 9, true, true, true, "کسی که کفش تعمیر می کند", "پاره دوز"));
        levels.put("268", new Level(268, 9, true, true, true, "کسی که جان را فدا کند", "جانفشان"));
        levels.put("269", new Level(269, 9, true, true, true, "هر یک از دو تودة خاکستری که در طرفین بطن سوم مغز قرار دارد و بخشی از دیوارة جانبی حفرة بطن سوم را تشکیل می دهد", "تالاموس"));
        levels.put("270", new Level(270, 9, true, true, true, "شکست خورده", "بازنده"));
        levels.put("271", new Level(271, 10, true, true, true, "بیرون کشیدن و برگزیدن چیزی یا کسی از میان یک مجموعه.", "انتخاب"));
        levels.put("272", new Level(272, 10, true, true, true, "پرنده‌ای زیبا از خانوادۀ کبک که نر آن دم چتری بزرگ با پرهای رنگین دارد.", "طاووس"));
        levels.put("273", new Level(273, 10, true, true, true, "یکی شدن", "اتحاد"));
        levels.put("274", new Level(274, 10, true, true, true, "باز کردن یا شکافتن بدن کسی که از دنیا رفته است به منظور شناختن و بررسی اندام های داخلی", "کالبد شکافی"));
        levels.put("275", new Level(275, 10, true, true, true, "پریشان حالی و بی تابی", "اضطراب"));
        levels.put("276", new Level(276, 10, true, true, true, "علمی که دربارۀ مقادیر و اعداد بحث می‌کند و شامل حساب، جبر، مقابله، و هندسه است", "ریاضی"));
        levels.put("277", new Level(277, 10, true, true, true, "همهمه", "غوغا"));
        levels.put("278", new Level(278, 10, true, true, true, "هر رویدادی که با فاجعه همراه باشد", "فاجعه آمیز"));
        levels.put("279", new Level(279, 10, true, true, true, "دریای کوچک که از هر طرف خاک بر آن احاطه کرده باشد.", "دریاچه"));
        levels.put("280", new Level(280, 10, true, true, true, "سنگی بی‌رنگ، گران‌بها، تشکیل‌شده از کربن خالص", "الماس"));
        levels.put("281", new Level(281, 10, true, true, true, "زبانی از خانواده ی زبان‌های هندواروپایی که در بلژیک هم رایج است", "فرانسوی"));
        levels.put("282", new Level(282, 10, true, true, true, "کامل ‌کردن", "تکمیل"));
        levels.put("283", new Level(283, 10, true, true, true, "سراسیمگی، شتابزدگی، عجله", "دستپاچگی"));
        levels.put("284", new Level(284, 10, true, true, true, "بزرگ داشتن", "گرامی داشتن"));
        levels.put("285", new Level(285, 10, true, true, true, "نام یکی از قهرمانان شاهنامه", "اسفندیار"));
        levels.put("286", new Level(286, 10, true, true, true, "قصد؛ نیت", "مقصود"));
        levels.put("287", new Level(287, 10, true, true, true, "فریاد شادی", "هلهله"));
        levels.put("288", new Level(288, 10, true, true, true, "علم بررسی عوارض کرۀ زمین", "جغرافیا"));
        levels.put("289", new Level(289, 10, true, true, true, "فاقد ازدحام و شلوغی", "خلوت"));
        levels.put("290", new Level(290, 10, true, true, true, "آشفتگی", "بحران"));

    }

}
