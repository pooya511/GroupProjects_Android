package com.taxiapps.wordcut.model;

import com.taxiapps.wordcut.model.json.LevelJson;
import com.taxiapps.wordcut.model.json.SeasonJson;

/**
 * Created by Parsa on 2018-03-13.
 */

public class Season {

    private int id;
    private String title;
    private int levelReward;
    private int seasonReward;
    private boolean isEnabled;
    private String disabledText;
    private int disabledImage;

    public Season(int id, String title, int levelReward, int seasonReward, String disabledText) {
        this.id = id;
        this.title = title;
        this.levelReward = levelReward;
        this.seasonReward = seasonReward;
        this.disabledText = disabledText;
    }

    public Season(int id, String title, int levelReward, int seasonReward) {
        this.id = id;
        this.title = title;
        this.levelReward = levelReward;
        this.seasonReward = seasonReward;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLevelReward() {
        return levelReward;
    }

    public void setLevelReward(int levelReward) {
        this.levelReward = levelReward;
    }

    public int getSeasonReward() {
        return seasonReward;
    }

    public void setSeasonReward(int seasonReward) {
        this.seasonReward = seasonReward;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public String getDisabledText() {
        return disabledText;
    }

    public void setDisabledText(String disabledText) {
        this.disabledText = disabledText;
    }

    public int getDisabledImage() {
        return disabledImage;
    }

    public void setDisabledImage(int disabledImage) {
        this.disabledImage = disabledImage;
    }

    public static boolean hasNextSeason(int seasonId, GameConfig gameConfig) {
        return gameConfig.hasEvent() ? gameConfig.getSeasons().containsKey(String.valueOf(seasonId)) : SeasonJson.seasons.containsKey(String.valueOf(seasonId));
    }

    public static boolean seasonHasLevel(int seasonId, GameConfig gameConfig) {

        if (gameConfig.hasEvent()) {
            for (int i = 1; i <= gameConfig.getLevels().size(); i++) {
                if (gameConfig.getLevels().get(String.valueOf(i)).getSeasonId() == seasonId)
                    return true;
            }
        } else {
            for (int i = 1; i <= LevelJson.levels.size(); i++) {
                if (LevelJson.levels.get(String.valueOf(i)).getSeasonId() == seasonId)
                    return true;
            }
        }

        return false;
    }

}