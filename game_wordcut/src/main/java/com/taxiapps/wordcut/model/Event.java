package com.taxiapps.wordcut.model;

import android.support.annotation.IntegerRes;

import java.util.Date;
import java.util.HashMap;

import modules.PublicModules;

public class Event {

    private String name;
    private String enrolDescription;
    private int enrolCost;
    private long eventStart;
    private long eventEnd;
    private String imageUrlMain;
    private String imageUrlSeasonsTop;
    private boolean registerRequireAllLevels;
    private String seasonsBackgroundImageUrl;
    private String seasonsButtonLabel;
    private String sponsorLink;
    private String sponsorLinkMode;
    private String sponsorPrizeImageUrl;
    private String sponsoredBy;
    private String title;
    private boolean active;
    private String imageUrlEventHeader;
    private String sponsorLogo;
    private String levelsUrl_ios;
    private String levelsUrl_android;
    private boolean shuffleLevels;

    /*
    baray sakhte shodan e in class 1 dictionary vared e an mishavad
    va field hay an az dakhel e dictionary load mishavad
    agar field i dar dictionary nabud meghdar e default rikhte mishavad
     */

    public Event(){

    }

    public Event(String name, HashMap<String, Object> data) {

        this.name = name;

        enrolDescription = String.valueOf(data.get("enrol_description")).equals("null") ? "" : String.valueOf(data.get("enrol_description"));
        enrolCost = String.valueOf(data.get("enrol_cost")).equals("null") ? 0 : Integer.valueOf(String.valueOf(data.get("enrol_cost")));
        eventStart = String.valueOf(data.get("event_start")).equals("null") ? 0 : new Date(String.valueOf(data.get("event_start"))).getTime();
        eventEnd = String.valueOf(data.get("event_end")).equals("null") ? 0 : new Date(String.valueOf(data.get("event_end"))).getTime();

        imageUrlMain = String.valueOf(data.get("imageurl_main")).equals("null") ? "" : String.valueOf(data.get("imageurl_main"));
        imageUrlSeasonsTop = String.valueOf(data.get("imageurl_seasons_top")).equals("null") ? "" : String.valueOf(data.get("imageurl_seasons_top"));

        registerRequireAllLevels = String.valueOf(data.get("register_require_all_levels")).equals("null") ? false : Boolean.valueOf(String.valueOf(data.get("register_require_all_levels")));

        seasonsBackgroundImageUrl = String.valueOf(data.get("seasons_background_image_url")).equals("null") ? "" : String.valueOf(data.get("seasons_background_image_url"));
        seasonsButtonLabel = String.valueOf(data.get("seasons_button_label")).equals("null") ? "" : String.valueOf(data.get("seasons_button_label"));
        sponsorLink = String.valueOf(data.get("sponsore_link")).equals("null") ? "" : String.valueOf(data.get("sponsore_link"));
        sponsorLinkMode = String.valueOf(data.get("sponsore_link_mode")).equals("null") ? "" : String.valueOf(data.get("sponsore_link_mode"));
        sponsorPrizeImageUrl = String.valueOf(data.get("sponsore_prize_image_url")).equals("null") ? "" : String.valueOf(data.get("sponsore_prize_image_url"));
        sponsoredBy = String.valueOf(data.get("sponsoredby")).equals("null") ? "" : String.valueOf(data.get("sponsoredby"));
        title = String.valueOf(data.get("title")).equals("null") ? "" : String.valueOf(data.get("title"));

        active = String.valueOf(data.get("active")).equals("null") ? false : Boolean.valueOf(String.valueOf(data.get("active")));

        imageUrlEventHeader = String.valueOf(data.get("imageurl_event_header")).equals("null") ? "" : String.valueOf(data.get("imageurl_event_header"));
        sponsorLogo = String.valueOf(data.get("sponsor_logo")).equals("null") ? "" : String.valueOf(data.get("sponsor_logo"));

        levelsUrl_ios = String.valueOf(data.get("levelsurl_ios")).equals("null") ? "" : String.valueOf(data.get("levelsurl_ios"));
        levelsUrl_android = String.valueOf(data.get("levelsurl_android")).equals("null") ? "" : String.valueOf(data.get("levelsurl_android"));

        shuffleLevels = String.valueOf(data.get("shuffle_levels")).equals("null") ? false : Boolean.valueOf(String.valueOf(data.get("shuffle_levels")));

        //instance = this;
    }

    /*private static Event instance;

    public static Event getInstance() {
        return instance;
    }*/

    /*public static Event newInstance(String name, HashMap<String, Object> data) {
        return new Event(name, data);
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnrolDescription() {
        return enrolDescription;
    }

    public void setEnrolDescription(String enrolDescription) {
        this.enrolDescription = enrolDescription;
    }

    public int getEnrolCost() {
        return enrolCost;
    }

    public void setEnrolCost(int enrolCost) {
        this.enrolCost = enrolCost;
    }

    public long getEventStart() {
        return eventStart;
    }

    public void setEventStart(long eventStart) {
        this.eventStart = eventStart;
    }

    public long getEventEnd() {
        return eventEnd;
    }

    public void setEventEnd(long eventEnd) {
        this.eventEnd = eventEnd;
    }

    public String getImageUrlMain() {
        return imageUrlMain;
    }

    public void setImageUrlMain(String imageUrlMain) {
        this.imageUrlMain = imageUrlMain;
    }

    public String getImageUrlSeasonsTop() {
        return imageUrlSeasonsTop;
    }

    public void setImageUrlSeasonsTop(String imageUrlSeasonsTop) {
        this.imageUrlSeasonsTop = imageUrlSeasonsTop;
    }

    public boolean isRegisterRequireAllLevels() {
        return registerRequireAllLevels;
    }

    public void setRegisterRequireAllLevels(boolean registerRequireAllLevels) {
        this.registerRequireAllLevels = registerRequireAllLevels;
    }

    public String getSeasonsBackgroundImageUrl() {
        return seasonsBackgroundImageUrl;
    }

    public void setSeasonsBackgroundImageUrl(String seasonsBackgroundImageUrl) {
        this.seasonsBackgroundImageUrl = seasonsBackgroundImageUrl;
    }

    public String getSeasonsButtonLabel() {
        return seasonsButtonLabel;
    }

    public void setSeasonsButtonLabel(String seasonsButtonLabel) {
        this.seasonsButtonLabel = seasonsButtonLabel;
    }

    public String getSponsorLink() {
        return sponsorLink;
    }

    public void setSponsorLink(String sponsorLink) {
        this.sponsorLink = sponsorLink;
    }

    public String getSponsorLinkMode() {
        return sponsorLinkMode;
    }

    public void setSponsorLinkMode(String sponsorLinkMode) {
        this.sponsorLinkMode = sponsorLinkMode;
    }

    public String getSponsorPrizeImageUrl() {
        return sponsorPrizeImageUrl;
    }

    public void setSponsorPrizeImageUrl(String sponsorPrizeImageUrl) {
        this.sponsorPrizeImageUrl = sponsorPrizeImageUrl;
    }

    public String getSponsoredBy() {
        return sponsoredBy;
    }

    public void setSponsoredBy(String sponsoredBy) {
        this.sponsoredBy = sponsoredBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getImageUrlEventHeader() {
        return imageUrlEventHeader;
    }

    public void setImageUrlEventHeader(String imageUrlEventHeader) {
        this.imageUrlEventHeader = imageUrlEventHeader;
    }

    public String getSponsorLogo() {
        return sponsorLogo;
    }

    public void setSponsorLogo(String sponsorLogo) {
        this.sponsorLogo = sponsorLogo;
    }

    public String getLevelsUrl_ios() {
        return levelsUrl_ios;
    }

    public void setLevelsUrl_ios(String levelsUrl_ios) {
        this.levelsUrl_ios = levelsUrl_ios;
    }

    public String getLevelsUrl_android() {
        return levelsUrl_android;
    }

    public void setLevelsUrl_android(String levelsUrl_android) {
        this.levelsUrl_android = levelsUrl_android;
    }

    public boolean isShuffleLevels() {
        return shuffleLevels;
    }

    public void setShuffleLevels(boolean shuffleLevels) {
        this.shuffleLevels = shuffleLevels;
    }

    /*public static void setInstance(Event instance) {
        Event.instance = instance;
    }*/
}