package com.taxiapps.wordcut.model;

import java.util.HashMap;

public class GameConfig {

    private Event event;
    private HashMap<String, Level> levels;
    private HashMap<String, Season> seasons;

    public GameConfig(){

    }

    public GameConfig(Event event, HashMap<String, Level> levels, HashMap<String, Season> seasons) {
        this.event = event;
        this.levels = levels;
        this.seasons = seasons;
    }

    public boolean hasEvent() {
        return event != null;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public HashMap<String, Level> getLevels() {
        return levels;
    }

    public void setLevels(HashMap<String, Level> levels) {
        this.levels = levels;
    }

    public HashMap<String, Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(HashMap<String, Season> seasons) {
        this.seasons = seasons;
    }
}
