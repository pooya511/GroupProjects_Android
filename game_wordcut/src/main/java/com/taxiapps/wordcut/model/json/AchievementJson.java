package com.taxiapps.wordcut.model.json;

import com.taxiapps.wordcut.model.Achievement;

import java.util.LinkedHashMap;

/**
 * Created by Parsa on 2018-04-03.
 */

public class AchievementJson {

//    public static String levels_1 = "levels_1";
//    public static String levels_5 = "levels_5";
//    public static String levels_20 = "levels_20";
//    public static String levels_50 = "levels_50";
//    public static String levels_100 = "levels_100";
//    public static String levels_200 = "levels_200";
//    //    public static String levels_300 = "levels_300";
//    public static String record_5s = "record_5s";
//    public static String record_10s = "record_10s";
//    public static String record_30s = "record_30s";
//    public static String share_1 = "share_1";
//    public static String share_10 = "share_10";
//    public static String eye_1 = "eye_1";
//    public static String eye_3 = "eye_3";
//    public static String eye_10 = "eye_10";
//    public static String dic_1 = "dic_1";
//    public static String dic_3 = "dic_3";
//    public static String dic_10 = "dic_10";
//    public static String skip1 = "skip_1";
//    public static String skip3 = "skip_3";
//    public static String skip10 = "skip_10";
//    public static String listenMusic = "listenMusic";
//    public static String playtime_1h = "playtime_1h";
//    public static String playtime_3h = "playtime_3h";
//    public static String playtime_10h = "playtime_10h";
//    //    public static String playtime_20h = "playtime_20h";
////    public static String playtime_1d = "playtime_1d";
//    public static String oneshot_1 = "oneshot_1";
//    public static String oneshot_10 = "oneshot_10";
//    public static String oneshot_20 = "oneshot_20";
//    public static String oneshot_50 = "oneshot_50";
//    public static String buyCoin = "buyCoin";
//    public static String masterWord = "masterWord";
//    public static String tapLogo = "tapLogo";


    public enum Achievements {

        // gamePlay
        levels_1("levels_1"),
        levels_5("levels_5"),
        levels_20("levels_20"),
        levels_50("levels_50"),
        levels_100("levels_100"),
        levels_200("levels_200"),
        //        levels_300 ( "levels_300"),
        record_5s("record_5s"),
        record_10s("record_10s"),
        record_30s("record_30s"),
        eye_1("eye_1"),
        eye_3("eye_3"),
        eye_10("eye_10"),
        dic_1("dic_1"),
        dic_3("dic_3"),
        dic_10("dic_10"),
        skip1("skip_1"),
        skip3("skip_3"),
        skip10("skip_10"),
        playtime_1h("playtime_1h"),
        playtime_3h("playtime_3h"),
        playtime_10h("playtime_10h"),
//        playtime_20h ( "playtime_20h"),
//        playtime_1d("playtime_1d"),
        oneshot_1("oneshot_1"),
        oneshot_10("oneshot_10"),
        oneshot_20("oneshot_20"),
        oneshot_50("oneshot_50"),
        masterWord("masterword"),

        // app
        share_1("share_1"),
        share_10("share_10"),
        listenMusic("listenmusic"),
        tapLogo("taplogo"),

        // both of them
        buyCoin("buycoin");


        public final String value;

        private Achievements(String s) {
            value = s;
        }

        public String toString() {
            return this.value;
        }
    }

    public static LinkedHashMap<String, Achievement> achievements = new LinkedHashMap<>();

    public static void initAchievements() {

        achievements.put(Achievements.levels_1.value, new Achievement(Achievements.levels_1.value, "Levels", "Complete Level 1", "اتمام مرحله ۱", 5, true));
        achievements.put(Achievements.levels_5.value, new Achievement(Achievements.levels_5.value, "Levels", "Complete Level 5", "اتمام مرحله ۵", 5, true));
        achievements.put(Achievements.levels_20.value, new Achievement(Achievements.levels_20.value, "Levels", "Complete Level 20", "اتمام مرحله ۲۰", 5, true));
        achievements.put(Achievements.levels_50.value, new Achievement(Achievements.levels_50.value, "Levels", "Complete Level 50", "اتمام مرحله ۵۰", 15, true));
        achievements.put(Achievements.levels_100.value, new Achievement(Achievements.levels_100.value, "Levels", "Complete Level 100", "اتمام مرحله ۱۰۰", 20, true));
        achievements.put(Achievements.levels_200.value, new Achievement(Achievements.levels_200.value, "Levels", "Complete Level 200", "اتمام مرحله ۲۰۰", 25, true));
//        achievements.put(levels_300,new Achievement(levels_300,"Levels","Complete level 300","اتمام مرحله ۳۰۰",10,true));

        achievements.put(Achievements.record_5s.value, new Achievement(Achievements.record_5s.value, "Fast Complete", "Complete a level in 5 seconds! (Do not use Skip)", "اتمام یک مرحله در حداکثر ۵ ثانیه", 5, true));
        achievements.put(Achievements.record_10s.value, new Achievement(Achievements.record_10s.value, "Fast Complete", "Complete a level in 10 seconds! (Do not use Skip)", "اتمام یک مرحله در حداکثر ۱۰ ثانیه", 5, true));
        achievements.put(Achievements.record_30s.value, new Achievement(Achievements.record_30s.value, "Fast Complete", "Complete a level in 30 seconds! (Do not use Skip)", "اتمام یک مرحله در حداکثر ۳۰ ثانیه", 5, true));

        achievements.put(Achievements.share_1.value, new Achievement(Achievements.share_1.value, "Share", "Share your app 1 time", "بازی و یا امتیاز خود را به اشتراک بگذارید", 5, true));
        achievements.put(Achievements.share_10.value, new Achievement(Achievements.share_10.value, "Share", "Share your app 10 time", "بازی و یا امتیاز خود را ۱۰ بار به اشتراک بگذارید", 20, true));

        achievements.put(Achievements.eye_1.value, new Achievement(Achievements.eye_1.value, "Eye", "Use Eye skill", "از تسهیل گر چشم استفاده کنید", 5, true));
        achievements.put(Achievements.eye_3.value, new Achievement(Achievements.eye_3.value, "Eye", "Use Eye skill 3 times", "از تسهیل گر چشم ۳ بار استفاده کنید", 5, true));
        achievements.put(Achievements.eye_10.value, new Achievement(Achievements.eye_10.value, "Eye", "Use Eye skill 10 times", "از تسهیل گر چشم ۱۰ بار استفاده کنید", 5, true));

        achievements.put(Achievements.dic_1.value, new Achievement(Achievements.dic_1.value, "Dictionary", "Use Dictionary skill", "از تسهیل گر دیکشنری استفاده کنید", 5, true));
        achievements.put(Achievements.dic_3.value, new Achievement(Achievements.dic_3.value, "Dictionary", "Use Dictionary skill 3 times", "از تسهیل گر دیکشنری ۳ بار استفاده کنید", 5, true));
        achievements.put(Achievements.dic_10.value, new Achievement(Achievements.dic_10.value, "Dictionary", "Use Dictionary skill 10 times", "از تسهیل گر دیکشنری ۱۰ بار استفاده کنید", 5, true));

        achievements.put(Achievements.skip1.value, new Achievement(Achievements.skip1.value, "Skip", "Use Skip skill", "از تسهیل گر ردکردن استفاده کنید", 5, true));
        achievements.put(Achievements.skip3.value, new Achievement(Achievements.skip3.value, "Skip", "Use Skip skill 3 times", "از تسهیل گر ردکردن ۳ بار استفاده کنید", 5, true));
        achievements.put(Achievements.skip10.value, new Achievement(Achievements.skip10.value, "Skip", "Use Skip skill 10 times", "از تسهیل گر ردکردن ۱۰ بار استفاده کنید", 5, true));

        achievements.put(Achievements.listenMusic.value, new Achievement(Achievements.listenMusic.value, "Music", "Listen completely to the main music", "به موسیقی اصلی بازی به طور کامل گوش دهید", 5, true));

        achievements.put(Achievements.playtime_1h.value, new Achievement(Achievements.playtime_1h.value, "Play Time", "Play Wordcut for 1 Hour", "در مجموع ۱ ساعت نیم کلمه را بازی کنید", 40, true));
        achievements.put(Achievements.playtime_3h.value, new Achievement(Achievements.playtime_3h.value, "Play Time", "Play Wordcut for 3 Hour", "در مجموع ۳ ساعت نیم کلمه را بازی کنید", 100, true));
        achievements.put(Achievements.playtime_10h.value, new Achievement(Achievements.playtime_10h.value, "Play Time", "Play Wordcut for 10 Hour", "در مجموع ۱۰ ساعت نیم کلمه را بازی کنید", 500, true));

        achievements.put(Achievements.oneshot_1.value, new Achievement(Achievements.oneshot_1.value, "One Shot", "Complete a level without any wrong answer", "یک مرحله را بدون خطا حل کنید", 5, true));
        achievements.put(Achievements.oneshot_10.value, new Achievement(Achievements.oneshot_10.value, "One Shot", "Complete 10 level without any wrong answer", "۱۰ مرحله را بدون خطا حل کنید", 10, true));
        achievements.put(Achievements.oneshot_20.value, new Achievement(Achievements.oneshot_20.value, "One Shot", "Complete 20 level without any wrong answer", "۲۰ مرحله را بدون خطا حل کنید", 30, true));
        achievements.put(Achievements.oneshot_50.value, new Achievement(Achievements.oneshot_50.value, "One Shot", "Complete 50 levels without any wrong answer", "۵۰ مرحله را بدون خطا حل کنید", 60, true));

        achievements.put(Achievements.buyCoin.value, new Achievement(Achievements.buyCoin.value, "Buy Coin", "Purchase an item", "یک خرید انجام دهید", 30, true));

        achievements.put(Achievements.masterWord.value, new Achievement(Achievements.masterWord.value, "Master Word", "Complete the level with Master Word", "مرحله ای که جوابش نیم کلمه هست را حل کنید", 40, false));
        achievements.put(Achievements.tapLogo.value, new Achievement(Achievements.tapLogo.value, "Tap Logo", "Tap Wordcut logo for 30 times", "لوگوی بازی در صفحه اصلی را ۳۰ بار تاچ کنید", 10, false));

    }

}
