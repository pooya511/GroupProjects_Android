package com.taxiapps.wordcut.model.listener;

import android.media.MediaPlayer;
import android.view.View;

import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;

public class PublicListener implements View.OnClickListener {

    private MediaPlayer soundEffect;

    @Override
    public void onClick(final View v) {
        soundEffect = MediaPlayer.create(v.getContext(), R.raw.button_action_sound);
        soundEffect.setOnCompletionListener(mp -> {
            soundEffect.release();
            soundEffect = MediaPlayer.create(v.getContext(), R.raw.button_action_sound);
        });
        if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1"))
            soundEffect.start();
    }
}
