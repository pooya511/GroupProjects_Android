package com.taxiapps.wordcut.model;

import android.content.Context;
import android.content.res.Resources;

import com.taxiapps.wordcut.PreferencesHelper;

import java.util.ArrayList;

/**
 * Created by Parsa on 2018-04-03.
 */

public class Achievement {

    private String id;
    private String header;
    private String toolTip;
    private String toolTipFa;
    private int reward;
    private boolean visible;
    private boolean isImageFront;

    private boolean isCollected;

    public Achievement(String id, String header, String toolTip, String toolTipFa, int reward, boolean visible) {
        this.id = id;
        this.header = header;
        this.toolTip = toolTip;
        this.toolTipFa = toolTipFa;
        this.reward = reward;
        this.visible = visible;
        this.isImageFront = true;
    }

    public interface AchievementCollectCallBack{
        void call();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public String getToolTipFa() {
        return toolTipFa;
    }

    public void setToolTipFa(String toolTipFa) {
        this.toolTipFa = toolTipFa;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isCollected() {
        return isCollected;
    }

    public void setCollected(boolean collected) {
        isCollected = collected;
    }


    public boolean isImageFront() {
        return isImageFront;
    }

    public void setImageFront(boolean imageFront) {
        isImageFront = imageFront;
    }

    public static boolean checkAchievementAvailability(String achievementID) {
        if (PreferencesHelper.getString(achievementID).equals("1"))
            return true;
        else
            return false;
    }

    public static void unlockAchievement(String achievementID) {
        PreferencesHelper.setString(achievementID, "1");
    }

    public static boolean achievementIsLocked(String achievementID) {
        return PreferencesHelper.getString(achievementID).equals("0");
    }

    public static int getBackgroundResource(Context context, String achievementID, boolean isEnable) {
        String fileName;
        if (isEnable) {
            fileName = "img_achieve_" + achievementID + "_fa";
        } else {
            fileName = "img_achieve_" + achievementID + "_fa_disabled";
        }
        Resources resources = context.getResources();
        return resources.getIdentifier(fileName, "drawable", context.getPackageName());
    }

    public static ArrayList<String> getUnreadAchievements() {

        ArrayList<String> unreadAchievements = new ArrayList<>();

        ArrayList<String> unlockedAchievements = PreferencesHelper.getStringArrayPref(PreferencesHelper.unlockedAchievement);
        ArrayList<String> collectedAchievements = PreferencesHelper.getStringArrayPref(PreferencesHelper.collectedAchievement);

        for (int i = 0; i < unlockedAchievements.size(); i++) { // tuy achievement hay unlock shode becharkh
            for (int j = 0; j < collectedAchievements.size(); j++) { // tuy collect shode ha ham becharkh
                if (!unlockedAchievements.contains(collectedAchievements.get(j))) {
                    unreadAchievements.add(collectedAchievements.get(j));
                }
            }
        }
        return unreadAchievements;
    }


}
