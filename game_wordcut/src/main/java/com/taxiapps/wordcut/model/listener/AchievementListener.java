package com.taxiapps.wordcut.model.listener;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import com.taxiapps.wordcut.ui.adapter.AchievementsAdapter;

public class AchievementListener implements View.OnClickListener {

    private AchievementsAdapter.AchievementViewHolder viewHolder;
    private boolean isFrontImage;
    private AnimationEnd animationEnd;

    private static ScaleAnimation scaleAnimation1 = new ScaleAnimation(1, 0, 1, 1, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);
    private static ScaleAnimation scaleAnimation2 = new ScaleAnimation(0, 1, 1, 1, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);

    public interface AnimationEnd{
        void call();
    }

    public AchievementListener(AchievementsAdapter.AchievementViewHolder viewHolder, boolean isFrontImage,AnimationEnd animationEnd) {
        this.viewHolder = viewHolder;
        this.isFrontImage = isFrontImage;
        this.animationEnd = animationEnd;

        scaleAnimation1.setDuration(150);
        scaleAnimation2.setDuration(150);

        scaleAnimation1.setFillAfter(true);
        scaleAnimation2.setFillAfter(true);

        viewHolder.achievementImageCointainer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        scaleAnimation(viewHolder, isFrontImage , animationEnd);
    }

    private static void scaleAnimation(final AchievementsAdapter.AchievementViewHolder viewHolder, final boolean isFront,final AnimationEnd animationEnd) {

        if (isFront)
            viewHolder.achievementImage.startAnimation(scaleAnimation1);
        else
            viewHolder.achievementImageBack.startAnimation(scaleAnimation1);

        scaleAnimation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isFront) {
                    viewHolder.achievementImage.setVisibility(View.INVISIBLE);
                    viewHolder.achievementImageBack.setVisibility(View.VISIBLE);
                    viewHolder.achievementImageBack.startAnimation(scaleAnimation2);

                    animationEnd.call();
                } else {
                    viewHolder.achievementImageBack.setVisibility(View.INVISIBLE);
                    viewHolder.achievementImage.setVisibility(View.VISIBLE);
                    viewHolder.achievementImage.startAnimation(scaleAnimation2);

                    animationEnd.call();
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

}
