package com.taxiapps.wordcut.model;

import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;

import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;

import java.util.ArrayList;
import java.util.Random;

public class Coin extends android.support.v7.widget.AppCompatImageView {

    private static ArrayList<Point> circlePoints;

    public interface CoinAddedCallBack {
        void call();
    }

    public Coin(Context context) {
        super(context);
        this.setBackgroundResource(R.mipmap.ic_coin);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(50, 50);
        //aval gone chon ye masiri ro bayad to halate gone animate beshe
        this.setVisibility(GONE);
        this.setLayoutParams(layoutParams);
    }

    public Coin(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setBackgroundResource(R.mipmap.ic_coin);
    }

    public Coin(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setBackgroundResource(R.mipmap.ic_coin);
    }

    public static void addCoin(final Context context, final FrameLayout parentFrame, int coinCount, final Point startPoint, final Point middlePoint, final Point endPoint, final CoinAddedCallBack coinAddedCallBack) {

        final MediaPlayer coinSound = MediaPlayer.create(context, R.raw.collecting);

        //coin ha inja be shared ezafe mishan
        int currentCoin = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.coin));
        currentCoin += coinCount;
        PreferencesHelper.setString(PreferencesHelper.coin, String.valueOf(currentCoin));

        //age tedad coin ha az 100 ta bozorgtar bood hamoon 100ta coin too safhe generate mishe animate dade mishe
        int updateCoin = 0;
        if (coinCount > 100) {
            updateCoin = coinCount / 100;
            coinCount = 100;
        }

        //array az coin haii ke gharare animate begiran misaze
        final ArrayList<Coin> coins = new ArrayList<>();
        for (int i = 0; i < coinCount; i++) {
            Coin coin = new Coin(context);
            coins.add(coin);
        }

        //array az noghte hai too dayere mide
        circlePoints = circlePointArray(startPoint, middlePoint, coins);

        if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")) {
            coinSound.start();
            coinSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        }

        for (int i = 0; i < coins.size(); i++) {
            final int i2 = i;
            //coin ro be safhe add mikone
            parentFrame.addView(coins.get(i));
            //be soorete gone coin ro az gooshe samt chap bala miare vasate safhe
            TranslateAnimation invisibleTranslate = new TranslateAnimation(startPoint.x, middlePoint.x, startPoint.y, middlePoint.y);
            invisibleTranslate.setFillAfter(true);
            final int finalUpdateCoin = updateCoin;
            invisibleTranslate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    //vahgti vasate safhe resid visible mikone va mibare samt noghte hai dayereii ke ghablan too ye arraylist rikhtim
                    coins.get(i2).setVisibility(VISIBLE);
                    animation = new TranslateAnimation(middlePoint.x, circlePoints.get(i2).x, middlePoint.y, circlePoints.get(i2).y);
                    animation.setDuration(500);
                    animation.setFillAfter(true);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            //vaghti be noghte hai too dayere resid ba ye delay random beyn 0 va 500 millisecond mibare samte maghsad
                            final int delay = new Random().nextInt((500) + 1);
                            animation = new TranslateAnimation(circlePoints.get(i2).x, endPoint.x, circlePoints.get(i2).y, endPoint.y);
                            animation.setDuration(500);
                            animation.setStartOffset(delay);
                            animation.setFillAfter(true);
                            animation.setInterpolator(new AccelerateInterpolator());
                            animation.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    //vaghti be maghsad resid coin ha ro az activity pak mikone
                                    parentFrame.removeView(coins.get(i2));

                                    //update textview e coins
                                    if (finalUpdateCoin != 0) {
                                        if (GamePlayActivity.getInstance() != null)
                                            GamePlayActivity.getInstance().updateCoins(true, finalUpdateCoin);
                                        MainActivity.getInstance().updateCoins(true, finalUpdateCoin);
                                    } else {
                                        if (GamePlayActivity.getInstance() != null)
                                            GamePlayActivity.getInstance().updateCoins(true, 1);
                                        MainActivity.getInstance().updateCoins(true, 1);
                                    }


                                    // age nabashe popup e onComplete bad az har coin miad ! bayad check konim akharin coin bashe
                                    if (i2 == coins.size() - 1) {
                                        coinAddedCallBack.call();
                                    }
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }
                            });
                            coins.get(i2).startAnimation(animation);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                    coins.get(i2).startAnimation(animation);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            coins.get(i).startAnimation(invisibleTranslate);
        }

    }

    public static void decreaseCoins(int coinCount) {

        int currentCoin = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.coin));
        currentCoin -= coinCount;
        PreferencesHelper.setString(PreferencesHelper.coin, String.valueOf(currentCoin));

        for (int i = 0; i < coinCount; i++) {
            new android.os.Handler().postDelayed(() -> {
                MainActivity.getInstance().updateCoins(false, -1);
                if (GamePlayActivity.getInstance() != null) {
                    GamePlayActivity.getInstance().updateCoins(false, -1);
                }
            }, 80 * i);
        }

    }

    private static int randomNumber() {
        //random numberi too baze dayere morde nazar mide
        return new Random().nextInt((250 + 250) + 1) - 250;
    }

    private static ArrayList<Point> circlePointArray(Point startPoint, Point middlePoint, ArrayList<Coin> coins) {
        ArrayList<Point> resArray = new ArrayList<>();

        for (int i = 0; i < coins.size(); i++) {

            //x va y yek noghte be sorate random az noghte (0,0) mide
            int x = startPoint.x + randomNumber();
            int y = startPoint.y + randomNumber();

            //check mikone ke age too dayre bood biyad oon noghte roo ba offset ke biyad noghte morde nazar too array add kone
            if ((Math.pow(x, 2) + Math.pow(y, 2)) > Math.pow(100, 2) && (Math.pow(x, 2) + Math.pow(y, 2)) < Math.pow(250, 2)) {
                final Point point = new Point();
                point.set(x + (middlePoint.x), y + (middlePoint.y));
                resArray.add(point);
            } else {
                i--;
            }

        }

        return resArray;
    }

    public static String getCoinNumber() {
        return PreferencesHelper.getString(PreferencesHelper.coin);
    }

}
