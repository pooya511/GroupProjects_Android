package com.taxiapps.wordcut.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.model.json.LevelJson;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Parsa on 2018-03-14.
 */

public class Level implements Serializable {

    private static final long serialVersionUID = 3725708732196223811L;
    private int id;
    private int seasonId;
    private boolean hasSkillEye;
    private boolean hasSkillSkip;
    private boolean hasSkillDic;
    private String skillDic;
    private String answer;
    private int image;
    private boolean isPassed;
    private boolean isUnlocked;
    private String svgImage;

    public Level(int id, int seasonId, boolean hasSkillEye, boolean hasSkillSkip, boolean hasSkillDic, String skillDic, String answer) {
        this.id = id;
        this.seasonId = seasonId;
        this.hasSkillEye = hasSkillEye;
        this.hasSkillSkip = hasSkillSkip;
        this.hasSkillDic = hasSkillDic;
        this.skillDic = skillDic;
        this.answer = answer;

    }

    public Level(int id, int seasonId, String skillDic, String answer, String svgImage) {
        this.id = id;
        this.seasonId = seasonId;
        this.skillDic = skillDic;
        this.answer = answer;
        this.svgImage = svgImage;
        this.hasSkillDic = true;
        this.hasSkillEye = true;
        this.hasSkillSkip = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }

    public boolean isHasSkillEye() {
        return hasSkillEye;
    }

    public void setHasSkillEye(boolean hasSkillEye) {
        this.hasSkillEye = hasSkillEye;
    }

    public boolean isHasSkillSkip() {
        return hasSkillSkip;
    }

    public void setHasSkillSkip(boolean hasSkillSkip) {
        this.hasSkillSkip = hasSkillSkip;
    }

    public boolean isHasSkillDic() {
        return hasSkillDic;
    }

    public void setHasSkillDic(boolean hasSkillDic) {
        this.hasSkillDic = hasSkillDic;
    }

    public String getSkillDic() {
        return skillDic;
    }

    public void setSkillDic(String skillDic) {
        this.skillDic = skillDic;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isPassed() {
        return isPassed;
    }

    public void setPassed(boolean passed) {
        isPassed = passed;
    }

    public boolean isUnlocked() {
        return isUnlocked;
    }

    public void setUnlocked(boolean unlocked) {
        this.isUnlocked = unlocked;
    }

    public String getSvgImage() {
        return svgImage;
    }

    public void setSvgImage(String svgImage) {
        this.svgImage = svgImage;
    }

    public static int getLastLevelOfSeason(int seasonId, GameConfig gameConfig) {

        ArrayList<Integer> levelsID = new ArrayList<>();

        if (gameConfig.hasEvent()) {
            for (int i = 1; i <= gameConfig.getLevels().size(); i++) {
                if (gameConfig.getLevels().get(String.valueOf(i)).getSeasonId() == seasonId)
                    levelsID.add(gameConfig.getLevels().get(String.valueOf(i)).getId());
            }
        } else {
            for (int i = 1; i <= LevelJson.levels.size(); i++) {
                if (LevelJson.levels.get(String.valueOf(i)).getSeasonId() == seasonId)
                    levelsID.add(LevelJson.levels.get(String.valueOf(i)).getId());
            }
        }

        return levelsID.get(levelsID.size() - 1);
    }

    public static int getLastUnlockedLevel(GameConfig gameConfig) {
        ArrayList<String> ids;
        if (gameConfig.hasEvent()) {
            ids = PreferencesHelper.getStringArrayPrefEvent(gameConfig.getEvent().getName(), PreferencesHelper.unLockedLevels);
        } else {
            ids = PreferencesHelper.getStringArrayPref(PreferencesHelper.unLockedLevels);
        }
        return Level.getLevelById(Integer.valueOf(ids.get(ids.size() - 1)), gameConfig).getSeasonId();
    }

    public static Level getLevelById(int id, GameConfig gameConfig) {
        return gameConfig.hasEvent() ? gameConfig.getLevels().get(String.valueOf(id)) : LevelJson.levels.get(String.valueOf(id));
    }

}
