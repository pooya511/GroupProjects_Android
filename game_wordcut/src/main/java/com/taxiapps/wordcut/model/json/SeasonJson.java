package com.taxiapps.wordcut.model.json;

import com.taxiapps.wordcut.model.Season;

import java.util.HashMap;

/**
 * Created by Parsa on 2018-03-13.
 */

public class SeasonJson {

    public static HashMap<String, Season> seasons = new HashMap<>();

    public static void initSeasons() {

        seasons.put("1", new Season(1, "", 3, 15, "باز نشده!"));
        seasons.put("2", new Season(2, "", 3, 15, "باز نشده!"));
        seasons.put("3", new Season(3, "", 3, 15, "باز نشده!"));
        seasons.put("4", new Season(4, "", 3, 15, "باز نشده!"));
        seasons.put("5", new Season(5, "", 3, 15, "باز نشده!"));
        seasons.put("6", new Season(6, "", 3, 15, "باز نشده!"));
        seasons.put("7", new Season(7, "", 3, 15, "باز نشده!"));
        seasons.put("8", new Season(8, "", 3, 15, "باز نشده!"));
        seasons.put("9", new Season(9, "", 3, 15, "باز نشده!"));
        seasons.put("10", new Season(10, "", 3, 15, "باز نشده!"));
        seasons.put("11", new Season(11, "", 3, 15, "باز نشده!"));

    }

}