package com.taxiapps.wordcut.ui.activity;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import com.backendless.Backendless;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;
import com.downloader.PRDownloader;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.event.EventDialogFragment;
import com.taxiapps.wordcut.model.Achievement;
import com.taxiapps.wordcut.model.Coin;
import com.taxiapps.wordcut.model.Event;
import com.taxiapps.wordcut.model.GameConfig;
import com.taxiapps.wordcut.model.json.AchievementJson;
import com.taxiapps.wordcut.model.json.LevelJson;
import com.taxiapps.wordcut.model.json.SeasonJson;
import com.taxiapps.wordcut.model.listener.PublicListener;
import com.taxiapps.wordcut.ui.adapter.SeasonAdapter;
import com.taxiapps.wordcut.ui.button.GameButtons;
import com.taxiapps.wordcut.ui.dialog.Dialogs;
import com.taxiapps.wordcut.ui.fragment.SeasonsFragment;
import com.taxiapps.wordcut.utils.AppModules;
import com.taxiapps.wordcut.utils.payment.PersianStorePayment;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import modules.PublicModules;
import modules.TX_Ad;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends FragmentActivity implements InternetConnectivityListener {

    private boolean weAreInMain = true; // baray handle kardane onBackPressed() ke tu activity e aval hamishe nayad birun

    private ArrayList<Integer> mainMusicBeatSecondsArrRipple = new ArrayList(Arrays.asList(
            52, 135, 218, 274, 301, 385, 468, 551, 608, 635, 718, 801, 885, 942, 968, 1051, 1134, 1218, 1274, 1301, 1384, 1468, 1551,
            1634, 1718, 1801, 1884, 1967, 2005, 2020, 2032, 2051, 2134, 2217, 2301, 2329, 2384, 2414, 2467, 2496, 2551, 2580, 2634, 2664, 2717,
            2746, 2774, 2830, 2857, 2884, 2914, 2940, 2967, 3050, 3134, 3217, 3300, 3384, 3385, 3400, 3414, 3442, 3468, 3524, 3538, 3552,
            3608, 3692, 3720, 3774, 3857, 3886, 3967, 4050, 4133, 4217, 4300, 4383, 4466, 4550, 4633, 4716, 4800, 4883, 4966, 5050, 5133,
            5216, 5299, 5383, 5466, 5549, 5633, 5716, 5799, 5883, 5966, 6049, 6132, 6216, 6299, 6382, 6466, 6549, 6632, 6716, 6799, 6882,
            6965, 7049, 7132, 7215, 7299, 7382, 7465, 7549, 7632, 7715, 7798, 7882, 7965, 8048, 8132, 8215, 8298, 8382, 8465, 8548, 8631,
            8715, 8798, 8881, 8965, 9048, 9131, 9215, 9298, 9381
    ));
    private ArrayList<Integer> mainMusicBeatSecondsArrBtn;

    private ImageView splashImageView, mainLogo, coinImg, eventSeasonTitle, levelListBanner;
    private ConstraintLayout headerLayout, footerLayout, mainListHeaderLayout, mainListFooterLayout;
    private LinearLayout indicatorView, newAchievementNotification;
    private Button settingBtn, achievementBtn, reportBtn, shareBtn, backBtn, coinAddBtn;
    private Button previousPageBtn, nextPageBtn, lotteryBtn;
    private Button start1, start2, start3, start4;
    private ViewPager levelsViewPager;
    private TextView seasonTextView, newAchievementCount, seasonTextViewTop, eventCountDownText;
    public TextView coinCountText;
    public FrameLayout parent;
    private FrameLayout eventContainer;
    public ImageView eventLogo;

    public SeasonAdapter levelsAdapter;

    private ArrayList<View> indicators = new ArrayList<>();
    private MediaPlayer mediaPlayer;
    private FrameLayout rippleContainer;

    private boolean musicPaused; // for listenMusic achievement

    public Point coinImagePoint, startPoint, middlePoint;

    private Dialog settingDialog;
    private Dialog shareDialog;
    public Dialog statusDialog;
    public Dialog achievementDialog;

    public boolean showRate;

    private static MainActivity instance;
    private HashMap<String, Object> serverData;

    public static ArrayList<Event> CurrEvents = new ArrayList<>();
    public static GameConfig Last_Config_Created;
    public static boolean adIsActive;

    private ArrayList<SeasonsFragment> fragments;
    private InternetAvailabilityChecker mInternetAvailabilityChecker;

    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] XAndY = new int[2];
        coinImg.getLocationOnScreen(XAndY);
        coinImagePoint = new Point(XAndY[0], XAndY[1]);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (PersianStorePayment.getInstance() != null)
            if (!PersianStorePayment.getInstance().mHelper.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
                Log.i(PersianStorePayment.getInstance().TAG, "onActivityResult is Success");
            } else {
                Log.i(PersianStorePayment.getInstance().TAG, "onActivityResult is Not Success");
            }

        if (requestCode == Application.SHARE_REQUEST_CODE) {

            int shareCount = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.shareCount));
            shareCount++;
            PreferencesHelper.setString(PreferencesHelper.shareCount, String.valueOf(shareCount));

            if (shareCount <= 5) { // ta 5 bar bishtar nabayad coin bedim be user
                Coin.addCoin(this, parent, 10, startPoint, middlePoint, coinImagePoint, () -> {
                    // Nothing to do yet
                });
            }

            //achievement handling
            switch (shareCount) {

                case 1:
                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.share_1.value)) {
                        PreferencesHelper.setString(AchievementJson.Achievements.share_1.value, "1");
                        Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.share_1.value)).show();
                    }
                    break;

                case 10:
                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.share_10.value)) {
                        PreferencesHelper.setString(AchievementJson.Achievements.share_10.value, "1");
                        Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.share_10.value)).show();
                    }
                    break;

            }


            // fireBaseAnalytics
            String event = "Share_Game_fa";
            Bundle bundle = new Bundle();
            bundle.putString("Device_Id", Application.deviceID);
            Application.firebaseAnalytics.logEvent(event, bundle);

        }

    }

    private void fireBaseSetup() {

        Application.firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Application.firebaseDB = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        Application.firebaseDB.setFirestoreSettings(settings);

    }

    private void backendlessSetup() {
        Backendless.initApp(this, Application.backendlessApplicationID, Application.backendlessAndroidApiKey);
    }

    private void internetAvailabilityCheckerSetup() {
        InternetAvailabilityChecker.init(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Application.localization(this);
        switch (Application.appSource) {
            case "bazaar":
                if (!PublicModules.appInstalledOrNot(MainActivity.this, "com.farsitel.bazaar"))
                    Application.appSource = "google";
                break;
            case "myket":
                if (!PublicModules.appInstalledOrNot(MainActivity.this, "ir.mservices.market"))
                    Application.appSource = "google";
                break;
        }
        setContentView(R.layout.activity_main);

        instance = this;
        PRDownloader.initialize(getApplicationContext());

        fireBaseSetup();
        backendlessSetup();
        internetAvailabilityCheckerSetup();

        init();
        initPoints();
        setListeners();

        initCoins();
        showSplash();

        checkPushPopup();
        purchaseOffer();

        mainUnreadAchievementsNotify();
        checkFirebaseEventAvailability();
        checkFireBaseAdvertiseAvailability();
//        AppModules.scheduleNotification(this, AppModules.getNotification(this, "zhooon baba"), 10000); //TODO

        TX_Ad.TX_AvailableAds(new TX_Ad.AdAPICallBack() {
            @Override
            public void getJsonArray(JSONArray result) {

                ArrayList<String> adNames = new ArrayList<>();

                for (int i = 0; i < result.length(); i++) {
                    try {
                        JSONObject currentAd = result.getJSONObject(i);
                        String adName = currentAd.getString("adName");
                        String adUrl = currentAd.getString("adZIPURL");

                        File adsDirectory = new File(TX_Ad.AD_DIRECTORY, adName);
                        if (!adsDirectory.exists()) { // age ke ad vojud nadasht , download kon
                            TX_Ad.downloadAds(MainActivity.this, adName, adUrl);
                        }

                        adNames.add(adName);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                // age ad e ezafi vojud dasht, pakesh kon
                TX_Ad.deleteUnusedAds(adNames);

            }

            @Override
            public void getStatus(int status) {

            }
        });

//        if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
//            WebView.setWebContentsDebuggingEnabled(true);
//        } //TODO for inspect

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            musicPaused = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mediaPlayer != null)
            if (PreferencesHelper.getString(PreferencesHelper.musicIsEnabled).equals("1")) {
                mediaPlayer.start();
                musicPaused = false;
            }

        if (PreferencesHelper.getString(PreferencesHelper.isUserRated).equals("0")) {
            ArrayList<String> levelsToRate = PreferencesHelper.getStringArrayPref(PreferencesHelper.levelsToRate);
            ArrayList<String> passLevels = PreferencesHelper.getStringArrayPref(PreferencesHelper.passedLevelsKey);
            if (showRate) {
//                for (int i = 0; i < passLevels.size(); i++) {
//                    for (int j = 0; j < levelsToRate.size(); j++) {
//                        if (passLevels.get(i).equals(levelsToRate.get(j))) {
//                            Dialogs.rateDialog(MainActivity.this).show();
//                            return;
//                        }
//                    }
//                }
                for (int i = 0; i < levelsToRate.size(); i++) {
                    if (passLevels.contains(levelsToRate.get(i))) {
                        Dialogs.rateDialog(MainActivity.this).show();
                        break;
                    }
                }
            }
        }

        statusDialog = Dialogs.statsDialog(MainActivity.this);
        achievementDialog = Dialogs.achievementsDialog(MainActivity.this);
        coinCountText.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.coin)));
    }

    @Override
    public void onBackPressed() {
        if (weAreInMain) {
            super.onBackPressed();
            stopMusic();
        } else {
            backBtn.callOnClick();
            checkFirebaseEventAvailability();
        }
    }

    private void handleEventDate() {
        int dayCount;
        int hourCount;

        // end event date
        Calendar endEventDate = Calendar.getInstance();
        endEventDate.setTime(new Date(MainActivity.CurrEvents.get(0).getEventEnd()));

        // start event date
        Calendar startEventDate = Calendar.getInstance();
        startEventDate.setTime(new Date(MainActivity.CurrEvents.get(0).getEventStart()));

        // current date
        Calendar todayDate = Calendar.getInstance();

        // hanuz munde ta berese
        if (startEventDate.getTimeInMillis() > todayDate.getTimeInMillis()) { // mohasebe aghaz e event
            dayCount = startEventDate.get(Calendar.DAY_OF_YEAR) - todayDate.get(Calendar.DAY_OF_YEAR);
            hourCount = startEventDate.get(Calendar.HOUR_OF_DAY) - todayDate.get(Calendar.HOUR_OF_DAY);

            eventCountDownText.setTextColor(getResources().getColor(R.color.greenLight));

            if (dayCount > 0) { // age rooz munde bashe ta beresim be shoru e event
                if (hourCount > 0) { // age bish az 1 saat ham munde bashe
                    eventCountDownText.setText(PublicModules.toPersianDigit(String.valueOf(dayCount)) + " روز و " + PublicModules.toPersianDigit(String.valueOf(hourCount)) + " ساعت تا شروع");
                } else { // age kamtar az 1 saat munde bashe
                    eventCountDownText.setText(PublicModules.toPersianDigit(String.valueOf(dayCount)) + " روز تا شروع");
                }
            } else { // age emruz gharare shoru she

                int minute = endEventDate.get(Calendar.MINUTE) - todayDate.get(Calendar.MINUTE);

                if (hourCount > 0) { // age chand saat munde be shoru
                    eventCountDownText.setText(PublicModules.toPersianDigit(String.valueOf(hourCount)) + " ساعت تا شروع");
                } else { // age 1 saat ya kamtar az 1 saat munde bashe be shoru
                    eventCountDownText.setText("کم تر از ۱ ساعت تا شروع");
                }
            }

        } else { // mohasebe payan e event (daghighan mesle balast)
            dayCount = endEventDate.get(Calendar.DAY_OF_YEAR) - todayDate.get(Calendar.DAY_OF_YEAR);
            hourCount = endEventDate.get(Calendar.HOUR_OF_DAY) - todayDate.get(Calendar.HOUR_OF_DAY);

            eventCountDownText.setTextColor(getResources().getColor(R.color.red));

            if (dayCount > 0) {
                if (hourCount > 0) {
                    eventCountDownText.setText(PublicModules.toPersianDigit(String.valueOf(dayCount)) + " روز و " + PublicModules.toPersianDigit(String.valueOf(hourCount)) + " ساعت تا پایان");
                } else {
                    eventCountDownText.setText(PublicModules.toPersianDigit(String.valueOf(dayCount)) + " روز تا پایان");
                }
            } else {
                int minute = endEventDate.get(Calendar.MINUTE) - todayDate.get(Calendar.MINUTE);

                if (hourCount > 0) {
                    eventCountDownText.setText(PublicModules.toPersianDigit(String.valueOf(hourCount)) + " ساعت تا پایان");
                } else if (hourCount < 0) {
                    eventCountDownText.setText("این رویداد تمام شده");
                } else {
                    if (minute > 0) {
                        eventCountDownText.setText("کم تر از ۱ ساعت تا پایان");
                    } else {
                        eventCountDownText.setText("این رویداد تمام شده");
                    }
                }
            }

        }

    }

    private void checkFirebaseEventAvailability() {
        Application.firebaseDB.collection("wordcutfa").document("events").addSnapshotListener((documentSnapshot, e) -> {
            if (documentSnapshot.getData() != null) {

                Map<String, Object> map = documentSnapshot.getData();
                Map.Entry<String, Object> entry = map.entrySet().iterator().next(); // felan faghat ba avalin event kar darim
                serverData = (HashMap<String, Object>) map.get(entry.getKey());

                if (serverData != null && serverData.size() > 0) {
                    MainActivity.CurrEvents.clear();

                    Event new_event = new Event(entry.getKey(), serverData);
                    MainActivity.CurrEvents.add(new_event);
                    if (MainActivity.CurrEvents.size() > 0 && MainActivity.CurrEvents.get(0).isActive()) { // agar eventi vojud dasht va faal bud

                        // init Main UI
                        eventContainer.setVisibility(View.VISIBLE);

                        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

                        Glide.with(MainActivity.this).load(MainActivity.CurrEvents.get(0).getImageUrlMain()).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                eventCountDownText.setVisibility(View.VISIBLE);
                                return false;
                            }
                        }).into(eventLogo);
                        handleEventDate();
                    } else {
                        eventContainer.setVisibility(View.GONE);
                    }
                }


            }
        });
    }

    private void checkFireBaseAdvertiseAvailability() {
        Application.firebaseDB.collection("wordcutfa").document("Ads").addSnapshotListener((documentSnapshot, e) -> {
            if (documentSnapshot.getData() != null) {
                Map<String, Object> map = documentSnapshot.getData();
                adIsActive = (boolean) map.get("Ads_AndroidIsActive");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null)
            stopMusic();

        if (PersianStorePayment.getInstance() != null) {
            if (PersianStorePayment.getInstance().mHelper != null)
                PersianStorePayment.getInstance().mHelper.dispose();
            PersianStorePayment.getInstance().mHelper = null;
        }
    }

    private void init() {
        showRate = false;

        splashImageView = findViewById(com.taxiapps.wordcut.R.id.act_main_splash_img);
        headerLayout = findViewById(com.taxiapps.wordcut.R.id.act_main_header_container);
        footerLayout = findViewById(com.taxiapps.wordcut.R.id.act_main_footer_container);
        rippleContainer = findViewById(com.taxiapps.wordcut.R.id.act_main_ripple_container);
        eventContainer = findViewById(R.id.act_main_event_container);
        mainLogo = findViewById(com.taxiapps.wordcut.R.id.act_main_menu_logo);
        coinImg = findViewById(com.taxiapps.wordcut.R.id.act_main_coin_img);
        eventLogo = findViewById(com.taxiapps.wordcut.R.id.act_main_event_image);

        eventSeasonTitle = findViewById(R.id.act_main_event_seasons_top);
        levelListBanner = findViewById(R.id.act_main_level_list_banner);
        lotteryBtn = findViewById(R.id.act_main_lottery_btn);

        parent = findViewById(R.id.act_main_root);

        settingBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_settings_btn);
        achievementBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_achievements_btn);
        reportBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_stats_btn);
        shareBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_share_btn);
        coinAddBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_coin_add);
        coinCountText = findViewById(com.taxiapps.wordcut.R.id.act_main_coin_count);
        newAchievementNotification = findViewById(com.taxiapps.wordcut.R.id.act_main_new_achievements);
        newAchievementCount = findViewById(com.taxiapps.wordcut.R.id.new_acgievements_count);
        start1 = findViewById(com.taxiapps.wordcut.R.id.act_main_start_btn1);
        start2 = findViewById(com.taxiapps.wordcut.R.id.act_main_start_btn2);
        start3 = findViewById(com.taxiapps.wordcut.R.id.act_main_start_btn3);
        start4 = findViewById(com.taxiapps.wordcut.R.id.act_main_start_btn4);

        mainListHeaderLayout = findViewById(com.taxiapps.wordcut.R.id.act_main_list_level_header_layout);
        mainListFooterLayout = findViewById(com.taxiapps.wordcut.R.id.act_main_list_level_footer_layout);
        levelsViewPager = findViewById(com.taxiapps.wordcut.R.id.act_main_list_level_fragment_container);
        indicatorView = findViewById(com.taxiapps.wordcut.R.id.act_main_list_level_indicator_container);
        backBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_back_btn);
        previousPageBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_previous_page);
        nextPageBtn = findViewById(com.taxiapps.wordcut.R.id.act_main_next_page);
        seasonTextView = findViewById(com.taxiapps.wordcut.R.id.act_main_season_text_view);
        seasonTextViewTop = findViewById(com.taxiapps.wordcut.R.id.act_main_season_top_title);
        eventCountDownText = findViewById(com.taxiapps.wordcut.R.id.act_main_event_countdown);

        mainMusicBeatSecondsArrBtn = new ArrayList<>();
        for (int i = 0; i < mainMusicBeatSecondsArrRipple.size(); i++) {
            if (i % 2 == 0)
                mainMusicBeatSecondsArrBtn.add(mainMusicBeatSecondsArrRipple.get(i));
        }
    }

    private void setListeners() {

        View.OnClickListener listeners = new PublicListener() {

            @Override
            public void onClick(View v) {
                super.onClick(v);
                if (v.equals(settingBtn)) {
                    if (settingDialog == null) {
                        settingDialog = Dialogs.settingsDialog(MainActivity.this);
                        settingDialog.show();
                    } else {
                        if (!settingDialog.isShowing()) {
                            settingDialog.show();
                        }
                    }
                }
                if (v.equals(start1) || v.equals(start2) || v.equals(start3) || v.equals(start4)) {

                    GameConfig game_config = new GameConfig(null, LevelJson.levels, SeasonJson.seasons);

                    Last_Config_Created = game_config;

                    showMain(false);
                    showLevels(true, game_config);

                    makeIndicators(game_config);

                    if (!game_config.hasEvent()) {
                        // going to user Season
                        ArrayList<String> unlockedSeasons = PreferencesHelper.getStringArrayPref(PreferencesHelper.unLockedSeasons);
                        levelsViewPager.setCurrentItem(Integer.valueOf(unlockedSeasons.get(unlockedSeasons.size() - 1)) - 1);
                        initIndicators(levelsViewPager.getCurrentItem(), game_config);
                    }

                }
                if (v.equals(eventLogo)) {
                    Event curr_event = MainActivity.CurrEvents.get(0);
                    EventDialogFragment.newInstance(curr_event, EventDialogFragment.ENTER).show(getFragmentManager(), "Event");
                }

                if (v.equals(lotteryBtn)) {
                    EventDialogFragment.newInstance(Last_Config_Created.getEvent(), EventDialogFragment.REGISTER).show(getFragmentManager(), "Event");
                }
                if (v.equals(backBtn)) {
                    showLevels(false, Last_Config_Created);
                    new Handler().postDelayed(() -> showMain(true), 200);

                }
                if (v.equals(reportBtn)) {
                    if (statusDialog == null) {
                        statusDialog = Dialogs.statsDialog(MainActivity.this);
                        statusDialog.show();
                    } else {
                        if (!statusDialog.isShowing()) {
                            statusDialog.show();
                        }
                    }
                }
                if (v.equals(achievementBtn)) {
                    if (achievementDialog == null) {
                        achievementDialog = Dialogs.achievementsDialog(MainActivity.this);
                        achievementDialog.show();
                    } else {
                        if (!achievementDialog.isShowing()) {
                            achievementDialog.show();
                        }
                    }
                }
                if (v.equals(shareBtn)) {
                    int shareCount = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.shareCount));

                    if (shareCount <= 5) {
                        if (shareDialog == null) {
                            shareDialog = Dialogs.shareDialog(MainActivity.this, () -> {
                                shareDialog.dismiss();
                                int shareCount1 = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.shareCount));
                                shareCount1++;
                                PreferencesHelper.setString(PreferencesHelper.shareCount, String.valueOf(shareCount1));

                                switch (shareCount1) {

                                    case 1:
                                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.share_1.value)) {
                                            PreferencesHelper.setString(AchievementJson.Achievements.share_1.value, "1");
                                            Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.share_1.value)).show();
                                        }
                                        break;

                                    case 10:
                                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.share_10.value)) {
                                            PreferencesHelper.setString(AchievementJson.Achievements.share_10.value, "1");
                                            Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.share_10.value)).show();
                                        }
                                        break;

                                }

                            });
                            shareDialog.show();
                        } else {
                            if (!shareDialog.isShowing()) {
                                shareDialog.show();
                            }
                        }

                    } else {
                        AppModules.share(MainActivity.this);
                    }
                }
                if (v.equals(coinAddBtn)) {
                    Dialogs.payment(MainActivity.this, false, parent).show();
                }
                if (v.equals(previousPageBtn)) {
                    levelsViewPager.setCurrentItem(levelsViewPager.getCurrentItem() - 1);
                }
                if (v.equals(nextPageBtn)) {
                    levelsViewPager.setCurrentItem(levelsViewPager.getCurrentItem() + 1);
                }

            }
        };

        levelsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                initIndicators(position, Last_Config_Created);
                initSeasonTextView(Last_Config_Created, position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mainLogo.setOnClickListener(v -> {
            int tapLogoCount = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.tapLogoCount));
            tapLogoCount++;
            PreferencesHelper.setString(PreferencesHelper.tapLogoCount, String.valueOf(tapLogoCount));

            if (tapLogoCount == 30) {
                if (Achievement.achievementIsLocked(AchievementJson.Achievements.tapLogo.value)) {
                    PreferencesHelper.setString(AchievementJson.Achievements.tapLogo.value, "1");
                    Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.tapLogo.value)).show();
                }

//                    achievementDialog = Dialogs.achievementsDialog(MainActivity.this); // reintialize achievement popup
            }
        });

        settingBtn.setOnClickListener(listeners);
        start1.setOnClickListener(listeners);
        start2.setOnClickListener(listeners);
        start3.setOnClickListener(listeners);
        start4.setOnClickListener(listeners);
        backBtn.setOnClickListener(listeners);
        shareBtn.setOnClickListener(listeners);
        reportBtn.setOnClickListener(listeners);
        achievementBtn.setOnClickListener(listeners);
        coinAddBtn.setOnClickListener(listeners);
        previousPageBtn.setOnClickListener(listeners);
        nextPageBtn.setOnClickListener(listeners);
        eventLogo.setOnClickListener(listeners);
        lotteryBtn.setOnClickListener(listeners);
    }

    private void initCoins() {
        coinCountText.setText(PublicModules.toPersianDigit(Coin.getCoinNumber()));
    }

    public void updateCoins(boolean isIncrease, int updateCoin) {
        if (isIncrease) {
            int coinCount = Integer.valueOf(coinCountText.getText().toString());
            coinCount += updateCoin;
            coinCountText.setText(PublicModules.toPersianDigit(String.valueOf(coinCount)));
        } else {
            int coinCount = Integer.valueOf(coinCountText.getText().toString());
            coinCount--;
            coinCountText.setText(PublicModules.toPersianDigit(String.valueOf(coinCount)));
        }
    }

    private void showSplash() {
        splashImageView.animate().setDuration(1500).alpha(0).withEndAction(() -> showMain(true));
    }

    private void makeRipple() {
        final View view = new View(this);
        view.setBackgroundResource(com.taxiapps.wordcut.R.drawable.sh_circle);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        view.setLayoutParams(new FrameLayout.LayoutParams(width, width));
        view.setScaleX(0);
        view.setScaleY(0);

        rippleContainer.addView(view);

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        scaleX.setInterpolator(new EasingInterpolator(Ease.CIRC_OUT));
        scaleX.setDuration(1200);// duration 3 seconds
        scaleX.start();

        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        scaleY.setInterpolator(new EasingInterpolator(Ease.CIRC_OUT));
        scaleY.setDuration(1200);
        scaleY.start();
        view.animate().alpha(0).setDuration(1200).withEndAction(new Runnable() {
            @Override
            public void run() {
                rippleContainer.removeView(view);
            }
        });

    }

    public void playMusic() {
        mediaPlayer = MediaPlayer.create(this, com.taxiapps.wordcut.R.raw.word_cut_320kbps);

        mediaPlayer.start();
        musicPaused = false;

        new CountDownTimer(mediaPlayer.getDuration(), 10) {
            @Override
            public void onTick(long millisUntilFinished) {

                if (mainMusicBeatSecondsArrRipple.contains(Math.round(mediaPlayer.getCurrentPosition() / 10))) {
                    makeRipple();
                }
                if (mainMusicBeatSecondsArrBtn.contains(Math.round(mediaPlayer.getCurrentPosition() / 10))) {
                    startBtnsAnimation();
                }
            }

            @Override
            public void onFinish() {

            }
        }.start();

        mediaPlayer.setOnCompletionListener(mp -> {
            if (PreferencesHelper.getString(PreferencesHelper.musicIsEnabled).equals("1"))
                if (GamePlayActivity.getInstance() == null) {
                    playMusic();
                }
            if (!musicPaused) {

                PreferencesHelper.setString(AchievementJson.Achievements.listenMusic.value, "1");
                runOnUiThread(() -> {
                    if (!MainActivity.getInstance().isFinishing()) {
                        Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.listenMusic.value)).show();
                    }
                });
            }
            if (Achievement.achievementIsLocked(AchievementJson.Achievements.listenMusic.value)) {
            }
        });

    }

    public void stopMusic() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            musicPaused = true;
        }
    }

    private void startBtnsAnimation() {
        start1.animate().setDuration(80).translationY(-60).withEndAction(new Runnable() {
            @Override
            public void run() {
                start1.animate().setDuration(80).translationY(0);
                start2.animate().setDuration(80).translationY(-60).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        start2.animate().setDuration(80).translationY(0);
                        start3.animate().setDuration(80).translationY(-60).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                start3.animate().setDuration(80).translationY(0);
                                start4.animate().setDuration(80).translationY(-60).withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        start4.animate().setDuration(80).translationY(0);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    public void showMain(boolean setVisible) {
        if (setVisible) {

            headerLayout.setAlpha(1);
            headerLayout.setVisibility(View.VISIBLE);
            headerLayout.animate().translationY(20).setInterpolator(new EasingInterpolator(Ease.BACK_OUT));

            footerLayout.setAlpha(1);
            footerLayout.setVisibility(View.VISIBLE);
            footerLayout.animate().translationY(-20).setInterpolator(new EasingInterpolator(Ease.BACK_OUT));

            if (PreferencesHelper.getString(PreferencesHelper.musicIsEnabled).equals("1")) {
                if (mediaPlayer == null) // age ghablan play nashode bud
                    playMusic();

            }

            weAreInMain = true;
        } else {

            headerLayout.setAlpha(0);
            headerLayout.setVisibility(View.GONE);
            headerLayout.animate().translationY(-20).setInterpolator(new EasingInterpolator(Ease.BACK_IN));

            footerLayout.setAlpha(0);
            footerLayout.setVisibility(View.GONE);
            footerLayout.animate().translationY(20).setInterpolator(new EasingInterpolator(Ease.BACK_IN));

            weAreInMain = false;

        }
    }

    public void makeIndicators(GameConfig gameConfig) {

        indicators.clear();
        indicatorView.removeAllViews();

        ViewGroup.MarginLayoutParams layoutParams;
        if (PublicModules.screenSize(this) == 1) {
            layoutParams = new LinearLayout.LayoutParams(17, 17);
        } else {
            layoutParams = new LinearLayout.LayoutParams(30, 30);
        }
        layoutParams.setMargins(0, 0, 10, 0);
        if (gameConfig.hasEvent()) {
            for (int i = 0; i < gameConfig.getSeasons().size(); i++) {
                View view = new View(this);
                view.setLayoutParams(layoutParams);
                indicators.add(view);
                indicatorView.addView(view);
            }
        } else {
            for (int i = 0; i < SeasonJson.seasons.size(); i++) {
                View view = new View(this);
                view.setLayoutParams(layoutParams);
                indicators.add(view);
                indicatorView.addView(view);
            }
        }

        initIndicators(levelsViewPager.getCurrentItem(), gameConfig);
    }

    private void initIndicators(int currentPageNumber, GameConfig gameConfig) {

        ViewGroup.MarginLayoutParams layoutParams;
        if (PublicModules.screenSize(this) == 1) {
            layoutParams = new LinearLayout.LayoutParams(23, 23);
        } else {
            layoutParams = new LinearLayout.LayoutParams(40, 40);
        }
        layoutParams.setMargins(0, 0, 10, 0);
        if (gameConfig.hasEvent()) {
            for (int i = 0; i < gameConfig.getSeasons().size(); i++) {
                if (i == currentPageNumber) {
                    indicators.get(i).setBackgroundResource(com.taxiapps.wordcut.R.drawable.sh_season_full_indicator);
                    indicators.get(i).setLayoutParams(layoutParams);
                } else {
                    indicators.get(i).setBackgroundResource(com.taxiapps.wordcut.R.drawable.sh_season_empty_indicator);
                    if (PublicModules.screenSize(this) == 1) {
                        indicators.get(i).getLayoutParams().width = 17;
                        indicators.get(i).getLayoutParams().height = 17;
                    } else {
                        indicators.get(i).getLayoutParams().width = 30;
                        indicators.get(i).getLayoutParams().height = 30;
                    }
                }
            }
        } else {
            for (int i = 0; i < SeasonJson.seasons.size(); i++) {
                if (i == currentPageNumber) {
                    indicators.get(i).setBackgroundResource(com.taxiapps.wordcut.R.drawable.sh_season_full_indicator);
                    indicators.get(i).setLayoutParams(layoutParams);
                } else {
                    indicators.get(i).setBackgroundResource(com.taxiapps.wordcut.R.drawable.sh_season_empty_indicator);
                    if (PublicModules.screenSize(this) == 1) {
                        indicators.get(i).getLayoutParams().width = 17;
                        indicators.get(i).getLayoutParams().height = 17;
                    } else {
                        indicators.get(i).getLayoutParams().width = 30;
                        indicators.get(i).getLayoutParams().height = 30;
                    }
                }
            }
        }
    }

    private void initSeasonTextView(GameConfig gameConfig, final int season) {

        if (gameConfig.hasEvent()) {
            seasonTextViewTop.setVisibility(View.VISIBLE);

            new CountDownTimer(500, 25) {
                int counter = 0;

                @Override
                public void onTick(long millisUntilFinished) {
                    if (counter == 0) {
                        seasonTextViewTop.setText("قصر");
                    }
                    if (counter == 1) {
                        seasonTextViewTop.setText("فعل");

                    }
                    if (counter == 2) {
                        seasonTextViewTop.setText("فضل");

                    }
                    if (counter == 3) {
                        seasonTextViewTop.setText("فسخ");

                    }
                    if (counter == 4) {
                        seasonTextViewTop.setText("غزل");

                    }
                    if (counter == 5) {
                        seasonTextViewTop.setText("رذل");
                        counter = 0;
                    }

                    counter++;
                }

                @Override
                public void onFinish() {
                    seasonTextViewTop.setText("فصل " + PublicModules.toPersianDigit(String.valueOf(season + 1)));
                    seasonTextView.setText(gameConfig.getEvent().getTitle());
                }
            }.start();

        } else {
            seasonTextViewTop.setVisibility(View.GONE);

            new CountDownTimer(500, 25) {
                int counter = 0;

                @Override
                public void onTick(long millisUntilFinished) {
                    if (counter == 0)
                        seasonTextView.setText("قصر");
                    if (counter == 1)
                        seasonTextView.setText("فعل");
                    if (counter == 2)
                        seasonTextView.setText("فضل");
                    if (counter == 3)
                        seasonTextView.setText("فسخ");
                    if (counter == 4)
                        seasonTextView.setText("غزل");
                    if (counter == 5) {
                        seasonTextView.setText("رذل");
                        counter = 0;
                    }

                    counter++;
                }

                @Override
                public void onFinish() {
                    seasonTextView.setText("فصل " + PublicModules.toPersianDigit(String.valueOf(season + 1)));
                }
            }.start();
        }


    }

    public void goNextSeason(int seasonID) {
        levelsViewPager.setCurrentItem(seasonID);
        initIndicators(seasonID, Last_Config_Created);
        initSeasonTextView(Last_Config_Created, seasonID);
    }

    public void showLevels(boolean setVisible, GameConfig gameConfig) {

        if (setVisible) {

            int listAppearCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.listAppearCount));
            listAppearCount++;
            PreferencesHelper.setString(PreferencesHelper.listAppearCount, String.valueOf(listAppearCount));

            levelsViewPager.setAlpha(1);
            levelsViewPager.setVisibility(View.VISIBLE);
            ScaleAnimation scaleAnimation = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, levelsViewPager.getWidth() / 2.0f, levelsViewPager.getHeight() / 2.0f);
            scaleAnimation.setDuration(300);
            scaleAnimation.setInterpolator(this, android.R.interpolator.accelerate_decelerate);
            scaleAnimation.setFillAfter(true);
            levelsViewPager.startAnimation(scaleAnimation);

            scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mainListHeaderLayout.setAlpha(1);
                    mainListHeaderLayout.setVisibility(View.VISIBLE);
                    mainListHeaderLayout.animate().translationY(20).setInterpolator(new EasingInterpolator(Ease.BACK_OUT));

                    indicatorView.setAlpha(1);
                    indicatorView.setVisibility(View.VISIBLE);

                    mainListFooterLayout.setAlpha(1);
                    mainListFooterLayout.setVisibility(View.VISIBLE);
                    mainListFooterLayout.animate().translationY(-20).setInterpolator(new EasingInterpolator(Ease.BACK_OUT));
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            if (gameConfig.hasEvent()) {
                Glide.with(this).load(gameConfig.getEvent().getImageUrlSeasonsTop()).into(eventSeasonTitle);
                eventSeasonTitle.setVisibility(View.VISIBLE);
                levelListBanner.setVisibility(View.GONE);
                lotteryBtn.setVisibility(View.VISIBLE);
                Glide.with(this).load(gameConfig.getEvent().getImageUrlSeasonsTop()).into(eventSeasonTitle);
                initSeasonTextView(gameConfig, levelsViewPager.getCurrentItem());

            } else {
                eventSeasonTitle.setVisibility(View.GONE);
                levelListBanner.setVisibility(View.VISIBLE);
                lotteryBtn.setVisibility(View.GONE);
                initSeasonTextView(gameConfig, levelsViewPager.getCurrentItem());
            }

            eventContainer.setVisibility(View.GONE);

            initAdapter(gameConfig);

        } else {

            mainListHeaderLayout.animate().translationY(-20).setInterpolator(new EasingInterpolator(Ease.BACK_IN));
            mainListHeaderLayout.animate().alpha(0).setDuration(200).withEndAction(new Runnable() {
                @Override
                public void run() {
                    mainListHeaderLayout.setVisibility(View.GONE);
                }
            });

            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, levelsViewPager.getWidth() / 2.0f, levelsViewPager.getHeight() / 2.0f);
            scaleAnimation.setDuration(200);
            scaleAnimation.setInterpolator(this, android.R.interpolator.accelerate_decelerate);
            scaleAnimation.setFillAfter(true);
            levelsViewPager.startAnimation(scaleAnimation);

            scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    levelsViewPager.animate().alpha(0).setDuration(150);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            if (fragments != null) {
                fragments.clear();
            }

            levelsViewPager.getAdapter().notifyDataSetChanged();
            levelsViewPager.setVisibility(View.GONE);

            indicatorView.setAlpha(0);
            indicatorView.setVisibility(View.INVISIBLE);

            mainListFooterLayout.animate().translationY(20).setInterpolator(new EasingInterpolator(Ease.BACK_IN));
            mainListFooterLayout.animate().alpha(0).setDuration(200).withEndAction(() -> mainListFooterLayout.setVisibility(View.GONE));

            checkFirebaseEventAvailability();
        }
    }

    public ArrayList<GameButtons> makeButtons(int season, GameConfig gameConfig) {

        ArrayList<GameButtons> targetBtns = new ArrayList<>();

        for (int i = 1; i <= gameConfig.getLevels().size(); i++) {
            if (gameConfig.getLevels().get(String.valueOf(i)).getSeasonId() == season) {
                GameButtons gameButton = new GameButtons(this, PublicModules.toPersianDigit(String.valueOf(i)), 14, gameConfig.getLevels().get(String.valueOf(i)));
                targetBtns.add(gameButton);
            }
        }

        return targetBtns;
    }

    public void initAdapter(GameConfig gameConfig) {

        fragments = new ArrayList<>();

        for (int i = 1; i <= gameConfig.getSeasons().size(); i++) {
            ArrayList<GameButtons> targetBtns = makeButtons(i, gameConfig);
            fragments.add(SeasonsFragment.newInstance(targetBtns));
        }

        levelsAdapter = new SeasonAdapter(getSupportFragmentManager(), fragments);

        levelsViewPager.setAdapter(levelsAdapter);
//        levelsViewPager.getAdapter().notifyDataSetChanged();

    }

    public void mainUnreadAchievementsNotify() {
        ArrayList<String> unlockedAchievementsArr = PreferencesHelper.getStringArrayPref(PreferencesHelper.unlockedAchievement);
        ArrayList<String> collectedAchievementArr = PreferencesHelper.getStringArrayPref(PreferencesHelper.collectedAchievement);

        int unreadAchievementsCount = 0;
        for (int i = 0; i < unlockedAchievementsArr.size(); i++) {
            if (!collectedAchievementArr.contains(unlockedAchievementsArr.get(i))) {
                unreadAchievementsCount++;
            }
        }

        if (unreadAchievementsCount <= 0) {
            newAchievementNotification.setVisibility(View.INVISIBLE);
        } else {
            newAchievementNotification.setVisibility(View.VISIBLE);
            newAchievementCount.setText(PublicModules.toPersianDigit(String.valueOf(unreadAchievementsCount)));
        }
    }

    private void initPoints() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        startPoint = new Point(0, 0);
        middlePoint = new Point(width / 2, height / 2);
    }

    private void checkPushPopup() {
        if (getIntent().hasExtra("i")) { // check mikonim az tu notification oomade ya na
            PublicModules.showPushPopup(this, getIntent().getStringExtra("popUpTitle"), getIntent().getStringExtra("popUpContent"), Application.appSource, getIntent().getStringExtra("popUpURL"));
        }
    }

    private void purchaseOffer() {

        int coinCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.coin));
        int listViewAppearCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.listAppearCount));
        final int shareCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.shareCount));

        // 50 = check mikonim age az coin e geruntarin skill e mun (skip) kamtar bud va ...
        if (coinCount < 50 && listViewAppearCount % 3 == 0) {
            if (shareCount < 5) {
                if (PublicModules.getBooleanWithChance(3.5)) {
                    if (new Random().nextBoolean()) {
                        Dialogs.payment(this, true, parent).show();
                    } else {
                        if (shareDialog == null) {
                            shareDialog = Dialogs.shareDialog(MainActivity.this, () -> {
                                shareDialog.dismiss();
                                int shareCount2 = shareCount;
                                shareCount2++;
                                PreferencesHelper.setString(PreferencesHelper.shareCount, String.valueOf(shareCount2));

                                switch (shareCount2) {

                                    case 1:
                                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.share_1.value)) {
                                            PreferencesHelper.setString(AchievementJson.Achievements.share_1.value, "1");
                                            Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.share_1.value)).show();
                                        }
                                        break;

                                    case 10:
                                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.share_10.value)) {
                                            PreferencesHelper.setString(AchievementJson.Achievements.share_10.value, "1");
                                            Dialogs.achievementUnlockedDialog(MainActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.share_10.value)).show();
                                        }
                                        break;

                                }

                            });
                            shareDialog.show();
                        } else {
                            if (!shareDialog.isShowing()) {
                                shareDialog.show();
                            }
                        }
                    }
                }
            } else {
                Dialogs.payment(this, true, parent).show();
            }

        }

    }

    public void rateUS(Context context) {
        if (Application.appSource.equals("google")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
            intent.setPackage("com.android.vending");
            startActivity(intent);
        }
        if (Application.appSource.equals("bazaar")) {
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setData(Uri.parse("bazaar://details?id=" + context.getPackageName()));
            intent.setPackage("com.farsitel.bazaar");
            startActivity(intent);
        }
        if (Application.appSource.equals("myket")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
            intent.setPackage("ir.mservices.market");
            startActivity(intent);
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (Last_Config_Created != null) {
            if (Last_Config_Created.hasEvent()) {
                String eventName = Last_Config_Created.getEvent().getName();
                if (isConnected && PreferencesHelper.getEventSettings(eventName, PreferencesHelper.eventUserPendingData).equals("1")) {
                    Application.backendlessHandling(eventName, new AsyncCallback() {
                        @Override
                        public void handleResponse(Object response) {
                            PreferencesHelper.setEventSettings(eventName, PreferencesHelper.eventUserPendingData, "0"); // yani movafagh bud va datay pending nadare
                            PreferencesHelper.setEventSettings(eventName, PreferencesHelper.isUserSubmitted, "1");
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                        }
                    });
                }
            }
        }

    }
}