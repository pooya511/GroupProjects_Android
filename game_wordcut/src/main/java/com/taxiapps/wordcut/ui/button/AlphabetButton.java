package com.taxiapps.wordcut.ui.button;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.taxiapps.wordcut.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Parsa on 2018-03-08.
 */

public class AlphabetButton extends android.support.v7.widget.AppCompatImageButton {

    public static ArrayList<String> alphabetIds = new ArrayList<>(Arrays.asList(
            "آ", "ا", "ب", "پ", "ت", "ث", "ج", "چ", "ح", "خ", "د", "ذ", "ر", "ز", "ژ", "س", "ش", "ص", "ض", "ط", "ظ", "ع", "غ", "ف", "ق", "ک", "گ", "ل", "م", "ن", "و", "ه", "ی"
    ));

    public enum AlphabetButtonResourceMode {

        ShowAlphabet(1),
        QuestionMark(2),
        WhiteSpace(3),
        WordCut(4);

        private int type;

        AlphabetButtonResourceMode(int type) {
            this.type = type;
        }

        public int value() {
            return this.type;
        }

    }

    private int alphabetId;
    private int adapterPosition;
    private AlphabetButtonResourceMode resourceMode;

    public int getAlphabetId() {
        return alphabetId;
    }

    public void setAlphabetId(int alphabetId) {
        this.alphabetId = alphabetId;
    }

    public AlphabetButtonResourceMode getResourceMode() {
        return resourceMode;
    }

    public void setShowAlphabet(Context context, AlphabetButtonResourceMode resourceMode, int alphabetID) {
        this.resourceMode = resourceMode;
        this.alphabetId = alphabetID;
        switch (resourceMode) {
            case WordCut:
                this.setBackgroundResource(com.taxiapps.wordcut.R.drawable.btn_word_cut_place_holder_fa);
                break;

            case QuestionMark:
                this.setBackgroundResource(com.taxiapps.wordcut.R.drawable.btn_letters_place_holder);
                break;

            case ShowAlphabet:
                this.setBackgroundResource(getBackgroundResourceID(context, alphabetID));
                break;
        }
    }

    public int getAdapterPosition() {
        return adapterPosition;
    }

    public void setAdapterPosition(int adapterPosition) {
        this.adapterPosition = adapterPosition;
    }

    public AlphabetButton(Context context) {
        super(context);
    }

    public AlphabetButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlphabetButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AlphabetButton(Context context, int alphabetId, AlphabetButtonResourceMode resourceMode) {
        super(context);
        this.alphabetId = alphabetId;
        this.resourceMode = resourceMode;

        LinearLayout.LayoutParams layoutParams;

        switch (this.resourceMode) {
            case QuestionMark:
                this.setBackgroundResource(com.taxiapps.wordcut.R.drawable.btn_letters_place_holder);
                layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.answerPlaceHolderWidth), (int) context.getResources().getDimension(R.dimen.answerPlaceHolderHeight));
                this.setLayoutParams(layoutParams);
                break;
            case WhiteSpace:
                this.setAlpha(0f);
                this.setBackgroundResource(com.taxiapps.wordcut.R.drawable.btn_letters_place_holder);
                layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.answerPlaceHolderWidth), (int) context.getResources().getDimension(R.dimen.answerPlaceHolderHeight));
                this.setLayoutParams(layoutParams);
                break;
        }
    }

    public static int getBackgroundResourceID(Context context, int alphabetID) {
        String fileName = "slc_btn_alpha_fa_" + alphabetID;
        Resources resources = context.getResources();
        return resources.getIdentifier(fileName, "drawable",
                context.getPackageName());
    }

    public static int getAlphabetIndex(String alphabet) {
        int res = -1;
        for (int i = 0; i < alphabetIds.size(); i++) {
            if (AlphabetButton.getAlphabetValue(i).equals(alphabet)) {
                res = i;
                break;
            }
        }
        return res;
    }

    public static ArrayList<AlphabetButton> getRandomAlphabets(Context context, String targetWord) {

        ArrayList<AlphabetButton> resultAlphabets = new ArrayList<>(18);
        char[] targetCharArray = targetWord.toCharArray();

        for (int i = 0; i < targetCharArray.length; i++) {
            if (!String.valueOf(targetCharArray[i]).equals(" ")) { // check mikonim jay khali ha ro narize
                AlphabetButton alphabetButton = new AlphabetButton(context, getAlphabetIndex(String.valueOf(targetCharArray[i])), AlphabetButtonResourceMode.ShowAlphabet);
                alphabetButton.setTag("NotSelected");
                resultAlphabets.add(alphabetButton);
            }
        }

        int arrayEmptyCapacity = 18 - resultAlphabets.size();

        for (int i = 0; i < arrayEmptyCapacity; i++) {

            Random randomGenerator = new Random();
            int randomIndex = randomGenerator.nextInt((32) + 1);

            AlphabetButton alphabetButton = new AlphabetButton(context, randomIndex, AlphabetButtonResourceMode.ShowAlphabet);
            alphabetButton.setTag("NotSelected");
            resultAlphabets.add(alphabetButton);

        }

        Collections.shuffle(resultAlphabets);
        return resultAlphabets;
    }

    public static String getAlphabetValue(int indexNumber) {
        return alphabetIds.get(indexNumber);
    }

}
