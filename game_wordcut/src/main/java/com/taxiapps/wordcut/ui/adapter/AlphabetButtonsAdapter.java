package com.taxiapps.wordcut.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Toast;

import com.taxiapps.wordcut.model.listener.PublicListener;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.R;

import java.util.ArrayList;
import java.util.HashMap;

import com.taxiapps.wordcut.ui.button.AlphabetButton;

public class AlphabetButtonsAdapter extends RecyclerView.Adapter<AlphabetButtonsAdapter.AlphabetButtonsViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    public ArrayList<AlphabetButton> alphabetButtonArr;

    private boolean isFirstTime;
    private HashMap<Integer, Integer> animPosition;

    private void initAnimPosition() {
        animPosition = new HashMap<>();
        animPosition.put(0, 0);

        animPosition.put(1, 30);
        animPosition.put(6, 60);

        animPosition.put(2, 90);
        animPosition.put(7, 120);
        animPosition.put(12, 150);

        animPosition.put(3, 180);
        animPosition.put(8, 210);
        animPosition.put(13, 240);

        animPosition.put(4, 270);
        animPosition.put(9, 300);
        animPosition.put(14, 330);

        animPosition.put(5, 360);
        animPosition.put(10, 390);
        animPosition.put(15, 420);

        animPosition.put(11, 450);
        animPosition.put(16, 480);

        animPosition.put(17, 510 );

    }

    private static AlphabetButtonsAdapter instance;

    public static AlphabetButtonsAdapter getInstance() {
        return instance;
    }

    public AlphabetButtonsAdapter(Context context, String targetWord) {
        this.context = context;
        alphabetButtonArr = AlphabetButton.getRandomAlphabets(context, targetWord);
        inflater = LayoutInflater.from(context);
        isFirstTime = true;
        initAnimPosition();
        instance = this;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return alphabetButtonArr.get(position).getAlphabetId();
    }

    @Override
    public AlphabetButtonsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.itm_alphabet_buttons, parent, false);
        AlphabetButton alphabetButton = view.findViewById(R.id.itm_alphabet_buttons_image_btn);
        alphabetButton.setBackgroundResource(AlphabetButton.getBackgroundResourceID(context, viewType));
        alphabetButton.setTag("NotSelected"); // baray inke hame ye meghdar e avalie E dashte bashan va neshun mide ke hichkodum hanuz select nashodan
        return new AlphabetButtonsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AlphabetButtonsViewHolder holder, final int position) {
        final AlphabetButton currAlphabetButton = alphabetButtonArr.get(position);

        holder.alphabetBtn.setShowAlphabet(context, currAlphabetButton.getResourceMode(), currAlphabetButton.getAlphabetId());

        holder.alphabetBtn.setOnClickListener(new PublicListener() {
            @Override
            public void onClick(View v) {

                if (currAlphabetButton.getTag().equals("NotSelected") && currAlphabetButton.getResourceMode() != AlphabetButton.AlphabetButtonResourceMode.WordCut) { // check mikonim ghablan select nashode bashe va khali ham nabashe
                    super.onClick(v);
                    GamePlayActivity.getInstance().selectAlphabet(currAlphabetButton, holder.getAdapterPosition(),0);
                }

            }
        });

        if (currAlphabetButton.getTag() != null && currAlphabetButton.getTag().equals("Removed")) { // vaghti hazf mikone bara animationesh settag mikonim ke befahme in kalame hazf shode va bayad animate she
            toastForKalakRashti(); // age nabashe momkene animate e alphabet button anjam nashe !!!
            GamePlayActivity.getInstance().selectAlphabetAnimation(holder.alphabetBtn,0,false);
            currAlphabetButton.setTag("NotSelected");
        }

        // baray inke serie aval ke adapt mishe animation dashte bashe .
        if (isFirstTime)
            setAnimation(holder.itemView, position);

        if (position + 1 == alphabetButtonArr.size()) {
            isFirstTime = false;
        }
    }

    @Override
    public int getItemCount() {
        return alphabetButtonArr.size();
    }

    private void setAnimation(final View itemView, int position) {
        //scale aval bozorg mikone bad to end action kochikesh mikone
        ScaleAnimation firstScaleAnimation = new ScaleAnimation(0f, 1.6f, 0f, 1.6f, 50, 50);
        firstScaleAnimation.setDuration(200);
        firstScaleAnimation.setStartOffset(animPosition.get(position));
        firstScaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {

            }
            public void onAnimationEnd(Animation animation) {

                ScaleAnimation secondScaleAnimation = new ScaleAnimation(1.6f, 1.0f, 1.6f, 1.0f, 50, 50);
                secondScaleAnimation.setDuration(200);
                AnimationSet animationSet = new AnimationSet(true);
                animationSet.addAnimation(secondScaleAnimation);
                itemView.startAnimation(animationSet);
            }
            public void onAnimationRepeat(Animation animation) {

            }
        });
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(firstScaleAnimation);
        itemView.startAnimation(animationSet);
    }

    private void toastForKalakRashti() {
        Toast toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
        toast.show();
        toast.cancel();
    }

    class AlphabetButtonsViewHolder extends RecyclerView.ViewHolder {

        private AlphabetButton alphabetBtn;

        public AlphabetButtonsViewHolder(View itemView) {
            super(itemView);
            alphabetBtn = itemView.findViewById(R.id.itm_alphabet_buttons_image_btn);
        }
    }

}