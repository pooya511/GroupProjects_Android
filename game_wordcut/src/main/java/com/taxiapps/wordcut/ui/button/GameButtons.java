package com.taxiapps.wordcut.ui.button;

import android.annotation.SuppressLint;
import android.content.Context;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;

import com.taxiapps.wordcut.model.Level;

import java.io.Serializable;

/**
 * Created by Parsa on 2018-03-08.
 */

public class GameButtons extends android.support.v7.widget.AppCompatButton implements Serializable{

    private static final long serialVersionUID = -3721069441736445349L;
    private int backgroundResource;
    private String label;
    private float labelSize;
    private Level level;

    public GameButtons(final Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GameButtons(final Context context, String label, float labelSize, Level level) {
        super(context);
        this.setText(label);
        this.setTextSize(labelSize);
        this.level = level;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getLabelSize() {
        return labelSize;
    }

    public void setLabelSize(float labelSize) {
        this.labelSize = labelSize;
    }

    public int getBackgroundResource() {
        return backgroundResource;
    }

    @Override
    public void setBackgroundResource(int backgroundResource) {
        this.backgroundResource = backgroundResource;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

}