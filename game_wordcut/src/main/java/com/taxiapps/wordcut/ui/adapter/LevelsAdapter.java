package com.taxiapps.wordcut.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.model.GameConfig;
import com.taxiapps.wordcut.model.Level;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;
import com.taxiapps.wordcut.ui.button.GameButtons;

import java.util.ArrayList;
import java.util.HashMap;

import modules.PublicModules;

/**
 * Created by Parsa on 2018-03-13.
 */

public class LevelsAdapter extends RecyclerView.Adapter<LevelsAdapter.LevelViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<GameButtons> gameButtons;

    private final int disableMode = 0;
    private final int enableMode = 1;
    private final MediaPlayer btnSound;

    private HashMap<Integer, Integer> animPosition;
    private GameConfig gameConfig;

    private void initAnimPosition() {

        animPosition = new HashMap<>();

        animPosition.put(0, 200);

        animPosition.put(1, 260);
        animPosition.put(5, 260);

        animPosition.put(2, 350);
        animPosition.put(6, 350);
        animPosition.put(10, 350);

        animPosition.put(3, 400);
        animPosition.put(7, 400);
        animPosition.put(11, 400);
        animPosition.put(15, 400);

        animPosition.put(4, 500);
        animPosition.put(8, 500);
        animPosition.put(12, 500);
        animPosition.put(16, 500);
        animPosition.put(20, 500);

        animPosition.put(9, 600);
        animPosition.put(13, 600);
        animPosition.put(17, 600);
        animPosition.put(21, 600);
        animPosition.put(25, 600);

        animPosition.put(14, 700);
        animPosition.put(18, 700);
        animPosition.put(22, 700);
        animPosition.put(26, 700);

        animPosition.put(19, 800);
        animPosition.put(23, 800);
        animPosition.put(27, 800);

        animPosition.put(24, 900);
        animPosition.put(28, 900);

        animPosition.put(29, 1000);

    }

//    private ArrayList<GameButtons> makeButtons(GameConfig gameConfig) {
//
//        ArrayList<GameButtons> targetBtns = new ArrayList<>();
//
//        for (int i = 0; i < gameConfig.getSeasons().size(); i++) {
//            for (int j = 0; j < gameConfig.getLevels().size(); j++) {
//                if (gameConfig.getLevels().get(String.valueOf(j+1)).getSeasonId() == gameConfig.getSeasons().get(String.valueOf(i+1)).getId()) {
//                    GameButtons gameButton = new GameButtons(context, PublicModules.toPersianDigit(String.valueOf(j+1)), 14, gameConfig.getLevels().get(String.valueOf(i+1)));
//                    targetBtns.add(gameButton);
//                }
//            }
//        }
//
//        return targetBtns;
//    }

    public LevelsAdapter(Context context, ArrayList<GameButtons> gameButtons, GameConfig aGameConfig) {
        this.context = context;
        this.gameConfig = aGameConfig;
        this.gameButtons = gameButtons;
        inflater = LayoutInflater.from(context);
        btnSound = MediaPlayer.create(context, R.raw.button_action_sound);
        initAnimPosition();
    }

    @Override
    public int getItemViewType(int position) {

        ArrayList<String> unlockedLevels;

        if (gameConfig.hasEvent()) {
            unlockedLevels = PreferencesHelper.getStringArrayPrefEvent(gameConfig.getEvent().getName(), PreferencesHelper.unLockedLevels);
        } else {
            unlockedLevels = PreferencesHelper.getStringArrayPref(PreferencesHelper.unLockedLevels);
        }

        Level curr_level = gameButtons.get(position).getLevel();
        String curr_level_id = String.valueOf(curr_level.getId());

        if (unlockedLevels.contains(String.valueOf(curr_level_id))) {
            curr_level.setUnlocked(true);
        } else {
            curr_level.setUnlocked(false);
        }

        if (curr_level.isUnlocked())
            return enableMode;
        else
            return disableMode;
    }

    @Override
    public LevelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == enableMode)
            return new LevelViewHolder(inflater.inflate(R.layout.itm_enable_levels, parent, false));
        else
            return new LevelViewHolder(inflater.inflate(R.layout.itm_disable_levels, parent, false));
    }

    @Override
    public void onBindViewHolder(final LevelViewHolder holder, int position) {
        final GameButtons button = gameButtons.get(position);

        holder.button.setText(PublicModules.toPersianDigit(String.valueOf(button.getLevel().getId())));
        holder.button.animate().alpha(1).setDuration(animPosition.get(position));
        holder.button.setOnTouchListener((v, event) -> {
            if (button.getLevel().isUnlocked()) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")) {
                        btnSound.start();
                    }

                    v.setPadding(0, 5, 0, 0);
                    v.setPressed(true);

                    Intent gamePlayIntent = new Intent(context, GamePlayActivity.class);
                    gamePlayIntent.putExtra("Level", button.getLevel());
                    context.startActivity(gamePlayIntent);

                } else {
                    v.setPadding(0, 0, 0, 5);
                    v.setPressed(false);
                }
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return gameButtons.size();
    }

    class LevelViewHolder extends RecyclerView.ViewHolder {

        private Button button;

        public LevelViewHolder(View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.itm_levels_target_button);
            button.setAlpha(0f);

        }

    }

}