package com.taxiapps.wordcut.ui.adapter;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taxiapps.wordcut.model.Achievement;
import com.taxiapps.wordcut.model.Coin;
import com.taxiapps.wordcut.model.listener.AchievementListener;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.ui.activity.MainActivity;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;
import modules.PublicModules;

public class AchievementsAdapter extends RecyclerView.Adapter<AchievementsAdapter.AchievementViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Achievement> achievementsArray;
    private FrameLayout parentLayout;

    private ArrayList<String> unlockedAchievements;
    private ArrayList<String> collectedAchievement;
    private Achievement.AchievementCollectCallBack achievementCollectCallBack;

    private final int disableType = 0;
    private final int unreadType = 1;
//    private final int collectedType = 2;


    public AchievementsAdapter(Context context, ArrayList<Achievement> achievementsArray, ArrayList<String> unlockedAchievements, ArrayList<String> collectedAchievement, FrameLayout parentLayout, Achievement.AchievementCollectCallBack achievementCollectCallBack) {
        this.parentLayout = parentLayout;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.achievementsArray = achievementsArray;
        this.unlockedAchievements = unlockedAchievements;
        this.collectedAchievement = collectedAchievement;

        this.achievementCollectCallBack = achievementCollectCallBack;

//        remove invisible items
        for (int i = 0; i < achievementsArray.size(); i++) {
            if (!achievementsArray.get(i).isVisible()) { // age visible nabud
                if (!unlockedAchievements.contains(achievementsArray.get(i).getId())) { // va age unlock ham nashode bud
                    Log.i("achievementsArray", achievementsArray.get(i).getId()); // pas bia az arrayi ke gharare neshun bedim , pakesh kon
                    achievementsArray.remove(i);
                    i--;
                }
            }
        }

        this.setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {

        Achievement currentAchievement = achievementsArray.get(position);

        for (int i = 0; i < unlockedAchievements.size(); i++) {
            if (unlockedAchievements.get(i).equals(currentAchievement.getId())) {
                for (int j = 0; j < collectedAchievement.size(); j++) {
                    if (collectedAchievement.get(j).equals(currentAchievement.getId())) {
                        achievementsArray.get(position).setCollected(true);
                        break;
                    }
                }
                return unreadType;
            }
        }
        return disableType;
    }

    @Override
    public AchievementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case unreadType:
                return new AchievementViewHolder(layoutInflater.inflate(com.taxiapps.wordcut.R.layout.item_achievements, parent, false));
            default:
                //return disabled view
                return new AchievementViewHolder(layoutInflater.inflate(com.taxiapps.wordcut.R.layout.item_disable_achievements, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final AchievementViewHolder holder, final int position) {
        final Achievement currentAchievement = achievementsArray.get(position);

        final int frontResource;
        Typeface iranSans = Typeface.createFromAsset(context.getAssets(), "fonts/" + "IRANSans_Medium.ttf");
        if (getItemViewType(position) == unreadType)
            frontResource = Achievement.getBackgroundResource(context, currentAchievement.getId(), true);
        else
            frontResource = Achievement.getBackgroundResource(context, currentAchievement.getId(), false);

        if (currentAchievement.isCollected()) {
            holder.goldContainer.setVisibility(View.GONE);
            holder.goldCollected.setVisibility(View.VISIBLE);
        } else {
            holder.goldContainer.setVisibility(View.VISIBLE);
            holder.goldCollected.setVisibility(View.GONE);
        }

        if (!currentAchievement.isCollected()) {
            holder.collectGold.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        if (getItemViewType(position) == unreadType) {
                            v.setOnTouchListener(null);
                            currentAchievement.setCollected(true);
                            notifyItemChanged(position);
                            int[] ints = new int[2];
                            v.getLocationOnScreen(ints);
                            Point start = new Point(0, 0);
                            Point middle = new Point(ints[0], ints[1]);
                            Point end = MainActivity.getInstance().coinImagePoint;
                            Coin.addCoin(context, parentLayout, currentAchievement.getReward(), start, middle, end, new Coin.CoinAddedCallBack() {
                                @Override
                                public void call() {

                                }

                            });

                            ArrayList<String> collectedAchievement = PreferencesHelper.getStringArrayPref(PreferencesHelper.collectedAchievement);
                            collectedAchievement.add(currentAchievement.getId());
                            PreferencesHelper.setStringArrayPref(PreferencesHelper.collectedAchievement, collectedAchievement);

                            achievementCollectCallBack.call(); //to change new achievements text in popUp
//                                    notifyDataSetChanged();

                            return true;
                        } else {
                            return false;
                        }
                    }
                    return false;
                }
            });
        }

        holder.goldCount.setText(PublicModules.toPersianDigit(String.valueOf(currentAchievement.getReward())));

        holder.goldCollected.setTypeface(iranSans);

        if (currentAchievement.isCollected()) {
            holder.goldContainer.setVisibility(View.GONE);
            holder.goldCollected.setVisibility(View.VISIBLE);
        }

        if (currentAchievement.isImageFront()) {
            holder.achievementImage.setImageResource(frontResource);
            holder.achievementImage.setVisibility(View.VISIBLE);
            holder.achievementImageBack.setVisibility(View.INVISIBLE);
        } else {
            holder.achievementImage.setVisibility(View.INVISIBLE);
            holder.achievementImageBack.setVisibility(View.VISIBLE);
        }

        if (currentAchievement.isImageFront()) {
            holder.achievementImageCointainer.setOnClickListener(new AchievementListener(holder, true, new AchievementListener.AnimationEnd() {
                @Override
                public void call() {
                    currentAchievement.setImageFront(false);
                    notifyItemChanged(position);
                }
            }));
        } else {
            holder.achievementImageCointainer.setOnClickListener(new AchievementListener(holder, false, new AchievementListener.AnimationEnd() {
                @Override
                public void call() {
                    currentAchievement.setImageFront(true);
                    notifyItemChanged(position);

                }
            }));
        }

        holder.achievementText.setText(currentAchievement.getToolTipFa());
        holder.achievementText.setTypeface(iranSans);

    }

    @Override
    public int getItemCount() {
        return achievementsArray.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class AchievementViewHolder extends RecyclerView.ViewHolder {

        public ImageView achievementImage, achievementSelectedImage, icCoin;
        public FrameLayout achievementImageBack, achievementImageCointainer;
        public AutofitTextView achievementText;
        public TextView goldCount, goldCollected;
        public Button collectGold;
        public LinearLayout goldContainer;


        public AchievementViewHolder(View itemView) {
            super(itemView);
            achievementImage = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_achieve_image);
            achievementImageBack = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_achieve_image_back);
            achievementSelectedImage = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_achieve_selected_img);
            achievementImageCointainer = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_image_container);
            achievementText = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_achieve_text);
            collectGold = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_collect_gold_btn);
            goldContainer = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_collect_gold_container);
            goldCount = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_gold_count);
            icCoin = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_ic_gold);
            goldCollected = itemView.findViewById(com.taxiapps.wordcut.R.id.item_achievements_gold_collected);
        }

    }

}
