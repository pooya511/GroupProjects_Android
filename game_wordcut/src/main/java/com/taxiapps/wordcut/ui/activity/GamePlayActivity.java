package com.taxiapps.wordcut.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.bumptech.glide.Glide;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGImageView;
import com.takusemba.spotlight.OnSpotlightStateChangedListener;
import com.takusemba.spotlight.OnTargetStateChangedListener;
import com.takusemba.spotlight.Spotlight;
import com.takusemba.spotlight.shape.Circle;
import com.takusemba.spotlight.shape.Shape;
import com.takusemba.spotlight.target.SimpleTarget;
import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.model.Achievement;
import com.taxiapps.wordcut.model.Coin;
import com.taxiapps.wordcut.model.Level;
import com.taxiapps.wordcut.model.Season;
import com.taxiapps.wordcut.model.json.AchievementJson;
import com.taxiapps.wordcut.model.listener.OnSwipeTouchListener;
import com.taxiapps.wordcut.model.listener.PublicListener;
import com.taxiapps.wordcut.ui.adapter.AlphabetButtonsAdapter;
import com.taxiapps.wordcut.ui.button.AlphabetButton;
import com.taxiapps.wordcut.ui.dialog.Dialogs;
import com.taxiapps.wordcut.utils.payment.PersianStorePayment;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;

import io.opencensus.stats.Aggregation;
import modules.PublicModules;
import modules.TX_Ad;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.taxiapps.wordcut.PreferencesHelper.unLockedLevels;
import static com.taxiapps.wordcut.ui.button.AlphabetButton.AlphabetButtonResourceMode.QuestionMark;
import static com.taxiapps.wordcut.ui.button.AlphabetButton.AlphabetButtonResourceMode.ShowAlphabet;

public class GamePlayActivity extends Activity {

    private Level level;

    private ImageButton dictionaryBtn, skipBtn, eyeBtn;
    private Button menuBtn, buyCoin;
    private ImageView targetWord, coinImage;
    private LinearLayout answerContainer, recyclerViewParent, coinLayout;
    private RecyclerView recyclerView;
    private TextView dictionaryText;
    public TextView coinCountText;

    public FrameLayout parentFrame, levelImageContainer;

    public Point gamePlayCoinImagePoint, startPoint, middlePoint;

    private static GamePlayActivity instance;
    private AlphabetButtonsAdapter adapter;
    private Dialog levelCompeletedDialog;

    private ArrayList<UserAnswerStrings> selectedButtonsValues;// baray check kardan e nahayi e javabe user estefade mishe

    public ArrayList<String> levelsToShowRate = new ArrayList<>();

    private int gamePlayPlayingTimer = 0;
    private boolean oneShotCount = true;
    private boolean usedSkip = false;
    private boolean usedDic = false;

//    private Dialog eyeDialog;
//    private Dialog skipDialog;
//    private Dialog dictionaryDialog;
//    private Dialog buyCoinDialog;

    private PublicListener listener;
    private AlphabetButton letter;
    private PointF letterPoint;
    private ImageView sponsorLogo;

    private SVGImageView eventLevelImage;
    private boolean activityPaused = false;

    private void initPoints(int[] targetEndPoint) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        startPoint = new Point(0, 0);
        middlePoint = new Point(width / 2, height / 2);
        gamePlayCoinImagePoint = new Point(targetEndPoint[0], targetEndPoint[1]);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int[] result = new int[2];
        coinImage.getLocationOnScreen(result);
        initPoints(result);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (PersianStorePayment.getInstance() != null)
            if (!PersianStorePayment.getInstance().mHelper.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
                Log.i(PersianStorePayment.getInstance().TAG, "onActivityResult is Success");
            } else {
                Log.i(PersianStorePayment.getInstance().TAG, "onActivityResult is Not Success");
            }

        /**
         * in advertise -> report type : url
         */
        if (requestCode == TX_Ad.URL_REQUEST) {
            Bundle bundle = getIntent().getExtras();
            try {
                JSONObject ad = new JSONObject(bundle.getString("ad"));
                String report = bundle.getString("report");
                String deviceID = bundle.getString("deviceID");
                String metadata = bundle.getString("metaData");
                TX_Ad.sendReportFromActivity(ad, report, deviceID, metadata);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    private void initSelectedButtonsValues(String levelAnswer) {
        selectedButtonsValues = new ArrayList<>();
        for (int i = 0; i < levelAnswer.replace(" ", "").length(); i++) {
            selectedButtonsValues.add(new UserAnswerStrings(-1, "#"));
        }
    }

    public static GamePlayActivity getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_play);
        instance = this;

        level = (Level) getIntent().getExtras().getSerializable("Level");

        init();
        setListeners();
        initCoins();

        if (!MainActivity.Last_Config_Created.hasEvent()) {
            if (level.getId() == 1 && PreferencesHelper.getString(PreferencesHelper.gamePlayLevelOneFirstTime).equals("0")) {
                gamePlayTutorial(GamePlayActivity.this);
            }

            if (level.getId() == 2 && PreferencesHelper.getString(PreferencesHelper.gamePlayLevelTwoFirstTime).equals("0")) {
                skillsTutorial(GamePlayActivity.this);
            }

            sponsorLogo.setVisibility(View.GONE);
            targetWord.setVisibility(View.VISIBLE);

        } else {
            Glide.with(this).load(MainActivity.Last_Config_Created.getEvent().getSponsorLogo()).into(sponsorLogo);
            sponsorLogo.setVisibility(View.VISIBLE);
            targetWord.setVisibility(View.GONE);

            try {
                eventLevelImage = new SVGImageView(this);
                eventLevelImage.setVisibility(View.VISIBLE);
                SVG svg = SVG.getFromString(levelBase64Decode(level.getSvgImage()));
                eventLevelImage.setSVG(svg);

                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                eventLevelImage.setLayoutParams(params);
                eventLevelImage.setAdjustViewBounds(true);
                eventLevelImage.setScaleType(ImageView.ScaleType.FIT_CENTER);

                levelImageContainer.addView(eventLevelImage);

            } catch (Exception ex) {

            }

        }

        Typeface IranSansLightTypeFace = Typeface.createFromAsset(GamePlayActivity.getInstance().getAssets(), "fonts/IRANSans_Light.ttf");
        dictionaryText.setTypeface(IranSansLightTypeFace);

        guidBtnAnimation();
        initWordPicture(level.getId());
        initDictionaryText(level.getSkillDic());
        initAnswerContainer(level.getAnswer());
        initRecyclerView(level.getAnswer());

        new Handler().postDelayed(() -> levelSound(), 500);

        initSelectedButtonsValues(level.getAnswer());

        achievementTimers();
        reportsHandling();
//        initDialogs();

        if (PreferencesHelper.getString(PreferencesHelper.isUserRated).equals("0")) {
            ArrayList<String> passLevels = PreferencesHelper.getStringArrayPref(PreferencesHelper.passedLevelsKey);
            for (int j = 0; j < levelsToShowRate.size(); j++) {
                if (passLevels.contains(levelsToShowRate.get(j))) {
                    MainActivity.getInstance().showRate = true;
                }
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
//        initDialogs();
        activityPaused = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityPaused = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        int playTime = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.playTimeValue));
        playTime += gamePlayPlayingTimer;
        PreferencesHelper.setString(PreferencesHelper.playTimeValue, String.valueOf(playTime));

        Log.i("gamePlayPlayingTimer", PreferencesHelper.getString(PreferencesHelper.playTimeValue));

        if (PersianStorePayment.getInstance() != null) {
            if (PersianStorePayment.getInstance().mHelper != null)
                PersianStorePayment.getInstance().mHelper.dispose();
            PersianStorePayment.getInstance().mHelper = null;
        }

        listener = null;
    }

    private void reportsHandling() {
        int playCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.playCount));
        playCount++;
        PreferencesHelper.setString(PreferencesHelper.playCount, String.valueOf(playCount));
    }

    private void init() {
        ArrayList<String> levelsToRate = PreferencesHelper.getStringArrayPref(PreferencesHelper.levelsToRate);
        for (int i = 0; i < levelsToRate.size(); i++) {
            levelsToShowRate.add(levelsToRate.get(i));
        }
//        levelsToShowRate.add("3");
//        levelsToShowRate.add("18");
//        levelsToShowRate.add("33");

        menuBtn = findViewById(R.id.act_game_play_menu_btn);
        buyCoin = findViewById(R.id.act_game_play_coin_add);
        coinCountText = findViewById(R.id.act_game_play_coin_count);

        sponsorLogo = findViewById(R.id.act_game_play_sponsor_logo);

        dictionaryBtn = findViewById(R.id.act_game_play_dic_btn);
        skipBtn = findViewById(R.id.act_game_play_skip_btn);
        eyeBtn = findViewById(R.id.act_game_play_eye_btn);

        dictionaryText = findViewById(R.id.act_game_play_level_dic_text);
        Typeface IranSansLightTypeFace = Typeface.createFromAsset(GamePlayActivity.getInstance().getAssets(), "fonts/IRANSans_Light.ttf");
        dictionaryText.setTypeface(IranSansLightTypeFace);

        targetWord = findViewById(R.id.act_game_play_level_image);
        answerContainer = findViewById(R.id.act_game_play_answer_container);
        recyclerView = findViewById(R.id.act_game_play_recycler_view);
        recyclerViewParent = findViewById(R.id.act_game_play_recycler_parent);

        coinLayout = findViewById(R.id.act_game_play_coin_layout);

        parentFrame = findViewById(R.id.act_game_play_parent_frame);
        coinImage = findViewById(R.id.act_game_play_coin_image);

        levelImageContainer = findViewById(R.id.act_game_play_level_image_container);
    }

    private void setListeners() {

        listener = new PublicListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(menuBtn)) {
                    super.onClick(v);
                    onBackPressed();
                }
                if (v.equals(buyCoin)) {
                    super.onClick(v);
//                    if (!buyCoinDialog.isShowing())
//                        buyCoinDialog.show();
                    Dialogs.payment(GamePlayActivity.this, false, parentFrame).show();
                }
                if (v.equals(dictionaryBtn)) {
                    if (!usedDic) {
                        super.onClick(v);

                        // fireBaseAnalytics
                        String event = "Skill_fa";
                        Bundle bundle = new Bundle();
                        bundle.putString("Name", "Dictionary");
                        bundle.putString("Mode", "Coin");// felan advertise nadaraim pas faghat coin e
                        Application.firebaseAnalytics.logEvent(event, bundle);

//                        if (!dictionaryDialog.isShowing())
//                            dictionaryDialog.show();

                        Dialogs.useSkillDialog(GamePlayActivity.this, Dialogs.UseSkillTypes.Dictionary, new Dialogs.UsingSkillCallBack() {
                            @Override
                            public void call() {

                                usedDic = true;
                                dictionaryBtn.setBackgroundResource(R.drawable.btn_dic_disabled);

                                int dicUsedCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.dicUsedCount)); // chand bar kolan estefadeh karde
                                dicUsedCount++; // chand bar estefadeh karde + alan ke estefadeh kard
                                PreferencesHelper.setString(PreferencesHelper.dicUsedCount, String.valueOf(dicUsedCount)); // meghdar e jadid o mirizimesh to shared

                                switch (dicUsedCount) { // achievement ha ro check mikonim

                                    case 1:
                                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.dic_1.value)) {
                                            PreferencesHelper.setString(AchievementJson.Achievements.dic_1.value, "1");
                                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.dic_1.value)).show();
                                        }
                                        break;

                                    case 3:
                                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.dic_3.value)) {
                                            PreferencesHelper.setString(AchievementJson.Achievements.dic_3.value, "1");
                                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.dic_3.value)).show();
                                        }
                                        break;

                                    case 10:
                                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.dic_10.value)) {
                                            PreferencesHelper.setString(AchievementJson.Achievements.dic_10.value, "1");
                                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.dic_10.value)).show();
                                        }
                                        break;

                                }
                                helpSound();
                            }

                        }).show();
                    }
                }
                if (v.equals(eyeBtn)) {
                    super.onClick(v);

                    // fireBaseAnalytics
                    String event = "Skill_fa";
                    Bundle bundle = new Bundle();
                    bundle.putString("Name", "Eye");
                    bundle.putString("Mode", "Coin");// felan advertise nadaraim pas faghat coin e
                    Application.firebaseAnalytics.logEvent(event, bundle);

//                    if (!eyeDialog.isShowing())
//                        eyeDialog.show();

                    Dialogs.useSkillDialog(GamePlayActivity.this, Dialogs.UseSkillTypes.Eye, new Dialogs.UsingSkillCallBack() {
                        @Override
                        public void call() {
                            int eyeUsedCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.eyeUsedCount)); // chand bar kolan estefadeh karde
                            eyeUsedCount++; // chand bar estefadeh karde + alan ke estefadeh kard
                            PreferencesHelper.setString(PreferencesHelper.eyeUsedCount, String.valueOf(eyeUsedCount)); // meghdar e jadid o mirizimesh to shared

                            switch (eyeUsedCount) {
                                case 1:
                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.eye_1.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.eye_1.value, "1");
                                        Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.eye_1.value)).show();
                                    }
                                    break;

                                case 3:
                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.eye_3.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.eye_3.value, "1");
                                        Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.eye_3.value)).show();
                                    }
                                    break;

                                case 10:
                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.eye_10.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.eye_10.value, "1");
                                        Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.eye_10.value)).show();
                                    }
                                    break;
                            }
                            helpSound();
                        }
                    }).show();

                }
                if (v.equals(skipBtn)) {
                    super.onClick(v);

                    // fireBaseAnalytics
                    String event = "Skill_fa";
                    Bundle bundle = new Bundle();
                    bundle.putString("Name", "Skip");
                    bundle.putString("Mode", "Coin"); // felan advertise nadaraim pas faghat coin e
                    Application.firebaseAnalytics.logEvent(event, bundle);

//                    if (!skipDialog.isShowing()) {
//                        skipDialog.show();
//                    }

                    Dialogs.useSkillDialog(GamePlayActivity.this, Dialogs.UseSkillTypes.Skip, new Dialogs.UsingSkillCallBack() {
                        @Override
                        public void call() {
                            int skipUsedCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.skipUsedCount)); // chand bar kolan estefadeh karde
                            skipUsedCount++; // chand bar estefadeh karde + alan ke estefadeh kard
                            PreferencesHelper.setString(PreferencesHelper.skipUsedCount, String.valueOf(skipUsedCount)); // meghdar e jadid o mirizimesh to shared

                            switch (skipUsedCount) {
                                case 1:
                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.skip1.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.skip1.value, "1");
                                        Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.skip1.value)).show();
                                    }
                                    break;

                                case 3:
                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.skip3.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.skip3.value, "1");
                                        Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.skip3.value)).show();
                                    }
                                    break;

                                case 10:
                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.skip10.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.skip10.value, "1");
                                        Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.skip10.value)).show();
                                    }
                                    break;
                            }
                        }
                    }).show();

                }
                if (v.equals(sponsorLogo)) {
                    try {
                        Uri uri = Uri.parse(MainActivity.Last_Config_Created.getEvent().getSponsorLink());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception e) {

                    }

                }

            }

        };

        menuBtn.setOnClickListener(listener);
        buyCoin.setOnClickListener(listener);
        dictionaryBtn.setOnClickListener(listener);
        eyeBtn.setOnClickListener(listener);
        skipBtn.setOnClickListener(listener);
        sponsorLogo.setOnClickListener(listener);

        answerContainer.setOnTouchListener(new OnSwipeTouchListener(this) {

                                               @Override
                                               public void onSwipeRight() {
                                                   super.onSwipeRight();
                                                   recyclerView.setClickable(false);
                                                   swipeHandling(false);
                                                   recyclerView.setClickable(true);
                                               }

                                               @Override
                                               public void onSwipeLeft() {
                                                   super.onSwipeLeft();
                                                   recyclerView.setClickable(false);
                                                   swipeHandling(true);
                                                   recyclerView.setClickable(true);
                                               }

                                               @Override
                                               public void onSwipeTop() {
                                                   super.onSwipeTop();
                                               }

                                               @Override
                                               public void onSwipeBottom() {
                                                   super.onSwipeBottom();
                                               }
                                           }
        );

    }

    private void initCoins() {
        coinCountText.setText(PublicModules.toPersianDigit(Coin.getCoinNumber()));
    }

    private void helpSound() {

        final MediaPlayer help = MediaPlayer.create(this, R.raw.eye_dic);

        if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1"))
            help.start();

        help.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                help.release();
            }
        });

    }

    public void updateCoins(boolean increase, int updateCoin) {
        if (increase) {
            int coinCount = Integer.valueOf(coinCountText.getText().toString());
            coinCount += updateCoin;
            coinCountText.setText(PublicModules.toPersianDigit(String.valueOf(coinCount)));
        } else {
            int coinCount = Integer.valueOf(coinCountText.getText().toString());
            coinCount--;
            coinCountText.setText(PublicModules.toPersianDigit(String.valueOf(coinCount)));
        }
    }

    private void achievementTimers() {
        try {
            new CountDownTimer(9999999999999l, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    gamePlayPlayingTimer++;
                    int totalTimes = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.playTimeValue));
                    if (gamePlayPlayingTimer + totalTimes >= 3600) {
                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.playtime_1h.value)) {
                            PreferencesHelper.setString(AchievementJson.Achievements.playtime_1h.value, "1");
                            if (!GamePlayActivity.getInstance().isFinishing() && !activityPaused) {
                                runOnUiThread(() -> {
                                    Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.playtime_1h.value)).show();
                                });
                            }
                        }
                    } else if (gamePlayPlayingTimer + totalTimes >= 10800) {
                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.playtime_3h.value)) {
                            PreferencesHelper.setString(AchievementJson.Achievements.playtime_3h.value, "1");
                            if (!GamePlayActivity.getInstance().isFinishing() && !activityPaused) {
                                runOnUiThread(() -> {
                                    Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.playtime_3h.value)).show();
                                });
                            }
                        }
                    } else if (gamePlayPlayingTimer + totalTimes >= 36000) {
                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.playtime_10h.value)) {
                            PreferencesHelper.setString(AchievementJson.Achievements.playtime_10h.value, "1");
                            if (!GamePlayActivity.getInstance().isFinishing() && !activityPaused) {
                                runOnUiThread(() -> {
                                    Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.playtime_10h.value)).show();
                                });
                            }

                        }
                    }
                }

                @Override
                public void onFinish() {

                }
            }.start();
        } catch (WindowManager.BadTokenException e) {

        }

    }

    private void swipeHandling(boolean swipeRight) {

        for (int i = 0; i < answerContainer.getChildCount(); i++) {

            final FrameLayout tempFrame = (FrameLayout) answerContainer.getChildAt(i); // frame o get mikonim
            final AlphabetButton alphabetButton = (AlphabetButton) tempFrame.getChildAt(0); // alphabet e frame ro get mikonim

            // animate timing
            int startDelay = swipeRight ? 160 * i : (selectedButtonsValues.size() - i) * 160;

            if (alphabetButton.getResourceMode() == ShowAlphabet) {
                final int i2 = i; // temp i for using in below blocks

                // creating new alphabetButton for placeHolders
                final AlphabetButton newAlphabetBtn = new AlphabetButton(GamePlayActivity.this, i, QuestionMark);
                newAlphabetBtn.setOnClickListener(new PublicListener() {
                    @Override
                    public void onClick(View v) {
                        if (newAlphabetBtn.getResourceMode() != QuestionMark) {
                            super.onClick(v);
                            removeAlphabet(newAlphabetBtn, i2);
                        }
                    }
                });
                tempFrame.addView(newAlphabetBtn, 0);

                // removing alphabetButton with animation
                alphabetButton.animate().setDuration(250).translationY(100f).alpha(0).setStartDelay(startDelay).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        removeAlphabet(alphabetButton, i2);
                        tempFrame.removeView(alphabetButton);
                    }
                });
            }


        }
    }

//    private void initDialogs() {
//
//        dictionaryDialog = Dialogs.useSkillDialog(GamePlayActivity.this, Dialogs.UseSkillTypes.Dictionary, new Dialogs.UsingSkillCallBack() {
//            @Override
//            public void call() {
//
//                usedDic = true;
//                dictionaryBtn.setBackgroundResource(R.drawable.btn_dic_disabled);
//
//                int dicUsedCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.dicUsedCount)); // chand bar kolan estefadeh karde
//                dicUsedCount++; // chand bar estefadeh karde + alan ke estefadeh kard
//                PreferencesHelper.setString(PreferencesHelper.dicUsedCount, String.valueOf(dicUsedCount)); // meghdar e jadid o mirizimesh to shared
//
//                switch (dicUsedCount) { // achievement ha ro check mikonim
//
//                    case 1:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.dic_1.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.dic_1.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.dic_1.value)).show();
//                        }
//                        break;
//
//                    case 3:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.dic_3.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.dic_3.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.dic_3.value)).show();
//                        }
//                        break;
//
//                    case 10:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.dic_10.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.dic_10.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.dic_10.value)).show();
//                        }
//                        break;
//
//                }
//                helpSound();
//            }
//
//        });
//
//        eyeDialog = Dialogs.useSkillDialog(GamePlayActivity.this, Dialogs.UseSkillTypes.Eye, new Dialogs.UsingSkillCallBack() {
//            @Override
//            public void call() {
//                int eyeUsedCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.eyeUsedCount)); // chand bar kolan estefadeh karde
//                eyeUsedCount++; // chand bar estefadeh karde + alan ke estefadeh kard
//                PreferencesHelper.setString(PreferencesHelper.eyeUsedCount, String.valueOf(eyeUsedCount)); // meghdar e jadid o mirizimesh to shared
//
//                switch (eyeUsedCount) {
//                    case 1:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.eye_1.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.eye_1.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.eye_1.value)).show();
//                        }
//                        break;
//
//                    case 3:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.eye_3.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.eye_3.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.eye_3.value)).show();
//                        }
//                        break;
//
//                    case 10:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.eye_10.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.eye_10.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.eye_10.value)).show();
//                        }
//                        break;
//                }
//                helpSound();
//            }
//        });
//
//        skipDialog = Dialogs.useSkillDialog(GamePlayActivity.this, Dialogs.UseSkillTypes.Skip, new Dialogs.UsingSkillCallBack() {
//            @Override
//            public void call() {
//                int skipUsedCount = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.skipUsedCount)); // chand bar kolan estefadeh karde
//                skipUsedCount++; // chand bar estefadeh karde + alan ke estefadeh kard
//                PreferencesHelper.setString(PreferencesHelper.skipUsedCount, String.valueOf(skipUsedCount)); // meghdar e jadid o mirizimesh to shared
//
//                switch (skipUsedCount) {
//                    case 1:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.skip1.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.skip1.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.skip1.value)).show();
//                        }
//                        break;
//
//                    case 3:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.skip3.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.skip3.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.skip3.value)).show();
//                        }
//                        break;
//
//                    case 10:
//                        if (Achievement.achievementIsLocked(AchievementJson.Achievements.skip10.value)) {
//                            PreferencesHelper.setString(AchievementJson.Achievements.skip10.value, "1");
//                            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, AchievementJson.achievements.get(AchievementJson.Achievements.skip10.value)).show();
//                        }
//                        break;
//                }
//            }
//        });
//
//        buyCoinDialog = Dialogs.payment(GamePlayActivity.this, false, parentFrame);
//
//    }

    private void guidBtnAnimation() {

        new CountDownTimer(1575898659985l, 20000) {
            int counter = 0;

            @Override
            public void onTick(long millisUntilFinished) {
                counter++;
                if (counter >= 3) { // serie aval bad e 40 sanie animate mishe . bad az oon har 20 sanie ye bar animate mishe .
                    eyeBtn.animate().setDuration(110).translationY(-30).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            eyeBtn.animate().setDuration(110).translationY(0);
                            skipBtn.animate().setDuration(110).translationY(-30).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    skipBtn.animate().setDuration(110).translationY(0);
                                    dictionaryBtn.animate().setDuration(110).translationY(-30).withEndAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            dictionaryBtn.animate().setDuration(110).translationY(0);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }

            }

            @Override
            public void onFinish() {

            }
        }.start();


    }

    private void initWordPicture(int levelID) {
        String fileName = "lvl_" + levelID;
        Resources resources = this.getResources();
        targetWord.setImageResource(resources.getIdentifier(fileName, "drawable", this.getPackageName()));
    }

    private void initDictionaryText(String dicText) {
        dictionaryText.setText(dicText);
    }

    private void initAnswerContainer(String targetWord) {
        for (int i = 0; i < targetWord.toCharArray().length; i++) {

            FrameLayout tempFrame = new FrameLayout(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(3, 3, 3, 3);
            tempFrame.setLayoutParams(layoutParams);
            tempFrame.setClickable(true);

            final AlphabetButton placeHolder;
            if (String.valueOf(targetWord.toCharArray()[i]).equals(" ")) {

                tempFrame.setClickable(false);
                tempFrame.setAlpha(0);
                placeHolder = new AlphabetButton(this, -1, AlphabetButton.AlphabetButtonResourceMode.WhiteSpace);

            } else {

                final int i2 = i; // temp i for using in below blocks

                placeHolder = new AlphabetButton(this, -1, QuestionMark);
                placeHolder.setOnClickListener(new PublicListener() {
                    @Override
                    public void onClick(View v) {
                        if (placeHolder.getResourceMode() != QuestionMark) {
                            super.onClick(v);
                            removeAlphabet(placeHolder, i2);
                        }
                    }
                });

            }

            tempFrame.addView(placeHolder);
            answerContainer.addView(tempFrame);

        }
    }

    private void initRecyclerView(String targetWord) {
        adapter = new AlphabetButtonsAdapter(this, targetWord);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 6));
        recyclerView.setAdapter(adapter);
    }

    public void selectAlphabet(AlphabetButton selectedAlphabetButton, int adapterPosition,
                               int startAnimationDelay) {

        for (int i = 0; i < answerContainer.getChildCount(); i++) {

            FrameLayout tempFrame = (FrameLayout) answerContainer.getChildAt(i);// frame o get mikonim
            AlphabetButton alphabetButton = (AlphabetButton) tempFrame.getChildAt(0); // alphabet e frame ro get mikonim
//            Log.i("CrashTest", "i = " + String.valueOf(i));
//            Log.i("CrashTest", "alphabetID = " + AlphabetButton.getAlphabetValue(alphabetButton.getAlphabetId()));

            if (alphabetButton.getResourceMode() == QuestionMark) {
                // jahay khali
                alphabetButton.setAdapterPosition(adapterPosition);
                alphabetButton.setShowAlphabet(this, ShowAlphabet, selectedAlphabetButton.getAlphabetId());
                selectAlphabetAnimation(alphabetButton, startAnimationDelay, true);

                for (int j = 0; j < selectedButtonsValues.size(); j++) {
                    if (selectedButtonsValues.get(j).getValue().equals("#")) {
                        selectedButtonsValues.get(j).setValue(AlphabetButton.getAlphabetValue(alphabetButton.getAlphabetId()));
                        selectedButtonsValues.get(j).setId(i);
                        break;
                    }
                }

                // horuf e alefba
                selectedAlphabetButton.setShowAlphabet(this, AlphabetButton.AlphabetButtonResourceMode.WordCut, -1);
                selectedAlphabetButton.setTag("Selected");
                AlphabetButtonsAdapter.getInstance().notifyDataSetChanged();

                if (isLast()) {
                    if (userAnswerIsValid()) {

                        int coinCount = Level.getLastLevelOfSeason(level.getSeasonId(), MainActivity.Last_Config_Created) == level.getId() ? 30 : 3;

                        if (usedSkip || isPassedBefore()) { //check mikonim age skip karde bud ya ghablan marhalaro rad karde bud coin nade
                            finishLevel();
                        } else {
                            Coin.addCoin(GamePlayActivity.this, parentFrame, coinCount, startPoint, middlePoint, gamePlayCoinImagePoint, new Coin.CoinAddedCallBack() {
                                @Override
                                public void call() {
                                    finishLevel();
                                }
                            });
                        }

                    } else {
                        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
                        answerContainer.startAnimation(shake);

                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(500);

                        int wrongAnswer = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.wrongAnswer));
                        wrongAnswer++;
                        PreferencesHelper.setString(PreferencesHelper.wrongAnswer, String.valueOf(wrongAnswer));

                    }
                }

                break;
            }

        }
    }

    private boolean isLast() {
        for (UserAnswerStrings userAnswerStrings : selectedButtonsValues) {
            if (userAnswerStrings.getValue().equals("#"))
                return false;
        }
        return true;
    }

    private void removeAlphabet(AlphabetButton alphabetPlaceHolderButton,
                                int answerContainerIndex) {

        oneShotCount = false; // for oneShotCount achievement

        // horuf e alefba

        if (alphabetPlaceHolderButton.getAlphabetId() != -1) {

            try {
                AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(alphabetPlaceHolderButton.getAdapterPosition()).setShowAlphabet(this, ShowAlphabet, alphabetPlaceHolderButton.getAlphabetId());
                AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(alphabetPlaceHolderButton.getAdapterPosition()).setTag("Removed");
                AlphabetButtonsAdapter.getInstance().notifyDataSetChanged();
            } catch (Exception e) {
                GamePlayActivity.getInstance().recreate();
            }

            // jahay khali
            for (int i = 0; i < selectedButtonsValues.size(); i++) {
                if (selectedButtonsValues.get(i).getValue().equals(AlphabetButton.getAlphabetValue(alphabetPlaceHolderButton.getAlphabetId()))
                        && selectedButtonsValues.get(i).getId() == answerContainerIndex) {
                    selectedButtonsValues.get(i).setValue("#");
                    selectedButtonsValues.get(i).setId(-1);
                    break;
                }

            }

            alphabetPlaceHolderButton.setShowAlphabet(this, QuestionMark, -1);
            alphabetPlaceHolderButton.setAdapterPosition(0);
        }

    }

    public void selectAlphabetAnimation(final AlphabetButton alphabetButton, int startDelay, boolean isPlaceHolder) {

        final float targetXAndY = isPlaceHolder ? 1.2f : 1.6f;

        ScaleAnimation firstScaleAnimation = new ScaleAnimation(0f, targetXAndY, 0f, targetXAndY, 50, 50);
        firstScaleAnimation.setDuration(100);
        if (startDelay != 0)
            firstScaleAnimation.setStartOffset(startDelay);
        firstScaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {

            }

            public void onAnimationEnd(Animation animation) {

                ScaleAnimation secondScaleAnimation = new ScaleAnimation(targetXAndY, 1.0f, targetXAndY, 1.0f, 50, 50);
                secondScaleAnimation.setDuration(80);
                AnimationSet animationSet = new AnimationSet(true);
                animationSet.addAnimation(secondScaleAnimation);
                alphabetButton.startAnimation(animationSet);
            }

            public void onAnimationRepeat(Animation animation) {

            }
        });
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(firstScaleAnimation);
        alphabetButton.startAnimation(animationSet);

    }

    private boolean userAnswerIsValid() {
        String correctAnswer = level.getAnswer().replace(" ", "");

        String userAnswer = "";
        for (int i = 0; i < selectedButtonsValues.size(); i++) {
            userAnswer = userAnswer.concat(selectedButtonsValues.get(i).getValue());
        }
        Log.i("ConcatTest", userAnswer);

        return userAnswer.equals(correctAnswer);
    }

    private void finishLevel() {

        int fastestRecord = Integer.parseInt(PreferencesHelper.getString(PreferencesHelper.fastestComplete));
        if (level.getId() == 1 && PreferencesHelper.getStringArrayPref(PreferencesHelper.unLockedLevels).size() == 1) { // age bar e aval bud ke har time i bud mirizim tu share
            PreferencesHelper.setString(PreferencesHelper.fastestComplete, String.valueOf(gamePlayPlayingTimer));
            Log.i("fastestRecord", "level1 " + PreferencesHelper.getString(PreferencesHelper.fastestComplete));
        } else {
            if (fastestRecord > gamePlayPlayingTimer) { // age na ke check mikonim aya zaman e in dafe az zaman e ghabli kamtar bud ya na . age bud ke savesh mikonim age na hichi
                PreferencesHelper.setString(PreferencesHelper.fastestComplete, String.valueOf(gamePlayPlayingTimer));
                Log.i("fastestRecord", "Nope! " + PreferencesHelper.getString(PreferencesHelper.fastestComplete));
            }
        }

        if (MainActivity.Last_Config_Created.hasEvent()) {

            //level pass shode jadid
            ArrayList<String> passedLevels = PreferencesHelper.getStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.passedLevelsKey);
            if (!passedLevels.contains(String.valueOf(level.getId()))) // age ghablan in level o rad nakarde bud , bia dobare beriz tush
                passedLevels.add(String.valueOf(level.getId()));
            PreferencesHelper.setStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.passedLevelsKey, passedLevels);

            // level unlock shode jadid
            ArrayList<String> unlockedLevels = PreferencesHelper.getStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.unLockedLevels);
            unlockedLevels.add(String.valueOf(level.getId() + 1));
            PreferencesHelper.setStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.unLockedLevels, unlockedLevels);

            // sending data to backendless
            if (!PreferencesHelper.getEventSettings(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.eventUserName).equals("")) {

                String eventName = MainActivity.Last_Config_Created.getEvent().getName();

                ArrayList<String> ids = PreferencesHelper.getStringArrayPrefEvent(eventName, PreferencesHelper.passedLevelsKey);
                int newScore = Integer.parseInt(ids.get(ids.size() - 1));
                PreferencesHelper.setEventSettings(eventName, PreferencesHelper.eventUserScore, String.valueOf(newScore));

                Application.backendlessHandling(eventName, new AsyncCallback() {
                    @Override
                    public void handleResponse(Object response) {
                        PreferencesHelper.setEventSettings(eventName, PreferencesHelper.eventUserPendingData, "0"); // yani movafagh bud va datay pending nadare
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        PreferencesHelper.setEventSettings(eventName, PreferencesHelper.eventUserPendingData, "1");// yani na movafagh bud va bayad har vaght net dashtim dobare befrestim
                    }
                });
            }


        } else {
            //level pass shode jadid
            ArrayList<String> passedLevels = PreferencesHelper.getStringArrayPref(PreferencesHelper.passedLevelsKey);
            passedLevels.add(String.valueOf(level.getId()));
            PreferencesHelper.setStringArrayPref(PreferencesHelper.passedLevelsKey, passedLevels);

            // level unlock shode jadid
            ArrayList<String> unlockedLevels = PreferencesHelper.getStringArrayPref(unLockedLevels);
            unlockedLevels.add(String.valueOf(level.getId() + 1));
            PreferencesHelper.setStringArrayPref(unLockedLevels, unlockedLevels);
        }

        checkAdvertise(TX_Ad.AdTypes.interestitial);

        boolean isLastLevelOfSeason = Level.getLastLevelOfSeason(level.getSeasonId(), MainActivity.Last_Config_Created) == level.getId() ? true : false;
        boolean hasNextSeason = Season.hasNextSeason(level.getSeasonId() + 1, MainActivity.Last_Config_Created);
        levelCompeletedDialog = Dialogs.levelCompletedDialog(GamePlayActivity.this, level, isLastLevelOfSeason, hasNextSeason);
        levelCompeletedDialog.show();
        levelCompeletedDialog.setCancelable(false);

        if (isLastLevelOfSeason) { // age marhale akhare season hastesh
            if (Season.hasNextSeason(level.getSeasonId() + 1, MainActivity.Last_Config_Created)) { // va age season e badi vojud dare
                if (MainActivity.Last_Config_Created.hasEvent()) {
                    ArrayList<String> unlockedSeasons = PreferencesHelper.getStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.unLockedSeasons); // bia bazesh kon
                    unlockedSeasons.add(String.valueOf(level.getSeasonId() + 1));
                    PreferencesHelper.setStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.unLockedSeasons, unlockedSeasons);
                } else {
                    ArrayList<String> unlockedSeasons = PreferencesHelper.getStringArrayPref(PreferencesHelper.unLockedSeasons); // bia bazesh kon
                    unlockedSeasons.add(String.valueOf(level.getSeasonId() + 1));
                    PreferencesHelper.setStringArrayPref(PreferencesHelper.unLockedSeasons, unlockedSeasons);
                }

                MainActivity.getInstance().goNextSeason(level.getSeasonId());
            }
        }
        MainActivity.getInstance().levelsAdapter.notifyDataSetChanged();

        Achievement unlockedAchievement = newUnlockedAchievement();
        if (unlockedAchievement != null) {
            Dialogs.achievementUnlockedDialog(GamePlayActivity.this, unlockedAchievement).show();
        }

        // fireBaseAnalytics
        String event = "Levels_fa";
        Bundle bundle = new Bundle();
        bundle.putString("LevelID", String.valueOf(level.getId()));
        Application.firebaseAnalytics.logEvent(event, bundle);

    }

    private void levelSound() {

        if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")) {
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.level_start_2);
            mediaPlayer.start();
        }

    }

    public void eyeSkill(boolean decreaseCoin) {

        for (int i = 0; i < answerContainer.getChildCount(); i++) {

            FrameLayout tempFrame = (FrameLayout) answerContainer.getChildAt(i); // frame o get mikonim
            AlphabetButton alphabetButton = (AlphabetButton) tempFrame.getChildAt(0); // alphabet e frame ro get mikonim

            if (alphabetButton.getResourceMode() == ShowAlphabet) {
                if (!AlphabetButton.getAlphabetValue(alphabetButton.getAlphabetId()).equals(String.valueOf(level.getAnswer().charAt(i)))) {
                    removeAlphabet(alphabetButton, i);
                }
            }

            if (alphabetButton.getResourceMode() == QuestionMark) {
                for (int j = 0; j < AlphabetButtonsAdapter.getInstance().alphabetButtonArr.size(); j++) {
                    if (AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(j).getResourceMode() == ShowAlphabet) {
                        if (AlphabetButton.getAlphabetValue(AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(j).getAlphabetId()).equals(String.valueOf(level.getAnswer().charAt(i)))) {
                            selectAlphabet(AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(j), j, 0);
                            break;
                        }
                    }
                }
                break;
            }

        }

        if (decreaseCoin) {
            Coin.decreaseCoins(10);
        }

    }

    public void skipSkill(boolean decreaseCoin) {
        usedSkip = true;
        if (decreaseCoin) {
            Coin.decreaseCoins(50);
        }
        autoCompleteLevel();
    }

    public void dictionarySkill(boolean decreaseCoin) {
        if (decreaseCoin) {
            Coin.decreaseCoins(20);
        }

        dictionaryText.animate().alpha(1).setDuration(700);
    }

    private void autoCompleteLevel() {

        String correctAnswer = level.getAnswer().replace(" ", "");

        ArrayList<Character> correctAnswerChars = new ArrayList<>();
        for (int i = 0; i < correctAnswer.length(); i++) {
            correctAnswerChars.add(correctAnswer.toCharArray()[i]);
        }

        for (int i = 0; i < answerContainer.getChildCount(); i++) {
            FrameLayout tempFrame = (FrameLayout) answerContainer.getChildAt(i); // frame o get mikonim
            AlphabetButton alphabetButton = (AlphabetButton) tempFrame.getChildAt(0); // alphabet e frame ro get mikonim

            if (alphabetButton.getResourceMode() == ShowAlphabet) {
                if (!AlphabetButton.getAlphabetValue(alphabetButton.getAlphabetId()).equals(String.valueOf(level.getAnswer().charAt(i)))) {
                    removeAlphabet(alphabetButton, i);
                }
            }
        }

        for (int i = 0; i < correctAnswerChars.size(); i++) {

            for (int j = 0; j < adapter.alphabetButtonArr.size(); j++) {

                if (adapter.alphabetButtonArr.get(j).getResourceMode() == ShowAlphabet) {

                    if (AlphabetButton.getAlphabetValue(adapter.alphabetButtonArr.get(j).getAlphabetId()).equals(String.valueOf(correctAnswerChars.get(i)))) {
                        if (adapter.alphabetButtonArr.get(j).getAlphabetId() != -1) {
                            if (!selectedButtonsValues.get(i).getValue().equals(AlphabetButton.getAlphabetValue(adapter.alphabetButtonArr.get(j).getAlphabetId())))
                                selectAlphabet(adapter.alphabetButtonArr.get(j), j, 100 * i);
                        } else {
                            continue;
                        }
                        break;
                    }

                }

            }
        }

    }

    private boolean isPassedBefore() {
        ArrayList<String> passedIDs;
        if (MainActivity.getInstance().Last_Config_Created.hasEvent()) {
            passedIDs = PreferencesHelper.getStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.passedLevelsKey);
        } else {
            passedIDs = PreferencesHelper.getStringArrayPref(PreferencesHelper.passedLevelsKey);
        }
        return passedIDs.contains(String.valueOf(level.getId()));
    }

    private void checkAdvertise(TX_Ad.AdTypes adType) {

        if (MainActivity.adIsActive) {
            int lastLevelID = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.lastLevelAd));
// ta level 3 nabayad ad neshun bede            // az akharin bari ke ad neshun dade, hadeaghal 1 level jelotar bashe      // ba chance 50%
            if (level.getId() > 3 && (level.getId() - lastLevelID > 1 || level.getId() < lastLevelID) && PublicModules.getBooleanWithChance(0.5)) {
                TX_Ad.showAd(this, adType, Application.deviceID, "android", complete -> {
                    PreferencesHelper.setString(PreferencesHelper.lastLevelAd, String.valueOf(level.getId()));
                });
            }
        }

    }

    private Achievement newUnlockedAchievement() {

        switch (level.getId()) {

            case 1:
                if (Achievement.achievementIsLocked(AchievementJson.Achievements.levels_1.value)) {
                    PreferencesHelper.setString(AchievementJson.Achievements.levels_1.value, "1");
                    return AchievementJson.achievements.get(AchievementJson.Achievements.levels_1.value);
                } else
                    break;

            case 5:
                if (Achievement.achievementIsLocked(AchievementJson.Achievements.levels_5.value)) {
                    PreferencesHelper.setString(AchievementJson.Achievements.levels_5.value, "1");
                    return AchievementJson.achievements.get(AchievementJson.Achievements.levels_5.value);
                } else
                    break;

            case 20:
                if (Achievement.achievementIsLocked(AchievementJson.Achievements.levels_20.value)) {
                    PreferencesHelper.setString(AchievementJson.Achievements.levels_20.value, "1");
                    return AchievementJson.achievements.get(AchievementJson.Achievements.levels_20.value);
                } else
                    break;

            case 50:
                if (Achievement.achievementIsLocked(AchievementJson.Achievements.levels_50.value)) {
                    PreferencesHelper.setString(AchievementJson.Achievements.levels_50.value, "1");
                    return AchievementJson.achievements.get(AchievementJson.Achievements.levels_50.value);
                } else
                    break;

            case 100:
                if (Achievement.achievementIsLocked(AchievementJson.Achievements.levels_100.value)) {
                    PreferencesHelper.setString(AchievementJson.Achievements.levels_100.value, "1");
                    return AchievementJson.achievements.get(AchievementJson.Achievements.levels_100.value);
                } else
                    break;

            case 200:
                if (Achievement.achievementIsLocked(AchievementJson.Achievements.levels_200.value)) {
                    PreferencesHelper.setString(AchievementJson.Achievements.levels_200.value, "1");
                    return AchievementJson.achievements.get(AchievementJson.Achievements.levels_200.value);
                } else
                    break;

        }

        if (gamePlayPlayingTimer <= 5) {
            if (Achievement.achievementIsLocked(AchievementJson.Achievements.record_5s.value)) {
                PreferencesHelper.setString(AchievementJson.Achievements.record_5s.value, "1");
                return AchievementJson.achievements.get(AchievementJson.Achievements.record_5s.value);
            }
        } else if (gamePlayPlayingTimer > 6 && gamePlayPlayingTimer <= 10) {
            if (Achievement.achievementIsLocked(AchievementJson.Achievements.record_10s.value)) {
                PreferencesHelper.setString(AchievementJson.Achievements.record_10s.value, "1");
                return AchievementJson.achievements.get(AchievementJson.Achievements.record_10s.value);
            }
        } else if (gamePlayPlayingTimer > 10 && gamePlayPlayingTimer <= 30) {
            if (Achievement.achievementIsLocked(AchievementJson.Achievements.record_30s.value)) {
                PreferencesHelper.setString(AchievementJson.Achievements.record_30s.value, "1");
                return AchievementJson.achievements.get(AchievementJson.Achievements.record_30s.value);
            }
        }

        if (oneShotCount) {
            int oneShotTime = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.oneShotCount));
            oneShotTime++;
            PreferencesHelper.setString(PreferencesHelper.oneShotCount, String.valueOf(oneShotTime));

            switch (oneShotTime) {
                case 1:
                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.oneshot_1.value)) {
                        PreferencesHelper.setString(AchievementJson.Achievements.oneshot_1.value, "1");
                        return AchievementJson.achievements.get(AchievementJson.Achievements.oneshot_1.value);
                    } else
                        break;

                case 10:
                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.oneshot_10.value)) {
                        PreferencesHelper.setString(AchievementJson.Achievements.oneshot_10.value, "1");
                        return AchievementJson.achievements.get(AchievementJson.Achievements.oneshot_10.value);
                    } else
                        break;

                case 20:
                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.oneshot_20.value)) {
                        PreferencesHelper.setString(AchievementJson.Achievements.oneshot_20.value, "1");
                        return AchievementJson.achievements.get(AchievementJson.Achievements.oneshot_20.value);
                    } else
                        break;

                case 50:
                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.oneshot_50.value)) {
                        PreferencesHelper.setString(AchievementJson.Achievements.oneshot_50.value, "1");
                        return AchievementJson.achievements.get(AchievementJson.Achievements.oneshot_50.value);
                    } else
                        break;
            }
        }

        if (level.getAnswer().equals("نیم کلمه")) {
            if (Achievement.achievementIsLocked(AchievementJson.Achievements.masterWord.value)) {
                PreferencesHelper.setString(AchievementJson.Achievements.masterWord.value, "1");
                return AchievementJson.achievements.get(AchievementJson.Achievements.masterWord.value);
            }
        }

        return null;
    }

    private void gamePlayTutorial(final Context context) {
        PreferencesHelper.setString(PreferencesHelper.gamePlayLevelOneFirstTime, "1");
        levelImageContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                levelImageContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int[] points = new int[2];
                ArrayList<SimpleTarget> targets = noneAlphabetsTargets(context);
                ArrayList<Integer> buttonsIndexArr = new ArrayList<>();
                String[] buttonsTitleArr = {"بیا با هم جواب بدیم...",
                        "حالا اینو بزن!",
                        "حالا این!",
                        "خوبه",
                        "حالا این یکی!",
                        "خوبه!",
                        "حالا این",
                        "و این",
                        "و در آخر این..."};
                noneAlphabetsTargets(context);

                for (int i = 0; i < level.getAnswer().length(); i++) {
                    for (int j = 0; j < AlphabetButtonsAdapter.getInstance().alphabetButtonArr.size(); j++) {
                        if (AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(j).getResourceMode() == ShowAlphabet) {
                            if (AlphabetButton.getAlphabetValue(AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(j).getAlphabetId()).equals(String.valueOf(level.getAnswer().charAt(i)))) {
                                if (!buttonsIndexArr.contains(j)) {
                                    buttonsIndexArr.add(j);
                                    letter = AlphabetButtonsAdapter.getInstance().alphabetButtonArr.get(j);
                                    recyclerView.findViewHolderForAdapterPosition(j).itemView.getLocationOnScreen(points);
                                    letterPoint = new PointF(points[0] + ((recyclerView.findViewHolderForAdapterPosition(j).itemView.getWidth() / 2) + context.getResources().getDimension(R.dimen.itemAlphabetButtonMargin)), points[1] + ((recyclerView.findViewHolderForAdapterPosition(j).itemView.getHeight() / 2) + context.getResources().getDimension(R.dimen.itemAlphabetButtonMargin)));
                                    targets.add(letterTarget(letter, j, letterPoint, buttonsTitleArr[i]));
                                    break;
                                }
                            }
                        }
                    }
                }


                Spotlight.with(GamePlayActivity.this)
                        .setOverlayColor(R.color.blackTest)
                        .setTargets(targets)
                        .setClosedOnTouchedOutside(true)
                        .setOnSpotlightStateListener(new OnSpotlightStateChangedListener() {
                            @Override
                            public void onStarted() {
                                // do nothing
                            }

                            @Override
                            public void onEnded() {
                                // do nothing
                            }
                        }).start();

            }
        });

    }

    private void skillsTutorial(final Context context) {
        PreferencesHelper.setString(PreferencesHelper.gamePlayLevelTwoFirstTime, "1");
        coinLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                coinLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                ArrayList<SimpleTarget> targets = skillsTarget(context);

                Spotlight.with(GamePlayActivity.this)
                        .setOverlayColor(R.color.blackTest)
                        .setTargets(targets)
                        .setClosedOnTouchedOutside(true)
                        .setOnSpotlightStateListener(new OnSpotlightStateChangedListener() {
                            @Override
                            public void onStarted() {
                                // do nothing
                            }

                            @Override
                            public void onEnded() {
                                // do nothing
                            }
                        }).start();
            }
        });
    }

    private ArrayList<SimpleTarget> noneAlphabetsTargets(final Context context) {

        ArrayList<SimpleTarget> targets = new ArrayList<>();

        SimpleTarget levelWordTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(levelImageContainer)
                .setShape(new Shape() {
                    @Override
                    public void draw(Canvas canvas, PointF point, float value, Paint paint) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            canvas.drawRoundRect(levelImageContainer.getLeft() + context.getResources().getDimension(R.dimen.gamePlayImageSpotlightHorizontalOffset), levelImageContainer.getTop() + context.getResources().getDimension(R.dimen.gamePlayImageSpotlightVerticalOffset), levelImageContainer.getRight() - context.getResources().getDimension(R.dimen.gamePlayImageSpotlightHorizontalOffset), levelImageContainer.getBottom() + context.getResources().getDimension(R.dimen.gamePlayImageSpotlightVerticalOffset), point.x, point.y, paint);
                        } else {
                            canvas.drawRect(levelImageContainer.getLeft() + context.getResources().getDimension(R.dimen.gamePlayImageSpotlightHorizontalOffset), levelImageContainer.getTop() + context.getResources().getDimension(R.dimen.gamePlayImageSpotlightVerticalOffset), levelImageContainer.getRight() - context.getResources().getDimension(R.dimen.gamePlayImageSpotlightHorizontalOffset), levelImageContainer.getBottom() + context.getResources().getDimension(R.dimen.gamePlayImageSpotlightVerticalOffset), paint);
                        }
                    }

                    @Override
                    public int getHeight() {
                        return levelImageContainer.getHeight();
                    }

                    @Override
                    public int getWidth() {
                        return levelImageContainer.getWidth();
                    }
                })
                .setTitle("کلمه ای که باید حدس بزنی!")
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        // do nothing
                    }
                })
                .setDuration(0)
                .build();

        targets.add(levelWordTarget);

        SimpleTarget buttonContainerTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(recyclerViewParent)
                .setShape(new Shape() {
                    @Override
                    public void draw(Canvas canvas, PointF point, float value, Paint paint) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            canvas.drawRoundRect(recyclerViewParent.getLeft() + context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightHorizontalOffset), recyclerViewParent.getTop() + context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightVerticalOffset), recyclerViewParent.getRight() - context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightHorizontalOffset), recyclerViewParent.getBottom() + context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightVerticalOffset), point.x, point.y, paint);
                        } else {
                            canvas.drawRect(recyclerViewParent.getLeft() + context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightHorizontalOffset), recyclerViewParent.getTop() + context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightVerticalOffset), recyclerViewParent.getRight() - context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightHorizontalOffset), recyclerViewParent.getBottom() + context.getResources().getDimension(R.dimen.gamePlayButtonsSpotlightVerticalOffset), paint);
                        }
                    }

                    @Override
                    public int getHeight() {
                        return recyclerViewParent.getHeight();
                    }

                    @Override
                    public int getWidth() {
                        return recyclerViewParent.getWidth();
                    }
                })
                .setTitle("از این حروف استفاده کن...")
                .setDuration(0)
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        // do nothing
                    }
                })
                .build();

        targets.add(buttonContainerTarget);

        SimpleTarget answerContainerTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(answerContainer)
                .setShape(new Shape() {
                    @Override
                    public void draw(Canvas canvas, PointF point, float value, Paint paint) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            canvas.drawRoundRect(answerContainer.getLeft() + context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightHorizontalOffset), answerContainer.getTop() + context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightVerticalOffset), answerContainer.getRight() - context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightHorizontalOffset), answerContainer.getBottom() + context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightVerticalOffset), point.x, point.y, paint);
                        } else {
                            canvas.drawRect(answerContainer.getLeft() + context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightHorizontalOffset), answerContainer.getTop() + context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightVerticalOffset), answerContainer.getRight() - context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightHorizontalOffset), answerContainer.getBottom() + context.getResources().getDimension(R.dimen.gamePlayAnswerSpotlightVerticalOffset), paint);
                        }
                    }

                    @Override
                    public int getHeight() {
                        return answerContainer.getHeight();
                    }

                    @Override
                    public int getWidth() {
                        return answerContainer.getWidth();
                    }
                })
                .setTitle("جواب رو اینجا بنویس")
                .setDuration(0)
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        // do nothing
                    }
                })
                .build();

        targets.add(answerContainerTarget);

        return targets;
    }

    private ArrayList<SimpleTarget> skillsTarget(final Context context) {

        ArrayList<SimpleTarget> targets = new ArrayList<>();

        SimpleTarget coinLayoutTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(coinLayout)
                .setShape(new Shape() {
                    @Override
                    public void draw(Canvas canvas, PointF point, float value, Paint paint) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            canvas.drawRoundRect(coinLayout.getLeft() - context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightHorizontalOffset), coinLayout.getTop() + context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightVerticalOffset), coinLayout.getRight() + context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightHorizontalOffset), coinLayout.getBottom() + context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightVerticalOffset), point.x, point.y, paint);
                        } else {
                            canvas.drawRect(coinLayout.getLeft() - context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightHorizontalOffset), coinLayout.getTop() + context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightVerticalOffset), coinLayout.getRight() + context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightHorizontalOffset), coinLayout.getBottom() + context.getResources().getDimension(R.dimen.gamePlayCoinsLayoutSpotlightVerticalOffset), paint);
                        }
                    }

                    @Override
                    public int getHeight() {
                        return coinLayout.getHeight();
                    }

                    @Override
                    public int getWidth() {
                        return coinLayout.getWidth();
                    }
                })
                .setTitle("اینجا تعداد سکه هات نمایش داده میشه!")
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        // do nothing
                    }
                })
                .setDuration(0)
                .build();

        targets.add(coinLayoutTarget);

        SimpleTarget eyeSkillTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(eyeBtn)
                .setShape(new Circle(eyeBtn.getWidth()))
                .setTitle("با این میتونی یک حرف رو ببینی یا حرف غلط رو اصلاح کنی!")
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        // do nothing
                    }
                })
                .setDuration(100)
                .build();

        targets.add(eyeSkillTarget);

        SimpleTarget dicSkillTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(dictionaryBtn)
                .setShape(new Circle(dictionaryBtn.getWidth()))
                .setTitle("از معنی کلمه میتونی برای حدس زدنش استفاده کنی!")
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        // do nothing
                    }
                })
                .setDuration(100)
                .build();

        targets.add(dicSkillTarget);

        SimpleTarget skipSkillTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(skipBtn)
                .setShape(new Circle(skipBtn.getWidth()))
                .setTitle("با این میتونی مرحله رو رد کنی!")
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        // do nothing
                    }
                })
                .setDuration(100)
                .build();

        targets.add(skipSkillTarget);

        return targets;
    }

    private SimpleTarget letterTarget(final AlphabetButton letter, int i, PointF letterPoint, String title) {

        final int index = i;
        SimpleTarget letterTarget = new SimpleTarget.Builder(GamePlayActivity.this)
                .setPoint(letterPoint)
                .setShape(new Circle(120f))
                .setTitle(title)
                .setDuration(100)
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                    @Override
                    public void onStarted(SimpleTarget target) {
                        // do nothing
                    }

                    @Override
                    public void onEnded(SimpleTarget target) {
                        selectAlphabet(letter, index, 0);
                    }
                })
                .build();

        return letterTarget;
    }

    private String levelBase64Decode(String base64) {

        byte[] decodeValue = Base64.decode(base64, Base64.DEFAULT);
        String finalString = new String(decodeValue, StandardCharsets.UTF_8);

        return finalString;
    }

    class UserAnswerStrings {
        private int id;
        private String value;

        public UserAnswerStrings(int id, String value) {
            this.id = id;
            this.value = value;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}