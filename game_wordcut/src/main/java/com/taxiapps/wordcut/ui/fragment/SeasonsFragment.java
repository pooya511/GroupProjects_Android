package com.taxiapps.wordcut.ui.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.model.GameConfig;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;
import com.taxiapps.wordcut.ui.adapter.LevelsAdapter;
import com.taxiapps.wordcut.ui.button.GameButtons;

import java.util.ArrayList;

/**
 * Created by Parsa on 2018-03-12.
 */

public class SeasonsFragment extends Fragment {

    public RecyclerView recyclerView;
    private TextView lockedTextView;

    public ArrayList<GameButtons> levelsButton = new ArrayList<>();

    public static SeasonsFragment newInstance(ArrayList<GameButtons> levelsButton) {

        Bundle args = new Bundle();
        args.putSerializable("levels",levelsButton);
        SeasonsFragment fragment = new SeasonsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        levelsButton = (ArrayList<GameButtons>) getArguments().getSerializable("levels");

        View view = inflater.inflate(R.layout.frg_level_btn_list, container, false);
        recyclerView = view.findViewById(R.id.frg_level_btn_list_recycler_view);
        lockedTextView = view.findViewById(R.id.frg_level_btn_list_lock_text_view);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        LevelsAdapter levelsAdapter = new LevelsAdapter(getActivity(), levelsButton, MainActivity.Last_Config_Created);
        recyclerView.setAdapter(levelsAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (levelsButton.size() == 0) {
            lockedTextView.setVisibility(View.VISIBLE);
            lockedTextView.setText("فصل جدید \n به زودی ...");
        } else {
            String seasonID = String.valueOf(levelsButton.get(0).getLevel().getSeasonId()); // getting season id from buttons . all buttons have same season id
            if (MainActivity.Last_Config_Created.hasEvent()) {
                if (PreferencesHelper.getStringArrayPrefEvent(MainActivity.Last_Config_Created.getEvent().getName(), PreferencesHelper.unLockedSeasons).contains(seasonID)) {
                    recyclerView.setAlpha(1f);
                    lockedTextView.setVisibility(View.GONE);
                } else {
                    recyclerView.setAlpha(0.6f);
                    lockedTextView.setVisibility(View.VISIBLE);
                }

            } else {
                if (PreferencesHelper.getStringArrayPref(PreferencesHelper.unLockedSeasons).contains(seasonID)) {
                    recyclerView.setAlpha(1f);
                    lockedTextView.setVisibility(View.GONE);
                } else {
                    recyclerView.setAlpha(0.6f);
                    lockedTextView.setVisibility(View.VISIBLE);
                }
            }

        }

    }

}
