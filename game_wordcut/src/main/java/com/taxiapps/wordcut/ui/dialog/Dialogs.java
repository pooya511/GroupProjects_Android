package com.taxiapps.wordcut.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.model.Achievement;
import com.taxiapps.wordcut.model.Coin;
import com.taxiapps.wordcut.model.GameConfig;
import com.taxiapps.wordcut.model.Level;
import com.taxiapps.wordcut.model.json.AchievementJson;
import com.taxiapps.wordcut.model.listener.PublicListener;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;
import com.taxiapps.wordcut.ui.adapter.AchievementsAdapter;
import com.taxiapps.wordcut.ui.adapter.PaymentProductAdapter;
import com.taxiapps.wordcut.utils.AppModules;
import com.taxiapps.wordcut.utils.payment.Payment;
import com.taxiapps.wordcut.utils.payment.Product;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import modules.PublicModules;
import modules.TX_Ad;
import saman.zamani.persiandate.PersianDate;


/**
 * Created by Sabih on 3/12/2018.
 */

public class Dialogs {

    private static boolean isSettingMode = true;

    //    public static boolean eyeAdPermission = true, dicAdPermission = true, skipAdPermission = true;
    private static int eyeCountDownLifeTime = (10 / 4) * 1000 * 60;
    private static int dicCountDownLifeTime = (20 / 4) * 1000 * 60;
    private static int skipCountDownLifeTime = (50 / 4) * 1000 * 60;
    private static int adEyeTime;
    private static int adDicTime;
    private static int adSkipTime;


    public enum UseSkillTypes {

        Eye(1),
        Skip(2),
        Dictionary(3);

        private int type;

        UseSkillTypes(int type) {
            this.type = type;
        }

        public int value() {
            return this.type;
        }

    }

    public interface UsingSkillCallBack {
        void call();
    }

    public interface ShareCallBack {
        void call();
    }

    public static Dialog settingsDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_settings, null);

        final ImageView musicImage = view.findViewById(R.id.pop_settings_music_image);
        final ImageView sfxImage = view.findViewById(R.id.pop_settings_sfx_image);
        final ImageView emailImage = view.findViewById(R.id.pop_settings_email_image);
        final ImageView removeAds = view.findViewById(R.id.pop_settings_remove_ads_image);

        TextView musicText = view.findViewById(R.id.pop_settings_music_text);
        TextView sfxText = view.findViewById(R.id.pop_settings_sfx_text);
        TextView contactText = view.findViewById(R.id.pop_settings_contact_text);
        TextView removeAdsText = view.findViewById(R.id.pop_settings_remove_ads_text);
        final TextView developersText = view.findViewById(R.id.pop_settings_developers_text);
        TextView versionText = view.findViewById(R.id.pop_settings_version_text);
        ConstraintLayout parent = view.findViewById(R.id.pop_settings_parent);

        versionText.setText("ورژن بازی: " + PublicModules.toPersianDigit(Application.appVersion));

        final LinearLayout settingContainer = view.findViewById(R.id.pop_settings_linear_container);
        final LinearLayout developerNames = view.findViewById(R.id.pop_settings_developers_name);

        Typeface iranSansNormal = Typeface.createFromAsset(context.getAssets(), "fonts/" + "IRANSans.ttf");
        Typeface iranSansBold = Typeface.createFromAsset(context.getAssets(), "fonts/" + "IRANSans_Bold.ttf");

        ViewGroup developers = developerNames;
        AppModules.setTypeface(developers, iranSansNormal);

        musicText.setTypeface(iranSansBold);
        sfxText.setTypeface(iranSansBold);
        contactText.setTypeface(iranSansBold);
        removeAdsText.setTypeface(iranSansBold);

        developersText.setTypeface(iranSansNormal);
        versionText.setTypeface(iranSansNormal);

        if (PreferencesHelper.getString(PreferencesHelper.musicIsEnabled).equals("0"))
            musicImage.setBackgroundResource(R.drawable.slc_settings_music_is_disable);
        else
            musicImage.setBackgroundResource(R.drawable.slc_settings_music_is_enable);

        if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("0"))
            sfxImage.setBackgroundResource(R.drawable.slc_settings_sfx_is_disable);
        else
            sfxImage.setBackgroundResource(R.drawable.slc_settings_sfx_is_enable);

        isSettingMode = true;

        View.OnClickListener listeners = new PublicListener() {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                if (v.equals(musicImage)) {
                    if (PreferencesHelper.getString(PreferencesHelper.musicIsEnabled).equals("1")) {
                        PreferencesHelper.setString(PreferencesHelper.musicIsEnabled, "0");
                        musicImage.setBackgroundResource(R.drawable.slc_settings_music_is_disable);
                        MainActivity.getInstance().stopMusic();
                    } else if (PreferencesHelper.getString(PreferencesHelper.musicIsEnabled).equals("0")) {
                        PreferencesHelper.setString(PreferencesHelper.musicIsEnabled, "1");
                        musicImage.setBackgroundResource(R.drawable.slc_settings_music_is_enable);
                        MainActivity.getInstance().playMusic();
                    }
                }
                if (v.equals(sfxImage)) {
                    if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")) {
                        PreferencesHelper.setString(PreferencesHelper.sfxIsEnabled, "0");
                        sfxImage.setBackgroundResource(R.drawable.slc_settings_sfx_is_disable);
                    } else if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("0")) {
                        PreferencesHelper.setString(PreferencesHelper.sfxIsEnabled, "1");
                        sfxImage.setBackgroundResource(R.drawable.slc_settings_sfx_is_enable);
                    }
                }
                if (v.equals(emailImage)) {
                    PublicModules.contactUsByEmail(context, "نیم کلمه", Application.appVersion);
                }
            }
        };

        developersText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSettingMode) {

                    // fireBaseAnalytics
                    String event = "Credits_fa";
                    Bundle bundle = new Bundle();
                    bundle.putString("Watched", "Yes");
                    Application.firebaseAnalytics.logEvent(event, bundle);

                    settingContainer.animate().alpha(0).setDuration(500);

                    developersText.animate().alpha(0).setDuration(500);
                    developersText.setText("بازگشت");
                    developersText.animate().alpha(1).setDuration(400);

                    developerNames.animate().alpha(1).setDuration(500);

                    isSettingMode = false;
                } else {
                    settingContainer.animate().alpha(1).setDuration(500);

                    developersText.animate().alpha(0).setDuration(500);
                    developersText.setText("سازندگان بازی");
                    developersText.animate().alpha(1).setDuration(400);

                    developerNames.animate().alpha(0).setDuration(500);

                    isSettingMode = true;
                }
            }
        });

        musicImage.setOnClickListener(listeners);
        sfxImage.setOnClickListener(listeners);
        emailImage.setOnClickListener(listeners);
        removeAds.setOnClickListener(listeners);

        final Dialog settings = new Dialog(context);
        settings.requestWindowFeature(Window.FEATURE_NO_TITLE);
        settings.setContentView(view);
        Window dialogWindow = settings.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.height = (int) context.getResources().getDimension(R.dimen.mainPopUpHeight);
        lp.width = ConstraintLayout.LayoutParams.MATCH_PARENT;

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins((int) context.getResources().getDimension(R.dimen.mainPopUpMarginLeftAndRight), (int) context.getResources().getDimension(R.dimen.mainPopUpMarginTop), (int) context.getResources().getDimension(R.dimen.mainPopUpMarginLeftAndRight), 0);
        parent.setLayoutParams(layoutParams);

        settings.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        settings.getWindow().setGravity(Gravity.BOTTOM);
        settings.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        return settings;
    }

    public static Dialog statsDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_stats, null);

        TextView playedCount = view.findViewById(R.id.pop_stats_levels_played_count);
        TextView completedLevels = view.findViewById(R.id.pop_stats_levels_resolved_count);
        TextView fastestResolveTimeText = view.findViewById(R.id.pop_stats_fastest_resolve_time);
        TextView eyeUsedCount = view.findViewById(R.id.pop_stats_use_eye_count);
        TextView dicUsedCount = view.findViewById(R.id.pop_stats_use_dic_count);
        TextView skipUsedCount = view.findViewById(R.id.pop_stats_use_skip_count);
        TextView wrongAnswerCount = view.findViewById(R.id.pop_stats_wrong_answers_count);
        TextView purchaseCount = view.findViewById(R.id.pop_stats_purchase_count);

        Typeface iranSans = Typeface.createFromAsset(context.getAssets(), "fonts/" + "IRANSans_UltraLight.ttf");
        AppModules.setTypeface((ViewGroup) view, iranSans);
        ConstraintLayout parent = view.findViewById(R.id.pop_stats_root);

        playedCount.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.playCount)));
        completedLevels.setText(PublicModules.toPersianDigit(String.valueOf(PreferencesHelper.getStringArrayPref(PreferencesHelper.passedLevelsKey).get(PreferencesHelper.getStringArrayPref(PreferencesHelper.passedLevelsKey).size() - 1))));
        fastestResolveTimeText.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.fastestComplete)));

        eyeUsedCount.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.eyeUsedCount)));
        dicUsedCount.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.dicUsedCount)));
        skipUsedCount.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.skipUsedCount)));
        wrongAnswerCount.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.wrongAnswer)));
        purchaseCount.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.purchaseCount)));

        final Dialog stats = new Dialog(context);
        stats.requestWindowFeature(Window.FEATURE_NO_TITLE);
        stats.setContentView(view);
        Window dialogWindow = stats.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.height = (int) context.getResources().getDimension(R.dimen.mainPopUpHeight);
        lp.width = ConstraintLayout.LayoutParams.MATCH_PARENT;

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins((int) context.getResources().getDimension(R.dimen.mainPopUpMarginLeftAndRight), (int) context.getResources().getDimension(R.dimen.mainPopUpMarginTop), (int) context.getResources().getDimension(R.dimen.mainPopUpMarginLeftAndRight), 0);
        parent.setLayoutParams(layoutParams);

        stats.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        stats.getWindow().setGravity(Gravity.BOTTOM);
        stats.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        return stats;
    }

    public static Dialog achievementsDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_achievements, null);

        ArrayList<Achievement> achievementArray = new ArrayList<>(AchievementJson.achievements.values());
        ArrayList<String> unlockedAchievementsArr = PreferencesHelper.getStringArrayPref(PreferencesHelper.unlockedAchievement);
        ArrayList<String> collectedAchievementArr = PreferencesHelper.getStringArrayPref(PreferencesHelper.collectedAchievement);

        Typeface iranSans = Typeface.createFromAsset(context.getAssets(), "fonts/" + "IRANSans_Medium.ttf");

        FrameLayout parent = view.findViewById(R.id.pop_achievements_root);
        TextView unlockedAchievements = view.findViewById(R.id.pop_achievements_unlocked_achievements);
        final TextView newAchievements = view.findViewById(R.id.pop_achievements_new_achievements);
        View cancelView = view.findViewById(R.id.pop_achievement_cancel_view);
        ImageView headerTitle = view.findViewById(R.id.pop_achievements_title);

        ConstraintLayout dialogRoot = view.findViewById(R.id.pop_achievement_dialog_root);

        unlockedAchievements.setTypeface(iranSans);
        unlockedAchievements.setText(PublicModules.toPersianDigit(String.valueOf(achievementArray.size())) + " / " + PublicModules.toPersianDigit(String.valueOf(unlockedAchievementsArr.size())) + " اچیومنت باز شده");

        newAchievements.setTypeface(iranSans);
        int unreadAchievementsCount = 0;
        for (int i = 0; i < unlockedAchievementsArr.size(); i++) {
            if (!collectedAchievementArr.contains(unlockedAchievementsArr.get(i))) {
                unreadAchievementsCount++;
            }
        }
        if (unreadAchievementsCount != 0) {
            newAchievements.setVisibility(View.VISIBLE);
            newAchievements.setText(PublicModules.toPersianDigit(String.valueOf(unreadAchievementsCount)) + " اچیومنت جدید");
        } else {
            newAchievements.setVisibility(View.GONE);
        }

        final Dialog achievementsDialog = new Dialog(context);

        RecyclerView recyclerView = view.findViewById(R.id.pop_achievements_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new AchievementsAdapter(context, achievementArray, unlockedAchievementsArr, collectedAchievementArr, parent, new Achievement.AchievementCollectCallBack() {
            @Override
            public void call() {

                int newAchievementsCount = Integer.parseInt(newAchievements.getText().toString().replaceAll("\\D", ""));
                newAchievementsCount = newAchievementsCount - 1;
                if (newAchievementsCount <= 0)
                    newAchievements.setVisibility(View.GONE);
                else
                    newAchievements.setText(PublicModules.toPersianDigit(String.valueOf(newAchievementsCount)) + " اچیومنت جدید");

                MainActivity.getInstance().mainUnreadAchievementsNotify();
            }
        }));


        achievementsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        achievementsDialog.setContentView(view);
        Window dialogWindow = achievementsDialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.height = ConstraintLayout.LayoutParams.MATCH_PARENT;
        lp.width = ConstraintLayout.LayoutParams.MATCH_PARENT;

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.AchievementPopHeight));
        layoutParams.setMargins((int) context.getResources().getDimension(R.dimen.mainPopUpMarginLeftAndRight), (int) context.getResources().getDimension(R.dimen.mainPopUpMarginTop), (int) context.getResources().getDimension(R.dimen.mainPopUpMarginLeftAndRight), 0);
        layoutParams.gravity = Gravity.BOTTOM;
        dialogRoot.setLayoutParams(layoutParams);

        achievementsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        achievementsDialog.getWindow().setGravity(Gravity.BOTTOM);
        achievementsDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        cancelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                achievementsDialog.dismiss();
            }
        });

        achievementsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MainActivity.getInstance().coinCountText.setText(PublicModules.toPersianDigit(PreferencesHelper.getString(PreferencesHelper.coin)));
                MainActivity.getInstance().achievementDialog = Dialogs.achievementsDialog(context);
                MainActivity.getInstance().mainUnreadAchievementsNotify();
            }
        });

        return achievementsDialog;
    }

    public static Dialog shareDialog(final Context context, final ShareCallBack callBack) {

        final Dialog shareDialog = new Dialog(context);
        shareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shareDialog.setContentView(R.layout.pop_share);
        shareDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        shareDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        Button shareBtn = shareDialog.getWindow().getDecorView().findViewById(R.id.pop_share_btn);
        shareBtn.setOnClickListener(v -> {
                    AppModules.share(context);
                    callBack.call();
                }
        );

        return shareDialog;
    }

    public static Dialog useSkillDialog(final Context context, final UseSkillTypes useSkillTypes, final UsingSkillCallBack usingSkillCallBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_use_skill, null);

        final ImageView skillImage = view.findViewById(R.id.pop_use_skill_img);
        final ImageView skillTitle = view.findViewById(R.id.pop_use_skill_title);
        final TextView skillValue = view.findViewById(R.id.pop_use_skill_value);
        final TextView skillDescription = view.findViewById(R.id.pop_use_skill_decription);
        final TextView adTimer = view.findViewById(R.id.pop_use_skill_ad_timer);
        Button skillOkBtn = view.findViewById(R.id.pop_use_skill_ok_btn);
        Button adBtn = view.findViewById(R.id.pop_use_skill_ad_btn);

        if (MainActivity.adIsActive) {
            adBtn.setVisibility(View.VISIBLE);
        } else {
            adBtn.setVisibility(View.GONE);
            adTimer.setVisibility(View.GONE);
        }

        long lastDate;
        long currDate;
        long savedDate;

        switch (useSkillTypes) {
            case Skip:

                skillImage.setImageDrawable(context.getResources().getDrawable(R.drawable.img_skip));
                skillTitle.setImageDrawable(context.getResources().getDrawable(R.drawable.title_skip_fa));
                skillValue.setText("۵۰");
                skillDescription.setText("رد کردن مرحله");

                lastDate = Long.valueOf(PreferencesHelper.getString(PreferencesHelper.skipAdTimeLimit));

                currDate = new PersianDate().getTime();
                savedDate = new PersianDate(lastDate).getTime();

                if (MainActivity.adIsActive && lastDate != 0 && currDate - savedDate <= skipCountDownLifeTime) {

                    adBtn.setEnabled(false);
                    adBtn.setBackgroundResource(R.drawable.btn_watch_add_empty);
                    adTimer.setVisibility(View.VISIBLE);

                    long remainingTime = skipCountDownLifeTime - (currDate - savedDate);

                    new CountDownTimer(remainingTime, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            long minute = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                            long second = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            adTimer.setText(PublicModules.toPersianDigit(minute + " : " + second));
                        }

                        @Override
                        public void onFinish() {
                            PreferencesHelper.setString(PreferencesHelper.skipAdTimeLimit, "0");

                            adBtn.setEnabled(true);
                            adBtn.setBackgroundResource(R.drawable.slc_btn_pop_watch_add);
                            adTimer.setVisibility(View.GONE);
                        }
                    }.start();
                }

                break;
            case Dictionary:

                skillImage.setImageDrawable(context.getResources().getDrawable(R.drawable.img_dic));
                skillTitle.setImageDrawable(context.getResources().getDrawable(R.drawable.title_dic_fa));
                skillValue.setText("۲۰");
                skillDescription.setText("نمایش معنی کلمه");

                lastDate = Long.valueOf(PreferencesHelper.getString(PreferencesHelper.dicAdTimeLimit));

                currDate = new PersianDate().getTime();
                savedDate = new PersianDate(lastDate).getTime();

                if (MainActivity.adIsActive && lastDate != 0 && currDate - savedDate <= dicCountDownLifeTime) {

                    adBtn.setEnabled(false);
                    adBtn.setBackgroundResource(R.drawable.btn_watch_add_empty);
                    adTimer.setVisibility(View.VISIBLE);

                    long remainingTime = dicCountDownLifeTime - (currDate - savedDate);

                    new CountDownTimer(remainingTime, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            long minute = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                            long second = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            adTimer.setText(PublicModules.toPersianDigit(minute + " : " + second));
                        }

                        @Override
                        public void onFinish() {
                            PreferencesHelper.setString(PreferencesHelper.dicAdTimeLimit, "0");

                            adBtn.setEnabled(true);
                            adBtn.setBackgroundResource(R.drawable.slc_btn_pop_watch_add);
                            adTimer.setVisibility(View.GONE);
                        }
                    }.start();

                }

                break;

            case Eye:

                skillImage.setImageDrawable(context.getResources().getDrawable(R.drawable.img_eye));
                skillTitle.setImageDrawable(context.getResources().getDrawable(R.drawable.title_eye_fa));
                skillValue.setText("۱۰");
                skillDescription.setText("نمایش یک حرف");

                lastDate = Long.valueOf(PreferencesHelper.getString(PreferencesHelper.eyeAdTimeLimit));

                currDate = new PersianDate().getTime();
                savedDate = new PersianDate(lastDate).getTime();

                if (MainActivity.adIsActive && lastDate != 0 && currDate - savedDate <= eyeCountDownLifeTime) {

                    adBtn.setEnabled(false);
                    adBtn.setBackgroundResource(R.drawable.btn_watch_add_empty);
                    adTimer.setVisibility(View.VISIBLE);

                    long remainingTime = eyeCountDownLifeTime - (currDate - savedDate);

                    new CountDownTimer(remainingTime, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            long minute = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                            long second = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            adTimer.setText(PublicModules.toPersianDigit(minute + " : " + second));
                        }

                        @Override
                        public void onFinish() {
                            PreferencesHelper.setString(PreferencesHelper.eyeAdTimeLimit, "0");

                            adBtn.setEnabled(true);
                            adBtn.setBackgroundResource(R.drawable.slc_btn_pop_watch_add);
                            adTimer.setVisibility(View.GONE);
                        }
                    }.start();
                }
                break;
        }


        final Dialog useSkill = new Dialog(context);
        useSkill.requestWindowFeature(Window.FEATURE_NO_TITLE);
        useSkill.setContentView(view);
        useSkill.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
        useSkill.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        skillOkBtn.setOnClickListener(new PublicListener() {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                if (useSkillTypes == UseSkillTypes.Eye) {
                    if (Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.coin)) < 10) {
                        Dialogs.payment(context, false, GamePlayActivity.getInstance().parentFrame).show();
                    } else {
                        GamePlayActivity.getInstance().eyeSkill(true);
                        useSkill.dismiss();
                    }
                }
                if (useSkillTypes == UseSkillTypes.Skip) {
                    if (Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.coin)) < 50) {
                        Dialogs.payment(context, false, GamePlayActivity.getInstance().parentFrame).show();
                    } else {
                        GamePlayActivity.getInstance().skipSkill(true);
                        useSkill.dismiss();
                    }
                }
                if (useSkillTypes == UseSkillTypes.Dictionary) {
                    if (Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.coin)) < 20) {
                        Dialogs.payment(context, false, GamePlayActivity.getInstance().parentFrame).show();
                    } else {
                        GamePlayActivity.getInstance().dictionarySkill(true);
                        useSkill.dismiss();
                    }
                }

                usingSkillCallBack.call();
            }
        });

        adBtn.setOnClickListener(new PublicListener() {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                TX_Ad.showAd(context, TX_Ad.AdTypes.reward, Application.deviceID, "android", complete -> {
                    if (complete) {
                        switch (useSkillTypes) {
                            case Eye:
                                ((Activity) context).runOnUiThread(() -> {
                                    GamePlayActivity.getInstance().eyeSkill(false);
                                    useSkill.dismiss();
                                });
                                break;
                            case Skip:
                                ((Activity) context).runOnUiThread(() -> {
                                    GamePlayActivity.getInstance().skipSkill(false);
                                    useSkill.dismiss();
                                });
                                break;
                            case Dictionary:
                                ((Activity) context).runOnUiThread(() -> {
                                    GamePlayActivity.getInstance().dictionarySkill(false);
                                    useSkill.dismiss();
                                });
                                break;
                        }
                        PreferencesHelper.setString(PreferencesHelper.eyeAdTimeLimit, String.valueOf(Calendar.getInstance().getTimeInMillis()));
                        PreferencesHelper.setString(PreferencesHelper.skipAdTimeLimit, String.valueOf(Calendar.getInstance().getTimeInMillis()));
                        PreferencesHelper.setString(PreferencesHelper.dicAdTimeLimit, String.valueOf(Calendar.getInstance().getTimeInMillis()));

                        usingSkillCallBack.call();

                    }
                });
            }
        });
        return useSkill;
    }

    public static Dialog levelCompletedDialog(final Context context, final Level level,
                                              boolean isLastSeasonLevel,
                                              boolean hasNextSeason) {

        MediaPlayer successSound = MediaPlayer.create(context, R.raw.success);
        if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")) {
            successSound.start();
        }

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_compeleted_level, null);

        Typeface ykanTypeFace = Typeface.createFromAsset(context.getAssets(), "fonts/" + "yekan.ttf");
        final Button achoievementsBtn = view.findViewById(R.id.pop_level_compeleted_achievements_btn);
        final Button menuBtn = view.findViewById(R.id.pop_level_compeleted_menu_btn);
        final Button nextBtn = view.findViewById(R.id.pop_level_compeleted_next_btn);
        TextView levelText = view.findViewById(R.id.pop_level_compeleted_level_level_text);
        final TextView wordText = view.findViewById(R.id.pop_level_compeleted_level_level_word);
        TextView newSeason = view.findViewById(R.id.pop_level_compeleted_new_season_text);
        TextView dic = view.findViewById(R.id.pop_level_compeleted_level_level_dic);

        dic.setText(PublicModules.toPersianDigit(level.getSkillDic()));
        dic.setVisibility(View.VISIBLE);

        String levelNumber = PublicModules.toPersianDigit("مرحله " + level.getId());
        levelText.setText(levelNumber);
        wordText.setText(level.getAnswer());
        wordText.setTypeface(ykanTypeFace);

        if (isLastSeasonLevel) {
            nextBtn.setVisibility(View.GONE);
            if (hasNextSeason) {
                newSeason.setVisibility(View.VISIBLE);
            } else {
                newSeason.setVisibility(View.INVISIBLE);
            }
        } else {
            nextBtn.setVisibility(View.VISIBLE);
            newSeason.setVisibility(View.INVISIBLE);
        }

        final Dialog levelCompeleted = new Dialog(context);
        levelCompeleted.setCanceledOnTouchOutside(false);
        levelCompeleted.requestWindowFeature(Window.FEATURE_NO_TITLE);
        levelCompeleted.setContentView(view);
        levelCompeleted.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
        levelCompeleted.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Level newLevel = Level.getLevelById(level.getId() + 1, MainActivity.Last_Config_Created);

        View.OnClickListener listeners = new PublicListener() {
            @Override
            public void onClick(View v) {
                super.onClick(v);
                if (v.equals(achoievementsBtn)) {
                    levelCompeleted.dismiss();
                    ((Activity) context).finish();
                    MainActivity.getInstance().showLevels(false, MainActivity.Last_Config_Created);
                    MainActivity.getInstance().showMain(true);
                    Dialogs.achievementsDialog(MainActivity.getInstance()).show();
                }
                if (v.equals(menuBtn)) {
                    levelCompeleted.dismiss();
                    ((Activity) context).finish();
                }
                if (v.equals(nextBtn)) {
                    Intent gamePlayIntent = new Intent(context, GamePlayActivity.class);
                    gamePlayIntent.putExtra("Level", newLevel);
                    context.startActivity(gamePlayIntent);
                    levelCompeleted.dismiss();
                    ((Activity) context).finish();
                }
            }
        };

        achoievementsBtn.setOnClickListener(listeners);
        menuBtn.setOnClickListener(listeners);
        nextBtn.setOnClickListener(listeners);

        return levelCompeleted;
    }

    public static Dialog achievementUnlockedDialog(Context context, Achievement achievement) {

        // fireBaseAnalytics
        String event = "Achievement_fa";
        Bundle bundle = new Bundle();
        bundle.putString("Name", achievement.getId());
        Application.firebaseAnalytics.logEvent(event, bundle);

        ArrayList<String> unlockedAchievement = PreferencesHelper.getStringArrayPref(PreferencesHelper.unlockedAchievement);
        unlockedAchievement.add(achievement.getId());
        PreferencesHelper.setStringArrayPref(PreferencesHelper.unlockedAchievement, unlockedAchievement);

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_achievement_unlocked, null);

        final Dialog achievementUnlockedDialog = new Dialog(context);
        achievementUnlockedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        achievementUnlockedDialog.setContentView(view);
        achievementUnlockedDialog.setCanceledOnTouchOutside(true);
        achievementUnlockedDialog.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);
        achievementUnlockedDialog.getWindow().setGravity(Gravity.TOP);
        achievementUnlockedDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        achievementUnlockedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        WindowManager.LayoutParams params = achievementUnlockedDialog.getWindow().getAttributes();
        achievementUnlockedDialog.setCanceledOnTouchOutside(true);
        params.y = -100;
        achievementUnlockedDialog.getWindow().setAttributes(params);

        ImageView achievementImage = view.findViewById(R.id.pop_achievement_unlocked_image);
        achievementImage.setBackgroundResource(Achievement.getBackgroundResource(context, achievement.getId(), true));

        new CountDownTimer(3000, 4000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (!((Activity) context).isFinishing()) {
                    if (achievementUnlockedDialog.isShowing()) {
                        achievementUnlockedDialog.dismiss();
                    }
                }
            }
        }.start();

        MainActivity.getInstance().mainUnreadAchievementsNotify();
        MainActivity.getInstance().achievementDialog = Dialogs.achievementsDialog(context);

        return achievementUnlockedDialog;
    }

    public static Dialog payment(final Context context, final boolean isOffMode,
                                 final FrameLayout parent) {

        // fireBaseAnalytics
        String event = "Purchases_fa";
        Bundle bundle = new Bundle();
        bundle.putString("Mode", isOffMode ? "50%" : "100%");
        Application.firebaseAnalytics.logEvent(event, bundle);

        if (PublicModules.isNetworkAvailable(context)) {

            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.pop_payment, null);

            final RecyclerView recyclerView = view.findViewById(R.id.pop_coin_add_recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            final ProgressBar progressBar = view.findViewById(R.id.pop_coin_add_waiting_progressbar);

            final Dialog payment = new Dialog(context);
            payment.requestWindowFeature(Window.FEATURE_NO_TITLE);
            payment.setContentView(view);
            payment.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);
            payment.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            try {
                Payment.GetProducts(Application.appID, Application.appSource, Application.deviceID, Application.userName, Application.serverUrl, new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        ((Activity) context).runOnUiThread(() -> {
                            payment.dismiss();
                            if (!((Activity) context).isFinishing()) {
                                Dialogs.paymentError(context, "خطا نامشخص").show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        if (response.isSuccessful()) {
                            ((Activity) context).runOnUiThread(() -> progressBar.setVisibility(View.GONE));
                            String responseBody = response.body().string();
                            try {
                                final ArrayList<Product> products = Payment.getProductContent(responseBody, isOffMode);
                                ((Activity) context).runOnUiThread(() -> recyclerView.setAdapter(new PaymentProductAdapter(context, products, parent, payment)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            return payment;
        } else {
            return Dialogs.paymentError(context, Payment.NETWORK_CONNECTION_ERROR);
        }
    }

    public static Dialog paymentError(final Context context, String errorMessage) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_payment_failed, null);

        TextView errorText = view.findViewById(R.id.pop_payment_failed_description);
        errorText.setText(errorMessage);
        Button accept = view.findViewById(R.id.pop_payment_accept);

        final MediaPlayer btnSound = MediaPlayer.create(context, R.raw.button_action_sound);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);

        accept.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")) {
                        btnSound.start();
                    }

                    v.setPadding(0, 5, 0, 0);
                    v.setPressed(true);

                    dialog.dismiss();

                } else {
                    v.setPadding(0, 0, 0, 5);
                    v.setPressed(false);
                }

                return true;
            }
        });


        final MediaPlayer errorSound = MediaPlayer.create(context, R.raw.error);

        if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorSound.start();
                }
            }, 100);

        }

        return dialog;
    }

    public static Dialog rateDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_google_play_rate, null);

        final TextView yes = view.findViewById(R.id.pop_google_play_rate_yes);
        final TextView no = view.findViewById(R.id.pop_google_play_rate_no);

        Typeface yekan = Typeface.createFromAsset(context.getAssets(), "fonts/" + "IRANSans_Light.ttf");
        AppModules.setTypeface((ViewGroup) view, yekan);

        final Dialog shareDialog = new Dialog(context);
        shareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shareDialog.setContentView(view);
        shareDialog.setCanceledOnTouchOutside(false);
        shareDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        shareDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        View.OnClickListener listeners = v -> {
            if (v.equals(yes)) {
                MainActivity.getInstance().rateUS(context);
                PreferencesHelper.setString(PreferencesHelper.isUserRated, "1");
                shareDialog.dismiss();
            }
            if (v.equals(no)) {
                ArrayList<String> levelsToRate = PreferencesHelper.getStringArrayPref(PreferencesHelper.levelsToRate);
                if (levelsToRate.size() > 0) {
                    levelsToRate.remove(0);
                }
                PreferencesHelper.setStringArrayPref(PreferencesHelper.levelsToRate, levelsToRate);
                shareDialog.dismiss();
            }
        };

        yes.setOnClickListener(listeners);
        no.setOnClickListener(listeners);

        return shareDialog;
    }

}