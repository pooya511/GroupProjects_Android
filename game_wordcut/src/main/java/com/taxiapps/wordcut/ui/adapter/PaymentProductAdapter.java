package com.taxiapps.wordcut.ui.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.PreferencesHelper;
import com.taxiapps.wordcut.model.Achievement;
import com.taxiapps.wordcut.model.Coin;
import com.taxiapps.wordcut.model.json.AchievementJson;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;
import com.taxiapps.wordcut.ui.dialog.Dialogs;
import com.taxiapps.wordcut.utils.payment.PersianStorePayment;
import com.taxiapps.wordcut.utils.payment.Payment;
import com.taxiapps.wordcut.utils.payment.Product;

import java.util.ArrayList;

import modules.PublicModules;

public class PaymentProductAdapter extends RecyclerView.Adapter<PaymentProductAdapter.PaymentViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Product> paymentItems;
    private FrameLayout parentFrame;
    private Dialog parentDialog;

    private final MediaPlayer btnSound;

    public PaymentProductAdapter(Context context, ArrayList<Product> paymentItems, FrameLayout parentFrame, Dialog parentDialog) {
        this.context = context;
        this.paymentItems = paymentItems;
        this.parentFrame = parentFrame;
        this.parentDialog = parentDialog;
        this.inflater = LayoutInflater.from(context);

        btnSound = MediaPlayer.create(context, com.taxiapps.wordcut.R.raw.button_action_sound);
    }

    @Override
    public PaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PaymentViewHolder(inflater.inflate(com.taxiapps.wordcut.R.layout.item_payment, parent, false));
    }

    @Override
    public void onBindViewHolder(PaymentViewHolder holder, int position) {

        final Product product = paymentItems.get(position);

        holder.coinCount.setText(PublicModules.toPersianDigit(product.getText()));
        holder.buyBtn.setText(PublicModules.toPersianDigit(String.valueOf(product.getPrice() / 10 + " ت")));
        holder.buyBtn.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                if (PreferencesHelper.getString(PreferencesHelper.sfxIsEnabled).equals("1")){
                    btnSound.start();
                }

                v.setPadding(0, 5, 0, 0);
                v.setPressed(true);

                switch (Application.appSource) {
                    case "google":
                        Payment.makeWebViewDialog(context, Payment.getBankRedirectURL(product.getProduct(),
                                "",
                                Application.gateWayUrl,
                                Application.appID,
                                Application.appSource,
                                Application.appVersion,
                                Application.deviceID),
                                () -> {
                                    //increase purchase count for report
                                    int purchaseCount = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.purchaseCount));
                                    purchaseCount++;
                                    PreferencesHelper.setString(PreferencesHelper.purchaseCount, String.valueOf(purchaseCount));

                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.buyCoin.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.buyCoin.value, "1");
                                        Dialogs.achievementUnlockedDialog(context, AchievementJson.achievements.get(AchievementJson.Achievements.buyCoin.value)).show();
                                    }

                                    parentDialog.dismiss();
                                    Point endPoint;
                                    if (GamePlayActivity.getInstance() == null)
                                        endPoint = MainActivity.getInstance().coinImagePoint;
                                    else
                                        endPoint = GamePlayActivity.getInstance().gamePlayCoinImagePoint;

                                    Coin.addCoin(context, parentFrame, Integer.valueOf(product.getText().replaceAll("\\D", "")), MainActivity.getInstance().startPoint, MainActivity.getInstance().middlePoint, endPoint, new Coin.CoinAddedCallBack() {
                                        @Override
                                        public void call() {
                                            // Nothing to do

                                        }
                                    });
                                }).show();
                        break;

                    case "bazaar":
                            PersianStorePayment.newInstance(context, product, parentDialog , Application.currentBase64 , new Payment.PaymentSuccessCallBack() {
                                @Override
                                public void call() {

                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.buyCoin.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.buyCoin.value, "1");
                                        Dialogs.achievementUnlockedDialog(context, AchievementJson.achievements.get(AchievementJson.Achievements.buyCoin.value)).show();
                                    }

                                    //increase purchase count for report
                                    int purchaseCount = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.purchaseCount));
                                    purchaseCount++;
                                    PreferencesHelper.setString(PreferencesHelper.purchaseCount, String.valueOf(purchaseCount));
                                }
                            });
                        break;

                        case "myket":
                            PersianStorePayment.newInstance(context, product, parentDialog , Application.currentBase64 , new Payment.PaymentSuccessCallBack() {
                                @Override
                                public void call() {

                                    if (Achievement.achievementIsLocked(AchievementJson.Achievements.buyCoin.value)) {
                                        PreferencesHelper.setString(AchievementJson.Achievements.buyCoin.value, "1");
                                        Dialogs.achievementUnlockedDialog(context, AchievementJson.achievements.get(AchievementJson.Achievements.buyCoin.value)).show();
                                    }

                                    //increase purchase count for report
                                    int purchaseCount = Integer.valueOf(PreferencesHelper.getString(PreferencesHelper.purchaseCount));
                                    purchaseCount++;
                                    PreferencesHelper.setString(PreferencesHelper.purchaseCount, String.valueOf(purchaseCount));
                                }
                            });
                        break;
                }


            } else {
                v.setPadding(0, 0, 0, 5);
                v.setPressed(false);
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return paymentItems.size();
    }

    class PaymentViewHolder extends RecyclerView.ViewHolder {

        private Button buyBtn;
        private TextView coinCount;

        public PaymentViewHolder(View itemView) {
            super(itemView);
            buyBtn = itemView.findViewById(com.taxiapps.wordcut.R.id.item_payment_price_btn);
            coinCount = itemView.findViewById(com.taxiapps.wordcut.R.id.item_payment_coin_count_text);
        }
    }

}
