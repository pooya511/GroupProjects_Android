package com.taxiapps.wordcut.utils.payment;

/**
 * Created by Parsa on 2018-02-24.
 */

public class Product {

    private int productType;

    private int id ;
    private String product;
    private String text ;
    private String textColor ;
    private int price;
    private String description;
    private String descriptionLicense;
    private String descriptionColor;
    private boolean visible;
    private boolean disable;
    private boolean hasCoupon;
    private long validDays;
    private String appFa;
    private String dateFa;

    public Product() {
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionLicense() {
        return descriptionLicense;
    }

    public void setDescriptionLicense(String descriptionLicense) {
        this.descriptionLicense = descriptionLicense;
    }

    public String getDescriptionColor() {
        return descriptionColor;
    }

    public void setDescriptionColor(String descriptionColor) {
        this.descriptionColor = descriptionColor;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public boolean isHasCoupon() {
        return hasCoupon;
    }

    public void setHasCoupon(boolean hasCoupon) {
        this.hasCoupon = hasCoupon;
    }

    public long getValidDays() {
        return validDays;
    }

    public void setValidDays(long validDays) {
        this.validDays = validDays;
    }

    public String getAppFa() {
        return appFa;
    }

    public void setAppFa(String appFa) {
        this.appFa = appFa;
    }

    public String getDateFa() {
        return dateFa;
    }

    public void setDateFa(String dateFa) {
        this.dateFa = dateFa;
    }
}
