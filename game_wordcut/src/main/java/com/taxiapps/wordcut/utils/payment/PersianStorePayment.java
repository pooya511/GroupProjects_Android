package com.taxiapps.wordcut.utils.payment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import com.taxiapps.wordcut.model.Coin;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;
import com.taxiapps.wordcut.utils.payment.util.IabHelper;
import com.taxiapps.wordcut.utils.payment.util.Purchase;

public class PersianStorePayment {
    private String base64EncodedPublicKey;
    public IabHelper mHelper;

    private Context context;
    private Product product;
    private Dialog payment;
    private Payment.PaymentSuccessCallBack successCallBack;

    public static final String TAG = "CafePayment";

    private static PersianStorePayment instance;

    public static PersianStorePayment getInstance() {
        return instance;
    }

    public static PersianStorePayment newInstance(Context context, Product product, Dialog payment , String base64EncodedPublicKey, Payment.PaymentSuccessCallBack successCallBack) {

        PersianStorePayment persianStorePayment = new PersianStorePayment();
        persianStorePayment.setContext(context);
        persianStorePayment.setProduct(product);
        persianStorePayment.setPayment(payment);
        persianStorePayment.setBase64EncodedPublicKey(base64EncodedPublicKey);
        persianStorePayment.setSuccessCallBack(successCallBack);

        instance = persianStorePayment;
        instance.initIabHelper();

        return instance;
    }

    private void initIabHelper() {

        mHelper = new IabHelper(context, base64EncodedPublicKey);

        mHelper.startSetup(result -> {
            if (result.isSuccess()) {
                Log.i(TAG, "initIabHelper is Success");
                launchPurchaseFlow();
            } else {
                Log.i(TAG, "initIabHelper is Not Success");
            }
        });

    }

    private void launchPurchaseFlow() {

        mHelper.launchPurchaseFlow((Activity) context, product.getProduct(), 100, (result, info) -> {
            if (result.isSuccess()) {
                Log.i(TAG, "queryInventoryAsync is Success");
                consumeAsync(info);
            } else {
                Log.i(TAG, "queryInventoryAsync is Not Success");
            }
        });
    }

    private void consumeAsync(Purchase purchase) {

        mHelper.consumeAsync(purchase, (purchase1, result) -> {
            if (result.isSuccess()) {
                Log.i(TAG, "consumeAsync is Success");
                payment.dismiss();

                String coinCount = product.getText().replaceAll("\\D", "");

                if (GamePlayActivity.getInstance() != null) {
                    if (context.equals(GamePlayActivity.getInstance())) {
                        Coin.addCoin(context, GamePlayActivity.getInstance().parentFrame, Integer.valueOf(coinCount)
                                , GamePlayActivity.getInstance().startPoint, GamePlayActivity.getInstance().middlePoint, GamePlayActivity.getInstance().gamePlayCoinImagePoint, new Coin.CoinAddedCallBack() {
                                    @Override
                                    public void call() {
                                        successCallBack.call();
                                    }
                                });
                    }
                } else {
                    if (context.equals(MainActivity.getInstance())) {
                        Coin.addCoin(context, MainActivity.getInstance().parent, Integer.valueOf(coinCount)
                                , MainActivity.getInstance().startPoint, MainActivity.getInstance().middlePoint, MainActivity.getInstance().coinImagePoint, new Coin.CoinAddedCallBack() {
                                    @Override
                                    public void call() {
                                        successCallBack.call();
                                    }
                                });
                    }
                }

            } else {
                Log.i(TAG, "consumeAsync is Not Success");
            }
        });

    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Dialog getPayment() {
        return payment;
    }

    public void setPayment(Dialog payment) {
        this.payment = payment;
    }

    public Payment.PaymentSuccessCallBack getSuccessCallBack() {
        return successCallBack;
    }

    public void setSuccessCallBack(Payment.PaymentSuccessCallBack successCallBack) {
        this.successCallBack = successCallBack;
    }

    public String getBase64EncodedPublicKey() {
        return base64EncodedPublicKey;
    }

    public void setBase64EncodedPublicKey(String base64EncodedPublicKey) {
        this.base64EncodedPublicKey = base64EncodedPublicKey;
    }

}
