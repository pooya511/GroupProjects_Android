package com.taxiapps.wordcut.utils.payment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.taxiapps.wordcut.Application;

import org.json.JSONException;
import org.json.JSONObject;

import com.taxiapps.wordcut.ui.dialog.Dialogs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static TxAnalytics.TxAnalytics.PurchaseVerify;

public class Payment {

    private static final String TAG = "Payment";
    private static OkHttpClient client = new OkHttpClient();

    public static final String SUCCESS = "0";
    public static final String UNKNOWN_ERROR = "-10";
    public static final String PRODUCT_INVALID = "-20";
    public static final String TRANSACTION_INVALID = "-30";
    public static final String COUPON_INVALID = "-40";
    public static final String COUPON_EXPIRED = "-41";
    public static final String COUPON_INVALID_APP = "-42";
    public static final String COUPON_EXPIRE_FAILED = "-48";
    public static final String APP_INVALID = "-60";
    public static final String BACKUP_SIZE_LIMIT = "-70";
    public static final String SMS_NUMBER_INVALID = "-80";
    public static final String SMS_METHOD1_DISABLED = "-81";
    public static final String SMS_UNKNOWN_ERROR = "-82";
    public static final String USER_NOT_EXISTS = "-90";
    public static final String LICENSE_NOT_EXISTS = "-50";
    public static final String USER_LICENSE_NOT_FOUND = "-51";

    public static final String INFO_PAYMENT_VERIFY_ERROR = "پرداخت ناموفق بوده است.در صورت کسر مبلغ وجه شما حداکثر تا ۱ ساعت به کارت بانکی برگشت داده می شود. شما می توانید هم اکنون مجددا تلاش کنید";
    public static final String INFO_PAYMENT_STATUS_CANCELED = "شما فرآیند خرید را لغو کردید. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_ISSUERDOWN = "در حال حاضر بانک صادر کننده پاسخگو نیست. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_UNKNOWN = "خطای نامشخص. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";


    public static final String PAYMENT_CANCELED_BY_USER = "پرداخت توسط کاربر لغو شد";
    public static final String BANK_NOT_RESPONDING = "بانک پساخگو نیست. بعدا تلاش کنید";
    public static final String PAYMENT_CONFIRMATION_ERROR = "خطا در تایید پرداخت";
    public static final String TRANSACTION_CONFIRMATION_ERROR = "خطا در تایید تراکنش";

    public static final String NETWORK_CONNECTION_ERROR = "لطفا به اینترنت متصل شوید";


    private static final ArrayList<String> productIDs = new ArrayList<>(Arrays.asList("p_1", "p_2", "p_3", "p_ad"));
    private static final ArrayList<String> productIDsWithOff = new ArrayList<>(Arrays.asList("p_1_off", "p_2_off", "p_3_off", "p_ad_off"));

    public interface PaymentSuccessCallBack {
        void call();
    }

    private static String makeJsonString(String responseStr) {
        if (responseStr.charAt(0) == '\"' && responseStr.charAt(responseStr.length() - 1) == '\"') {
            responseStr = responseStr.substring(1, responseStr.length() - 1);// از بین بردن " های اول و آخر
        }
        responseStr = responseStr.replaceAll("\\\\", "");
        return responseStr;
    }

    public static String getBankRedirectURL(String productContent,
                                            String aCoupon,
                                            String gateWayUrl,
                                            String appID,
                                            String appSource,
                                            String appVersion,
                                            String deviceID
    ) {

        String ret = gateWayUrl + "/" + appID + "/" + appSource + "/" + appVersion + "/" + productContent + "/" + deviceID;
        if (!aCoupon.trim().equals("")) {
            ret = gateWayUrl + "/" + appID + "/" + appSource + "/" + appVersion + "/" + productContent + "/" + deviceID + "/" + aCoupon;
        }
        Log.i("URL", ret);
        return ret;
    }

    public static ArrayList<Product> getProductContent(String str, boolean isOffMode) throws JSONException {
        ArrayList<Product> productContentArray = new ArrayList<>();
        JSONObject parentJson = new JSONObject(makeJsonString(str));
        JSONObject productsJson = (JSONObject) parentJson.get("products");
        for (int i = 0; i < productsJson.length(); i++) {

            JSONObject childJson = (JSONObject) productsJson.get(String.valueOf(i));
            Product product = new Product();
            product.setId(Integer.parseInt(childJson.getString("id")));
            product.setDescriptionColor(childJson.getString("description_color"));
            product.setText(childJson.getString("text"));
            product.setDescriptionLicense("description_license");
            product.setVisible(Boolean.valueOf(childJson.getString("visible")));
            product.setHasCoupon(Boolean.valueOf(childJson.getString("hasCoupon")));
            product.setProduct(childJson.getString("product"));
            product.setValidDays(Long.valueOf(childJson.getString("license_valid_days")));
            product.setPrice(Integer.valueOf(childJson.getString("price")));
            product.setDescription(childJson.getString("description"));
            product.setAppFa(childJson.getString("app_fa"));
            product.setDisable(Boolean.valueOf(childJson.getString("disabled")));
            product.setDateFa(childJson.getString("date_fa"));
            product.setTextColor(childJson.getString("text_color"));

            if (isOffMode) {
                if (productIDsWithOff.contains(product.getProduct())) {
                    if (!product.getProduct().contains("ad")) {  // felan ad nadarim
                        productContentArray.add(product);
                    }
                }
            } else {
                if (productIDs.contains(product.getProduct())) {
                    if (!product.getProduct().contains("ad")) { // felan ad nadarim
                        productContentArray.add(product);
                    }
                }
            }

        }

        return productContentArray;
    }

    public static void GetProducts(String appID,
                                   String appSource,
                                   String deviceID,
                                   String userNumber,
                                   String serverUrl,
                                   Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("guid", deviceID);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "GetProducts");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public static Dialog makeWebViewDialog(final Context context, String URL, final PaymentSuccessCallBack paymentSuccessCallBack) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(com.taxiapps.wordcut.R.layout.pop_payment_webview, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();

        lp.width = ConstraintLayout.LayoutParams.MATCH_PARENT;
        lp.height = ConstraintLayout.LayoutParams.MATCH_PARENT;

        dialogWindow.setAttributes(lp);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //
        final WebView myWebView = view.findViewById(com.taxiapps.wordcut.R.id.pop_payment_web_view);
        myWebView.getSettings().setSupportZoom(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setDomStorageEnabled(true);
        myWebView.loadUrl(URL);
        myWebView.setWebChromeClient(new WebChromeClient());
        //       myWebView.evaluateJavascript("alert('parsa')", null);
        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public void onPageStarted(final WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                view.evaluateJavascript("tx_reference_input.value", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(final String s) {
                        Log.i("s", s);
                        if (!s.equals("null")) {
                            String pcode = s.replace("\"", "");
                            Log.i("pcode", pcode);
                            if (s.length() > 0) {
                                Log.i("onReceiveValue", s);
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        myWebView.clearCache(true);
                                        myWebView.onPause();
                                        myWebView.removeAllViews();
                                        myWebView.destroyDrawingCache();
                                        myWebView.destroy();
                                    }
                                });
                                try {
                                    PurchaseVerify(pcode, Application.serverUrl, new Callback() {
                                        String responseStr;

                                        @Override
                                        public void onFailure(Request request, IOException e) {

                                        }

                                        @Override
                                        public void onResponse(Response response) throws IOException {
                                            if (response.isSuccessful()) {
                                                responseStr = response.body().string();
                                                Log.i("Response: ", responseStr);
                                                if (responseStr.charAt(0) == '\"' && responseStr.charAt(responseStr.length() - 1) == '\"') {
                                                    responseStr = responseStr.substring(1, responseStr.length() - 1);// از بین بردن " های اول و آخر
                                                }
                                                responseStr = responseStr.replaceAll("\\\\", ""); // // از بین بردن / های اضافی
                                                /*
                                                ------------------------------------------
                                                 */
                                                try {
                                                    JSONObject jsonObject = new JSONObject(responseStr);
                                                    if (jsonObject.getString("status").equals(SUCCESS)) { // بدون خطا از سرور
                                                        Log.i(TAG, "بدون خطا از سرور");
                                                        if (jsonObject.getString("_status").equals("OK")) { //تراکنش انجام شده
                                                            Log.i(TAG, "تراکنش انجام شده");
                                                            if (jsonObject.getString("verify").equals(SUCCESS)) {
                                                                Log.i(TAG, "پرداخت موفق");
//                                                                C_Setting.setStringValue(C_Setting.LICENSE_DATA, responseStr);

                                                                ((Activity) context).runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        paymentSuccessCallBack.call();
                                                                        dialog.dismiss();
                                                                        Toast.makeText(context, "Purchase Successfully", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });
                                                            } else { //پرداخت ناموفق
                                                                Log.i(TAG, "پرداخت ناموفق");
                                                                ((Activity) context).runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Dialogs.paymentError(context, Payment.INFO_PAYMENT_VERIFY_ERROR).show();
                                                                        dialog.dismiss();
                                                                    }
                                                                });
                                                            }
                                                        } else if (jsonObject.getString("_status").equals("Canceled By User")) {
                                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Dialogs.paymentError(context, Payment.PAYMENT_CANCELED_BY_USER).show();
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                        } else if (jsonObject.getString("_status").equals("IssuerDown_Slm")) {
                                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Dialogs.paymentError(context, Payment.BANK_NOT_RESPONDING).show();
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                        } else {
                                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Dialogs.paymentError(context, Payment.INFO_PAYMENT_STATUS_UNKNOWN).show();
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                        }
                                                    } else if (jsonObject.getString("status").equals(TRANSACTION_INVALID)) { // تراکنش پیدا نشد و سرور خطا میدهد
                                                        ((Activity) context).runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Dialogs.paymentError(context, Payment.PAYMENT_CONFIRMATION_ERROR);
                                                                dialog.dismiss();
                                                            }
                                                        });
                                                        Log.i(TAG, "خطا در پرداخت لطفا دوباره تلاش کنید ");
                                                    } else {
                                                        ((Activity) context).runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Dialogs.paymentError(context, "خطا نامشخص");
                                                                dialog.dismiss();
                                                            }
                                                        });
                                                        Log.i(TAG, "خطای نا مشخص");
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                ((Activity) context).runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Dialogs.paymentError(context, Payment.TRANSACTION_CONFIRMATION_ERROR);
                                                        dialog.dismiss();
                                                    }
                                                });
                                                Log.i(TAG, "خطا در تایید تراکنش");
                                            }
                                        }
                                    });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else if (s.length() == 0) {
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Dialogs.paymentError(context, Payment.TRANSACTION_CONFIRMATION_ERROR);
                                        dialog.dismiss();
                                    }
                                });
                            }
                        }
                    }
                });
            }
        };
        myWebView.setWebViewClient(webViewClient);
        //
        return dialog;
    }

}
