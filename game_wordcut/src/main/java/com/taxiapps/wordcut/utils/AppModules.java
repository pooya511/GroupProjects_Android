package com.taxiapps.wordcut.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.R;
import com.taxiapps.wordcut.event.NotificationPublisher;
import com.taxiapps.wordcut.model.Level;
import com.taxiapps.wordcut.model.Season;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import de.cketti.mailto.EmailIntentBuilder;

import static android.content.ContentValues.TAG;

/**
 * Created by Parsa on 2018-03-14.
 */

public class AppModules {

    public static void setTypeface(ViewGroup viewGroup, Typeface typeface) {
        if (viewGroup == null) return;

        int children = viewGroup.getChildCount();
        Log.d(TAG, "setTypeface " + viewGroup + " : " + children);
        for (int i = 0; i < children; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                setTypeface((ViewGroup) view, typeface);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                textView.setTypeface(typeface);
            }
        }
    }

    public static void share(Context context) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        String shareBody = "";

        switch (Application.appSource) {

            case "google":
                sharingIntent.setType("text/plain");
                shareBody = Application.SHARE_LINK_GOOGLE;
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                ((Activity) context).startActivityForResult(Intent.createChooser(sharingIntent, "Share using"), Application.SHARE_REQUEST_CODE);
                break;

            case "bazaar":
                sharingIntent.setType("text/plain");
                shareBody = Application.SHARE_LINK_BAZAAR;
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                ((Activity) context).startActivityForResult(Intent.createChooser(sharingIntent, "Share using"), Application.SHARE_REQUEST_CODE);
                break;

                case "myket":
                sharingIntent.setType("text/plain");
                shareBody = Application.SHARE_LINK_MYKET;
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                ((Activity) context).startActivityForResult(Intent.createChooser(sharingIntent, "Share using"), Application.SHARE_REQUEST_CODE);
                break;

        }

    }

    public static LinkedHashMap<String, Level> levelJsonToHashMap(JSONArray jsonArray) throws JSONException {

        LinkedHashMap<String, Level> map = new LinkedHashMap<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            map.put(jsonObject.getString("id"), new Level(Integer.valueOf(jsonObject.getString("id")),
                    Integer.valueOf(jsonObject.getString("season_id")),
                    jsonObject.getString("skill_dic"),
                    jsonObject.getString("answer"),
                    jsonObject.getString("svg_image")));
        }

        return map;
    }

    public static LinkedHashMap<String, Season> seasonJsonToHashMap(JSONArray jsonArray) throws JSONException {

        LinkedHashMap<String, Season> map = new LinkedHashMap<String, Season>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            map.put(jsonObject.getString("id"), new Season(Integer.valueOf(jsonObject.getString("id")),
                    jsonObject.getString("title"),
                    Integer.valueOf(jsonObject.getString("level_reward")),
                    Integer.valueOf(jsonObject.getString("season_reward"))));
        }

        return map;
    }

    public static void scheduleNotification(Context context, Notification notification, int delay) {
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    public static Notification getNotification(Context context, String content) {
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle("نیم کلمه");
        builder.setContentText(content);
        builder.setSmallIcon(R.drawable.ic_word_cut);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(soundUri);
        return builder.build();
    }

}