package com.taxiapps.wordcut;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.IDataStore;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.FirebaseFirestore;
import com.taxiapps.wordcut.model.json.AchievementJson;
import com.taxiapps.wordcut.model.json.LevelJson;
import com.taxiapps.wordcut.model.json.SeasonJson;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import modules.PublicModules;
import modules.TX_Ad;
import okhttp3.OkHttpClient;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Sabih on 3/8/2018.
 */

public class Application extends MultiDexApplication {

    public static FirebaseAnalytics firebaseAnalytics;
    public static FirebaseFirestore firebaseDB;

    public static String appID;
    public static String appSource;
    public static String appVersion;
    public static String app_fa; // for cafe bazaar
    public static String deviceID;
    public static String userName;
    public static String serverUrl;
    public static String gateWayUrl;
    public static String os;
    public static String currentBase64;


    private String BAZAAR_BASE64 = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwDDGL9FmWyHrZkpnfo/iqtcvr0nLuDkX/7BTL0wrbsqgMz3z56+I1wIzxPIrSgYFZ3vZZjAxNayXAY8inQN9+45pebSKkOFCOHFIEGv5yPiE3RtVcZfOwRxwFzhG1cS7q3pDp+Y6o5xIZWhvmwjcnswn9gB3I1ziyhEDCDdnV8qYVxlk46SNccThlajYjoshTN+pZ/C/48qISekfDn7iYh9sFkRZrgKwRKPKaDFsVUCAwEAAQ==";
    private String MYKET_BASE64 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxPfGf0c3pggeOv61FrpBPb32QUIxpUyXut78GUoQep2nnW8cvjLKk1FbdKgrW2mFPlO7ZcWX+q3Pt3UyjzgW0endi0rHlVkGMM0kqv6ZhCms+Bq9C+guaGwi8TRn4Q2mzLtsxIZzM5c+BKBAv6Kn7s57lq/V4TaCmtq/L0fZyzQIDAQAB";

    public static final String SHARE_LINK_GOOGLE = "https://play.google.com/store/apps/details?id=com.taxiapps.wordcut";
    public static final String SHARE_LINK_BAZAAR = "https://cafebazaar.ir/app/com.taxiapps.wordcut/?l=fa";
    public static final String SHARE_LINK_MYKET = "https://myket.ir/app/com.taxiapps.wordcut";
    public static final int SHARE_REQUEST_CODE = 150;

    public static final String backendlessApplicationID = "B5FBF914-9B11-DAD3-FFED-4267FFA36700";
    public static final String backendlessAndroidApiKey = "B3E0B3BF-176F-63CF-FF39-A81EE8D64C00";

    private void initStoreVariables() {

        appID = "wordcutfa";
        app_fa = "نیم کلمه";

        try {
            appVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        appSource = "google";
        deviceID = PublicModules.macAddressHandling(this, "WORDCUTFA_");
        userName = "";
        serverUrl = "http://api.taxiapps.ir/api/taxiapps";
        gateWayUrl = "http://api.taxiapps.ir/Bank";
        os = "android";

    }

    @Override
    public void onCreate() {
        super.onCreate();

        TX_Ad.AD_DIRECTORY = getFilesDir().getAbsolutePath() + "/" + "ads";

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/newfont_B.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        PreferencesHelper.init(this);
        SeasonJson.initSeasons();
        LevelJson.initLevels();
        AchievementJson.initAchievements();
        initStoreVariables();
        if (appSource.equals("bazaar")) {
            currentBase64 = BAZAAR_BASE64;
        }
        if (appSource.equals("myket")) {
            currentBase64 = MYKET_BASE64;
        }
    }

    /**
     * baray ine ke user age zabune system ro avaz kard
     * app kharab nashe
     *
     * @param context
     */
    public static void localization(Context context) {
        Resources res = context.getResources();

        Locale locale = new Locale("en");
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.locale = locale;

        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    public static void backendlessHandling(String eventName, AsyncCallback callback) {

        int score = Integer.parseInt(PreferencesHelper.getEventSettings(eventName, PreferencesHelper.eventUserScore));
        String name = PreferencesHelper.getEventSettings(eventName, PreferencesHelper.eventUserName);
        String phone = PreferencesHelper.getEventSettings(eventName, PreferencesHelper.eventUserPhone);
        String email = PreferencesHelper.getEventSettings(eventName, PreferencesHelper.eventUserMail);

        IDataStore<Map> contactStorage = Backendless.Data.of(eventName + "_results");
        DataQueryBuilder queryBuilder = DataQueryBuilder.create();
        queryBuilder.setWhereClause("phone =" + phone);

        contactStorage.find(queryBuilder, new AsyncCallback<List<Map>>() {
            @Override
            public void handleResponse(List<Map> response) {

                HashMap<String, Object> user = new HashMap();
                user.put("appsource", Application.appSource);
                user.put("email", email);
                user.put("fullname", name);
                user.put("phone", phone);
                user.put("score", score);

                if (response.size() > 0) {
                    HashMap<String, Object> found_user = (HashMap<String, Object>) response.get(0);
                    found_user.put("score", user.get("score"));
                    found_user.put("fullname", user.get("fullname"));
                    found_user.put("email", user.get("email"));
                    user = found_user;
                }

                Backendless.Data.of(eventName + "_results").save(user, callback);
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });


    }

}
