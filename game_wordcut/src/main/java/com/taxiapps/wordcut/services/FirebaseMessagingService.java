package com.taxiapps.wordcut.services;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.wordcut.Application;
import com.taxiapps.wordcut.ui.activity.GamePlayActivity;
import com.taxiapps.wordcut.ui.activity.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import TxAnalytics.TxAnalytics;
import modules.PublicModules;

import static TxAnalytics.TxAnalytics.MessageAdd;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private static String i;
    private String customField;
    private JSONObject custom;
    private JSONObject urlDetails;
    private String title = "", message = "", url = "";

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        try {
            customField = remoteMessage.getData().get("custom");
            custom = new JSONObject(PublicModules.makeJsonString(customField));
            i = custom.getString("i");
            if (custom.has("a")) {
                urlDetails = custom.getJSONObject("a");
                title = urlDetails.getString("tx_pop_title");
                message = urlDetails.getString("tx_pop_msg");
                url = urlDetails.getString("tx_pop_url");
            }
            MessageAdd(i, TxAnalytics.MessageTypes.PUSH, Application.appID, Application.appSource, Application.appVersion, Application.deviceID, "", Application.serverUrl, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        Log.i("Response", response.body().string());
                        Log.i("MessageAdd", "message sent");
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try { // methode appIsForeground bayad hamrah ba try catch bashe
            if (MainActivity.getInstance() != null) {// agar ke main activity null nabud.albate hichvaght pish nemiad vali mahze ehtiat gozashtam
                if (GamePlayActivity.getInstance() != null) {
                    if (!GamePlayActivity.getInstance().isFinishing()) { // agar ke tu safhe i az baseActivity budim ke finish nashode bud
                        if (PublicModules.appIsForeground(GamePlayActivity.getInstance()) || !GamePlayActivity.getInstance().isFinishing()) // check mikonim ke app forground bashe
                            if (message.equals("") || title.equals("")) { // agar ke server custom message nafrestade bud
                                PublicModules.showPushPopup(GamePlayActivity.getInstance(), remoteMessage.getData().get("title"), remoteMessage.getData().get("alert"), Application.appSource, ""); // bia ye popup mamuli neshun bede
                            } else {
                                PublicModules.showPushPopup(GamePlayActivity.getInstance(), title, message, Application.appSource, url); // agar  ke server custom message ferestade bud bia ba url bazesh kon
                            }
                        else { // age app background bud bia notification besaz
                            makeNotification(remoteMessage);
                        }
                    }
                } else {
                    if (MainActivity.getInstance() != null) { // code payin baray MainActivitye ke az base activity extend nemikone{
                        if (PublicModules.appIsForeground(getApplicationContext()) || !MainActivity.getInstance().isFinishing()) {
                            Log.i("PUSH", "main not finish");
                            if (message.equals("") || title.equals("")) { // pop up ba url neshun bedam ya na
                                PublicModules.showPushPopup(MainActivity.getInstance(), remoteMessage.getData().get("title"), remoteMessage.getData().get("alert"), Application.appSource, "");// neshun nade
                            } else {
                                PublicModules.showPushPopup(MainActivity.getInstance(), title, message, Application.appSource, url); // neshun bede
                            }
                        } else {
                            if (MainActivity.getInstance().isFinishing()) {
                                makeNotification(remoteMessage);
                            }
                        }
                    }
                }
            } else { // when app is absolutely terminate
                makeNotification(remoteMessage);
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void makeNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        NotificationCompat.Builder notiBuilder = new NotificationCompat.Builder(this);
        //
        Log.i("i", i);
        intent.putExtra("i", i);
        intent.putExtra("popUpTitle", remoteMessage.getData().get("title"));
        intent.putExtra("popUpContent", remoteMessage.getData().get("alert"));
        intent.putExtra("popUpURL", url);
        notiBuilder.setContentTitle(remoteMessage.getData().get("title"));
        notiBuilder.setContentText(remoteMessage.getData().get("alert"));
        //
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        notiBuilder.setAutoCancel(true);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notiBuilder.setSound(uri);
        notiBuilder.setSmallIcon(com.taxiapps.wordcut.R.drawable.ic_word_cut);
        notiBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notiBuilder.build());
    }

}
