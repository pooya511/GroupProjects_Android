package com.taxiapps.wordcut.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.wordcut.Application;

import org.json.JSONException;
import org.json.JSONObject;

import com.taxiapps.wordcut.utils.payment.Payment;

import java.io.IOException;

import modules.PublicModules;

import static TxAnalytics.TxAnalytics.UpdateClientPushID;

public class FirebaseService extends FirebaseInstanceIdService {

    private static final String REG_TOKEN = "REG_TOKEN";

    @Override
    public void onTokenRefresh() {
        String recentToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN, recentToken);
        if (recentToken != null && !recentToken.equals("")) {
            try {
                UpdateClientPushID(recentToken, Application.appID, Application.appSource, Application.appVersion, Application.deviceID, Application.os, "", Application.serverUrl, new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {

                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        if (response.isSuccessful()) {
                            String responseStr = response.body().string();
                            try {
                                JSONObject responseJson = new JSONObject(PublicModules.makeJsonString(responseStr));
                                if (responseJson.getString("status").equals(Payment.SUCCESS)) {
                                    Log.d(REG_TOKEN, "status is " + responseJson.getString("status"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
