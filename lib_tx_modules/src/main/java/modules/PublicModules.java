package modules;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.lib_modules.R;

import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import de.cketti.mailto.EmailIntentBuilder;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

/**
 * Created by Parsa on 9/20/2017.
 */

public class PublicModules {

    public static long[] getMonthInterval(int offset) {

        long[] res = new long[2];

        int nowYear;
        int nowMonth;
        int resYear;
        int resMonth;

        PersianDate calendar = new PersianDate();
        calendar.setHour(0);
        calendar.setMinute(0);
        calendar.setSecond(0);

        nowYear = calendar.getShYear();
        nowMonth = calendar.getShMonth() - 1;

        resMonth = nowMonth + offset;
        resYear = (int) (nowYear + Math.floor(resMonth / 12f));
        resMonth = ((resMonth % 12) + 12) % 12;

        PersianDate start = new PersianDate(calendar.getTime());
        start.setShYear(resYear);
        start.setShDay(1);
        start.setShMonth(resMonth + 1);

        res[0] = start.getTime();

        resMonth = nowMonth + offset + 1;
        resYear = (int) (nowYear + Math.floor(resMonth / 12f));
        resMonth = ((resMonth % 12) + 12) % 12;

        PersianDate end = new PersianDate(calendar.getTime());
        end.setShYear(resYear);
        end.setShDay(1);
        end.setShMonth(resMonth + 1);

        res[1] = end.getTime();

        return res;
    }

    public static long[] getYearInterval(int offset) {

        long[] res = new long[2];

        PersianDate calendar = new PersianDate();
        calendar.setHour(0);
        calendar.setMinute(0);
        calendar.setSecond(0);

        PersianDate pCalendar = new PersianDate(calendar.getTime());
        pCalendar.setShDay(1);
        pCalendar.setShMonth(1);
        //   Log.i("pCalendar", pCalendar.getPersianLongDateAndTime());
        res[0] = pCalendar.getTime();

        pCalendar.setShYear(pCalendar.getShYear() + offset);
        //       Log.i("pCalendar", pCalendar.getPersianLongDateAndTime());
        res[1] = pCalendar.getTime();

        return res;
    }

    public static long[] getDayInterval() {

        long[] res = new long[2];

        PersianDate calendar = new PersianDate();
        calendar.setHour(0);
        calendar.setMinute(0);
        calendar.setSecond(1);

        //  Log.i("PersianCalendar S", calendar.getPersianLongDateAndTime());
        res[0] = calendar.getTime();

        calendar.setHour(23);
        calendar.setMinute(59);
        calendar.setSecond(59);

        //     Log.i("PersianCalendar E", calendar.getPersianLongDateAndTime());
        res[1] = calendar.getTime();

        return res;
    }

    public static long[] getWeekInterval() {

        long[] res = new long[2];

        PersianDate pCalendar = new PersianDate();

//        if (pCalendar.get(PersianCalendar.DAY_OF_WEEK) != pCalendar.getFirstDayOfWeek()) {
//            pCalendar.set(PersianCalendar.DAY_OF_WEEK, pCalendar.getFirstDayOfWeek());
//            pCalendar.setPersianDate(pCalendar.getShYear(), pCalendar.getShMonth(), pCalendar.getShDay() - 1);
//        }
        switch (pCalendar.dayOfWeek()) {
            case 0:
                break;
            case 1:
                pCalendar.addDay(-1);
                break;
            case 2:
                pCalendar.addDay(-2);
                break;
            case 3:
                pCalendar.addDay(-3);
                break;
            case 4:
                pCalendar.addDay(-4);
                break;
            case 5:
                pCalendar.addDay(-5);
                break;
            case 6:
                pCalendar.addDay(-6);
                break;


        }

        pCalendar.setHour(0);
        pCalendar.setMinute(0);
        pCalendar.setSecond(0);
//        Log.i("pCalendar S", pCalendar.getPersianLongDateAndTime());
        res[0] = pCalendar.getTime();

        pCalendar.addWeek(1);
//        Log.i("pCalendar E", pCalendar.getPersianLongDateAndTime());
        res[1] = pCalendar.getTime();

        return res;
    }

    public static long[] getCustomInterval(int startOffset, int endOffset) {

        long[] res = new long[2];

        PersianDate persianCalendar = new PersianDate();

        long start;
        long end;

//        persianCalendar.setPersianDate(persianCalendar.getShYear(), persianCalendar.getShMonth(), persianCalendar.getShDay() + (startOffset + 1));
        persianCalendar.setShDay(persianCalendar.getShDay() + (startOffset + 1));
        start = persianCalendar.getTime();
        res[0] = start;

        persianCalendar = new PersianDate();
//        persianCalendar.setPersianDate(persianCalendar.getShYear(), persianCalendar.getShMonth(), persianCalendar.getShDay() + endOffset);
        persianCalendar.setShDay(persianCalendar.getShDay() + endOffset);
        end = persianCalendar.getTime();
        res[1] = end;
        // + (endOffset + 1)

        return res;
    }

    public static boolean dateIsValid(String year, String month, String day) {
        boolean isFirst6Month;
        int yearInt = 0;
        int monthInt = 0;
        int dayInt = 0;

        if (!year.equals("") && !month.equals("") && !day.equals("")) {
            yearInt = Integer.parseInt(year);
            monthInt = Integer.parseInt(month);
            dayInt = Integer.parseInt(day);
        }

        if (yearInt < 1300 || yearInt > 1499) {
            return false;
        }

        Log.i("Year Integer", String.valueOf(yearInt));
        Log.i("is leap year", String.valueOf(new PersianDate().isLeap(yearInt)));

        if (monthInt > 0 && monthInt <= 6) {
            isFirst6Month = true;
        } else if (monthInt > 6 && monthInt <= 12) {
            isFirst6Month = false;
        } else {
            return false;
        }
        if (isFirst6Month) {
            if (dayInt <= 0 || dayInt > 31) {
                return false;
            }
        } else {
            if (dayInt <= 0 || dayInt > 30) {
                return false;
            } else {
                PersianDate persianCalendar = new PersianDate();
//                persianCalendar.setPersianDate(yearInt, 1, 1);
                persianCalendar.setShYear(yearInt);
                persianCalendar.setShMonth(1);
                persianCalendar.setShDay(1);
//                Log.i("persian short date", persianCalendar.getPersianShortDate());
//                Log.i("is leap year", String.valueOf(persianCalendar.isPersianLeapYear()));
                if (monthInt == 12 && !persianCalendar.isLeap()) {
                    if (dayInt <= 0 || dayInt > 29) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

//    public static String thousandSeparatorStr(String str) {
//        return NumberFormat.getNumberInstance(Locale.US).format(Double.valueOf(toEnglishDigit(str)));
//    }

    public static long dateDiff(long date1, long date2, String component) {

        PersianDate pc1 = new PersianDate(date1);
        pc1.setHour(0);
        pc1.setMinute(0);
        pc1.setSecond(0);

        PersianDate pc2 = new PersianDate(date2);
        pc2.setHour(0);
        pc2.setMinute(0);
        pc2.setSecond(0);

        int divideValue;

        switch (component) {

            case "year":
                return pc1.getShYear() - pc2.getShYear();
            case "month":
                return (pc1.getShYear() * 12 + pc1.getShMonth()) - (pc2.getShYear() * 12 + pc2.getShMonth());
            case "week":
                divideValue = 7 * 24 * 60 * 60 * 1000;
                break;
            case "day":
                divideValue = 24 * 60 * 60 * 1000;
                break;
            case "hour":
                divideValue = 60 * 60 * 1000;
                break;
            case "minute":
                divideValue = 60 * 1000;
                break;
            case "second":
                divideValue = 1000;
                break;
            default:
                divideValue = 1;
                break;
        }

        return ((pc2.getTime() - pc1.getTime()) / divideValue);
    }

    public static String timeStamp2String(long date, String format) {

        PersianDate persianCalendar = new PersianDate(date);

        String year;
        String month;

        switch (format) {

            case "yy mm dd":
                return String.valueOf(persianCalendar.getShYear()).substring(2) + " " + String.valueOf(persianCalendar.getShMonth()) + " " +
                        String.valueOf(persianCalendar.getShDay());

            case "yyyy mm dd":
                return String.valueOf(persianCalendar.getShYear()) + "/" + String.valueOf(persianCalendar.getShMonth()) + "/" +
                        String.valueOf(persianCalendar.getShDay());

            case "yyyy mm":
                return String.valueOf(persianCalendar.getShYear()) + " " + String.valueOf(persianCalendar.getShMonth());

            case "yy mm":
                return String.valueOf(persianCalendar.getShYear()).substring(2) + " " + String.valueOf(persianCalendar.getShMonth());

            case "yy MM dd":
                return String.valueOf(persianCalendar.getShYear()).substring(2) + " " + persianCalendar.monthName() + " " +
                        String.valueOf(persianCalendar.getShDay());

            case "yyyy MM dd":
                return String.valueOf(persianCalendar.getShDay()) + " " + persianCalendar.monthName() + " " +
                        String.valueOf(persianCalendar.getShYear());

            case "yyyy MM":
                return String.valueOf(persianCalendar.getShYear()) + " " + persianCalendar.monthName();

            case "yy MM":
                return persianCalendar.monthName() + String.valueOf(persianCalendar.getShYear()).substring(2);

            case "MMMM yy":
                return persianCalendar.monthName() + " " + String.valueOf(persianCalendar.getShYear()).substring(2);

            case "dd MMMM yyyy":
                return persianCalendar.getShDay() + " " + persianCalendar.monthName() + " " + String.valueOf(persianCalendar.getShYear());

            case "yyyy":
                return String.valueOf(persianCalendar.getShYear());

            case "MMMM":
                return persianCalendar.monthName();

            case "yyyy/MM/dd HH:mm":

                return String.valueOf(persianCalendar.getShYear()) + "/" + String.valueOf(persianCalendar.getShMonth()) + "/" +
                        String.valueOf(persianCalendar.getShDay()) + " " + PersianDateFormat.format(persianCalendar, "H:i");

            case "YYYY":
                return String.valueOf(persianCalendar.getShYear());

            case "hh:mm":

                if (persianCalendar.getHour() < 10)
                    year = "0" + String.valueOf(persianCalendar.getHour());
                else
                    year = String.valueOf(persianCalendar.getHour());

                if (persianCalendar.getMinute() < 10)
                    month = "0" + String.valueOf(persianCalendar.getMinute());
                else
                    month = String.valueOf(persianCalendar.getMinute());

                return year + ":" + month;

            default:
                return PersianDateFormat.format(persianCalendar, "Y/m/d") + " فرمت لازم پشتیبانی نمیشود.";
        }

    }

    public static String string2TimeStamp(String str) {

        int year, month, day;

        String[] strArray = str.split(" ");

//        PersianCalendar pCalendar = new PersianCalendar();
        PersianDate persianDate = new PersianDate();

        String[] date = strArray[0].split("/");

        year = Integer.parseInt(date[0]);
        month = Integer.parseInt(date[1]);
        day = Integer.parseInt(date[2]);

        // -1 be dalile in ke khode persian calendar 1 dune ezafe mikone

//        pCalendar.setPersianDate(year, month - 1, day);
//        pCalendar.set(PersianCalendar.HOUR_OF_DAY, 0);
//        pCalendar.set(PersianCalendar.MINUTE, 0);
//        pCalendar.set(PersianCalendar.SECOND, 0);
//        pCalendar.set(PersianCalendar.MILLISECOND, 0);
        persianDate.setShYear(year);
        persianDate.setShMonth(month);
        persianDate.setShDay(day);
        persianDate.setHour(0);
        persianDate.setMinute(0);
        persianDate.setSecond(0);

        if (strArray.length > 1) {
            if (strArray[1].contains(":")) {
                String[] time = strArray[1].split(":");

//                pCalendar.set(PersianCalendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
//                pCalendar.set(PersianCalendar.MINUTE, Integer.valueOf(time[1]));
//                pCalendar.set(PersianCalendar.SECOND, 0);
//                pCalendar.set(PersianCalendar.MILLISECOND, 0);
                persianDate.setHour(Integer.valueOf(time[0]));
                persianDate.setMinute(Integer.valueOf(time[1]));
                persianDate.setSecond(0);

            }
        }
        return String.valueOf(persianDate.getTime());

    }

    public static String toEnglishDigit(String persianDigit) {
        if (persianDigit == null) {
            persianDigit = "";
        }
        return persianDigit.replace("۰", "0").replace("۱", "1").replace("۲", "2").replace("۳", "3").replace("۴", "4").replace("۵", "5")
                .replace("۶", "6").replace("۷", "7").replace("۸", "8").replace("۹", "9");

    }

    public static String toPersianDigit(String englishDigit) {
        if (englishDigit == null) {
            englishDigit = "";
        }
        return englishDigit.replace("0", "۰").replace("1", "۱").replace("2", "۲").replace("3", "۳").replace("4", "۴").replace("5", "۵")
                .replace("6", "۶").replace("7", "۷").replace("8", "۸").replace("9", "۹");
    }

    public static int[] getWidthAndHeight(Context context, int widthPercent, int heightPercent) {
        int widthAndHeight[] = new int[2];
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        widthAndHeight[0] = widthPercent * displayMetrics.widthPixels / 100;
        widthAndHeight[1] = heightPercent * displayMetrics.heightPixels / 100;
        return widthAndHeight;
    }

    public static int screenSize(Context context) {

        int screenSize = context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return 1;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return 2;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return 3;
            default:
                return 4; // alaki
        }
    }

    public static void stringToBitmap(String str, ImageView imgView) {

        byte[] imageAsBytes = Base64.decode(str.getBytes(), 0);
        imgView.setImageBitmap(BitmapFactory.decodeByteArray(
                imageAsBytes, 0, imageAsBytes.length));

    }

    public static void collapseAnimate(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    public static String makeJsonString(String responseStr) {
        if (responseStr.charAt(0) == '\"' && responseStr.charAt(responseStr.length() - 1) == '\"') {
            responseStr = responseStr.substring(1, responseStr.length() - 1);// از بین بردن " های اول و آخر
        }
        responseStr = responseStr.replaceAll("\\\\", "");
        return responseStr;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String macPart = "00" + Integer.toHexString(b & 0xFF);
                    macPart = macPart.length() > 2 ? macPart.substring(macPart.length() - 2) : macPart;
                    res1.append(macPart + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    public static String macAddressHandling(Context context, String appNameUpperCase) {
        String letters = "abcdefghijklmnopqrstuvwxyz0123456789";
        String lettersReverse = "012345abcdef6789ghmrijklstuvwnopqxyz";
        lettersReverse = lettersReverse.toUpperCase();
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//        String macAddress = wifiInfo.getMacAddress();
        String macAddress = getMacAddress();
        macAddress = macAddress.toLowerCase();
        Log.i("mac address", macAddress);
        macAddress = macAddress.replaceAll(":", "");
        Log.i("mac address 2", macAddress);
        for (int i = 0; i < macAddress.length(); i++) {
            int target = letters.indexOf(macAddress.charAt(i));
            StringBuilder stringBuilder = new StringBuilder(macAddress);
            stringBuilder.setCharAt(i, lettersReverse.charAt(target));
            macAddress = stringBuilder.toString();
        }
        Log.i("mac address", macAddress);
        /**
         * bayad aval app Id ro add konim behesh
         */
        macAddress = appNameUpperCase + macAddress;
        return macAddress;
    }

    public static boolean appIsForeground(Context context) throws ExecutionException, InterruptedException {
        return new ForegroundCheckTask().execute(context).get();
    }

    public static void showPushPopup(final Context context, final String title, final String content, final String appSource, final String link) {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                PublicDialogs.makeMessageDialog(context, PublicDialogs.MessageDialogTypes.NORMAL, title, content, link, appSource).show();
            }
        });
    }

    public static void customToast(Context context, String title, String description, boolean isSuccess) {

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom,
                (ViewGroup) ((Activity) context).findViewById(R.id.toast_layout_root));

        ImageView image = layout.findViewById(R.id.toast_img);
        if (isSuccess)
            image.setImageResource(R.drawable.ic_success);
        else
            image.setImageResource(R.drawable.ic_unsuccess);
        TextView titleText = layout.findViewById(R.id.toast_title);
        titleText.setText(title);
        TextView descriptionText = layout.findViewById(R.id.toast_description);
        descriptionText.setText(description);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 25);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static boolean getBooleanWithChance(double chance) {

        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0));
        Collections.shuffle(arrayList);

        double result = arrayList.size() * chance;

        return arrayList.get(0) < result;

    }

    public static void contactUsByEmail(Context context, String appName, String appVersion) {

        String screenSize;
        switch (screenSize(context)) {
            case 1:
                screenSize = "Normal";
                break;
            case 2:
                screenSize = "Large";
                break;
            case 3:
                screenSize = "XLarge";
                break;
            default:
                screenSize = "Normal";
                break;
        }

        Intent emailIntent = EmailIntentBuilder.from(context)
                .to("support@taxiapps.org")
                .subject("پیام پشتیبانی | " + appName)
                .body("شماره تماس شما:\n" +
                        "پیام شما:" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "appVersion: " + appVersion + "\n" +
                        "deviceOS: Android" + "\n" +
                        "deviceModel: " + Build.MANUFACTURER + " " + Build.MODEL + "\n" +
                        "androidVersion: " + Build.VERSION.SDK_INT + "\n" +
                        "screenSize: " + screenSize + "\n"
                )
                .build();
        context.startActivity(emailIntent);

    }

    public static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

}
