package modules;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.taxiapps.lib_modules.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.zeroturnaround.zip.ZipException;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class TX_Ad extends Dialog {

    private static final String TAG = "AdTest";

    private static final String SERVER_URL = "http://api.taxiapps.ir/api/taxiapps";
    private static final int CRITICAL_TIMEOUT = 40;
    public static final int URL_REQUEST = 10;

    private static OkHttpClient client = new OkHttpClient();
    private Context context;
    private JSONObject ad;
    private String deviceID;
    private String metaData;
    private AdCompleteCallBack adCompleteCallBack;

    public interface AdAPICallBack {
        void getJsonArray(JSONArray result);

        void getStatus(int status);
    }

    public interface AdCompleteCallBack {
        void onComplete(boolean complete);
    }

    public enum AdTypes {

        interestitial("interestitial"),
        reward("reward"),
        banner("banner");

        private final String name;

        AdTypes(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }


    }

    public static String AD_DIRECTORY;

    @SuppressLint("SetJavaScriptEnabled")
    public TX_Ad(final Context context, final String indexUrl, JSONObject ad, String deviceID, String metaData, AdCompleteCallBack adCompleteCallBack) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.pop_advertise);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        getWindow().setLayout(MATCH_PARENT, MATCH_PARENT);

        this.context = context;
        this.ad = ad;
        this.deviceID = deviceID;
        this.metaData = metaData;
        this.adCompleteCallBack = adCompleteCallBack;

        WebView adWebView = findViewById(R.id.pop_advertise_webview);
        WebSettings webSettings = adWebView.getSettings();
        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);


        adWebView.setWebChromeClient(new WebChromeClient());
        adWebView.addJavascriptInterface(new JavaScriptInterface(), "interface");
//        adWebView.loadUrl("file:///android_asset/dakhlokharj_interstitial/index.html");// //TODO delete this line
        adWebView.loadUrl("file:///" + indexUrl);

    }

    public static void TX_AvailableAds(final AdAPICallBack adAPICallBack) {

        JSONObject resJson = new JSONObject();
        final JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            resJson.put("method", "TXA_AvailableAds");
            resJson.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, resJson.toString());
        final Request request = new Request.Builder()
                .url(SERVER_URL)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.i(TAG, "TX_AvailableAds" + e.getMessage());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String responseStr = response.body().string();
                Log.i(TAG, "TX_AvailableAds" + responseStr);
                try {
                    JSONObject responseJsn = new JSONObject(responseStr);
                    int status = (int) responseJsn.get("status");
                    JSONArray jsonArr = (JSONArray) responseJsn.get("ads");

                    adAPICallBack.getJsonArray(jsonArr);
                    adAPICallBack.getStatus(status);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void TX_AD_Get(AdTypes adTypes, String deviceID, String metadata, final AdAPICallBack adAPICallBack) {

        JSONObject resJson = new JSONObject();
        final JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("device_id", deviceID);
            innerJson.put("metadata", metadata);
            innerJson.put("type", adTypes.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            resJson.put("method", "TXA_Ad_Get");
            resJson.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, resJson.toString());
        final Request request = new Request.Builder()
                .url(SERVER_URL)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.i(TAG, "TX_AD_Get" + e.getMessage());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String responseStr = response.body().string();
                Log.i(TAG, "TX_AD_Get" + responseStr);
                try {
                    JSONObject responseJsn = new JSONObject(responseStr);
                    int status = (int) responseJsn.get("status");
                    JSONArray jsonArr = (JSONArray) responseJsn.get("ad");

                    adAPICallBack.getJsonArray(jsonArr);
                    adAPICallBack.getStatus(status);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void TX_AD_Report(JSONObject ad, String reportType, String deviceID, String metadata, final AdAPICallBack adAPICallBack) {

        JSONObject resJson = new JSONObject();
        final JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("ad_id", ad.get("ID"));
            innerJson.put("report_type", reportType);
            innerJson.put("device_id", deviceID);
            innerJson.put("metadata", metadata);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            resJson.put("method", "TXA_Ad_Report");
            resJson.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, resJson.toString());
        final Request request = new Request.Builder()
                .url(SERVER_URL)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.i(TAG, "TX_AD_Report" + e.getMessage());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String responseStr = response.body().string();
                try {
                    int status = (int) new JSONObject(responseStr).get("status");
                    if (adAPICallBack != null) {
                        adAPICallBack.getStatus(status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void showAd(final Context context, AdTypes adType, final String deviceID, final String metaData, final AdCompleteCallBack adCompleteCallBack) {

        TX_Ad.TX_AD_Get(adType, deviceID, "android", new TX_Ad.AdAPICallBack() {
            @Override
            public void getJsonArray(JSONArray result) {
                try {
                    final JSONObject ad = result.getJSONObject(0);
                    final String adName = ad.getString("adName");

                    File adDirectory = new File(AD_DIRECTORY, adName);
                    if (adDirectory.exists()) {
                        final File index = new File(adDirectory, "index.html");
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new TX_Ad(context, index.getAbsolutePath(), ad, deviceID, metaData, adCompleteCallBack).show();
                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getStatus(int status) {

            }
        });

    }

    public static void downloadAds(final Context context, final String adName, String url) {

        final File cache = context.getCacheDir();
        final File downloadFile = new File(cache, adName + ".zip");
        try {
            downloadFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        PRDownloader.download(url, cache.getAbsolutePath(), downloadFile.getName())
                .build().start(new OnDownloadListener() {
            @Override
            public void onDownloadComplete() {
                File extractedFiles = new File(AD_DIRECTORY, adName);
                if (!extractedFiles.exists()) {
                    extractedFiles.mkdir();
                }
                try {
                    ZipUtil.unpack(downloadFile, extractedFiles);
                    downloadFile.delete();
                } catch (ZipException e) {
                    e.getStackTrace();
                }

            }

            @Override
            public void onError(Error error) {

            }
        });
    }

    public static void deleteUnusedAds(ArrayList<String> adsName) {

        File[] listFiles = new File(AD_DIRECTORY).listFiles();
        if (listFiles != null) {
            List<File> files = Arrays.asList(listFiles);

            for (int i = 0; i < files.size(); i++) {
                if (!adsName.contains(files.get(i).getName())) {
                    deleteDirectory(files.get(i));
                }
            }
        }

    }

    private static void deleteDirectory(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        files[i].delete();
                    }
                }
            }
            file.delete();
        }
    }

    class JavaScriptInterface {
        @JavascriptInterface
        public void TXAD_CallBack(final String report, String url) {
            switch (report) {
                case "close":
                    dismiss();
                    TX_Ad.TX_AD_Report(ad, report, deviceID, metaData, null);
                    break;
                case "fill":
                    TX_Ad.TX_AD_Report(ad, report, deviceID, metaData, null);
                    break;
                case "complete":
                    try {
                        adCompleteCallBack.onComplete(ad.getString("adType").equals(AdTypes.reward.toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    TX_Ad.TX_AD_Report(ad, report, deviceID, metaData, null);
                    break;
                case "url":
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    ((Activity) context).startActivityForResult(intent, URL_REQUEST);
                    Intent dataRes = new Intent();
                    dataRes.putExtra("ad", ad.toString());
                    dataRes.putExtra("report", report);
                    dataRes.putExtra("deviceID", deviceID);
                    dataRes.putExtra("metaData", metaData);
                    ((Activity) context).setIntent(dataRes);
                    break;
                case "error":
                    dismiss();
                    adCompleteCallBack.onComplete(false);
                    Toast.makeText(context, "error1", Toast.LENGTH_SHORT).show();
                    break;

            }

        }

    }

    /**
     * this method should be use in OnActivityResult() method !!! Dont Forget That ;)
     *
     * @param ad       ad
     * @param report   report Type
     * @param deviceID deviceID
     * @param metaData OS (android)
     */
    public static void sendReportFromActivity(JSONObject ad, String report, String deviceID, String metaData) {
        TX_Ad.TX_AD_Report(ad, report, deviceID, metaData, null);
    }

}