package modules;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurelhubert.simpleratingbar.SimpleRatingBar;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.lib_modules.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Pattern;

import TxAnalytics.TxAnalytics;

import static TxAnalytics.TxAnalytics.RateUs;

/**
 * Created by Parsa on 2018-03-05.
 */

public class PublicDialogs {

    public enum MessageDialogTypes {

        ERROR("Error"),
        NORMAL("Normal"),
        SUCCESS("Success"),
        WARNING("Warning");

        private final String name;

        private MessageDialogTypes(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }
    }

    public interface RateUsCallBack {
        void call(int rate);
    }

    public interface GetUserNumberCallBack {
        void call(String number);
    }

    //TODO
    public interface PermissionExplainCallBack{
        void ok();
    }
    //TODO

    public static Dialog makeDialog(Context context, View view, int width, int height, boolean isWithStaticHeight) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //
        int ScreenSizeResults[] = PublicModules.getWidthAndHeight(context, width, height);
        lp.width = ScreenSizeResults[0];
        if (isWithStaticHeight)
            lp.height = ScreenSizeResults[1];
        else
            lp.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;

        dialogWindow.setAttributes(lp);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    public static Dialog makeMessageDialog(final Context context, PublicDialogs.MessageDialogTypes mesType, String title, String description, final String link, final String appSource) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_error_msg, null);
        int width;
        int height;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 85;
                height = 21;
                break;
            case 2:
                width = 65;
                height = 17;
                break;
            case 3:
                width = 50;
                height = 13;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }
        final Dialog dialog = makeDialog(context, view, width, height, false);
        //
        final TextView titleTextView = view.findViewById(R.id.dialog_error_msg_title);
        final TextView descriptionTextView = view.findViewById(R.id.dialog_error_msg_description);
        switch (mesType) {
            case SUCCESS:
                titleTextView.setTextColor(Color.parseColor("#29bf91"));
                descriptionTextView.setTextColor(Color.parseColor("#a5a5a5"));
                break;
            case ERROR:
                titleTextView.setTextColor(Color.parseColor("#f44336"));
                break;
            default:
                titleTextView.setTextColor(Color.BLACK);
                break;
        }
        titleTextView.setText(title);
        descriptionTextView.setText(description);
        final LinearLayout closeButton = view.findViewById(R.id.dialog_rate_us_cancel);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (!link.equals("")) {
                    Intent intention = null;
                    if (link.equals("market://details?id=com.taxiapps.dakhlokharj")) {
                        switch (appSource) {
                            case "google":
                                intention = new Intent(Intent.ACTION_VIEW);
                                intention.setPackage("com.android.vending");
                                intention.setData(Uri.parse(link));
                                break;
                            case "bazaar":
                                intention = new Intent(Intent.ACTION_EDIT);
                                intention.setPackage("com.farsitel.bazaar");
                                intention.setData(Uri.parse(link));
                                break;
                            case "myket":
                                intention = new Intent(Intent.ACTION_VIEW);
                                intention.setPackage("ir.mservices.market");
                                intention.setData(Uri.parse(link));
                                break;
                        }
                    } else {
                        intention = new Intent(Intent.ACTION_VIEW);
                        intention.setData(Uri.parse("http://" + link));
                    }
                    context.startActivity(intention);
                }
            }
        });
        //
        return dialog;
    }

    public static Dialog spinnerDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View spinnerDialogLayout = inflater.inflate(R.layout.pop_spinner, null);

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(spinnerDialogLayout);
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //
        lp.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        lp.width = ConstraintLayout.LayoutParams.WRAP_CONTENT;


        dialogWindow.setAttributes(lp);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    public static Dialog rateUsDialog(final Context context
            , int rate
            , final String appID
            , final String appVersion
            , final String appSource
            , final String deviceId
            , final String userNumber
            , final String serverUrl
            , final RateUsCallBack rateUsCallBack) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_rate_us, null);
        int width;
        int height;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 85;
                height = 38;
                break;
            case 2:
                width = 64;
                height = 26;
                break;
            case 3:
                width = 50;
                height = 20;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }
        final Dialog dialog = makeDialog(context, view, width, height, false);
        //
        final EditText commentEditText = view.findViewById(R.id.dialog_rate_us_comment_edittext);
        final SimpleRatingBar ratingBar = view.findViewById(R.id.dialog_rate_us_ratingbar);
        final LinearLayout submit = view.findViewById(R.id.dialog_rate_us_submit);
        final LinearLayout cancel = view.findViewById(R.id.dialog_rate_us_cancel);
        //
        if (rate == 0)
            ratingBar.setRating(3);
        else
            ratingBar.setRating(rate);
        //
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.equals(cancel)) {
                    dialog.dismiss();
                }
                if (view.equals(submit)) {
                    if (PublicModules.isNetworkAvailable(context)) {
                        final String stars = String.valueOf(ratingBar.getRating());
                        Log.i("Rating Bar", stars);
                        try {
                            RateUs(stars, appID, appSource, appVersion, deviceId, userNumber, serverUrl, new Callback() {

                                private String rateLink;

                                @Override
                                public void onFailure(Request request, IOException e) {
                                    Log.i("RateUs", "Not working");
                                }

                                @Override
                                public void onResponse(Response response) throws IOException {
                                    if (response.isSuccessful()) {
                                        Log.i("RateUs", "is working");
                                        try {
                                            String responseStr = response.body().string();
                                            Log.i("rating responseStr", responseStr);
                                            JSONObject responseJson = new JSONObject(PublicModules.makeJsonString(responseStr));
                                            final String retMessage = responseJson.getString("ret_msg");
                                            rateLink = "";
                                            if (!responseJson.getString("rate_link").equals("")) {
                                                rateLink = responseJson.getString("rate_link");
                                            }
                                            rateUsCallBack.call(ratingBar.getRating());
                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    makeMessageDialog(context, MessageDialogTypes.SUCCESS, "موفق", retMessage, rateLink, appSource).show();
                                                }
                                            });
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String comment;
                        if (!commentEditText.getText().toString().equals("")) {
                            comment = commentEditText.getText().toString();
                            try {
                                TxAnalytics.MessageAdd(comment, TxAnalytics.MessageTypes.MESSAGE, appID, appSource, appVersion, deviceId, userNumber, serverUrl, new Callback() {
                                    @Override
                                    public void onFailure(Request request, IOException e) {
                                        Log.i("message sending", "not working");
                                    }

                                    @Override
                                    public void onResponse(Response response) throws IOException {
                                        if (response.isSuccessful()) {
                                            Log.i("message sending", "is working");
                                        }
                                    }
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        dialog.dismiss();
                    } else {
                        makeMessageDialog(context, MessageDialogTypes.ERROR, "خطای اینترنت", "لطفا به اینترنت متصل شوید", "", appSource).show();
                    }
                }
            }
        };
        //
        submit.setOnClickListener(listener);
        cancel.setOnClickListener(listener);
        //
        return dialog;
    }

    public static Dialog getUserNumber(final Context context, final int resourceId, final String appId, final String appSource, final String appVersion, final String deviceId, final String userNumberStr, final String serverUrl, final GetUserNumberCallBack getUserNumberCallBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_user_phone_number, null);

        final EditText userNumber = view.findViewById(R.id.dialog_user_number_number_edittext);
        final Button save = view.findViewById(R.id.dialog_user_number_done_button);
        final Button clearBtn = view.findViewById(R.id.dialog_user_number_clear_button);
        final TextView cancel = view.findViewById(R.id.dialog_user_number_cancel);
        final ImageView logo = view.findViewById(R.id.tx_payment_info_logo);
        logo.setBackgroundResource(resourceId);

        int width;
        int height;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 74;
                height = 55;
                break;
            case 2:
                width = 55;
                height = 45;
                break;
            case 3:
                width = 45;
                height = 35;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }

        final Dialog dialog = makeDialog(context, view, width, height, true);

        View.OnClickListener itemsListeners = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (view.equals(save)) {

                    if (userNumber.getText().toString().matches("(^[0][9].{9}$)")) {

                        Pattern pattern = Pattern.compile("(^[0][9].{9}$)");
                        if (pattern.matcher(userNumber.getText().toString()).matches()) {
                            getUserNumberCallBack.call(userNumber.getText().toString());
                            dialog.dismiss();
                            PublicDialogs.sendMessageOrBugDialog(context, resourceId, appId, appSource, appVersion, deviceId, userNumberStr, serverUrl).show();
                        } else {
                            clearBtn.setVisibility(View.VISIBLE);
                            PublicDialogs.makeMessageDialog(context, MessageDialogTypes.ERROR, "خطا", "شماره همراه وارد شده صحیح نیست !", "", appSource).show();
                        }

                    } else {
                        PublicDialogs.makeMessageDialog(context, MessageDialogTypes.ERROR, "خطا", "شماره موبایل وارد شده معتبر نیست.", "", appSource).show();
                        userNumber.setText("");
                    }

                }

                if (view.equals(cancel))
                    dialog.dismiss();

                if (view.equals(clearBtn))
                    userNumber.setText("");


            }
        };

        TextWatcher userNumberWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String input = userNumber.getText().toString();

                if (input.length() > 0)
                    clearBtn.setVisibility(View.VISIBLE);
                else
                    clearBtn.setVisibility(View.GONE);

//                if (input.length() == 11)
//                    AppModules.hideSoftKeyboard(context);

            }

        };

        userNumber.addTextChangedListener(userNumberWatcher);

        save.setOnClickListener(itemsListeners);
        cancel.setOnClickListener(itemsListeners);
        clearBtn.setOnClickListener(itemsListeners);

        return dialog;
    }

    public static Dialog sendMessageOrBugDialog(final Context context, int resourceId, final String appId, final String appSource, final String appVersion, final String deviceId, final String userNumber, final String serverUrl) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_send_message, null);
        int width;
        int height;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 85;
                height = 70;
                break;
            case 2:
                width = 55;
                height = 47;
                break;
            case 3:
                width = 42;
                height = 37;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }
        final Dialog dialog = makeDialog(context, view, width, height, true);
        //
        final EditText message = view.findViewById(R.id.setting_send_message_edittext);
        final TextView clearMessage = view.findViewById(R.id.setting_send_message_clear_edittext);
        final Button send = view.findViewById(R.id.setting_send_message_send_button);
        final ImageView logo = view.findViewById(R.id.pop_send_message_logo_img);
        logo.setBackgroundResource(resourceId);
        //
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.equals(clearMessage)) {
                    message.setText("");
                }
                if (view.equals(send)) {
                    if (!message.getText().toString().equals("")) {
                        try {
                            TxAnalytics.MessageAdd(message.getText().toString(), TxAnalytics.MessageTypes.BUG, appId, appSource, appVersion, deviceId, userNumber, serverUrl,
                                    new Callback() {
                                        @Override
                                        public void onFailure(Request request, IOException e) {
                                            Log.i("MessageAdd_Bug", "Not Working");
                                        }

                                        @Override
                                        public void onResponse(Response response) throws IOException {
                                            if (response.isSuccessful()) {
                                                Log.i("MessageAdd_Bug", "is Working");
                                                try {
                                                    String responseStr = response.body().string();
                                                    Log.i("responseStr", responseStr);
                                                    JSONObject responseJson = new JSONObject(PublicModules.makeJsonString(responseStr));
                                                    final String retMessage = responseJson.getString("ret_msg");
                                                    ((Activity) context).runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            makeMessageDialog(context, MessageDialogTypes.SUCCESS, "موفق", retMessage, "", appSource).show();
                                                            dialog.dismiss();
                                                        }
                                                    });
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        makeMessageDialog(context, MessageDialogTypes.ERROR, "محتوا خالی !", "فیلد شما خالی است.", "", appSource).show();
                    }
                }
            }
        };
        clearMessage.setOnClickListener(listener);
        send.setOnClickListener(listener);
        //
        return dialog;
    }

    public static Dialog telegramDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_telegram, null);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width;
        int height;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 100;
                height = 38;
                break;
            case 2:
                width = 64;
                height = 26;
                break;
            case 3:
                width = 50;
                height = 20;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }
        final Dialog dialog = PublicDialogs.makeDialog(context, view, width, height, false);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        final TextView channel = view.findViewById(R.id.dialog_telegram_see_channel);
        final TextView close = view.findViewById(R.id.dialog_telegram_cancel);
        //

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.equals(close)) {
                    dialog.dismiss();
                }
                if (view.equals(channel)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/taxiapps"));
                    context.startActivity(intent);
                    dialog.dismiss();
                }
            }
        };
        //
        close.setOnClickListener(listener);
        channel.setOnClickListener(listener);

        return dialog;
    }


    //TODO
    public static Dialog permissionExplainDialog(final Context context, String permission , final PermissionExplainCallBack permissionExplainCallBack) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_permission_explain);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        String allow = "ALLOW";
        TextView desc = dialog.findViewById(R.id.pop_permission_explain_desc);
        Button ok = dialog.findViewById(R.id.pop_permission_explain_ok_btn);

        String descStr = "برای " + permission + " احتیاج به دسترسی گوشی هست لطفا دسترسی را " + allow + " کنید. در غیر این صورت کارآیی برنامه با مشکل مواجه خواهد شد.";

        Spannable res = new SpannableString(PublicModules.toPersianDigit(descStr));
        res.setSpan(new ForegroundColorSpan(Color.parseColor("#36B19E")), descStr.indexOf(permission), descStr.indexOf(permission) + permission.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        res.setSpan(new ForegroundColorSpan(Color.parseColor("#36B19E")), descStr.indexOf(allow), descStr.indexOf(allow) + allow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        desc.setText(res);

        ok.setOnClickListener(v -> {
            dialog.dismiss();
            permissionExplainCallBack.ok();
        });

        return dialog;
    }

    public static class AlwaysDeny extends Dialog {
        public AlwaysDeny(@NonNull final Context context, String titleStr, String descStr) {
            super(context);
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.setContentView(R.layout.pop_allways_deny);
            this.setCanceledOnTouchOutside(true);
            this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);

            final TextView cancel = this.findViewById(R.id.pop_allways_deny_cancel);
            final TextView goToSetting = this.findViewById(R.id.pop_allways_deny_go_to_setting);
            TextView title = this.findViewById(R.id.pop_always_deny_title);
            TextView desc = this.findViewById(R.id.pop_always_deny_description);

            title.setText(titleStr);
            desc.setText(descStr);

            View.OnClickListener listeners = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.equals(cancel)){
                        dismiss();
                    }
                    if (v.equals(goToSetting)){
                        dismiss();
                        Intent intent = new Intent();
                        intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                        intent.setData(uri);
                        context.startActivity(intent);
                    }
                }
            };

            cancel.setOnClickListener(listeners);
            goToSetting.setOnClickListener(listeners);
        }
    }
    //TODO

}
