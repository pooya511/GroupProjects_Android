package TxAnalytics;

import android.util.Log;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Sabih on 3/5/2018.
 */

public class TxAnalytics {

   private static OkHttpClient client = new OkHttpClient();

    public enum MessageTypes {

        FUN("FUN"),
        RATE("RATE"),
        BUG("BUG"),
        USERPHONE("USERPHONE"),
        EXCEPTION("EXCEPTION"),
        MESSAGE("MESSAGE"),
        PUSH("Push"),
        ADCLICK("ADCLICK");

        private final String name;

        private MessageTypes(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }
    }

    //////
    public static void MessageAdd(String msg,
                                  MessageTypes msg_type,
                                  String appID,
                                  String appSource,
                                  String appVersion,
                                  String deviceID,
                                  String userNumber,
                                  String serverUrl,
                                  Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("msg", msg);
            innerJson.put("msgtype", msg_type);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "MessageAdd");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void GeneratePINVerifyMethod1(String appID,
                                                String appSource,
                                                String appVersion,
                                                String deviceID,
                                                String userNumber,
                                                String serverUrl,
                                                Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "GeneratePINVerifyMethod1");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    ////
    public static void GeneratePINVerifyMethod2(String appID,
                                                String appSource,
                                                String appVersion,
                                                String deviceID,
                                                String userNumber,
                                                String serverUrl,
                                                Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "GeneratePINVerifyMethod2");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void CheckPINVerifyMethod1(String pin,
                                             String appID,
                                             String appSource,
                                             String appVersion,
                                             String deviceID,
                                             String userNumber,
                                             String serverUrl,
                                             Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("username", userNumber);
            innerJson.put("pin", pin);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "CheckPINVerifyMethod1");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void CheckPINVerifyMethod2(String appID,
                                             String appSource,
                                             String appVersion,
                                             String deviceID,
                                             String userNumber,
                                             String serverUrl,
                                             Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "CheckPINVerifyMethod2");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void PurchaseIsActive(String appID,
                                        String appSource,
                                        String appVersion,
                                        String deviceID,
                                        String userNumber,
                                        String serverUrl,
                                        Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "PurchaseIsActive");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void AppUse(String appID,
                              String appSource,
                              String appVersion,
                              String deviceID,
                              String serverUrl,
                              Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            Log.i("AppUse: ", "step 1 OK");
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("AppUse: ", "step 1 !OK");
        }

        try {
            jsonObject.put("method", "AppUse");
            jsonObject.put("params", innerJson);
            Log.i("AppUse: ", "step 2 OK");
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("AppUse: ", "step 2 !OK");
        }

        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void UpdateClientPushID(String pushid,
                                          String appID,
                                          String appSource,
                                          String appVersion,
                                          String deviceID,
                                          String os,
                                          String userNumber,
                                          String serverUrl,
                                          Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("pushid", pushid);
            innerJson.put("os", os);

            if (!userNumber.equals(""))
                innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "AddPushNotification_Client");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void RateUs(String stars,
                              String appID,
                              String appSource,
                              String appVersion,
                              String deviceID,
                              String userNumber,
                              String serverUrl,
                              Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("stars", stars);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "RateUs");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void VersionCheck(String appID,
                                    String appSource,
                                    String appVersion,
                                    String deviceID,
                                    String serverUrl,
                                    Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "VersionCheck");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void PurchaseVerify(String pcode,
                                      String serverUrl,
                                      Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("pcode", pcode);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "PurchaseVerify");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void GetProducts(String appID,
                                   String appSource,
                                   String deviceID,
                                   String userNumber,
                                   String serverUrl,
                                   Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("guid", deviceID);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "GetProducts");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void PurchaseByCoupon(String coupon,
                                        String product,
                                        String appID,
                                        String appSource,
                                        String appVersion,
                                        String deviceID,
                                        String userNumber,
                                        String serverUrl,
                                        Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("product", product);
            innerJson.put("coupon", coupon);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "PurchaseByCoupon2");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void ReactivateLicense(String licenseId,
                                         String appID,
                                         String appSource,
                                         String appVersion,
                                         String deviceID,
                                         String userNumber,
                                         String serverUrl,
                                         Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("version", appVersion);
            innerJson.put("guid", deviceID);
            innerJson.put("licenseid", licenseId);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "ReactivateLicense");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static void GetUserLicenses(String appID,
                                       String appSource,
                                       String userNumber,
                                       String serverUrl,
                                       Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("appid", appID);
            innerJson.put("source", appSource);
            innerJson.put("username", userNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "GetUserLicenses");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }
    /////

    public static void CouponGet(String coupon,
                                 String serverUrl,
                                 Callback callback) throws IOException {
        JSONObject jsonObject = new JSONObject();
        JSONObject innerJson = new JSONObject();
        try {
            innerJson.put("coupon", coupon);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObject.put("method", "CouponGet");
            jsonObject.put("params", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(serverUrl)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    /////

    public static String getBankRedirectURL(String productContent,
                                            String aCoupon,
                                            String gateWayUrl,
                                            String appID,
                                            String appSource,
                                            String appVersion,
                                            String deviceID,
                                            String userNumber) {

        String ret = gateWayUrl + "/" + appID + "/" + appSource + "/" + appVersion + "/" + productContent + "/" + deviceID + "/" + userNumber;
        if (!aCoupon.trim().equals("")) {
            ret = gateWayUrl + "/" + appID + "/" + appSource + "/" + appVersion + "/" + productContent + "/" + deviceID + "/" + userNumber + "/" + aCoupon;
        }
        Log.i("URL", ret);
        return ret;
    }

}
