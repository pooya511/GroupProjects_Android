package db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import modules.PublicModules;
import utils.AppModules;

/**
 * Created by Parsa on 9/20/2017.
 */

public class DBManager {

    public static DataBaseOpenHelper dataBaseOpenHelper;
    public static SQLiteDatabase database;

    public DBManager(Context context) {
        dataBaseOpenHelper = new DataBaseOpenHelper(context);
    }

    /**
     * open the dataBase connection for write
     */
    public static void openForWrite() {
        database = dataBaseOpenHelper.getWritableDatabase();
    }

    /**
     * open the dataBase connection for read
     */
    public static void openForRead() {
        database = dataBaseOpenHelper.getReadableDatabase();
    }

    /**
     * close the dataBase Connection
     */
    public static void close() {
        if (database != null) {
            database.close();
        }
    }

    public static void deletePastYearsData() {
        openForWrite();
        String query = "DELETE FROM T_Transaction WHERE trnDate < " + PublicModules.getYearInterval(0)[0];
        database.execSQL(query);
        close();
    }

}
