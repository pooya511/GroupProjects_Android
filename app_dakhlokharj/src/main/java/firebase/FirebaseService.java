package firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.net.ssl.SSLContext;

import model.EnumsAndConstants;
import model.Settings;
import modules.PublicModules;

import static TxAnalytics.TxAnalytics.UpdateClientPushID;

public class FirebaseService extends FirebaseInstanceIdService {

    private static final String REG_TOKEN = "REG_TOKEN";

    @Override
    public void onTokenRefresh() {
        String recentToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN, recentToken);
        if (recentToken != null && !recentToken.equals("")) {
            try {
                String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER);
                UpdateClientPushID(recentToken, EnumsAndConstants.APP_ID , EnumsAndConstants.APP_SOURCE , EnumsAndConstants.APP_VERSION , EnumsAndConstants.DEVICE_ID , EnumsAndConstants.OS , userNumber ,EnumsAndConstants.SERVER_URL ,new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {

                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        if (response.isSuccessful()) {
                            String responseStr = response.body().string();
                            try {
                                JSONObject responseJson = new JSONObject(PublicModules.makeJsonString(responseStr));
                                if (responseJson.getString("status").equals(EnumsAndConstants.SUCCESS)) {
                                    Log.d(REG_TOKEN, responseJson.getString("status"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
