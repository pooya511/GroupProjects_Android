package firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.dakhlokharj.BaseActivity;
import com.taxiapps.dakhlokharj.MainActivity;
import com.taxiapps.dakhlokharj.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import TxAnalytics.TxAnalytics;
import model.EnumsAndConstants;
import model.Settings;
import modules.PublicModules;

import static TxAnalytics.TxAnalytics.MessageAdd;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private static String i;
    private String customField;
    private JSONObject custom;
    private JSONObject urlDetails;
    private String title = "", message = "", url = "";

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        try {
            customField = remoteMessage.getData().get("custom");
            custom = new JSONObject(PublicModules.makeJsonString(customField));
            i = custom.getString("i");
            if (custom.has("a")) {
                urlDetails = custom.getJSONObject("a");
                title = urlDetails.getString("tx_pop_title");
                message = urlDetails.getString("tx_pop_msg");
                url = urlDetails.getString("tx_pop_url");
            }
            String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER);
            MessageAdd(i, TxAnalytics.MessageTypes.PUSH, EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, userNumber, EnumsAndConstants.SERVER_URL, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        Log.i("Response", response.body().string());
                        Log.i("MessageAdd", "message sent");
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try { // methode appIsForeground bayad hamrah ba try catch bashe
            if (BaseActivity.getInstance() != null) { // agar ke base activity null nabud.albate hichvaght pish nemiad vali mahze ehtiat gozashtam
                if (!BaseActivity.getInstance().isFinishing()) { // agar ke tu safhe i az baseActivity budim ke finish nashode bud
                    if (BaseActivity.appIsForeground(getApplicationContext()) || !BaseActivity.getInstance().isFinishing()) // check mikonim ke app forground bashe
                        if (message.equals("") || title.equals("")) { // agar ke server custom message nafrestade bud
                            BaseActivity.showPushPopup(remoteMessage.getData().get("title"), remoteMessage.getData().get("alert"), ""); // bia ye popup mamuli neshun bede
                        } else {
                            BaseActivity.showPushPopup(title, message, url); // agar  ke server custom message ferestade bud bia ba url bazesh kon
                        }
                    else { // age app background bud bia notification besaz
                        makeNotification(remoteMessage);
                    }
                } else {
                    if (MainActivity.getInstance() != null) { // code payin baray MainActivitye ke az base activity extend nemikone{
                        if (MainActivity.appIsForeground(getApplicationContext()) || !MainActivity.getInstance().isFinishing()) {
                            Log.i("PUSH", "main not finish");
                            if (message.equals("") || title.equals("")) { // pop up ba url neshun bedam ya na
                                MainActivity.showPushPopup(remoteMessage.getData().get("title"), remoteMessage.getData().get("alert"), "");// neshun nade
                            } else {
                                MainActivity.showPushPopup(title, message, url); // neshun bede
                            }
                        } else {
                            if (MainActivity.getInstance().isFinishing()) {
                                makeNotification(remoteMessage);
                            }
                        }
                    }
                }
            } else { // when app is absolutely terminate
                makeNotification(remoteMessage);
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void makeNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, MainActivity.class);
        NotificationCompat.Builder notiBuilder = new NotificationCompat.Builder(this);
        //
        Log.i("i", i);
        intent.putExtra("i", i);
        intent.putExtra("popUpTitle", remoteMessage.getData().get("title"));
        intent.putExtra("popUpContent", remoteMessage.getData().get("alert"));
        intent.putExtra("popUpURL", url);
        notiBuilder.setContentTitle(remoteMessage.getData().get("title"));
        notiBuilder.setContentText(remoteMessage.getData().get("alert"));
        //
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        notiBuilder.setAutoCancel(true);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notiBuilder.setSound(uri);
        notiBuilder.setSmallIcon(R.drawable.ic_dakhlokharj);
        notiBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notiBuilder.build());
    }

}
