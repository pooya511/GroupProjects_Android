package model;

import android.annotation.SuppressLint;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import modules.PublicModules;

/**
 * Created by Parsa on 10/3/2017.
 */

public class Currency {

    private static HashMap<String, CurrencyDesc> supportedCurrency = new HashMap<>();

    static {
        supportedCurrency.put("0", new CurrencyDesc("ريال", "ريال", "ر", true));
        supportedCurrency.put("1", new CurrencyDesc("تومان", "تومان", "ت", true));
        supportedCurrency.put("2", new CurrencyDesc("AED", "AED", "AED", false));
        supportedCurrency.put("3", new CurrencyDesc("USD", "$", "$", false));
        supportedCurrency.put("4", new CurrencyDesc("EUR", "€", "€", false));
        supportedCurrency.put("5", new CurrencyDesc("TRY", "₺", "₺", false));
        supportedCurrency.put("6", new CurrencyDesc("CNY", "¥", "¥", false));
    }

    public enum CurrencyMode {
        Full("full"),
        Short("short"),
        Symbol("symbol");

        private String type;

        CurrencyMode(String type) {
            this.type = type;
        }

        public String value() {
            return this.type;
        }
    }

    public static class CurrencyString {

        public String raw = "";
        public String separated = "";

        public static CurrencyString getCurrencyString(String amount, CurrencyMode currencyMode) {
            amount = PublicModules.toEnglishDigit(amount);
            amount = amount.replace(",", "");
            return getCurrencyString(Double.parseDouble(amount), currencyMode);
        }

        @SuppressLint("DefaultLocale")
        public static CurrencyString getCurrencyString(double amount, CurrencyMode currencyMode) {

            CurrencyString resCurrencyString = new CurrencyString();
            String currentCurrency = Settings.getSetting(Settings.CASH_TYPE_INDEX);

            resCurrencyString.raw = String.valueOf(amount);

            for (Map.Entry<String, CurrencyDesc> entry : supportedCurrency.entrySet()) {
                if (entry.getKey().equals(currentCurrency)) {
                    if (entry.getValue().isRialToman) {
                        resCurrencyString.raw = String.format(Locale.US, "%.0f", amount);
                        resCurrencyString.separated = String.format(Locale.US, "%,.0f", amount);

                        resCurrencyString.raw = PublicModules.toPersianDigit(resCurrencyString.raw);
                        resCurrencyString.separated = PublicModules.toPersianDigit(resCurrencyString.separated);
                    } else {
                        resCurrencyString.raw = String.format(Locale.US, "%.2f", amount);
                        resCurrencyString.separated = String.format(Locale.US, "%,.2f", amount);

                        resCurrencyString.raw = PublicModules.toEnglishDigit(resCurrencyString.raw);
                        resCurrencyString.separated = PublicModules.toEnglishDigit(resCurrencyString.separated);
                    }

                    if (currencyMode != null) {
                        resCurrencyString.raw += " " + getCurrency(currencyMode);
                        resCurrencyString.separated += " " + getCurrency(currencyMode);
                    }
                }
            }

            return resCurrencyString;
        }

        public static String getCurrency(CurrencyMode currencyMode) {
            String currentCurrency = Settings.getSetting(Settings.CASH_TYPE_INDEX);

            for (Map.Entry<String, CurrencyDesc> entry : supportedCurrency.entrySet()) {
                if (entry.getKey().equals(currentCurrency)) {
                    switch (currencyMode.value()) {
                        case "short":
                            return supportedCurrency.get(currentCurrency).shortt;
                        case "full":
                            return supportedCurrency.get(currentCurrency).full;
                        case "symbol":
                            return supportedCurrency.get(currentCurrency).symbol;
                    }
                }
            }
            return "";
        }

    }

    private static class CurrencyDesc {

        public String full;
        public String shortt;
        public String symbol;
        public boolean isRialToman;

        public CurrencyDesc(String full, String shortt, String symbol, boolean isRialToman) {
            this.full = full;
            this.shortt = shortt;
            this.symbol = symbol;
            this.isRialToman = isRialToman;
        }

    }

}