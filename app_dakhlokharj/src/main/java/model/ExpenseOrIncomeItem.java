package model;

import android.os.Parcel;
import android.os.Parcelable;

import modules.PublicModules;

import java.util.Locale;

/**
 * Created by Parsa on 2017-11-21.
 */

public class ExpenseOrIncomeItem implements Parcelable {

    private int ID;
    private double amount;
    private String title;
    private String currency;
    private String icon;
    private String iconColor;
    private String date;
    private String description;
    private int trnType;
    private String trnColor;
    public String trnImagePath;

    public ExpenseOrIncomeItem(int ID, String title, double amount, String icon, String iconColor, String date, String description, String trnColor, String trnImagePath) {
        this.ID = ID;
        this.title = title;
        this.amount = amount;
        this.icon = icon;
        this.iconColor = iconColor;
        this.date = date;
        this.description = description;
        this.trnImagePath = trnImagePath;
        this.trnColor = trnColor;
    }

    protected ExpenseOrIncomeItem(Parcel in) {
        ID = in.readInt();
        amount = in.readDouble();
        title = in.readString();
        currency = in.readString();
        icon = in.readString();
        iconColor = in.readString();
        date = in.readString();
        description = in.readString();
        trnType = in.readInt();
        trnColor = in.readString();
        trnImagePath = in.readString();
    }

    public static final Creator<ExpenseOrIncomeItem> CREATOR = new Creator<ExpenseOrIncomeItem>() {
        @Override
        public ExpenseOrIncomeItem createFromParcel(Parcel in) {
            return new ExpenseOrIncomeItem(in);
        }

        @Override
        public ExpenseOrIncomeItem[] newArray(int size) {
            return new ExpenseOrIncomeItem[size];
        }
    };

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getAmount() {
        String res = String.valueOf(amount);
        res = Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("0") || Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("1") ?
                String.format(Locale.US,"%.0f", Double.valueOf(res)) :
                String.format(Locale.US,"%.2f", Double.valueOf(res));
        return Double.valueOf(PublicModules.toEnglishDigit(res));
    }

    public void setAmount(double amount) {
        this.amount = Double.valueOf(String.format(Locale.US,"%.2f", Double.valueOf(PublicModules.toEnglishDigit(String.valueOf(amount)))));
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTrnType() {
        return trnType;
    }

    public void setTrnType(int trnType) {
        this.trnType = trnType;
    }

    public String getTrnImagePath() {
        return trnImagePath;
    }

    public void setTrnImagePath(String trnImagePath) {
        this.trnImagePath = trnImagePath;
    }

    public String getTrnColor() {
        return trnColor;
    }

    public void setTrnColor(String trnColor) {
        this.trnColor = trnColor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeDouble(amount);
        dest.writeString(title);
        dest.writeString(currency);
        dest.writeString(icon);
        dest.writeString(iconColor);
        dest.writeString(date);
        dest.writeString(description);
        dest.writeInt(trnType);
        dest.writeString(trnColor);
        dest.writeString(trnImagePath);
    }
}
