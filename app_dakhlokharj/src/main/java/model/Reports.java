package model;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import db.DBManager;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Reports {

    public static class SmartReport {

        public static String mostUsedDay() {
            if (Transaction.getCountAll().equals("0"))
                return "";
            else {
                String res ="0";
                ArrayList<HashMap<String, Object>> hashArray = new ArrayList<>();
                HashMap<String, Integer> weekDays = new HashMap<>();
                weekDays.put("0", 0);
                weekDays.put("1", 0);
                weekDays.put("2", 0);
                weekDays.put("3", 0);
                weekDays.put("4", 0);
                weekDays.put("5", 0);
                weekDays.put("6", 0);


                DBManager.openForRead();
                Cursor resCursor = DBManager.database.rawQuery("SELECT trnDate FROM T_Transaction WHERE trnType='1'", null);
                resCursor.moveToFirst();
                while (!resCursor.isAfterLast()) {

                    HashMap<String, Object> temp = new HashMap<>();
                    temp.put("trnDate", resCursor.getLong(0));

                    hashArray.add(temp);
                    resCursor.moveToNext();

                }
                resCursor.close();
                DBManager.close();


                for (int i = 0; i < hashArray.size(); i++) {
                    PersianDate date = new PersianDate(Long.parseLong(String.valueOf(hashArray.get(i).get("trnDate"))));
                    if (weekDays.containsKey(PersianDateFormat.format(date,"w")))
                        weekDays.put(String.valueOf(PersianDateFormat.format(date,"w")), weekDays.get(String.valueOf(PersianDateFormat.format(date,"w"))) + 1);

                }

                for (int i = 0; i < weekDays.size(); i++) {
                    if (weekDays.get(String.valueOf(i)) != 0) {
                        res = String.valueOf(weekDays.get(String.valueOf(i)));
                        break;
                    }
                }

                for (int i = 1; i < weekDays.size(); i++) {
                    if (weekDays.get(String.valueOf(i)) > Integer.parseInt(res))
                        res = String.valueOf(weekDays.get(String.valueOf(i)));
                }

                String finalRes = "";

                for (Map.Entry entry : weekDays.entrySet()) {
                    if (String.valueOf(entry.getValue()).equals(res)) {
                        finalRes = String.valueOf(entry.getKey());
                        break;
                    }
                }

                return AppModules.dayWeekToString(finalRes);
            }
        }

        public static String mostExpensiveSubCategory() {

            String query = "SELECT IFNULL((SELECT B.lsdName FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID WHERE trnType='1' GROUP BY lsdID,B.lsdName ORDER BY SUM(trnAmount) DESC  LIMIT 1),'-')";
            String res = "";

            DBManager.openForRead();
            Cursor resCursor = DBManager.database.rawQuery(query, null);
            resCursor.moveToFirst();
            res = resCursor.getString(0);

            resCursor.close();
            DBManager.close();

            return res;
        }

        public static String fridaysExpenseAverage() {

            if (Transaction.getCountAll().equals("0"))
                return "-";

            else {

                ArrayList<HashMap<String, Object>> hashArray = new ArrayList<>();
                String query = "SELECT trnDate,trnAmount FROM T_Transaction WHERE trnType='1'";

                DBManager.openForRead();
                Cursor resCursor = DBManager.database.rawQuery(query, null);
                resCursor.moveToFirst();
                while (!resCursor.isAfterLast()) {

                    HashMap<String, Object> hashM = new HashMap<>();

                    hashM.put("trnDate", resCursor.getLong(0));
                    hashM.put("trnAmount", resCursor.getDouble(1));

                    hashArray.add(hashM);
                    resCursor.moveToNext();
                }

                resCursor.close();
                DBManager.close();


                double count = 0;
                double total = 0;

                for (int i = 0; i < hashArray.size(); i++) {
                    PersianDate date = new PersianDate(Long.parseLong(String.valueOf(hashArray.get(i).get("trnDate"))));
                    if (PersianDateFormat.format(date,"w").equals("6")) {
                        total = total + Double.parseDouble(String.valueOf(hashArray.get(i).get("trnAmount")));
                        count++;
                    }
                }

                if (total == 0 && count == 0)
                    return "-";
                else

                    return Currency.CurrencyString.getCurrencyString((total / count), Currency.CurrencyMode.Short).separated;
            }

        }

        public static ArrayList<ArrayList<String>> unusedCategory() {

            ArrayList<ArrayList<String>> res = new ArrayList<>();

            String query = "SELECT lsdName, lsdIcon, lsdColor FROM T_List_Detail WHERE ID NOT IN (SELECT lsdID FROM T_Transaction)";

            DBManager.openForRead();

            Cursor ret = DBManager.database.rawQuery(query, null);
            ret.moveToFirst();
            while (!ret.isAfterLast()) {

                ArrayList<String> item = new ArrayList<>();
                item.add(ret.getString(ret.getColumnIndex("lsdName")));
                item.add(ret.getString(ret.getColumnIndex("lsdIcon")));
                item.add(ret.getString(ret.getColumnIndex("lsdColor")));

                res.add(item);

                ret.moveToNext();
            }

            DBManager.close();

            return res;
        }

    }

    public static class YearlyReport {

        public static String totalExpensesThisYear() {

            double res;
            long[] thisYear = PublicModules.getYearInterval(1);

            String query = "SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + thisYear[0] + " AND trnDate < " + thisYear[1];

            DBManager.openForRead();
            Cursor resCursor = DBManager.database.rawQuery(query, null);

            resCursor.moveToFirst();
            res = resCursor.getDouble(0);

            resCursor.close();
            DBManager.close();


            return Currency.CurrencyString.getCurrencyString(res, Currency.CurrencyMode.Short).separated;
        }

        public static String totalIncomeThisYear() {

            double res;
            long[] thisYear = PublicModules.getYearInterval(1);

            String query = "SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + thisYear[0] + " AND trnDate < " + thisYear[1];

            DBManager.openForRead();
            Cursor resCursor = DBManager.database.rawQuery(query, null);

            resCursor.moveToFirst();
            res = resCursor.getDouble(0);

            resCursor.close();
            DBManager.close();

            return Currency.CurrencyString.getCurrencyString(res, Currency.CurrencyMode.Short).separated;

        }

        public static String daysWithoutExpense() {

            if (Transaction.getCountAll().equals("0"))
                return "-";
            else {

                long[] thisYear = PublicModules.getYearInterval(1);

                String query = "SELECT trnDate FROM T_Transaction WHERE trnType='1' AND trnDate>= " + thisYear[0] + " AND trnDate < " + thisYear[1];
                ArrayList<HashMap<String, Long>> hashArray = new ArrayList<>();

                DBManager.openForRead();
                Cursor resCursor = DBManager.database.rawQuery(query, null);

                resCursor.moveToFirst();
                while (!resCursor.isAfterLast()) {

                    HashMap<String, Long> hashMap = new HashMap<>();

                    hashMap.put("trnDate", resCursor.getLong(0));

                    hashArray.add(hashMap);
                    resCursor.moveToNext();
                }

                resCursor.close();
                DBManager.close();

                HashMap<Long, Long> dict = new HashMap<>();
                for (int i = 0; i < hashArray.size(); i++) {

                    PersianDate pCalendar = new PersianDate(hashArray.get(i).get("trnDate"));
                    long dayOfYear = AppModules.calendarOrdinality(pCalendar);
                    if (dict.get(dayOfYear) != null)
                        dict.put(dayOfYear, dayOfYear + 1);
                    else
                        dict.put(dayOfYear, Long.valueOf(1));
                }

                long totalPassedDays = PublicModules.dateDiff(PublicModules.getYearInterval(1)[0], new Date().getTime(), "day");
                return String.valueOf(totalPassedDays - dict.size());
            }

        }

        public static String averageExpending() {

            if (Transaction.getCountAll().equals("0"))
                return "-";
            else {

                long[] thisYear = PublicModules.getYearInterval(1);

                String query1 = "SELECT trnDate FROM T_Transaction WHERE trnType='1' AND trnDate>= " + thisYear[0] + " AND trnDate < " + thisYear[1];
                ArrayList<HashMap<String, Long>> hashArray = new ArrayList<>();

                DBManager.openForRead();
                Cursor res1 = DBManager.database.rawQuery(query1, null);

                res1.moveToFirst();
                while (!res1.isAfterLast()) {

                    HashMap<String, Long> hashMap = new HashMap<>();

                    hashMap.put("trnDate", res1.getLong(0));

                    hashArray.add(hashMap);
                    res1.moveToNext();
                }

                res1.close();
                DBManager.close();

                HashMap<Long, Long> dict = new HashMap<>();
                for (int i = 0; i < hashArray.size(); i++) {

                    PersianDate pCalendar = new PersianDate(hashArray.get(i).get("trnDate"));
                    long dayOfYear = AppModules.calendarOrdinality(pCalendar);
                    if (dict.get(dayOfYear) != null)
                        dict.put(dayOfYear, dayOfYear + 1);
                    else
                        dict.put(dayOfYear, Long.valueOf(1));
                }


                String query2 = "SELECT trnDate,trnAmount FROM T_Transaction WHERE trnType='1' AND trnDate>= " + thisYear[0] + " AND trnDate < " + thisYear[1];
                double total = 0;

                ArrayList<HashMap<String, Object>> hashArray2 = new ArrayList<>();
                DBManager.openForRead();
                Cursor res2 = DBManager.database.rawQuery(query2, null);
                res2.moveToFirst();
                while (!res2.isAfterLast()) {

                    HashMap<String, Object> hashMap = new HashMap<>();

                    hashMap.put("trnDate", res2.getLong(0));
                    hashMap.put("trnAmount", res2.getDouble(1));

                    hashArray2.add(hashMap);
                    res2.moveToNext();
                }

                res2.close();
                DBManager.close();

                HashMap<String, Integer> weekDays = new HashMap<>();
                weekDays.put("1", 0);
                weekDays.put("2", 0);
                weekDays.put("3", 0);
                weekDays.put("4", 0);
                weekDays.put("5", 0);
                weekDays.put("6", 0);
                weekDays.put("7", 0);

                for (int i = 0; i < hashArray2.size(); i++) {
                    total = total + Double.valueOf(String.valueOf(hashArray2.get(i).get("trnAmount")));
                }

                double totalPassedDays = (double) PublicModules.dateDiff(PublicModules.getYearInterval(1)[0], new Date().getTime(), "day");

                return Currency.CurrencyString.getCurrencyString(((int) total / (int) totalPassedDays), Currency.CurrencyMode.Short).separated;
            }
        }

        public static ArrayList<ArrayList> ChartReport() {

            ArrayList<ArrayList> res = new ArrayList<>();

            ArrayList<Double> expenses = new ArrayList<>();
            ArrayList<Double> incomes = new ArrayList<>();
            ArrayList<Double> budgets = new ArrayList<>();
            ArrayList<String> months = new ArrayList<>();

            DBManager.openForRead();

            for (int i = 1; i <= 11; i++) {

                long[] currentMonth = PublicModules.getMonthInterval(-1 * i);

                Cursor result1 = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + currentMonth[0] + " AND trnDate < " + currentMonth[1], null);
                Cursor result2 = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + currentMonth[0] + " AND trnDate < " + currentMonth[1], null);
                Cursor result3 = DBManager.database.rawQuery("SELECT IFNULL(SUM(budAmount),0) FROM T_Budget WHERE lsdID=0", null);

                result1.moveToFirst();
                double res1 = result1.getDouble(0);

                result2.moveToFirst();
                double res2 = result2.getDouble(0);

                result3.moveToFirst();
                double res3 = result3.getDouble(0);

                months.add(PublicModules.timeStamp2String(currentMonth[0], "yy MM"));
                expenses.add(res1);
                incomes.add(res2);
                budgets.add(res3);

                result1.close();
                result2.close();
                result3.close();

            }

            DBManager.close();

            res.add(expenses);
            res.add(incomes);
            res.add(budgets);
            res.add(months);

            return res;
        }

    }

    public static class Monthly {

        public static ArrayList<MonthReport> recent12Months() {

            ArrayList<MonthReport> resArray = new ArrayList<>();

            long minDate = Transaction.getFirstLastTransaction()[0] != 0 ? new PersianDate(Transaction.getFirstLastTransaction()[0]).getTime() : new PersianDate().getTime();
            long maxDate = Transaction.getFirstLastTransaction()[1] != 0 ? new PersianDate(Transaction.getFirstLastTransaction()[1]).getTime() : new PersianDate().getTime();

            long monthCountMin = PublicModules.dateDiff(new PersianDate(minDate).getTime(), new PersianDate().getTime(), "month");
            long monthCountMax = PublicModules.dateDiff(new PersianDate(maxDate).getTime(), new PersianDate().getTime(), "month");

            monthCountMin = monthCountMin > 0 ? 0 : monthCountMin;
            monthCountMax = monthCountMax < 0 ? 0 : monthCountMax;

            DBManager.openForRead();

            for (int i = (int) monthCountMin; i <= (int) monthCountMax; i++) {

                MonthReport monthReport = new MonthReport();

                long[] currentMonth = PublicModules.getMonthInterval(i);

                Cursor res1 = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + currentMonth[0] + " AND trnDate < " + currentMonth[1], null);
                Cursor res2 = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + currentMonth[0] + " AND trnDate < " + currentMonth[1], null);

                res1.moveToFirst();
                double trnAmount1 = res1.getDouble(0);
                res1.close();

                res2.moveToFirst();
                double trnAmount2 = res2.getDouble(0);
                res2.close();

                monthReport.month = PublicModules.timeStamp2String(PublicModules.getMonthInterval(i)[0], "MMMM yy");

                monthReport.expense = Currency.CurrencyString.getCurrencyString(trnAmount1, Currency.CurrencyMode.Short).separated;
                monthReport.income = Currency.CurrencyString.getCurrencyString(trnAmount2, Currency.CurrencyMode.Short).separated;

                resArray.add(monthReport);
            }

            DBManager.close();

            return resArray;
        }

    }

    public static class Summary {

        public static ArrayList<String[]> summaryReport() {

            ArrayList<String[]> resArray = new ArrayList<>();

            long[] today = PublicModules.getDayInterval();
            long[] thisWeek = PublicModules.getWeekInterval();
            long[] past10Days = PublicModules.getCustomInterval(-10, 0);
            long[] pastMonth = PublicModules.getMonthInterval(0);
            long[] pastYear = PublicModules.getYearInterval(1);

            DBManager.openForRead();

            Cursor todayEXCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + today[0] + " AND trnDate < " + today[1], null);
            Cursor todayINCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + today[0] + " AND trnDate < " + today[1], null);
            String todayString = PublicModules.timeStamp2String(today[0], "dd MMMM yyyy");

            todayEXCursor.moveToFirst();
            double todayEX = todayEXCursor.getDouble(0);
            todayEXCursor.close();

            todayINCursor.moveToFirst();
            double todayIN = todayINCursor.getDouble(0);
            todayINCursor.close();

            resArray.add(new String[]{Currency.CurrencyString.getCurrencyString(todayEX, Currency.CurrencyMode.Short).separated,
                    Currency.CurrencyString.getCurrencyString(todayIN, Currency.CurrencyMode.Short).separated,
                    todayString
            });

            Cursor thisWeekEXCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + thisWeek[0] + " AND trnDate < " + thisWeek[1], null);
            Cursor thisWeekINCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + thisWeek[0] + " AND trnDate < " + thisWeek[1], null);

            thisWeekEXCursor.moveToFirst();
            double thisWeekEX = thisWeekEXCursor.getDouble(0);
            thisWeekEXCursor.close();

            thisWeekINCursor.moveToFirst();
            double thisWeekIN = thisWeekINCursor.getDouble(0);
            thisWeekINCursor.close();

            resArray.add(new String[]{Currency.CurrencyString.getCurrencyString(thisWeekEX, Currency.CurrencyMode.Short).separated,
                    Currency.CurrencyString.getCurrencyString(thisWeekIN, Currency.CurrencyMode.Short).separated
            });

            Cursor past10DaysEXCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + past10Days[0] + " AND trnDate < " + past10Days[1], null);
            Cursor past10DaysINCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + past10Days[0] + " AND trnDate < " + past10Days[1], null);

            past10DaysEXCursor.moveToFirst();
            double past10DaysEX = past10DaysEXCursor.getDouble(0);
            past10DaysEXCursor.close();

            past10DaysINCursor.moveToFirst();
            double past10DaysIN = past10DaysINCursor.getDouble(0);
            past10DaysINCursor.close();

            resArray.add(new String[]{Currency.CurrencyString.getCurrencyString(past10DaysEX, Currency.CurrencyMode.Short).separated,
                    Currency.CurrencyString.getCurrencyString(past10DaysIN, Currency.CurrencyMode.Short).separated
            });

            Cursor pastMonthEXCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + pastMonth[0] + " AND trnDate < " + pastMonth[1], null);
            Cursor pastMonthINCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + pastMonth[0] + " AND trnDate < " + pastMonth[1], null);
            String pastMonthString = PublicModules.timeStamp2String(pastMonth[0], "MMMM");

            pastMonthEXCursor.moveToFirst();
            double pastMonthEX = pastMonthEXCursor.getDouble(0);
            pastMonthEXCursor.close();

            pastMonthINCursor.moveToFirst();
            double pastMonthIN = pastMonthINCursor.getDouble(0);
            pastMonthINCursor.close();

            resArray.add(new String[]{Currency.CurrencyString.getCurrencyString(pastMonthEX, Currency.CurrencyMode.Short).separated,
                    Currency.CurrencyString.getCurrencyString(pastMonthIN, Currency.CurrencyMode.Short).separated,
                    pastMonthString
            });

            Cursor pastYearEXCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + pastYear[0] + " AND trnDate < " + pastYear[1], null);
            Cursor pastYearINCursor = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + pastYear[0] + " AND trnDate < " + pastYear[1], null);
            String pastYearText = PublicModules.timeStamp2String(pastYear[0], "YYYY");


            pastYearEXCursor.moveToFirst();
            double pastYearEX = pastYearEXCursor.getDouble(0);
            pastYearEXCursor.close();

            pastYearINCursor.moveToFirst();
            double pastYearIN = pastYearINCursor.getDouble(0);
            pastYearINCursor.close();

            resArray.add(new String[]{Currency.CurrencyString.getCurrencyString(pastYearEX, Currency.CurrencyMode.Short).separated,
                    Currency.CurrencyString.getCurrencyString(pastYearIN, Currency.CurrencyMode.Short).separated,
                    pastYearText
            });

            DBManager.close();

            return resArray;

        }

    }

    public static class ReportCategory {

        public static ArrayList<HashMap<String, Object>> byCategories() {

            ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();


            String query = "SELECT  C.*," +
                    "CAST(CAST(IFNULL(SUM(CASE WHEN B.trnType=1 THEN B.trnAmount ELSE 0 END),0) AS REAL) AS STRING) AS 'EX'" +
                    "FROM T_List_Detail A " +
                    "LEFT JOIN T_Transaction B ON A.ID=B.LSDID " +
                    "LEFT JOIN T_List C ON A.lstID = C.ID " +
                    "WHERE C.lstType = 1 " +
                    "GROUP BY C.lstName " +
                    "ORDER BY C.lstOrder";


            DBManager.openForRead();

            Cursor resCursor = DBManager.database.rawQuery(query, null);
            resCursor.moveToFirst();

            while (!resCursor.isAfterLast()) {

                HashMap<String, Object> dataHashMap = new HashMap<>();

                dataHashMap.put("ID", resCursor.getInt(0));
                dataHashMap.put("lstName", resCursor.getString(1));
                dataHashMap.put("lstIcon", resCursor.getString(2));
                dataHashMap.put("lstColor", resCursor.getString(3));
                dataHashMap.put("lstOrder", resCursor.getInt(4));
                dataHashMap.put("lstType", resCursor.getString(5));

                dataHashMap.put("EX", resCursor.getString(6));

                resArray.add(dataHashMap);
                resCursor.moveToNext();
            }

            resCursor.close();
            DBManager.close();

            return resArray;
        }

    }

    public static class ReportPayee {

        public static ArrayList<ArrayList<String>> byPayee(int payeeID, long dateFrom, long dateTo, int transType) {

            ArrayList<ArrayList<String>> resArray = new ArrayList<>();

            String fromTimeStamp = "";
            String toTimeStamp = "";
            String query = "";

            if (dateFrom != 0)
                fromTimeStamp = PublicModules.string2TimeStamp(PublicModules.timeStamp2String(dateFrom, "yyyy/MM/dd HH:mm"));

            if (dateTo != 0)
                toTimeStamp = PublicModules.string2TimeStamp(PublicModules.timeStamp2String(dateTo, "yyyy/MM/dd HH:mm"));

            query = String.valueOf("SELECT A.*,B.lstID,B.lsdName,B.lsdIcon,B.lsdColor,C.lstName,C.lstIcon,C.lstColor,C.lstOrder " +
                    "FROM T_Transaction A " +
                    "INNER JOIN T_List_Detail B ON A.lsdID = B.ID " +
                    "INNER JOIN T_List C ON B.lstID=C.ID " +
                    "WHERE 1=1 " +
                    "AND pyeID = " + payeeID);
//                    " ORDER BY lstOrder");

            if (!fromTimeStamp.equals(""))
                query = query.concat(" AND trnDate>= " + fromTimeStamp);

            if (!toTimeStamp.equals(""))
                query = query.concat(" AND trnDate< " + toTimeStamp);

            if (transType != 0)
                query = query.concat(" AND trnType= " + transType);

            query = query.concat(" ORDER BY lstOrder");


            DBManager.openForRead();
            Cursor resCursor = DBManager.database.rawQuery(query, null);

            ArrayList<HashMap<String, Object>> hashArray = new ArrayList<>();

            if (resCursor.getCount() > 0) {

                resCursor.moveToFirst();

                while (!resCursor.isAfterLast()) {

                    HashMap<String, Object> dataHashMap = new HashMap();
                    dataHashMap.put(Transaction.TransactionCol.id.value(), resCursor.getInt(0));
                    dataHashMap.put(Transaction.TransactionCol.lsdID.value(), resCursor.getInt(1));
                    dataHashMap.put(Transaction.TransactionCol.trnAmount.value(), resCursor.getDouble(2));
                    dataHashMap.put(Transaction.TransactionCol.trnDate.value(), resCursor.getString(3));
                    dataHashMap.put(Transaction.TransactionCol.pyeID.value(), resCursor.getInt(4));
                    dataHashMap.put(Transaction.TransactionCol.acnID.value(), resCursor.getInt(5));
                    dataHashMap.put(Transaction.TransactionCol.trnDescription.value(), resCursor.getString(6));
                    dataHashMap.put(Transaction.TransactionCol.trnImagePath.value(), resCursor.getString(7));
                    dataHashMap.put(Transaction.TransactionCol.trnColor.value(), resCursor.getString(8));
                    dataHashMap.put(Transaction.TransactionCol.trnType.value(), resCursor.getInt(9));
                    dataHashMap.put(Transaction.TransactionCol.trnReference.value(), resCursor.getInt(11));

                    dataHashMap.put(GroupDetail.GroupDetailCol.lstID.value(), resCursor.getInt(12));
                    dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(13));
                    dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(14));
                    dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(15));

                    dataHashMap.put(Group.GroupCol.lstName.value(), resCursor.getString(16));
                    dataHashMap.put(Group.GroupCol.lstIcon.value(), resCursor.getString(17));
                    dataHashMap.put(Group.GroupCol.lstColor.value(), resCursor.getString(18));
                    dataHashMap.put(Group.GroupCol.lstOrder.value(), resCursor.getInt(19));

                    hashArray.add(dataHashMap);

                    resCursor.moveToNext();
                }

                resCursor.close();
                DBManager.close();

            }

            for (int i = 0; i < hashArray.size(); i++) {

                ArrayList<String> itemArray = new ArrayList<>();

                String name = String.valueOf(hashArray.get(i).get("lsdName"));
                String amount = Currency.CurrencyString.getCurrencyString(String.valueOf(hashArray.get(i).get("trnAmount")), Currency.CurrencyMode.Short).separated;
                long dateTimeInterval = Long.parseLong(String.valueOf(hashArray.get(i).get("trnDate")));
                String date = PublicModules.timeStamp2String(dateTimeInterval, "yyyy/MM/dd HH:mm");
                String iconCode = String.valueOf(hashArray.get(i).get("lsdIcon"));
                String iconColor = String.valueOf(hashArray.get(i).get("lsdColor"));
                String color = String.valueOf(hashArray.get(i).get("trnColor"));
                String transactionType = String.valueOf(hashArray.get(i).get("trnType"));

                itemArray.add(name);
                itemArray.add(amount);
                itemArray.add(date);
                itemArray.add(iconCode);
                itemArray.add(iconColor);
                itemArray.add(color);
                itemArray.add(transactionType);

                resArray.add(itemArray);
            }


            return resArray;
        }

    }

    public static class DateRangeReport {

        public static ArrayList<ArrayList<String>> withDates(long dateFrom, long dateTo) {

            ArrayList<ArrayList<String>> resArray = new ArrayList<>();

            String fromTimeStamp = "";
            String toTimeStamp = "";
            String query = "";

            if (dateFrom != 0)
                fromTimeStamp = PublicModules.string2TimeStamp(PublicModules.timeStamp2String(dateFrom, "yyyy/MM/dd HH:mm"));

            if (dateTo != 0)
                toTimeStamp = PublicModules.string2TimeStamp(PublicModules.timeStamp2String(dateTo, "yyyy/MM/dd HH:mm"));

            if (!fromTimeStamp.equals("") && !toTimeStamp.equals("")) {

                query = "SELECT A.*,B.lstID,B.lsdName,B.lsdIcon,B.lsdColor,C.lstName,C.lstIcon,C.lstColor,C.lstOrder " +
                        "FROM T_Transaction A " +
                        "INNER JOIN T_List_Detail B ON A.lsdID = B.ID " +
                        "INNER JOIN T_List C ON B.lstID=C.ID " +
                        "WHERE 1=1 " +
                        " AND trnDate>= " + fromTimeStamp +
                        " AND trnDate< " + toTimeStamp +
                        " ORDER BY lstOrder";

            } else {
                query = "SELECT A.*,B.lstID,B.lsdName,B.lsdIcon,B.lsdColor,C.lstName,C.lstIcon,C.lstColor,C.lstOrder " +
                        "FROM T_Transaction A " +
                        "INNER JOIN T_List_Detail B ON A.lsdID = B.ID " +
                        "INNER JOIN T_List C ON B.lstID=C.ID " +
                        "ORDER BY lstOrder";
            }

            DBManager.openForRead();

            Cursor resCursor = DBManager.database.rawQuery(query, null);

            ArrayList<HashMap<String, Object>> hashMapArray = new ArrayList<>();

            if (resCursor.getCount() > 0) {

                resCursor.moveToFirst();

                while (!resCursor.isAfterLast()) {

                    HashMap<String, Object> dataHashMap = new HashMap();

                    dataHashMap.put(Transaction.TransactionCol.id.value(), resCursor.getInt(0));
                    dataHashMap.put(Transaction.TransactionCol.lsdID.value(), resCursor.getInt(1));
                    dataHashMap.put(Transaction.TransactionCol.trnAmount.value(), resCursor.getDouble(2));
                    dataHashMap.put(Transaction.TransactionCol.trnDate.value(), resCursor.getString(3));
                    dataHashMap.put(Transaction.TransactionCol.pyeID.value(), resCursor.getInt(4));
                    dataHashMap.put(Transaction.TransactionCol.acnID.value(), resCursor.getInt(5));
                    dataHashMap.put(Transaction.TransactionCol.trnDescription.value(), resCursor.getString(6));
                    dataHashMap.put(Transaction.TransactionCol.trnImagePath.value(), resCursor.getString(7));
                    dataHashMap.put(Transaction.TransactionCol.trnColor.value(), resCursor.getString(8));
                    dataHashMap.put(Transaction.TransactionCol.trnType.value(), resCursor.getInt(9));
                    dataHashMap.put(Transaction.TransactionCol.trnReference.value(), resCursor.getInt(11));

                    dataHashMap.put(GroupDetail.GroupDetailCol.lstID.value(), resCursor.getInt(12));
                    dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(13));
                    dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(14));
                    dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(15));

                    dataHashMap.put(Group.GroupCol.lstName.value(), resCursor.getString(16));
                    dataHashMap.put(Group.GroupCol.lstIcon.value(), resCursor.getString(17));
                    dataHashMap.put(Group.GroupCol.lstColor.value(), resCursor.getString(18));
                    dataHashMap.put(Group.GroupCol.lstOrder.value(), resCursor.getInt(19));

                    hashMapArray.add(dataHashMap);

                    resCursor.moveToNext();
                }

                resCursor.close();
                DBManager.close();

            }

            for (int i = 0; i < hashMapArray.size(); i++) {

                ArrayList<String> itemArray = new ArrayList<>();

                String name = String.valueOf(hashMapArray.get(i).get("lstName"));

                String amount = Currency.CurrencyString.getCurrencyString(String.valueOf(hashMapArray.get(i).get("trnAmount")), Currency.CurrencyMode.Short).separated;
                long dateTimeInterval = Long.parseLong(String.valueOf(hashMapArray.get(i).get("trnDate")));
                String date = PublicModules.timeStamp2String(dateTimeInterval, "yyyy/MM/dd hh:mm");
                String iconCode = String.valueOf(hashMapArray.get(i).get("lstIcon"));
                String iconColor = String.valueOf(hashMapArray.get(i).get("lstColor"));
                String color = String.valueOf(hashMapArray.get(i).get("trnColor"));
                String transactionType = String.valueOf(hashMapArray.get(i).get("trnType"));

                itemArray.add(name);
                itemArray.add(amount);
                itemArray.add(date);
                itemArray.add(iconCode);
                itemArray.add(iconColor);
                itemArray.add(color);
                itemArray.add(transactionType);


                resArray.add(itemArray);

            }

            return resArray;
        }

    }

    public static class Widget {

        public static ArrayList<ArrayList<String>> chart1Data() {

            ArrayList<ArrayList<String>> resArray = new ArrayList<>();

            ArrayList<String> expenses = new ArrayList<>();
            ArrayList<String> incomes = new ArrayList<>();

            Cursor res1 = null;
            Cursor res2 = null;

            DBManager.openForRead();

            for (int i = 3; i > 0; i--) {

                long[] currentMonth = PublicModules.getMonthInterval(-1 * i);


                res1 = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=1 AND trnDate>= " + currentMonth[0] + " AND trnDate < " + currentMonth[1], null);
                res2 = DBManager.database.rawQuery("SELECT IFNULL(CAST(SUM(trnAmount) AS REAL),0) FROM T_Transaction WHERE trnType=2 AND trnDate>= " + currentMonth[0] + " AND trnDate < " + currentMonth[1], null);

                res1.moveToFirst();
                double income = res1.getDouble(0);
                incomes.add(Currency.CurrencyString.getCurrencyString(income, Currency.CurrencyMode.Short).separated);

                res2.moveToFirst();
                double expense = res2.getDouble(0);


                expenses.add(Currency.CurrencyString.getCurrencyString(expense, Currency.CurrencyMode.Short).separated);
            }

            res1.close();
            res2.close();
            DBManager.close();


            return resArray;
        }

    }

    public static class MonthReport {

        public String month;
        public String expense;
        public String income;

        public MonthReport() {

        }

    }
}