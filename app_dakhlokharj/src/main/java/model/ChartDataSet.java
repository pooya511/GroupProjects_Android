package model;

import android.graphics.Color;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.List;

/**
 * Created by Parsa on 2017-11-15.
 */

public class ChartDataSet extends LineDataSet{

    public ChartDataSet(final List<Entry> yVals, String label, String colorString,boolean drawCircles) {
        super(yVals, label);
        this.setDrawCircleHole(false);
        this.setCircleColor(Color.parseColor(colorString));
        this.setColor(Color.parseColor(colorString));
        this.setDrawCircles(drawCircles);
        this.setCircleRadius(2.0f);
        this.setLineWidth(1);
        this.setDrawValues(false);
        this.setDrawVerticalHighlightIndicator(false);
        this.setDrawHorizontalHighlightIndicator(false);
        this.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.valueOf(yVals.get(dataSetIndex));
            }
        });
    }
}
