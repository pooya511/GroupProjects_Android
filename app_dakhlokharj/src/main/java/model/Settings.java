package model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.taxiapps.dakhlokharj.Application;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Settings {

    // app directory in external storage
    public static final String APP_DIRECTORY = "APP_DIRECTORY";

    // app amount
    public static final String APP_AMOUNT = "APP_AMOUNT";

    // currency Setting
    public static final String CASH_TYPE_INDEX = "CASH_TYPE_INDEX";
    public static final String AMOUNT_TYPE_FACE = "AMOUNT_TYPE_FACE";

    // backup setting
    public static final String BACKUP_COUNTER = "BACKUP_COUNTER";
    public static final String LAST_BACKUP_FILE_NAME = "LAST_BACKUP_FILE_NAME";
    public static final String BACKUP_DESTINATION = "BACKUP_DESTINATION";

    // datePicker Setting
    public static final String LAST_DATEPICKER_TYPE = "LAST_DATEPICKER_TYPE";

    // main Setting
    public static final String EASY_ACCESS_TYPE = "EASY_ACCESS_TYPE";
    public static final String LAST_WIDGET = "LAST_WIDGET";

    // Password
    public static final String PASSWORD = "PASSWORD";
    public static final String FINGER_PRINT_IS_ENABLE = "FINGER_PRINT_IS_ENABLE";

    // filter type for expenses and incomes
    public static final String EXPENSE_ADAPTER_FILTER = "EXPENSE_ADAPTER_FILTER";
    public static final String INCOME_ADAPTER_FILTER = "INCOME_ADAPTER_FILTER";

    // screenshot paths
    public static final String CHART_SCREENSHOT_PATH = "CHART_SCREENSHOT_PATH";
    public static final String SMART_REPORT_SCREENSHOT_PATH = "SMART_REPORT_SCREENSHOT_PATH";
    public static final String REPORT_BY_MONTH_SCREENSHOT_PATH = "REPORT_BY_MONTH_SCREENSHOT_PATH";
    public static final String PERIOD_REPORT_SCREENSHOT_PATH = "PERIOD_REPORT_SCREENSHOT_PATH";
    public static final String PAYEE_REPORT_SCREENSHOT_PATH = "PAYEE_REPORT_SCREENSHOT_PATH";
    public static final String CATEGORY_REPORT_SCREENSHOT_PATH = "CATEGORY_REPORT_SCREENSHOT_PATH";
    public static final String SUMMARY_REPORT_SCREENSHOT_PATH = "SUMMARY_REPORT_SCREENSHOT_PATH";
    public static final String DETAIL_ACCOUNT_SCREENSHOT_PATH = "DETAIL_ACCOUNT_SCREENSHOT_PATH";

    // api requirements
    public static final String TX_USER_NUMBER = "TX_USER_NUMBER";
    public static final String LICENSE_DATA = "LICENSE_DATA";
    public static final String LICENSE_EXPIRE = "LICENSE_EXPIRE";
    public static final String USER_RATE = "USER_RATE";
    public static final String PURCHASE_STATUS = "PURCHASE_STATUS";

    // sms
    public static final String SMS_IS_ENABLE = "SMS_IS_ENABLE";
    public static final String SMS_NUMBERS = "SMS_NUMBERS";
    public static final String LAST_SMS_COUNT = "LAST_SMS_COUNT";

    // app
    public static final String APP_RUNNING_NUMBER = "APP_RUNNING_NUMBER";
    public static final String APP_VERSION = "APP_VERSION";

    // google drive settings
    public static final String ACCOUNT_NAME = "ACCOUNT_NAME";
    public static final String CLOUD_ERROR = "CLOUD_ERROR";
    public static final String AUTO_BACKUP = "AUTO_BACKUP";
    public static final String BACKUP_IMAGE = "BACKUP_IMAGE";
    public static final String BACKUP_PERIOD = "BACKUP_PERIOD";
    public static final String BACKUP_MAXIMUM_FILE_COUNT = "BACKUP_MAXIMUM_FILE_COUNT";
    public static final String BACKUP_LAST_TIME = "BACKUP_LAST_TIME";

    // list orders
    public static final String ACCOUNT_ORDER_MAP = "ACCOUNT_ORDER_MAP";
    public static final String PAYEE_ORDER_MAP = "PAYEE_ORDER_MAP";
    public static final String IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME = "IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME";

    private static final String APP_DIRECTORY_DV = "/storage/emulated/0/Dakhl_o_kharj";
    private static final String CASH_TYPE_INDEX_DV = "0";
    private static final String AMOUNT_TYPE_FACE_DV = "yekan.ttf";
    private static final String BACKUP_COUNTER_DV = "2";
    private static final String BACKUP_DESTINATION_DV = "/storage/emulated/0/Dakhl_o_kharj/DakhloKharj_backups";
    private static final String LAST_BACKUP_FILE_NAME_DV = "";
    private static final String LAST_DATE_PICKER_TYPE_DV = "";
    private static final String EASY_ACCESS_DV = "1";
    private static final String LAST_WIDGET_DV = "0";
    private static final String EXPENSE_ADAPTER_FILTER_DV = "0";
    private static final String PASSWORD_DV = "";
    private static final String CHART_SCREENSHOT_PATH_DV = "";
    private static final String SMART_REPORT_SCREENSHOT_PATH_DV = "";
    private static final String REPORT_BY_MONTH_SCREENSHOT_PATH_DV = "";
    private static final String PERIOD_REPORT_SCREENSHOT_PATH_DV = "";
    private static final String PAYEE_REPORT_SCREENSHOT_PATH_DV = "";
    private static final String CATEGORY_REPORT_SCREENSHOT_PATH_DV = "";
    private static final String SUMMARY_REPORT_SCREENSHOT_PATH_DV = "";
    private static final String DETAIL_ACCOUNT_SCREENSHOT_PATH_DV = "";
    private static final String INCOME_ADAPTER_FILTER_DV = "0";
    private static final String FINGER_PRINT_IS_ENABLE_DV = "0";
    private static final String TX_USER_NUMBER_DV = "";
    private static final String USER_RATE_DV = "0";
    private static final String APP_AMOUNT_DV = "0";
    private static final String PURCHASE_STATUS_DV = "-1";
    private static final String LICENSE_DATA_DV = "";
    private static final String LICENSE_EXPIRE_DV = "0";
    private static final String SMS_IS_ENABLE_DV = "true";
    private static final String LAST_SMS_COUNT_DV = "0";
    private static final String APP_RUNNING_NUMBER_DV = "0";

    private static final String ACCOUNT_NAME_DV = "";
    private static final String CLOUD_ERROR_DV = "1";
    private static final String AUTO_BACKUP_DV = "0";
    private static final String BACKUP_IMAGE_DV = "0";
    private static final String BACKUP_LAST_TIME_DV = "0";
    private static final String BACKUP_PERIOD_DV = EnumsAndConstants.BackupPeriodItems.Daily.value();
    private static final String BACKUP_MAXIMUM_FILE_COUNT_DV = String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.Five.value());
    private static final String IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME_DV = "0";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public static void init(Context context) {

        sharedPreferences = context.getSharedPreferences(Application.AppEnums.SHARED_PREFRENCES.value(), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (!sharedPreferences.contains(APP_DIRECTORY)) {
            editor.putString(APP_DIRECTORY, APP_DIRECTORY_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(AMOUNT_TYPE_FACE)) {
            editor.putString(AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Arabic.value());
            editor.apply();
        }

        if (!sharedPreferences.contains(CASH_TYPE_INDEX)) {
            editor.putString(CASH_TYPE_INDEX, CASH_TYPE_INDEX_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(BACKUP_COUNTER)) {
            editor.putString(BACKUP_COUNTER, BACKUP_COUNTER_DV);
            editor.apply();
        }

        /*
         * /storage/emulated/0/Dakhl_o_kharj == new File(Environment.getExternalStorageDirectory(), "Dakhl_o_kharj")
         */
        if (!sharedPreferences.contains(BACKUP_DESTINATION)) {
            editor.putString(BACKUP_DESTINATION, BACKUP_DESTINATION_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(LAST_BACKUP_FILE_NAME)) {
            editor.putString(LAST_BACKUP_FILE_NAME, LAST_BACKUP_FILE_NAME_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(LAST_DATEPICKER_TYPE)) {
            editor.putString(LAST_DATEPICKER_TYPE, LAST_DATE_PICKER_TYPE_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(EASY_ACCESS_TYPE)) {
            editor.putString(EASY_ACCESS_TYPE, EASY_ACCESS_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(LAST_WIDGET)) {
            editor.putString(LAST_WIDGET, LAST_WIDGET_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(EXPENSE_ADAPTER_FILTER)) {
            editor.putString(EXPENSE_ADAPTER_FILTER, EXPENSE_ADAPTER_FILTER_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(INCOME_ADAPTER_FILTER)) {
            editor.putString(INCOME_ADAPTER_FILTER, INCOME_ADAPTER_FILTER_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(PASSWORD)) {
            editor.putString(PASSWORD, PASSWORD_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(CHART_SCREENSHOT_PATH)) {
            editor.putString(CHART_SCREENSHOT_PATH, CHART_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(SMART_REPORT_SCREENSHOT_PATH)) {
            editor.putString(SMART_REPORT_SCREENSHOT_PATH, SMART_REPORT_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(REPORT_BY_MONTH_SCREENSHOT_PATH)) {
            editor.putString(REPORT_BY_MONTH_SCREENSHOT_PATH, REPORT_BY_MONTH_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(PERIOD_REPORT_SCREENSHOT_PATH)) {
            editor.putString(PERIOD_REPORT_SCREENSHOT_PATH, PERIOD_REPORT_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(PAYEE_REPORT_SCREENSHOT_PATH)) {
            editor.putString(PAYEE_REPORT_SCREENSHOT_PATH, PAYEE_REPORT_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(CATEGORY_REPORT_SCREENSHOT_PATH)) {
            editor.putString(CATEGORY_REPORT_SCREENSHOT_PATH, CATEGORY_REPORT_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(SUMMARY_REPORT_SCREENSHOT_PATH)) {
            editor.putString(SUMMARY_REPORT_SCREENSHOT_PATH, SUMMARY_REPORT_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(DETAIL_ACCOUNT_SCREENSHOT_PATH)) {
            editor.putString(DETAIL_ACCOUNT_SCREENSHOT_PATH, DETAIL_ACCOUNT_SCREENSHOT_PATH_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(FINGER_PRINT_IS_ENABLE)) {
            editor.putString(FINGER_PRINT_IS_ENABLE, FINGER_PRINT_IS_ENABLE_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(TX_USER_NUMBER)) {
            editor.putString(TX_USER_NUMBER, TX_USER_NUMBER_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(USER_RATE)) {
            editor.putString(USER_RATE, USER_RATE_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(PURCHASE_STATUS)) {
            editor.putString(PURCHASE_STATUS, PURCHASE_STATUS_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(LICENSE_DATA)) {
            editor.putString(LICENSE_DATA, LICENSE_DATA_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(LICENSE_EXPIRE)) {
            editor.putString(LICENSE_EXPIRE, LICENSE_EXPIRE_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(SMS_IS_ENABLE)) {
            editor.putString(SMS_IS_ENABLE, SMS_IS_ENABLE_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(LAST_SMS_COUNT)) {
            editor.putString(LAST_SMS_COUNT, LAST_SMS_COUNT_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(APP_RUNNING_NUMBER)) {
            editor.putString(APP_RUNNING_NUMBER, APP_RUNNING_NUMBER_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(APP_VERSION)) {
            editor.putString(APP_VERSION, EnumsAndConstants.APP_VERSION);
            editor.apply();
        }

        if (!sharedPreferences.contains(APP_AMOUNT)) {
            editor.putString(APP_AMOUNT, APP_AMOUNT_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(ACCOUNT_NAME)) {
            editor.putString(ACCOUNT_NAME, ACCOUNT_NAME_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(CLOUD_ERROR)) {
            editor.putString(CLOUD_ERROR, CLOUD_ERROR_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(AUTO_BACKUP)) {
            editor.putString(AUTO_BACKUP, AUTO_BACKUP_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(BACKUP_IMAGE)) {
            editor.putString(BACKUP_IMAGE, BACKUP_IMAGE_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(BACKUP_PERIOD)) {
            editor.putString(BACKUP_PERIOD, BACKUP_PERIOD_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(BACKUP_MAXIMUM_FILE_COUNT)) {
            editor.putString(BACKUP_MAXIMUM_FILE_COUNT, BACKUP_MAXIMUM_FILE_COUNT_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(BACKUP_LAST_TIME)) {
            editor.putString(BACKUP_LAST_TIME, BACKUP_LAST_TIME_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME)) {
            editor.putString(IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME, IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME_DV);
            editor.apply();
        }

        if (!sharedPreferences.contains(SMS_NUMBERS)) {
            Set<String> defaultSet = new HashSet<>();
            defaultSet.add("+981000");
            defaultSet.add("+982000");
            defaultSet.add("+983000");
            defaultSet.add("+985000");
            editor.putStringSet(SMS_NUMBERS, defaultSet);
            editor.apply();
        }

    }

    public static String getSetting(String key) {
        return sharedPreferences.getString(key, "");
    }

    public static void setSetting(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public static Set<String> getStringSet(String key) {
        return sharedPreferences.getStringSet(key, null);
    }

    public static void setStringSet(String key, Set<String> values) {
        editor.putStringSet(key, values);
        editor.apply();
    }

    public static void saveMap(String key, HashMap<String, String> inputMap) {
        if (sharedPreferences != null) {
            Gson gson = new Gson();
            String hashMapString = gson.toJson(inputMap);
            sharedPreferences.edit().putString(key, hashMapString).apply();
        }
    }

    public static HashMap<String, String> loadMap(String key){
        HashMap<String, String> outputMap = new HashMap<>();
        try{
            String storedHashMapString = sharedPreferences.getString(key, (new JSONObject()).toString());
            java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
            Gson gson = new Gson();
            return  gson.fromJson(storedHashMapString, type);

        }catch(Exception e){
            e.printStackTrace();
        }
        return outputMap;
    }

}
