package model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;

import db.DBManager;

/**
 * Created by Parsa on 9/20/2017.
 */

public class GroupDetail extends TX_Entity implements Parcelable {

    public GroupDetail() {

    }

    public GroupDetail(String lsdName) {
        setLsdName(lsdName);
    }

    protected GroupDetail(Parcel in) {
    }

    public static final Creator<GroupDetail> CREATOR = new Creator<GroupDetail>() {
        @Override
        public GroupDetail createFromParcel(Parcel in) {
            return new GroupDetail(in);
        }

        @Override
        public GroupDetail[] newArray(int size) {
            return new GroupDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public enum GroupDetailCol {

        id("ID"),
        lstID("lstID"),
        lsdName("lsdName"),
        lsdIcon("lsdIcon"),
        lsdColor("lsdColor");


        private final String name;

        GroupDetailCol(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }

    }

    private static final String childTableName = "T_List_Detail";
    private ArrayList<DBCol> dbColArrayList = new ArrayList<>(
            Arrays.asList(new DBCol(null, GroupDetailCol.id.value(), EnumsAndConstants.Types.INTEGER, 10, false, false, true),
                    new DBCol(null, GroupDetailCol.lstID.value(), EnumsAndConstants.Types.INTEGER, 20, true, true, false),
                    new DBCol(null, GroupDetailCol.lsdName.value(), EnumsAndConstants.Types.STRING, 30, true, true, false),
                    new DBCol(null, GroupDetailCol.lsdIcon.value(), EnumsAndConstants.Types.STRING, 40, true, true, false),
                    new DBCol(null, GroupDetailCol.lsdColor.value(), EnumsAndConstants.Types.STRING, 50, true, true, false)
            )
    );

    @Override
    String childTableName() {
        return childTableName;
    }

    @Override
    ArrayList<DBCol> childDBColArrayList() {
        return dbColArrayList;
    }

    public GroupDetail(int ID, int lstID, String lsdName, String lsdIcon, String lsdColor) {
        setID(ID);
        setLstID(lstID);
        setLsdName(lsdName);
        setLsdIcon(lsdIcon);
        setLsdColor(lsdColor);
    }

    public int getID() {
        return super.GetInt("ID");
    }

    public void setID(int ID) {
        super.Set("ID", ID);
    }

    public int getLstID() {
        return super.GetInt("lstID");
    }

    public void setLstID(int lstID) {
        super.Set("lstID", lstID);
    }

    public String getLsdName() {
        return super.GetString("lsdName");
    }

    public void setLsdName(String lsdName) {
        super.Set("lsdName", lsdName);
    }

    public String getLsdIcon() {
        return super.GetString("lsdIcon");
    }

    public void setLsdIcon(String lsdIcon) {
        super.Set("lsdIcon", lsdIcon);
    }

    public String getLsdColor() {
        return super.GetString("lsdColor");
    }

    public void setLsdColor(String lsdColor) {
        super.Set("lsdColor", lsdColor);
    }

    @Override
    public int insert() {
        int inserted_id = super.insert();
        setID(inserted_id);
        return inserted_id;
    }

    public static ArrayList<GroupDetail> getAll(int groupID) {

        ArrayList<GroupDetail> groupDetails = new ArrayList<>();

        String query = "SELECT * FROM T_List_Detail WHERE lstID=" + groupID;

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {

            GroupDetail groupDetail = new GroupDetail();

            groupDetail.setID(resCursor.getInt(0));
            groupDetail.setLstID(resCursor.getInt(1));
            groupDetail.setLsdName(resCursor.getString(2));
            groupDetail.setLsdIcon(resCursor.getString(3));
            groupDetail.setLsdColor(resCursor.getString(4));

            groupDetails.add(groupDetail);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return groupDetails;
    }

    public static GroupDetail getGroupDetailByID(int groupDetailID) {

        GroupDetail targetGroupDetail = new GroupDetail();

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery("SELECT * FROM T_List_Detail WHERE ID = " + groupDetailID, null);
        resCursor.moveToFirst();
        if (resCursor.getCount() > 0) {
            targetGroupDetail.setID(resCursor.getInt(resCursor.getColumnIndex(GroupDetailCol.id.value())));
            targetGroupDetail.setLstID(resCursor.getInt(resCursor.getColumnIndex(GroupDetailCol.lstID.value())));
            targetGroupDetail.setLsdName(resCursor.getString(resCursor.getColumnIndex(GroupDetailCol.lsdName.value())));
            targetGroupDetail.setLsdIcon(resCursor.getString(resCursor.getColumnIndex(GroupDetailCol.lsdIcon.value())));
            targetGroupDetail.setLsdColor(resCursor.getString(resCursor.getColumnIndex(GroupDetailCol.lsdColor.value())));
            return targetGroupDetail;
        }
        resCursor.close();
        DBManager.close();

        return null;
    }

}
