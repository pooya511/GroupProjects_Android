package model;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.AccountListActivity;
import com.taxiapps.dakhlokharj.AccountToAccountActivity;
import com.taxiapps.dakhlokharj.NewAndEditAccountActivity;
import com.taxiapps.dakhlokharj.R;

import java.io.Serializable;
import java.util.*;

import adapters.AccountAdapter;
import db.DBManager;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.AppModules;
import utils.DialogUtils;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Account extends TX_Entity implements Parcelable {

    private static AccountAdapter accountAdapter;
    public static RecyclerView accountRecycle;
    public static ConstraintLayout didntFind;

    public Account() {

    }

    protected Account(Parcel in) {
        isExpanded = in.readByte() != 0;
        dbColArrayList = in.createTypedArrayList(DBCol.CREATOR);
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isExpanded ? 1 : 0));
        dest.writeTypedList(dbColArrayList);
    }

    public enum AccountCol {

        id("ID"),
        acnName("acnName"),
        acnAccount("acnAccount"),
        acnCard("acnCard"),
        acnSHABA("acnSHABA"),
        acnType("acnType"),
        acnIcon("acnIcon"),
        acnIsActive("acnIsActive"),
        acnVisibleHomePage("acnVisibleHomePage"),
        acnBalance("acnBalance"),
        rowAddedDate("RowAddedDate");

        private final String name;

        AccountCol(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }

    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public String parsa = "parsa";

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public boolean isExpanded; // this item is for UI

    private static final String childTableName = "T_Account";
    private ArrayList<DBCol> dbColArrayList = new ArrayList<>(
            Arrays.asList(new DBCol(null, AccountCol.id.value(), EnumsAndConstants.Types.INTEGER, 10, false, false, true),
                    new DBCol(null, AccountCol.acnName.value(), EnumsAndConstants.Types.STRING, 20, true, true, false),
                    new DBCol(null, AccountCol.acnAccount.value(), EnumsAndConstants.Types.STRING, 30, true, true, false),
                    new DBCol(null, AccountCol.acnCard.value(), EnumsAndConstants.Types.STRING, 40, true, true, false),
                    new DBCol(null, AccountCol.acnSHABA.value(), EnumsAndConstants.Types.STRING, 50, true, true, false),
                    new DBCol(null, AccountCol.acnType.value(), EnumsAndConstants.Types.STRING, 60, true, true, false),
                    new DBCol(null, AccountCol.acnIcon.value(), EnumsAndConstants.Types.STRING, 70, true, true, false),
                    new DBCol(null, AccountCol.acnIsActive.value(), EnumsAndConstants.Types.INTEGER, 80, true, true, false),
                    new DBCol(null, AccountCol.acnVisibleHomePage.value(), EnumsAndConstants.Types.INTEGER, 90, true, true, false),
                    new DBCol(null, AccountCol.acnBalance.value(), EnumsAndConstants.Types.INTEGER, 100, true, true, false),
                    new DBCol(null, AccountCol.rowAddedDate.value(), EnumsAndConstants.Types.STRING, 110, false, false, false)
            )
    );

    @Override
    String childTableName() {
        return childTableName;
    }

    @Override
    ArrayList<DBCol> childDBColArrayList() {
        return dbColArrayList;
    }

    public Account(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }

    public Account(int ID, String acnName, String acnAccount, String acnCard, String acnShaba, String acnType, int acnIcon, int acnIsActive, int acnVisibleHomePage, double acnBalance, String rowAdedDate) {
        setID(ID);
        setAcnName(acnName);
        setAcnAccount(acnAccount);
        setAcnCard(acnCard);
        setAcnShaba(acnShaba);
        setAcnType(acnType);
        setAcnIcon(acnIcon);
        setAcnIsActive(acnIsActive);
        setAcnVisibleHomePage(acnVisibleHomePage);
        setAcnBalance(String.valueOf(acnBalance));
        setRowAddedDate(rowAdedDate);
    }

    public Account(int ID, String acnName, String acnAccount, String acnCard, String acnShaba, String acnType, int acnIcon, int acnIsActive, int acnVisibleHomePage, double acnBalance) {
        setID(ID);
        setAcnName(acnName);
        setAcnAccount(acnAccount);
        setAcnCard(acnCard);
        setAcnShaba(acnShaba);
        setAcnType(acnType);
        setAcnIcon(acnIcon);
        setAcnIsActive(acnIsActive);
        setAcnVisibleHomePage(acnVisibleHomePage);
        setAcnBalance(String.valueOf(acnBalance));
    }

    public String getRowAddedDate() {
        return super.GetString("RowAddedDate");
    }

    public void setRowAddedDate(String rowAdedDate) {
        super.Set("RowAddedDate", rowAdedDate);
    }

    public int getID() {
        return super.GetInt("ID");
    }

    public void setID(int ID) {
        super.Set("ID", ID);
    }

    public String getAcnName() {
        return super.GetString("acnName");
    }

    public void setAcnName(String acnName) {
        super.Set("acnName", acnName);
    }

    public String getAcnAccount() {
        return super.GetString("acnAccount");
    }

    public void setAcnAccount(String acnAccount) {
        super.Set("acnAccount", acnAccount);
    }

    public String getAcnCard() {
        return super.GetString("acnCard");
    }

    public void setAcnCard(String acnCard) {
        super.Set("acnCard", acnCard);
    }

    public String getAcnShaba() {
        return super.GetString("acnSHABA");
    }

    public void setAcnShaba(String acnShaba) {
        super.Set("acnSHABA", acnShaba);
    }

    public String getAcnType() {
        return super.GetString("acnType");
    }

    public void setAcnType(String acnType) {
        super.Set("acnType", acnType);
    }

    public int getAcnIcon() {
        return super.GetInt("acnIcon");
    }

    public void setAcnIcon(int acnIcon) {
        super.Set("acnIcon", acnIcon);
    }

    public int getAcnIsActive() {
        return super.GetInt("acnIsActive");
    }

    public void setAcnIsActive(int acnIsActive) {
        super.Set("acnIsActive", acnIsActive);
    }

    public int getAcnVisibleHomePage() {
        return super.GetInt("acnVisibleHomePage");
    }

    public void setAcnVisibleHomePage(int acnVisibleHomePage) {
        super.Set("acnVisibleHomePage", acnVisibleHomePage);
    }

    public String getAcnBalance() {
        String res = String.valueOf(super.GetDouble("acnBalance"));
        res = Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("0") || Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("1") ?
                String.format(Locale.US,"%.0f", Double.valueOf(res)) :
                String.format(Locale.US,"%.2f", Double.valueOf(res));
        return PublicModules.toEnglishDigit(res);
    }

    public void setAcnBalance(String acnBalance) {
        acnBalance = PublicModules.toEnglishDigit(acnBalance);
        super.Set("acnBalance", Double.valueOf(PublicModules.toEnglishDigit(String.format(Locale.US,"%.2f", Double.valueOf(acnBalance))).replace("٫",".")));
    }

    public static ArrayList<Account> getAll() {
        ArrayList<Account> accountList = new ArrayList<>();
        String query = "SELECT A.*,B.bnkLogo FROM T_Account A INNER JOIN T_Bank B ON A.acnIcon = B.ID";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        while (!resultCursor.isAfterLast()) {

            Account account = new Account(false);

            account.setID(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.id.value())));
            account.setAcnName(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnName.value())));
            account.setAcnAccount(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnAccount.value())));
            account.setAcnCard(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnCard.value())));
            account.setAcnShaba(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnSHABA.value())));
            account.setAcnType(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnType.value())));
            account.setAcnIcon(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.acnIcon.value())));
            account.setAcnIsActive(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.acnIsActive.value())));
            account.setAcnVisibleHomePage(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.acnVisibleHomePage.value())));
            account.setAcnBalance(String.valueOf(resultCursor.getDouble(resultCursor.getColumnIndex(AccountCol.acnBalance.value()))));
            account.setRowAddedDate(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.rowAddedDate.value())));

            accountList.add(account);
            resultCursor.moveToNext();
        }
        resultCursor.close();
        DBManager.close();

        return accountList;
    }

    public static ArrayList<Account> getAllActive() {
        ArrayList<Account> accountList = new ArrayList<>();
        String query = "SELECT A.*,B.bnkLogo FROM T_Account A INNER JOIN T_Bank B ON A.acnIcon = B.ID WHERE acnIsActive = 1";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        while (!resultCursor.isAfterLast()) {
            Account account = new Account(false);

            account.setID(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.id.value())));
            account.setAcnName(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnName.value())));
            account.setAcnAccount(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnAccount.value())));
            account.setAcnCard(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnCard.value())));
            account.setAcnShaba(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnSHABA.value())));
            account.setAcnType(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.acnType.value())));
            account.setAcnIcon(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.acnIcon.value())));
            account.setAcnIsActive(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.acnIsActive.value())));
            account.setAcnVisibleHomePage(resultCursor.getInt(resultCursor.getColumnIndex(AccountCol.acnVisibleHomePage.value())));
            account.setAcnBalance(String.valueOf(resultCursor.getDouble(resultCursor.getColumnIndex(AccountCol.acnBalance.value()))));
            account.setRowAddedDate(resultCursor.getString(resultCursor.getColumnIndex(AccountCol.rowAddedDate.value())));

            accountList.add(account);
            resultCursor.moveToNext();
        }
        resultCursor.close();
        DBManager.close();

        return accountList;
    }

    public static ArrayList<Transaction> getAllTransaction(int accountId) {
        ArrayList<Transaction> transactionArray = new ArrayList<>();
        String query = "SELECT A.*,B.lstID,B.lsdName,B.lsdIcon,B.lsdColor,C.lstName,C.lstIcon,C.lstColor,C.lstOrder FROM T_Transaction A LEFT JOIN T_List_Detail B ON A.lsdID = B.ID LEFT JOIN T_List C ON B.lstID=C.ID WHERE A.acnID= " + accountId + " ORDER BY A.RowAddedDate DESC";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        while (!resultCursor.isAfterLast()) {
            Transaction transaction = new Transaction();

            transaction.setID(resultCursor.getInt(resultCursor.getColumnIndex(Transaction.TransactionCol.id.value())));
            transaction.setIsdID(resultCursor.getInt(resultCursor.getColumnIndex(Transaction.TransactionCol.lsdID.value())));
            transaction.setTrnAmount(String.valueOf(resultCursor.getDouble(resultCursor.getColumnIndex(Transaction.TransactionCol.trnAmount.value()))));
            transaction.setTrnDate(resultCursor.getString(resultCursor.getColumnIndex(Transaction.TransactionCol.trnDate.value())));
            transaction.setPyeId(resultCursor.getInt(resultCursor.getColumnIndex(Transaction.TransactionCol.pyeID.value())));
            transaction.setAcnId(resultCursor.getInt(resultCursor.getColumnIndex(Transaction.TransactionCol.acnID.value())));
            transaction.setTrnDescription(resultCursor.getString(resultCursor.getColumnIndex(Transaction.TransactionCol.trnDescription.value())));
            transaction.setTrnImagePath(resultCursor.getString(resultCursor.getColumnIndex(Transaction.TransactionCol.trnImagePath.value())));
            transaction.setTrnColor(resultCursor.getString(resultCursor.getColumnIndex(Transaction.TransactionCol.trnColor.value())));
            transaction.setTrnType(resultCursor.getInt(resultCursor.getColumnIndex(Transaction.TransactionCol.trnType.value())));
            transaction.setTrnReference(resultCursor.getInt(resultCursor.getColumnIndex(Transaction.TransactionCol.trnReference.value())));

            transaction.lstId = resultCursor.getInt(resultCursor.getColumnIndex(GroupDetail.GroupDetailCol.lstID.value()));
            transaction.lsdName = resultCursor.getString(resultCursor.getColumnIndex(GroupDetail.GroupDetailCol.lsdName.value()));
            transaction.lsdIcon = resultCursor.getString(resultCursor.getColumnIndex(GroupDetail.GroupDetailCol.lsdIcon.value()));
            transaction.lsdColor = resultCursor.getString(resultCursor.getColumnIndex(GroupDetail.GroupDetailCol.lsdColor.value()));

            transaction.lstName = resultCursor.getString(resultCursor.getColumnIndex(Group.GroupCol.lstName.value()));
            transaction.lsdIcon = resultCursor.getString(resultCursor.getColumnIndex(Group.GroupCol.lstIcon.value()));
            transaction.lstColor = resultCursor.getString(resultCursor.getColumnIndex(Group.GroupCol.lstColor.value()));
            transaction.lstOrder = resultCursor.getInt(resultCursor.getColumnIndex(Group.GroupCol.lstOrder.value()));

            transactionArray.add(transaction);
            resultCursor.moveToNext();
        }
        resultCursor.close();
        DBManager.close();

        return transactionArray;
    }

    public static Account getAccount(int id) {

        Account account = new Account();

        DBManager.openForRead();

        Cursor resCursor = DBManager.database.rawQuery("SELECT * FROM T_Account WHERE ID = " + id, null);
        if (resCursor.getCount() > 0) {
            resCursor.moveToFirst();

            account.setID(resCursor.getInt(resCursor.getColumnIndex(AccountCol.id.value())));
            account.setAcnName(resCursor.getString(resCursor.getColumnIndex(AccountCol.acnName.value())));
            account.setAcnAccount(resCursor.getString(resCursor.getColumnIndex(AccountCol.acnAccount.value())));
            account.setAcnCard(resCursor.getString(resCursor.getColumnIndex(AccountCol.acnCard.value())));
            account.setAcnShaba(resCursor.getString(resCursor.getColumnIndex(AccountCol.acnSHABA.value())));
            account.setAcnType(resCursor.getString(resCursor.getColumnIndex(AccountCol.acnType.value())));
            account.setAcnIcon(resCursor.getInt(resCursor.getColumnIndex(AccountCol.acnIcon.value())));
            account.setAcnIsActive(resCursor.getInt(resCursor.getColumnIndex(AccountCol.acnIsActive.value())));
            account.setAcnVisibleHomePage(resCursor.getInt(resCursor.getColumnIndex(AccountCol.acnVisibleHomePage.value())));
            account.setAcnBalance(String.valueOf(resCursor.getInt(resCursor.getColumnIndex(AccountCol.acnBalance.value()))));


        }
        resCursor.close();
        DBManager.close();


        return account;
    }

    public static double getBalance(int accountId) {
        String query = "SELECT IFNULL(SUM(CASE WHEN trnType=1 THEN -1*trnAmount ELSE trnAmount END) ,0) + IFNULL((SELECT acnBalance FROM T_Account WHERE ID= " + accountId + "),0) 'OUT'  FROM T_Transaction WHERE acnID= " + accountId;
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        double result = resultCursor.getDouble(0);
        resultCursor.close();
        DBManager.close();

        return result;
    }

    public static int count() {

        String query = "SELECT COUNT(*) FROM T_Account WHERE acnVisibleHomePage ='1'";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        int result = resultCursor.getInt(0);
        resultCursor.close();
        DBManager.close();

        return result;
    }

    @Override
    public int insert() {
        int inserted_id = super.insert();
        setID(inserted_id);
        return inserted_id;
    }

    @Override
    public void delete() {
        super.delete();
        ArrayList<Transaction> transactions = getAllTransaction(getID());
        for (int i = 0; i < transactions.size(); i++) {
            if (transactions.get(i).getTrnType() == 3 || transactions.get(i).getTrnType() == 4) {

                // ebteda refrence hay transaction ro pak mikonim
                Transaction transaction = new Transaction(transactions.get(i).getTrnReference());
                transaction.delete();

                // sepas khode transaction ro pak mikonim
                transactions.get(i).delete();
            }
        }
    }

    public interface AccountCallBack {
        void call(Account account);
    }

    public static Dialog accountList(final Context context, ArrayList<Account> accounts, ArrayList<Integer> exclusionIDs, final AccountCallBack callBack) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View banksLayout = inflater.inflate(R.layout.pop_bank_and_payee_select, null);

        final ImageView clearImg = banksLayout.findViewById(R.id.pop_bank_and_payee_clear_img);
        TextView text = banksLayout.findViewById(R.id.pop_bank_and_payee_text);
        final TextView addAccount = banksLayout.findViewById(R.id.pop_bank_and_payee_add);
        didntFind = banksLayout.findViewById(R.id.pop_bank_and_payee_didnt_add_account_layout);

        final EditText searchEditText = banksLayout.findViewById(R.id.pop_bank_and_payee_search_hint);
        searchEditText.setHint("جستجو نام حساب");
        text.setText("هیچ حساب فعالی یافت نشد");
        addAccount.setText("+ اضافه کردن حساب");

        accountRecycle = banksLayout.findViewById(R.id.pop_bank_and_payee_recycler);
        accountRecycle.setLayoutManager(new LinearLayoutManager(context));
        accountRecycle.setHasFixedSize(true);

        if (exclusionIDs != null) {
            for (int i = 0; i < accounts.size(); i++) {
                for (int j = 0; j < exclusionIDs.size(); j++) {
                    if (accounts.get(i).getID() == exclusionIDs.get(j)) {
                        accounts.remove(i);
                    }
                }
            }
        }

        if (accounts.size() == 0) {
            accountRecycle.setVisibility(View.GONE);
            didntFind.setVisibility(View.VISIBLE);
        } else {
            accountRecycle.setVisibility(View.VISIBLE);
            didntFind.setVisibility(View.GONE);
            accountAdapter = new AccountAdapter(context, callBack, Account.sortAccountsOrder(accounts, Settings.loadMap(Settings.ACCOUNT_ORDER_MAP)));
            accountAdapter.notifyDataSetChanged();
            accountRecycle.setAdapter(accountAdapter);
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 1) {
                    ArrayList<Account> filteredAccounts = AppModules.filterAccount(String.valueOf(s), Account.getAllActive());
                    if (filteredAccounts.size() == 0) {
                        accountRecycle.setVisibility(View.GONE);
                        didntFind.setVisibility(View.VISIBLE);
                        addAccount.setVisibility(View.GONE
                        );
                    } else {
                        accountRecycle.setVisibility(View.VISIBLE);
                        didntFind.setVisibility(View.GONE);
                        accountAdapter.setAccountArrayList(Account.sortAccountsOrder(filteredAccounts, Settings.loadMap(Settings.ACCOUNT_ORDER_MAP)));
                        accountRecycle.setAdapter(accountAdapter);
                        accountAdapter.notifyDataSetChanged();
                    }

                    if (s.length() > 0) {
                        clearImg.setVisibility(View.VISIBLE);
                    } else {
                        clearImg.setVisibility(View.GONE);
                    }

                }

            }
        };
        searchEditText.addTextChangedListener(textWatcher);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 80;
                height = 63;
                break;
            case 2:
                width = 70;
                height = 57;
                break;
            case 3:
                width = 60;
                height = 50;
                break;
        }

        Dialog accountDialog = PublicDialogs.makeDialog(context, banksLayout, width, height, true);

        addAccount.setOnClickListener(v -> {
            Intent intent = new Intent(context, NewAndEditAccountActivity.class);
            ((Activity) context).startActivityForResult(intent, AccountListActivity.REQUEST_CODE_ADD_ACCOUNT);
            accountDialog.dismiss();
        });
        clearImg.setOnClickListener(v -> searchEditText.setText(""));

        return accountDialog;
    }

    public static ArrayList<Account> sortAccountsOrder(ArrayList<Account> accounts, HashMap<String, String> order) {
        List<Map.Entry<String, String>> orderMap = sortByValue(order);

        ArrayList<Account> sortArray = new ArrayList<>();
        ArrayList<Account> allAccounts = accounts;

        if (allAccounts.size() != 0) {
            for (int i = 0; i < orderMap.size(); i++) {
                Account tempAccount = getAccountById(allAccounts, Integer.parseInt(orderMap.get(i).getKey()));
                if (tempAccount != null)
                    sortArray.add(tempAccount);
            }
        }

        return sortArray;
    }

    public static void updateAccountOrderHashMap(ArrayList<Account> accounts) {
        LinkedHashMap<String, String> order = new LinkedHashMap<>();

        if (accounts.size() != 0) {
            for (int i = 0; i < accounts.size(); i++) {
                order.put(String.valueOf(accounts.get(i).getID()), String.valueOf(i));
            }
        }

        Settings.saveMap(Settings.ACCOUNT_ORDER_MAP, order);
    }

    public static List<Map.Entry<String, String>> sortByValue(HashMap<String, String> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, String>> list =
                new LinkedList<>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, (o1, o2) -> (o1.getValue()).compareTo(o2.getValue()));

        return list;
    }

    private static Account getAccountById(ArrayList<Account> accounts, int id) {

        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getID() == id) {
                return accounts.get(i);
            }
        }
        return null;
    }

}