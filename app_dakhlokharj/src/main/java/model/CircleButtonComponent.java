package model;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import at.markushi.ui.CircleButton;

/**
 * Created by Parsa on 2017-12-16.
 */

public class CircleButtonComponent extends CircleButton {

    private String color;
    private int id;

    public CircleButtonComponent(Context context) {
        super(context);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.setColor(Color.parseColor(color));
        this.color = color;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }
}
