package model;

import android.os.Parcel;
import android.os.Parcelable;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Parsa on 2017-11-27.
 */

public class ChartConfig implements Parcelable {

    private ArrayList<List<Entry>> entries;
    private ArrayList<String> labels;
    private ArrayList<String> colors;
    private List<String> axisLabels;
    private int bottomLabelCount;
    private int leftLabelCount;
    private float labelRotationAngle;
    private boolean drawCircles;
    private boolean hasEnoughData;

    public ChartConfig(ArrayList<List<Entry>> entries, ArrayList<String> labels, ArrayList<String> colors, List<String> axisLabels, int bottomLabelCount, int leftLabelCount, float labelRotationAngle,boolean drawCircles,boolean hasEnoughData) {
        this.entries = entries;
        this.labels = labels;
        this.colors = colors;
        this.axisLabels = axisLabels;
        this.bottomLabelCount = bottomLabelCount;
        this.leftLabelCount = leftLabelCount;
        this.labelRotationAngle = labelRotationAngle;
        this.drawCircles = drawCircles;
        this.hasEnoughData = hasEnoughData;
    }

    protected ChartConfig(Parcel in) {
        labels = in.createStringArrayList();
        colors = in.createStringArrayList();
        axisLabels = in.createStringArrayList();
        bottomLabelCount = in.readInt();
        leftLabelCount = in.readInt();
        labelRotationAngle = in.readFloat();
        drawCircles = in.readByte() != 0;
        hasEnoughData = in.readByte() != 0;
    }

    public static final Creator<ChartConfig> CREATOR = new Creator<ChartConfig>() {
        @Override
        public ChartConfig createFromParcel(Parcel in) {
            return new ChartConfig(in);
        }

        @Override
        public ChartConfig[] newArray(int size) {
            return new ChartConfig[size];
        }
    };

    public ArrayList<List<Entry>> getEntries() {
        return entries;
    }

    public void setEntries(ArrayList<List<Entry>> entries) {
        this.entries = entries;
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<String> labels) {
        this.labels = labels;
    }

    public ArrayList<String> getColors() {
        return colors;
    }

    public void setColors(ArrayList<String> colors) {
        this.colors = colors;
    }

    public List<String> getAxisLabels() {
        return axisLabels;
    }

    public void setAxisLabels(ArrayList<String> axisLabels) {
        this.axisLabels = axisLabels;
    }

    public int getBottomLabelCount() {
        return bottomLabelCount;
    }

    public void setBottomLabelCount(int bottomLabelCount) {
        this.bottomLabelCount = bottomLabelCount;
    }

    public int getLeftLabelCount() {
        return leftLabelCount;
    }

    public void setLeftLabelCount(int leftLabelCount) {
        this.leftLabelCount = leftLabelCount;
    }

    public float getLabelRotationAngle() {
        return labelRotationAngle;
    }

    public void setLabelRotationAngle(float labelRotationAngle) {
        this.labelRotationAngle = labelRotationAngle;
    }

    public boolean isDrawCircles() {
        return drawCircles;
    }

    public void setDrawCircles(boolean drawCircles) {
        this.drawCircles = drawCircles;
    }

    public boolean isHasEnoughData() {
        return hasEnoughData;
    }

    public void setHasEnoughData(boolean hasEnoughData) {
        this.hasEnoughData = hasEnoughData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(labels);
        dest.writeStringList(colors);
        dest.writeStringList(axisLabels);
        dest.writeInt(bottomLabelCount);
        dest.writeInt(leftLabelCount);
        dest.writeFloat(labelRotationAngle);
        dest.writeByte((byte) (drawCircles ? 1 : 0));
        dest.writeByte((byte) (hasEnoughData ? 1 : 0));
    }
}
