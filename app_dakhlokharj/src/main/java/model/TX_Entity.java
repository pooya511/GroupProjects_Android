package model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;

import db.DBManager;
import utils.AppModules;

/**
 * Created by Parsa on 9/20/2017.
 */

public abstract class TX_Entity{

    abstract String childTableName();

    abstract ArrayList<DBCol> childDBColArrayList();

    private DBCol findDBCol(String colKey) {
        for (DBCol dbcol : childDBColArrayList()) {
            if (dbcol.key.equals(colKey))
                return dbcol;
        }
        return null;
    }

    public String GetString(String colKey) {
        if (colKey != null && findDBCol(colKey).value != null)
            return String.valueOf(findDBCol(colKey).value);
        else
            return null;
    }

    public int GetInt(String colKey) {
        if (colKey != null && findDBCol(colKey).value != null)
            return Integer.parseInt(String.valueOf(findDBCol(colKey).value));
        else
            return -1;
    }

    public long GetLong(String colKey) {
        if (findDBCol(colKey) != null)
            return (long) findDBCol(colKey).value;
        else
            return -1;
    }

    public double GetDouble(String colKey) {
        if (colKey != null && findDBCol(colKey).value != null)
            return Double.parseDouble(String.valueOf(findDBCol(colKey).value));
        else
            return -1;
    }

    public float GetFloat(String colKey) {
        if (colKey != null && findDBCol(colKey).value != null)
            return Float.parseFloat(String.valueOf(findDBCol(colKey).value));
        else
            return -1;
    }

    public boolean GetBool(String colKey) {
        if (colKey != null && findDBCol(colKey).value != null)
            return Boolean.parseBoolean(String.valueOf(findDBCol(colKey).value));
        else
            return false;
    }

    public void Set(String colKey, Object colValue) {
        if (findDBCol(colKey) == null)
            return;
        else
            findDBCol(colKey).value = colValue;

    }

    private DBCol getPk() {
        for (int i = 0; i < childDBColArrayList().size(); i++) {
            if (childDBColArrayList().get(i).isPK) {
                return childDBColArrayList().get(i);
            }
        }
        return null;
    }

    private boolean contains(String[] strArray, String key) {
        for (int i = 0; i < strArray.length; i++) {
            if (strArray[i].equals(key))
                return true;
        }
        return false;
    }

    public int insert() {
        String query = "INSERT INTO " + childTableName() + " (@COLS) VALUES (@VALUES)";
        ArrayList<String> columnList = new ArrayList<>();
        ArrayList<String> valuesList = new ArrayList<>();
        //
        for (DBCol dbCol : childDBColArrayList()) {
            if (dbCol.inInsert) {
                columnList.add(dbCol.key);
                valuesList.add(AppModules.toDB(dbCol.value, dbCol.type));
            }
        }
        query = AppModules.replaceFirst(query, "@COLS", AppModules.makeStringFromArrayListStrings(columnList, ","));
        query = AppModules.replaceLast(query, "@VALUES", AppModules.makeStringFromArrayListStrings(valuesList, ","));
        //
        DBManager.openForWrite();
        DBManager.database.execSQL(query);
        DBCol pkCol = getPk();
        int inserted_id = 0;
        if (pkCol != null && pkCol.type == EnumsAndConstants.Types.INTEGER) {
            Cursor cursor = DBManager.database.rawQuery("SELECT MAX(" + pkCol.key + ") FROM " + childTableName(), null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                inserted_id = cursor.getInt(0);
                if (inserted_id != 0)
                    getPk().value = inserted_id;
            }
            cursor.close();
        }
        DBManager.close();
        //
        return inserted_id;
    }

    public void select() {
        String sql = "SELECT * FROM " + childTableName() + " WHERE " + getPk().key + "=" + AppModules.toDB(getPk().value, getPk().type) + " LIMIT 1";
        DBManager.openForRead();
        Cursor dataTabelCursor = DBManager.database.rawQuery(sql, null);
        dataTabelCursor.moveToFirst();

        String[] columns = new String[dataTabelCursor.getColumnCount()];

        for (int i = 0; i < dataTabelCursor.getColumnCount(); i++) {
            columns[i] = dataTabelCursor.getColumnName(i);
        }

        for (int i = 0; i < childDBColArrayList().size(); i++) {
            if (dataTabelCursor.getCount() > 0 && contains(columns, childDBColArrayList().get(i).key))
                Set(childDBColArrayList().get(i).key, dataTabelCursor.getString(dataTabelCursor.getColumnIndex(childDBColArrayList().get(i).key)));
            else
                Set(childDBColArrayList().get(i).key, null);
        }

        dataTabelCursor.close();
        DBManager.close();
    }

    public void update(String[] cols) {
        String sql = "UPDATE " + childTableName() + " SET @COLSVALUES WHERE " + getPk().key + "=" + AppModules.toDB(getPk().value, getPk().type);
        ArrayList<String> colValsList = new ArrayList<>();
        for (DBCol dbCol : childDBColArrayList()) {
            if (dbCol.inUpdate && !dbCol.isPK && (cols == null || contains(cols, dbCol.key))) {
                colValsList.add(dbCol.key + "=" + AppModules.toDB(dbCol.value, dbCol.type));
            }
        }
        sql = AppModules.replaceFirst(sql, "@COLSVALUES", AppModules.makeStringFromArrayListStrings(colValsList, ","));
        DBManager.openForWrite();
        DBManager.database.execSQL(sql);
        DBManager.close();
    }

    public void delete() {
        String sql = "DELETE FROM " + childTableName() + " WHERE " + getPk().key + "=" + AppModules.toDB(getPk().value, getPk().type);
        DBManager.openForWrite();
        DBManager.database.execSQL(sql);
        DBManager.close();
    }

}

class DBCol implements Parcelable {

    public Object value;
    public String key;
    public EnumsAndConstants.Types type;
    public int order;
    public boolean inInsert = true;
    public boolean inUpdate = true;
    public boolean isPK = true;

    public DBCol(Object value, String key, EnumsAndConstants.Types type, int order, boolean inInsert, boolean inUpdate, boolean isPK) {
        this.value = value;
        this.key = key;
        this.type = type;
        this.order = order;
        this.inInsert = inInsert;
        this.inUpdate = inUpdate;
        this.isPK = isPK;
    }

    protected DBCol(Parcel in) {
        key = in.readString();
        order = in.readInt();
        inInsert = in.readByte() != 0;
        inUpdate = in.readByte() != 0;
        isPK = in.readByte() != 0;
    }

    public static final Creator<DBCol> CREATOR = new Creator<DBCol>() {
        @Override
        public DBCol createFromParcel(Parcel in) {
            return new DBCol(in);
        }

        @Override
        public DBCol[] newArray(int size) {
            return new DBCol[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeInt(order);
        dest.writeByte((byte) (inInsert ? 1 : 0));
        dest.writeByte((byte) (inUpdate ? 1 : 0));
        dest.writeByte((byte) (isPK ? 1 : 0));
    }
}