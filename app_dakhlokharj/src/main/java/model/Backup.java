package model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import db.DBManager;
import saman.zamani.persiandate.PersianDate;

/**
 * Created by Parsa on 9/20/2017.
 */

/**
 * useless class ******************************
 */
public class Backup {

    public enum BackupType {

        Temp(1),
        Storage(2);

        private final int type;

        BackupType(int s) {
            type = s;
        }

        public int value() {
            return this.type;
        }

    }

    public enum RecoveryType {

        FromStoarge(1),
        FromShare(2);

        private final int type;

        RecoveryType(int s) {
            type = s;
        }

        public int value() {
            return this.type;
        }

    }

    public interface SelectBackUpCallBack {
        void call(File file);
    }

    private static void createBackupFile(BackupType backupType) throws IOException {

        File oldDbPath = new File(DBManager.database.getPath());

        File destinationPath = null;
        if (backupType == BackupType.Temp)
            destinationPath = new File("/storage/emulated/0/Document/DakhloKharj_backups/Temp");
        else if (backupType == BackupType.Storage)
            destinationPath = new File(Settings.getSetting(Settings.BACKUP_DESTINATION));

        if (!destinationPath.exists())
            destinationPath.mkdirs();

        /*
        Start making file name
         */

        PersianDate pCalendar = new PersianDate();
        String month, day;


        if (pCalendar.getShMonth() + 1 < 10)
            month = "0" + String.valueOf(pCalendar.getShMonth() + 1);
        else
            month = String.valueOf(pCalendar.getShMonth() + 1);


        if (pCalendar.getShDay() < 10)
            day = "0" + String.valueOf(pCalendar.getShDay());
        else
            day = String.valueOf(pCalendar.getShDay());


        String fileName = "dakhlokharj_db_" + String.valueOf(pCalendar.getShYear()).substring(2) + month + day + ".db";

        /*
        End making file name
         */

        File res = new File(destinationPath.getAbsolutePath(), fileName);

        if (oldDbPath.exists()) {

            InputStream in = new FileInputStream(oldDbPath);
            OutputStream out = new FileOutputStream(res);

            byte[] buf = new byte[1024];
            int len;

            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();

        }
        zip(res.getPath(), fileNameWithoutExtension(fileName), ".dak", backupType);

        res.delete(); // remove after zip

    }

    public static boolean recoveryDataFromStorage(File targetDB) {

        Backup.unZip(fileNameWithoutExtension(targetDB.getName()), Settings.getSetting(Settings.BACKUP_DESTINATION), Settings.getSetting(Settings.BACKUP_DESTINATION), ".dak", RecoveryType.FromStoarge);
        // Environment.getExternalStorageDirectory() + "/Document/DakhloKharj_backups"

        try {
            replaceDataBases(new File(Settings.getSetting(Settings.BACKUP_DESTINATION), fileNameWithoutExtension(targetDB.getName()) + ".db"), RecoveryType.FromStoarge);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    public static boolean recoveryDataFromShare(File sharedDB) {

        Log.i("fileName", String.valueOf(sharedDB.getName()));

        File dbPath = new File(DBManager.database.getPath()).getParentFile();

        File tempDb = new File(dbPath, "dakhlokharj.dak");
//        if (!tempDb.exists())
//            tempDb.mkdir();

        copy(sharedDB, tempDb);

        File tempPath = new File(Settings.getSetting(Settings.BACKUP_DESTINATION) + "/Temp");

        if (!tempPath.exists())
            tempPath.mkdirs();

        File temp = new File(tempPath.getPath(), "dakhlokharj.db");

        unZip(tempDb.getName(), tempDb.getPath(), temp.getPath(), "", RecoveryType.FromShare);

        try {
            replaceDataBases(temp, RecoveryType.FromShare);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static void backup(Context context, BackupType backupType) {

        // create
        try {
            //  for (int i = 0; i < 4; i++) {
            createBackupFile(backupType);
            //   }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //share
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        String parentPath = Settings.getSetting(Settings.BACKUP_DESTINATION);
        if (backupType == BackupType.Temp)
            parentPath = parentPath + "/Temp";

        File dbFile = new File(parentPath, Settings.getSetting(Settings.LAST_BACKUP_FILE_NAME) + ".dak");
        Uri dbUri = Uri.parse("file://" + dbFile.getPath());

        sharingIntent.setType("application/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, dbUri);
        context.startActivity(Intent.createChooser(sharingIntent, "Share BackUp using"));

    }

    private static boolean replaceDataBases(File backUpDb, RecoveryType recoveryType) throws IOException {

        //  File backUpDb = new File(Settings.getSetting(Settings.BACKUP_DESTINATION.value()), Settings.getSetting(Settings.LAST_BACKUP_FILE_NAME.value()) + ".db");

        File dbPath = new File(DBManager.database.getPath(), "dakhlokharj.db").getParentFile();

        String destination = null;
        if (recoveryType == RecoveryType.FromStoarge) {

            backUpDb.renameTo(new File(Settings.getSetting(Settings.BACKUP_DESTINATION), "dakhlokharj.db"));

            destination = backUpDb.getParent();

        } else if (recoveryType == RecoveryType.FromShare) {

            backUpDb.renameTo(new File(Settings.getSetting(Settings.BACKUP_DESTINATION) + "/Temp", "dakhlokharj.db"));

            destination = backUpDb.getParent();

        }

        if (copy(new File(destination, "dakhlokharj.db"), dbPath))
            new File(Settings.getSetting(Settings.BACKUP_DESTINATION), "dakhlokharj.db").delete();

        return true;
    }

    private static void zip(String directoryString, String zipFileName, String extension, BackupType backupType) throws IOException {

        BufferedInputStream origin;

        String counter = Settings.getSetting(Settings.BACKUP_COUNTER);

        String destination = null;
        if (backupType == BackupType.Temp)
            destination = Settings.getSetting(Settings.BACKUP_DESTINATION) + "/Temp";
        else if (backupType == BackupType.Storage)
            destination = Settings.getSetting(Settings.BACKUP_DESTINATION);


        File zipFile;
        if (!new File(destination, zipFileName + extension).exists())
            zipFile = new File(destination, zipFileName + extension);
        else {
            zipFile = new File(destination, zipFileName + "_" + counter + extension);
            Settings.setSetting(Settings.BACKUP_COUNTER, String.valueOf(Integer.valueOf(counter) + 1));
        }

        Settings.setSetting(Settings.LAST_BACKUP_FILE_NAME, fileNameWithoutExtension(zipFile.getName()));

        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
        try {
            byte data[] = new byte[1024];

            FileInputStream fi = new FileInputStream(directoryString);
            origin = new BufferedInputStream(fi, 1024);
            try {
                ZipEntry entry = new ZipEntry(directoryString.substring(directoryString.lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, 1024)) != -1) {
                    out.write(data, 0, count);
                }
            } finally {
                origin.close();
            }

        } finally {
            out.close();
        }
    }

    private static boolean unZip(String zipFile, String source, String destination, String extension, RecoveryType recoveryType) {
        try {
            zipFile = zipFile + extension;
            File inputFile = null;
            if (recoveryType.value() == RecoveryType.FromShare.value())
                inputFile = new File(source);
            else if (recoveryType.value() == RecoveryType.FromStoarge.value())
                inputFile = new File(source, zipFile);

            FileInputStream inputStream = new FileInputStream(inputFile);
            ZipInputStream zipStream = new ZipInputStream(inputStream);
            ZipEntry zEntry;
            while ((zEntry = zipStream.getNextEntry()) != null) {
                Log.d("Unzip", "Unzipping " + zEntry.getName() + " at "
                        + destination);

                if (zEntry.isDirectory()) {
                    handleDirectory(zEntry.getName(), destination);
                } else {
                    zipFile = fileNameWithoutExtension(zipFile) + ".db";
                    FileOutputStream fout = null;

                    if (recoveryType.value() == RecoveryType.FromShare.value()) {
                        fout = new FileOutputStream(destination);
                    } else if (recoveryType.value() == RecoveryType.FromStoarge.value()) {
                        Settings.setSetting(Settings.LAST_BACKUP_FILE_NAME, fileNameWithoutExtension(zipFile));
                        fout = new FileOutputStream(destination + "/" + zipFile);
                    }

                    BufferedOutputStream bufout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[1024];
                    int read;
                    while ((read = zipStream.read(buffer)) != -1) {
                        bufout.write(buffer, 0, read);
                    }

                    zipStream.closeEntry();
                    bufout.close();
                    fout.close();
                }
            }
            //inputFile.delete();  // file ro bad az unzip kardan pak nemikone
            zipStream.close();
            Log.d("Unzip", "Unzipping onComplete. path :  " + destination);
        } catch (Exception e) {
            Log.d("Unzip", "Unzipping failed");
            e.printStackTrace();
            return false;
        }

        return true;

    }

    private static void handleDirectory(String dir, String destination) {
        File f = new File(destination + dir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }

    private static boolean copy(File src, File dst) {

        InputStream in = null;
        OutputStream out = null;

        try {
            in = new FileInputStream(src);
            try {
                out = new FileOutputStream(dst);
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                if (in != null)
                    out.close();
                if (out != null)
                    in.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static String fileNameWithoutExtension(String str) {

        for (int i = 0; i < str.length(); i++) {

            if (str.charAt(i) == '.')
                return str.substring(0, i);

        }
        return "";
    }

}