package model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import db.DBManager;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import utils.AppModules;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Transaction extends TX_Entity implements Parcelable, Cloneable {

    public int lstId;
    public String lsdName;
    public String lsdIcon;
    public String lsdColor;
    public String lstName;
    public String lstIcon;
    public String lstColor;
    public int lstOrder;

    public Transaction() {

    }

    public Transaction(String price, String date, int transaction) {
        setTrnAmount(price);
        setTrnDate(date);
        setTrnType(transaction);
    }

    protected Transaction(Parcel in) {
        lstId = in.readInt();
        lsdName = in.readString();
        lsdIcon = in.readString();
        lsdColor = in.readString();
        lstName = in.readString();
        lstIcon = in.readString();
        lstColor = in.readString();
        lstOrder = in.readInt();
        dbColArrayList = in.createTypedArrayList(DBCol.CREATOR);
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(lstId);
        dest.writeString(lsdName);
        dest.writeString(lsdIcon);
        dest.writeString(lsdColor);
        dest.writeString(lstName);
        dest.writeString(lstIcon);
        dest.writeString(lstColor);
        dest.writeInt(lstOrder);
        dest.writeTypedList(dbColArrayList);
    }

    public enum TransactionCol {

        id("ID"),
        lsdID("lsdID"),
        trnAmount("trnAmount"),
        trnDate("trnDate"),
        pyeID("pyeID"),
        acnID("acnID"),
        trnDescription("trnDescription"),
        trnImagePath("trnImagePath"),
        trnColor("trnColor"),
        trnType("trnType"),
        trnReference("trnReference");

        private final String name;

        TransactionCol(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }

    }

    @Override
    public Transaction clone() {
        Transaction ts = new Transaction();
        ts.setID(this.getID());
        ts.setIsdID(this.getIsdID());
        ts.setTrnAmount(this.getTrnAmount());
        ts.setTrnDate(this.getTrnDate());
        ts.setPyeId(this.getPyeId());
        ts.setAcnId(this.getAcnId());
        ts.setTrnDescription(this.getTrnDescription());
        ts.setTrnImagePath(this.getTrnImagePath());
        ts.setTrnColor(this.getTrnColor());
        ts.setTrnType(this.getTrnType());
        ts.setTrnReference(this.getTrnReference());
        return ts;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Transaction)) {
            return super.equals(obj);
        } else {
            Transaction temp = (Transaction) obj;
            return temp.getID() == this.getID() &&
                    temp.getIsdID() == this.getIsdID() &&
                    temp.getTrnAmount().equals(this.getTrnAmount()) &&
                    temp.getTrnDate().equals(this.getTrnDate()) &&
                    temp.getPyeId() == this.getPyeId() &&
                    temp.getAcnId() == this.getAcnId() &&
                    temp.getTrnDescription().equals(this.getTrnDescription()) &&
                    temp.getTrnImagePath().equals(this.getTrnImagePath()) &&
                    temp.getTrnColor().equals(this.getTrnColor()) &&
                    temp.getTrnType() == this.getTrnType() &&
                    temp.getTrnReference() == this.getTrnReference();
        }
    }


    private static final String childTableName = "T_Transaction";
    private ArrayList<DBCol> dbColArrayList = new ArrayList<>(
            Arrays.asList(new DBCol(null, TransactionCol.id.value(), EnumsAndConstants.Types.INTEGER, 10, false, false, true),
                    new DBCol(null, TransactionCol.lsdID.value(), EnumsAndConstants.Types.INTEGER, 20, true, true, false),
                    new DBCol(0, TransactionCol.trnAmount.value(), EnumsAndConstants.Types.DOUBLE, 30, true, true, false),
                    new DBCol(null, TransactionCol.trnDate.value(), EnumsAndConstants.Types.STRING, 40, true, true, false),
                    new DBCol(null, TransactionCol.pyeID.value(), EnumsAndConstants.Types.INTEGER, 50, true, true, false),
                    new DBCol(null, TransactionCol.acnID.value(), EnumsAndConstants.Types.INTEGER, 60, true, true, false),
                    new DBCol(null, TransactionCol.trnDescription.value(), EnumsAndConstants.Types.STRING, 70, true, true, false),
                    new DBCol(null, TransactionCol.trnImagePath.value(), EnumsAndConstants.Types.STRING, 80, true, true, false),
                    new DBCol(null, TransactionCol.trnColor.value(), EnumsAndConstants.Types.STRING, 90, true, true, false),
                    new DBCol(null, TransactionCol.trnType.value(), EnumsAndConstants.Types.INTEGER, 100, true, true, false),
                    new DBCol(null, TransactionCol.trnReference.value(), EnumsAndConstants.Types.INTEGER, 110, true, true, false)
            )
    );

    public Transaction(int ID, int lsdID, double trnAmount, String trnDate, int pyeId, int acnId, String trnDescription, String trnImagePath, String trnColor, int trnType) {
        setID(ID);
        setIsdID(lsdID);
        setTrnAmount(String.valueOf(trnAmount));
        setTrnDate(trnDate);
        setPyeId(pyeId);
        setAcnId(acnId);
        setTrnDescription(trnDescription);
        setTrnImagePath(trnImagePath);
        setTrnColor(trnColor);
        setTrnType(trnType);
    }

    public Transaction(int id) {
        setID(id);
    }

    public Transaction(int ID, int lsdID, double trnAmount, String trnDate, int pyeId, int acnId, String trnDescription, String trnImagePath, String trnColor, int trnType, int trnReference) {
        setID(ID);
        setIsdID(lsdID);
        setTrnAmount(String.valueOf(trnAmount));
        setTrnDate(trnDate);
        setPyeId(pyeId);
        setAcnId(acnId);
        setTrnDescription(trnDescription);
        setTrnImagePath(trnImagePath);
        setTrnColor(trnColor);
        setTrnType(trnType);
        setTrnReference(trnReference);
    }

    public static void appPurchased() {

        if (!Settings.getSetting(Settings.APP_AMOUNT).equals("")) { // yani ghablan kharide bude va alan faal shode va transaction E nadashte
            double price = Double.parseDouble(Settings.getSetting(Settings.APP_AMOUNT));
            if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان")) {
                price = price / 10;
            }
            Transaction transaction = new Transaction(0, 60, price, String.valueOf(new PersianDate().getTime()), 0, 0, "", "", "#B8B8B8", 1);
            transaction.insert();
        }
    }

    @Override
    String childTableName() {
        return childTableName;
    }

    @Override
    ArrayList<DBCol> childDBColArrayList() {
        return dbColArrayList;
    }

    public int getID() {
        return super.GetInt("ID");
    }

    public void setID(int ID) {
        super.Set("ID", ID);
    }

    public int getIsdID() {
        return super.GetInt("lsdID");
    }

    public void setIsdID(int isdID) {
        super.Set("lsdID", isdID);
    }

    public String getTrnAmount() {
        double resD = super.GetDouble("trnAmount");
        resD = resD == -1 ? 0 : resD;
        String resStr = PublicModules.toEnglishDigit(String.valueOf(resD));
        resStr = Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("0") || Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("1") ?
                String.format(Locale.US,"%.0f", Double.valueOf(resStr)) :
                String.format(Locale.US,"%.2f", Double.valueOf(resStr));
        return resStr.equals("-1") ? "0" : resStr;
    }

    public void setTrnAmount(String trnAmount) {
        trnAmount = String.format(Locale.US,"%.2f", Double.valueOf(PublicModules.toEnglishDigit(trnAmount)));
        trnAmount = PublicModules.toEnglishDigit(trnAmount);
        super.Set("trnAmount", Double.valueOf(trnAmount));
    }

    public String getTrnDate() {
        return super.GetString("trnDate") == null ? "0" : super.GetString("trnDate");
    }

    public void setTrnDate(String trnDate) {
        super.Set("trnDate", trnDate);
    }

    public int getPyeId() {
        return super.GetInt("pyeID");
    }

    public void setPyeId(int pyeId) {
        super.Set("pyeID", pyeId);
    }

    public int getAcnId() {
        return super.GetInt("acnID");
    }

    public void setAcnId(int acnId) {
        super.Set("acnID", acnId);
    }

    public String getTrnDescription() {
        return super.GetString("trnDescription") == null ? "" : super.GetString("trnDescription");
    }

    public void setTrnDescription(String trnDescription) {
        super.Set("trnDescription", trnDescription);
    }

    public String getTrnImagePath() {
        if (super.GetString("trnImagePath") != null) {
            if (new File(super.GetString("trnImagePath")).exists()) {
                return super.GetString("trnImagePath");
            }
        }
        return "";
    }

    public void setTrnImagePath(String trnImagePath) {
        super.Set("trnImagePath", trnImagePath);
    }

    public String getTrnColor() {
        return super.GetString("trnColor");
    }

    public void setTrnColor(String trnColor) {
        super.Set("trnColor", trnColor);
    }

    public int getTrnType() {
        return super.GetInt("trnType");
    }

    public void setTrnType(int trnType) {
        super.Set("trnType", trnType);
    }

    public int getTrnReference() {
        return super.GetInt("trnReference");
    }

    public void setTrnReference(int trnReference) {
        super.Set("trnReference", trnReference);
    }

    public static Transaction getTransaction(int id) {
        Transaction transaction = new Transaction();

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery("SELECT * FROM T_Transaction WHERE ID =" + id, null);

        if (resCursor.getCount() > 0) {
            resCursor.moveToFirst();

            transaction.setID(resCursor.getInt(resCursor.getColumnIndex(TransactionCol.id.value())));
            transaction.setIsdID(resCursor.getInt(resCursor.getColumnIndex(TransactionCol.lsdID.value())));
            transaction.setTrnAmount(String.valueOf(resCursor.getDouble(resCursor.getColumnIndex(TransactionCol.trnAmount.value()))));
            transaction.setTrnDate(resCursor.getString(resCursor.getColumnIndex(TransactionCol.trnDate.value())));
            transaction.setPyeId(resCursor.getInt(resCursor.getColumnIndex(TransactionCol.pyeID.value())));
            transaction.setAcnId(resCursor.getInt(resCursor.getColumnIndex(TransactionCol.acnID.value())));
            transaction.setTrnDescription(resCursor.getString(resCursor.getColumnIndex(TransactionCol.trnDescription.value())));
            transaction.setTrnImagePath(resCursor.getString(resCursor.getColumnIndex(TransactionCol.trnImagePath.value())));
            transaction.setTrnColor(resCursor.getString(resCursor.getColumnIndex(TransactionCol.trnColor.value())));
            transaction.setTrnType(resCursor.getInt(resCursor.getColumnIndex(TransactionCol.trnType.value())));
            transaction.setTrnType(resCursor.getInt(resCursor.getColumnIndex(TransactionCol.trnReference.value())));
        }

        resCursor.close();
        DBManager.close();

        return transaction;
    }

    public static ArrayList<HashMap<String, Object>> getAll() {

        ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();

        String query = "SELECT A.*,B.lstID,B.lsdName,B.lsdIcon,B.lsdColor," +
                "C.lstName,C.lstIcon,C.lstColor,C.lstOrder,C.lstType," +
                "ifnull(D.pyeName,'') pyeName, ifnull(E.acnName,'') acnName" +
                ", ifnull(F.bnkTitle,'') bnkTitle FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID" +
                " INNER JOIN T_List C ON B.lstID=C.ID " +
                "LEFT JOIN T_PAYEE D ON A.pyeID=D.ID " +
                "LEFT JOIN T_ACCOUNT E ON A.acnID=E.ID  " +
                "LEFT JOIN T_BANK F ON E.acnIcon = F.ID WHERE trnType = 1 OR trnType = 2 ORDER BY trnDate DESC";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {
            HashMap<String, Object> dataHashMap = new HashMap<>();

            // A transaction data
            dataHashMap.put(TransactionCol.id.value(), resCursor.getInt(0));
            dataHashMap.put(TransactionCol.lsdID.value(), resCursor.getInt(1));
            dataHashMap.put(TransactionCol.trnAmount.value(), resCursor.getDouble(2));
            dataHashMap.put(TransactionCol.trnDate.value(), resCursor.getString(3));
            dataHashMap.put(TransactionCol.pyeID.value(), resCursor.getInt(4));
            dataHashMap.put(TransactionCol.acnID.value(), resCursor.getInt(5));
            dataHashMap.put(TransactionCol.trnDescription.value(), resCursor.getString(6));
            dataHashMap.put(TransactionCol.trnImagePath.value(), resCursor.getString(7));
            dataHashMap.put(TransactionCol.trnColor.value(), resCursor.getString(8));
            dataHashMap.put(TransactionCol.trnType.value(), resCursor.getInt(9));
            dataHashMap.put(TransactionCol.trnReference.value(), resCursor.getInt(11));

            // B listDetail data
            dataHashMap.put(GroupDetail.GroupDetailCol.lstID.value(), resCursor.getInt(12));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(13));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(14));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(15));

            // C list data
            dataHashMap.put(Group.GroupCol.lstName.value(), resCursor.getString(16));
            dataHashMap.put(Group.GroupCol.lstIcon.value(), resCursor.getString(17));
            dataHashMap.put(Group.GroupCol.lstColor.value(), resCursor.getString(18));
            dataHashMap.put(Group.GroupCol.lstOrder.value(), resCursor.getString(19));
            dataHashMap.put(Group.GroupCol.lstType.value(), resCursor.getString(20));

            // D payee data
            dataHashMap.put(Payee.PayeeCol.pyeName.value(), resCursor.getString(21));

            // E account date
            dataHashMap.put(Account.AccountCol.acnName.value(), resCursor.getString(22));

            // F bank data
            dataHashMap.put(Bank.BankCol.bnkTitle.value(), resCursor.getString(23));

            resArray.add(dataHashMap);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return resArray;
    }

    public static ArrayList<HashMap<String, Object>> getAll(EnumsAndConstants.TransactionTypes type) {

        ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();

        String query = "SELECT A.*" +
                ",B.lstID,B.lsdName,B.lsdIcon,B.lsdColor " +
                ",C.ID,C.lstName,C.lstIcon,C.lstColor,C.lstOrder " +
                "FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID " +
                "INNER JOIN T_List C ON B.lstID=C.ID AND trnType=" + type.value() + " ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {

            HashMap<String, Object> dataHashMap = new HashMap<>();

            // A transaction data
            dataHashMap.put(TransactionCol.id.value(), resCursor.getInt(0));
            dataHashMap.put(TransactionCol.lsdID.value(), resCursor.getInt(1));
            dataHashMap.put(TransactionCol.trnAmount.value(), resCursor.getDouble(2));
            dataHashMap.put(TransactionCol.trnDate.value(), resCursor.getString(3));
            dataHashMap.put(TransactionCol.pyeID.value(), resCursor.getInt(4));
            dataHashMap.put(TransactionCol.acnID.value(), resCursor.getInt(5));
            dataHashMap.put(TransactionCol.trnDescription.value(), resCursor.getString(6));
            dataHashMap.put(TransactionCol.trnImagePath.value(), resCursor.getString(7));
            dataHashMap.put(TransactionCol.trnColor.value(), resCursor.getString(8));
            dataHashMap.put(TransactionCol.trnType.value(), resCursor.getInt(9));
            dataHashMap.put(TransactionCol.trnReference.value(), resCursor.getInt(11));

            // B listDetail data
            dataHashMap.put(GroupDetail.GroupDetailCol.lstID.value(), resCursor.getInt(12));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(13));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(14));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(15));

            // C list data
            dataHashMap.put(Group.GroupCol.id.value(), resCursor.getString(16));
            dataHashMap.put(Group.GroupCol.lstName.value(), resCursor.getString(17));
            dataHashMap.put(Group.GroupCol.lstIcon.value(), resCursor.getString(18));
            dataHashMap.put(Group.GroupCol.lstColor.value(), resCursor.getString(19));
            dataHashMap.put(Group.GroupCol.lstOrder.value(), resCursor.getString(20));

            resArray.add(dataHashMap);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return resArray;
    }

    public static ArrayList<HashMap<String, Object>> getAll(String from, String to, EnumsAndConstants.TransactionTypes type) {

        ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();

        String query = "SELECT A.*,B.lstID,B.lsdName,B.lsdIcon,B.lsdColor,C.lstName,C.lstIcon,C.lstColor,C.lstOrder FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID INNER JOIN T_List C ON B.lstID=C.ID WHERE trnDate>= " + from + " AND trnDate < " + to + " AND trnType=" + type.value() + " ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {

            HashMap<String, Object> dataHashMap = new HashMap<>();

            // A transaction data
            dataHashMap.put(TransactionCol.id.value(), resCursor.getInt(0));
            dataHashMap.put(TransactionCol.lsdID.value(), resCursor.getInt(1));
            dataHashMap.put(TransactionCol.trnAmount.value(), resCursor.getDouble(2));
            dataHashMap.put(TransactionCol.trnDate.value(), resCursor.getString(3));
            dataHashMap.put(TransactionCol.pyeID.value(), resCursor.getInt(4));
            dataHashMap.put(TransactionCol.acnID.value(), resCursor.getInt(5));
            dataHashMap.put(TransactionCol.trnDescription.value(), resCursor.getString(6));
            dataHashMap.put(TransactionCol.trnImagePath.value(), resCursor.getString(7));
            dataHashMap.put(TransactionCol.trnColor.value(), resCursor.getString(8));
            dataHashMap.put(TransactionCol.trnType.value(), resCursor.getInt(9));
            dataHashMap.put(TransactionCol.trnReference.value(), resCursor.getInt(11));

            // B listDetail data
            dataHashMap.put(GroupDetail.GroupDetailCol.lstID.value(), resCursor.getInt(12));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(13));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(14));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(15));

            // C list data
            dataHashMap.put(Group.GroupCol.lstName.value(), resCursor.getString(16));
            dataHashMap.put(Group.GroupCol.lstIcon.value(), resCursor.getString(17));
            dataHashMap.put(Group.GroupCol.lstColor.value(), resCursor.getString(18));
            dataHashMap.put(Group.GroupCol.lstOrder.value(), resCursor.getString(19));

            resArray.add(dataHashMap);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return resArray;
    }

    public static ArrayList<HashMap<String, Object>> getAll(String from, String to) {

        ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();

        String query = "SELECT A.*,B.lstID,B.lsdName,B.lsdIcon,B.lsdColor,C.lstName,C.lstIcon,C.lstColor,C.lstOrder FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID INNER JOIN T_List C ON B.lstID=C.ID WHERE trnDate>= " + from + " AND trnDate <= " + to + " AND trnType = 1 OR trnType = 2 ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {

            HashMap<String, Object> dataHashMap = new HashMap<>();

            // A transaction data
            dataHashMap.put(TransactionCol.id.value(), resCursor.getInt(0));
            dataHashMap.put(TransactionCol.lsdID.value(), resCursor.getInt(1));
            dataHashMap.put(TransactionCol.trnAmount.value(), resCursor.getDouble(2));
            dataHashMap.put(TransactionCol.trnDate.value(), resCursor.getString(3));
            dataHashMap.put(TransactionCol.pyeID.value(), resCursor.getInt(4));
            dataHashMap.put(TransactionCol.acnID.value(), resCursor.getInt(5));
            dataHashMap.put(TransactionCol.trnDescription.value(), resCursor.getString(6));
            dataHashMap.put(TransactionCol.trnImagePath.value(), resCursor.getString(7));
            dataHashMap.put(TransactionCol.trnColor.value(), resCursor.getString(8));
            dataHashMap.put(TransactionCol.trnType.value(), resCursor.getInt(9));
            dataHashMap.put(TransactionCol.trnReference.value(), resCursor.getInt(11));

            // B listDetail data
            dataHashMap.put(GroupDetail.GroupDetailCol.lstID.value(), resCursor.getInt(12));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(13));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(14));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(15));

            // C list data
            dataHashMap.put(Group.GroupCol.lstName.value(), resCursor.getString(16));
            dataHashMap.put(Group.GroupCol.lstIcon.value(), resCursor.getString(17));
            dataHashMap.put(Group.GroupCol.lstColor.value(), resCursor.getString(18));
            dataHashMap.put(Group.GroupCol.lstOrder.value(), resCursor.getString(19));

            resArray.add(dataHashMap);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return resArray;
    }

    public static String getSumAll(String from, String to, EnumsAndConstants.TransactionTypes type) {
        String res = "0";
        String query = "SELECT IFNULL(SUM(trnAmount),0) FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID INNER JOIN T_List C ON B.lstID=C.ID WHERE trnDate>= " + from + " AND trnDate <= " + to + " AND trnType=" + type.value() + " ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        if (resCursor.getCount() > 0) {
            res = String.valueOf(resCursor.getDouble(0));
        }

        resCursor.close();
        DBManager.close();

        return res;
    }

    public static String getSumThisMonth(int listID, EnumsAndConstants.TransactionTypes type) {

        String res = "0";

        String startOfMonth = String.valueOf(PublicModules.getMonthInterval(0)[0]);
        String endOfMonth = String.valueOf(PublicModules.getMonthInterval(0)[1]);

        String query = "SELECT IFNULL(SUM(trnAmount),0) FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID INNER JOIN T_List C ON B.lstID=C.ID WHERE trnDate>= " + startOfMonth + " AND trnDate <= " + endOfMonth + " AND trnType=" + type.value() + " AND C.ID = " + String.valueOf(listID) + " ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();

        if (resCursor.getCount() > 0)
            res = resCursor.getString(0);

        resCursor.close();
        DBManager.close();

        return res;
    }

    public static String getCountThisMonth(EnumsAndConstants.TransactionTypes type) {

        String res = "0";

        String startOfMonth = String.valueOf(PublicModules.getMonthInterval(0)[0]);
        String endOfMonth = String.valueOf(PublicModules.getMonthInterval(0)[1]);

        String query = "SELECT COUNT(*) FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID INNER JOIN T_List C ON B.lstID=C.ID WHERE trnDate>= " + startOfMonth + " AND trnDate <= " + endOfMonth + " AND trnType=" + type.value() + " ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();

        if (resCursor.getCount() > 0)
            res = resCursor.getString(0);

        resCursor.close();
        DBManager.close();

        return res;
    }

    public static String getSumThisMonth(EnumsAndConstants.TransactionTypes type) {

        String res = "0";

        String startOfMonth = String.valueOf(PublicModules.getMonthInterval(0)[0]);
        String endOfMonth = String.valueOf(PublicModules.getMonthInterval(0)[1]);

        String query = "SELECT SUM(trnAmount) FROM T_Transaction A INNER JOIN T_List_Detail B ON A.lsdID = B.ID INNER JOIN T_List C ON B.lstID=C.ID WHERE trnDate>= " + startOfMonth + " AND trnDate <= " + endOfMonth + " AND trnType=" + type.value() + " ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        if (resCursor.getCount() > 0) {
            resCursor.moveToFirst();
            res = String.valueOf(resCursor.getDouble(0));
        }

        resCursor.close();
        DBManager.close();

        return res;
    }

    public static String getCountAll() {

        String res = "0";

        String query = "SELECT COUNT(*) FROM T_Transaction WHERE trnType = 1 OR trnType = 2";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();

        if (resCursor.getCount() > 0)
            res = resCursor.getString(0);

        resCursor.close();
        DBManager.close();

        return res;
    }

    public static ArrayList<HashMap<String, Object>> getTop3() {

        ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();

        String query = "SELECT lsdID,COUNT(*) FROM T_Transaction WHERE trnType = 1 OR trnType = 2 GROUP BY lsdID HAVING COUNT(*) > 0 ORDER BY COUNT(*) DESC LIMIT 3";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {
            HashMap<String, Object> dataHashMap = new HashMap<>();

            dataHashMap.put("lsdID", resCursor.getInt(0));
            dataHashMap.put("COUNT", resCursor.getInt(1));

            resArray.add(dataHashMap);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return resArray;
    }

    public static ArrayList<HashMap<String, Object>> getTop3Detail() {

        ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();

        String query = "SELECT A.*,B.* FROM T_LIST_DETAIL A INNER JOIN (SELECT lsdID,trnType,COUNT(*) '_COUNT' FROM T_Transaction WHERE trnType = 1 OR trnType = 2 GROUP BY lsdID HAVING COUNT(*) > 0 ORDER BY COUNT(*) DESC LIMIT 3) B ON A.ID = B.LSDID";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToNext();
        while (!resCursor.isAfterLast()) {

            HashMap<String, Object> dataHashMap = new HashMap<>();

            dataHashMap.put(GroupDetail.GroupDetailCol.id.value(), resCursor.getInt(0));
            dataHashMap.put(GroupDetail.GroupDetailCol.lstID.value(), resCursor.getInt(1));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(2));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(3));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(4));

            dataHashMap.put(TransactionCol.lsdID.value(), resCursor.getInt(5));
            dataHashMap.put(TransactionCol.trnType.value(), resCursor.getInt(6));
            dataHashMap.put("_COUNT", resCursor.getInt(7));

            resArray.add(dataHashMap);
            resCursor.moveToNext();
        }
        resCursor.close();
        DBManager.close();

        return resArray;
    }

    public static long[] getFirstLastTransaction() {

        long[] resArray = new long[2];

        String minQuery = "SELECT IFNULL(MIN(trnDate),0) FROM T_Transaction WHERE trnType = 1 OR trnType = 2";
        String maxQuery = "SELECT IFNULL(MAX(trnDate),0) FROM T_Transaction WHERE trnType = 1 OR trnType = 2";

        DBManager.openForRead();

        Cursor minCursor = DBManager.database.rawQuery(minQuery, null);
        minCursor.moveToFirst();
        resArray[0] = minCursor.getLong(0);
        minCursor.close();

        Cursor maxCursor = DBManager.database.rawQuery(maxQuery, null);
        maxCursor.moveToFirst();
        resArray[1] = maxCursor.getLong(0);
        maxCursor.close();

        DBManager.close();

        return resArray;
    }

    public static ArrayList<HashMap<String, Object>> getLastTransaction() {

        ArrayList<HashMap<String, Object>> resArray = new ArrayList<>();

        String query = "SELECT A.*,B.lsdName,B.lsdIcon,B.lsdColor FROM T_Transaction AS A INNER JOIN T_List_Detail AS B ON A.lsdID = B.ID WHERE A.trnDate = (SELECT IFNULL(MAX(trnDate),0) FROM T_Transaction)";

        DBManager.openForRead();

        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {

            HashMap<String, Object> dataHashMap = new HashMap<>();
            // Transaction data
            dataHashMap.put(TransactionCol.id.value(), resCursor.getInt(0));
            dataHashMap.put(TransactionCol.lsdID.value(), resCursor.getInt(1));
            dataHashMap.put(TransactionCol.trnAmount.value(), resCursor.getDouble(2));
            dataHashMap.put(TransactionCol.trnDate.value(), resCursor.getString(3));
            dataHashMap.put(TransactionCol.pyeID.value(), resCursor.getInt(4));
            dataHashMap.put(TransactionCol.acnID.value(), resCursor.getInt(5));
            dataHashMap.put(TransactionCol.trnDescription.value(), resCursor.getString(6));
            dataHashMap.put(TransactionCol.trnImagePath.value(), resCursor.getString(7));
            dataHashMap.put(TransactionCol.trnColor.value(), resCursor.getString(8));
            dataHashMap.put(TransactionCol.trnType.value(), resCursor.getInt(9));
            dataHashMap.put(TransactionCol.trnReference.value(), resCursor.getInt(11));

            // GroupDetail data
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdName.value(), resCursor.getString(12));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdIcon.value(), resCursor.getString(13));
            dataHashMap.put(GroupDetail.GroupDetailCol.lsdColor.value(), resCursor.getString(14));

            resArray.add(dataHashMap);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return resArray;
    }

    public static int deleteUpTo(String to) {

        int resInt;

        String resQuery = "SELECT COUNT(*) FROM T_TRANSACTION WHERE trnDate < " + to;
        String deleteQuery = "DELETE FROM T_TRANSACTION WHERE trnDate <" + to;

        DBManager.openForRead();

        Cursor resCursor = DBManager.database.rawQuery(resQuery, null);
        resCursor.moveToFirst();
        resInt = resCursor.getInt(0);
        resCursor.close();

        DBManager.close();

        DBManager.openForWrite();
        DBManager.database.execSQL(deleteQuery);
        DBManager.close();

        return resInt;
    }

    public static int getCount(EnumsAndConstants.TransactionTypes type) {

        int resInt;

        String query = "SELECT COUNT(*) FROM T_Transaction WHERE trnType=" + type.value();

        DBManager.openForRead();

        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();

        resInt = resCursor.getInt(0);

        resCursor.close();
        DBManager.close();

        return resInt;
    }

    @Override
    public int insert() {
        int inserted_id = super.insert();
        setID(inserted_id);
        return inserted_id;
    }

    @Override
    public void delete() {

        if (getTrnType() == 3 || getTrnType() == 4) {
            DBManager.openForWrite();
            DBManager.database.execSQL("DELETE FROM " + childTableName + " WHERE ID =" + getTrnReference());
            DBManager.close();
            super.delete();
        } else {
            super.delete();
        }
    }

    //    public static void deleteAll() {
//
//        DBManager.openForWrite();
//        DBManager.database.execSQL("DELETE FROM T_Transaction");
//        DBManager.close();
//
//    }
//
//    public static void selectTest() {
//        DBManager.openForRead();
//        Cursor cursor = DBManager.database.rawQuery("SELECT * FROM T_Transaction", null);
//        cursor.close();
//        DBManager.close();
//
//    }

}