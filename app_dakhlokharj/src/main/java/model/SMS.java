package model;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.MainActivity;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import adapters.SMSAdapter;
import db.DBManager;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.DialogUtils;

/**
 * Created by Parsa on 2017-10-24.
 */

public class SMS {

    private static final String bankRegex = "((?<=\\بانک |بانك )[ء-ي ا-ی]+)|(?<=بانك )(.*)";
    private static final String trnTypeRegex = "(پايانه فروش|پایانه فروش|خريداينترنتی|خریداینترنتی|واريز|برداشت| -|واریز|خرید|انتقال|پرداخت قبض)";
    private static final String amountRegex = "\\d.*,[0-9]+";

    private String date;
    private String price;
    private int transactionType;

    public interface SMSDialogCallBack {
        void call(ArrayList<Transaction> transactions);
    }

    public SMS(String date, String price, int transactionType) {
        this.date = date;
        this.price = price;
        this.transactionType = transactionType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(EnumsAndConstants.TransactionTypes transactionType) {
        this.transactionType = transactionType.value();
    }

    /**
     * for first time
     *
     * @param context
     * @return
     */
    public static int getSmsCount(Context context) {
        Uri uri = Uri.parse("content://sms/inbox");
        Cursor inboxCursor = context.getContentResolver().query(uri, null, null, null, null);
        if (inboxCursor != null) {
            Settings.setSetting(Settings.LAST_SMS_COUNT, String.valueOf(inboxCursor.getCount()));
            return inboxCursor.getCount();
        } else
            return 0;
    }

    public static List<SMS> readInbox(Context context, boolean saveInStorage) {

        List<SMS> smsList = new ArrayList<>();
        try {
            Uri uri = Uri.parse("content://sms/inbox");
            Cursor inboxCursor = context.getContentResolver().query(uri, null, null, null, null);

            int lastSmsCount = Integer.parseInt(Settings.getSetting(Settings.LAST_SMS_COUNT));

            if (inboxCursor != null) {

                inboxCursor.moveToFirst();
                for (int i = lastSmsCount; i < inboxCursor.getCount(); i++) {

                    String body = PublicModules.toEnglishDigit(inboxCursor.getString(inboxCursor.getColumnIndexOrThrow("body")));
                    String phoneNumber = inboxCursor.getString(inboxCursor.getColumnIndexOrThrow("address"));
                    String date = inboxCursor.getString(inboxCursor.getColumnIndexOrThrow("date"));

                    Set<String> targetNumbers = Settings.getStringSet(Settings.SMS_NUMBERS);
                    phoneNumber = generatingPhoneNumber(phoneNumber);

                    for (String item : targetNumbers) {
                        if (phoneNumber.startsWith(item)) {

                            Pattern namePattern = Pattern.compile(bankRegex);
                            Matcher nameMatcher = namePattern.matcher(body);

                            Pattern trnTypePattern = Pattern.compile(trnTypeRegex);
                            Matcher trnTypeMatcher = trnTypePattern.matcher(body);

                            Pattern amountPattern = Pattern.compile(amountRegex);
                            Matcher amountMatcher = amountPattern.matcher(body); // digit hay farsi ro tashkhis nemide

                            if (trnTypeMatcher.find() && amountMatcher.find() && nameMatcher.find()) {
                                int trnType = 0;
                                if (trnTypeMatcher.group(0).equals("برداشت") ||
                                        trnTypeMatcher.group(0).equals("پرداخت قبض") ||
                                        trnTypeMatcher.group(0).equals("انتقال") ||
                                        trnTypeMatcher.group(0).equals("خرید") ||
                                        trnTypeMatcher.group(0).equals("خریداینترنتی") ||
                                        trnTypeMatcher.group(0).equals("خريداينترنتی") ||
                                        trnTypeMatcher.group(0).equals("پایانه فروش") ||
                                        trnTypeMatcher.group(0).equals("پايانه فروش")
                                        ) {
                                    trnType = 1;
                                } else if (trnTypeMatcher.group(0).equals("واریز") || trnTypeMatcher.group(0).equals("واريز")) {
                                    trnType = 2;
                                }
                                SMS sms = new SMS(date, amountMatcher.group(0), trnType);
                                smsList.add(sms);
                            }

                        }
                    }
                    inboxCursor.moveToNext();

                }
                if (saveInStorage) {
                    Settings.setSetting(Settings.LAST_SMS_COUNT, String.valueOf(inboxCursor.getCount()));
                }
                inboxCursor.close();
            }

            return smsList;
        } catch (SecurityException e) {
            PublicDialogs.makeMessageDialog(context, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "متاسفانه امکان دسترسی به پیامک های شما نیست!", "", EnumsAndConstants.APP_SOURCE).show();
            return smsList;
        }
    }

    public static void writeToDb(ArrayList<Transaction> transactionList) {
        DBManager.openForWrite();
        for (int i = 0; i < transactionList.size(); i++) {
            if (transactionList.get(i).getIsdID() != -1) {
                Transaction transaction = new Transaction(0, transactionList.get(i).getIsdID(), Double.valueOf(transactionList.get(i).getTrnAmount()), transactionList.get(i).getTrnDate(), transactionList.get(i).getPyeId(), transactionList.get(i).getAcnId(), transactionList.get(i).getTrnDescription(), transactionList.get(i).getTrnImagePath(), transactionList.get(i).getTrnColor(), transactionList.get(i).getTrnType());
                transaction.insert();
            }
        }
        DBManager.close();
    }

    private static String generatingPhoneNumber(String str) {
        if (!str.startsWith("+98")) {
            if (str.startsWith("0")) {
                str = "+98" + str.replaceFirst("0", "");
            } else {
                str = "+98".concat(str);
            }
        }
        return str;
    }

    public static Dialog smsSelectingDialog(final Context context, final SMSDialogCallBack callBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View smsLayout = inflater.inflate(R.layout.pop_sms_select, null);

        RecyclerView smsList = smsLayout.findViewById(R.id.pop_sms_select_recycler);
        TextView expensesAndIncomesCount = smsLayout.findViewById(R.id.pop_sms_select_expense_and_incomes_count);
        Button save, cancel;
        save = smsLayout.findViewById(R.id.pop_sms_select_save);
        cancel = smsLayout.findViewById(R.id.pop_sms_select_cancel);

        smsList.setLayoutManager(new LinearLayoutManager(context));
        smsList.setAdapter(new SMSAdapter(context));

        expensesAndIncomesCount.setText(String.valueOf(SMSAdapter.getInstance().incomesCount) + " واریز" + String.valueOf(SMSAdapter.getInstance().expensesCount) + " برداشت");

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 90;
                height = 75;
                break;

            case 2:
                width = 70;
                height = 60;
                break;

            case 3:
                width = 55;
                height = 55;
                break;

        }

        final Dialog smsDialog = PublicDialogs.makeDialog(context, smsLayout, width, height, true);

        save.setOnClickListener(new View.OnClickListener() {
            boolean everyThingIsOk; // :D
            //  String warningMessage = "شما تراکنش بدون دسته بندی دارید!\n اگر ادامه دهید این پیامک به عنوان تراکنش ذخیره نمی شود.\n می خواهید ادامه دهید ؟";

            @Override
            public void onClick(View v) {
                everyThingIsOk = true;
                for (int i = 0; i < SMSAdapter.getInstance().transactionList.size(); i++) {
                    if (SMSAdapter.getInstance().transactionList.get(i).getIsdID() == -1) {
                        everyThingIsOk = false;
                        break;
                    }
                }
                if (everyThingIsOk) {
                    callBack.call(SMSAdapter.getInstance().transactionList);
                    smsDialog.dismiss();
                } else
                    warningSMSDialog(context, smsDialog).show();
            }
        });

        cancel.setOnClickListener(v -> smsDialog.dismiss());

        return smsDialog;
    }

    private static Dialog warningSMSDialog(Context context, final Dialog parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View warningDialogLayout = inflater.inflate(R.layout.pop_sms_warning, null);

        TextView backTextView, continueTextView;
        backTextView = warningDialogLayout.findViewById(R.id.pop_sms_warning_back_text_view);
        continueTextView = warningDialogLayout.findViewById(R.id.pop_sms_warning_continue_text_view);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 27;
                break;

            case 2:
                width = 70;
                height = 20;
                break;

            case 3:
                width = 60;
                height = 17;
                break;

        }

        final Dialog warningDialog = PublicDialogs.makeDialog(context, warningDialogLayout, width, height, false);

        backTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });

        continueTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SMS.writeToDb(SMSAdapter.getInstance().transactionList);
                warningDialog.dismiss();
                parent.dismiss();
                MainActivity.getInstance().reloadMainData();
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
            }
        });

        return warningDialog;
    }
}
