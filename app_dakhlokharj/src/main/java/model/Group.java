package model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;

import db.DBManager;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Group extends TX_Entity implements Parcelable {

    protected Group(Parcel in) {
        children = in.createTypedArrayList(ExpenseOrIncomeItem.CREATOR);
    }

    private ArrayList<ExpenseOrIncomeItem> children = new ArrayList<>();

    public Group() {

    }

    public Group(String lstName) {
        setLstName(lstName);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(children);
    }

    public enum GroupCol {

        id("ID"),
        lstName("lstName"),
        lstIcon("lstIcon"),
        lstColor("lstColor"),
        lstOrder("lstOrder"),
        lstType("lstType");

        private final String name;


        GroupCol(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }

    }

    private static final String childTableName = "T_List";

    private ArrayList<DBCol> dbColArrayList = new ArrayList<>(
            Arrays.asList(new DBCol(null, GroupCol.id.value(), EnumsAndConstants.Types.INTEGER, 10, false, false, true),
                    new DBCol(null, GroupCol.lstName.value(), EnumsAndConstants.Types.STRING, 20, true, true, false),
                    new DBCol(null, GroupCol.lstIcon.value(), EnumsAndConstants.Types.STRING, 30, true, true, false),
                    new DBCol(null, GroupCol.lstColor.value(), EnumsAndConstants.Types.STRING, 40, true, true, false),
                    new DBCol(null, GroupCol.lstOrder.value(), EnumsAndConstants.Types.INTEGER, 50, true, true, false),
                    new DBCol(null, GroupCol.lstType.value(), EnumsAndConstants.Types.INTEGER, 60, true, true, false)
            )
    );

    @Override
    String childTableName() {
        return childTableName;
    }

    @Override
    ArrayList<DBCol> childDBColArrayList() {
        return dbColArrayList;
    }

    public Group(int ID, String lstName, String lstIcon, String lstColor, int lstOrder, int lstType) {
        setID(ID);
        setLstName(lstName);
        setLstIcon(lstIcon);
        setLstColor(lstColor);
        setLstOrder(lstOrder);
        setLstType(lstType);
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    public ArrayList<ExpenseOrIncomeItem> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<ExpenseOrIncomeItem> children) {
        this.children = children;
    }

    public int getID() {
        return super.GetInt("ID");
    }

    public void setID(int ID) {
        super.Set("ID", ID);
    }

    public String getLstName() {
        return super.GetString("lstName");
    }

    public void setLstName(String istName) {
        super.Set("lstName", istName);
    }

    public String getLstIcon() {
        return super.GetString("lstIcon");
    }

    public void setLstIcon(String istIcon) {
        super.Set("lstIcon", istIcon);
    }

    public String getLstColor() {
        return super.GetString("lstColor");
    }

    public void setLstColor(String istColor) {
        super.Set("lstColor", istColor);
    }

    public int getLstOrder() {
        return super.GetInt("lstOrder");
    }

    public void setLstOrder(int istOrder) {
        super.Set("lstOrder", istOrder);
    }

    public int getLstType() {
        return super.GetInt("lstType");
    }

    public void setLstType(int lstType) {
        super.Set("lstType", lstType);
    }

    public static ArrayList<Group> getAll(int type) {

        ArrayList<Group> lists = new ArrayList<>();

        String query = "SELECT * FROM T_List WHERE lstType=" + type + " ORDER BY lstOrder";

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        while (!resCursor.isAfterLast()) {

            Group group = new Group();

            group.setID(resCursor.getInt(0));
            group.setLstName(resCursor.getString(1));
            group.setLstIcon(resCursor.getString(2));
            group.setLstColor(resCursor.getString(3));
            group.setLstOrder(resCursor.getInt(4));
            group.setLstType(resCursor.getInt(5));

            lists.add(group);
            resCursor.moveToNext();
        }

        resCursor.close();
        DBManager.close();

        return lists;
    }

    public static String getGroupNameWithLsdID(int lsdID) {

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery("SELECT  A.ID , B.lstName FROM T_List_Detail A INNER JOIN T_List B ON A.lstID = B.ID where A.ID = " + lsdID, null);
        resCursor.moveToFirst();
        if (resCursor.getCount() > 0)
            return resCursor.getString(resCursor.getColumnIndex("lstName"));
        resCursor.close();
        DBManager.close();

        return "-";
    }

    @Override
    public int insert() {
        int inserted_id = super.insert();
        setID(inserted_id);
        Budget.budgetSync();
        return inserted_id;
    }
}
