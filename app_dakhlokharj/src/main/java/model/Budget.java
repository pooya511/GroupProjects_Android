package model;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import db.DBManager;
import modules.PublicModules;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Budget extends TX_Entity {

    public String lstName;
    public String lstColor;
    public String lstIcon;

    public Budget() {

    }

    public enum BudgetCol {

        id("ID"),
        lstID("lstID"),
        lsdID("lsdID"),
        budPeriod("budPeriod"),
        budAmount("budAmount"),
        budType("budType"),
        budIsActive("budIsActive");


        private final String name;

        BudgetCol(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }

    }


    private static final String childTableName = "T_Budget";
    private ArrayList<DBCol> dbColArrayList = new ArrayList<>(
            Arrays.asList(new DBCol(null, BudgetCol.id.value(), EnumsAndConstants.Types.INTEGER, 10, false, false, true),
                    new DBCol(null, BudgetCol.lstID.value(), EnumsAndConstants.Types.INTEGER, 20, true, true, false),
                    new DBCol(null, BudgetCol.lsdID.value(), EnumsAndConstants.Types.INTEGER, 30, true, true, false),
                    new DBCol(null, BudgetCol.budPeriod.value(), EnumsAndConstants.Types.STRING, 40, true, true, false),
                    new DBCol(null, BudgetCol.budAmount.value(), EnumsAndConstants.Types.DOUBLE, 50, true, true, false),
                    new DBCol(null, BudgetCol.budType.value(), EnumsAndConstants.Types.INTEGER, 60, true, true, false),
                    new DBCol(null, BudgetCol.budIsActive.value(), EnumsAndConstants.Types.INTEGER, 70, true, true, false)
            )
    );


    @Override
    String childTableName() {
        return childTableName;
    }

    @Override
    ArrayList<DBCol> childDBColArrayList() {
        return dbColArrayList;
    }

    public Budget(int ID, int lstID, int lsdID, String budPeriod, double budAmount, int budType, int budIsActive) {
        setID(ID);
        setIstID(lstID);
        setIsdID(lsdID);
        setBudPeriod(budPeriod);
        setBudAmount(String.valueOf(budAmount));
        setBudType(budType);
        setBudIsActive(budIsActive);
    }

    public int getID() {
        return super.GetInt("ID");
    }

    public void setID(int ID) {
        super.Set("ID", ID);
    }

    public int getIstID() {
        return super.GetInt("lstID");
    }

    public void setIstID(int istID) {
        super.Set("lstID", istID);
    }

    public int getIsdID() {
        return super.GetInt("lsdID");
    }

    public void setIsdID(int isdID) {
        super.Set("lsdID", isdID);
    }

    public String getBudPeriod() {
        return super.GetString("budPeriod");
    }

    public void setBudPeriod(String budPeriod) {
        super.Set("budPeriod", budPeriod);
    }

    public String getBudAmount() {
        String res = String.valueOf(super.GetDouble("budAmount"));
        res = Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("0") || Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("1") ?
                String.format(Locale.US,"%.0f", Double.valueOf(res)) :
                String.format(Locale.US,"%.2f", Double.valueOf(res));
        return res;
    }

    public void setBudAmount(String budAmount) {
        super.Set("budAmount", Double.valueOf(String.format(Locale.US,"%.2f", Double.valueOf(PublicModules.toEnglishDigit(budAmount)))));
    }

    public int getBudType() {
        return super.GetInt("budType");
    }

    public void setBudType(int budType) {
        super.Set("budType", budType);
    }

    public int getBudIsActive() {
        return super.GetInt("budIsActive");
    }

    public void setBudIsActive(int budIsActive) {
        super.Set("budIsActive", budIsActive);
    }

    public static void budgetSync() {
        DBManager.openForWrite();
        String insertQuery =
                "INSERT INTO T_BUDGET (" + BudgetCol.lstID + ")" +
                        "SELECT ID FROM T_LIST WHERE ID NOT IN (SELECT " + BudgetCol.lstID + " FROM T_BUDGET);" +
                        "INSERT INTO T_BUDGET (" + BudgetCol.lsdID + ")" +
                        "SELECT ID FROM T_LIST_DETAIL WHERE ID NOT IN (SELECT " + BudgetCol.lsdID + " FROM T_BUDGET))";
        DBManager.database.execSQL(insertQuery);
        String deleteQuery = "DELETE FROM T_BUDGET WHERE " + BudgetCol.lstID + " NOT IN (SELECT ID FROM T_LIST) AND " + BudgetCol.lsdID + " NOT IN (SELECT ID FROM T_LIST_DETAIL)";
        DBManager.database.execSQL(deleteQuery);
        DBManager.close();
    }

    public static ArrayList<Budget> getAllByList() {

        budgetSync();

        ArrayList<Budget> budgetArray = new ArrayList<>();
        String query = "SELECT A.*,B.lstName,B.lstColor,B.lstIcon FROM T_BUDGET A INNER JOIN T_LIST B ON A.lstID=B.ID WHERE A.lsdID=0 AND B.lstType=1 ORDER BY B.lstOrder";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        while (!resultCursor.isAfterLast()) {

            Budget budget = new Budget();

            budget.setID(resultCursor.getInt(resultCursor.getColumnIndex(BudgetCol.id.value())));
            budget.setIstID(resultCursor.getInt(resultCursor.getColumnIndex(BudgetCol.lstID.value())));
            budget.setIsdID(resultCursor.getInt(resultCursor.getColumnIndex(BudgetCol.lsdID.value())));
            budget.setBudPeriod(resultCursor.getString(resultCursor.getColumnIndex(BudgetCol.budPeriod.value())));
            budget.setBudAmount(String.valueOf(resultCursor.getDouble(resultCursor.getColumnIndex(BudgetCol.budAmount.value()))));
            budget.setBudType(resultCursor.getInt(resultCursor.getColumnIndex(BudgetCol.budType.value())));
            budget.setBudIsActive(resultCursor.getInt(resultCursor.getColumnIndex(BudgetCol.budIsActive.value())));

            budget.lstName = resultCursor.getString(resultCursor.getColumnIndex(Group.GroupCol.lstName.value()));
            budget.lstColor = resultCursor.getString(resultCursor.getColumnIndex(Group.GroupCol.lstColor.value()));
            budget.lstIcon = resultCursor.getString(resultCursor.getColumnIndex(Group.GroupCol.lstIcon.value()));

            budgetArray.add(budget);
            resultCursor.moveToNext();
        }
        resultCursor.close();
        DBManager.close();

        return budgetArray;
    }

    public static String getSumAll() {

        String res = "0";

        String query = "SELECT SUM(budAmount) FROM T_BUDGET A WHERE lsdID=0";

        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);

        resultCursor.moveToFirst();
        if (resultCursor.getCount() > 0)
            res = String.valueOf(resultCursor.getDouble(0));

        resultCursor.close();
        DBManager.close();

        return res;
    }

    public static String getSumAll(int listId) {

        String res = "0";

        String query = "SELECT budAmount FROM T_BUDGET A WHERE lsdID=0 AND lstID=" + listId;

        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);

        resultCursor.moveToFirst();
        if (resultCursor.getCount() > 0)
            res = resultCursor.getString(0);

        resultCursor.close();
        DBManager.close();

        return res;
    }

}
