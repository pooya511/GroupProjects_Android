package model;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.NewAndEditPayeeActivity;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import adapters.PopPayeeAdapter;
import db.DBManager;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.AppModules;
import utils.DialogUtils;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Payee extends TX_Entity implements Parcelable {

    public static RecyclerView payeeRecyclerList;
    public static ConstraintLayout didntFindlayout;
    private static PopPayeeAdapter popPayeeAdapter;

    public Payee() {
    }

    protected Payee(Parcel in) {
        dbColArrayList = in.createTypedArrayList(DBCol.CREATOR);
    }

    public static final Creator<Payee> CREATOR = new Creator<Payee>() {
        @Override
        public Payee createFromParcel(Parcel in) {
            return new Payee(in);
        }

        @Override
        public Payee[] newArray(int size) {
            return new Payee[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(dbColArrayList);
    }

    public enum PayeeCol {

        id("ID"),
        pyeName("pyeName"),
        pyeAccount("pyeAccount"),
        pyeAccount2("pyeAccount2"),
        pyePhone("pyePhone"),
        pyeIsActive("pyeIsActive");


        private final String name;

        PayeeCol(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }

    }

    private static final String childTableName = "T_Payee";
    private ArrayList<DBCol> dbColArrayList = new ArrayList<>(
            Arrays.asList(new DBCol(null, PayeeCol.id.value(), EnumsAndConstants.Types.INTEGER, 10, false, false, true),
                    new DBCol(null, PayeeCol.pyeName.value(), EnumsAndConstants.Types.STRING, 20, true, true, false),
                    new DBCol(null, PayeeCol.pyeAccount.value(), EnumsAndConstants.Types.STRING, 30, true, true, false),
                    new DBCol(null, PayeeCol.pyeAccount2.value(), EnumsAndConstants.Types.STRING, 40, true, true, false),
                    new DBCol(null, PayeeCol.pyePhone.value(), EnumsAndConstants.Types.STRING, 50, true, true, false),
                    new DBCol(null, PayeeCol.pyeIsActive.value(), EnumsAndConstants.Types.INTEGER, 60, true, true, false)
            )
    );

    @Override
    String childTableName() {
        return childTableName;
    }

    @Override
    ArrayList<DBCol> childDBColArrayList() {
        return dbColArrayList;
    }

    public Payee(int ID, String pyeName, String pyeAccount, String pyeAccount2, String pyePhone, int pyeIsActive) {
        setID(ID);
        setPyeName(pyeName);
        setPyeAccount(pyeAccount);
        setPyeAccount2(pyeAccount2);
        setPyePhone(pyePhone);
        setPyeIsActive(pyeIsActive);
    }

    public int getID() {
        return super.GetInt("ID");
    }

    public void setID(int ID) {
        super.Set("ID", ID);
    }

    public String getPyeName() {
        return super.GetString("pyeName");
    }

    public void setPyeName(String pyeName) {
        super.Set("pyeName", pyeName);
    }

    public String getPyeAccount() {
        return super.GetString("pyeAccount");
    }

    public void setPyeAccount(String pyeAccount) {
        super.Set("pyeAccount", pyeAccount);
    }

    public String getPyeAccount2() {
        return super.GetString("pyeAccount2");
    }

    public void setPyeAccount2(String pyeAccount2) {
        super.Set("pyeAccount2", pyeAccount2);
    }

    public String getPyePhone() {
        return super.GetString("pyePhone");
    }

    public void setPyePhone(String pyePhone) {
        super.Set("pyePhone", pyePhone);
    }

    public int getPyeIsActive() {
        return super.GetInt("pyeIsActive");
    }

    public void setPyeIsActive(int pyeIsActive) {
        super.Set("pyeIsActive", pyeIsActive);
    }

    public static Payee getPayee(int id) {

        Payee payee = new Payee();

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery("SELECT * FROM T_Payee WHERE ID = " + id, null);
        if (resCursor.getCount() > 0) {
            resCursor.moveToFirst();

            payee.setID(resCursor.getInt(resCursor.getColumnIndex(PayeeCol.id.value())));
            payee.setPyeName(resCursor.getString(resCursor.getColumnIndex(PayeeCol.pyeName.value())));
            payee.setPyeAccount(resCursor.getString(resCursor.getColumnIndex(PayeeCol.pyeAccount.value())));
            payee.setPyeAccount2(resCursor.getString(resCursor.getColumnIndex(PayeeCol.pyeAccount2.value())));
            payee.setPyePhone(resCursor.getString(resCursor.getColumnIndex(PayeeCol.pyePhone.value())));
            payee.setPyeIsActive(resCursor.getInt(resCursor.getColumnIndex(PayeeCol.pyeIsActive.value())));

        }
        resCursor.close();
        DBManager.close();

        return payee;
    }

    public static ArrayList<Payee> getAll() {

        ArrayList<Payee> payees = new ArrayList<>();
        String query = "SELECT * FROM T_Payee";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        while (!resultCursor.isAfterLast()) {

            Payee payee = new Payee();

            payee.setID(resultCursor.getInt(0));
            payee.setPyeName(resultCursor.getString(1));
            payee.setPyeAccount(resultCursor.getString(2));
            payee.setPyeAccount2(resultCursor.getString(3));
            payee.setPyePhone(resultCursor.getString(4));
            payee.setPyeIsActive(resultCursor.getInt(5));

            payees.add(payee);
            resultCursor.moveToNext();
        }

        resultCursor.close();
        DBManager.close();

        return payees;
    }

    public static ArrayList<Payee> getAllActive() {

        ArrayList<Payee> payees = new ArrayList<>();
        String query = "SELECT * FROM T_Payee WHERE pyeIsActive = 1";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        while (!resultCursor.isAfterLast()) {

            Payee payee = new Payee();

            payee.setID(resultCursor.getInt(0));
            payee.setPyeName(resultCursor.getString(1));
            payee.setPyeAccount(resultCursor.getString(2));
            payee.setPyeAccount2(resultCursor.getString(3));
            payee.setPyePhone(resultCursor.getString(4));
            payee.setPyeIsActive(resultCursor.getInt(5));

            payees.add(payee);
            resultCursor.moveToNext();
        }

        resultCursor.close();
        DBManager.close();

        return payees;
    }

    public static int count() {
        int res = 0;

        String query = "SELECT COUNT(*) FROM T_Payee";
        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery(query, null);
        resCursor.moveToFirst();
        if (resCursor.getCount() > 0)
            res = resCursor.getInt(0);
        resCursor.close();
        DBManager.close();

        return res;
    }

    @Override
    public int insert() {
        int inserted_id = super.insert();
        setID(inserted_id);
        return inserted_id;
    }

    public static Dialog payeeList(final Context context, final PopPayeeAdapter.PayeeCallBack callBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View payeeListLayout = inflater.inflate(R.layout.pop_bank_and_payee_select, null);

        final EditText searchEditText = payeeListLayout.findViewById(R.id.pop_bank_and_payee_search_hint);
        final ImageView clearImg = payeeListLayout.findViewById(R.id.pop_bank_and_payee_clear_img);
        payeeRecyclerList = payeeListLayout.findViewById(R.id.pop_bank_and_payee_recycler);
        TextView textView = payeeListLayout.findViewById(R.id.pop_bank_and_payee_text);
        final TextView addPayee = payeeListLayout.findViewById(R.id.pop_bank_and_payee_add);
        didntFindlayout = payeeListLayout.findViewById(R.id.pop_bank_and_payee_didnt_add_account_layout);

        searchEditText.setHint("جستجو نام طرف حساب");
        textView.setText("هیچ طرف حساب فعالی یافت نشد");
        addPayee.setText("+ اضافه کردن طرف حساب");

        payeeRecyclerList.setLayoutManager(new LinearLayoutManager(context));
        payeeRecyclerList.setHasFixedSize(true);

        ArrayList<Payee> payees = Payee.getAllActive();
        popPayeeAdapter = new PopPayeeAdapter(context, callBack);

        if (payees.size() == 0) {
            payeeRecyclerList.setVisibility(View.GONE);
            didntFindlayout.setVisibility(View.VISIBLE);
        } else {
            payeeRecyclerList.setVisibility(View.VISIBLE);
            didntFindlayout.setVisibility(View.GONE);
            popPayeeAdapter.setReturnedData(Payee.sortPayeesOrder(payees , Settings.loadMap(Settings.PAYEE_ORDER_MAP)));
            popPayeeAdapter.getInstance().notifyDataSetChanged();
            payeeRecyclerList.setAdapter(popPayeeAdapter);
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length()>1){
                    ArrayList<Payee> filteredPayees = AppModules.filterPayee(String.valueOf(s), Payee.getAllActive());
                    if (filteredPayees.size() == 0) {
                        payeeRecyclerList.setVisibility(View.GONE);
                        didntFindlayout.setVisibility(View.VISIBLE);
                        addPayee.setVisibility(View.GONE);
                    } else {
                        didntFindlayout.setVisibility(View.GONE);
                        payeeRecyclerList.setVisibility(View.VISIBLE);
                        popPayeeAdapter.setReturnedData(Payee.sortPayeesOrder(filteredPayees , Settings.loadMap(Settings.PAYEE_ORDER_MAP)));
                        payeeRecyclerList.setAdapter(popPayeeAdapter);
                        PopPayeeAdapter.getInstance().notifyDataSetChanged();
                    }

                    if (PopPayeeAdapter.getInstance() != null)
                        PopPayeeAdapter.getInstance().notifyDataSetChanged();

                    if (s.length() > 0) {
                        clearImg.setVisibility(View.VISIBLE);
                    } else {
                        clearImg.setVisibility(View.GONE);
                    }
                }


            }
        };

        searchEditText.addTextChangedListener(textWatcher);

        addPayee.setOnClickListener(v -> {
            Intent intent = new Intent(context , NewAndEditPayeeActivity.class);
            context.startActivity(intent);
        });

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 80;
                height = 63;
                break;

            case 2:
                width = 70;
                height = 57;
                break;

            case 3:
                width = 60;
                height = 50;
                break;

        }

        clearImg.setOnClickListener(v -> searchEditText.setText(""));

        final Dialog resDialog = PublicDialogs.makeDialog(context, payeeListLayout, width, height, true);

        return resDialog;
    }

    public static ArrayList<Payee> sortPayeesOrder(ArrayList<Payee> payees, HashMap<String, String> order) {
        List<Map.Entry<String, String>> orderMap = Account.sortByValue(order);

        ArrayList<Payee> sortArray = new ArrayList<>();
        ArrayList<Payee> allPayees = payees;

        if (allPayees.size() != 0) {
            for (int i = 0; i < orderMap.size(); i++) {
                Payee tempPayee = getPayeeById(allPayees, Integer.parseInt(orderMap.get(i).getKey()));
                if (tempPayee != null)
                    sortArray.add(tempPayee);
            }
        }

        return sortArray;
    }

    public static void updatePayeeOrderHashMap(ArrayList<Payee> payees) {
        LinkedHashMap<String, String> order = new LinkedHashMap<>();

        if (payees.size() != 0) {
            for (int i = 0; i < payees.size(); i++) {
                order.put(String.valueOf(payees.get(i).getID()), String.valueOf(i));
            }
        }

        Settings.saveMap(Settings.PAYEE_ORDER_MAP, order);
    }

    private static Payee getPayeeById(ArrayList<Payee> payees, int id) {

        for (int i = 0; i < payees.size(); i++) {
            if (payees.get(i).getID() == id) {
                return payees.get(i);
            }
        }
        return null;
    }
}
