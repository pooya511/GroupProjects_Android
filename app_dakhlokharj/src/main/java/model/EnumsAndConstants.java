package model;

/**
 * Created by Parsa on 9/26/2017.
 */

public class EnumsAndConstants {

    public static String SERVER_URL = "http://api.taxiapps.ir/api/taxiapps";
    public static String GATEWAY_URL = "http://api.taxiapps.ir/BankWithUser";
    public static String APP_ID = "dakhlokharj";
    public static String APP_VERSION = "";
    public static String APP_SOURCE = "google";
    public static String DEVICE_ID = "";
    public static String GOOGLE_DEVELOPER_ID = "00206505599730386804";
    public static String BAZAAR_DEVELOPER_ID = "taxiapps";
    public static String OS = "android";
    // ----------------------- API Constants

    public static final String SUCCESS = "0";
    public static final String UNKNOWN_ERROR = "-10";
    public static final String PRODUCT_INVALID = "-20";
    public static final String TRANSACTION_INVALID = "-30";
    public static final String COUPON_INVALID = "-40";
    public static final String COUPON_EXPIRED = "-41";
    public static final String COUPON_INVALID_APP = "-42";
    public static final String COUPON_EXPIRE_FAILED = "-48";
    public static final String APP_INVALID = "-60";
    public static final String BACKUP_SIZE_LIMIT = "-70";
    public static final String SMS_NUMBER_INVALID = "-80";
    public static final String SMS_METHOD1_DISABLED = "-81";
    public static final String SMS_UNKNOWN_ERROR = "-82";
    public static final String USER_NOT_EXISTS = "-90";
    public static final String LICENSE_NOT_EXISTS = "-50";
    public static final String USER_LICENSE_NOT_FOUND = "-51";

    // ----------------------- Error API Constants
    public static final String INFO_PAYMENT_VERIFY_ERROR = "پرداخت ناموفق بوده است.در صورت کسر مبلغ وجه شما حداکثر تا ۱ ساعت به کارت بانکی برگشت داده می شود. شما می توانید هم اکنون مجددا تلاش کنید";
    public static final String INFO_PAYMENT_STATUS_CANCELED = "شما فرآیند خرید را لغو کردید. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_ISSUERDOWN = "در حال حاضر بانک صادر کننده پاسخگو نیست. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_UNKNOWN = "خطای نامشخص. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";

    public static final String SHARE_LINK_GOOGLE = "https://play.google.com/store/apps/details?id=com.taxiapps.dakhlokharj";
    public static final String SHARE_LINK_BAZAAR = "https://cafebazaar.ir/app/com.taxiapps.dakhlokharj/?l=fa";
    public static final String SHARE_LINK_MYKET = "https://myket.ir/app/com.taxiapps.dakhlokharj";

    //base64
    public static final String BAZAAR_PAYMENT_BASE64 = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwDWz80z8cB7u9/sXD71CUed9fxOA5XoD5f1kl/tA4HANU9yX6eyoFzUoDdbEiL0NJ9m9b7z+/gp63AJ4Um4OHh0/bmZcSxmbe1eeTqrvTu/VH0wG0D15D8ajSe+sOJx95FziThBbwq81WxuWuwAP13L/bxxOA6LlnAUY26EDYlsVA5ckbYEVWZ0xxm26E0PcvLuDo+I/ao6SzQADnhNbGhDPeCXHekZPv6/BFZcKSkCAwEAAQ==";
    public static final String MYKET_PAYMENT_BASE64 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCEhXG4ggf2xKTaLV66nQjTFcvWtGUU6Iz9lBAsrsrd+VBP7Mk5RdhWcCmNAuTYAnywHJNMok9YS0rivXWcrCAS1JF7vLLbAxa2pc9DoqGHLR197qXBdjdYX4iXStjFL0MuIVG4wr9BxtV0NlO5YwcYi8rUrq7AqXMcQmfmyCTY6wIDAQAB";

    public enum Types {

        INTEGER("Integer"),
        BOOLEAN("Boolean"),
        STRING("String"),
        FLOAT("Float"),
        DOUBLE("Double"),
        LONG("Long");

        private final String name;

        Types(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }

    }

    public enum TransactionTypes {

        Expense(1),
        Income(2),
        WithDraw(3),
        Deposit(4);

        private int type;

        TransactionTypes(int type) {
            this.type = type;
        }

        public int value() {
            return this.type;
        }

    }

    public enum AmountTypeFace {

        Arabic("yekan.ttf"),
        Kharejik("yekan_en.ttf"); // :D

        private String typeFaceValue;

        AmountTypeFace(String type) {
            this.typeFaceValue = type;
        }

        public String value() {
            return this.typeFaceValue;
        }

    }

    public enum BackupPeriodItems {

        Daily("Daily"),
        Weekly("Weekly"),
        Monthly("Monthly");

        private String period;

        BackupPeriodItems(String type) {
            this.period = type;
        }

        public String value() {
            return this.period;
        }

    }

    public enum BackupMaximumFileCountItems {

        One(1),
        Five(5),
        Ten(10),
        Fifty(50);

        private int type;

        BackupMaximumFileCountItems(int type) {
            this.type = type;
        }

        public int value() {
            return this.type;
        }

    }

}