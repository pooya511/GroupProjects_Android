package model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.drive.DriveId;

public class MyMetaData implements Parcelable {

    private DriveId driveId;
    private String title;
    private long lastModifiedDate; //TimeStamp
    private long fileSize;

    public MyMetaData(DriveId driveId ,String title, long lastModifiedDate, long fileSize) {
        this.driveId = driveId;
        this.title = title;
        this.lastModifiedDate = lastModifiedDate;
        this.fileSize = fileSize;
    }

    protected MyMetaData(Parcel in) {
        driveId = in.readParcelable(DriveId.class.getClassLoader());
        title = in.readString();
        lastModifiedDate = in.readLong();
        fileSize = in.readLong();
    }

    public static final Creator<MyMetaData> CREATOR = new Creator<MyMetaData>() {
        @Override
        public MyMetaData createFromParcel(Parcel in) {
            return new MyMetaData(in);
        }

        @Override
        public MyMetaData[] newArray(int size) {
            return new MyMetaData[size];
        }
    };

    public DriveId getDriveId() {
        return driveId;
    }

    public void setDriveId(DriveId driveId) {
        this.driveId = driveId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(driveId, flags);
        dest.writeString(title);
        dest.writeLong(lastModifiedDate);
        dest.writeLong(fileSize);
    }
}
