package model;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.Arrays;

import adapters.BankAdapter;
import db.DBManager;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.DialogUtils;
import utils.AppModules;

/**
 * Created by Parsa on 9/20/2017.
 */

public class Bank extends TX_Entity {

    public Bank() {

    }

    public enum BankCol {

        id("ID"),
        bnkTitle("bnkTitle"),
        bnkLogo("bnkLogo"),
        bnkIsDeleted("bnkIsDeleted"),
        bnkOrder("bnkOrder");

        private final String name;

        BankCol(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }

    }

    private static final String childTableName = "T_Bank";
    private ArrayList<DBCol> dbColArrayList = new ArrayList<>(
            Arrays.asList(new DBCol(null, BankCol.id.value(), EnumsAndConstants.Types.INTEGER, 10, false, false, true),
                    new DBCol(null, BankCol.bnkTitle.value(), EnumsAndConstants.Types.STRING, 20, true, true, false),
                    new DBCol(null, BankCol.bnkLogo.value(), EnumsAndConstants.Types.STRING, 30, true, true, false),
                    new DBCol(null, BankCol.bnkIsDeleted.value(), EnumsAndConstants.Types.BOOLEAN, 40, true, true, false),
                    new DBCol(null, BankCol.bnkOrder.value(), EnumsAndConstants.Types.INTEGER, 50, true, true, false)
            )
    );

    @Override
    String childTableName() {
        return childTableName;
    }

    @Override
    ArrayList<DBCol> childDBColArrayList() {
        return dbColArrayList;
    }

    public Bank(int ID, String bnkTitle, String bnkLogo, boolean bnkIsDeleted, int bnkOrder) {
        setID(ID);
        setBnkTitle(bnkTitle);
        setBnkLogo(bnkLogo);
        setBnkIsDeleted(bnkIsDeleted);
        setBnkOrder(bnkOrder);
    }

    public int getID() {
        return super.GetInt("ID");
    }

    public void setID(int ID) {
        super.Set("ID", ID);
    }

    public String getBnkTitle() {
        return super.GetString("bnkTitle");
    }

    public void setBnkTitle(String bnkTitle) {
        super.Set("bnkTitle", bnkTitle);
    }

    public String getBnkLogo() {
        return super.GetString("bnkLogo");
    }

    public void setBnkLogo(String bnkLogo) {
        super.Set("bnkLogo", bnkLogo);
    }

    public boolean isBnkIsDeleted() {
        return super.GetBool("bnkIsDeleted");
    }

    public void setBnkIsDeleted(boolean bnkIsDeleted) {
        super.Set("bnkIsDeleted", bnkIsDeleted);
    }

    public int getBnkOrder() {
        return super.GetInt("bnkOrder");
    }

    public void setBnkOrder(int bnkOrder) {
        super.Set("bnkOrder", bnkOrder);
    }

    public static ArrayList<Bank> getAll() {
        ArrayList<Bank> bnkArray = new ArrayList<>();
        String query = "SELECT * FROM T_Bank";
        DBManager.openForRead();
        Cursor resultCursor = DBManager.database.rawQuery(query, null);
        resultCursor.moveToFirst();
        int ggg = 0;
        while (!resultCursor.isAfterLast()) {
            Bank bank = new Bank();

            bank.setID(resultCursor.getInt(0));
            bank.setBnkTitle(resultCursor.getString(1));
            bank.setBnkLogo(resultCursor.getString(2));

            if (resultCursor.getInt(3) == 1)
                bank.setBnkIsDeleted(true);
            else
                bank.setBnkIsDeleted(false);

            bank.setBnkOrder(resultCursor.getInt(4));

            bnkArray.add(bank);
            resultCursor.moveToNext();
            ggg++;
        }
        Log.i("count ggg", String.valueOf(ggg));

        resultCursor.close();
        DBManager.close();

        return bnkArray;
    }

    public static Bank getBankWithID(int id) {

        Bank bank = new Bank();

        DBManager.openForRead();
        Cursor resCursor = DBManager.database.rawQuery("SELECT * FROM T_Bank WHERE ID =" + id, null);

        if (resCursor.getCount() > 0) {
            resCursor.moveToFirst();

            bank.setID(resCursor.getInt(resCursor.getColumnIndex(BankCol.id.value())));
            bank.setBnkTitle(resCursor.getString(resCursor.getColumnIndex(BankCol.bnkTitle.value())));
            bank.setBnkLogo(resCursor.getString(resCursor.getColumnIndex(BankCol.bnkLogo.value())));

            if (resCursor.getInt(resCursor.getColumnIndex(BankCol.bnkIsDeleted.value())) == 1)
                bank.setBnkIsDeleted(true);
            else if (resCursor.getInt(resCursor.getColumnIndex(BankCol.bnkIsDeleted.value())) == 0)
                bank.setBnkIsDeleted(false);

            bank.setBnkOrder(resCursor.getInt(resCursor.getColumnIndex(BankCol.bnkLogo.value())));
        }

        resCursor.close();
        DBManager.close();

        return bank;
    }

    @Override
    public int insert() {
        int inserted_id = super.insert();
        setID(inserted_id);
        return inserted_id;
    }

    public interface BankCallback {
        void call(Bank bank);
    }

    public static Dialog choosingBank(final Context context, final BankCallback callback) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View banksLayout = inflater.inflate(R.layout.pop_bank_and_payee_select, null);

        final ImageView clearImg = banksLayout.findViewById(R.id.pop_bank_and_payee_clear_img);

        final EditText searchEditText = banksLayout.findViewById(R.id.pop_bank_and_payee_search_hint);
        searchEditText.setHint("جستجو نام بانک");

        BankAdapter bankAdapter = new BankAdapter(context, Bank.getAll(), callback);

        final RecyclerView bankRecycle = banksLayout.findViewById(R.id.pop_bank_and_payee_recycler);
        bankRecycle.setLayoutManager(new LinearLayoutManager(context));
        bankRecycle.setHasFixedSize(true);
        bankRecycle.setAdapter(bankAdapter);


        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                ArrayList<Bank> filteredPayees = AppModules.filterBank(String.valueOf(s), Bank.getAll());
                bankRecycle.setAdapter(new BankAdapter(context, filteredPayees, callback));
                bankAdapter.notifyDataSetChanged();

                if (s.length() > 0) {
                    clearImg.setVisibility(View.VISIBLE);
                } else {
                    clearImg.setVisibility(View.GONE);
                }

            }
        };

        searchEditText.addTextChangedListener(textWatcher);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 80;
                height = 63;
                break;

            case 2:
                width = 70;
                height = 57;
                break;

            case 3:
                width = 60;
                height = 50;
                break;

        }

        clearImg.setOnClickListener(v -> searchEditText.setText(""));

        Dialog banksDialog = PublicDialogs.makeDialog(context, banksLayout, width, height, true);

        return banksDialog;
    }

}
