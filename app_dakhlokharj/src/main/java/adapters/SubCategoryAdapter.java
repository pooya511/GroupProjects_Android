package adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import dialog_fragments.CategoryDialogFragment;
import fragments.SelectSubCategory;
import model.GroupDetail;
import utils.AppModules;

/**
 * Created by Parsa on 2018-01-03.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<GroupDetail> groupDetailArray;
    private int groupId;
    public boolean showMinusIcon; // yani user mikhad Edit kone
    private SelectSubCategory.SelectedSubCategoryCallBack callBack;

    private final int ADD_VIEW = 1;
    private final int NORMAL_VIEW = 2;

    public SubCategoryAdapter(Context context, int groupId, SelectSubCategory.SelectedSubCategoryCallBack callBack, ArrayList<GroupDetail> groupDetailArray, boolean isEditMode) {
        this.context = context;
        this.groupId = groupId;
        inflater = LayoutInflater.from(context);
        this.callBack = callBack;
        if (groupDetailArray == null) {
            this.groupDetailArray = GroupDetail.getAll(groupId);
            if (this.groupDetailArray.size() == 0) {
                this.groupDetailArray.add(0, new GroupDetail("new"));
                showMinusIcon = true;
            }
        } else {
            this.groupDetailArray = groupDetailArray;
            showMinusIcon = isEditMode;
        }
        instance = this;
    }

    public static SubCategoryAdapter instance = null;

    public static SubCategoryAdapter getInstance() {
        return instance;
    }

    public void reloadData() {
        groupDetailArray = GroupDetail.getAll(groupId);
        notifyDataSetChanged();
    }

    public void setFiltredData(String filterSymbol) {
        for (int i = 0; i < groupDetailArray.size(); i++) {
            GroupDetail currGroup = groupDetailArray.get(i);
            if (!currGroup.getLsdName().contains(filterSymbol)) {
                groupDetailArray.remove(currGroup);
                notifyItemRemoved(i);
            }
        }
    }

    public void editStart() {

        if (groupDetailArray.size() > 0)
            if (!groupDetailArray.get(0).getLsdName().equals("new")) {
                groupDetailArray.add(0, new GroupDetail("new"));
                showMinusIcon = true;
                SelectSubCategory.isEditMode = true;
                //minus.setVisibility(View.VISIBLE);
                SelectSubCategory.edit.setText("ذخیره");
                instance.notifyDataSetChanged();
            }

    }

    public void editEnd() {

        showMinusIcon = false;
        SelectSubCategory.isEditMode = false;
        if (groupDetailArray.size() > 1)
            groupDetailArray.remove(0);
        // if (minus != null)
        // minus.setVisibility(View.GONE);
        SelectSubCategory.edit.setText("ویرایش");
        reloadData();
        instance.notifyDataSetChanged();

    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        if (groupDetailArray.get(position).getLsdName().equals("new"))
            return ADD_VIEW;
        else
            return NORMAL_VIEW;
    }

    @Override
    public SubCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == NORMAL_VIEW)
            return new SubCategoryViewHolder(inflater.inflate(R.layout.item_sub_categories, parent, false));
        else
            return new SubCategoryViewHolder(inflater.inflate(R.layout.item_edit_categories, parent, false));
    }

    @Override
    public void onBindViewHolder(final SubCategoryViewHolder holder, final int position) {

        final GroupDetail groupDetail = groupDetailArray.get(position);

        if (holder.getItemViewType() == NORMAL_VIEW) {

            if (position == 0)
                holder.topBorder.setVisibility(View.GONE);
            else
                holder.topBorder.setVisibility(View.VISIBLE);

            holder.swipeLayout.setRightSwipeEnabled(false);

            if (showMinusIcon)
                holder.minus.setVisibility(View.VISIBLE);
            else
                holder.minus.setVisibility(View.GONE);

            holder.minus.setOnClickListener(v -> holder.swipeLayout.open(true));

            holder.removeLayout.setOnClickListener(new View.OnClickListener() {
                private Dialog removeDialog;

                @Override
                public void onClick(View v) {

                    removeDialog = CategoryDialogFragment.removeCategoriesAndSubCategories(context, () -> {
                        groupDetailArray.remove(groupDetail);
                        groupDetail.delete(); // delete From DB
                        notifyDataSetChanged();
                        removeDialog.dismiss();
                    });

                    removeDialog.show();
                }
            });

            holder.title.setText(groupDetail.getLsdName());

            holder.icon = AppModules.setTypeFace(context, holder.icon, "fontawesome_webfont.ttf");
            holder.icon.setText(AppModules.convertIconFromString(context, groupDetail.getLsdIcon()));
            holder.icon.setTextColor(Color.parseColor(groupDetail.getLsdColor()));

        }

        holder.itemView.setOnClickListener(v -> callBack.call(groupDetail));


    }

    @Override
    public int getItemCount() {
        return groupDetailArray.size();
    }

    public class SubCategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView title, icon;
        private SwipeLayout swipeLayout;
        private LinearLayout removeLayout;
        private ImageView minus;
        private View topBorder;

        public SubCategoryViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_sub_categories_title);
            icon = itemView.findViewById(R.id.item_sub_categories_icon);
            minus = itemView.findViewById(R.id.item_sub_categories_ic_delete);
            swipeLayout = itemView.findViewById(R.id.item_sub_categories_swipe_layout);
            removeLayout = itemView.findViewById(R.id.item_sub_categories_remove_layout);
            topBorder = itemView.findViewById(R.id.item_sub_categories_top_border);

        }
    }

}
