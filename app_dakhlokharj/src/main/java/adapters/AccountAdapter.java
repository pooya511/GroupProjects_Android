package adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.dakhlokharj.R;

import java.io.Serializable;
import java.util.ArrayList;

import model.Account;
import model.Bank;
import modules.PublicModules;

/**
 * Created by Parsa on 2017-12-11.
 */

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountViewHolder> implements Serializable {

    private ArrayList<Account> accountArrayList;

    public void setAccountArrayList(ArrayList<Account> accountArrayList) {
        this.accountArrayList = accountArrayList;
    }

    private Context context;
    private Account.AccountCallBack callBack;
    private LayoutInflater inflater;

    public AccountAdapter(Context context, Account.AccountCallBack callBack, ArrayList<Account> accountArrayList) {
        this.context = context;
        this.callBack = callBack;
        this.accountArrayList = accountArrayList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AccountViewHolder(inflater.inflate(R.layout.item_bank, parent, false));
    }

    @Override
    public void onBindViewHolder(AccountViewHolder holder, int position) {

        Account account = accountArrayList.get(position);

        holder.accountTitle.setText(account.getAcnName());
        PublicModules.stringToBitmap(Bank.getBankWithID(account.getAcnIcon()).getBnkLogo(), holder.bnkLogo);

    }

    @Override
    public int getItemCount() {
        return accountArrayList.size();
    }

    class AccountViewHolder extends RecyclerView.ViewHolder {

        private TextView accountTitle;
        private ImageView bnkLogo;

        public AccountViewHolder(View itemView) {
            super(itemView);

            accountTitle = itemView.findViewById(R.id.item_bank_bank_name);
            bnkLogo = itemView.findViewById(R.id.item_bank_bank_logo);

            itemView.setOnClickListener(v -> callBack.call(accountArrayList.get(getAdapterPosition())));

        }
    }

}