package adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.HashMap;

import model.Currency;
import model.GroupDetail;
import model.Transaction;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;

/**
 * Created by Parsa on 2018-01-03.
 */

public class PeriodReportAdapter extends RecyclerView.Adapter<PeriodReportAdapter.PeriodReportViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private TextView amount;

    private static PeriodReportAdapter instance;

    public static PeriodReportAdapter getInstance() {
        if (instance == null)
            Log.i("PeriodReportAdapter", "init nakardi meimoon");

        return instance;
    }

    private ArrayList<HashMap<String, Object>> transactionArrayList;

    public PeriodReportAdapter(Context context, ArrayList<HashMap<String, Object>> transactionArrayList) {
        this.transactionArrayList = transactionArrayList;
        this.context = context;
        inflater = LayoutInflater.from(context);
        instance = this;
    }

    private void setTypeFaces() {
        amount.setTypeface(Application.amountTypeFace(context));
    }

    @Override
    public PeriodReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PeriodReportViewHolder(inflater.inflate(R.layout.item_period_report, parent, false));
    }

    @Override
    public void onBindViewHolder(PeriodReportViewHolder holder, int position) {

        HashMap<String, Object> targetItem = transactionArrayList.get(position);

        if (position + 1 == transactionArrayList.size())
            holder.bottomBorder.setVisibility(View.VISIBLE);
        else
            holder.bottomBorder.setVisibility(View.GONE);

        setTypeFaces();

        holder.icon = AppModules.setTypeFace(context, holder.icon, "fontawesome_webfont.ttf");
        holder.icon.setText(AppModules.convertIconFromString(context, String.valueOf(targetItem.get(GroupDetail.GroupDetailCol.lsdIcon.value()))));
        holder.icon.setTextColor(Color.parseColor(String.valueOf(targetItem.get(GroupDetail.GroupDetailCol.lsdColor.value()))));

        holder.title.setText(String.valueOf(targetItem.get(GroupDetail.GroupDetailCol.lsdName.value())));
//        holder.date.setText(PublicModules.timeStamp2String(Long.valueOf(String.valueOf(targetItem.get(Transaction.TransactionCol.trnDate.value()))), "yyyy/MM/dd HH:mm"));
        String date = PersianDateFormat.format(new PersianDate(Long.valueOf(String.valueOf(targetItem.get(Transaction.TransactionCol.trnDate.value())))),"Y/m/d H:i");
        holder.date.setText(date);

        if (Integer.valueOf(String.valueOf(targetItem.get(Transaction.TransactionCol.trnType.value()))) == 1)
            amount.setTextColor(Color.parseColor("#ee3333"));
        else if (Integer.valueOf(String.valueOf(targetItem.get(Transaction.TransactionCol.trnType.value()))) == 2)
            amount.setTextColor(Color.parseColor("#008000"));

        amount.setText(Currency.CurrencyString.getCurrencyString(String.valueOf(targetItem.get(Transaction.TransactionCol.trnAmount.value())), Currency.CurrencyMode.Short).separated);

    }

    @Override
    public int getItemCount() {
        return transactionArrayList.size();
    }

    public class PeriodReportViewHolder extends RecyclerView.ViewHolder {

        private TextView icon, title, date;
        private View bottomBorder;

        public PeriodReportViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.period_report_list_activity_item_icon);
            title = itemView.findViewById(R.id.period_report_list_activity_item_name_text);
            date = itemView.findViewById(R.id.period_report_list_activity_item_date_text);
            amount = itemView.findViewById(R.id.period_report_list_activity_item_amount_text);
            bottomBorder = itemView.findViewById(R.id.period_report_list_activity_bottom_border);
        }
    }
}
