package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import model.Payee;

/**
 * Created by Parsa on 2017-12-04.
 */

public class PopPayeeAdapter extends RecyclerView.Adapter<PopPayeeAdapter.PayeeViewHolder> {

    private static Context context;
    private ArrayList<Payee> returnedData;

    public void setReturnedData(ArrayList<Payee> returnedData) {
        this.returnedData = returnedData;
    }

    private PayeeCallBack callBack;

    public interface PayeeCallBack {
        void call(Payee payee);
    }

    public PopPayeeAdapter(Context context, PayeeCallBack callBack) {
        this.context = context;
        this.callBack = callBack;
        instance = this;
    }

    private static PopPayeeAdapter instance = null;

    public static PopPayeeAdapter getInstance() {

        if (instance == null) {
            try {
            } catch (Exception e) {
                Toast.makeText(context, "PopPayeeAdapter should be init", Toast.LENGTH_SHORT).show();
            }
        }
        return instance;
    }

    @Override
    public PayeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_payee, parent, false);
        return new PayeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PayeeViewHolder holder, int position) {
        Payee currentPayee = returnedData.get(position);
        holder.payeeName.setText(currentPayee.getPyeName());
    }

    @Override
    public int getItemCount() {
        return returnedData.size();
    }

    class PayeeViewHolder extends RecyclerView.ViewHolder {

        private TextView payeeName;

        private PayeeViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> callBack.call(returnedData.get(getAdapterPosition())));
            payeeName = itemView.findViewById(R.id.item_payee_payee_name);
        }

    }

}