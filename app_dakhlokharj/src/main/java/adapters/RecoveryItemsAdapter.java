package adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.sip.SipSession;
import android.os.Parcel;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.GoogleDriveBackupActivity;
import com.taxiapps.dakhlokharj.MainActivity;
import com.taxiapps.dakhlokharj.R;
import com.taxiapps.dakhlokharj.SettingActivity;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import model.Currency;
import model.EnumsAndConstants;
import model.MyMetaData;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;

public class RecoveryItemsAdapter extends RecyclerView.Adapter<RecoveryItemsAdapter.RecoveryItemsViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<MyMetaData> metadataArrayList;
    private Context context;
    private int lastItemPosition = 0;

    public interface RemoveCallBack {
        void call(boolean remove);
    }

    public RecoveryItemsAdapter(Context context, ArrayList<MyMetaData> metadataArrayList) {
        this.context = context;
        this.metadataArrayList = metadataArrayList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public RecoveryItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecoveryItemsViewHolder(inflater.inflate(R.layout.item_recovery, parent, false));
    }

    @Override
    public void onBindViewHolder(RecoveryItemsViewHolder holder, int position) {

        MyMetaData currentMetaData = metadataArrayList.get(position);

        holder.name.setText(currentMetaData.getTitle());

        String size = PublicModules.readableFileSize(currentMetaData.getFileSize());
        holder.size.setText(size);

        PersianDate metaDataModifiedDate = new PersianDate(currentMetaData.getLastModifiedDate());
        PersianDateFormat formatter = new PersianDateFormat("Y/m/d");
        String date = formatter.format(metaDataModifiedDate).equals(formatter.format(new PersianDate())) ? "امروز" : formatter.format(metaDataModifiedDate);
        holder.date.setText(date);

        View.OnClickListener listener = v -> {
            if (Application.isLicenseValid()) {
                if (!currentMetaData.getTitle().contains("android")) {
                    GoogleDriveBackupActivity.getInstance().platformWarning(context, continueBaby -> {
                        if (continueBaby) {
                            recoveryWarning(context, currentMetaData).show();
                        }
                    }).show();
                } else {
                    recoveryWarning(context, currentMetaData).show();
                }
            } else {
                String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                Payment.newInstance(R.drawable.ic_dakhlokharj,
                        EnumsAndConstants.APP_ID,
                        EnumsAndConstants.APP_SOURCE,
                        EnumsAndConstants.APP_VERSION,
                        "دخل و خرج",
                        Application.currentBase64,
                        "در نسخه رایگان امکان پشتیبانگیری و بازیابی \n فعال نیست !",
                        limitationText,
                        userNumber,
                        EnumsAndConstants.DEVICE_ID,
                        EnumsAndConstants.SERVER_URL,
                        EnumsAndConstants.GATEWAY_URL,
                        new TX_License.SetLicenseCallback() {
                            @Override
                            public void call(long expireDate, String license) {

                                Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                Settings.setSetting(Settings.LICENSE_DATA, license);

                                try {
                                    JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                    Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                MainActivity.getInstance().updateFragment(false);
                                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                    Transaction.appPurchased();

                                MainActivity.getInstance().reloadMainData();
                                if (SettingActivity.getInstance() != null)
                                    SettingActivity.getInstance().premiumUIHandling(false);
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        },
                        new Payment.UserNameCallBack() {
                            @Override
                            public void getUserName(String username) {
                                Settings.setSetting(Settings.TX_USER_NUMBER, username);
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        }
                ).show(((Activity) context).getFragmentManager(), "tx_payment");
            }
        };

        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {
                super.onStartOpen(layout);
                holder.itemView.setEnabled(false);

                if (position != lastItemPosition) {
                    notifyItemChanged(lastItemPosition);
                }
                lastItemPosition = position;
            }

            @Override
            public void onClose(SwipeLayout layout) {
                super.onClose(layout);
                holder.itemView.setEnabled(true);
                lastItemPosition = 0;
            }

        });

        holder.itemView.setOnClickListener(listener);
        holder.removeLayout.setOnClickListener(v -> {
            removeWarningDialog(context, remove -> {
                if (remove) {
                    GoogleDriveBackupActivity.mDriveResourceClient.delete(currentMetaData.getDriveId().asDriveFile()).addOnSuccessListener(aVoid -> {
                        PublicModules.customToast(context, "موفق", "فایل بکاپ شما با موفقیت حذف شد.", true);

                        metadataArrayList.remove(position);
                        notifyItemRemoved(position);
                    });
                }
            }).show();

        });

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return metadataArrayList.size();
    }

    private Dialog removeWarningDialog(Context context, RemoveCallBack removeCallBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View exitSureDialogLayout = inflater.inflate(R.layout.pop_allways_deny, null);

        final TextView remove, cancel, titleText, descText;
        remove = exitSureDialogLayout.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = exitSureDialogLayout.findViewById(R.id.pop_allways_deny_cancel);
        titleText = exitSureDialogLayout.findViewById(R.id.pop_always_deny_title);
        descText = exitSureDialogLayout.findViewById(R.id.pop_always_deny_description);


        titleText.setText("هشدار");
        descText.setText("از حذف کردن فایل بکاپ خود مطمئن هستید ؟");
        remove.setText("بله");
        cancel.setText("لغو");
        remove.setTextColor(Color.RED);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 72;
                height = 19;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog exitDialog = PublicDialogs.makeDialog(context, exitSureDialogLayout, width, height, false);
        exitDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel)) {
                exitDialog.dismiss();
            }

            if (v.equals(remove)) {
                removeCallBack.call(true);
                exitDialog.dismiss();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        remove.setOnClickListener(dialogListeners);

        return exitDialog;
    }

    private Dialog recoveryWarning(Context context, MyMetaData targetMetaData) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.pop_recovery_last_warning);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);

        TextView cancel, recovery;
        cancel = dialog.getWindow().getDecorView().findViewById(R.id.pop_recovery_last_warning_close_text);
        recovery = dialog.getWindow().getDecorView().findViewById(R.id.pop_recovery_last_warning_recovery_text);

        View.OnClickListener listener = v -> {
            switch (v.getId()) {

                case R.id.pop_recovery_last_warning_close_text:
                    dialog.dismiss();
                    break;
                case R.id.pop_recovery_last_warning_recovery_text:
                    GoogleDriveBackupActivity.getInstance().progressDialog(context, false, targetMetaData).show();
                    dialog.dismiss();
                    break;
            }
        };

        cancel.setOnClickListener(listener);
        recovery.setOnClickListener(listener);

        return dialog;
    }

    class RecoveryItemsViewHolder extends RecyclerView.ViewHolder {

        private TextView name, date, size;
        private LinearLayout removeLayout;
        private SwipeLayout swipeLayout;

        public RecoveryItemsViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.item_recovery_meta_data_name);
            date = itemView.findViewById(R.id.item_recovery_meta_data_date);
            size = itemView.findViewById(R.id.item_recovery_meta_data_size);
            removeLayout = itemView.findViewById(R.id.item_recovery_remove_layout);
            swipeLayout = itemView.findViewById(R.id.item_recovery_swipe_layout);

        }
    }

}