package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.AddAndEditExpensesAndIncomesActivity;
import com.taxiapps.dakhlokharj.DetailAccountActivity;
import com.taxiapps.dakhlokharj.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.Currency;
import model.EnumsAndConstants;
import model.ExpenseOrIncomeItem;
import model.Group;
import model.GroupDetail;
import model.Transaction;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;
import utils.DialogUtils;

public class DetailAccountAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Transaction> allTransaction;

    public DetailAccountAdapter(Context context, ArrayList<Transaction> allTransaction) {
        this.context = context;
        this.allTransaction = allTransaction;
    }

    @Override
    public int getCount() {
        return allTransaction.size();
    }

    @Override
    public Object getItem(int position) {
        return allTransaction.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_expenses_or_income_list_layout, parent, false);

        final Transaction transaction = (Transaction) getItem(position);

        TextView itemName, itemDate, itemIcon, itemDescription, itemAmount;

        itemName = view.findViewById(R.id.expenses_list_activity_item_name_text);
        itemDate = view.findViewById(R.id.expenses_list_activity_item_date_text);
        itemIcon = view.findViewById(R.id.expenses_list_activity_item_icon);
        itemDescription = view.findViewById(R.id.expenses_list_activity_item_description_text);
        itemAmount = view.findViewById(R.id.expenses_list_activity_item_amount_text);
        view.findViewById(R.id.expenses_list_activity_item_arrow_img_view).setVisibility(View.INVISIBLE);

        PersianDate p = new PersianDate(Long.valueOf(transaction.getTrnDate()));
        String dateFormatted = PersianDateFormat.format(p, "Y/m/d H:i");
        itemDate.setText(dateFormatted);

        if (!transaction.getTrnDescription().equals("null")) {
            itemDescription.setText(transaction.getTrnDescription());
        }

        final Intent intent = new Intent(context, AddAndEditExpensesAndIncomesActivity.class);

        if (transaction.getTrnType() == EnumsAndConstants.TransactionTypes.Expense.value()) {
            intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.EditExpense.value());
            itemAmount.setTextColor(context.getResources().getColor(R.color.red));
        }
        if (transaction.getTrnType() == EnumsAndConstants.TransactionTypes.Income.value()) {
            intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.EditIncome.value());
            itemAmount.setTextColor(context.getResources().getColor(R.color.green));
        }

        itemAmount.setText(Currency.CurrencyString.getCurrencyString(transaction.getTrnAmount(), Currency.CurrencyMode.Full).separated);

        if (transaction.getTrnType() == EnumsAndConstants.TransactionTypes.Income.value() || transaction.getTrnType() == EnumsAndConstants.TransactionTypes.Expense.value()) {
            GroupDetail groupDetail = GroupDetail.getGroupDetailByID(transaction.getIsdID());
            itemName.setText(groupDetail.getLsdName());
            itemIcon = AppModules.setTypeFace(context, itemIcon, "fontawesome_webfont.ttf");
            itemIcon.setText(AppModules.convertIconFromString(context, groupDetail.getLsdIcon()));
            itemIcon.setTextColor(Color.parseColor(groupDetail.getLsdColor()));

            view.setOnClickListener(v -> {
                intent.putExtra("CategoryName", Group.getGroupNameWithLsdID(transaction.getIsdID()));
                ExpenseOrIncomeItem item = new ExpenseOrIncomeItem(
                        transaction.getID(),
                        transaction.lsdName,
                        Double.parseDouble(transaction.getTrnAmount()),
                        transaction.lsdIcon,
                        transaction.lsdColor,
                        transaction.getTrnDate(),
                        transaction.getTrnDescription(),
                        transaction.getTrnColor(),
                        transaction.getTrnImagePath()
                );
                intent.putExtra("Item", item);
                context.startActivity(intent);
            });

        } else {
            switch (transaction.getTrnType()) {

                case 3:
                    itemIcon.setText("⬆");
                    itemName.setText("برداشت از حساب");
                    itemName.setTextColor(Color.RED);
                    itemAmount.setTextColor(Color.RED);
                    break;

                case 4:
                    itemIcon.setText("⬇");
                    itemName.setText("واریز به حساب");
                    itemName.setTextColor(Color.GREEN);
                    itemAmount.setTextColor(Color.GREEN);
                    break;

            }

            view.setOnLongClickListener(v -> {
                DialogUtils.removeTransactionDialog(context, () -> {
                    transaction.delete();

                    ((Activity) context).finish();
                    ((Activity) context).overridePendingTransition(0, 0);
                    context.startActivity(((Activity) context).getIntent());
                    ((Activity) context).overridePendingTransition(0, 0);
                }).show();
                return true;
            });

        }

        return view;
    }
}
