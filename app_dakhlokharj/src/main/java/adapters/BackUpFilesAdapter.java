package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.R;

import java.io.File;
import java.util.ArrayList;

import model.Backup;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

/**
 * Created by Parsa on 2018-01-08.
 */

public class BackUpFilesAdapter extends RecyclerView.Adapter<BackUpFilesAdapter.BackUpViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<File> files;
    private Backup.SelectBackUpCallBack callBack;

    public BackUpFilesAdapter(Context context, ArrayList<File> files, Backup.SelectBackUpCallBack callBack) {
        this.context = context;
        this.callBack = callBack;
        this.files = files;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public BackUpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BackUpViewHolder(inflater.inflate(R.layout.item_backup_file, parent, false));
    }

    @Override
    public void onBindViewHolder(BackUpViewHolder holder, int position) {
        final File file = files.get(position);
        holder.fileName.setText(file.getName());
        holder.fileDate.setText(PersianDateFormat.format(new PersianDate(file.lastModified()), "Y/m/d"));
        holder.itemView.setOnClickListener(v -> {
            // Backup.recoveryDataFromStorage(file);
            callBack.call(file);
        });
    }

    @Override
    public int getItemCount() {
        return files.size();
    }


    public class BackUpViewHolder extends RecyclerView.ViewHolder {

        private TextView fileName, fileDate;

        public BackUpViewHolder(View itemView) {
            super(itemView);
            fileName = itemView.findViewById(R.id.item_backup_file_file_name_text_view);
            fileDate = itemView.findViewById(R.id.item_backup_file_file_date_text_view);
        }

    }
}
