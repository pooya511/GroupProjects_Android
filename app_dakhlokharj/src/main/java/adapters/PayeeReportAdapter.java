package adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import model.Currency;
import model.EnumsAndConstants;
import modules.PublicModules;
import utils.AppModules;

/**
 * Created by Parsa on 2018-01-03.
 */

public class PayeeReportAdapter extends RecyclerView.Adapter<PayeeReportAdapter.PayeeReportViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<ArrayList<String>> data;
    private TextView amount;

    public PayeeReportAdapter(Context context, ArrayList<ArrayList<String>> data) {
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    private void setTypeFaces() {
        amount.setTypeface(Application.amountTypeFace(context));
    }

    @Override
    public PayeeReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PayeeReportViewHolder(inflater.inflate(R.layout.item_expenses_or_income_list_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(PayeeReportViewHolder holder, int position) {

        holder.borderTop.setVisibility(View.VISIBLE);
        if (position == data.size())
            holder.borderBottom.setVisibility(View.VISIBLE);

        ArrayList<String> currentItem = data.get(position);

        setTypeFaces();

        holder.arrow.setVisibility(View.GONE);
        holder.description.setVisibility(View.GONE);

        holder.icon = AppModules.setTypeFace(context, holder.icon, "fontawesome_webfont.ttf");
        holder.icon.setText(AppModules.convertIconFromString(context, currentItem.get(3)));
        holder.icon.setTextColor(Color.parseColor(currentItem.get(4)));
        holder.date.setText(currentItem.get(2));
        holder.title.setText(currentItem.get(0));
        amount.setText(Currency.CurrencyString.getCurrencyString(PublicModules.toEnglishDigit(currentItem.get(1).replaceAll("\\D", "")), Currency.CurrencyMode.Short).separated);
        if (Integer.valueOf(currentItem.get(6)) == EnumsAndConstants.TransactionTypes.Expense.value())
            amount.setTextColor(context.getResources().getColor(R.color.red));
        if (Integer.valueOf(currentItem.get(6)) == EnumsAndConstants.TransactionTypes.Income.value())
            amount.setTextColor(context.getResources().getColor(R.color.green));


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PayeeReportViewHolder extends RecyclerView.ViewHolder {

        private TextView description, icon, date, title;
        private ImageView arrow;
        private View borderTop, borderBottom;

        public PayeeReportViewHolder(View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.expenses_list_activity_item_description_text);
            icon = itemView.findViewById(R.id.expenses_list_activity_item_icon);
            date = itemView.findViewById(R.id.expenses_list_activity_item_date_text);
            title = itemView.findViewById(R.id.expenses_list_activity_item_name_text);
            amount = itemView.findViewById(R.id.expenses_list_activity_item_amount_text);
            arrow = itemView.findViewById(R.id.expenses_list_activity_item_arrow_img_view);
            borderTop = itemView.findViewById(R.id.expenses_list_activity_item_top_border);
            borderBottom = itemView.findViewById(R.id.expenses_list_activity_item_bottom_border);
        }
    }

}
