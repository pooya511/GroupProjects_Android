package adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.AddAndEditExpensesAndIncomesActivity;
import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.ExpensesOrIncomeListActivity;
import com.taxiapps.dakhlokharj.MainActivity;
import com.taxiapps.dakhlokharj.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import at.markushi.ui.CircleButton;
import model.Currency;
import model.EnumsAndConstants;
import model.ExpenseOrIncomeItem;
import model.Group;
import model.GroupDetail;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;
import utils.DialogUtils;

public class ExpensesAndIncomesListAdapter extends BaseExpandableListAdapter {

    private ArrayList<HashMap<String, Object>> returnedData = new ArrayList<>(); // pak nashe ! NullPointer Mide moghe clear()
    private Context context;
    private long[] month;
    private int filterType;
    private EnumsAndConstants.TransactionTypes type;
    private ArrayList<Group> groups = new ArrayList<>();
    private ArrayList<ExpenseOrIncomeItem> groupDetails = new ArrayList<>();
    private ArrayList<Transaction> transactions = new ArrayList<>();
    private TextView itemAmount;

    private interface RemoveTrnCallBack {
        void call();
    }

    public ExpensesAndIncomesListAdapter(Context context, long[] month, int filterType, EnumsAndConstants.TransactionTypes type) {

        this.context = context;
        this.filterType = filterType;
        this.type = type;
        this.month = month;

        returnedData.clear();
        groupDetails.clear();
        groups.clear();
        transactions.clear();

        instance = this;
        returnedData = Transaction.getAll(String.valueOf(month[0]), String.valueOf(month[1]), type);
        initList();

    }

    private static ExpensesAndIncomesListAdapter instance = null;

    public static ExpensesAndIncomesListAdapter getInstance() {
        if (instance == null)
            if (ExpensesOrIncomeListActivity.getInstance() != null)
                return new ExpensesAndIncomesListAdapter(ExpensesOrIncomeListActivity.getInstance().context, ExpensesOrIncomeListActivity.getInstance().month, ExpensesOrIncomeListActivity.getInstance().filterValue, ExpensesOrIncomeListActivity.getInstance().type);
        return instance;
    }

    private void initList() {

        for (int i = 0; i < returnedData.size(); i++) {

            Group group = new Group();
            group.setID(Integer.valueOf(String.valueOf(returnedData.get(i).get(Group.GroupCol.id.value()))));
            group.setLstColor(String.valueOf(returnedData.get(i).get(Group.GroupCol.lstColor.value())));
            group.setLstIcon(String.valueOf(returnedData.get(i).get(Group.GroupCol.lstIcon.value())));
            if (filterType == 0) {
                group.setLstName(String.valueOf(returnedData.get(i).get(Group.GroupCol.lstName.value())));
            } else if (filterType == 1) {
                group.setLstName(PersianDateFormat.format(new PersianDate(Long.valueOf(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnDate.value())))), "Y/m/d"));
            }

            group.setLstOrder(Integer.valueOf(String.valueOf(returnedData.get(i).get(Group.GroupCol.lstOrder.value()))));

            ExpenseOrIncomeItem expenseOrIncomeItem = new ExpenseOrIncomeItem(
                    Integer.valueOf(String.valueOf(returnedData.get(i).get(GroupDetail.GroupDetailCol.id.value()))),
                    String.valueOf(returnedData.get(i).get(GroupDetail.GroupDetailCol.lsdName.value())),
                    Double.valueOf(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnAmount.value()))),
                    String.valueOf(returnedData.get(i).get(GroupDetail.GroupDetailCol.lsdIcon.value())),
                    String.valueOf(returnedData.get(i).get(GroupDetail.GroupDetailCol.lsdColor.value())),
                    String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnDate.value())),
                    String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnDescription.value())),
                    String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnColor.value())),
                    String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnImagePath.value()))
            );
            groupDetails.add(expenseOrIncomeItem);

            if (filterType == 0) {
                if (groups.size() > 0) {
                    getInstance().setChildren(group, expenseOrIncomeItem);
                    if (group.getLstName().equals(groups.get(groups.size() - 1).getLstName())) {
                        for (int j = 0; j < group.getChildren().size(); j++) {
                            groups.get(groups.size() - 1).getChildren().add(group.getChildren().get(j));
                        }
                    } else {
                        groups.add(group);
                    }
                } else {
                    getInstance().setChildren(group, expenseOrIncomeItem);
                    groups.add(group);
                }
            }
            if (filterType == 1) {
                if (groups.size() > 0) {
                    getInstance().setChildren(group, expenseOrIncomeItem);
                    if (group.getLstName().equals(groups.get(groups.size() - 1).getLstName())) {
                        for (int j = 0; j < group.getChildren().size(); j++) {
                            groups.get(groups.size() - 1).getChildren().add(group.getChildren().get(j));
                        }
                    } else {
                        groups.add(group);
                    }
                } else {
                    getInstance().setChildren(group, expenseOrIncomeItem);
                    groups.add(group);
                }

                Collections.sort(groups, (o1, o2) -> {
                    int o1Day = Integer.parseInt(o1.getLstName().replaceAll("\\D", ""));
                    int o2Day = Integer.parseInt(o2.getLstName().replaceAll("\\D", ""));
                    return Integer.compare(o1Day, o2Day);
                });
            }

            Transaction transaction = new Transaction();
            transaction.setID(Integer.valueOf(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.id.value()))));
            transaction.setAcnId(Integer.valueOf(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.acnID.value()))));
            transaction.setIsdID(Integer.valueOf(String.valueOf(returnedData.get(i).get(GroupDetail.GroupDetailCol.id.value()))));
            transaction.setPyeId(Integer.valueOf(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.pyeID.value()))));
            transaction.setTrnAmount(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnAmount.value())));
            transaction.setTrnColor(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnColor.value())));
            transaction.setTrnDate(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnDate.value())));
            transaction.setTrnDescription(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnDescription.value())));
            transaction.setTrnImagePath(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnImagePath.value())));
            transaction.setTrnType(Integer.valueOf(String.valueOf(returnedData.get(i).get(Transaction.TransactionCol.trnType.value()))));

            transactions.add(transaction);
        }

    }

    private void setChildren(Group group, ExpenseOrIncomeItem groupDetail) {
        ArrayList<ExpenseOrIncomeItem> children = new ArrayList<>();
        if (groupDetail.getID() == group.getID()) {
            children.add(groupDetail);
        }
        group.setChildren(children);
    }

    public void reloadData(EnumsAndConstants.TransactionTypes type) {

        returnedData.clear();
        groupDetails.clear();
        groups.clear();
        transactions.clear();

        try {
            returnedData = Transaction.getAll(String.valueOf(ExpensesOrIncomeListActivity.getInstance().month[0]), String.valueOf(ExpensesOrIncomeListActivity.getInstance().month[1]), ExpensesOrIncomeListActivity.getInstance().type);
            initList();
            getInstance().notifyDataSetChanged();
        } catch (NullPointerException e) {
            // Oskole tavajoh nakon
        }

        /*
         * for refreshing list fragment
         */

        FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
        if (fragmentManager.findFragmentByTag("expenseAndIncome") != null) {
            Fragment fragment = fragmentManager.findFragmentByTag("expenseAndIncome");
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.detach(fragment);
            fragmentTransaction.attach(fragment);
            if (!((Activity) context).isFinishing()) // age in khat nabashe mitune bugza bashe va crash (chi ?! kasi chizi goft ?!) kone
                fragmentTransaction.commitAllowingStateLoss();
        }

        /*
         * for update footer section of page
         */
        ExpensesOrIncomeListActivity.updateFooter(month[0], month[1], type);

    }

    private void setTypeFaces() {
        itemAmount.setTypeface(Application.amountTypeFace(context));
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).getChildren().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).getChildren().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groups.get(groupPosition).getID();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupDetails.get(childPosition).getID();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        Group group = (Group) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_header_expenses_or_income_list_layout, null);
        }
        double totalPrice = 0;

        for (int i = 0; i < transactions.size(); i++) {
            for (int j = 0; j < group.getChildren().size(); j++) {
                if (transactions.get(i).getIsdID() == group.getChildren().get(j).getID()) {
                    totalPrice += Double.valueOf(transactions.get(i).getTrnAmount());
                }
            }
        }

        TextView headerCategoryName, headerTotalAmount, headerCategoryIcon;
        headerCategoryName = convertView.findViewById(R.id.expenses_list_activity_header_category_name);
        headerTotalAmount = convertView.findViewById(R.id.expenses_list_activity_header_total_amount);
        headerCategoryIcon = convertView.findViewById(R.id.expenses_list_activity_header_category_icon);

        headerCategoryName.setText(group.getLstName());
        headerCategoryIcon = AppModules.setTypeFace(context, headerCategoryIcon, "fontawesome_webfont.ttf");

        if (filterType == 0) {
            headerCategoryIcon.setText(AppModules.convertIconFromString(context, group.getLstIcon()));
        } else if (filterType == 1) {
            headerCategoryIcon.setText(R.string.fa_calendar);
        }

        headerTotalAmount.setText(Currency.CurrencyString.getCurrencyString(totalPrice, Currency.CurrencyMode.Short).separated);
        Typeface typeface = Application.amountTypeFace(context);
        headerTotalAmount.setTypeface(typeface);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final ExpenseOrIncomeItem item = (ExpenseOrIncomeItem) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_expenses_or_income_list_layout, null);
        }

        TextView itemName, itemDate, itemIcon, itemDescription;
        ImageView cameraIcon;
        CircleButton itemTag;

        itemName = convertView.findViewById(R.id.expenses_list_activity_item_name_text);
        itemDate = convertView.findViewById(R.id.expenses_list_activity_item_date_text);
        itemIcon = convertView.findViewById(R.id.expenses_list_activity_item_icon);
        itemDescription = convertView.findViewById(R.id.expenses_list_activity_item_description_text);
        itemAmount = convertView.findViewById(R.id.expenses_list_activity_item_amount_text);
        cameraIcon = convertView.findViewById(R.id.expenses_list_activity_has_image_img);
        itemTag = convertView.findViewById(R.id.expenses_list_activity_tag);

        if (!item.getTrnColor().equals("#B8B8B8") && !item.getTrnColor().equals("null")) {
            itemTag.setVisibility(View.VISIBLE);
            itemTag.setColor(Color.parseColor(item.getTrnColor()));
        } else {
            itemTag.setVisibility(View.INVISIBLE);
        }

        setTypeFaces();

        PersianDate p = new PersianDate(Long.parseLong(item.getDate()));
        PersianDateFormat formatter = new PersianDateFormat("H:i Y/m/d");
        itemDate.setText(formatter.format(p));

        if (item.getTrnImagePath() != null || item.getTrnImagePath().equals("null") || item.getTrnImagePath().equals(""))
            cameraIcon.setVisibility(View.GONE);
        else
            cameraIcon.setVisibility(View.VISIBLE);

        if (!item.getDescription().equals("null") || item.getDescription().equals(""))
            itemDescription.setText(item.getDescription());

        if (type == EnumsAndConstants.TransactionTypes.Expense)
            itemAmount.setTextColor(context.getResources().getColor(R.color.red));
        if (type == EnumsAndConstants.TransactionTypes.Income)
            itemAmount.setTextColor(context.getResources().getColor(R.color.green));

        itemAmount.setText(Currency.CurrencyString.getCurrencyString(item.getAmount(), Currency.CurrencyMode.Short).separated);

        itemName.setText(item.getTitle());
        itemIcon = AppModules.setTypeFace(context, itemIcon, "fontawesome_webfont.ttf");
        itemIcon.setText(AppModules.convertIconFromString(context, item.getIcon()));
        itemIcon.setTextColor(Color.parseColor(item.getIconColor()));

        convertView.setOnClickListener(v -> {

            Intent intent = new Intent(context, AddAndEditExpensesAndIncomesActivity.class);
            if (type == EnumsAndConstants.TransactionTypes.Expense)
                intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.EditExpense.value());
            if (type == EnumsAndConstants.TransactionTypes.Income)
                intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.EditIncome.value());

            intent.putExtra("Item", item);
            intent.putExtra("CategoryName", groups.get(groupPosition).getLstName());
            context.startActivity(intent);

        });

        convertView.setOnLongClickListener(v -> {

            final Transaction transaction = Transaction.getTransaction(item.getID());

            removeTransactionDialog(context, () -> {
                if (transaction.getTrnImagePath() != null) {
                    File file = new File(transaction.getTrnImagePath());
                    if (file.exists())
                        file.delete();
                }

                transaction.delete();
                ExpensesAndIncomesListAdapter.getInstance().reloadData(type);
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
            }).show();

            return true;
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private Dialog removeTransactionDialog(Context context, final ExpensesAndIncomesListAdapter.RemoveTrnCallBack callBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View dlgLayout = inflater.inflate(R.layout.pop_remove_transaction, null);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 22;
                break;

            case 2:
                width = 55;
                height = 16;
                break;

            case 3:
                width = 45;
                height = 13;
                break;

        }


        final Dialog removeDlg = PublicDialogs.makeDialog(context, dlgLayout, width, height, false);

        TextView remove, cancel;
        remove = dlgLayout.findViewById(R.id.pop_remove_transaction_delete);
        cancel = dlgLayout.findViewById(R.id.pop_remove_transaction_cancel);

        remove.setOnClickListener(v -> {
            callBack.call();
            removeDlg.dismiss();
        });

        cancel.setOnClickListener(v -> removeDlg.dismiss());

        return removeDlg;
    }

}