package adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.AddAndEditExpensesAndIncomesActivity;
import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.R;
import com.taxiapps.dakhlokharj.SearchActivity;

import java.util.ArrayList;
import java.util.HashMap;

import model.Currency;
import model.EnumsAndConstants;
import model.ExpenseOrIncomeItem;
import model.Group;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;

public class SearchResultListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private HashMap<String, ArrayList<SearchActivity.SearchedTransactions>> data;

    private ArrayList<SearchResultItem> resArray = new ArrayList<>();
    private TextView amount;

    public SearchResultListAdapter(Context context, HashMap<String, ArrayList<SearchActivity.SearchedTransactions>> data) {
        this.context = context;
        this.data = data;
        initListView();
    }

    private void setTypeFace() {
        amount.setTypeface(Application.amountTypeFace(context));
    }

    private void initListView() {

        SearchResultItem byDesc = new SearchResultItem();
        SearchResultItem byBnk = new SearchResultItem();
        SearchResultItem byPye = new SearchResultItem();
        SearchResultItem byLsdName = new SearchResultItem();
        SearchResultItem byLstName = new SearchResultItem();
        SearchResultItem byAcnName = new SearchResultItem();

        byDesc.setTitle("توضیحات");
        byBnk.setTitle("نام بانک");
        byPye.setTitle("نام طرف حساب");
        byLsdName.setTitle("گروه اصلی");
        byLstName.setTitle("زیر گروه");
        byAcnName.setTitle("نام حساب");

        byDesc.setChildren(data.get("byTrnDescription"));
        byBnk.setChildren(data.get("byBankTitle"));
        byPye.setChildren(data.get("byPayeName"));
        byLsdName.setChildren(data.get("byLsdName"));
        byLstName.setChildren(data.get("byLstName"));
        byAcnName.setChildren(data.get("byAcnName"));

        resArray.add(byDesc);
        resArray.add(byBnk);
        resArray.add(byPye);
        resArray.add(byLsdName);
        resArray.add(byLstName);
        resArray.add(byAcnName);

    }

    @Override
    public int getGroupCount() {
        return resArray.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return resArray.get(groupPosition).getChildren().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return resArray.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return resArray.get(groupPosition).getChildren().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_header_search_list, null);

        TextView title = view.findViewById(R.id.item_header_search_list_header_search_title);
        TextView count = view.findViewById(R.id.item_header_search_list_header_search_child_count);

        SearchResultItem currentGroupItem = (SearchResultItem) getGroup(groupPosition);

        if (currentGroupItem.getChildren().get(0).getTrnDescription().equals("Empty")) {
            count.setText("0");
        } else {
            count.setText(String.valueOf(getChildrenCount(groupPosition)));
        }

        title.setText(resArray.get(groupPosition).getTitle());

        return view;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        SearchActivity.SearchedTransactions currentChildItem = (SearchActivity.SearchedTransactions) getChild(groupPosition, childPosition);

        if (!currentChildItem.getTrnDescription().equals("Empty")) {
            view = layoutInflater.inflate(R.layout.item_expenses_or_income_list_layout, null);

            TextView icon = view.findViewById(R.id.expenses_list_activity_item_icon);
            TextView title = view.findViewById(R.id.expenses_list_activity_item_name_text);
            TextView date = view.findViewById(R.id.expenses_list_activity_item_date_text);
            amount = view.findViewById(R.id.expenses_list_activity_item_amount_text);
            TextView description = view.findViewById(R.id.expenses_list_activity_item_description_text);

            description.setVisibility(View.GONE);

            icon = AppModules.setTypeFace(context, icon, "fontawesome_webfont.ttf");
            icon.setText(AppModules.convertIconFromString(context, resArray.get(groupPosition).getChildren().get(childPosition).getLsdIcon()));
            icon.setTextColor(Color.parseColor(resArray.get(groupPosition).getChildren().get(childPosition).getLsdColor()));

            title.setText(resArray.get(groupPosition).getChildren().get(childPosition).getLsdName());

            PersianDate p = new PersianDate(Long.parseLong(resArray.get(groupPosition).getChildren().get(childPosition).getTrnDate()));
            String dateFormatted = PersianDateFormat.format(p,"Y/m/d H:i");

            date.setText(dateFormatted);

            setTypeFace();

            amount.setText(Currency.CurrencyString.getCurrencyString(String.valueOf((int) resArray.get(groupPosition).getChildren().get(childPosition).getTrnAmount()), Currency.CurrencyMode.Short).separated);

            final Intent intent = new Intent(context, AddAndEditExpensesAndIncomesActivity.class);

            if (resArray.get(groupPosition).getChildren().get(childPosition).getTrnType() == EnumsAndConstants.TransactionTypes.Expense.value()) {
                intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.EditExpense.value());
                amount.setTextColor(context.getResources().getColor(R.color.red));
            }
            if (resArray.get(groupPosition).getChildren().get(childPosition).getTrnType() == EnumsAndConstants.TransactionTypes.Income.value()) {
                intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.EditIncome.value());
                amount.setTextColor(context.getResources().getColor(R.color.green));
            }

            view.setOnClickListener(v -> {
                ExpenseOrIncomeItem item = new ExpenseOrIncomeItem(
                        resArray.get(groupPosition).getChildren().get(childPosition).getId(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getLsdName(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getTrnAmount(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getLsdIcon(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getLsdColor(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getTrnDate(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getTrnDescription(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getTrnColor(),
                        resArray.get(groupPosition).getChildren().get(childPosition).getTrnImagePath()
                );
                intent.putExtra("Item", item);
                intent.putExtra("CategoryName", Group.getGroupNameWithLsdID(resArray.get(groupPosition).getChildren().get(childPosition).getLsdId()));
                context.startActivity(intent);
            });

        } else {
            view = layoutInflater.inflate(R.layout.item_empty_search, null);
        }
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class SearchResultItem {

        private String title;
        private ArrayList<SearchActivity.SearchedTransactions> children;

        public SearchResultItem() {
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public ArrayList<SearchActivity.SearchedTransactions> getChildren() {
            return children;
        }

        public void setChildren(ArrayList<SearchActivity.SearchedTransactions> children) {
            this.children = children;
        }
    }
}