package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.dakhlokharj.NewAndEditPayeeActivity;
import com.taxiapps.dakhlokharj.R;
import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;
import model.Payee;
import model.Settings;

/**
 * Created by Parsa on 2018-01-03.
 */

public class PayeeListAdapter extends DragItemAdapter<Payee, PayeeListAdapter.PayeeViewHolder> {

    private static Context context;
    private LayoutInflater inflater;
    private static ArrayList<Payee> payees;
    final int REQUEST_CODE_ADD_PAYEE = 1;

    /**
     * natunestim ba putExtra payee ro befrestim
     * tasmim gereftam ba ye object static az payee
     * in karo bokonam !
     */
    public static Payee targetPayee;

    public PayeeListAdapter(Context context , ArrayList<Payee> payees) {
        this.context = context;
        this.payees = payees;
        this.inflater = LayoutInflater.from(context);
        setItemList(payees);
    }

    public void refreshData() {
        payees = Payee.getAll();
        notifyDataSetChanged();
    }

    private void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    @Override
    public PayeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PayeeViewHolder(inflater.inflate(R.layout.item_payee, parent, false));
    }

    @Override
    public void onBindViewHolder(PayeeViewHolder holder, int position) {

        super.onBindViewHolder(holder, position);
        final Payee payee = payees.get(position);

        if (payee.getPyeIsActive() == 1) {
            holder.payeeStatus.setVisibility(View.GONE);
            holder.payeeName.setTextColor(Color.parseColor("#000000"));
        } else {
            holder.payeeStatus.setVisibility(View.VISIBLE);
            holder.payeeName.setTextColor(Color.parseColor("#a7a7a7"));
        }

        if (position == 0) {
            holder.topBorder.setVisibility(View.VISIBLE);
        }
        if (position + 1 == payees.size()) {
            setMargins(holder.bottomBorder, 0, 0, 0, 0);
        }

        holder.payeeName.setText(payee.getPyeName());

        holder.itemView.setOnClickListener(v -> {

            targetPayee = payee;

            Intent intent = new Intent(context, NewAndEditPayeeActivity.class);
            ((Activity) context).startActivityForResult(intent, REQUEST_CODE_ADD_PAYEE);

        });

    }

    @Override
    public long getUniqueItemId(int position) {
        return payees.get(position).getID();
    }

    @Override
    public int getItemCount() {
        return payees.size();
    }

    class PayeeViewHolder extends DragItemAdapter.ViewHolder {

        private View topBorder, bottomBorder;
        private TextView payeeName, payeeStatus;
        public PayeeViewHolder(View itemView) {
            super(itemView, R.id.item_payee_parent, true);
            topBorder = itemView.findViewById(R.id.item_payee_seperator2);
            bottomBorder = itemView.findViewById(R.id.item_payee_seperator);
            payeeName = itemView.findViewById(R.id.item_payee_payee_name);
            payeeStatus = itemView.findViewById(R.id.item_payee_payee_status);
        }
    }

}
