package adapters;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Parsa on 2017-11-13.
 */

public class PageAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    private PageAdapter(FragmentManager fm) {
        super(fm);
    }

    public static PageAdapter initInstance(FragmentManager fm, List<Fragment> fragments) {
        PageAdapter pageAdapter = new PageAdapter(fm);
        pageAdapter.fragments = fragments;
        return pageAdapter;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        if (position >= getCount()) {
            new Handler().post(() -> {
                FragmentManager manager = ((Fragment) object).getFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove((Fragment) object);
                trans.commitAllowingStateLoss();
            });
        }
    }

}
