package adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.HashMap;

import model.Currency;
import utils.AppModules;

/**
 * Created by Parsa on 2018-01-03.
 */

public class CategoryReportAdapter extends RecyclerView.Adapter<CategoryReportAdapter.CategoryViewHolder> {

    private Context context;
    private ArrayList<HashMap<String, Object>> arrayList;
    private LayoutInflater inflater;

    private TextView amount;

    public CategoryReportAdapter(Context context, ArrayList<HashMap<String, Object>> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        this.inflater = LayoutInflater.from(context);
    }

    private void setTypeFaces() {
        amount.setTypeface(Application.amountTypeFace(context));
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(inflater.inflate(R.layout.item_budget_or_report_by_category, parent, false));
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {

        setTypeFaces();

        holder.title.setText(String.valueOf(arrayList.get(position).get("lstName")));

        amount.setTextColor(context.getResources().getColor(R.color.red));
        amount.setText(Currency.CurrencyString.getCurrencyString(String.valueOf(arrayList.get(position).get("EX")), Currency.CurrencyMode.Short).separated);

        holder.icon = AppModules.setTypeFace(context, holder.icon, "fontawesome_webfont.ttf");
        holder.icon.setText(AppModules.convertIconFromString(context, String.valueOf(arrayList.get(position).get("lstIcon"))));
        holder.icon.setTextColor(Color.parseColor(String.valueOf(arrayList.get(position).get("lstColor"))));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView icon, title;
        public CategoryViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.item_budget_or_report_by_category_icon_textview);
            title = itemView.findViewById(R.id.item_budget_or_report_by_category_title_textview);
            amount = itemView.findViewById(R.id.item_budget_or_report_by_category_amount_textview);
        }
    }

}
