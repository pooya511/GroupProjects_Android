package adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Parsa on 2018-01-31.
 */

public class WalkThroughAdapter extends FragmentPagerAdapter {

    private ArrayList<android.support.v4.app.Fragment> fragments;

    public WalkThroughAdapter(android.support.v4.app.FragmentManager fm, ArrayList<android.support.v4.app.Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
