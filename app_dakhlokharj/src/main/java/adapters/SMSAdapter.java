package adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.AddAndEditExpensesAndIncomesActivity;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import model.Currency;
import model.EnumsAndConstants;
import model.GroupDetail;
import model.SMS;
import model.Transaction;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import utils.AppModules;

/**
 * Created by Parsa on 2018-01-04.
 */

public class SMSAdapter extends RecyclerView.Adapter<SMSAdapter.SMSViewHolder> {

    private Context context;
    private LayoutInflater inflater;

    public ArrayList<Transaction> transactionList;
    public int expensesCount;
    public int incomesCount;

    private static SMSAdapter instance;

    public static SMSAdapter getInstance() {
        return instance;
    }

    public SMSAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        ArrayList<SMS> smsList = (ArrayList<SMS>) SMS.readInbox(context, true);
        transactionList = new ArrayList<>();

        for (SMS sms : smsList) {

            if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان")) {
                Double price = Double.valueOf(sms.getPrice().replaceAll("\\D", "")) / 10;
                sms.setPrice(String.valueOf(price));
            }

            transactionList.add(new Transaction(sms.getPrice().replaceAll("\\D", ""), sms.getDate(), sms.getTransactionType()));

            if (sms.getTransactionType() == EnumsAndConstants.TransactionTypes.Expense.value()) {
                expensesCount++;
            }
            if (sms.getTransactionType() == EnumsAndConstants.TransactionTypes.Income.value()) {
                incomesCount++;
            }

        }
        instance = this;
    }

    @Override
    public SMSViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SMSViewHolder(inflater.inflate(R.layout.item_expenses_or_income_list_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(SMSViewHolder holder, final int position) {

        final Transaction transaction = transactionList.get(position);
        final GroupDetail groupDetail = GroupDetail.getGroupDetailByID(transaction.getIsdID());

        holder.itemIcon = AppModules.setTypeFace(context, holder.itemIcon, "fontawesome_webfont.ttf");

        if (groupDetail == null) {
            holder.itemName.setText("[ لطفا دسته بندی را انتخاب کنید ]");
            holder.itemIcon.setTextColor(Color.parseColor("#ff9900"));
            holder.itemIcon.setText(R.string.fa_question_circle_o);
        } else {
            holder.itemName.setText(groupDetail.getLsdName());
            holder.itemIcon.setTextColor(Color.parseColor(groupDetail.getLsdColor()));
            holder.itemIcon.setText(AppModules.convertIconFromString(context, groupDetail.getLsdIcon()));
        }

        if (transaction.getTrnType() == EnumsAndConstants.TransactionTypes.Expense.value())
            holder.itemAmount.setTextColor(Color.RED);
        if (transaction.getTrnType() == EnumsAndConstants.TransactionTypes.Income.value())
            holder.itemAmount.setTextColor(Color.GREEN);

        holder.itemAmount.setText(Currency.CurrencyString.getCurrencyString(transaction.getTrnAmount(), Currency.CurrencyMode.Full).separated);

        holder.itemDate.setText(PublicModules.timeStamp2String(new PersianDate(Long.valueOf(transaction.getTrnDate())).getTime(), "yyyy/MM/dd HH:mm"));

        holder.itemDescription.setText(transaction.getTrnDescription());

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, AddAndEditExpensesAndIncomesActivity.class);
            intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.SmsMode.value());
            intent.putExtra("position", position);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    public class SMSViewHolder extends RecyclerView.ViewHolder {

        private TextView itemName, itemDate, itemIcon, itemDescription, itemAmount;

        public SMSViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.expenses_list_activity_item_name_text);
            itemDate = itemView.findViewById(R.id.expenses_list_activity_item_date_text);
            itemIcon = itemView.findViewById(R.id.expenses_list_activity_item_icon);
            itemDescription = itemView.findViewById(R.id.expenses_list_activity_item_description_text);
            itemAmount = itemView.findViewById(R.id.expenses_list_activity_item_amount_text);
        }

    }

}