package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import model.Reports;

/**
 * Created by Parsa on 2018-01-03.
 */

public class MonthlyReportAdapter extends RecyclerView.Adapter<MonthlyReportAdapter.MonthlyReportViewHolder> {

    private ArrayList<Reports.MonthReport> dataArrayList;
    private Context context;
    private LayoutInflater inflater;

    private TextView expenseAmount;
    private TextView incomeAmount;

    public MonthlyReportAdapter(Context context, ArrayList<Reports.MonthReport> dataArrayList) {
        this.dataArrayList = dataArrayList;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    private void setTypeFaces() {
        expenseAmount.setTypeface(Application.amountTypeFace(context));
        incomeAmount.setTypeface(Application.amountTypeFace(context));
    }

    @Override
    public MonthlyReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MonthlyReportViewHolder(inflater.inflate(R.layout.item_report_by_month, parent, false));
    }

    @Override
    public void onBindViewHolder(MonthlyReportViewHolder holder, int position) {

        Reports.MonthReport monthReport = dataArrayList.get(position);

        setTypeFaces();

        holder.month.setText(monthReport.month);
        expenseAmount.setText(monthReport.expense);
        incomeAmount.setText(monthReport.income);
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    class MonthlyReportViewHolder extends RecyclerView.ViewHolder {

        private TextView month;
        public MonthlyReportViewHolder(View itemView) {
            super(itemView);
            month = itemView.findViewById(R.id.item_report_by_month_month_name);
            expenseAmount = itemView.findViewById(R.id.item_report_by_month_expense_amount_text_view);
            incomeAmount = itemView.findViewById(R.id.item_report_by_month_income_amount_text_view);
        }
    }

}
