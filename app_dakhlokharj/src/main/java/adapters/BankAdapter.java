package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import model.Bank;
import modules.PublicModules;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.BankViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Bank> bankList;
    private Context context;
    private Bank.BankCallback callback;

    public BankAdapter(Context context, ArrayList<Bank> bankList, Bank.BankCallback callback) {
        this.bankList = bankList;
        this.context = context;
        this.callback = callback;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public BankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_bank, parent, false);
        return new BankViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BankViewHolder holder, int position) {

        Bank bank = bankList.get(position);

        holder.bankName.setText(bank.getBnkTitle());
        PublicModules.stringToBitmap(bank.getBnkLogo(), holder.bankLogo);

    }

    @Override
    public int getItemCount() {
        return bankList.size();
    }

    public class BankViewHolder extends RecyclerView.ViewHolder {

        private TextView bankName;
        private ImageView bankLogo;
        private View seperator;

        public BankViewHolder(View itemView) {
            super(itemView);

            bankName = itemView.findViewById(R.id.item_bank_bank_name);
            bankLogo = itemView.findViewById(R.id.item_bank_bank_logo);
            seperator = itemView.findViewById(R.id.item_bank_seperator);

            itemView.setOnClickListener(v -> callback.call(bankList.get(getAdapterPosition())));

        }

    }

}
