package adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.dakhlokharj.FilterBankSmsActivity;
import com.taxiapps.dakhlokharj.R;

import java.util.LinkedHashSet;
import java.util.Set;

import model.Settings;

public class BankSmsNumberAdapter extends RecyclerView.Adapter<BankSmsNumberAdapter.BankSmsNumberViewHolder>{

    private Context context;
    private LayoutInflater layoutInflater;

    public BankSmsNumberAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public BankSmsNumberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_sms_number, parent, false);
        return new BankSmsNumberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BankSmsNumberViewHolder holder, final int position) {

        holder.smsNumber.setText(((FilterBankSmsActivity)context).numbersStringArray.get(position));

        if ((position + 1) == ((FilterBankSmsActivity)context).numbersStringArray.size())
            holder.deleteNumber.setVisibility(View.VISIBLE);
        else
            holder.bottomBorder.setVisibility(View.GONE);

        holder.deleteNumber.setOnClickListener(v -> {
            refreshData(((FilterBankSmsActivity)context).numbersStringArray.get(holder.getAdapterPosition()));
            if (((FilterBankSmsActivity)context).numbersStringArray.size() == 0)
                ((FilterBankSmsActivity)context).checkArrayItems(true);
            else
                ((FilterBankSmsActivity)context).checkArrayItems(false);
        });

        holder.itemView.setOnClickListener(v -> {
            Dialog addPreNumberDialog = ((FilterBankSmsActivity)context).addPreNumberDialog(context , ((FilterBankSmsActivity)context).numbersStringArray.get(position));
            addPreNumberDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
            addPreNumberDialog.show();
            ((FilterBankSmsActivity)context).arrayPosition = position;
        });

    }

    @Override
    public int getItemCount() {
        return ((FilterBankSmsActivity)context).numbersStringArray.size();
    }

    class BankSmsNumberViewHolder extends RecyclerView.ViewHolder{

        private TextView smsNumber , deleteNumber;
        private View bottomBorder;

        public BankSmsNumberViewHolder(View itemView) {
            super(itemView);

            smsNumber = itemView.findViewById(R.id.item_sms_number_number_text);
            deleteNumber = itemView.findViewById(R.id.item_sms_number_delete_text);
            bottomBorder = itemView.findViewById(R.id.item_sms_number_border_bottom);

        }
    }

    private void refreshData(String deleted){
        ((FilterBankSmsActivity)context).numbersStringArray.remove(deleted);
        Set<String> set = new LinkedHashSet<>(((FilterBankSmsActivity)context).numbersStringArray);
        Settings.setStringSet(Settings.SMS_NUMBERS, set);
        ((FilterBankSmsActivity)context).numbersStringArray = ((FilterBankSmsActivity)context).refreshArray();
        notifyDataSetChanged();
    }

}
