package adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.daimajia.swipe.SwipeLayout;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import dialog_fragments.CategoryDialogFragment;
import fragments.SelectCategory;
import model.Budget;
import model.Currency;
import model.EnumsAndConstants;
import model.Group;
import model.Transaction;
import modules.PublicModules;
import utils.AppModules;

/**
 * Created by Parsa on 2018-01-03.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Group> groupArray;
    private EnumsAndConstants.TransactionTypes type;
    private SelectCategory.SelectedCategoryCallBack categoryCallBack;
    public boolean showMinusIcon;

    private final int ADD_VIEW = 1;
    private final int NORMAL_VIEW = 2;

    public CategoryAdapter(Context context, EnumsAndConstants.TransactionTypes type, SelectCategory.SelectedCategoryCallBack categoryCallBack, ArrayList<Group> groupArray, boolean isEditMode) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.type = type;
        this.categoryCallBack = categoryCallBack;
        if (groupArray == null)
            this.groupArray = Group.getAll(type.value());
        else
            this.groupArray = groupArray;
        instance = this;
        showMinusIcon = isEditMode;
    }

    private static CategoryAdapter instance = null;

    public static CategoryAdapter getInstance() {
        if (instance == null) {
            try {
            } catch (Exception e) {
                //  Toast.makeText(context, "dadash yadet raft init koni classo categoryAdapter2", Toast.LENGTH_SHORT).show();
            }
        }
        return instance;
    }

    public void reloadData() {
        groupArray = Group.getAll(type.value());
        notifyDataSetChanged();
    }

    public void setFiltredData(String filterSymbol) {
        for (int i = 0; i < groupArray.size(); i++) {
            Group currGroup = groupArray.get(i);
            if (!currGroup.getLstName().contains(filterSymbol)) {
                groupArray.remove(currGroup);
                notifyItemRemoved(i);
            }
        }
    }

    public void editStart() {
        if (groupArray.size() > 0)
            if (!groupArray.get(0).getLstName().equals("new")) {
                groupArray.add(0, new Group("new"));
                showMinusIcon = true;
                SelectCategory.isEditMode = true;
                // minus.setVisibility(View.VISIBLE);
                SelectCategory.edit.setText("ذخیره");
                instance.notifyDataSetChanged();
            }
    }

    public void editEnd() {
        showMinusIcon = false;
        SelectCategory.isEditMode = false;
        if (groupArray.size() > 1)
            groupArray.remove(0);
        //  minus.setVisibility(View.GONE);
        SelectCategory.edit.setText("ویرایش");
        reloadData();
        instance.notifyDataSetChanged();
    }

    public void updateGroupOrder() {
        for (int i = 0; i < getInstance().getItemCount(); i++) {
            Group group = groupArray.get(i);
            group.setLstOrder(i);
            group.update(new String[]{Group.GroupCol.lstOrder.value()});
        }
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        if (groupArray.get(position).getLstName().equals("new"))
            return ADD_VIEW;
        else
            return NORMAL_VIEW;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ADD_VIEW)
            return new CategoryViewHolder(inflater.inflate(R.layout.item_edit_categories, parent, false));
        else
            return new CategoryViewHolder(inflater.inflate(R.layout.item_categories, parent, false));
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, final int position) {

        final Group group = groupArray.get(position);

        if (holder.getItemViewType() == NORMAL_VIEW) {

            if (position == 0)
                holder.topBorder.setVisibility(View.GONE);
            else
                holder.topBorder.setVisibility(View.VISIBLE);

            holder.title.setText(group.getLstName());
            holder.swipeLayout.setRightSwipeEnabled(false);

            holder.icon = AppModules.setTypeFace(context, holder.icon, "fontawesome_webfont.ttf");
            holder.icon.setText(AppModules.convertIconFromString(context, group.getLstIcon()));
            holder.icon.setTextColor(Color.parseColor(group.getLstColor()));

            if (showMinusIcon)
                holder.minus.setVisibility(View.VISIBLE);
            else
                holder.minus.setVisibility(View.GONE);

            holder.minus.setOnClickListener(v -> holder.swipeLayout.open(true));

            holder.removeLayout.setOnClickListener(new View.OnClickListener() {
                private Dialog removeDialog;

                @Override
                public void onClick(View v) {
                    removeDialog = CategoryDialogFragment.removeCategoriesAndSubCategories(context, () -> {
                        groupArray.remove(group);
                        group.delete();
                        notifyDataSetChanged();
                        removeDialog.dismiss();
                    });
                    removeDialog.show();
                }
            });

            double budget = Double.valueOf(Budget.getSumAll(group.getID()));
            double used = Double.valueOf(Transaction.getSumThisMonth(group.getID(), type));

            if (budget > 0 && type.value() == 1) {

                holder.remainingAmount.setVisibility(View.VISIBLE);

                if (used > budget) {
                    holder.remainingAmount.setText(Currency.CurrencyString.getCurrencyString(budget - used, null).separated + " بیشتر");
                    holder.remainingAmount.setTextColor(Color.RED);
                } else {
                    holder.remainingAmount.setText("مانده بودجه: " + Currency.CurrencyString.getCurrencyString(String.valueOf(budget - used), Currency.CurrencyMode.Short).separated);
                }
            }

            if (budget != 0) {
                if ((int) ((used / budget) * 100) > 100) {
                    holder.progressBar.setProgress(100);
                } else {
                    holder.progressBar.setProgress((int) ((used / budget) * 100));
                }
                holder.progressBar.setReachedBarColor(Color.parseColor(detectProgressColor((int) ((used / budget) * 100))));
                holder.progressBar.setProgressTextColor(Color.parseColor(detectProgressColor((int) ((used / budget) * 100))));
            } else
                holder.progressBar.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(v -> categoryCallBack.call(group));

    }

    @Override
    public int getItemCount() {
        return groupArray.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView remainingAmount, title, icon;
        private NumberProgressBar progressBar;
        private SwipeLayout swipeLayout;
        private LinearLayout removeLayout;
        private ImageView minus;
        private View topBorder;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            remainingAmount = itemView.findViewById(R.id.item_categories_remaining_budget_text_view);
            title = itemView.findViewById(R.id.item_categories_title);
            icon = itemView.findViewById(R.id.item_categories_icon);
            minus = itemView.findViewById(R.id.item_categories_ic_delete);
            progressBar = itemView.findViewById(R.id.item_categories_remaining_budget_progressbar);
            swipeLayout = itemView.findViewById(R.id.item_categories_swipe_layout);
            removeLayout = itemView.findViewById(R.id.item_categories_remove_layout);
            topBorder = itemView.findViewById(R.id.item_categories_top_border);
        }
    }

    private String detectProgressColor(int percent) {
        String color = "";
        if (percent >= 0 && percent < 65)
            color = "#009624";
        if (percent >= 65 && percent < 90)
            color = "#ff8f00";
        if (percent >= 90)
            color = "#ff0000";
        return color;
    }
}
