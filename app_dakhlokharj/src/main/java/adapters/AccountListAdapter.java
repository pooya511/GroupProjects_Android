package adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.dakhlokharj.AccountListActivity;
import com.taxiapps.dakhlokharj.AccountToAccountActivity;
import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.DetailAccountActivity;
import com.taxiapps.dakhlokharj.NewAndEditAccountActivity;
import com.taxiapps.dakhlokharj.R;
import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import model.Account;
import model.Bank;
import model.Currency;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;

/**
 * Created by Parsa on 2018-01-02.
 */

public class AccountListAdapter extends DragItemAdapter<Account, AccountListAdapter.AccountViewHolder> {

    private static Context context;
    private LayoutInflater inflater;
    public ArrayList<Account> accounts;
    private ArrayList<Transaction> transactions;
    private double itemAccountBalance;

    public int lastPosition = -1;

    public AccountListAdapter(Context context, ArrayList<Account> accounts) {
        this.context = context;
        this.accounts = accounts;
        this.inflater = LayoutInflater.from(context);

        for (int i = 0; i < accounts.size(); i++) {
            accounts.get(i).setExpanded(false);
        }
        setItemList(accounts);
    }

    public void refreshData() {
        HashMap<String, String> map = Settings.loadMap(Settings.ACCOUNT_ORDER_MAP);
        accounts = Account.sortAccountsOrder(Account.getAll(), map);
        setItemList(accounts);
        notifyDataSetChanged();
    }

    @Override
    public AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AccountViewHolder(inflater.inflate(R.layout.item_account, parent, false));
    }

    @Override
    public void onBindViewHolder(AccountViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        final Account account = accounts.get(position);

        if (account.isExpanded) {
            holder.expandedItem.setVisibility(View.VISIBLE);
        } else {
            holder.expandedItem.setVisibility(View.GONE);
        }

        if (account.getAcnIsActive() == 1) {
            holder.accountState.setVisibility(View.GONE);
        } else {
            holder.accountState.setVisibility(View.VISIBLE);
        }

        PublicModules.stringToBitmap(Bank.getBankWithID(account.getAcnIcon()).getBnkLogo(), holder.accountIc);

        holder.accountName.setText(account.getAcnName());

        itemAccountBalance = Double.parseDouble(account.getAcnBalance());
        transactions = Account.getAllTransaction(account.getID());

        if (transactions.size() == 0) {
            if (itemAccountBalance > 0) {
                holder.accountAmount.setText(Currency.CurrencyString.getCurrencyString(itemAccountBalance, Currency.CurrencyMode.Short).separated);
                holder.accountAmount.setTextColor(Color.parseColor("#71D009"));
            } else if (itemAccountBalance == 0) {
                holder.accountAmount.setText(Currency.CurrencyString.getCurrencyString(itemAccountBalance, Currency.CurrencyMode.Short).separated);
                holder.accountAmount.setTextColor(Color.parseColor("#000000"));
            }

        } else {

            for (int i = 0; i < transactions.size(); i++) {
                double trnAmount = Double.parseDouble(transactions.get(i).getTrnAmount());
                if (transactions.get(i).getTrnType() == 1 || transactions.get(i).getTrnType() == 3) {
                    itemAccountBalance = itemAccountBalance - trnAmount;
                } else {
                    itemAccountBalance = itemAccountBalance + trnAmount;
                }

            }
            if (itemAccountBalance > 0) {
                holder.accountAmount.setText(Currency.CurrencyString.getCurrencyString(itemAccountBalance, Currency.CurrencyMode.Short).separated);
                holder.accountAmount.setTextColor(Color.parseColor("#71D009"));
            } else if (itemAccountBalance < 0) {
                holder.accountAmount.setText("منفی ".concat(Currency.CurrencyString.getCurrencyString(Math.abs(itemAccountBalance), Currency.CurrencyMode.Short).separated));
                holder.accountAmount.setTextColor(Color.parseColor("#FF0000"));
            } else if (itemAccountBalance == 0) {
                holder.accountAmount.setText(Currency.CurrencyString.getCurrencyString("0", Currency.CurrencyMode.Short).separated);
                holder.accountAmount.setTextColor(Color.parseColor("#000000"));
            }
        }

        holder.accountNumber.setText(account.getAcnAccount());
        holder.cardNumber.setText(account.getAcnCard());
        holder.shabaNumber.setText(account.getAcnShaba());

        if (position + 1 == accounts.size()) {
            holder.bottomBorder.setVisibility(View.VISIBLE);
        } else {
            holder.bottomBorder.setVisibility(View.GONE);
        }

        holder.deleteAccount.setOnClickListener(v -> {
            Dialog removeAccount = removeAccount(context, account);
            removeAccount.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
            removeAccount.show();
        });

        holder.detailAccount.setOnClickListener(v -> {
            Intent intent = new Intent(context, DetailAccountActivity.class);
            intent.putExtra("accountId", account.getID());
            intent.putExtra("accountName", account.getAcnName());
            context.startActivity(intent);
        });

        holder.accountToAccount.setOnClickListener(v -> {
            Intent intent = new Intent(context, AccountToAccountActivity.class);
            intent.putExtra("accountID", account.getID());
            ((Activity) context).startActivityForResult(intent, AccountListActivity.REQUEST_TRANSFER_ACCOUNT);
        });

        holder.itemView.setOnClickListener(v -> {
            Account currentAccount = accounts.get(position);
            currentAccount.setExpanded(true);
            notifyItemChanged(position);

            if (lastPosition != -1 && lastPosition != position && lastPosition < getItemCount()) {
                Account lastAccount = accounts.get(lastPosition);
                lastAccount.setExpanded(false);
                notifyItemChanged(lastPosition);
            }

            if (lastPosition == position) {
                AccountListActivity.targetAccount = accounts.get(position);

                Intent intent = new Intent(context, NewAndEditAccountActivity.class);
                ((Activity) context).startActivityForResult(intent, AccountListActivity.REQUEST_CODE_ADD_ACCOUNT);
            }

            lastPosition = position;
        });

    }

    @Override
    public long getUniqueItemId(int position) {
        return accounts.get(position).getID();
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }

    class AccountViewHolder extends DragItemAdapter.ViewHolder {

        private ImageView accountIc, deleteAccount;
        private ConstraintLayout expandedItem;
        private TextView accountName, accountNumber, cardNumber, shabaNumber, accountState, accountAmount, detailAccount, accountToAccount;
        //        private TextView accountCreateDate;
        private View bottomBorder;

        public AccountViewHolder(View itemView) {
            super(itemView, R.id.item_account_parent, true);
            accountIc = itemView.findViewById(R.id.item_account_img);
            deleteAccount = itemView.findViewById(R.id.item_account_delete_img);
            accountName = itemView.findViewById(R.id.item_account_name_text_view);
            expandedItem = itemView.findViewById(R.id.item_account_expanded_layout);
            accountNumber = itemView.findViewById(R.id.item_account_account_number_text_view);
            cardNumber = itemView.findViewById(R.id.item_account_card_number_text_view);
            shabaNumber = itemView.findViewById(R.id.item_account_shaba_number_text_view);
            //accountCreateDate = itemView.findViewById(R.id.item_account_account_create_date_text_view);
            accountState = itemView.findViewById(R.id.item_account_stat_text_view);
            accountAmount = itemView.findViewById(R.id.item_account_amount_text_view);
            detailAccount = itemView.findViewById(R.id.item_account_show_sub_account_text_view);
            bottomBorder = itemView.findViewById(R.id.item_account_bottom_border);
            accountToAccount = itemView.findViewById(R.id.item_account_account_to_account_text_view);
            accountAmount.setTypeface(Application.amountTypeFace(context));
        }

    }

    private Dialog removeAccount(final Context context, final Account account) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View removeAccount = inflater.inflate(R.layout.pop_remove_payee, null);

        final TextView removePayee, cancel, title, desc;
        removePayee = removeAccount.findViewById(R.id.pop_remove_payee_reset);
        cancel = removeAccount.findViewById(R.id.pop_remove_payee_cancel);
        title = removeAccount.findViewById(R.id.pop_remove_payee_title);
        desc = removeAccount.findViewById(R.id.pop_remove_payee_description);

        title.setText("حذف حساب");
        desc.setText("تمام تراکنش های مرتبط با این حساب هم حذف می شوند!");

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 84;
                height = 21;
                break;
            case 2:
                width = 70;
                height = 16;
                break;
            case 3:
                width = 55;
                height = 13;
                break;
        }

        final Dialog resetDataDialog = PublicDialogs.makeDialog(context, removeAccount, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel))
                resetDataDialog.dismiss();

            if (v.equals(removePayee)) {
                account.delete();
                HashMap<String, String> map = Settings.loadMap(Settings.ACCOUNT_ORDER_MAP);
                map.remove(String.valueOf(account.getID()));
                Settings.saveMap(Settings.ACCOUNT_ORDER_MAP, map);
                resetDataDialog.dismiss();
                refreshData();
                AccountListActivity.getInstance().checkListContent();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        removePayee.setOnClickListener(dialogListeners);

        return resetDataDialog;
    }

}