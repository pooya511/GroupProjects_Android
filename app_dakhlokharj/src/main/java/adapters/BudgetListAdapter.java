package adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.ExpensesBudgetActivity;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import model.Budget;
import model.Currency;
import utils.AppModules;
import utils.DialogUtils;

/**
 * Created by Parsa on 2018-01-02.
 */

public class BudgetListAdapter extends RecyclerView.Adapter<BudgetListAdapter.BudgetViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Budget> returnedData;

    public BudgetListAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        returnedData = Budget.getAllByList();
    }

    @Override
    public BudgetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BudgetViewHolder(inflater.inflate(R.layout.item_budget_or_report_by_category, parent, false));
    }

    @Override
    public void onBindViewHolder(final BudgetViewHolder holder, int position) {

        final Budget budget = returnedData.get(position);

        holder.title.setText(budget.lstName);

        holder.amount.setText(Currency.CurrencyString.getCurrencyString(budget.getBudAmount(), Currency.CurrencyMode.Short).separated);
        holder.amount.setTextColor(Color.parseColor("#3090ff"));

        holder.icon = AppModules.setTypeFace(context, holder.icon, "fontawesome_webfont.ttf");
        holder.icon.setTextColor(Color.parseColor(String.valueOf(budget.lstColor)));
        holder.icon.setText(AppModules.convertIconFromString(context, String.valueOf(budget.lstIcon)));

        holder.selectAmount.setOnClickListener(v -> DialogUtils.keypadDialog(context, holder.amount.getText().toString().replaceAll("\\D", ""), number -> {
            holder.amount.setText(Currency.CurrencyString.getCurrencyString(number, Currency.CurrencyMode.Short).separated);
            budget.setBudAmount(number.replaceAll("\\D", ""));
            budget.update(new String[]{Budget.BudgetCol.budAmount.value()});
            ExpensesBudgetActivity.reloadData();
        }).show());

    }

    @Override
    public int getItemCount() {
        return returnedData.size();
    }

    public class BudgetViewHolder extends RecyclerView.ViewHolder {

        private TextView title, icon, amount;
        private ConstraintLayout selectAmount;

        public BudgetViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_budget_or_report_by_category_title_textview);
            icon = itemView.findViewById(R.id.item_budget_or_report_by_category_icon_textview);
            selectAmount = itemView.findViewById(R.id.item_budget_or_report_by_category_select_amount_layout);
            amount = itemView.findViewById(R.id.item_budget_or_report_by_category_amount_textview);
            amount.setTypeface(Application.amountTypeFace(context));
        }
    }

}
