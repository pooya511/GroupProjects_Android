package dialog_fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.taxiapps.dakhlokharj.AddAndEditExpensesAndIncomesActivity;
import com.taxiapps.dakhlokharj.R;

import adapters.CategoryAdapter;
import adapters.SubCategoryAdapter;
import fragments.AddOrEditCategories;
import fragments.SelectCategory;
import fragments.SelectSubCategory;
import model.EnumsAndConstants;
import model.Group;
import model.GroupDetail;
import modules.PublicDialogs;
import modules.PublicModules;

/**
 * Created by Parsa on 2017-12-13.
 */

public class CategoryDialogFragment extends DialogFragment {

    private EnumsAndConstants.TransactionTypes type;
    private SelectedCategoriesByUserCallBack callBack;

    private FrameLayout fragmentContainer;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    public interface RemoveCategoriesCallBack {
        void call();
    }

    public interface SelectedCategoriesByUserCallBack extends Parcelable {
        void call(Group group, GroupDetail groupDetail);
    }

    public static CategoryDialogFragment newInstance(EnumsAndConstants.TransactionTypes type, SelectedCategoriesByUserCallBack callBack) {
        Bundle args = new Bundle();
        args.putInt("Type", type.value());
        args.putParcelable("CallBack", callBack);
        CategoryDialogFragment fragment = new CategoryDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        Window dialogWindow = getDialog().getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(false);
        int width;
        int height;
        switch (PublicModules.screenSize(getActivity())) {
            case 1:
                width = 80;
                height = 75;
                break;
            case 2:
                width = 65;
                height = 65;
                break;
            case 3:
                width = 65;
                height = 65;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }

        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();

        int ScreenSizeResults[] = PublicModules.getWidthAndHeight(getActivity(), width, height);
        lp.width = ScreenSizeResults[0];
        lp.height = ScreenSizeResults[1];

        dialogWindow.setAttributes(lp);
    }

    @SuppressLint("ResourceType")
    public void initFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.start_slide_left, R.anim.end_slide_left);
        fragmentTransaction.add(fragmentContainer.getId(), fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        SelectCategory.isEditMode = false;
        if (AddAndEditExpensesAndIncomesActivity.progressBar != null) {
            AddAndEditExpensesAndIncomesActivity.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        type = getArguments().getInt("Type") == 1 ? EnumsAndConstants.TransactionTypes.Expense : EnumsAndConstants.TransactionTypes.Income;
        callBack = getArguments().getParcelable("CallBack");

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View mainLayout = inflater.inflate(R.layout.frg_select_category, null);

        fragmentManager = getChildFragmentManager();

        fragmentContainer = mainLayout.findViewById(R.id.frg_select_category_container);
        ConstraintLayout cancelCons = mainLayout.findViewById(R.id.frg_select_cancel_layout);
        cancelCons.setOnClickListener(v -> dismiss());

        initFragment(SelectCategory.newInstance(type, new SelectCategory.SelectedCategoryCallBack() {
            @Override
            public void call(final Group group) {
                if (fragmentManager.getBackStackEntryCount() <= 1) {
                    if (CategoryAdapter.getInstance().showMinusIcon) {

                        initFragment(AddOrEditCategories.newInstance(new AddOrEditCategories.CategoryItem(group.getID(), group.getLstName(), group.getLstIcon(), group.getLstColor(), group.getLstOrder(), type.value(), AddOrEditCategories.CategoryItem.CategoryItemType.CATEGORY)));

                    } else {

                        initFragment(SelectSubCategory.newInstance(group, new SelectSubCategory.SelectedSubCategoryCallBack() {
                            @Override
                            public void call(GroupDetail groupDetail) {

                                if (SubCategoryAdapter.getInstance().showMinusIcon) {

                                    initFragment(AddOrEditCategories.newInstance(new AddOrEditCategories.CategoryItem(groupDetail.getID(), group.getID(), groupDetail.getLsdName(), groupDetail.getLsdIcon(), groupDetail.getLsdColor(), AddOrEditCategories.CategoryItem.CategoryItemType.SUBCATEGORY)));

                                } else {

                                    callBack.call(group, groupDetail);
                                    dismiss();

                                }

                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }

                        }));

                    }

                }
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {

            }
        }));

        return mainLayout;
    }

    public static Dialog removeCategoriesAndSubCategories(Context context, final RemoveCategoriesCallBack categoriesCallBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View passwordLayout = inflater.inflate(R.layout.pop_remove_categories_and_subcategories, null);

        final ConstraintLayout remove, cancel;
        remove = passwordLayout.findViewById(R.id.pop_remove_categories_remove);
        cancel = passwordLayout.findViewById(R.id.pop_remove_categories_cancel);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 80;
                height = 30;
                break;

            case 2:
                width = 65;
                height = 24;
                break;

            case 3:
                width = 55;
                height = 20;
                break;

        }

        final Dialog resDialog = PublicDialogs.makeDialog(context, passwordLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(remove))
                categoriesCallBack.call();

            if (v.equals(cancel))
                resDialog.dismiss();

        };

        remove.setOnClickListener(dialogListeners);
        cancel.setOnClickListener(dialogListeners);
        return resDialog;
    }

}