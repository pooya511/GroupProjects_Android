package dialog_fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.R;

import fragments.SelectByDayFragment;
import fragments.SelectBySelectingFragment;
import fragments.SelectByTyping;
import model.Settings;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import utils.AppModules;

public class DatePicker extends DialogFragment {

    private String focusedFragment;

    private DatePickerCallBack callBack;
    private static Fragment selectByDay, selectBySelect, selectByType;
    private PersianDate pCalendar;

    public interface DatePickerCallBack extends Parcelable {
        void call(PersianDate persianCalendar);
    }

    public static DatePicker newInstance(long currentDate, DatePickerCallBack callBack) {

        Bundle args = new Bundle();
        args.putParcelable("CallBack", callBack);
        args.putLong("CurrentDate", currentDate);
        DatePicker fragment = new DatePicker();
        fragment.setArguments(args);
        return fragment;

    }

    private void initFragment(PersianDate persianCalendar) {
        selectByDay = SelectByDayFragment.newInstance(persianCalendar.getTime());
        selectBySelect = SelectBySelectingFragment.newInstance(persianCalendar.getTime());
        selectByType = SelectByTyping.newInstance(persianCalendar.getTime());
    }

    @Override
    public void onStart() {
        super.onStart();

        Window dialogWindow = getDialog().getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(false);
        int width;
        int height;
        switch (PublicModules.screenSize(getActivity())) {
            case 1:
                width = 80;
                height = 44;
                break;
            case 2:
                width = 68;
                height = 33;
                break;
            case 3:
                width = 60;
                height = 37;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }

        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        //
        int ScreenSizeResults[] = PublicModules.getWidthAndHeight(getActivity(), width, height);
        lp.width = ScreenSizeResults[0];
        lp.height = ScreenSizeResults[1];

        dialogWindow.setAttributes(lp);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        callBack = getArguments().getParcelable("CallBack");

        if (getArguments().getLong("CurrentDate") == 0)
            pCalendar = new PersianDate();
        else
            pCalendar = new PersianDate(getArguments().getLong("CurrentDate"));

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View datePickerLayout = inflater.inflate(R.layout.pop_date_picker, null);

        final FrameLayout fragmentFrame = datePickerLayout.findViewById(R.id.pop_date_picker_main_fragment_container);

        final TextView byDay, bySelect, byType, ok, cancel, todayDate;
        byDay = datePickerLayout.findViewById(R.id.pop_date_picker_by_day);
        bySelect = datePickerLayout.findViewById(R.id.pop_date_picker_by_select);
        byType = datePickerLayout.findViewById(R.id.pop_date_picker_by_typing);
        ok = datePickerLayout.findViewById(R.id.pop_date_picker_ok);
        cancel = datePickerLayout.findViewById(R.id.pop_date_picker_cancel);
        todayDate = datePickerLayout.findViewById(R.id.pop_time_picker_select_today);

        final TextView[] headerBtns = new TextView[3];
        headerBtns[0] = byDay;
        headerBtns[1] = bySelect;
        headerBtns[2] = byType;

        final FragmentManager fm = getChildFragmentManager();

        initFragment(pCalendar);

        // default datePicker type Start
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        switch (Settings.getSetting(Settings.LAST_DATEPICKER_TYPE)) {

            case "byDay":
                fragmentTransaction.replace(fragmentFrame.getId(), selectByDay);
                focusedFragment = "byDay";
                handleButtonTextColors(byDay, headerBtns);
                byDay.setBackgroundResource(R.drawable.sh_date_picker_by_day_selected);
                bySelect.setBackgroundResource(R.drawable.sh_date_picker_by_select_default);
                byType.setBackgroundResource(R.drawable.sh_date_picker_by_typing_default);
                break;

            case "bySelect":
                fragmentTransaction.replace(fragmentFrame.getId(), selectBySelect);
                focusedFragment = "bySelect";
                handleButtonTextColors(bySelect, headerBtns);
                bySelect.setBackgroundResource(R.drawable.sh_date_picker_by_select_selected);
                byDay.setBackgroundResource(R.drawable.sh_date_picker_by_day_default);
                byType.setBackgroundResource(R.drawable.sh_date_picker_by_typing_default);
                break;

            case "byType":
                fragmentTransaction.replace(fragmentFrame.getId(), selectByType);
                focusedFragment = "byType";
                handleButtonTextColors(byType, headerBtns);
                byType.setBackgroundResource(R.drawable.sh_date_picker_by_typing_selected);
                byDay.setBackgroundResource(R.drawable.sh_date_picker_by_day_default);
                bySelect.setBackgroundResource(R.drawable.sh_date_picker_by_select_default);
                break;

            case "":
                fragmentTransaction.replace(fragmentFrame.getId(), selectBySelect);
                focusedFragment = "bySelect";
                handleButtonTextColors(bySelect, headerBtns);
                bySelect.setBackgroundResource(R.drawable.sh_date_picker_by_select_selected);
                byDay.setBackgroundResource(R.drawable.sh_date_picker_by_day_default);
                byType.setBackgroundResource(R.drawable.sh_date_picker_by_typing_default);
                break;

        }

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        // default datePicker type End

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(byDay)) {

                focusedFragment = "byDay";

                handleButtonTextColors(byDay, headerBtns);
                byDay.setBackgroundResource(R.drawable.sh_date_picker_by_day_selected);
                bySelect.setBackgroundResource(R.drawable.sh_date_picker_by_select_default);
                byType.setBackgroundResource(R.drawable.sh_date_picker_by_typing_default);

                FragmentTransaction fragmentTransaction1 = fm.beginTransaction();

                fragmentTransaction1.replace(fragmentFrame.getId(), selectByDay);
                fragmentTransaction1.addToBackStack(null);
                fragmentTransaction1.commit();

            }

            if (v.equals(bySelect)) {

                focusedFragment = "bySelect";

                handleButtonTextColors(bySelect, headerBtns);
                bySelect.setBackgroundResource(R.drawable.sh_date_picker_by_select_selected);
                byDay.setBackgroundResource(R.drawable.sh_date_picker_by_day_default);
                byType.setBackgroundResource(R.drawable.sh_date_picker_by_typing_default);

                FragmentTransaction fragmentTransaction1 = fm.beginTransaction();

                fragmentTransaction1.replace(fragmentFrame.getId(), selectBySelect);
                fragmentTransaction1.addToBackStack(null);
                fragmentTransaction1.commit();

            }

            if (v.equals(byType)) {

                focusedFragment = "byType";

                handleButtonTextColors(byType, headerBtns);
                byType.setBackgroundResource(R.drawable.sh_date_picker_by_typing_selected);
                byDay.setBackgroundResource(R.drawable.sh_date_picker_by_day_default);
                bySelect.setBackgroundResource(R.drawable.sh_date_picker_by_select_default);

                FragmentTransaction fragmentTransaction1 = fm.beginTransaction();

                fragmentTransaction1.replace(fragmentFrame.getId(), selectByType);
                fragmentTransaction1.addToBackStack(null);
                fragmentTransaction1.commit();

            }

            if (v.equals(ok)) {

                switch (focusedFragment) {

                    case "byDay":

                        callBack.call(SelectByDayFragment.getDate());
                        Settings.setSetting(Settings.LAST_DATEPICKER_TYPE, focusedFragment);
                        dismiss();

                        break;

                    case "bySelect":

                        callBack.call(SelectBySelectingFragment.getDate());
                        Settings.setSetting(Settings.LAST_DATEPICKER_TYPE, focusedFragment);
                        dismiss();
                        break;

                    case "byType":

                        Settings.setSetting(Settings.LAST_DATEPICKER_TYPE, focusedFragment);
                        if (SelectByTyping.getDate(getActivity()) != null)
                            callBack.call(SelectByTyping.getDate(getActivity()));
                        dismiss();
                        break;

                }

            }

            if (v.equals(todayDate)) {

                switch (focusedFragment) {

                    case "byDay":
                        SelectByDayFragment.setWheelDatePicker(new PersianDate().getTime());
                        break;

                    case "bySelect":
                        SelectBySelectingFragment.setDate(new PersianDate().getTime());
                        break;

                    case "byType":
                        SelectByTyping.setDate(new PersianDate().getTime());
                        break;
                }
            }

            if (v.equals(cancel))
                dismiss();


        };

        byDay.setOnClickListener(dialogListeners);
        bySelect.setOnClickListener(dialogListeners);
        byType.setOnClickListener(dialogListeners);
        ok.setOnClickListener(dialogListeners);
        cancel.setOnClickListener(dialogListeners);
        todayDate.setOnClickListener(dialogListeners);

        return datePickerLayout;
    }

    private static void handleButtonTextColors(TextView v, TextView[] views) {

        for (int i = 0; i < views.length; i++) {

            if (views[i].equals(v))
                v.setTextColor(Color.WHITE);
            else
                views[i].setTextColor(Color.parseColor("#009688"));

        }

    }

}