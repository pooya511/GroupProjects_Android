package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import model.EnumsAndConstants;
import utils.AppModules;

public class ReportsActivity extends BaseActivity {

    private TextView tipsIcon, tipsText;
    private ConstraintLayout smartReportLayout, payeesReportLayout, yearlyReportLayout, monthlyReportLayout, rangeReportLayout, categoryReportLayout, summaryReportLayout;

    public static final ArrayList<String> tipsArray = new ArrayList<>(Arrays.asList(
            "با استفاده از وسایل نقلیه به سلامت محیط زیست کمک کنیم",
            "با کم کردن فست فود ها به سلامت خود کمک کنیم",
            "سفر به اعماق طبیعت آرامش بخش و مفرح است",
            "ورزش برای سلامتی روح و جسم ضروری است",
            "کمک مستمر به خیریه ها معیشت عمومی را تامین می کند",
            "کنترل هزینه ها می تواند به صرفه جویی کمک کند"
    ));

    public static String[][] arrayImages = new String[6][2];

    private Object[][] getRandomTip() {

        Object[][] result = new Object[1][2];

        arrayImages[0][0] = ReportsActivity.this.getResources().getString(R.string.fa_car);
        arrayImages[0][1] = "#040A34";

        arrayImages[1][0] = ReportsActivity.this.getResources().getString(R.string.fa_cutlery);
        arrayImages[1][1] = "#C0E652";

        arrayImages[2][0] = ReportsActivity.this.getResources().getString(R.string.fa_plane);
        arrayImages[2][1] = "#26d029";

        arrayImages[3][0] = ReportsActivity.this.getResources().getString(R.string.fa_futbol_o);
        arrayImages[3][1] = "#FF9000";

        arrayImages[4][0] = ReportsActivity.this.getResources().getString(R.string.fa_gift);
        arrayImages[4][1] = "#ede070";

        arrayImages[5][0] = ReportsActivity.this.getResources().getString(R.string.fa_money);
        arrayImages[5][1] = "#5F3F4B";

        //
        Random random = new Random();
        int rand = random.nextInt(arrayImages.length);

        result[0][0] = tipsArray.get(rand);
        result[0][1] = arrayImages[rand];

        return result;
    }

    @Override
    Context context() {
        return ReportsActivity.this;
    }

    @Override
    void init() {

        smartReportLayout = findViewById(R.id.reports_activity_smart_report_layout);
        payeesReportLayout = findViewById(R.id.reports_activity_payees_report_layout);
        yearlyReportLayout = findViewById(R.id.reports_activity_yearly_report_layout);
        monthlyReportLayout = findViewById(R.id.reports_activity_monthly_report_layout);
        rangeReportLayout = findViewById(R.id.reports_activity_range_report_layout);
        categoryReportLayout = findViewById(R.id.reports_activity_category_report_layout);
        summaryReportLayout = findViewById(R.id.reports_activity_summary_report_layout);
        tipsText = findViewById(R.id.reports_activity_tips_text);
        tipsIcon = findViewById(R.id.reports_activity_tips_img);
    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = v -> {
            Intent intent;
            ReportsItemActivity.ReportTypesEnum target = null;

            switch (v.getId()) {

                case R.id.reports_activity_smart_report_layout:
                    target = ReportsItemActivity.ReportTypesEnum.SmartReport;
                    break;

                case R.id.reports_activity_payees_report_layout:
                    target = ReportsItemActivity.ReportTypesEnum.PayeeReports;
                    break;

                case R.id.reports_activity_yearly_report_layout:
                    target = ReportsItemActivity.ReportTypesEnum.ReportByYear;
                    break;

                case R.id.reports_activity_monthly_report_layout:
                    target = ReportsItemActivity.ReportTypesEnum.ReportByMonth;
                    break;

                case R.id.reports_activity_range_report_layout:
                    target = ReportsItemActivity.ReportTypesEnum.PeriodReport;
                    break;

                case R.id.reports_activity_category_report_layout:
                    target = ReportsItemActivity.ReportTypesEnum.ReportByCategory;
                    break;

                case R.id.reports_activity_summary_report_layout:
                    target = ReportsItemActivity.ReportTypesEnum.ReportSummary;
                    break;

            }

            intent = new Intent(ReportsActivity.this, ReportsItemActivity.class);
            intent.putExtra("reportType", target.value());
            startActivity(intent);
        };

        smartReportLayout.setOnClickListener(listeners);
        payeesReportLayout.setOnClickListener(listeners);
        yearlyReportLayout.setOnClickListener(listeners);
        monthlyReportLayout.setOnClickListener(listeners);
        rangeReportLayout.setOnClickListener(listeners);
        categoryReportLayout.setOnClickListener(listeners);
        summaryReportLayout.setOnClickListener(listeners);


    }

    @Override
    void setAmountTypeFaces() {
        //Nothing
    }

    private void setRandomTips() {

        Object[][] tips = getRandomTip();

        String text = (String) tips[0][0];
        String[] tipsIconArray = (String[]) tips[0][1];
        String icon = tipsIconArray[0];
        String color = tipsIconArray[1];

        tipsIcon.setText(icon);
        tipsIcon.setTextColor(Color.parseColor(color));
        tipsText.setText(text);
        tipsIcon = AppModules.setTypeFace(ReportsActivity.this, tipsIcon, "fontawesome_webfont.ttf");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        init();
        setAmountTypeFaces();
        setListeners();

        setRandomTips();

    }
}
