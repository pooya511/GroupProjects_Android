package com.taxiapps.dakhlokharj;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.Mask;
import com.redmadrobot.inputmask.model.CaretString;
import com.sevenheaven.iosswitch.ShSwitchView;

import java.util.HashMap;

import model.Account;
import model.Bank;
import model.Currency;
import model.EnumsAndConstants;
import model.Settings;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.AppModules;
import utils.DialogUtils;

public class NewAndEditAccountActivity extends BaseActivity {

    private EditText accountNameEditText, accountNumberEditText, cardNumberEditText, shabaNumberEditText;
    private ImageView accountNameClear, accountNumberClear, cardNumberClear, shabaNumberClear, bankImage;
    private ConstraintLayout bankContainer, removeLayout;
    private ShSwitchView currentAccountSwitch, accountStatSwitch;
    private TextView initialBalanceTextView, bankName, titleTextView, saveTextView, currency;

    private Account account;

    private Dialog bnkDialog;

    @Override
    Context context() {
        return NewAndEditAccountActivity.this;
    }

    @Override
    void init() {

        accountNameEditText = findViewById(R.id.new_and_edit_account_activity_name_edittext);
        accountNumberEditText = findViewById(R.id.new_and_edit_account_activity_account_number_edittext);
        accountNumberEditText = (EditText) AppModules.setTypeFace(this, accountNumberEditText, "Roboto-Light.ttf");
        cardNumberEditText = findViewById(R.id.new_and_edit_account_activity_card_number_edittext);
        cardNumberEditText = (EditText) AppModules.setTypeFace(this, cardNumberEditText, "Roboto-Light.ttf");
        shabaNumberEditText = findViewById(R.id.new_and_edit_account_activity_shaba_number_edittext);
        shabaNumberEditText = (EditText) AppModules.setTypeFace(this, shabaNumberEditText, "Roboto-Light.ttf");

        bankContainer = findViewById(R.id.new_and_edit_account_activity_bank_container);
        removeLayout = findViewById(R.id.new_and_edit_account_activity_remove_layout);

        accountStatSwitch = findViewById(R.id.new_and_edit_account_activity_account_status_switch);
        currentAccountSwitch = findViewById(R.id.new_and_edit_account_activity_current_account_switch);

        initialBalanceTextView = findViewById(R.id.new_and_edit_account_activity_initial_balance_text_view);
        bankName = findViewById(R.id.new_and_edit_account_activity_bank_name_textview);
        titleTextView = findViewById(R.id.new_and_edit_account_activity_title_textview);
        saveTextView = findViewById(R.id.new_and_edit_account_activity_save_textview);
        currency = findViewById(R.id.new_and_edit_account_activity_initial_balance_currency);

        accountNameClear = findViewById(R.id.new_and_edit_account_activity_name_edittext_clear_btn);
        accountNumberClear = findViewById(R.id.new_and_edit_account_activity_account_number_edittext_clear_btn);
        cardNumberClear = findViewById(R.id.new_and_edit_account_activity_card_number_edittext_clear_btn);
        shabaNumberClear = findViewById(R.id.new_and_edit_account_activity_shaba_number_edittext_clear_btn);
        bankImage = findViewById(R.id.new_and_edit_account_activity_bank_logo_img);

    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = v -> {
            if (v.equals(bankContainer)) {
                bnkDialog = Bank.choosingBank(NewAndEditAccountActivity.this, bank -> {
                    account.setAcnIcon(bank.getID());
                    PublicModules.stringToBitmap(bank.getBnkLogo(), bankImage);
                    bankName.setText(Bank.getBankWithID(account.getAcnIcon()).getBnkTitle());
                    bnkDialog.dismiss();
                });

                bnkDialog.show();
            }

            if (v.equals(initialBalanceTextView)) {
                DialogUtils.keypadDialog(NewAndEditAccountActivity.this, initialBalanceTextView.getText().toString().replaceAll("\\D", ""), number -> {
                    number = number.replaceAll("\\D","");
                    account.setAcnBalance(number);
                    initialBalanceTextView.setText(Currency.CurrencyString.getCurrencyString(number, Currency.CurrencyMode.Full).separated);
                }).show();
            }

            if (v.equals(saveTextView)) {

                if (!accountValidation().isValid) {
                    String description = accountValidation().message;
                    Dialog dialog = PublicDialogs.makeMessageDialog(NewAndEditAccountActivity.this, PublicDialogs.MessageDialogTypes.ERROR, "خطا", description, "" , EnumsAndConstants.APP_SOURCE);
                    dialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                    dialog.show();
                } else {
                    if (AccountListActivity.targetAccount != null) {
                        setAccountDetails();

                        account.update(new String[]{Account.AccountCol.acnName.value(), Account.AccountCol.acnIcon.value(), Account.AccountCol.acnAccount.value(), Account.AccountCol.acnCard.value(), Account.AccountCol.acnSHABA.value(), Account.AccountCol.acnType.value(), Account.AccountCol.acnBalance.value(), Account.AccountCol.acnIsActive.value()});
                        setResult(RESULT_OK);
                        //
                        customToast("ویرایش حساب", "حساب ویرایش شد");
                        //
                        finish();
                    } else {
                        setAccountDetails();

                        account.insert();
                        setResult(RESULT_OK);

                        customToast("اضافه کردن حساب", "حساب جدید اضافه شد");

                        HashMap<String , String> map = Settings.loadMap(Settings.ACCOUNT_ORDER_MAP);
                        map.put(String.valueOf(account.getID()) , String.valueOf(999));
                        Settings.saveMap(Settings.ACCOUNT_ORDER_MAP , map);

                        finish();
                    }

                }

            }

            if (v.equals(removeLayout)) {
                Dialog removeAccount = removeAccount(NewAndEditAccountActivity.this);
                removeAccount.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                removeAccount.show();
            }
        };

        bankContainer.setOnClickListener(listeners);
        initialBalanceTextView.setOnClickListener(listeners);
        saveTextView.setOnClickListener(listeners);
        removeLayout.setOnClickListener(listeners);

    }

    @Override
    void setAmountTypeFaces() {
        initialBalanceTextView.setTypeface(Application.amountTypeFace(NewAndEditAccountActivity.this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_and_edit_account);

        init();
        setAmountTypeFaces();
        setListeners();

        currency.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
        accountStatSwitch.setOn(true);

        if (AccountListActivity.targetAccount != null) {
            account = AccountListActivity.targetAccount;

            accountNameEditText.setText(account.getAcnName());
            PublicModules.stringToBitmap(Bank.getBankWithID(account.getAcnIcon()).getBnkLogo(), bankImage);
            bankName.setText(Bank.getBankWithID(account.getAcnIcon()).getBnkTitle());
            accountNumberEditText.setText(account.getAcnAccount());

            final Mask mask = new Mask("[0000]-[0000]-[0000]-[0000]");
            final String input = account.getAcnCard();
            final Mask.Result result = mask.apply(
                    new CaretString(
                            input,
                            input.length()
                    ),
                    true
            );
            final String output = result.getFormattedText().getString();
            cardNumberEditText.setText(output);

            shabaNumberEditText.setText(account.getAcnShaba());

            if (account.getAcnType().equals("1"))
                currentAccountSwitch.setOn(true);
            else
                currentAccountSwitch.setOn(false);

            initialBalanceTextView.setText(Currency.CurrencyString.getCurrencyString(account.getAcnBalance(),null).separated);

            if (account.getAcnIsActive() == 1)
                accountStatSwitch.setOn(true);
            else
                accountStatSwitch.setOn(false);
        } else {
            titleTextView.setText("حساب");
            removeLayout.setVisibility(View.GONE);
            account = new Account(false);
        }

        //card editText mask
        final MaskedTextChangedListener cardNumberMask = new MaskedTextChangedListener("[0000]-[0000]-[0000]-[0000]", false, cardNumberEditText, null, (b, s) -> cardNumberEditText.setSelection(s.length()));

        cardNumberEditText.setOnKeyListener((v, keyCode, event) -> {

            String userText = cardNumberEditText.getText().toString();

            if (keyCode == KeyEvent.KEYCODE_DEL) {
                cardNumberEditText.setText(userText.substring(0, userText.length()));
                cardNumberEditText.setSelection(cardNumberEditText.getText().toString().length());
            }
            return false;

        });

        cardNumberEditText.addTextChangedListener(cardNumberMask);
        cardNumberEditText.setOnFocusChangeListener(cardNumberMask);

        //

        MaskedTextChangedListener shabaNumberMask = new MaskedTextChangedListener("{IR}[000000000000000000000000]", false, shabaNumberEditText, null, (b, s) -> {

        });

        shabaNumberEditText.addTextChangedListener(shabaNumberMask);
        shabaNumberEditText.setOnFocusChangeListener(shabaNumberMask);

        //onClick listeners
        View.OnClickListener listeners = v -> {
            if (v.equals(accountNameClear)) {
                accountNameEditText.setText("");
            }
            if (v.equals(accountNumberClear)) {
                accountNumberEditText.setText("");
            }
            if (v.equals(cardNumberClear)) {
                cardNumberEditText.setText("");
            }
            if (v.equals(shabaNumberClear)) {
                shabaNumberEditText.setText("");
            }
        };
        accountNameClear.setOnClickListener(listeners);
        accountNumberClear.setOnClickListener(listeners);
        cardNumberClear.setOnClickListener(listeners);
        shabaNumberClear.setOnClickListener(listeners);


        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (accountNameEditText.isFocused()) {
                    if (!s.toString().equals("")) {
                        accountNameClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        accountNameClear.setVisibility(View.GONE);
                    }
                } else if (!accountNameEditText.isFocused()) {
                    accountNameClear.setVisibility(View.GONE);
                }
                if (accountNumberEditText.isFocused()) {
                    if (!s.toString().equals("")) {
                        accountNumberClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        accountNumberClear.setVisibility(View.GONE);
                    }
                } else if (!accountNumberEditText.isFocused()) {
                    accountNumberClear.setVisibility(View.GONE);
                }
                if (cardNumberEditText.isFocused()) {
                    if (!s.toString().equals("")) {
                        cardNumberClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        cardNumberClear.setVisibility(View.GONE);
                    }
                } else if (!cardNumberEditText.isFocused()) {
                    cardNumberClear.setVisibility(View.GONE);
                }
                if (shabaNumberEditText.isFocused()) {
                    if (!s.toString().equals("")) {
                        shabaNumberClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        shabaNumberClear.setVisibility(View.GONE);
                    }
                } else if (!shabaNumberEditText.isFocused()) {
                    shabaNumberClear.setVisibility(View.GONE);
                }
            }
        };

        accountNameEditText.addTextChangedListener(watcher);
        accountNumberEditText.addTextChangedListener(watcher);
        cardNumberEditText.addTextChangedListener(watcher);
        shabaNumberEditText.addTextChangedListener(watcher);

        View.OnFocusChangeListener onFocusChangeListener = (v, hasFocus) -> {
            if (v.equals(accountNameEditText)) {
                if (hasFocus) {
                    if (!accountNameEditText.getText().toString().equals("")) {
                        accountNameClear.setVisibility(View.VISIBLE);
                    } else if (accountNameEditText.getText().toString().equals("")) {
                        accountNameClear.setVisibility(View.GONE);
                    }
                } else
                    accountNameClear.setVisibility(View.GONE);
            }
            if (v.equals(accountNumberEditText)) {
                if (hasFocus) {
                    if (!accountNumberEditText.getText().toString().equals("")) {
                        accountNumberClear.setVisibility(View.VISIBLE);
                    } else if (accountNumberEditText.getText().toString().equals("")) {
                        accountNumberClear.setVisibility(View.GONE);
                    }
                } else
                    accountNumberClear.setVisibility(View.GONE);
            }
            if (v.equals(cardNumberEditText)) {
                if (hasFocus) {
                    if (!cardNumberEditText.getText().toString().equals("")) {
                        cardNumberClear.setVisibility(View.VISIBLE);
                    } else if (cardNumberEditText.getText().toString().equals("")) {
                        cardNumberClear.setVisibility(View.GONE);
                    }
                } else
                    cardNumberClear.setVisibility(View.GONE);
            }
            if (v.equals(shabaNumberEditText)) {
                if (hasFocus) {
                    if (!shabaNumberEditText.getText().toString().equals("")) {
                        shabaNumberClear.setVisibility(View.VISIBLE);
                    } else if (shabaNumberEditText.getText().toString().equals("")) {
                        shabaNumberClear.setVisibility(View.GONE);
                    }
                } else
                    shabaNumberClear.setVisibility(View.GONE);
            }

        };

        accountNameEditText.setOnFocusChangeListener(onFocusChangeListener);
        accountNumberEditText.setOnFocusChangeListener(onFocusChangeListener);
        cardNumberEditText.setOnFocusChangeListener(onFocusChangeListener);
        shabaNumberEditText.setOnFocusChangeListener(onFocusChangeListener);

    }

    private AccountValidation accountValidation() {
        AccountValidation validation = new AccountValidation();
        validation.isValid = true;
        validation.message = "";

        if (accountNameEditText.getText().toString().equals("")) {
            validation.isValid = false;
            validation.message = "ورود نام برای حساب اجباری است.";
        } else if (bankName.getText().toString().equals("انتخاب")) {
            validation.isValid = false;
            validation.message = "انتخاب بانک اجباری است.";
        }
        return validation;
    }

    private void setAccountDetails() {
        account.setAcnName(accountNameEditText.getText().toString());
        account.setAcnAccount(accountNumberEditText.getText().toString());
        account.setAcnCard(cardNumberEditText.getText().toString().replace("-", ""));
        account.setAcnShaba(shabaNumberEditText.getText().toString());

        if (currentAccountSwitch.isOn())
            account.setAcnType("1");
        else
            account.setAcnType("0");

//        account.setAcnBalance(PublicModules.toEnglishDigit(initialBalanceTextView.getText().toString()));

        if (accountStatSwitch.isOn())
            account.setAcnIsActive(1);
        else
            account.setAcnIsActive(0);
    }

    public Dialog removeAccount(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View removeAccount = inflater.inflate(R.layout.pop_remove_payee, null);

        final TextView removePayee, cancel, title, desc;
        removePayee = removeAccount.findViewById(R.id.pop_remove_payee_reset);
        cancel = removeAccount.findViewById(R.id.pop_remove_payee_cancel);
        title = removeAccount.findViewById(R.id.pop_remove_payee_title);
        desc = removeAccount.findViewById(R.id.pop_remove_payee_description);

        title.setText("حذف حساب");
        desc.setText("آیا این حساب حذف شود؟");

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 21;
                break;

            case 2:
                width = 70;
                height = 16;
                break;

            case 3:
                width = 55;
                height = 13;
                break;

        }

        final Dialog resetDataDialog = PublicDialogs.makeDialog(context, removeAccount, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel))
                resetDataDialog.dismiss();

            if (v.equals(removePayee)) {
                account.delete();
                HashMap<String , String> map = Settings.loadMap(Settings.ACCOUNT_ORDER_MAP);
                map.remove(String.valueOf(account.getID()));
                Settings.saveMap(Settings.ACCOUNT_ORDER_MAP , map);
                setResult(RESULT_OK);
                finish();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        removePayee.setOnClickListener(dialogListeners);

        return resetDataDialog;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    public class AccountValidation {
        public String message;
        public boolean isValid;
    }

    private void customToast(String title, String description) {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom,
                findViewById(R.id.toast_layout_root));

        ImageView image = layout.findViewById(R.id.toast_img);
        image.setImageResource(R.drawable.ic_success);
        TextView titleText = layout.findViewById(R.id.toast_title);
        titleText.setText(title);
        TextView descriptionText = layout.findViewById(R.id.toast_description);
        descriptionText.setText(description);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 25);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}