package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.io.File;

import model.Settings;

public class SplashActivity extends BaseActivity{

    // Splash screen timer
    private int SPLASH_TIME_OUT = 1700;

    @Override
    Context context() {
        return SplashActivity.this;
    }

    @Override
    void init() {
        //Nothing
    }

    @Override
    void setListeners() {
        //Nothing
    }

    @Override
    void setAmountTypeFaces() {
        //Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        init();
        setAmountTypeFaces();
        setListeners();

        /*
         * Showing splash screen with a timer. This will be useful when you
         * want to show case your app logo / company
         */
        new Handler().postDelayed(() -> {
            // This method will be executed once the timer is over
            // Start your app main activity
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            // close this activity
            finish();
        }, SPLASH_TIME_OUT);

    }
}
