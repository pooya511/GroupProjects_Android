package com.taxiapps.dakhlokharj;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import adapters.PopPayeeAdapter;
import dialog_fragments.DatePicker;
import model.Payee;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class PayeeReportSearchActivity extends BaseActivity {

    private DatePicker datePicker;

    private ConstraintLayout payee, fromDate, toDate, expense, income, search;
    private TextView payeeName, fromDateRes, toDateRes;
    private ImageView expenseImage, incomeImage;

    private boolean expenseIsChecked, incomeIsChecked;

    private ReturnedItem returnedItem = new ReturnedItem();

    @Override
    Context context() {
        return PayeeReportSearchActivity.this;
    }

    @Override
    void init() {

        payee = findViewById(R.id.payee_report_search_activity_filter_layout);
        fromDate = findViewById(R.id.payee_report_search_activity_from_date_layout);
        toDate = findViewById(R.id.payee_report_search_activity_to_date_layout);
        expense = findViewById(R.id.payee_report_search_activity_type_expense_layout);
        income = findViewById(R.id.payee_report_search_activity_type_income_layout);
        payeeName = findViewById(R.id.payee_report_search_activity_filter_text);
        fromDateRes = findViewById(R.id.payee_report_search_activity_from_date_text);
        toDateRes = findViewById(R.id.payee_report_search_activity_to_date_text);
        expenseImage = findViewById(R.id.payee_report_search_activity_type_expense_checkbox_img);
        incomeImage = findViewById(R.id.payee_report_search_activity_type_income_checkbox_img);
        search = findViewById(R.id.payee_report_search_activity_search_layout);

        PersianDateFormat formatter = new PersianDateFormat("Y/m/d");

        if (getIntent().getParcelableExtra("item") != null) {
            returnedItem = getIntent().getParcelableExtra("item");
            if (returnedItem.payeeName != null)
                payeeName.setText(returnedItem.payeeName);
            if (returnedItem.dateFrom != 0)
                fromDateRes.setText(formatter.format(new PersianDate(returnedItem.dateFrom)));
            if (returnedItem.dateTo != 0)
                toDateRes.setText(formatter.format(new PersianDate(returnedItem.dateTo)));
            if (returnedItem.isExpense) {
                expenseIsChecked = true;
                expenseImage.setBackgroundResource(R.drawable.ic_checkbox_checked);
            }
            if (returnedItem.isIncome) {
                incomeIsChecked = true;
                incomeImage.setBackgroundResource(R.drawable.ic_checkbox_checked);
            }
        }

    }

    @Override
    void setListeners() {

        View.OnTouchListener listener = new View.OnTouchListener() {

            private Dialog payeeDialog;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (v.equals(payee)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        v.setBackgroundColor(getResources().getColor(R.color.grayLight));
                        payeeDialog = Payee.payeeList(PayeeReportSearchActivity.this, new PopPayeeAdapter.PayeeCallBack() {
                            @Override
                            public void call(Payee payee) {
                                payeeDialog.dismiss();
                                payeeName.setTextColor(Color.parseColor("#3090ff"));
                                payeeName.setText(payee.getPyeName());
                                returnedItem.payeeName = payee.getPyeName();
                                returnedItem.payeeId = payee.getID();
                            }
                        });
                        payeeDialog.show();

                    } else {
                        v.setBackgroundColor(getResources().getColor(R.color.white));
                    }
                }
                if (v.equals(fromDate)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        v.setBackgroundColor(getResources().getColor(R.color.grayLight));
                        long date;
                        if (returnedItem.dateFrom != 0)
                            date = returnedItem.dateFrom;
                        else
                            date = new PersianDate().getTime();
                        datePicker = DatePicker.newInstance(date, new DatePicker.DatePickerCallBack() {
                            @Override
                            public void call(PersianDate persianCalendar) {
                                fromDateRes.setText(PublicModules.timeStamp2String(persianCalendar.getTime(), "yyyy MM dd"));
                                persianCalendar.setHour(0);
                                persianCalendar.setMinute(0);
                                returnedItem.dateFrom = persianCalendar.getTime();
                                datePicker.dismiss();
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        });
                        datePicker.show(getFragmentManager(), null);

                    } else {
                        v.setBackgroundColor(getResources().getColor(R.color.white));
                    }
                }
                if (v.equals(toDate)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        v.setBackgroundColor(getResources().getColor(R.color.grayLight));
                        long date;
                        if (returnedItem.dateTo != 0)
                            date = returnedItem.dateTo;
                        else
                            date = new PersianDate().getTime();
                        datePicker = DatePicker.newInstance(date, new DatePicker.DatePickerCallBack() {
                            @Override
                            public void call(PersianDate persianCalendar) {
                                toDateRes.setText(PublicModules.timeStamp2String(persianCalendar.getTime(), "yyyy MM dd"));
                                persianCalendar.setHour(11);
                                persianCalendar.setMinute(59);
                                returnedItem.dateTo = persianCalendar.getTime();
                                datePicker.dismiss();
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        });
                        datePicker.show(getFragmentManager(), null);
                    } else {
                        v.setBackgroundColor(getResources().getColor(R.color.white));
                    }
                }
                if (v.equals(expense)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        v.setBackgroundColor(getResources().getColor(R.color.grayLight));
                        if (expenseIsChecked) {
                            expenseIsChecked = false;
                            expenseImage.setBackgroundResource(R.drawable.ic_checkbox_unchecked);
                        } else {
                            expenseIsChecked = true;
                            expenseImage.setBackgroundResource(R.drawable.ic_checkbox_checked);
                        }
                        returnedItem.isExpense = expenseIsChecked;
                    } else {
                        v.setBackgroundColor(getResources().getColor(R.color.white));
                    }
                }
                if (v.equals(income)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        v.setBackgroundColor(getResources().getColor(R.color.grayLight));
                        if (incomeIsChecked) {
                            incomeIsChecked = false;
                            incomeImage.setBackgroundResource(R.drawable.ic_checkbox_unchecked);
                        } else {
                            incomeIsChecked = true;
                            incomeImage.setBackgroundResource(R.drawable.ic_checkbox_checked);
                        }
                        returnedItem.isIncome = incomeIsChecked;
                    } else {
                        v.setBackgroundColor(getResources().getColor(R.color.white));
                    }
                }
                if (v.equals(search)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        v.setBackgroundColor(getResources().getColor(R.color.blue));
                        if (returnedItem.payeeName != null) {
                            Intent intent = getIntent();
                            intent.putExtra("data", returnedItem);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Animation shake = AnimationUtils.loadAnimation(PayeeReportSearchActivity.this, R.anim.shake);
                            payeeName.startAnimation(shake);
                            payeeName.setTextColor(Color.RED);
                        }
                    } else
                        v.setBackgroundColor(Color.parseColor("#3090ff"));

                }

                return true;
            }
        };

        payee.setOnTouchListener(listener);
        fromDate.setOnTouchListener(listener);
        toDate.setOnTouchListener(listener);
        expense.setOnTouchListener(listener);
        income.setOnTouchListener(listener);
        search.setOnTouchListener(listener);

    }

    @Override
    void setAmountTypeFaces() {
        //Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payee_report_search);

        init();
        setAmountTypeFaces();
        setListeners();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    public static class ReturnedItem implements Parcelable {

        public String payeeName;
        public int payeeId;
        public long dateFrom;
        public long dateTo;
        public boolean isExpense;
        public boolean isIncome;

        public ReturnedItem() {
        }

        public ReturnedItem(String payeeName, int payeeId, long dateFrom, long dateTo, boolean isExpense, boolean isIncome) {
            this.payeeName = payeeName;
            this.payeeId = payeeId;
            this.dateFrom = dateFrom;
            this.dateTo = dateTo;
            this.isExpense = isExpense;
            this.isIncome = isIncome;
        }

        protected ReturnedItem(Parcel in) {
            payeeName = in.readString();
            payeeId = in.readInt();
            dateFrom = in.readLong();
            dateTo = in.readLong();
            isExpense = in.readByte() != 0;
            isIncome = in.readByte() != 0;
        }

        public static final Creator<ReturnedItem> CREATOR = new Creator<ReturnedItem>() {
            @Override
            public ReturnedItem createFromParcel(Parcel in) {
                return new ReturnedItem(in);
            }

            @Override
            public ReturnedItem[] newArray(int size) {
                return new ReturnedItem[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(payeeName);
            dest.writeInt(payeeId);
            dest.writeLong(dateFrom);
            dest.writeLong(dateTo);
            dest.writeByte((byte) (isExpense ? 1 : 0));
            dest.writeByte((byte) (isIncome ? 1 : 0));
        }

    }

}