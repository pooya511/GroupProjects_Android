package com.taxiapps.dakhlokharj;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import adapters.DetailAccountAdapter;
import github.nisrulz.screenshott.ScreenShott;
import model.Account;
import model.Currency;
import model.EnumsAndConstants;
import model.GroupDetail;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.AppModules;

public class DetailAccountActivity extends BaseActivity {

    private ListView listView;
    private ConstraintLayout didntAddTransactionLayout;
    private LinearLayout listTitle;
    private TextView accountName, detailAccountNumber;
    private ArrayList<Transaction> transactions;
    private LinearLayout shareLayout;
    private ConstraintLayout detailAccountLayout;
    private final int PERMISSIONS_REQUEST_DETAIL_ACCOUNT_SCREENSHOT = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_DETAIL_ACCOUNT_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(detailAccountLayout, "DetailAccountScreenShot");
                    Dialog shareDialog = shareDialog(DetailAccountActivity.this);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(DetailAccountActivity.this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(DetailAccountActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog alwaysDenyDialog = alwaysDenyDialog(DetailAccountActivity.this);
                        alwaysDenyDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        alwaysDenyDialog.show();
                    }
                }
                break;

        }
    }

    @Override
    Context context() {
        return DetailAccountActivity.this;
    }

    @Override
    void init() {
        listView = findViewById(R.id.activity_detail_account_list_view);
        didntAddTransactionLayout = findViewById(R.id.activity_detail_account_didnt_add_transaction_layout);
        listTitle = findViewById(R.id.activity_detail_account_list_title);
        accountName = findViewById(R.id.activity_detail_account_account_name);
        detailAccountNumber = findViewById(R.id.activity_detail_account_detail_account_number);
        shareLayout = findViewById(R.id.activity_detail_account_share_layout);
        detailAccountLayout = findViewById(R.id.activity_detail_account_layout);
    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(DetailAccountActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((DetailAccountActivity.this),
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_DETAIL_ACCOUNT_SCREENSHOT);
                } else {

                    takeScreenShot(detailAccountLayout, "DetailAccountScreenShot");
                    Dialog shareDialog = shareDialog(DetailAccountActivity.this);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                }
            }
        };

        shareLayout.setOnClickListener(listeners);
    }

    @Override
    void setAmountTypeFaces() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_account);
        init();
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        transactions = Account.getAllTransaction(getIntent().getIntExtra("accountId", 0));

        if (transactions.size() == 0) {
            didntAddTransactionLayout.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            listTitle.setVisibility(View.GONE);
        } else {
            didntAddTransactionLayout.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            listTitle.setVisibility(View.VISIBLE);
            accountName.setText(getIntent().getStringExtra("accountName"));
            detailAccountNumber.setText(" ( " + String.valueOf(transactions.size()) + " مورد ) ");
            DetailAccountAdapter detailAccountAdapter = new DetailAccountAdapter(DetailAccountActivity.this, transactions);
            listView.setAdapter(detailAccountAdapter);
        }
    }

    private Dialog shareDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View shareReportLayout = inflater.inflate(R.layout.pop_report_share, null);

        final ConstraintLayout shareImg, shareText, cancel;
        TextView icon1, icon2;
        shareImg = shareReportLayout.findViewById(R.id.pop_report_share_img);
        shareText = shareReportLayout.findViewById(R.id.pop_report_share_text);
        cancel = shareReportLayout.findViewById(R.id.pop_report_share_cancel);

        icon1 = shareReportLayout.findViewById(R.id.pop_report_share_ic1);
        icon2 = shareReportLayout.findViewById(R.id.pop_report_share_ic2);

        icon1 = AppModules.setTypeFace(DetailAccountActivity.this, icon1, "fontawesome_webfont.ttf");
        icon2 = AppModules.setTypeFace(DetailAccountActivity.this, icon2, "fontawesome_webfont.ttf");

        icon1.setText(R.string.fa_picture_o);
        icon2.setText(R.string.fa_file_text);


        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 82;
                height = 34;
                break;

            case 2:
                width = 75;
                height = 29;
                break;

            case 3:
                width = 60;
                height = 23;
                break;

        }

        final Dialog faDialog = PublicDialogs.makeDialog(context, shareReportLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(shareImg)) {

                shareImage(Settings.getSetting(Settings.DETAIL_ACCOUNT_SCREENSHOT_PATH));
                faDialog.dismiss();

            }
            if (v.equals(shareText)) {
                String text = "ریز حساب برای " + getIntent().getStringExtra("accountName") + "\n\n";
                for (int i = 0; i < transactions.size(); i++) {
                    GroupDetail groupDetail = GroupDetail.getGroupDetailByID(transactions.get(i).getIsdID());
                    text = text.concat(PublicModules.toPersianDigit((String.valueOf(i + 1))) + " - " + groupDetail.getLsdName() + "\n");
                    text = text.concat(PublicModules.toPersianDigit(PublicModules.timeStamp2String(Long.valueOf(transactions.get(i).getTrnDate()), "yyyy/MM/dd HH:mm")) + "\n");
                    if ((i + 1) != transactions.size()) {
                        text = text.concat(transactions.get(i).getTrnAmount()+ "\n\n");
                    } else {
                        text = text.concat(transactions.get(i).getTrnAmount()+ "\n");
                    }
                }
                text = text.concat("\n\n" + "گزارش از اپلیکیشن دخل و خرج");
                shareText(text);
                faDialog.dismiss();
            }
            if (v.equals(cancel)) {
                faDialog.dismiss();
            }
        };

        shareImg.setOnClickListener(dialogListeners);
        shareText.setOnClickListener(dialogListeners);
        cancel.setOnClickListener(dialogListeners);

        return faDialog;
    }

    private void shareText(String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(intent, "Share by"));
    }

    private void shareImage(String imagePath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        String path = "file://" + imagePath;
        Uri screenshotUri = Uri.parse(path);
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        intent.setType("image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(intent, "Share by"));
    }

    public void takeScreenShot(View view, String name) {
        Bitmap bitmap_hiddenview = ScreenShott.getInstance().takeScreenShotOfView(view);
        try {
            File file = ScreenShott.getInstance().saveScreenshotToPicturesFolder(DetailAccountActivity.this, bitmap_hiddenview, name);
            Settings.setSetting(String.valueOf(Settings.DETAIL_ACCOUNT_SCREENSHOT_PATH), file.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Dialog alwaysDenyDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View popAllwaysDeny = inflater.inflate(R.layout.pop_allways_deny, null);

        final TextView goToSetting, cancel;
        goToSetting = popAllwaysDeny.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = popAllwaysDeny.findViewById(R.id.pop_allways_deny_cancel);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 72;
                height = 19;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog rpydDialog = PublicDialogs.makeDialog(context, popAllwaysDeny, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel))
                rpydDialog.dismiss();

            if (v.equals(goToSetting)) {
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);

                rpydDialog.dismiss();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        goToSetting.setOnClickListener(dialogListeners);

        return rpydDialog;
    }

}
