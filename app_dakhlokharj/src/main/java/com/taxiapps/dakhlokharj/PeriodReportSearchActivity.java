package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.TextView;

import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import utils.AppModules;

public class PeriodReportSearchActivity extends BaseActivity {

    private ConstraintLayout fromDate, toDate, search;
    private TextView fromDateTextView, toDateTextView;

    public long fromDateTimeStamp = 0;
    public long toDateTimeStamp = 0;

    private static PeriodReportSearchActivity instance;

    public static PeriodReportSearchActivity getInstance() {
        if (instance == null)
            instance = new PeriodReportSearchActivity();
        return instance;
    }

    @Override
    Context context() {
        return PeriodReportSearchActivity.this;
    }

    @Override
    void init() {
        fromDate = findViewById(R.id.period_report_search_activity_from_date_layout);
        toDate = findViewById(R.id.period_report_search_activity_to_date_layout);
        fromDateTextView = findViewById(R.id.period_report_search_activity_from_date_text);
        toDateTextView = findViewById(R.id.period_report_search_activity_to_date_text);
        search = findViewById(R.id.period_report_search_activity_search_layout);

        if (getInstance().fromDateTimeStamp != 0)
            fromDateTextView.setText(PublicModules.timeStamp2String(getInstance().fromDateTimeStamp, "yyyy MM dd"));

        if (getInstance().toDateTimeStamp != 0)
            toDateTextView.setText(PublicModules.timeStamp2String(getInstance().toDateTimeStamp, "yyyy MM dd"));

    }

    @Override
    void setListeners() {

        View.OnClickListener listener = new View.OnClickListener() {
            dialog_fragments.DatePicker datePicker = null;

            @Override
            public void onClick(View v) {

                if (v.equals(fromDate)) {

                    long date;
                    if (getInstance().fromDateTimeStamp != 0)
                        date = getInstance().fromDateTimeStamp;
                    else
                        date = new PersianDate().getTime();

                    datePicker = dialog_fragments.DatePicker.newInstance(date, new dialog_fragments.DatePicker.DatePickerCallBack() {
                        @Override
                        public void call(PersianDate persianCalendar) {
                            fromDateTextView.setText(PublicModules.timeStamp2String(persianCalendar.getTime(), "yyyy MM dd"));
                            getInstance().fromDateTimeStamp = persianCalendar.getTime();
                            datePicker.dismiss();
                        }

                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }
                    });
                    datePicker.show(getFragmentManager(), null);
                }

                if (v.equals(toDate)) {

                    long date;
                    if (getInstance().toDateTimeStamp != 0)
                        date = getInstance().toDateTimeStamp;
                    else
                        date = new PersianDate().getTime();

                    datePicker = dialog_fragments.DatePicker.newInstance(date, new dialog_fragments.DatePicker.DatePickerCallBack() {
                        @Override
                        public void call(PersianDate persianCalendar) {
                            toDateTextView.setText(PublicModules.timeStamp2String(persianCalendar.getTime(), "yyyy MM dd"));
                            getInstance().toDateTimeStamp = persianCalendar.getTime();
                            datePicker.dismiss();
                        }

                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }
                    });
                    datePicker.show(getFragmentManager(), null);

                }

                if (v.equals(search)) {

                    if (getInstance().toDateTimeStamp == 0 && getInstance().fromDateTimeStamp == 0)
                        setResult(RESULT_CANCELED);
                    else
                        setResult(RESULT_OK);

                    finish();
                }

            }
        };

        fromDate.setOnClickListener(listener);
        toDate.setOnClickListener(listener);
        search.setOnClickListener(listener);

    }

    @Override
    void setAmountTypeFaces() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_period_report_search);

        instance = this;

        init();
        setListeners();
        setAmountTypeFaces();
    }
}
