package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;

import adapters.SearchResultListAdapter;
import model.Account;
import model.Bank;
import model.Group;
import model.GroupDetail;
import model.Payee;
import model.Transaction;
import utils.AppModules;

public class SearchActivity extends BaseActivity {

    private ArrayList<HashMap<String, Object>> data;
    private ArrayList<SearchedTransactions> byTrnDescription, byBankTitle, byLsdName, byLstName, byPayeName, byAcnName;
    private HashMap<String, ArrayList<SearchedTransactions>> arrayListsMap;
    private EditText searchEditText;
    private ExpandableListView expandableListView;
    private SearchResultListAdapter searchResultListAdapter;
    private ImageView clearImg;


    @Override
    Context context() {
        return SearchActivity.this;
    }

    private void initArrays(String s) {

        byTrnDescription = new ArrayList<>();
        byBankTitle = new ArrayList<>();
        byLsdName = new ArrayList<>();
        byLstName = new ArrayList<>();
        byPayeName = new ArrayList<>();
        byAcnName = new ArrayList<>();


        for (int i = 0; i < data.size(); i++) {

            SearchedTransactions transactions = new SearchedTransactions();

            transactions.setId(Integer.valueOf(String.valueOf(data.get(i).get(Transaction.TransactionCol.id.value()))));
            transactions.setLsdId(Integer.valueOf(String.valueOf(data.get(i).get(Transaction.TransactionCol.lsdID.value()))));
            transactions.setTrnAmount(Float.valueOf(String.valueOf(data.get(i).get(Transaction.TransactionCol.trnAmount.value()))));
            transactions.setTrnDate(String.valueOf(data.get(i).get(Transaction.TransactionCol.trnDate.value())));
            transactions.setPyeId(Integer.valueOf(String.valueOf(data.get(i).get(Transaction.TransactionCol.pyeID.value()))));
            transactions.setAcnId(Integer.valueOf(String.valueOf(data.get(i).get(Transaction.TransactionCol.acnID.value()))));
            transactions.setTrnDescription(String.valueOf(data.get(i).get(Transaction.TransactionCol.trnDescription.value())));
            transactions.setTrnImagePath(String.valueOf(data.get(i).get(Transaction.TransactionCol.trnImagePath.value())));
            transactions.setTrnColor(String.valueOf(data.get(i).get(Transaction.TransactionCol.trnColor.value())));
            transactions.setTrnType(Integer.valueOf(String.valueOf(data.get(i).get(Transaction.TransactionCol.trnType.value()))));
            transactions.setLstId(Integer.valueOf(String.valueOf(data.get(i).get(GroupDetail.GroupDetailCol.lstID.value()))));
            transactions.setLsdName(String.valueOf(data.get(i).get(GroupDetail.GroupDetailCol.lsdName.value())));
            transactions.setLsdIcon(String.valueOf(data.get(i).get(GroupDetail.GroupDetailCol.lsdIcon.value())));
            transactions.setLsdColor(String.valueOf(data.get(i).get(GroupDetail.GroupDetailCol.lsdColor.value())));
            transactions.setLstName(String.valueOf(data.get(i).get(Group.GroupCol.lstName.value())));
            transactions.setLstIcon(String.valueOf(data.get(i).get(Group.GroupCol.lstIcon.value())));
            transactions.setLstColor(String.valueOf(data.get(i).get(Group.GroupCol.lstColor.value())));
            transactions.setLstOrder(Integer.valueOf(String.valueOf(data.get(i).get(Group.GroupCol.lstOrder.value()))));
            transactions.setLstType(Integer.valueOf(String.valueOf(data.get(i).get(Group.GroupCol.lstType.value()))));
            transactions.setPyeName(String.valueOf(data.get(i).get(Payee.PayeeCol.pyeName.value())));
            transactions.setAcnName(String.valueOf(data.get(i).get(Account.AccountCol.acnName.value())));
            transactions.setBankTitle(String.valueOf(data.get(i).get(Bank.BankCol.bnkTitle.value())));

            if (String.valueOf(data.get(i).get(Transaction.TransactionCol.trnDescription.value())).contains(s))
                byTrnDescription.add(transactions);

            if (String.valueOf(data.get(i).get(Bank.BankCol.bnkTitle.value())).contains(s))
                byBankTitle.add(transactions);

            if (String.valueOf(data.get(i).get(GroupDetail.GroupDetailCol.lsdName.value())).contains(s))
                byLsdName.add(transactions);

            if (String.valueOf(data.get(i).get(Group.GroupCol.lstName.value())).contains(s))
                byLstName.add(transactions);

            if (String.valueOf(data.get(i).get(Payee.PayeeCol.pyeName.value())).contains(s))
                byPayeName.add(transactions);

            if (String.valueOf(data.get(i).get(Account.AccountCol.acnName.value())).contains(s))
                byAcnName.add(transactions);
        }

        if (byTrnDescription.size() == 0) {
            byTrnDescription.add(new SearchedTransactions("Empty"));
        }
        if (byAcnName.size() == 0) {
            byAcnName.add(new SearchedTransactions("Empty"));
        }
        if (byPayeName.size() == 0) {
            byPayeName.add(new SearchedTransactions("Empty"));
        }
        if (byLstName.size() == 0) {
            byLstName.add(new SearchedTransactions("Empty"));
        }
        if (byBankTitle.size() == 0) {
            byBankTitle.add(new SearchedTransactions("Empty"));
        }
        if (byLsdName.size() == 0) {
            byLsdName.add(new SearchedTransactions("Empty"));
        }

        arrayListsMap.put("byTrnDescription", byTrnDescription);
        arrayListsMap.put("byBankTitle", byBankTitle);
        arrayListsMap.put("byLsdName", byLsdName);
        arrayListsMap.put("byLstName", byLstName);
        arrayListsMap.put("byPayeName", byPayeName);
        arrayListsMap.put("byAcnName", byAcnName);


        searchResultListAdapter = new SearchResultListAdapter(SearchActivity.this, arrayListsMap);
        expandableListView.setAdapter(searchResultListAdapter);

        if (searchResultListAdapter.getGroupCount() > 0) {

            if (byTrnDescription.size() == 1 && byTrnDescription.get(0).getTrnDescription().equals("Empty")) {
                expandableListView.collapseGroup(0);
            } else {
                expandableListView.expandGroup(0);
            }

            if (byBankTitle.size() == 1 && byBankTitle.get(0).getTrnDescription().equals("Empty")) {
                expandableListView.collapseGroup(1);
            } else {
                expandableListView.expandGroup(1);
            }

            if (byPayeName.size() == 1 && byPayeName.get(0).getTrnDescription().equals("Empty")) {
                expandableListView.collapseGroup(2);
            } else {
                expandableListView.expandGroup(2);
            }

            if (byLstName.size() == 1 && byLstName.get(0).getTrnDescription().equals("Empty")) {
                expandableListView.collapseGroup(3);
            } else {
                expandableListView.expandGroup(3);
            }

            if (byLsdName.size() == 1 && byLsdName.get(0).getTrnDescription().equals("Empty")) {
                expandableListView.collapseGroup(4);
            } else {
                expandableListView.expandGroup(4);
            }

            if (byAcnName.size() == 1 && byAcnName.get(0).getTrnDescription().equals("Empty")) {
                expandableListView.collapseGroup(5);
            } else {
                expandableListView.expandGroup(5);
            }

        }


    }

    @Override
    void init() {
        searchEditText = findViewById(R.id.search_activity_search_edittext);
        expandableListView = findViewById(R.id.frg_search_expandable_listview);
        clearImg = findViewById(R.id.search_activity_clear_btn);
        byTrnDescription = new ArrayList<>();
        byBankTitle = new ArrayList<>();
        byLsdName = new ArrayList<>();
        byLstName = new ArrayList<>();
        byPayeName = new ArrayList<>();
        byAcnName = new ArrayList<>();
        data = new ArrayList<>();

        arrayListsMap = new HashMap<>();

    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = v -> {
            if (v.equals(clearImg)) {
                searchEditText.setText("");
            }
        };

        clearImg.setOnClickListener(listeners);

    }

    @Override
    void setAmountTypeFaces() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        init();
        setAmountTypeFaces();
        setListeners();

        data = Transaction.getAll();

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 2) {
                    initArrays(String.valueOf(s));
                }

                if (s.length() > 0) {
                    clearImg.setVisibility(View.VISIBLE);
                } else {
                    clearImg.setVisibility(View.GONE);
                }

            }
        };

        searchEditText.addTextChangedListener(textWatcher);

    }

    public class SearchedTransactions {

        private int id, lsdId, pyeId, acnId, trnType, lstId, lstType, lstOrder;
        private float trnAmount;
        private String trnDate, trnDescription, trnImagePath, trnColor, lsdName, lsdIcon, lsdColor, lstName, lstIcon, lstColor, pyeName, acnName, bankTitle;

        public SearchedTransactions() {
        }

        public SearchedTransactions(String trnDescription) {
            this.trnDescription = trnDescription;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLsdId() {
            return lsdId;
        }

        public void setLsdId(int lsdId) {
            this.lsdId = lsdId;
        }

        public int getPyeId() {
            return pyeId;
        }

        public void setPyeId(int pyeId) {
            this.pyeId = pyeId;
        }

        public int getAcnId() {
            return acnId;
        }

        public void setAcnId(int acnId) {
            this.acnId = acnId;
        }

        public int getTrnType() {
            return trnType;
        }

        public void setTrnType(int trnType) {
            this.trnType = trnType;
        }

        public int getLstId() {
            return lstId;
        }

        public void setLstId(int lstId) {
            this.lstId = lstId;
        }

        public int getLstType() {
            return lstType;
        }

        public void setLstType(int lstType) {
            this.lstType = lstType;
        }

        public int getLstOrder() {
            return lstOrder;
        }

        public void setLstOrder(int lstOrder) {
            this.lstOrder = lstOrder;
        }

        public float getTrnAmount() {
            return trnAmount;
        }

        public void setTrnAmount(float trnAmount) {
            this.trnAmount = trnAmount;
        }

        public String getTrnDate() {
            return trnDate;
        }

        public void setTrnDate(String trnDate) {
            this.trnDate = trnDate;
        }

        public String getTrnDescription() {
            return trnDescription;
        }

        public void setTrnDescription(String trnDescription) {
            this.trnDescription = trnDescription;
        }

        public String getTrnImagePath() {
            return trnImagePath;
        }

        public void setTrnImagePath(String trnImagePath) {
            this.trnImagePath = trnImagePath;
        }

        public String getTrnColor() {
            return trnColor;
        }

        public void setTrnColor(String trnColor) {
            this.trnColor = trnColor;
        }

        public String getLsdName() {
            return lsdName;
        }

        public void setLsdName(String lsdName) {
            this.lsdName = lsdName;
        }

        public String getLsdIcon() {
            return lsdIcon;
        }

        public void setLsdIcon(String lsdIcon) {
            this.lsdIcon = lsdIcon;
        }

        public String getLsdColor() {
            return lsdColor;
        }

        public void setLsdColor(String lsdColor) {
            this.lsdColor = lsdColor;
        }

        public String getLstName() {
            return lstName;
        }

        public void setLstName(String lstName) {
            this.lstName = lstName;
        }

        public String getLstIcon() {
            return lstIcon;
        }

        public void setLstIcon(String lstIcon) {
            this.lstIcon = lstIcon;
        }

        public String getLstColor() {
            return lstColor;
        }

        public void setLstColor(String lstColor) {
            this.lstColor = lstColor;
        }

        public String getPyeName() {
            return pyeName;
        }

        public void setPyeName(String pyeName) {
            this.pyeName = pyeName;
        }

        public String getAcnName() {
            return acnName;
        }

        public void setAcnName(String acnName) {
            this.acnName = acnName;
        }

        public String getBankTitle() {
            return bankTitle;
        }

        public void setBankTitle(String bankTitle) {
            this.bankTitle = bankTitle;
        }
    }
}
