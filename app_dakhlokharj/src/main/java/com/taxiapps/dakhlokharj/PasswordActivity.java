package com.taxiapps.dakhlokharj;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mtramin.rxfingerprint.RxFingerprint;
import com.mtramin.rxfingerprint.data.FingerprintAuthenticationResult;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import model.Settings;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.DialogUtils;
import utils.AppModules;

public class PasswordActivity extends BaseActivity {

    private Button button0, button1, button2, button3, button4, button5, button6, button7, button8, button9;
    private View indicator1, indicator2, indicator3, indicator4;
    private LinearLayout indicatorsContainer;
    private ConstraintLayout banTextViewContainer;
    private TextView banCountDown, deleteText, exit;
    private String passwordString = "";
    private boolean userHasPermission = true; // to handle 3 time wrong pass
    private CountDownTimer banCountDownTimer;
    private int counter = 0;
    private ImageView fingerImg;
    private Dialog fingerPrintDialog;
    private TextView tryAgainText;

    public enum PassEnteringType {

        UserWantToSet(1),
        UserWantToEnter(2);

        private int type;

        PassEnteringType(int type) {
            this.type = type;
        }

        public int value() {
            return this.type;
        }

    }

    public static Disposable disposable;

    public static void disposeScanner(Disposable disposable) {
        disposable.dispose();
    }

    @Override
    Context context() {
        return PasswordActivity.this;
    }

    public interface FingerPrintCallBack {
        void call(Boolean isOK);
    }

    @Override
    void init() {

        Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/avenir-next-regular.ttf");

        button0 = findViewById(R.id.password_activity_btn0);
        button1 = findViewById(R.id.password_activity_btn1);
        button2 = findViewById(R.id.password_activity_btn2);
        button3 = findViewById(R.id.password_activity_btn3);
        button4 = findViewById(R.id.password_activity_btn4);
        button5 = findViewById(R.id.password_activity_btn5);
        button6 = findViewById(R.id.password_activity_btn6);
        button7 = findViewById(R.id.password_activity_btn7);
        button8 = findViewById(R.id.password_activity_btn8);
        button9 = findViewById(R.id.password_activity_btn9);
        indicator1 = findViewById(R.id.password_activity_indicator1);
        indicator2 = findViewById(R.id.password_activity_indicator2);
        indicator3 = findViewById(R.id.password_activity_indicator3);
        indicator4 = findViewById(R.id.password_activity_indicator4);
        deleteText = findViewById(R.id.password_activity_delete_textview);
        indicatorsContainer = findViewById(R.id.password_activity_indicators_container);
        banTextViewContainer = findViewById(R.id.password_activity_ban_textview_container);
        banCountDown = findViewById(R.id.password_activity_ban_textview);
        exit = findViewById(R.id.password_activity_exit_textview);
        fingerImg = findViewById(R.id.password_activity_ic_fingerprint);

        button0.setTypeface(typeface);
        button1.setTypeface(typeface);
        button2.setTypeface(typeface);
        button3.setTypeface(typeface);
        button4.setTypeface(typeface);
        button5.setTypeface(typeface);
        button6.setTypeface(typeface);
        button7.setTypeface(typeface);
        button8.setTypeface(typeface);
        button9.setTypeface(typeface);

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    void setListeners() {

        @SuppressLint("ClickableViewAccessibility") View.OnTouchListener listeners = (v, event) -> {
            if (v.equals(button0)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button0.setTextColor(Color.parseColor("#ffffff"));
                    button0.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("0");
                } else {
                    button0.setTextColor(Color.parseColor("#007aff"));
                    button0.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button1)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button1.setTextColor(Color.parseColor("#ffffff"));
                    button1.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("1");
                } else {
                    button1.setTextColor(Color.parseColor("#007aff"));
                    button1.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button2)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button2.setTextColor(Color.parseColor("#ffffff"));
                    button2.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("2");
                } else {
                    button2.setTextColor(Color.parseColor("#007aff"));
                    button2.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button3)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button3.setTextColor(Color.parseColor("#ffffff"));
                    button3.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("3");
                } else {
                    button3.setTextColor(Color.parseColor("#007aff"));
                    button3.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button4)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button4.setTextColor(Color.parseColor("#ffffff"));
                    button4.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("4");
                } else {
                    button4.setTextColor(Color.parseColor("#007aff"));
                    button4.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button5)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button5.setTextColor(Color.parseColor("#ffffff"));
                    button5.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("5");
                } else {
                    button5.setTextColor(Color.parseColor("#007aff"));
                    button5.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button6)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button6.setTextColor(Color.parseColor("#ffffff"));
                    button6.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("6");
                } else {
                    button6.setTextColor(Color.parseColor("#007aff"));
                    button6.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button7)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button7.setTextColor(Color.parseColor("#ffffff"));
                    button7.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("7");
                } else {
                    button7.setTextColor(Color.parseColor("#007aff"));
                    button7.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button8)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button8.setTextColor(Color.parseColor("#ffffff"));
                    button8.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("8");
                } else {
                    button8.setTextColor(Color.parseColor("#007aff"));
                    button8.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }
            if (v.equals(button9)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    button9.setTextColor(Color.parseColor("#ffffff"));
                    button9.setBackgroundResource(R.drawable.sh_password_btn_selected);
                    passwordString = passHandling("9");
                } else {
                    button9.setTextColor(Color.parseColor("#007aff"));
                    button9.setBackgroundResource(R.drawable.sh_password_btn);
                }
            }

            if (v.equals(deleteText)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (passwordString.length() > 0) {
                        passwordString = passwordString.substring(0, passwordString.length() - 1);
                    }
                    passHandling("");
                    deleteText.setTextColor(Color.parseColor("#FF0012D7"));
                } else {
                    deleteText.setTextColor(Color.parseColor("#007AFF"));
                }
            }

            if (v.equals(fingerImg)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    fingerPrintDialog = fingerPrintDialog(PasswordActivity.this);
                    fingerPrintDialog.show();
                    fingerImg.setAlpha(0.5f);
                } else {
                    fingerImg.setAlpha(1f);
                }
            }
            if (v.equals(exit))
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    finish();
            return true;
        };

        button0.setOnTouchListener(listeners);
        button1.setOnTouchListener(listeners);
        button2.setOnTouchListener(listeners);
        button3.setOnTouchListener(listeners);
        button4.setOnTouchListener(listeners);
        button5.setOnTouchListener(listeners);
        button6.setOnTouchListener(listeners);
        button7.setOnTouchListener(listeners);
        button8.setOnTouchListener(listeners);
        button9.setOnTouchListener(listeners);
        deleteText.setOnTouchListener(listeners);
        exit.setOnTouchListener(listeners);
        fingerImg.setOnTouchListener(listeners);

    }

    @Override
    void setAmountTypeFaces() {
        //Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        init();
        setAmountTypeFaces();
        setListeners();

        // set Fingerprint icon Visibility
        if (getIntent().getIntExtra("PassEnteringType", 1) == PassEnteringType.UserWantToSet.value()) {
            exit.setVisibility(View.VISIBLE);
            fingerImg.setVisibility(View.GONE);
        } else {
            exit.setVisibility(View.GONE);
        }

        if (Settings.getSetting(Settings.FINGER_PRINT_IS_ENABLE).equals("1") && getIntent().getIntExtra("PassEnteringType", 1) == PassEnteringType.UserWantToEnter.value()) {
            fingerImg.setVisibility(View.VISIBLE);
        } else {
            fingerImg.setVisibility(View.GONE);
        }
    }

    //handle password validations
    private String passHandling(String number) {
        if (userHasPermission) {

            if (passwordString.length() <= 4) {
                passwordString = passwordString + number;
            }
            if (passwordString.length() == 0) {
                indicator1.setBackgroundResource(R.drawable.sh_password_empty_indicator);
                indicator2.setBackgroundResource(R.drawable.sh_password_empty_indicator);
                indicator3.setBackgroundResource(R.drawable.sh_password_empty_indicator);
                indicator4.setBackgroundResource(R.drawable.sh_password_empty_indicator);
            }
            if (passwordString.length() == 1) {
                indicator1.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator2.setBackgroundResource(R.drawable.sh_password_empty_indicator);
                indicator3.setBackgroundResource(R.drawable.sh_password_empty_indicator);
                indicator4.setBackgroundResource(R.drawable.sh_password_empty_indicator);
            }
            if (passwordString.length() == 2) {
                indicator1.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator2.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator3.setBackgroundResource(R.drawable.sh_password_empty_indicator);
                indicator4.setBackgroundResource(R.drawable.sh_password_empty_indicator);
            }
            if (passwordString.length() == 3) {
                indicator1.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator2.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator3.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator4.setBackgroundResource(R.drawable.sh_password_empty_indicator);
            }
            if (passwordString.length() == 4) {
                indicator1.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator2.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator3.setBackgroundResource(R.drawable.sh_password_full_indicator);
                indicator4.setBackgroundResource(R.drawable.sh_password_full_indicator);

                if (getIntent().getIntExtra("PassEnteringType", 1) == PassEnteringType.UserWantToSet.value()) {
                    Settings.setSetting(Settings.PASSWORD, passwordString);
                    setResult(RESULT_OK);
                    new CountDownTimer(700, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            finish();
                        }
                    }.start();
                }
                if (getIntent().getIntExtra("PassEnteringType", 1) == PassEnteringType.UserWantToEnter.value()) {
                    String userPass = Settings.getSetting(Settings.PASSWORD);
                    if (passwordString.equals(userPass)) {
                        finish();
                    } else {
                        counter++;
                        if (counter == 3) {
                            banTextViewContainer.setVisibility(View.VISIBLE);
                            indicatorsContainer.setVisibility(View.GONE);
                            banCountDownTimer.start();
                            userHasPermission = false;
                        } else {
                            Animation shake = AnimationUtils.loadAnimation(PasswordActivity.this, R.anim.shake);
                            makeDefault();
                            indicatorsContainer.startAnimation(shake);
                        }
                    }
                }
            }
        }
        banCountDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                banCountDown.setText(millisUntilFinished / 1000 + " ثانیه");
            }

            @Override
            public void onFinish() {
                banTextViewContainer.setVisibility(View.GONE);
                indicatorsContainer.setVisibility(View.VISIBLE);
                userHasPermission = true;
                counter = 0;
                makeDefault();
            }
        };
        return passwordString;
    }

    //handle fingerprint validations
    private void fingerPrintHandling(final FingerPrintCallBack fingerPrintCallBack) {

        disposable = RxFingerprint.authenticate(this)
                .subscribe(fingerprintAuthenticationResult -> {
                    switch (fingerprintAuthenticationResult.getResult()) {
                        case FAILED:
                            fingerPrintCallBack.call(false);
                            break;
                        case AUTHENTICATED:
                            fingerPrintCallBack.call(true);
                            finish();
                            break;
                    }
                }, throwable -> Log.e("ERROR", "authenticate", throwable));

    }

    //resets indicator and passwors string to default and empty
    private void makeDefault() {
        passwordString = "";
        indicator1.setBackgroundResource(R.drawable.sh_password_empty_indicator);
        indicator2.setBackgroundResource(R.drawable.sh_password_empty_indicator);
        indicator3.setBackgroundResource(R.drawable.sh_password_empty_indicator);
        indicator4.setBackgroundResource(R.drawable.sh_password_empty_indicator);
    }

    // fingerprint dialog
    private Dialog fingerPrintDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View passwordLayout = inflater.inflate(R.layout.pop_finger_print, null);

        final TextView cancel;
        cancel = passwordLayout.findViewById(R.id.dialog_finger_cancel);
        tryAgainText = passwordLayout.findViewById(R.id.pop_finger_print_try_again_text);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 35;
                break;

            case 2:
                width = 67;
                height = 26;
                break;

            case 3:
                width = 55;
                height = 22;
                break;

        }

        final Dialog faDialog = PublicDialogs.makeDialog(context, passwordLayout, width, height, false);

        fingerPrintHandling(isOK -> {
            if (isOK) {
                tryAgainText.setVisibility(View.INVISIBLE);
            } else {
                Animation shake = AnimationUtils.loadAnimation(PasswordActivity.this, R.anim.shake);
                tryAgainText.setVisibility(View.VISIBLE);
                tryAgainText.startAnimation(shake);

            }
        });

        View.OnClickListener dialogListeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(cancel)) {
                    tryAgainText.setVisibility(View.INVISIBLE);
                    faDialog.dismiss();
                }
            }
        };

        cancel.setOnClickListener(dialogListeners);

        return faDialog;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy(); // age mikhad biad tu app dg niazy nist ke setting ro dorost kone !
        if (getIntent().getIntExtra("PassEnteringType", 10) == PassEnteringType.UserWantToSet.value())
            SettingActivity.checkFingerPrintAvailability();
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getIntExtra("PassEnteringType", 10) == PassEnteringType.UserWantToSet.value())
            super.onBackPressed();

    }
}
