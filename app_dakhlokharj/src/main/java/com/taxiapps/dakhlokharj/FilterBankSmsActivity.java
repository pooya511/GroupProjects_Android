package com.taxiapps.dakhlokharj;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import adapters.BankSmsNumberAdapter;
import model.Settings;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.AppModules;
import utils.DialogUtils;

public class FilterBankSmsActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private ImageView addNumber;
    private ConstraintLayout dontHaveNumberLayout;

    public ArrayList<String> numbersStringArray = new ArrayList<>();
    private BankSmsNumberAdapter bankSmsNumberAdapter;
    public int arrayPosition;

    @Override
    Context context() {
        return FilterBankSmsActivity.this;
    }

    @Override
    void init() {
        recyclerView = findViewById(R.id.activity_filter_bank_sms_recycler_view);
        addNumber = findViewById(R.id.activity_filter_bank_sms_new_number_image);
        dontHaveNumberLayout = findViewById(R.id.activity_filter_bank_sms_dont_have_number);
        numbersStringArray = refreshArray();
    }

    @Override
    void setListeners() {
        addNumber.setOnClickListener(v -> {
            Dialog dialog = addPreNumberDialog(FilterBankSmsActivity.this, "");
            dialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
            dialog.show();
        });
    }

    @Override
    void setAmountTypeFaces() {
        //nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_bank_sms);
        init();
        setListeners();
        setAmountTypeFaces();

        recyclerView.setLayoutManager(new LinearLayoutManager(FilterBankSmsActivity.this));
        bankSmsNumberAdapter = new BankSmsNumberAdapter(FilterBankSmsActivity.this);
        if (numbersStringArray.size() > 0) {
            checkArrayItems(false);
            recyclerView.setAdapter(bankSmsNumberAdapter);
        } else{
            checkArrayItems(true);
        }
    }

    public void checkArrayItems(boolean isSizeZero) {

        if (isSizeZero) {
            dontHaveNumberLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            dontHaveNumberLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

    public Dialog addPreNumberDialog(final Context context, final String editTextValue) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View addNewNumberLayout = inflater.inflate(R.layout.pop_add_new_number, null);

        final LinearLayout submit, cancel;
        final EditText numberEditText;
        final TextView errorText;

        submit = addNewNumberLayout.findViewById(R.id.pop_add_new_number_submit);
        cancel = addNewNumberLayout.findViewById(R.id.pop_add_new_number_cancel);
        numberEditText = addNewNumberLayout.findViewById(R.id.pop_add_new_number_edittext);
        errorText = addNewNumberLayout.findViewById(R.id.pop_add_new_number_error_text);

        if (editTextValue.startsWith("+98")){
            numberEditText.setText(editTextValue.substring(3 , editTextValue.length()));
            numberEditText.setSelection(numberEditText.getText().length());
        }
        else
            numberEditText.setText(editTextValue);


        AppModules.showSoftKeyboard(context,numberEditText);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 86;
                height = 24;
                break;

            case 2:
                width = 67;
                height = 18;
                break;

            case 3:
                width = 67;
                height = 16;
                break;

        }

        final Dialog addNewNumberDialog = PublicDialogs.makeDialog(context, addNewNumberLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel)) {
                addNewNumberDialog.dismiss();
                AppModules.hideSoftKeyboard(context,numberEditText);
            }

            if (v.equals(submit)) {
                if (numberEditText.getText().toString().equals("")) {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText("لطفا جای خالی را پر کنید");
                } else {
                    AppModules.hideSoftKeyboard(context,numberEditText);
                    if (isExist("+98" + numberEditText.getText().toString())) {
                        errorText.setVisibility(View.VISIBLE);
                        errorText.setText("پیش شماره تکراری است");
                    } else {
                        if (editTextValue.equals("")) {
                            errorText.setVisibility(View.GONE);
                            String numberString = numberEditText.getText().toString();

                            if (numberString.startsWith("0"))
                                numberString = numberString.substring(1, numberString.length());

                            numbersStringArray.add("+98" + numberString);

                            Set<String> set = new LinkedHashSet<>(numbersStringArray);
                            Settings.setStringSet(Settings.SMS_NUMBERS, set);
                            numbersStringArray = refreshArray();
                            addNewNumberDialog.dismiss();
                        } else {
                            String numberString = numberEditText.getText().toString();
                            if (numberString.startsWith("0"))
                                numberString = numberString.substring(1, numberString.length());

                            String formatedString = "+98" + numberString;

                            numbersStringArray.set(arrayPosition , formatedString);
                            Set<String> set = new LinkedHashSet<>(numbersStringArray);
                            Settings.setStringSet(Settings.SMS_NUMBERS, set);
                            numbersStringArray = refreshArray();
                            addNewNumberDialog.dismiss();
                        }

                    }

                    if (numbersStringArray.size() > 0)
                        checkArrayItems(false);

                    bankSmsNumberAdapter.notifyDataSetChanged();
                }
            }
        };

        cancel.setOnClickListener(dialogListeners);
        submit.setOnClickListener(dialogListeners);

        return addNewNumberDialog;
    }

    private boolean isExist(String s) {
        for (int i = 0; i < numbersStringArray.size(); i++) {
            if (s.equals(numbersStringArray.get(i)))
                return true;
        }
        return false;
    }

    public ArrayList<String> refreshArray() {
        Set<String> set = Settings.getStringSet(Settings.SMS_NUMBERS);
        return new ArrayList<>(set);
    }

}
