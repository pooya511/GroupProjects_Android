package com.taxiapps.dakhlokharj;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import adapters.ExpensesAndIncomesListAdapter;
import adapters.SMSAdapter;
import at.markushi.ui.CircleButton;
import dialog_fragments.DatePicker;
import dialog_fragments.CategoryDialogFragment;
import model.Account;
import model.Currency;
import model.EnumsAndConstants;
import model.ExpenseOrIncomeItem;
import model.Group;
import model.GroupDetail;
import model.Payee;
import model.PhotoProvider;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;
import utils.DialogUtils;

public class AddAndEditExpensesAndIncomesActivity extends BaseActivity {

    private Uri mImageUri;
    private Animation shake;

    private interface RemovePictureCallBack {
        void call();
    }

    private final int REQUEST_IMAGE_CAPTURE = 1;
    private final int PICK_IMAGE = 2;
    private final int CAMERA_PERMISSION_AND_STORAGE_PERMISSION = 10;

    private TextView subCategoryIcon, header, subCategory, category, amount, amountCurrency, date, timeTextView, payeeName, accountName, removeImageTextView, replaceImageTextView, saveTxtView, payeeTitle, accountTitle;
    private ConstraintLayout remove, categoryCons, priceCons, dateCons, timeCons, payToCons, fromAccountCons, pictureCons, attachedImageLayout, notAttachedImageLayout;
    private ImageView ques1, ques2, imageToAttach, removePayee, removeAccount;
    private EditText description;
    private CircleButton tag1, tag2, tag3, tag4, tag5, tag6;
    private ScrollView scrollView;
    public static ProgressBar progressBar;

    private ExpenseOrIncomeItem item;
    private Transaction tempTransaction, transaction;

    private Dialog choosePicture;
    private Dialog removePicture;

    private String groupName;

    private ArrayList<View> tagArray = new ArrayList<>();
    private EnumsAndConstants.TransactionTypes type;

    private String tempTrnImage; // path e movaghat baray trnPicture e. baray zamanie ke pak kone vali save nakone.

    public enum IntentTypes {

        NewExpense(1),
        EditExpense(2),
        NewIncome(3),
        EditIncome(4),
        SmsMode(5),
        Widget(6);

        private int type;

        IntentTypes(int type) {
            this.type = type;
        }

        public int value() {
            return this.type;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == AccountListActivity.REQUEST_CODE_ADD_ACCOUNT && resultCode == RESULT_OK) {
            fromAccountCons.callOnClick();
        }

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            final Bitmap imageBitmap = grabImage(AddAndEditExpensesAndIncomesActivity.this);

            File file = new File(mImageUri.getPath());
            file.delete(); // aks e asli bayad pak she . inja pakesh mikonim

            imageToAttach.setImageBitmap(imageBitmap);
            if (transaction.getTrnDate() != null) {
                transaction.setTrnImagePath(createImageFile(imageBitmap, String.valueOf(Long.valueOf(transaction.getTrnDate()))));
            } else {
                transaction.setTrnImagePath(createImageFile(imageBitmap, String.valueOf(new Date().getTime())));
            }
            uiHandling(true);
            choosePicture.dismiss();
        }

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                bitmap = Bitmap.createScaledBitmap(bitmap, 450, 650, true);
                imageToAttach.setImageBitmap(bitmap);
                if (transaction.getTrnDate() != null)
                    transaction.setTrnImagePath(createImageFile(bitmap, String.valueOf(Long.valueOf(transaction.getTrnDate()))));
                else
                    transaction.setTrnImagePath(createImageFile(bitmap, String.valueOf(new Date().getTime())));
                uiHandling(true);
                choosePicture.dismiss();
            } catch (IOException e) {
                PublicDialogs.makeMessageDialog(AddAndEditExpensesAndIncomesActivity.this, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "متاسفانه امکان انتخاب فایل مورد نظر نیست!", "", EnumsAndConstants.APP_SOURCE).show();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION_AND_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    captureImage(AddAndEditExpensesAndIncomesActivity.this);
                else
                    ActivityCompat.requestPermissions(AddAndEditExpensesAndIncomesActivity.this,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                            CAMERA_PERMISSION_AND_STORAGE_PERMISSION);
                break;
        }
    }

    @Override
    Context context() {
        return AddAndEditExpensesAndIncomesActivity.this;
    }

    @Override
    void init() {

        tempTrnImage = "";

        remove = findViewById(R.id.add_expenses_and_incomes_activity_remove_layout);
        header = findViewById(R.id.add_expenses_and_incomes_activity_title_textview);
        ques1 = findViewById(R.id.add_expenses_and_incomes_activity_question1_icon);
        ques2 = findViewById(R.id.add_expenses_and_incomes_activity_question2_icon);
        subCategory = findViewById(R.id.add_expenses_and_incomes_activity_category_textview);
        category = findViewById(R.id.add_expenses_and_incomes_activity_sub_category_textview);
        amount = findViewById(R.id.add_expenses_and_incomes_activity_amount_textview);
        amountCurrency = findViewById(R.id.add_expenses_and_incomes_activity_currency_textview);
        date = findViewById(R.id.add_expenses_and_incomes_activity_date_textview);
        timeTextView = findViewById(R.id.add_expenses_and_incomes_activity_time_textview);
        description = findViewById(R.id.add_expenses_and_incomes_activity_description_edittext);
        subCategoryIcon = findViewById(R.id.add_expenses_and_incomes_activity_category_icon);
        payeeName = findViewById(R.id.add_expenses_and_incomes_activity_payee_name);
        accountName = findViewById(R.id.add_expenses_and_incomes_activity_account_name);
        payeeTitle = findViewById(R.id.act_add_ex_and_in_payee_title);
        accountTitle = findViewById(R.id.act_add_ex_and_in_account_title);
        pictureCons = findViewById(R.id.add_expenses_and_incomes_activity_attach_picture);
        saveTxtView = findViewById(R.id.add_expenses_and_incomes_activity_save_textview);
        removePayee = findViewById(R.id.add_expenses_and_incomes_activity_payee_name_remove);
        removeAccount = findViewById(R.id.add_expenses_and_incomes_activity_account_name_remove);
        scrollView = findViewById(R.id.act_add_expense_and_incomes_scroll_view);
        progressBar = findViewById(R.id.add_expenses_and_incomes_activity_loading);

        tag1 = findViewById(R.id.add_expenses_and_incomes_activity_tag1_button);
        tag2 = findViewById(R.id.add_expenses_and_incomes_activity_tag2_button);
        tag3 = findViewById(R.id.add_expenses_and_incomes_activity_tag3_button);
        tag4 = findViewById(R.id.add_expenses_and_incomes_activity_tag4_button);
        tag5 = findViewById(R.id.add_expenses_and_incomes_activity_tag5_button);
        tag6 = findViewById(R.id.add_expenses_and_incomes_activity_tag6_button);

        tagArray.add(tag1);
        tagArray.add(tag2);
        tagArray.add(tag3);
        tagArray.add(tag4);
        tagArray.add(tag5);
        tagArray.add(tag6);

        categoryCons = findViewById(R.id.add_expenses_and_incomes_activity_select_category_layout);
        priceCons = findViewById(R.id.add_expenses_and_incomes_activity_select_amount_layout);
        dateCons = findViewById(R.id.add_expenses_and_incomes_activity_select_date_layout);
        timeCons = findViewById(R.id.add_expenses_and_incomes_activity_select_time_layout);
        payToCons = findViewById(R.id.add_expenses_and_incomes_activity_select_payee_layout);
        fromAccountCons = findViewById(R.id.add_expenses_and_incomes_activity_select_account_layout);

        attachedImageLayout = findViewById(R.id.add_expenses_and_incomes_activity_attached_image_layout);
        notAttachedImageLayout = findViewById(R.id.add_expenses_and_incomes_activity_not_attached_image_layout);
        imageToAttach = findViewById(R.id.add_expenses_and_incomes_activity_image_to_attach);
        removeImageTextView = findViewById(R.id.add_expenses_and_incomes_activity_remove_image_textview);
        replaceImageTextView = findViewById(R.id.add_expenses_and_incomes_activity_replace_image_textview);

        amountCurrency.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));

        subCategoryIcon = AppModules.setTypeFace(AddAndEditExpensesAndIncomesActivity.this, subCategoryIcon, "fontawesome_webfont.ttf");

        PersianDate pc;
        PersianDateFormat dateFormatter = new PersianDateFormat("Y/m/d");
        PersianDateFormat timeFormatter = new PersianDateFormat("H:i");

        switch (getIntent().getIntExtra("Type", 0)) {

            case 1:

                transaction = new Transaction();

                transaction.setTrnColor("#B8B8B8");
                transaction.setTrnImagePath("");

                header.setText("هزینه جدید");
                header.setTextColor(Color.BLACK);
                payeeTitle.setText(R.string.add_and_edit_ex_payee_title);
                accountTitle.setText(R.string.add_and_edit_ex_account_title);
                remove.setVisibility(View.GONE);
                type = EnumsAndConstants.TransactionTypes.Expense;

                if (ExpensesOrIncomeListActivity.getInstance() != null) {
                    if (ExpensesOrIncomeListActivity.getInstance().staticPersianDate.getShMonth() == new PersianDate().getShMonth())
                        date.setText(dateFormatter.format(new PersianDate()));
                    else
                        date.setText(dateFormatter.format(ExpensesOrIncomeListActivity.getInstance().staticPersianDate));
                } else {
                    date.setText(dateFormatter.format(new PersianDate()));
                }

                timeTextView.setText(new SimpleDateFormat("HH:mm ").format(new Date()));

                break;

            case 2:

                item = getIntent().getParcelableExtra("Item");
                transaction = Transaction.getTransaction(item.getID());

                header.setText("ویرایش هزینه");
                groupName = Group.getGroupNameWithLsdID(transaction.getIsdID());
                subCategory.setText(item.getTitle());
                subCategoryIcon.setText(AppModules.convertIconFromString(AddAndEditExpensesAndIncomesActivity.this, item.getIcon()));
                subCategoryIcon.setTextColor(Color.parseColor(item.getIconColor()));
                category.setText("(" + groupName + ")");
                amount.setText(Currency.CurrencyString.getCurrencyString(transaction.getTrnAmount(), null).separated);
                amountCurrency.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                payeeTitle.setText(R.string.add_and_edit_ex_payee_title);
                accountTitle.setText(R.string.add_and_edit_ex_account_title);

                if (Account.getAccount(transaction.getAcnId()).getAcnName() == null)
                    accountName.setText("انتخاب حساب");
                else
                    accountName.setText(Account.getAccount(transaction.getAcnId()).getAcnName());

                if (Payee.getPayee(transaction.getPyeId()).getPyeName() == null)
                    payeeName.setText("انتخاب طرف حساب");
                else
                    payeeName.setText(Payee.getPayee(transaction.getPyeId()).getPyeName());

                pc = new PersianDate(Long.valueOf(transaction.getTrnDate()));
                date.setText(dateFormatter.format(pc));
                timeTextView.setText(timeFormatter.format(pc));

                description.setText(transaction.getTrnDescription());

                if (transaction.getTrnImagePath() != null && !transaction.getTrnImagePath().equals("")) {
                    uiHandling(true);
                    imageToAttach.setImageBitmap(AppModules.getImageFromFilePath(transaction.getTrnImagePath()));
                } else
                    uiHandling(false);

                type = EnumsAndConstants.TransactionTypes.Expense;

                if (transaction.getTrnColor().equals("null"))
                    transaction.setTrnColor("#B8B8B8");

                switch (transaction.getTrnColor()) {

                    case "#FC635A":
                        tagHandling(tag1);
                        break;
                    case "#FCAF39":
                        tagHandling(tag2);
                        break;
                    case "#F9E53D":
                        tagHandling(tag3);
                        break;
                    case "#C3E742":
                        tagHandling(tag4);
                        break;
                    case "#56A5FB":
                        tagHandling(tag5);
                        break;
                    default:
                        tagHandling(tag6);

                }

                break;

            case 3:

                transaction = new Transaction();

                transaction.setTrnColor("#B8B8B8");
                transaction.setTrnImagePath("");

                header.setText("درآمد جدید");
                header.setTextColor(Color.BLACK);
                payeeTitle.setText(R.string.add_and_edit_in_payee_title);
                accountTitle.setText(R.string.add_and_edit_in_account_title);
                remove.setVisibility(View.GONE);
                type = EnumsAndConstants.TransactionTypes.Income;

                if (ExpensesOrIncomeListActivity.getInstance() != null) {
                    if (ExpensesOrIncomeListActivity.getInstance().staticPersianDate.getShMonth() == new PersianDate().getShMonth())
                        date.setText(dateFormatter.format(new PersianDate()));
                    else
                        date.setText(dateFormatter.format(ExpensesOrIncomeListActivity.getInstance().staticPersianDate));
                } else {
                    date.setText(dateFormatter.format(new PersianDate()));
                }

                timeTextView.setText(new SimpleDateFormat("HH:mm").format(new Date()));

                break;

            case 4:

                item = getIntent().getParcelableExtra("Item");
                transaction = Transaction.getTransaction(item.getID());

                header.setText("ویرایش درآمد");
                groupName = getIntent().getStringExtra("CategoryName");
                subCategory.setText(item.getTitle());
                subCategoryIcon.setText(AppModules.convertIconFromString(AddAndEditExpensesAndIncomesActivity.this, item.getIcon()));
                subCategoryIcon.setTextColor(Color.parseColor(item.getIconColor()));
                category.setText("(" + groupName + ")");
                amount.setText(Currency.CurrencyString.getCurrencyString(transaction.getTrnAmount(), null).separated);
                amountCurrency.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                accountName.setText(Account.getAccount(transaction.getAcnId()).getAcnName());
                payeeName.setText(Payee.getPayee(transaction.getPyeId()).getPyeName());
                payeeTitle.setText(R.string.add_and_edit_in_payee_title);
                accountTitle.setText(R.string.add_and_edit_in_account_title);

                if (Account.getAccount(transaction.getAcnId()).getAcnName() == null)
                    accountName.setText("انتخاب حساب");
                else
                    accountName.setText(Account.getAccount(transaction.getAcnId()).getAcnName());

                if (Payee.getPayee(transaction.getPyeId()).getPyeName() == null)
                    payeeName.setText("انتخاب طرف حساب");
                else
                    payeeName.setText(Payee.getPayee(transaction.getPyeId()).getPyeName());

                pc = new PersianDate(Long.valueOf(transaction.getTrnDate()));
                date.setText(dateFormatter.format(pc));
                timeTextView.setText(timeFormatter.format(pc));

                description.setText(transaction.getTrnDescription());

                if (transaction.getTrnImagePath() != null && !transaction.getTrnImagePath().equals("")) {
                    uiHandling(true);
                    imageToAttach.setImageBitmap(AppModules.getImageFromFilePath(transaction.getTrnImagePath()));
                } else
                    uiHandling(false);

                type = EnumsAndConstants.TransactionTypes.Income;

                switch (transaction.getTrnColor()) {

                    case "#FC635A":
                        tagHandling(tag1);
                        break;
                    case "#FCAF39":
                        tagHandling(tag2);
                        break;
                    case "#F9E53D":
                        tagHandling(tag3);
                        break;
                    case "#C3E742":
                        tagHandling(tag4);
                        break;
                    case "#56A5FB":
                        tagHandling(tag5);
                        break;
                    default:
                        tagHandling(tag6);

                }

                break;

            case 5:

                transaction = SMSAdapter.getInstance().transactionList.get(getIntent().getIntExtra("position", 0));

                type = transaction.getTrnType() == 1 ? EnumsAndConstants.TransactionTypes.Expense : EnumsAndConstants.TransactionTypes.Income;
                if (type == EnumsAndConstants.TransactionTypes.Expense) {
                    header.setText("ویرایش هزینه");
                    payeeTitle.setText(R.string.add_and_edit_ex_payee_title);
                    accountTitle.setText(R.string.add_and_edit_ex_account_title);
                }
                if (type == EnumsAndConstants.TransactionTypes.Income) {
                    header.setText("ویرایش درآمد");
                    payeeTitle.setText(R.string.add_and_edit_in_payee_title);
                    accountTitle.setText(R.string.add_and_edit_in_account_title);
                }

                if (transaction.getIsdID() != -1) {
                    GroupDetail tempGD = GroupDetail.getGroupDetailByID(transaction.getIsdID());
                    subCategory.setText(tempGD.getLsdName());
                    subCategoryIcon.setText(AppModules.convertIconFromString(AddAndEditExpensesAndIncomesActivity.this, tempGD.getLsdIcon()));
                    subCategoryIcon.setTextColor(Color.parseColor(tempGD.getLsdColor()));
                    category.setText(Group.getGroupNameWithLsdID(tempGD.getID()));
                }
                amount.setText(Currency.CurrencyString.getCurrencyString(transaction.getTrnAmount(), null).separated);

                pc = new PersianDate(Long.valueOf(transaction.getTrnDate()));
                date.setText(dateFormatter.format(pc));
                timeTextView.setText(timeFormatter.format(pc));

                accountName.setText(Account.getAccount(transaction.getAcnId()).getAcnName());
                payeeName.setText(Payee.getPayee(transaction.getPyeId()).getPyeName());
                amountCurrency.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                description.setText(transaction.getTrnDescription());

                if (Account.getAccount(transaction.getAcnId()).getAcnName() == null)
                    accountName.setText("انتخاب حساب");
                else
                    accountName.setText(Account.getAccount(transaction.getAcnId()).getAcnName());

                if (Payee.getPayee(transaction.getPyeId()).getPyeName() == null)
                    payeeName.setText("انتخاب طرف حساب");
                else
                    payeeName.setText(Payee.getPayee(transaction.getPyeId()).getPyeName());

                remove.setVisibility(View.GONE);
                saveTxtView.setText("ثبت");

                if (transaction.getTrnImagePath() != null) {
                    uiHandling(true);
                    imageToAttach.setImageBitmap(AppModules.getImageFromFilePath(transaction.getTrnImagePath()));
                } else
                    uiHandling(false);

                if (transaction.getTrnColor() == null)
                    transaction.setTrnColor("#B8B8B8");

                switch (transaction.getTrnColor()) {

                    case "#FC635A":
                        tagHandling(tag1);
                        break;
                    case "#FCAF39":
                        tagHandling(tag2);
                        break;
                    case "#F9E53D":
                        tagHandling(tag3);
                        break;
                    case "#C3E742":
                        tagHandling(tag4);
                        break;
                    case "#56A5FB":
                        tagHandling(tag5);
                        break;
                    default:
                        tagHandling(tag6);

                }

                break;

            case 6:

                transaction = new Transaction();
                type = getIntent().getStringExtra("trnType").equals("1") ? EnumsAndConstants.TransactionTypes.Expense : EnumsAndConstants.TransactionTypes.Income;

                GroupDetail tempGD = GroupDetail.getGroupDetailByID(getIntent().getIntExtra("groupDetailID", 0));
                subCategory.setText(tempGD.getLsdName());
                subCategoryIcon.setText(AppModules.convertIconFromString(AddAndEditExpensesAndIncomesActivity.this, tempGD.getLsdIcon()));
                subCategoryIcon.setTextColor(Color.parseColor(tempGD.getLsdColor()));
                category.setText("(" + Group.getGroupNameWithLsdID(tempGD.getID()) + ")");

                transaction.setIsdID(tempGD.getID());

                if (Account.getAccount(transaction.getAcnId()).getAcnName() == null)
                    accountName.setText("انتخاب حساب");
                else
                    accountName.setText(Account.getAccount(transaction.getAcnId()).getAcnName());

                if (Payee.getPayee(transaction.getPyeId()).getPyeName() == null)
                    payeeName.setText("انتخاب طرف حساب");
                else
                    payeeName.setText(Payee.getPayee(transaction.getPyeId()).getPyeName());

                transaction.setTrnColor("#B8B8B8");
                transaction.setTrnImagePath("");

                if (type == EnumsAndConstants.TransactionTypes.Income) {
                    header.setText("درآمد جدید");
                    payeeTitle.setText(R.string.add_and_edit_in_payee_title);
                    accountTitle.setText(R.string.add_and_edit_in_account_title);
                } else if (type == EnumsAndConstants.TransactionTypes.Expense) {
                    header.setText("هزینه جدید");
                    payeeTitle.setText(R.string.add_and_edit_ex_payee_title);
                    accountTitle.setText(R.string.add_and_edit_ex_account_title);
                }

                header.setTextColor(Color.BLACK);
                remove.setVisibility(View.GONE);
                date.setText(dateFormatter.format(new PersianDate()));
                timeTextView.setText(PublicModules.timeStamp2String(new PersianDate().getTime(), "hh:mm"));

                break;
        }

        this.tempTransaction = transaction.clone();
    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = new View.OnClickListener() {

            private Dialog accountDialog;
            private Dialog keyPad;
            private DatePicker datePicker;
            private Dialog timePicker;
            private Dialog payeeSelect;

            @Override
            public void onClick(View v) {

                switch (v.getId()) {

                    case R.id.add_expenses_and_incomes_activity_question1_icon:
                        payToQuestionMark(AddAndEditExpensesAndIncomesActivity.this).show();
                        break;

                    case R.id.add_expenses_and_incomes_activity_question2_icon:
                        fromAccountQuestionMark(AddAndEditExpensesAndIncomesActivity.this).show();
                        break;

                    case R.id.add_expenses_and_incomes_activity_select_category_layout:
                        progressBar.setVisibility(View.VISIBLE);
                        CategoryDialogFragment.newInstance(type, new CategoryDialogFragment.SelectedCategoriesByUserCallBack() {
                            @Override
                            public void call(Group group, GroupDetail groupDetail) {
                                transaction.setIsdID(groupDetail.getID());

                                subCategory.setTextColor(Color.parseColor("#3295f3"));
                                subCategory.setText(groupDetail.getLsdName());
                                category.setText("(" + group.getLstName() + ")");
                                subCategoryIcon.setTextColor(Color.parseColor(groupDetail.getLsdColor()));
                                subCategoryIcon.setText(AppModules.convertIconFromString(AddAndEditExpensesAndIncomesActivity.this, groupDetail.getLsdIcon()));
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        }).show(getFragmentManager(), null);
                        break;

                    case R.id.add_expenses_and_incomes_activity_select_amount_layout:
                        keyPad = DialogUtils.keypadDialog(AddAndEditExpensesAndIncomesActivity.this, amount.getText().toString(), number -> {
                            amount.setTextColor(Color.parseColor("#3295f3"));
                            amount.setText(Currency.CurrencyString.getCurrencyString(number, null).separated);
                            transaction.setTrnAmount(PublicModules.toEnglishDigit(number.replace(",", "")));
                            keyPad.dismiss();
                        });
                        keyPad.show();
                        break;

                    case R.id.add_expenses_and_incomes_activity_select_date_layout:
                        datePicker = DatePicker.newInstance(Long.valueOf(PublicModules.string2TimeStamp(date.getText().toString())), new DatePicker.DatePickerCallBack() {
                            @Override
                            public void call(PersianDate persianCalendar) {
                                date.setText(PersianDateFormat.format(persianCalendar, "Y/m/d"));
                                //  transaction.setTrnDate(AppModules.string2TimeStamp(date.getText().toString()));
                                datePicker.dismiss();
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        });
                        datePicker.show(getFragmentManager(), null);
                        break;

                    case R.id.add_expenses_and_incomes_activity_select_time_layout:
                        timePicker = DialogUtils.timePickerDialog(AddAndEditExpensesAndIncomesActivity.this, timeTextView.getText().toString(), time -> {
                            timeTextView.setText(time);
                            timePicker.dismiss();
                        });
                        timePicker.show();
                        break;

                    case R.id.add_expenses_and_incomes_activity_select_payee_layout:
                        payeeSelect = Payee.payeeList(AddAndEditExpensesAndIncomesActivity.this, payee -> {
                            payeeName.setText(payee.getPyeName());
                            transaction.setPyeId(payee.getID());
                            payeeSelect.dismiss();

                            removePayee.setVisibility(View.VISIBLE);
                        });
                        payeeSelect.show();
                        break;

                }

                if (v.equals(tag1) || v.equals(tag2) || v.equals(tag3) || v.equals(tag4) || v.equals(tag5) || v.equals(tag6)) {
                    tagHandling(v);

                    if (v.equals(tag1))
                        transaction.setTrnColor("#FC635A");
                    if (v.equals(tag2))
                        transaction.setTrnColor("#FCAF39");
                    if (v.equals(tag3))
                        transaction.setTrnColor("#F9E53D");
                    if (v.equals(tag4))
                        transaction.setTrnColor("#C3E742");
                    if (v.equals(tag5))
                        transaction.setTrnColor("#56A5FB");
                    if (v.equals(tag6))
                        transaction.setTrnColor("#B8B8B8");
                }

                if (v.equals(fromAccountCons)) {
                    ArrayList<Account> activeAccounts = Account.getAllActive();
                    accountDialog = Account.accountList(AddAndEditExpensesAndIncomesActivity.this, activeAccounts, null, account -> {
                        accountName.setText(account.getAcnName());
                        transaction.setAcnId(account.getID());
                        accountDialog.dismiss();

                        removeAccount.setVisibility(View.VISIBLE);
                    });
                    accountDialog.show();
                }

                if (v.equals(pictureCons)) {
                    choosePicture.show();
                }

                if (v.equals(replaceImageTextView)) {
                    choosePicture.show();
                }

                if (v.equals(removeImageTextView)) {
                    removePicture = removePictureDialog(AddAndEditExpensesAndIncomesActivity.this, () -> {

//                            File imageFile = new File(transaction.getTrnImagePath());
//                            if (imageFile.exists())
//                                imageFile.delete();

                        // transaction.setTrnImagePath(null);
                        tempTrnImage = transaction.getTrnImagePath();
//                            transaction.setTrnImagePath("");
                        uiHandling(false);
                        removePicture.dismiss();
                    });
                    removePicture.show();
                }

                if (v.equals(remove)) {
                    DialogUtils.removeTransactionDialog(AddAndEditExpensesAndIncomesActivity.this, () -> {
                        if (transaction.getTrnImagePath() != null) {
                            File file = new File(transaction.getTrnImagePath());
                            if (file.exists())
                                file.delete();
                        }

                        transaction.delete();

                        ExpensesAndIncomesListAdapter.getInstance().reloadData(type);
                        MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                        finish();
                    }).show();
                }

                if (v.equals(saveTxtView)) {

                    if (transaction.getIsdID() != -1) { // check mikonim subcategory entekhab karde ya na
                        if (transaction.getTrnAmount().equals("-1") || transaction.getTrnAmount().equals("0")) { // transaction amount nabayad 0 bashad
                            amount.setTextColor(Color.RED);
                            amount.startAnimation(shake);
                        } else {
                            generateTransaction();

                            if (getIntent().getIntExtra("Type", 0) == IntentTypes.SmsMode.value()) { // az sms ha oomade ya na

                                SMSAdapter.getInstance().transactionList.set(getIntent().getIntExtra("position", 0), transaction);
                                SMSAdapter.getInstance().notifyDataSetChanged();
                                finish();

                            } else {
                                if (!tempTrnImage.equals("")) {// check mikonim yaru aksesho pak karde ya na . age tempTrnImage por bud yani pak karde
                                    transaction.setTrnImagePath("");
                                    File imageFile = new File(tempTrnImage);
                                    if (imageFile.exists()) {
                                        imageFile.delete(); // az tu storage pakesh mikonim
                                    }
                                }
                                if (transaction.getID() == -1) { // az inja mifahmim yaru mikhad edit kone ya add kone . age -1 bashe yani mikhad add kone

                                    transaction.setTrnType(type.value());

                                    transaction.insert();
                                    customToast("اضافه کردن تراکنش", "تراکنش جدید اضافه شد");
                                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                    try {
                                        ExpensesAndIncomesListAdapter.getInstance().reloadData(type);
                                    } catch (NullPointerException e) {


                                    }
                                    finish();
                                } else {
                                    transaction.update(new String[]{Transaction.TransactionCol.lsdID.value(),
                                            Transaction.TransactionCol.trnAmount.value(),
                                            Transaction.TransactionCol.trnDate.value(),
                                            Transaction.TransactionCol.trnColor.value(),
                                            Transaction.TransactionCol.acnID.value(),
                                            Transaction.TransactionCol.pyeID.value(),
                                            Transaction.TransactionCol.trnDescription.value(),
                                            Transaction.TransactionCol.trnImagePath.value(),});
                                    customToast("ویرایش تراکنش", "تراکنش ویرایش شد");
                                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                                    if (ExpensesAndIncomesListAdapter.getInstance() != null)
                                        ExpensesAndIncomesListAdapter.getInstance().reloadData(type);
                                    finish();
                                }
                            }
                        }

                    } else {
                        subCategory.setTextColor(Color.RED);
                        subCategory.startAnimation(shake);
                    }

                }

                if (v.equals(imageToAttach)) {
                    showTransactionImage(AddAndEditExpensesAndIncomesActivity.this, transaction.getTrnImagePath()).show();
                }

                if (v.equals(removePayee)) {
                    payeeName.setText(R.string.choose_payee);
                    transaction.setPyeId(0);

                    removePayee.setVisibility(View.INVISIBLE);
                }

                if (v.equals(removeAccount)) {
                    accountName.setText(R.string.choose_account);
                    transaction.setAcnId(0);

                    removeAccount.setVisibility(View.INVISIBLE);
                }

            }
        };

        ques1.setOnClickListener(listeners);
        ques2.setOnClickListener(listeners);
        categoryCons.setOnClickListener(listeners);
        priceCons.setOnClickListener(listeners);
        dateCons.setOnClickListener(listeners);
        timeCons.setOnClickListener(listeners);
        payToCons.setOnClickListener(listeners);
        fromAccountCons.setOnClickListener(listeners);
        pictureCons.setOnClickListener(listeners);
        replaceImageTextView.setOnClickListener(listeners);
        removeImageTextView.setOnClickListener(listeners);
        saveTxtView.setOnClickListener(listeners);
        remove.setOnClickListener(listeners);
        imageToAttach.setOnClickListener(listeners);
        removeAccount.setOnClickListener(listeners);
        removePayee.setOnClickListener(listeners);

        tag1.setOnClickListener(listeners);
        tag2.setOnClickListener(listeners);
        tag3.setOnClickListener(listeners);
        tag4.setOnClickListener(listeners);
        tag5.setOnClickListener(listeners);
        tag6.setOnClickListener(listeners);

    }

    @Override
    void setAmountTypeFaces() {
        amount.setTypeface(Application.amountTypeFace(AddAndEditExpensesAndIncomesActivity.this));
    }

    private void generateTransaction() {
        transaction.setTrnDate(PublicModules.string2TimeStamp(PublicModules.toEnglishDigit(date.getText().toString() + " " + timeTextView.getText().toString())));
        transaction.setTrnDescription(description.getText().toString());
    }

    private void uiHandling(final boolean hasImage) {

        if (hasImage) {

            attachedImageLayout.setVisibility(View.VISIBLE);
            attachedImageLayout.invalidate();
            notAttachedImageLayout.setVisibility(View.GONE);
            notAttachedImageLayout.invalidate();
        } else {
            attachedImageLayout.setVisibility(View.GONE);
            attachedImageLayout.invalidate();
            notAttachedImageLayout.setVisibility(View.VISIBLE);
            notAttachedImageLayout.invalidate();
        }

    }

    private void tagHandling(View v) {

        int defaultSize = 0, clickedSize = 0;

        switch (PublicModules.screenSize(AddAndEditExpensesAndIncomesActivity.this)) {

            case 1:
                defaultSize = (int) getResources().getDimension(R.dimen.tag_buttons_default_size_normal);
                clickedSize = (int) getResources().getDimension(R.dimen.tag_buttons_clicked_size_normal);
                break;

            case 2:
                defaultSize = (int) getResources().getDimension(R.dimen.tag_buttons_default_size_large);
                clickedSize = (int) getResources().getDimension(R.dimen.tag_buttons_clicked_size_large);
                break;

            case 3:
                defaultSize = (int) getResources().getDimension(R.dimen.tag_buttons_default_size_xlarge);
                clickedSize = (int) getResources().getDimension(R.dimen.tag_buttons_clicked_size_xlarge);
                break;

        }

        for (int i = 0; i < tagArray.size(); i++) {

            if (v.equals(tagArray.get(i))) {
                if (tagArray.get(i).getWidth() != clickedSize) {
                    tagArray.get(i).setLayoutParams(new LinearLayout.LayoutParams(clickedSize, clickedSize));
                    tagArray.get(i).setAlpha(1);
                }
            } else {
                tagArray.get(i).setLayoutParams(new LinearLayout.LayoutParams(defaultSize, defaultSize));
                tagArray.get(i).setAlpha(0.6f);
            }

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expenses_and_incomes);

        init();
        setAmountTypeFaces();
        setListeners();

        shake = AnimationUtils.loadAnimation(AddAndEditExpensesAndIncomesActivity.this, R.anim.shake);
        choosePicture = pictureSelecting(AddAndEditExpensesAndIncomesActivity.this);
        scrollView.post(() -> scrollView.fullScroll(ScrollView.FOCUS_UP));
    }

    private Dialog fromAccountQuestionMark(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View fromAccountLayout = inflater.inflate(R.layout.pop_why_select_account, null);

        TextView close;

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 30;
                break;

            case 2:
                width = 70;
                height = 20;
                break;

            case 3:
                width = 62;
                height = 17;
                break;


        }

        final Dialog fromAccount = PublicDialogs.makeDialog(context, fromAccountLayout, width, height, false);
        fromAccount.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);

        close = fromAccount.findViewById(R.id.pop_why_select_account_close);
        close.setOnClickListener(v -> fromAccount.dismiss());

        return fromAccount;
    }

    private Dialog payToQuestionMark(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View payTotLayout = inflater.inflate(R.layout.pop_why_payee_account, null);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 30;
                break;

            case 2:
                width = 70;
                height = 20;
                break;

            case 3:
                width = 62;
                height = 17;
                break;

        }

        final Dialog payTo = PublicDialogs.makeDialog(context, payTotLayout, width, height, false);
        payTo.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);

        TextView close = payTo.findViewById(R.id.pop_why_payee_account_close);
        close.setOnClickListener(v -> payTo.dismiss());

        return payTo;

    }

    private Dialog widgetError(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View widgetErrorLayout = inflater.inflate(R.layout.pop_widget_error, null);

        TextView close = widgetErrorLayout.findViewById(R.id.pop_widget_error_close);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 60;
                height = 18;
                break;

            case 3:
                width = 50;
                height = 15;
                break;


        }

        final Dialog widgetErrorDialog = PublicDialogs.makeDialog(context, widgetErrorLayout, width, height, false);

        close.setOnClickListener(v -> widgetErrorDialog.dismiss());

        return widgetErrorDialog;
    }

    private Dialog removePictureDialog(Context context, final RemovePictureCallBack callBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View dlgLayout = inflater.inflate(R.layout.pop_delete_picture, null);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 29;
                break;

            case 2:
                width = 57;
                height = 21;
                break;

            case 3:
                width = 47;
                height = 18;
                break;


        }

        final Dialog removePictureDlg = PublicDialogs.makeDialog(context, dlgLayout, width, height, false);

        ConstraintLayout delete, cancel;
        delete = dlgLayout.findViewById(R.id.pop_delete_picture_delete_layout);
        cancel = dlgLayout.findViewById(R.id.pop_delete_picture_cancel_layout);

        delete.setOnClickListener(v -> callBack.call());

        cancel.setOnClickListener(v -> removePictureDlg.dismiss());

        return removePictureDlg;
    }

    private Dialog pictureSelecting(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View pictureSelectingLayout = inflater.inflate(R.layout.pop_picture_select_choices, null);

        final ConstraintLayout gallery, camera, cancel;
        gallery = pictureSelectingLayout.findViewById(R.id.pop_picture_select_choices_gallery);
        camera = pictureSelectingLayout.findViewById(R.id.pop_picture_select_choices_camera_layout);
        cancel = pictureSelectingLayout.findViewById(R.id.pop_picture_select_cancel);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 34;
                break;

            case 2:
                width = 67;
                height = 26;
                break;

            case 3:
                width = 50;
                height = 21;
                break;

        }

        final Dialog pscDialog = PublicDialogs.makeDialog(context, pictureSelectingLayout, width, height, false);

        View.OnClickListener dialogListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(cancel))
                    pscDialog.dismiss();

                if (v.equals(gallery))
                    choosePictureFromGallery(context);

                if (v.equals(camera)) {
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            )
                        ActivityCompat.requestPermissions(((Activity) context),
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                CAMERA_PERMISSION_AND_STORAGE_PERMISSION);
                    else
                        captureImage(context);
                }

            }
        };

        cancel.setOnClickListener(dialogListener);
        gallery.setOnClickListener(dialogListener);
        camera.setOnClickListener(dialogListener);

        return pscDialog;
    }

    private void captureImage(Context context) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            File photo = null;
            try {
                photo = createTemporaryFile(context, "picture", ".jpg");
            } catch (Exception e) {
                e.printStackTrace();
            }
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            mImageUri = PhotoProvider.getPhotoUri(photo);

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            ((Activity) context).startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

        }

    }

    private Bitmap grabImage(Context context) {
        context.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = context.getContentResolver();
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(cr, mImageUri);
            bitmap = Bitmap.createScaledBitmap(bitmap, 450, 650, true);
        } catch (Exception e) {
            Toast.makeText(context, "Failed to load", Toast.LENGTH_SHORT).show();
            Log.d(":(", "Failed to load", e);
        }
        return bitmap;
    }

    private File createTemporaryFile(Context context, String part, String ext) throws Exception {

        //  File tempDir = Environment.getExternalStorageDirectory();
        File tempDir = new File(Settings.getSetting(Settings.APP_DIRECTORY), "TransactionPicture/");
        if (!tempDir.exists())
            tempDir.mkdirs();

        return File.createTempFile(part, ext, tempDir);
    }

    private void choosePictureFromGallery(Context context) {

        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // Always show the chooser (if there are multiple options available)
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);

    }

    /**
     * @param imageName baray inke betune esme image ro bezare ru file string path ro return mikone
     */
    private String createImageFile(Bitmap bitmap, String imageName) {

        File parentFile = new File(Settings.getSetting(Settings.APP_DIRECTORY), "TransactionPicture");
        if (!parentFile.exists())
            parentFile.mkdir();
        File imageFile = new File(parentFile, imageName + ".png");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            //  Toast.makeText(, "are you Kidding Me ?!?!", Toast.LENGTH_SHORT).show();
        }

        return imageFile.getAbsolutePath();

    }

    private Dialog showTransactionImage(Context context, String resourcePath) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogLayout = inflater.inflate(R.layout.pop_show_transaction_image_view, null);

        ImageView imageView = dialogLayout.findViewById(R.id.pop_show_transaction_image_view);
        Bitmap resImage = AppModules.getImageFromFilePath(resourcePath);
        imageView.setImageBitmap(resImage);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 80;
                height = 70;
                break;

            case 2:
                width = 60;
                height = 60;
                break;

            case 3:
                width = 60;
                height = 60;
                break;


        }

        final Dialog dialog = PublicDialogs.makeDialog(context, dialogLayout, width, height, true);

        return dialog;
    }

    @Override
    public void onBackPressed() {
        if (tempTransaction.equals(transaction)) {
            super.onBackPressed();
            if (ExpensesAndIncomesListAdapter.getInstance() != null) {
                ExpensesAndIncomesListAdapter.getInstance().reloadData(type);
            }

            finish();
        } else {
            cancelDialog(this).show();
        }

    }

    private void customToast(String title, String description) {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom, findViewById(R.id.toast_layout_root));

        ImageView image = layout.findViewById(R.id.toast_img);
        image.setImageResource(R.drawable.ic_success);
        TextView titleText = layout.findViewById(R.id.toast_title);
        titleText.setText(title);
        TextView descriptionText = layout.findViewById(R.id.toast_description);
        descriptionText.setText(description);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 25);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    private Dialog cancelDialog(Context context) {

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_allways_deny);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final TextView exit, cancel, titleText, descText;
        exit = dialog.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = dialog.findViewById(R.id.pop_allways_deny_cancel);
        titleText = dialog.findViewById(R.id.pop_always_deny_title);
        descText = dialog.findViewById(R.id.pop_always_deny_description);

        titleText.setText("خروج");
        descText.setText("شما تراکنش خود را ذخیره نکرده اید.\n از خروج اطمینان دارید؟");
        exit.setText("خروج");
        cancel.setText("لغو");
        exit.setTextColor(Color.RED);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel)) {
                dialog.dismiss();
            }

            if (v.equals(exit)) {
                super.onBackPressed();
                if (ExpensesAndIncomesListAdapter.getInstance() != null) {
                    ExpensesAndIncomesListAdapter.getInstance().reloadData(type);
                }
                finish();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        exit.setOnClickListener(dialogListeners);

        return dialog;
    }

}