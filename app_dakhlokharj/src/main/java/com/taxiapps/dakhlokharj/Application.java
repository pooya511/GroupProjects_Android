package com.taxiapps.dakhlokharj;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Build;
import android.os.LocaleList;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;
import android.util.Log;

import com.mtramin.rxfingerprint.RxFingerprint;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import db.DBManager;
import model.Account;
import model.EnumsAndConstants;
import model.Payee;
import model.Settings;
import modules.PublicModules;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Application extends MultiDexApplication {

    private DBManager dbManager;

    public static boolean fingerPrintIsAvailable;
    public static int WIDTH;
    public static int HEIGHT;
    public static String currentBase64;

    public enum AppEnums {

        SHARED_PREFRENCES("SHARED_PREFRENCES");

        private final String name;

        AppEnums(String s) {
            name = s;
        }

        public String value() {
            return this.name;
        }
    }

    public static void createAppFolder() {
        File path = new File(Settings.getSetting(Settings.APP_DIRECTORY));
        if (!path.exists())
            path.mkdirs();
    }

    /**
     * ba tavajoh be currency typeface barmigardune
     *
     * @param context
     * @return targetTypeface
     */
    public static Typeface amountTypeFace(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/" + Settings.getSetting(Settings.AMOUNT_TYPE_FACE));
    }

    /**
     * baray ine ke user age zabune system ro avaz kard
     * app kharab nashe
     *
     * @param context
     */
    public static void localization(Context context) {

        String language = "en";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Locale locale = new Locale(language);
            Locale.setDefault(locale);

            Configuration configuration = context.getResources().getConfiguration();
            configuration.setLocale(locale);

            context.createConfigurationContext(configuration);
        } else {
            Locale locale = new Locale(language);
            Locale.setDefault(locale);

            Resources resources = context.getResources();

            Configuration configuration = resources.getConfiguration();
            configuration.setLayoutDirection(locale);
            configuration.locale = locale;

            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Settings.init(this);
        Application.createAppFolder();
        dbManager = new DBManager(this);

        tir_97_Update_hasNot_trnReference();
        bankTableChecker();

        if (!Settings.getSetting(Settings.APP_VERSION).equals("")) {
            if (Integer.valueOf(Settings.getSetting(Settings.APP_VERSION).replaceAll("\\D", "")) <= 10200100) {
                //Log.i("APP_VERSION", Settings.getSetting(Settings.APP_VERSION).replaceAll("\\D", ""));
                //Log.i("APP_VERSION", "kuchiktar bud");
                trnDateChanger();
            }
        }
        if (EnumsAndConstants.APP_SOURCE.equals("bazaar")) {
            currentBase64 = EnumsAndConstants.BAZAAR_PAYMENT_BASE64;
        }
        if (EnumsAndConstants.APP_SOURCE.equals("myket")) {
            currentBase64 = EnumsAndConstants.MYKET_PAYMENT_BASE64;
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/yekan.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        try {
            EnumsAndConstants.DEVICE_ID = PublicModules.macAddressHandling(this, "DAKHLOKHARJ_");
            EnumsAndConstants.APP_VERSION = String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
            Settings.setSetting(Settings.APP_VERSION, EnumsAndConstants.APP_VERSION);
            Log.i("APP_VERSION", "changed to " + Settings.getSetting(Settings.APP_VERSION).replaceAll("\\D", ""));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        fingerPrintIsAvailable = RxFingerprint.isAvailable(this);

        // for payment Dialogs size
        switch (PublicModules.screenSize(Application.this)) {
            case 1:
                WIDTH = 85;
                HEIGHT = 70;
                break;
            case 2:
                WIDTH = 55;
                HEIGHT = 50;
                break;
            case 3:
                WIDTH = 45;
                HEIGHT = 50;
                break;
            default:
                WIDTH = 0;
                HEIGHT = 0;
                break;
        }


        //check mikonim age databasesh ghadimie update mikonim column hasho
        if (EnumsAndConstants.APP_VERSION.equals("1.1.4")) {
            DBManager.openForWrite();
            try {
                DBManager.database.execSQL("UPDATE T_Account SET acnIcon = bnkID");
            } catch (SQLException e) {

            }
            DBManager.close();
            Log.i("DB", "Done");
        }

        if (Settings.getSetting(Settings.IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME).equals("0")) {
            Account.updateAccountOrderHashMap(Account.getAll());
            Payee.updatePayeeOrderHashMap(Payee.getAll());
            Settings.setSetting(Settings.IS_ACCOUNT_PAYEE_ORDERED_FIRST_TIME, "1");
        }

    }

    public static void tir_97_Update_hasNot_trnReference() {
        boolean hasTrnReferenceColumn = false;

        DBManager.openForWrite();
        try {
            Cursor cursor = DBManager.database.rawQuery("PRAGMA table_info(T_Transaction)", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                hasTrnReferenceColumn = cursor.getString(cursor.getColumnIndex("name")).equals("trnReference"); // age hamchin columni dasht
                if (hasTrnReferenceColumn)
                    break;
                cursor.moveToNext();
            }
            cursor.close();
        } catch (SQLException e) {

        }

        if (!hasTrnReferenceColumn) {
            DBManager.database.execSQL("ALTER TABLE T_Transaction ADD COLUMN trnReference INTEGER Default 0;");
        }
        DBManager.close();
    }

    public static void bankTableChecker() {
        boolean hasBnkIsDeleted = false;

        DBManager.openForWrite();
        try {
            Cursor cursor = DBManager.database.rawQuery("PRAGMA table_info(T_Bank)", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                hasBnkIsDeleted = cursor.getString(cursor.getColumnIndex("name")).equals("bnkIsDeleted"); // age hamchin columni nadasht
                if (hasBnkIsDeleted)
                    break;
                cursor.moveToNext();
            }
            cursor.close();
        } catch (SQLException e) {

        }

        if (!hasBnkIsDeleted) {
            DBManager.database.execSQL("ALTER TABLE T_Bank RENAME TO T_Bank_TEMP;");
            DBManager.database.execSQL("CREATE TABLE 'T_Bank' (" +
                    "'ID' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'bnkTitle' TEXT," +
                    "'bnkLogo' TEXT," +
                    "'bnkIsDeleted' INTEGER," +
                    "'bnkOrder' INTEGER);");
            DBManager.database.execSQL("INSERT INTO T_Bank SELECT * FROM T_Bank_TEMP;");
            DBManager.database.execSQL("DROP TABLE IF EXISTS T_Bank_TEMP;");
        }

        DBManager.close();

    }

    public static void trnDateChanger() {
        DBManager.openForWrite();
        DBManager.database.execSQL("UPDATE T_Transaction SET trnDate = (trnDate - 16200000)");
        DBManager.close();
    }

    public static boolean isLicenseValid() {
        if (Settings.getSetting(Settings.PURCHASE_STATUS).equals("1")) { // app bayad payment dashte bashe
            if (getLicense() != null) {
                long expireDate = Long.parseLong(Settings.getSetting(Settings.LICENSE_EXPIRE));
                long currentDate = new Date().getTime();
                if (currentDate < expireDate) { // agar tarikhe engheza nagzashte bud
                    return true; // license ok
                } else {
                    return false; // license is expired
                }
            } else {
                return false; // license nadare
            }
        } else // app bayad full version bashe
            return true;
    }

    public static String getLicense() {
        if (!Settings.getSetting(Settings.LICENSE_DATA).equals("")) {
            return Settings.getSetting(Settings.LICENSE_DATA);
        } else {
            return null;
        }
    }

}
