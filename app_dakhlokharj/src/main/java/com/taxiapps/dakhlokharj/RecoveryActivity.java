package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;

import adapters.RecoveryItemsAdapter;
import model.MyMetaData;

public class RecoveryActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private ArrayList<MyMetaData> metadataArrayList;

    @Override
    Context context() {
        return this;
    }

    @Override
    void init() {
        recyclerView = findViewById(R.id.act_recovery_recycler_view);
    }

    @Override
    void setListeners() {

    }

    @Override
    void setAmountTypeFaces() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery);

        init();
        setListeners();

        metadataArrayList = getIntent().getParcelableArrayListExtra("metadataArrayList");

        initRecycler();
    }

    private void initRecycler() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new RecoveryItemsAdapter(this, metadataArrayList));
        recyclerView.setHasFixedSize(true);
    }



}
