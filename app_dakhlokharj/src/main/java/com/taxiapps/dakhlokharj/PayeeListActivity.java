package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.woxthebox.draglistview.DragListView;

import java.util.ArrayList;
import java.util.List;

import adapters.PayeeListAdapter;
import model.Payee;
import model.Settings;
import modules.PublicModules;

public class PayeeListActivity extends BaseActivity {

    private DragListView payeeList;
    private ImageView addPayee;
    private ConstraintLayout didntAddPayeeLayout;
    private final int REQUEST_CODE_ADD_PAYEE = 1;
    private TextView listTitle;
    private PayeeListAdapter payeeListAdapter;
    private ArrayList<Payee> payees;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD_PAYEE && resultCode == RESULT_OK) {
            if (payeeListAdapter != null) {
                payeeListAdapter.refreshData();
                payeeListAdapter.targetPayee = null;
            }
            checkListContent();

        }
        if (resultCode == RESULT_CANCELED) {
            payeeListAdapter.targetPayee = null;
        }
    }

    @Override
    Context context() {
        return PayeeListActivity.this;
    }

    @Override
    void init() {
        payeeList = findViewById(R.id.payee_list_activity_list_view);
        addPayee = findViewById(R.id.payee_list_new_payee_image);
        didntAddPayeeLayout = findViewById(R.id.payee_list_activity_didnt_add_payee_layout);
        listTitle = findViewById(R.id.payee_list_activity_textview1);
    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = v -> {
            if (v.equals(addPayee)) {
                Intent intent = new Intent(PayeeListActivity.this, NewAndEditPayeeActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ADD_PAYEE);
            }
        };
        addPayee.setOnClickListener(listeners);
    }

    @Override
    void setAmountTypeFaces() {
        //Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payee_list);

        init();
        setAmountTypeFaces();
        setListeners();

        checkListContent();
    }

    private void checkListContent() {
        payees = Payee.getAll();
        if (payees.size() == 0) {
            didntAddPayeeLayout.setVisibility(View.VISIBLE);
            listTitle.setVisibility(View.GONE);
            payeeList.setVisibility(View.GONE);
        } else {
            didntAddPayeeLayout.setVisibility(View.GONE);
            listTitle.setVisibility(View.VISIBLE);
            payeeList.setVisibility(View.VISIBLE);
            initAdapter();
        }
    }

    private void initAdapter() {

        payees = Payee.sortPayeesOrder(Payee.getAll(), Settings.loadMap(Settings.PAYEE_ORDER_MAP));
        payeeListAdapter = new PayeeListAdapter(PayeeListActivity.this, payees);
        payeeList.setLayoutManager(new LinearLayoutManager(PayeeListActivity.this));
        payeeList.setAdapter(payeeListAdapter, true);
        payeeList.setDragListListener(new DragListView.DragListListener() {
            @Override
            public void onItemDragStarted(int position) {
            }

            @Override
            public void onItemDragging(int itemPosition, float x, float y) {
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition) {
                    PublicModules.customToast(PayeeListActivity.this, "لیست طرف حساب ها", "ترتیب نمایش بروزرسانی شد", true);
                }
            }
        });
        payeeList.setCanDragHorizontally(false);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (payeeListAdapter != null) {
            ArrayList<Payee> newOrderedPayees = listToArrayList(payeeListAdapter.getItemList());
            Payee.updatePayeeOrderHashMap(newOrderedPayees);
        }
    }

    private ArrayList<Payee> listToArrayList(List<Payee> payeesList) {

        ArrayList<Payee> res = new ArrayList<>();

        for (int i = 0; i < payeesList.size(); i++) {
            res.add(payeesList.get(i));
        }
        return res;
    }
}
