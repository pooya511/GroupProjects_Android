package com.taxiapps.dakhlokharj;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import adapters.BudgetListAdapter;
import model.Budget;
import model.Currency;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.DialogUtils;
import utils.AppModules;

public class ExpensesBudgetActivity extends BaseActivity {

    private RecyclerView listView;
    private ImageView restBudget;
    private TextView helpBudget;
    private static TextView sumBudgetInMonth;

    @Override
    Context context() {
        return ExpensesBudgetActivity.this;
    }

    @Override
    void init() {
        listView = findViewById(R.id.expenses_budget_activity_list_view);
        restBudget = findViewById(R.id.expenses_budget_activity_rest_budget_ic);
        helpBudget = findViewById(R.id.expenses_budget_activity_help_budget_textview);
        sumBudgetInMonth = findViewById(R.id.expenses_budget_activity_total_budget_in_month_text);
    }

    @Override
    void setListeners() {
    }

    @Override
    void setAmountTypeFaces() {
        sumBudgetInMonth.setTypeface(Application.amountTypeFace(ExpensesBudgetActivity.this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses_budget);

        init();
        setAmountTypeFaces();
        setListeners();

        reloadData();

        View.OnTouchListener listeners = (v, event) -> {

            if (v.equals(restBudget)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    budgetReset(ExpensesBudgetActivity.this).show();
                    restBudget.setAlpha(0.5f);
                } else {
                    restBudget.setAlpha(1f);
                }
            }
            if (v.equals(helpBudget)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    budgetHelp(ExpensesBudgetActivity.this).show();
                    helpBudget.setAlpha(0.5f);
                } else {
                    helpBudget.setAlpha(1f);
                }
            }

            return true;
        };
        restBudget.setOnTouchListener(listeners);
        helpBudget.setOnTouchListener(listeners);

        BudgetListAdapter budgetListAdapter = new BudgetListAdapter(ExpensesBudgetActivity.this);
        listView.setLayoutManager(new LinearLayoutManager(ExpensesBudgetActivity.this));
        listView.setHasFixedSize(true);
        listView.setAdapter(budgetListAdapter);
    }

    private Dialog budgetHelp(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View budgetHelpLayout = inflater.inflate(R.layout.pop_budget_help, null);

        TextView close = budgetHelpLayout.findViewById(R.id.pop_budget_help_close);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 27;
                break;

            case 2:
                width = 70;
                height = 18;
                break;

            case 3:
                width = 60;
                height = 15;
                break;

        }

        final Dialog budgetHelp = PublicDialogs.makeDialog(context, budgetHelpLayout, width, height, false);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                budgetHelp.dismiss();

            }
        });

        return budgetHelp;
    }

    private Dialog budgetReset(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View budgetResetLayout = inflater.inflate(R.layout.pop_reset_budget, null);

        TextView close = budgetResetLayout.findViewById(R.id.pop_reset_budget_cancel);
        TextView reset = budgetResetLayout.findViewById(R.id.pop_reset_budget_reset);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 21;
                break;

            case 2:
                width = 65;
                height = 16;
                break;

            case 3:
                width = 50;
                height = 13;
                break;

        }

        final Dialog budgetReset = PublicDialogs.makeDialog(context, budgetResetLayout, width, height, false);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                budgetReset.dismiss();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBudgets();
                MainActivity.getInstance().reloadMainData();
                finish();
                budgetReset.dismiss();
            }
        });

        return budgetReset;
    }

    public static void reloadData() {
        sumBudgetInMonth.setText(Currency.CurrencyString.getCurrencyString(Budget.getSumAll(), Currency.CurrencyMode.Short).separated);
    }

    private void resetBudgets() {
        ArrayList<Budget> budgets = Budget.getAllByList();

        for (int i = 0; i < budgets.size(); i++) {
            Budget budget = budgets.get(i);

            budget.setBudAmount(String.valueOf(0));
            budget.update(new String[]{Budget.BudgetCol.budAmount.value()});
        }
    }
}