package com.taxiapps.dakhlokharj;

import android.app.Activity;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;

import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortableField;
import com.google.android.gms.tasks.Task;

import com.google.android.gms.tasks.Tasks;
import com.google.api.client.util.IOUtils;
import com.sevenheaven.iosswitch.ShSwitchView;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;
import org.zeroturnaround.zip.ZipUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import db.DBManager;
import model.Account;
import model.Currency;
import model.EnumsAndConstants;
import model.MyMetaData;
import model.Payee;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class GoogleDriveBackupActivity extends BaseActivity {

    public interface GoogleDriveBackupCallBack {
        void call(boolean success);
    }

    public interface GoogleDriveGetFilesCallBack {
        void call(ArrayList<MyMetaData> returnedMetaDataArray);
    }

    public interface PlatformWarningCallBack {
        void call(boolean continueBaby);
    }

    private TextView signIn, settingTitle, emailText, autoBackupPeriodText, maximumBackupFileCountText, staticBackupText, recoveryText, lastBackupTextView;
    private LinearLayout signInLayoutHeaderContainer, signInLayoutFooterContainer;
    private static ProgressBar loading;
    private View separator;
    private RelativeLayout autoBackupPeriodTime, maximumBackupFileCount;
    private ShSwitchView autoBackupSwitch, imagesBackupSwitch;

    private static GoogleSignInClient mGoogleSignInClient;
    public static DriveResourceClient mDriveResourceClient;
    private GoogleSignInAccount account;

    private final int RC_SIGN_IN = 0;

    private static GoogleDriveBackupActivity instance;

    public static GoogleDriveBackupActivity getInstance() {
        return instance;
    }

    @Override
    Context context() {
        return this;
    }

    @Override
    void init() {

        signIn = findViewById(R.id.act_google_drive_backup_connect_text);
        signInLayoutHeaderContainer = findViewById(R.id.act_google_drive_backup_sign_in_header_container);
        signInLayoutFooterContainer = findViewById(R.id.act_google_drive_backup_sign_in_footer_container);
        staticBackupText = findViewById(R.id.act_google_drive_backup_static_backup_text);
        settingTitle = findViewById(R.id.act_google_drive_backup_settings_title_text);
        emailText = findViewById(R.id.act_google_drive_backup_email_text);
        loading = findViewById(R.id.act_google_drive_backup_loading_progress);
        autoBackupPeriodTime = findViewById(R.id.act_google_drive_backup_period_backup_container);
        autoBackupPeriodText = findViewById(R.id.act_google_drive_backup_automatic_period_backup_text);
        maximumBackupFileCount = findViewById(R.id.act_google_drive_backup_maximum_backups_container);
        maximumBackupFileCountText = findViewById(R.id.act_google_drive_backup_maximum_backups_text_view);
        autoBackupSwitch = findViewById(R.id.act_google_drive_backup_automatic_backup_switch);
        imagesBackupSwitch = findViewById(R.id.act_google_drive_backup_images_backup_switch);
        recoveryText = findViewById(R.id.act_google_drive_backup_recovery_text);
        lastBackupTextView = findViewById(R.id.act_google_drive_backup_last_backup_date_text_view);
        separator = findViewById(R.id.act_google_drive_backup_separator_1);

        if (Settings.getSetting(Settings.AUTO_BACKUP).equals("1")) {
            autoBackupSwitch.setOn(true);
        } else {
            autoBackupSwitch.setOn(false);
        }

        if (Settings.getSetting(Settings.BACKUP_IMAGE).equals("1")) {
            imagesBackupSwitch.setOn(true);
        } else {
            imagesBackupSwitch.setOn(false);
        }

        if (Settings.getSetting(Settings.BACKUP_PERIOD).equals(EnumsAndConstants.BackupPeriodItems.Daily.value())) {
            autoBackupPeriodText.setText("روزانه");
        } else if (Settings.getSetting(Settings.BACKUP_PERIOD).equals(EnumsAndConstants.BackupPeriodItems.Weekly.value())) {
            autoBackupPeriodText.setText("هفتگی");
        } else if (Settings.getSetting(Settings.BACKUP_PERIOD).equals(EnumsAndConstants.BackupPeriodItems.Monthly.value())) {
            autoBackupPeriodText.setText("ماهانه");
        }

        if (Settings.getSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT).equals(String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.One.value()))) {
            maximumBackupFileCountText.setText("۱ فایل");
        } else if (Settings.getSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT).equals(String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.Five.value()))) {
            maximumBackupFileCountText.setText("۵ فایل");
        } else if (Settings.getSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT).equals(String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.Ten.value()))) {
            maximumBackupFileCountText.setText("۱۰ فایل");
        } else if (Settings.getSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT).equals(String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.Fifty.value()))) {
            maximumBackupFileCountText.setText("۵۰ فایل");
        }

    }

    @Override
    void setListeners() {

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.act_google_drive_backup_connect_text:
                        signIn();
                        break;

                    case R.id.act_google_drive_backup_email_text:
                        signOutPopup().show();
                        break;

                    case R.id.act_google_drive_backup_period_backup_container:
                        autoBackupPeriodDialog().show();
                        break;

                    case R.id.act_google_drive_backup_maximum_backups_container:
                        maximumFileCountDialog().show();
                        break;

                    case R.id.act_google_drive_backup_static_backup_text:
                        if (Application.isLicenseValid()) {
                            if (PublicModules.isNetworkAvailable(GoogleDriveBackupActivity.this)) {
                                progressDialog(GoogleDriveBackupActivity.this, true, null).show();
                            } else {
                                PublicDialogs.makeMessageDialog(GoogleDriveBackupActivity.this, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "لطفا از اتصال اینترنت خود مطمئن شوید", "", EnumsAndConstants.APP_SOURCE).show();
                            }
                        } else {
                            String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                            String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                            Payment.newInstance(R.drawable.ic_dakhlokharj,
                                    EnumsAndConstants.APP_ID,
                                    EnumsAndConstants.APP_SOURCE,
                                    EnumsAndConstants.APP_VERSION,
                                    "دخل و خرج",
                                    Application.currentBase64,
                                    "در نسخه رایگان امکان پشتیبانگیری و بازیابی \n فعال نیست !",
                                    limitationText,
                                    userNumber,
                                    EnumsAndConstants.DEVICE_ID,
                                    EnumsAndConstants.SERVER_URL,
                                    EnumsAndConstants.GATEWAY_URL,
                                    new TX_License.SetLicenseCallback() {
                                        @Override
                                        public void call(long expireDate, String license) {

                                            Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                            Settings.setSetting(Settings.LICENSE_DATA, license);

                                            try {
                                                JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                                Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                            MainActivity.getInstance().updateFragment(false);
                                            MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                            if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                                Transaction.appPurchased();

                                            MainActivity.getInstance().reloadMainData();
                                            if (SettingActivity.getInstance() != null)
                                                SettingActivity.getInstance().premiumUIHandling(false);
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    },
                                    new Payment.UserNameCallBack() {
                                        @Override
                                        public void getUserName(String username) {
                                            Settings.setSetting(Settings.TX_USER_NUMBER, username);
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    }
                            ).show(getFragmentManager(), "tx_payment");
                        }
                        break;

                    case R.id.act_google_drive_backup_recovery_text:
                        if (PublicModules.isNetworkAvailable(GoogleDriveBackupActivity.this)) {
                            if (account != null) {
                                loading.setVisibility(View.VISIBLE);
                                Drive.DriveApi.requestSync(mDriveResourceClient.asGoogleApiClient()).setResultCallback(new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(@NonNull Status status) {
                                        if (status.getStatusCode() == Status.RESULT_SUCCESS.getStatusCode()) {
                                            listBackupFiles(returnedMetaDataArray -> {
                                                if (returnedMetaDataArray.size() > 0) {
                                                    for (int i = 0; i < returnedMetaDataArray.size(); i++) {
                                                        if (returnedMetaDataArray.get(i).getTitle().contains(".dak")) {
                                                            Intent recoveryIntent = new Intent(GoogleDriveBackupActivity.this, RecoveryActivity.class);
                                                            recoveryIntent.putParcelableArrayListExtra("metadataArrayList", returnedMetaDataArray);
                                                            startActivity(recoveryIntent);
                                                            break;
                                                        }
                                                    }
                                                } else {
                                                    PublicModules.customToast(GoogleDriveBackupActivity.this, "خطا", "آیتمی برای بازیابی یافت نشد!", false);
                                                }
                                                loading.setVisibility(View.GONE);
                                            });
                                        } else {
                                            Toast.makeText(GoogleDriveBackupActivity.this, "ممکن است بروزرسانی لیست فایل ها کمی بطول بینجامد", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            } else {
                                PublicDialogs.makeMessageDialog(GoogleDriveBackupActivity.this, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "ابتدا باید وارد حساب گوگل خود شوید", "", EnumsAndConstants.APP_SOURCE).show();
                            }
                        } else {
                            PublicDialogs.makeMessageDialog(GoogleDriveBackupActivity.this, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "لطفا از اتصال اینترنت خود مطمئن شوید", "", EnumsAndConstants.APP_SOURCE).show();
                        }

                        break;
                }
            }
        };

        signIn.setOnClickListener(listener);
        emailText.setOnClickListener(listener);
        autoBackupPeriodTime.setOnClickListener(listener);
        maximumBackupFileCount.setOnClickListener(listener);
        staticBackupText.setOnClickListener(listener);
        recoveryText.setOnClickListener(listener);

        autoBackupSwitch.setOnSwitchStateChangeListener(isOn -> {
            Settings.setSetting(Settings.AUTO_BACKUP, isOn ? "1" : "0");
            Settings.setSetting(Settings.BACKUP_LAST_TIME, String.valueOf(new Date().getTime()));
        });

        imagesBackupSwitch.setOnSwitchStateChangeListener(isOn -> Settings.setSetting(Settings.BACKUP_IMAGE, isOn ? "1" : "0"));

    }

    @Override
    void setAmountTypeFaces() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_drive_backup);
        instance = this;

        init();
        setListeners();
        setAmountTypeFaces();
        initLastBackupDate();

        mGoogleSignInClient = buildGoogleSignInClient();
        account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            // last account that signed in
            account = GoogleSignIn.getLastSignedInAccount(this);
            mDriveResourceClient = Drive.getDriveResourceClient(this, account);

            // init UI
            String target = account.getEmail() == null ? account.getDisplayName() : account.getEmail();
            signInUI(target);

        } else {
            signIn();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case RC_SIGN_IN:
                if (resultCode == RESULT_OK) {
                    loading.setVisibility(View.GONE);

                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    handleSignInResult(task);

                } else {
                    loading.setVisibility(View.GONE);
                    PublicModules.customToast(this, "خطا", "متاسفانه در حال حاضر امکان برقرای ارتباط وجود ندارد!", false);
                }
                break;

        }

    }

    private GoogleSignInClient buildGoogleSignInClient() {
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Drive.SCOPE_APPFOLDER)
                        .build();
        return GoogleSignIn.getClient(this, signInOptions);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        loading.setVisibility(View.VISIBLE);
    }

    public static void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(task -> getInstance().signOutUI());
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        try {
            account = completedTask.getResult(ApiException.class);

            completedTask.addOnSuccessListener(googleSignInAccount -> {
                mDriveResourceClient = Drive.getDriveResourceClient(this, account);
                String target = account.getEmail() == null ? account.getDisplayName() : account.getEmail();
                signInUI(target);
            });
            completedTask.addOnFailureListener(e -> signOutUI());

        } catch (ApiException e) {
            e.printStackTrace();
        }

    }

    public void signInUI(String accountIdentifier) {
        signInLayoutHeaderContainer.setVisibility(View.VISIBLE);
        signInLayoutFooterContainer.setVisibility(View.VISIBLE);
        separator.setVisibility(View.GONE);
        settingTitle.setVisibility(View.VISIBLE);
        signIn.setVisibility(View.GONE);

        emailText.setText("متصل به اکانت " + accountIdentifier);
    }

    private void signOutUI() {
        signInLayoutHeaderContainer.setVisibility(View.GONE);
        signInLayoutFooterContainer.setVisibility(View.GONE);
        separator.setVisibility(View.VISIBLE);
        settingTitle.setVisibility(View.GONE);
        signIn.setVisibility(View.VISIBLE);
    }

    private void initLastBackupDate() {
        String lastBackupDate = Settings.getSetting(Settings.BACKUP_LAST_TIME);
        if (!lastBackupDate.equals("0")) {
            PersianDateFormat format = new PersianDateFormat("H:i Y/m/d");
            String formattedDate = format.format(new PersianDate(Long.valueOf(lastBackupDate)));
            String lastDateText = "آخرین تاریخ پشتیبانگیری: " + formattedDate;
            lastBackupTextView.setText(lastDateText);
        }
    }

    public static void backupAndSendFileToGoogleDrive(Context context, DriveResourceClient mDriveResourceClient, boolean isAutoMode, GoogleDriveBackupCallBack callBack) {

        // backup
        final Task<DriveFolder> appFolderTask = mDriveResourceClient.getAppFolder();
        final Task<DriveContents> createContentsTask = mDriveResourceClient.createContents();
        final File backupFile = createZipFile(context, isAutoMode);
        Tasks.whenAll(appFolderTask, createContentsTask)
                .continueWithTask(task -> {
                    DriveFolder parent = appFolderTask.getResult();
                    DriveContents contents = createContentsTask.getResult();

                    IOUtils.copy(new FileInputStream(backupFile), contents.getOutputStream());

                    String os = "android";
                    String timeStamp = new PersianDateFormat().format(new PersianDate(), "YmdHi");
                    String title = isAutoMode ? "Backup_" + timeStamp + "_AUTO.dak." + os : "Backup_" + timeStamp + ".dak." + os;
                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                            .setTitle(title)
                            .setMimeType("application/zip")
                            .build();

                    return mDriveResourceClient.createFile(parent, changeSet, contents);
                })
                .addOnSuccessListener(
                        driveFile -> {
                            if (loading != null) // hengam e auto backup momkene null bashe !
                                loading.setVisibility(View.GONE);

                            // delete file from cache after sending to GoogleDrive
                            backupFile.delete();

                            Settings.setSetting(Settings.BACKUP_LAST_TIME, String.valueOf(new Date().getTime())); // tarikh e akharin backup ro negah midarim

                            // check backups limitation
                            listBackupFiles(returnedMetaDataArray -> {

                                ArrayList<MyMetaData> autoBackups = new ArrayList<>();
                                for (int i = 0; i < returnedMetaDataArray.size(); i++) {
                                    if (returnedMetaDataArray.get(i).getTitle().endsWith("_AUTO.dak.android")) {
                                        autoBackups.add(returnedMetaDataArray.get(i));
                                    }
                                }

                                int maximumBackupCount = Integer.parseInt(Settings.getSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT));
                                if (autoBackups.size() > maximumBackupCount) {
                                    List<MyMetaData> targets = autoBackups.subList(0, autoBackups.size() - maximumBackupCount);
                                    for (int i = 0; i < targets.size(); i++) {
                                        mDriveResourceClient.delete(targets.get(i).getDriveId().asDriveFile());
                                    }
                                }
                            });


                            callBack.call(true);
                        })
                .addOnFailureListener(e -> {
                    loading.setVisibility(View.GONE);
                    PublicModules.customToast(context, "خطا", "متاسفانه در حال حاضر امکان برقرای ارتباط وجود ندارد!", false);
                    callBack.call(false);
                });
    }

    private static void listBackupFiles(GoogleDriveGetFilesCallBack callBack) {

        SortOrder sortOrder = new SortOrder.Builder().addSortDescending(SortableField.CREATED_DATE).build();
        Query query = new Query.Builder()
                .setSortOrder(sortOrder)
                .build();

        mDriveResourceClient.query(query).addOnSuccessListener(metadata -> {
            ArrayList<MyMetaData> metadataArrayList = new ArrayList<>();
            for (int i = 0; i < metadata.getCount(); i++) {
                Metadata currMetaData = metadata.get(i);
                if (currMetaData.getTitle().contains(".dak")) { // check mikonim bara app e ma bashe
                    metadataArrayList.add(new MyMetaData(currMetaData.getDriveId(), currMetaData.getTitle(), currMetaData.getModifiedDate().getTime(), currMetaData.getFileSize()));
                }
            }
            callBack.call(metadataArrayList);

        });
    }

    public static File createZipFile(Context context, boolean isAutoMode) {

        File res = null;

        // DataBase
        File dbFile = new File(DBManager.database.getPath());
        // making a temp shared file and writing data on it
        File sharedPreferences = new File(context.getFilesDir().getParent(), Application.AppEnums.SHARED_PREFRENCES.value() + ".xml");
        // make version file
        File versionFile = new File(context.getFilesDir().getParent(), "android." + EnumsAndConstants.APP_VERSION + ".txt");
        try {
            sharedPreferences.createNewFile();
            versionFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        saveSharedPreferencesToFile(context, sharedPreferences);

        String timeStamp = new PersianDateFormat().format(new PersianDate(), "YmdHi");
        File resFile = isAutoMode ? new File(context.getCacheDir(), timeStamp + "_AUTO.dak.android") : new File(context.getCacheDir(), timeStamp + ".dak.android");

        String[] targetFiles;
        File picZip = null;
        if (imagesBackuping()) {
            picZip = new File(Settings.getSetting(Settings.APP_DIRECTORY), "TransactionPicture.zip");
            ZipUtil.pack(new File(Settings.getSetting(Settings.APP_DIRECTORY), "TransactionPicture/"), picZip);

            targetFiles = new String[]{dbFile.getAbsolutePath(), sharedPreferences.getAbsolutePath(), versionFile.getAbsolutePath(), picZip.getAbsolutePath()};
        } else {
            targetFiles = new String[]{dbFile.getAbsolutePath(), sharedPreferences.getAbsolutePath(), versionFile.getAbsolutePath()};
        }

        try {
            if (!resFile.exists()) {
                resFile.createNewFile();
            }
            res = zipFiles(targetFiles, resFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // deleting temp files
        sharedPreferences.delete();
        versionFile.delete();

        if (picZip != null) picZip.delete();

        return res;
    }

    private static File zipFiles(String[] files, String zipFile) throws IOException {
        BufferedInputStream origin;
        int bufferSize = 6 * 1024;
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
        try {
            byte data[] = new byte[bufferSize];

            for (int i = 0; i < files.length; i++) {
                FileInputStream fi = new FileInputStream(files[i]);
                origin = new BufferedInputStream(fi, bufferSize);
                try {
                    ZipEntry entry = new ZipEntry(files[i].substring(files[i].lastIndexOf("/") + 1));
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0, bufferSize)) != -1) {
                        out.write(data, 0, count);
                    }
                } finally {
                    origin.close();
                }
            }
        } finally {
            out.close();
        }
        return new File(zipFile);
    }

    private void unzip(File zipFile, File targetDirectory) throws IOException {
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(new FileInputStream(zipFile)));
        try {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                File file = new File(targetDirectory, ze.getName());
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " +
                            dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }
            }
        } finally {
            zis.close();
        }
    }

    public static void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }

    public static boolean saveSharedPreferencesToFile(Context context, File dst) {
        boolean res = false;
        ObjectOutputStream output = null;
        try {
            output = new ObjectOutputStream(new FileOutputStream(dst));
            SharedPreferences pref =
                    context.getSharedPreferences(Application.AppEnums.SHARED_PREFRENCES.value(), MODE_PRIVATE);

            Map<String, ?> settings = pref.getAll();

//            settings.remove(Settings.LICENSE_DATA);
//            settings.remove(Settings.APP_VERSION); // in 2 field nabayd zakhire va bazyabi shavand

            output.writeObject(settings);

            res = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    private boolean loadSharedPreferencesFromFile(File src) {
        boolean res = false;
        ObjectInputStream input = null;
        try {
            input = new ObjectInputStream(new FileInputStream(src));
            SharedPreferences.Editor prefEdit = getSharedPreferences(Application.AppEnums.SHARED_PREFRENCES.value(), MODE_PRIVATE).edit();
//            prefEdit.clear();
            Map<String, ?> entries = (Map<String, ?>) input.readObject();
            for (Map.Entry<String, ?> entry : entries.entrySet()) {
                Object v = entry.getValue();
                String key = entry.getKey();

                if (v instanceof Boolean)
                    prefEdit.putBoolean(key, ((Boolean) v).booleanValue());
                else if (v instanceof Float)
                    prefEdit.putFloat(key, ((Float) v).floatValue());
                else if (v instanceof Integer)
                    prefEdit.putInt(key, ((Integer) v).intValue());
                else if (v instanceof Long)
                    prefEdit.putLong(key, ((Long) v).longValue());
                else if (v instanceof String)
                    if (!key.equals(Settings.LICENSE_DATA) || !key.equals(Settings.APP_VERSION) || !key.equals(Settings.LAST_SMS_COUNT)) {  // data e license va app version ro nabayad bargardune
                        prefEdit.putString(key, ((String) v));
                    }

            }
            prefEdit.commit();
            res = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    private static boolean imagesBackuping() {
        File imagesDir = new File(Settings.getSetting(Settings.APP_DIRECTORY), "TransactionPicture/");
        if (imagesDir.exists() && imagesDir.listFiles().length != 0 && Settings.getSetting(Settings.BACKUP_IMAGE).equals("1")) {
            return true;
        }
        return false;
    }

    private Dialog signOutPopup() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.pop_sign_out_google_account);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView close, signOut;
        close = dialog.getWindow().getDecorView().findViewById(R.id.pop_sign_out_google_account_close_text);
        signOut = dialog.getWindow().getDecorView().findViewById(R.id.pop_sign_out_google_account_sign_out_text);

        close.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                v.setAlpha(0.5f);
                dialog.dismiss();
            } else {
                v.setAlpha(1);
            }
            return true;
        });

        signOut.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                v.setAlpha(0.5f);
                signOut();
                dialog.dismiss();
            } else {
                v.setAlpha(1);
            }
            return true;
        });

        return dialog;
    }

    private Dialog autoBackupPeriodDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.pop_auto_backup_period);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        final TextView daily, weekly, monthly, cancel;
        daily = dialog.getWindow().getDecorView().findViewById(R.id.pop_auto_backup_period_daily);
        weekly = dialog.getWindow().getDecorView().findViewById(R.id.pop_auto_backup_period_weekly);
        monthly = dialog.getWindow().getDecorView().findViewById(R.id.pop_auto_backup_period_monthly);
        cancel = dialog.getWindow().getDecorView().findViewById(R.id.pop_auto_backup_period_cancel);

        View.OnClickListener listener = v -> {
            switch (v.getId()) {

                case R.id.pop_auto_backup_period_daily:
                    Settings.setSetting(Settings.BACKUP_PERIOD, EnumsAndConstants.BackupPeriodItems.Daily.value());
                    autoBackupPeriodText.setText("روزانه");
                    dialog.dismiss();
                    break;

                case R.id.pop_auto_backup_period_weekly:
                    Settings.setSetting(Settings.BACKUP_PERIOD, EnumsAndConstants.BackupPeriodItems.Weekly.value());
                    autoBackupPeriodText.setText("هفتگی");
                    dialog.dismiss();
                    break;

                case R.id.pop_auto_backup_period_monthly:
                    Settings.setSetting(Settings.BACKUP_PERIOD, EnumsAndConstants.BackupPeriodItems.Monthly.value());
                    autoBackupPeriodText.setText("ماهانه");
                    dialog.dismiss();
                    break;

                case R.id.pop_auto_backup_period_cancel:
                    dialog.dismiss();
                    break;

            }
        };

        daily.setOnClickListener(listener);
        weekly.setOnClickListener(listener);
        monthly.setOnClickListener(listener);
        cancel.setOnClickListener(listener);

        return dialog;
    }

    private Dialog maximumFileCountDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.pop_maximum_backup_file_counter);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        TextView oneFile, fiveFile, tenFile, fifteenFile, cancel;
        oneFile = dialog.getWindow().getDecorView().findViewById(R.id.pop_maximum_backup_file_counter_1_file);
        fiveFile = dialog.getWindow().getDecorView().findViewById(R.id.pop_maximum_backup_file_counter_5_file);
        tenFile = dialog.getWindow().getDecorView().findViewById(R.id.pop_maximum_backup_file_counter_10_file);
        fifteenFile = dialog.getWindow().getDecorView().findViewById(R.id.pop_maximum_backup_file_counter_50_file);
        cancel = dialog.getWindow().getDecorView().findViewById(R.id.pop_maximum_backup_file_counter_cancel);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.pop_maximum_backup_file_counter_1_file:
                        Settings.setSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT, String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.One.value()));
                        maximumBackupFileCountText.setText("۱ فایل");
                        dialog.dismiss();
                        break;

                    case R.id.pop_maximum_backup_file_counter_5_file:
                        Settings.setSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT, String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.Five.value()));
                        maximumBackupFileCountText.setText("۵ فایل");
                        dialog.dismiss();
                        break;

                    case R.id.pop_maximum_backup_file_counter_10_file:
                        Settings.setSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT, String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.Ten.value()));
                        maximumBackupFileCountText.setText("۱۰ فایل");
                        dialog.dismiss();
                        break;

                    case R.id.pop_maximum_backup_file_counter_50_file:
                        Settings.setSetting(Settings.BACKUP_MAXIMUM_FILE_COUNT, String.valueOf(EnumsAndConstants.BackupMaximumFileCountItems.Fifty.value()));
                        maximumBackupFileCountText.setText("۵۰ فایل");
                        dialog.dismiss();
                        break;

                    case R.id.pop_maximum_backup_file_counter_cancel:
                        dialog.dismiss();
                        break;

                }
            }
        };

        oneFile.setOnClickListener(listener);
        fiveFile.setOnClickListener(listener);
        tenFile.setOnClickListener(listener);
        fifteenFile.setOnClickListener(listener);
        cancel.setOnClickListener(listener);

        return dialog;
    }

    public Dialog progressDialog(Context context, boolean isUpload, MyMetaData metaData) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.pop_google_drive_progress);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView status = dialog.getWindow().getDecorView().findViewById(R.id.pop_google_drive_progress_text);
        NumberProgressBar progressBar = dialog.getWindow().getDecorView().findViewById(R.id.pop_google_drive_progress_progressbar);

        if (isUpload) {
            progressBar.setVisibility(View.GONE);
            backupAndSendFileToGoogleDrive(context, mDriveResourceClient, false, success -> {
                if (success) {
                    initLastBackupDate();

                    status.setText("پشتیبانگیری موفق");
                    new Handler().postDelayed(() ->
                                    dialog.dismiss()
                            , 2000);

                    Drive.DriveApi.requestSync(mDriveResourceClient.asGoogleApiClient());
                } else {
                    dialog.dismiss();
                }
            });

        } else {

            status.setText("در حال دانلود فایل پشتیبان");
            OpenFileCallback openFileCallback = new OpenFileCallback() {
                @Override
                public void onProgress(long l, long l1) {
                    int progress = (int) (l * 100 / l1);
                    progressBar.setProgress(progress);
                }

                @Override
                public void onContents(@NonNull DriveContents driveContents) {

                    progressBar.setProgress(100);
                    File file = new File(context.getCacheDir(), "Downloaded_" + metaData.getTitle());
                    try {
                        file.createNewFile();
                        OutputStream outputStream = new FileOutputStream(file);
                        IOUtils.copy(driveContents.getInputStream(), outputStream);
                        outputStream.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // extract db and shared preferences
                    try {
                        //  create temp folder for extracting downloaded files
                        File tempFile = new File(getCacheDir(), "Temp");
                        if (!tempFile.exists()) {
                            tempFile.mkdir();
                        }

                        // extracting
                        unzip(file, tempFile);
                        // deleting source file ( downloaded file )
                        file.delete();

                        //  android mode
                        if (new File(tempFile, "android.txt").exists()) {
                            androidRecovery(context, tempFile);
                        } else if (new File(tempFile, "ios.txt").exists()) {  //  ios mode
                            iosRecovery(context, tempFile);
                        } else {
                            if (new File(tempFile, "DB_Dakhlokharj.db").exists()) {
                                iosRecovery(context, tempFile);
                            } else if (new File(tempFile, "dakhlokharj.db").exists()) {
                                androidRecovery(context, tempFile);
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    dialog.dismiss();

                }

                @Override
                public void onError(@NonNull Exception e) {
                }
            };
            mDriveResourceClient.openFile(metaData.getDriveId().asDriveFile(), DriveFile.MODE_READ_ONLY, openFileCallback);

        }
        return dialog;
    }

    private void androidRecovery(Context context, File tempFile) throws IOException {
        // replace new db file with old db file
        File systemDbFile = new File(DBManager.database.getPath());
        File extractedDbFile = new File(tempFile, "dakhlokharj.db");
        copy(extractedDbFile, systemDbFile);

        // replace settings file
        File newShared;
        if (new File(tempFile, Application.AppEnums.SHARED_PREFRENCES.value() + ".xml").exists()) { // android mode
            newShared = new File(tempFile, Application.AppEnums.SHARED_PREFRENCES.value() + ".xml");
            loadSharedPreferencesFromFile(newShared);
        }

        // check TransactionPicture availability and handling
        File zippedPictureFile = new File(tempFile, "TransactionPicture.zip");
        if (zippedPictureFile.exists()) {
            File transactionPictureDir = new File(Settings.getSetting(Settings.APP_DIRECTORY), "TransactionPicture/");
            if (!transactionPictureDir.exists()) {
                transactionPictureDir.mkdir();
            }

//            if (zippedPictureFile.listFiles() != null) {
                ZipUtil.unpack(zippedPictureFile, transactionPictureDir);
//            }
        } else {
            ArrayList<HashMap<String, Object>> allTransactions = Transaction.getAll();
            for (HashMap<String, Object> transaction : allTransactions) {
                Transaction currTrn = new Transaction();
                currTrn.setID(Integer.valueOf(String.valueOf(transaction.get("ID"))));
                currTrn.setTrnImagePath("");
                currTrn.update(new String[]{Transaction.TransactionCol.trnImagePath.value()});
            }
        }

        for (File f : tempFile.listFiles()) {
            f.delete();
        }
        tempFile.delete();

        // update DB Columns
        Application.tir_97_Update_hasNot_trnReference();
        Application.bankTableChecker();
        // update sort hashmap
        Account.updateAccountOrderHashMap(Account.getAll());
        Payee.updatePayeeOrderHashMap(Payee.getAll());

        completeDialog(context, true).show();
    }

    private void iosRecovery(Context context, File tempFile) throws IOException {

        // replace new db file with old db file
        File systemDbFile = new File(DBManager.database.getPath());
        File extractedDbFile = new File(tempFile, "DB_Dakhlokharj.db");
        extractedDbFile.renameTo(new File(tempFile, "dakhlokharj.db")); // rename to android db
        extractedDbFile = new File(tempFile, "dakhlokharj.db");
        copy(extractedDbFile, systemDbFile);

        // check TransactionPicture availability and handling
        File zippedPictureFile = new File(tempFile, "images.zip");
        if (zippedPictureFile.exists()) {
            File transactionPictureDir = new File(Settings.getSetting(Settings.APP_DIRECTORY), "TransactionPicture/");
            if (!transactionPictureDir.exists()) {
                transactionPictureDir.mkdir();
            }

            if (zippedPictureFile.listFiles() != null) {
                ZipUtil.unpack(zippedPictureFile, transactionPictureDir);
            }
        } else {
            ArrayList<HashMap<String, Object>> allTransactions = Transaction.getAll();
            for (HashMap<String, Object> transaction : allTransactions) {
                Transaction currTrn = new Transaction();
                currTrn.setID(Integer.valueOf(String.valueOf(transaction.get("ID"))));
                currTrn.setTrnImagePath("");
                currTrn.update(new String[]{Transaction.TransactionCol.trnImagePath.value()});
            }
        }

        changeIOSFields();

        for (File f : tempFile.listFiles()) {
            f.delete();
        }
        tempFile.delete();

        // update DB Columns
        Application.tir_97_Update_hasNot_trnReference();
        Application.bankTableChecker();
        // update sort hashmap
        Account.updateAccountOrderHashMap(Account.getAll());
        Payee.updatePayeeOrderHashMap(Payee.getAll());

        completeDialog(context, true).show();
    }

    private void changeIOSFields() {

        ArrayList<HashMap<String, Object>> allTransactions = Transaction.getAll();

        for (int i = 0; i < allTransactions.size(); i++) {
            HashMap tempHashMap = allTransactions.get(i);
            Transaction currentTransaction = Transaction.getTransaction(Integer.valueOf(String.valueOf(tempHashMap.get("ID"))));

            String iosImagePath = currentTransaction.getTrnImagePath();
            if (iosImagePath != null && !iosImagePath.equals("")) { // check mikonim age aks dasht
                String newImagePath = Settings.getSetting(Settings.APP_DIRECTORY) + "/TransactionPicture/" + iosImagePath;
                currentTransaction.setTrnImagePath(newImagePath);
            }

            String iosDate = currentTransaction.getTrnDate();
            if (iosDate.contains(".")) {
                iosDate = String.valueOf(((long) Double.parseDouble(currentTransaction.getTrnDate())));
            }
            String newDate = iosDate + "000";
            currentTransaction.setTrnDate(newDate);

            currentTransaction.update(new String[]{Transaction.TransactionCol.trnImagePath.value(), Transaction.TransactionCol.trnDate.value()});
        }

    }

    public Dialog completeDialog(Context context, boolean isSucces) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_google_drive_success);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView close = dialog.getWindow().getDecorView().findViewById(R.id.pop_google_drive_success_close);
        TextView description = dialog.getWindow().getDecorView().findViewById(R.id.pop_google_drive_success_t_description);

        if (isSucces) {
            description.setText(R.string.pop_recovery_success);
        } else {
            description.setText(R.string.pop_recovery_failed);
            description.setTextColor(Color.RED);
        }

        close.setOnClickListener(v -> {
            dialog.dismiss();
            ((Activity) context).finish();
        });

        return dialog;
    }

    public Dialog platformWarning(Context context, PlatformWarningCallBack callBack) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.pop_platform_warning);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView continueT, cancel;
        continueT = dialog.getWindow().getDecorView().findViewById(R.id.pop_platform_warning_continue);
        cancel = dialog.getWindow().getDecorView().findViewById(R.id.pop_platform_warning_cancel);

        continueT.setOnClickListener(v -> {
            callBack.call(true);
            dialog.dismiss();
        });

        cancel.setOnClickListener(v -> {
            callBack.call(false);
            dialog.dismiss();
        });

        return dialog;
    }

}