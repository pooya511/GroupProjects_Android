package com.taxiapps.dakhlokharj;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.fragment.Limits;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import fragments.ExpensesAndIncomes;
import model.Currency;
import model.EnumsAndConstants;
import model.Settings;
import model.Transaction;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class ExpensesOrIncomeListActivity extends BaseActivity {

    private FrameLayout fragmentContainer;
    private ImageView newExpense, nextMonth, backMonth, filter;
    private TextView date;
    public TextView totalAmount;
    private TextView activityHeader;

    private int monthValue;  // default value
    public PersianDate staticPersianDate;
    public int filterValue;
    public long[] month;
    public EnumsAndConstants.TransactionTypes type;
    public Context context;

    private static ExpensesOrIncomeListActivity instance;

    public static ExpensesOrIncomeListActivity getInstance() {
        return instance;
    }

    private void initMonthValue() {
        monthValue = 0;
    }

    @Override
    Context context() {
        return ExpensesOrIncomeListActivity.this;
    }

    @Override
    void init() {

        instance = this;

        newExpense = findViewById(R.id.activity_expense_or_income_list_new_expense_image);
        fragmentContainer = findViewById(R.id.expenses_or_income_fragment_container);
        nextMonth = findViewById(R.id.expenses_or_income_list_activity_next_month);
        backMonth = findViewById(R.id.expenses_or_income_list_activity_back_month);
        date = findViewById(R.id.activity_expenses_or_income_month_text_view);
        totalAmount = findViewById(R.id.expenses_or_income_month_total_amount_text_view);
        filter = findViewById(R.id.expenses_or_income_list_activity_filter);
        activityHeader = findViewById(R.id.expenses_or_income_header_text_view);

        initMonthValue();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Limits.getInstance().mHelper != null)
            if (!Limits.getInstance().mHelper.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            } else {
                Log.d("sabih", "onActivityResult handled by IABUtil.");
            }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Limits.getInstance() != null) {
            if (Limits.getInstance().mHelper != null)
                Limits.getInstance().mHelper.dispose();
            Limits.getInstance().mHelper = null;
        }
    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = v -> {

            EnumsAndConstants.TransactionTypes trnType = null;
            AddAndEditExpensesAndIncomesActivity.IntentTypes intentType = null;
            int limit;

            if (getIntent().getStringExtra("Type").equals("Expense")) {
                intentType = AddAndEditExpensesAndIncomesActivity.IntentTypes.NewExpense;
                trnType = EnumsAndConstants.TransactionTypes.Expense;
            }
            if (getIntent().getStringExtra("Type").equals("Income")) {
                intentType = AddAndEditExpensesAndIncomesActivity.IntentTypes.NewIncome;
                trnType = EnumsAndConstants.TransactionTypes.Income;
            }

            switch (v.getId()) {

                case R.id.activity_expense_or_income_list_new_expense_image:
                    Intent intent = new Intent(ExpensesOrIncomeListActivity.this, AddAndEditExpensesAndIncomesActivity.class);

                    limit = trnType == EnumsAndConstants.TransactionTypes.Expense ? 10 : 4;
                    if (Integer.valueOf(Transaction.getCountThisMonth(trnType)) < limit) {
                        intent.putExtra("Type", intentType.value());
                        startActivity(intent);
                    } else {
                        if (Application.isLicenseValid()) {
                            intent.putExtra("Type", intentType.value());
                            startActivity(intent);
                        } else {
                            String title;
                            if (trnType == EnumsAndConstants.TransactionTypes.Expense)
                                title = "در نسخه رایگان حداکثر " + limit + " مورد هزینه میتوانید ثبت کنید!";
                            else
                                title = "در نسخه رایگان حداکثر " + limit + " مورد درآمد میتوانید ثبت کنید!";

                            String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                            String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                            Payment.newInstance(R.drawable.ic_dakhlokharj,
                                    EnumsAndConstants.APP_ID,
                                    EnumsAndConstants.APP_SOURCE,
                                    EnumsAndConstants.APP_VERSION,
                                    "دخل و خرج",
                                    Application.currentBase64,
                                    title,
                                    limitationText,
                                    userNumber,
                                    EnumsAndConstants.DEVICE_ID,
                                    EnumsAndConstants.SERVER_URL,
                                    EnumsAndConstants.GATEWAY_URL,
                                    new TX_License.SetLicenseCallback() {
                                        @Override
                                        public void call(long expireDate, String license) {

                                            Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                            Settings.setSetting(Settings.LICENSE_DATA, license);

                                            try {
                                                JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                                Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                            MainActivity.getInstance().updateFragment(false);
                                            MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                            if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                                Transaction.appPurchased();

                                            MainActivity.getInstance().reloadMainData();

                                            if (SettingActivity.getInstance() != null)
                                                SettingActivity.getInstance().premiumUIHandling(false);
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    },
                                    new Payment.UserNameCallBack() {
                                        @Override
                                        public void getUserName(String username) {
                                            Settings.setSetting(Settings.TX_USER_NUMBER, username);
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    }
                            ).show(getFragmentManager(), "tx_payment");
                        }
                    }
                    break;

                case R.id.expenses_or_income_list_activity_next_month:
                    monthValue--;
                    initFragment(monthValue, filterValue, type);
                    break;

                case R.id.expenses_or_income_list_activity_back_month:
                    monthValue++;
                    initFragment(monthValue, filterValue, type);
                    break;

                case R.id.expenses_or_income_list_activity_filter:
                    getFilterValue();
                    if (filterValue == 0) {
                        filterValue++;
                        if (getIntent().getStringExtra("Type").equals("Expense"))
                            Settings.setSetting(Settings.EXPENSE_ADAPTER_FILTER, String.valueOf(filterValue));
                        if (getIntent().getStringExtra("Type").equals("Income"))
                            Settings.setSetting(Settings.INCOME_ADAPTER_FILTER, String.valueOf(filterValue));

                    } else if (filterValue == 1) {
                        filterValue--;
                        if (getIntent().getStringExtra("Type").equals("Expense"))
                            Settings.setSetting(Settings.EXPENSE_ADAPTER_FILTER, String.valueOf(filterValue));
                        if (getIntent().getStringExtra("Type").equals("Income"))
                            Settings.setSetting(Settings.INCOME_ADAPTER_FILTER, String.valueOf(filterValue));

                    }
                    initFragment(monthValue, filterValue, type);
                    switch (filterValue) {
                        case 0:
                            Toast.makeText(context, "نمایش به تفکیک دسته بندی", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            Toast.makeText(context, "نمایش به تفکیک روز", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    break;
            }

        };

        newExpense.setOnClickListener(listeners);
        nextMonth.setOnClickListener(listeners);
        backMonth.setOnClickListener(listeners);
        filter.setOnClickListener(listeners);

    }

    @Override
    void setAmountTypeFaces() {
        totalAmount.setTypeface(Application.amountTypeFace(ExpensesOrIncomeListActivity.this));
    }

    private void getFilterValue() {
        if (getIntent().getStringExtra("Type").equals("Expense")) {
            filterValue = Integer.parseInt(Settings.getSetting(Settings.EXPENSE_ADAPTER_FILTER));
        }
        if (getIntent().getStringExtra("Type").equals("Income")) {
            filterValue = Integer.parseInt(Settings.getSetting(Settings.INCOME_ADAPTER_FILTER));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses_or_income_list);

        context = ExpensesOrIncomeListActivity.this;

        init();
        setAmountTypeFaces();
        setListeners();

        if (getIntent().getStringExtra("Type").equals("Expense")) {
            type = EnumsAndConstants.TransactionTypes.Expense;
            activityHeader.setText("هزینه ها");
        } else if (getIntent().getStringExtra("Type").equals("Income")) {
            type = EnumsAndConstants.TransactionTypes.Income;
            activityHeader.setText("درآمد ها");
        }

        type = getIntent().getStringExtra("Type").equals("Expense") ? EnumsAndConstants.TransactionTypes.Expense : EnumsAndConstants.TransactionTypes.Income;
        getFilterValue();
        initFragment(monthValue, filterValue, type);
    }

    private void initFragment(int monthOffset, int filterType, EnumsAndConstants.TransactionTypes type) {
        month = PublicModules.getMonthInterval(monthOffset);
        PersianDate pCalendar = new PersianDate(month[0]);

        staticPersianDate = pCalendar;
        PersianDateFormat formatter = new PersianDateFormat("F Y");
        date.setText(formatter.format(pCalendar));

        android.app.Fragment expenseAndIncomeFragment = null;

        if (type == EnumsAndConstants.TransactionTypes.Expense) {
            getInstance().totalAmount.setTextColor(getResources().getColor(R.color.red));
            updateFooter(month[0], month[1], EnumsAndConstants.TransactionTypes.Expense);
            expenseAndIncomeFragment = ExpensesAndIncomes.newInstance(month, filterType, EnumsAndConstants.TransactionTypes.Expense);
        } else if (type == EnumsAndConstants.TransactionTypes.Income) {
            getInstance().totalAmount.setTextColor(getResources().getColor(R.color.green));
            updateFooter(month[0], month[1], EnumsAndConstants.TransactionTypes.Income);
            expenseAndIncomeFragment = ExpensesAndIncomes.newInstance(month, filterType, EnumsAndConstants.TransactionTypes.Income);
        }

        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(fragmentContainer.getId(), expenseAndIncomeFragment, "expenseAndIncome");
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initFragment(monthValue, filterValue, type);
    }

    public static void updateFooter(long startOfMonth, long endOfMonth, EnumsAndConstants.TransactionTypes type) {
        if (getInstance() != null) {
            if (getInstance().totalAmount != null) {
                getInstance().totalAmount.setText(Currency.CurrencyString.getCurrencyString(Transaction.getSumAll(String.valueOf(startOfMonth), String.valueOf(endOfMonth), type), Currency.CurrencyMode.Short).separated);
            }
        }
    }

}