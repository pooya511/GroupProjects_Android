package com.taxiapps.dakhlokharj;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.Mask;
import com.redmadrobot.inputmask.model.CaretString;
import com.sevenheaven.iosswitch.ShSwitchView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import adapters.PayeeListAdapter;
import adapters.PopPayeeAdapter;
import model.EnumsAndConstants;
import model.Payee;
import model.Settings;
import modules.PublicDialogs;
import modules.PublicModules;
import utils.AppModules;
import utils.DialogUtils;

public class NewAndEditPayeeActivity extends BaseActivity {

    private EditText payeeNameEdittext, accountNumberEdittext, cardNumberEdittext, phoneNumberEdittext;
    private ShSwitchView payeeStatusSwitch;
    private ConstraintLayout removeLayout;
    private ImageView payeeNameClear, accountNumberClear, cardNumberClear, phoneNumberClear;
    private Payee payeeFromList;
    private TextView titleTextView, saveTextView;

    @Override
    Context context() {
        return NewAndEditPayeeActivity.this;
    }

    @Override
    void init() {
        payeeNameEdittext = findViewById(R.id.new_and_edit_payee_activity_name_edittext);
        accountNumberEdittext = findViewById(R.id.new_and_edit_payee_activity_account_number_edittext);
        accountNumberEdittext = (EditText) AppModules.setTypeFace(this, accountNumberEdittext, "Roboto-Light.ttf");
        cardNumberEdittext = findViewById(R.id.new_and_edit_payee_activity_card_number_edittext);
        cardNumberEdittext = (EditText) AppModules.setTypeFace(this, cardNumberEdittext, "Roboto-Light.ttf");
        phoneNumberEdittext = findViewById(R.id.new_and_edit_payee_activity_phone_number_edittext);
        phoneNumberEdittext = (EditText) AppModules.setTypeFace(this, phoneNumberEdittext, "Roboto-Light.ttf");
        payeeStatusSwitch = findViewById(R.id.new_and_edit_payee_activity_payee_status_switch);
        removeLayout = findViewById(R.id.new_and_edit_payee_activity_remove_layout);
        titleTextView = findViewById(R.id.new_and_edit_payee_activity_title_textview);
        saveTextView = findViewById(R.id.new_and_edit_payee_activity_save_textview);


        payeeNameClear = findViewById(R.id.new_and_edit_payee_activity_name_edittext_clear_btn);
        accountNumberClear = findViewById(R.id.new_and_edit_payee_activity_account_number_edittext_clear_btn);
        cardNumberClear = findViewById(R.id.new_and_edit_payee_activity_card_number_edittext_clear_btn);
        phoneNumberClear = findViewById(R.id.new_and_edit_payee_activity_phone_number_edittext_clear_btn);
    }

    @Override
    void setListeners() {

        View.OnClickListener listeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(payeeNameClear)) {
                    payeeNameEdittext.setText("");
                }
                if (v.equals(accountNumberClear)) {
                    accountNumberEdittext.setText("");
                }
                if (v.equals(cardNumberClear)) {
                    cardNumberEdittext.setText("");
                }
                if (v.equals(phoneNumberClear)) {
                    phoneNumberEdittext.setText("");
                }
                if (v.equals(saveTextView)) {
                    if (payeeNameEdittext.getText().toString().equals("")) {
                        Dialog dialog = PublicDialogs.makeMessageDialog(NewAndEditPayeeActivity.this, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "ورود نام برای طرف حساب اجباری است.", "" , EnumsAndConstants.APP_SOURCE);
                        dialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        dialog.show();
                    } else {
                        if (PayeeListAdapter.targetPayee != null) {
                            payeeFromList.setPyeName(payeeNameEdittext.getText().toString());
                            payeeFromList.setPyeAccount(accountNumberEdittext.getText().toString());
                            payeeFromList.setPyeAccount2(cardNumberEdittext.getText().toString().replaceAll("-", ""));
                            if (payeeStatusSwitch.isOn())
                                payeeFromList.setPyeIsActive(1);
                            else
                                payeeFromList.setPyeIsActive(0);
                            payeeFromList.setPyePhone(phoneNumberEdittext.getText().toString());
                            payeeFromList.update(new String[]{Payee.PayeeCol.pyeAccount.value(), Payee.PayeeCol.pyeAccount2.value(), Payee.PayeeCol.pyeIsActive.value(), Payee.PayeeCol.pyeName.value(), Payee.PayeeCol.pyePhone.value()});
                            setResult(RESULT_OK);
                            //
                            customToast("ویرایش طرف حساب", "طرف حساب ویرایش شد");
                            //
                            finish();
                        } else {
                            Payee newPayee = new Payee();
                            newPayee.setID(0);
                            newPayee.setPyeName(payeeNameEdittext.getText().toString());
                            newPayee.setPyeAccount(accountNumberEdittext.getText().toString());
                            newPayee.setPyeAccount2(cardNumberEdittext.getText().toString().replaceAll("-", ""));
                            if (payeeStatusSwitch.isOn())
                                newPayee.setPyeIsActive(1);
                            else
                                newPayee.setPyeIsActive(0);
                            newPayee.setPyePhone(phoneNumberEdittext.getText().toString());
                            newPayee.insert();
                            setResult(RESULT_OK);
                            //
                            customToast("اضافه کردن طرف حساب", "طرف حساب جدید اضافه شد");
                            //

                            HashMap<String , String> map = Settings.loadMap(Settings.PAYEE_ORDER_MAP);
                            map.put(String.valueOf(newPayee.getID()) , String.valueOf(999));
                            Settings.saveMap(Settings.PAYEE_ORDER_MAP , map);

                            finish();
                        }
                        if (PopPayeeAdapter.getInstance() != null) {
                            PopPayeeAdapter.getInstance().setReturnedData(Payee.sortPayeesOrder(Payee.getAllActive() , Settings.loadMap(Settings.PAYEE_ORDER_MAP)));
                            PopPayeeAdapter.getInstance().notifyDataSetChanged();
                            Payee.payeeRecyclerList.setAdapter(PopPayeeAdapter.getInstance());
                            Payee.payeeRecyclerList.setVisibility(View.VISIBLE);
                            Payee.didntFindlayout.setVisibility(View.GONE);
                        }
                    }
                }
                if (v.equals(removeLayout)) {
                    Dialog removePayeeDialog = removePayee(NewAndEditPayeeActivity.this);
                    removePayeeDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    removePayeeDialog.show();
                }
            }
        };
        payeeNameClear.setOnClickListener(listeners);
        accountNumberClear.setOnClickListener(listeners);
        cardNumberClear.setOnClickListener(listeners);
        phoneNumberClear.setOnClickListener(listeners);
        saveTextView.setOnClickListener(listeners);
        removeLayout.setOnClickListener(listeners);
    }

    @Override
    void setAmountTypeFaces() {
        //Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_and_edit_payee);

        init();
        setAmountTypeFaces();
        setListeners();

        payeeStatusSwitch.setOn(true);
        if (PayeeListAdapter.targetPayee != null) {
            payeeFromList = PayeeListAdapter.targetPayee;
            payeeNameEdittext.setText(payeeFromList.getPyeName());
            accountNumberEdittext.setText(payeeFromList.getPyeAccount());

            if (payeeFromList.getPyeAccount2() != null) {
                final Mask mask = new Mask("[0000]-[0000]-[0000]-[0000]");
                final String input = payeeFromList.getPyeAccount2();
                final Mask.Result result = mask.apply(
                        new CaretString(
                                input,
                                input.length()
                        ),
                        true // you may consider disabling autocompletion for your case
                );
                final String output = result.getFormattedText().getString();
                cardNumberEdittext.setText(output);
            }

            phoneNumberEdittext.setText(payeeFromList.getPyePhone());
            if (payeeFromList.getPyeIsActive() == 1)
                payeeStatusSwitch.setOn(true);
            else
                payeeStatusSwitch.setOn(false);

        } else {
            titleTextView.setText("طرف حساب");
            removeLayout.setVisibility(View.GONE);
        }

        //card editText mask
        final MaskedTextChangedListener cardNumberTextWatcher = new MaskedTextChangedListener("[0000]-[0000]-[0000]-[0000]", false, cardNumberEdittext, null, (b, s) -> cardNumberEdittext.setSelection(s.length()));

        cardNumberEdittext.setOnKeyListener((v, keyCode, event) -> {
            String userText = cardNumberEdittext.getText().toString();
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                cardNumberEdittext.setText(userText.substring(0, userText.length()));
                cardNumberEdittext.setSelection(cardNumberEdittext.getText().toString().length());
            }
            return false;
        });

        cardNumberEdittext.addTextChangedListener(cardNumberTextWatcher);
        cardNumberEdittext.setOnFocusChangeListener(cardNumberTextWatcher);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (payeeNameEdittext.isFocused()) {
                    if (!s.toString().equals("")) {
                        payeeNameClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        payeeNameClear.setVisibility(View.GONE);
                    }
                } else if (!payeeNameEdittext.isFocused()) {
                    payeeNameClear.setVisibility(View.GONE);
                }
                if (accountNumberEdittext.isFocused()) {
                    if (!s.toString().equals("")) {
                        accountNumberClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        accountNumberClear.setVisibility(View.GONE);
                    }
                } else if (!accountNumberEdittext.isFocused()) {
                    accountNumberClear.setVisibility(View.GONE);
                }
                if (cardNumberEdittext.isFocused()) {
                    if (!s.toString().equals("")) {
                        cardNumberClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        cardNumberClear.setVisibility(View.GONE);
                    }
                } else if (!cardNumberEdittext.isFocused()) {
                    cardNumberClear.setVisibility(View.GONE);
                }
                if (phoneNumberEdittext.isFocused()) {
                    if (!s.toString().equals("")) {
                        phoneNumberClear.setVisibility(View.VISIBLE);
                    } else if (s.toString().equals("")) {
                        phoneNumberClear.setVisibility(View.GONE);
                    }
                } else if (!phoneNumberEdittext.isFocused()) {
                    phoneNumberClear.setVisibility(View.GONE);
                }
            }
        };

        payeeNameEdittext.addTextChangedListener(watcher);
        accountNumberEdittext.addTextChangedListener(watcher);
        cardNumberEdittext.addTextChangedListener(watcher);
        phoneNumberEdittext.addTextChangedListener(watcher);

        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v.equals(payeeNameEdittext)) {
                    if (hasFocus) {
                        if (!payeeNameEdittext.getText().toString().equals("")) {
                            payeeNameClear.setVisibility(View.VISIBLE);
                        } else if (payeeNameEdittext.getText().toString().equals("")) {
                            payeeNameClear.setVisibility(View.GONE);
                        }
                    } else
                        payeeNameClear.setVisibility(View.GONE);
                }
                if (v.equals(accountNumberEdittext)) {
                    if (hasFocus) {
                        if (!accountNumberEdittext.getText().toString().equals("")) {
                            accountNumberClear.setVisibility(View.VISIBLE);
                        } else if (accountNumberEdittext.getText().toString().equals("")) {
                            accountNumberClear.setVisibility(View.GONE);
                        }
                    } else
                        accountNumberClear.setVisibility(View.GONE);
                }
                if (v.equals(cardNumberEdittext)) {
                    if (hasFocus) {
                        if (!cardNumberEdittext.getText().toString().equals("")) {
                            cardNumberClear.setVisibility(View.VISIBLE);
                        } else if (cardNumberEdittext.getText().toString().equals("")) {
                            cardNumberClear.setVisibility(View.GONE);
                        }
                    } else
                        cardNumberClear.setVisibility(View.GONE);
                }
                if (v.equals(phoneNumberEdittext)) {
                    if (hasFocus) {
                        if (!phoneNumberEdittext.getText().toString().equals("")) {
                            phoneNumberClear.setVisibility(View.VISIBLE);
                        } else if (phoneNumberEdittext.getText().toString().equals("")) {
                            phoneNumberClear.setVisibility(View.GONE);
                        }
                    } else
                        phoneNumberClear.setVisibility(View.GONE);
                }

            }
        };
        payeeNameEdittext.setOnFocusChangeListener(onFocusChangeListener);
        accountNumberEdittext.setOnFocusChangeListener(onFocusChangeListener);
        cardNumberEdittext.setOnFocusChangeListener(onFocusChangeListener);
        phoneNumberEdittext.setOnFocusChangeListener(onFocusChangeListener);
    }

    private Dialog removePayee(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View resetDataLayout = inflater.inflate(R.layout.pop_remove_payee, null);

        final TextView removePayee, cancel;
        removePayee = resetDataLayout.findViewById(R.id.pop_remove_payee_reset);
        cancel = resetDataLayout.findViewById(R.id.pop_remove_payee_cancel);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 21;
                break;

            case 2:
                width = 70;
                height = 16;
                break;

            case 3:
                width = 55;
                height = 13;
                break;

        }

        final Dialog resetDataDialog = PublicDialogs.makeDialog(context, resetDataLayout, width, height, false);

        View.OnClickListener dialogListeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(cancel))
                    resetDataDialog.dismiss();

                if (v.equals(removePayee)) {
                    payeeFromList.delete();
                    HashMap<String , String> map = Settings.loadMap(Settings.PAYEE_ORDER_MAP);
                    map.remove(String.valueOf(payeeFromList.getID()));
                    Settings.saveMap(Settings.PAYEE_ORDER_MAP , map);
                    setResult(RESULT_OK);
                    finish();
                }
            }
        };

        cancel.setOnClickListener(dialogListeners);
        removePayee.setOnClickListener(dialogListeners);

        return resetDataDialog;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    private void customToast(String title, String description) {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        ImageView image = layout.findViewById(R.id.toast_img);
        image.setImageResource(R.drawable.ic_success);
        TextView titleText = layout.findViewById(R.id.toast_title);
        titleText.setText(title);
        TextView descriptionText = layout.findViewById(R.id.toast_description);
        descriptionText.setText(description);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 25);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}
