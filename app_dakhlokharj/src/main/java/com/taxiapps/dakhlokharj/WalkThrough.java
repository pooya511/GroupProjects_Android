package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import adapters.WalkThroughAdapter;
import fragments.WalkThroughFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Parsa on 2018-01-31.
 */

public class WalkThrough extends FragmentActivity {

    private ViewPager viewPager;
    private TextView startApp;
    private View page1, page2, page3, page4, page5;

    private void init() {

        viewPager = findViewById(R.id.activity_guid_walkthrough_viewpager);
        startApp = findViewById(R.id.activity_guid_walkthrough_start_app);
        page1 = findViewById(R.id.activity_guid_walkthrough_page1);
        page2 = findViewById(R.id.activity_guid_walkthrough_page2);
        page3 = findViewById(R.id.activity_guid_walkthrough_page3);
        page4 = findViewById(R.id.activity_guid_walkthrough_page4);
        page5 = findViewById(R.id.activity_guid_walkthrough_page5);

    }

    private void setListener() {

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                indicatorHandling(position + 1);

                if (position + 1 == viewPager.getAdapter().getCount())
                    startApp.setVisibility(View.INVISIBLE);
                else
                    startApp.setVisibility(View.VISIBLE);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        View.OnClickListener listener = v -> {

            if (v.equals(startApp)) {
                for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                finish();
            }

        };

        startApp.setOnClickListener(listener);

    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guid_walkthrough);

        init();
        setListener();

        viewPager.setAdapter(new WalkThroughAdapter(getSupportFragmentManager(), initFragments()));
        indicatorHandling(1); // init avalie
    }

    @Override
    public void onBackPressed() {

    }

    private ArrayList<Fragment> initFragments() {

        ArrayList<Fragment> fragments = new ArrayList<>();

        fragments.add(WalkThroughFragment.newInstance(R.drawable.img_page_1,
                "وقتی می دونید چقدر هزینه می کنید و \n چقدر درآمد دارید یک حس خوب بهتون دست میده!",
                false
        ));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.img_page_2,
                "با مدیریت هزینه ها همیشه ایده های جدید \n‌ به سرتون میزنه!",
                false
        ));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.img_page_3,
                "با بودجه بندی مناسب در برنامه دخل و خرج\n هیچ وقت زیادی خرج نمی کنید!",
                false
        ));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.ic_walkthrough_google_drive,
                "نگران نباشید برای اطمینان خاطر شما\n امکان پشتیبانگیری ابری بر روی Google Drive را فراهم کردیم!",
                false
        ));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.img_page_5,
                "حالا وقتشه با یک مدیریت صحیح به پول هاتون آب بدید\n تا زیاد بشن!",
                true
        ));

        return fragments;
    }

    private void indicatorHandling(int currentPage) {

        switch (currentPage) {
            case 1:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 2:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 3:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 4:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 5:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                break;
        }

    }

}