package com.taxiapps.dakhlokharj;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;
import com.sevenheaven.iosswitch.ShSwitchView;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.fragment.Limits;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import TxAnalytics.TxAnalytics;
import adapters.BackUpFilesAdapter;
import db.DBManager;
import db.DataBaseOpenHelper;
import model.Backup;
import model.Currency;
import model.EnumsAndConstants;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import utils.AppModules;

import static TxAnalytics.TxAnalytics.MessageAdd;

public class SettingActivity extends BaseActivity {

    private TextView rasTextView, premiumText1, premiumText2, versionText, appSourceText;
    private TextView currencyTextView;
    private ImageView menuIcon;
    private Button knowMoreBtn;
    private ImageView closePremium;
    private ConstraintLayout buyCompleteVersionLayout;
    private ConstraintLayout passwordLayout;
    private static ConstraintLayout fingerPrintLayout;
    private ShSwitchView fingerSwitch;
    private ShSwitchView smsSwitch;
    private ConstraintLayout rasgirLayout;
    private ConstraintLayout yadavarLayout;
    private ConstraintLayout wordCutLayout;
    private ConstraintLayout currencyLayout;
    public ConstraintLayout backupGoogleDriveLayout;
    private ConstraintLayout removeLastYearDataLayout;
    private ConstraintLayout clearDataBaseLayout;
    private ConstraintLayout resetSettingLayout;
    private ConstraintLayout aboutTaxiAppsLayout;
    private ConstraintLayout rateUsLayout;
    private ConstraintLayout sendMessageLayout;
    private ConstraintLayout otherAppsLayout;
    private ConstraintLayout shareLinkLayout;
    private ConstraintLayout latelyChangesLayout;
    private ConstraintLayout smsLayout;
    private ConstraintLayout seeLicenseLayout;
    private ConstraintLayout walkThroughLayout;
    private ConstraintLayout menuIconContainer;

    private final int PERMISSIONS_REQUEST_WRITE_STORAGE = 2;
    private final int PERMISSIONS_REQUEST_READ_STORAGE = 3;

    private static SettingActivity instance;

    public static SettingActivity getInstance() {
        if (instance == null)
            return null;
        return instance;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            String dialogTitle = "رمز ورود";
            String dialogDescription = "رمز ورود با موفقیت تنظیم شد. برای ورود در دفعات بعدی به این رمز احتیاج خواهید داشت.";
            Dialog dialog = PublicDialogs.makeMessageDialog(SettingActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, dialogTitle, dialogDescription, "", EnumsAndConstants.APP_SOURCE);
            dialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
            dialog.show();

        }
        if (requestCode == 20 && resultCode == RESULT_OK) {
            customToast("ویرایش رمز", "رمز ویرایش شد", true);
        }
        if (Limits.getInstance() != null)
            if (Limits.getInstance().mHelper != null)
                if (!Limits.getInstance().mHelper.handleActivityResult(requestCode, resultCode, data)) {
                    super.onActivityResult(requestCode, resultCode, data);
                } else {
                    Log.d("sabih", "onActivityResult handled by IABUtil.");
                }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Limits.getInstance() != null) {
            if (Limits.getInstance().mHelper != null)
                Limits.getInstance().mHelper.dispose();
            Limits.getInstance().mHelper = null;
            //  Log.d(childClassName,"destroy");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Backup.backup(SettingActivity.this, Backup.BackupType.Storage);
                    customToast("پشتیبانگیری", "پشتیبانگیری با موفقیت انجام شد", true);
                } else {
                    customToast("پشتیبانگیری", "خطا در پشتیبانگیری", false);
                    if (ActivityCompat.shouldShowRequestPermissionRationale(SettingActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "پشتیبانگیری";
                        String denyDialogDescription = "برای پشتیبانگیری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(SettingActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog alwaysDenyDialog = alwaysDenyDialog(SettingActivity.this);
                        alwaysDenyDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        alwaysDenyDialog.show();
                    }
                }
                break;
            case PERMISSIONS_REQUEST_READ_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    restoreBackup();
                else {
                    customToast("بازیابی", "خطا در بازیابی", false);
                    if (ActivityCompat.shouldShowRequestPermissionRationale(SettingActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "بازیابی";
                        String denyDialogDescription = "برای بازیابی نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(SettingActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog alwaysDenyDialog = alwaysDenyDialog(SettingActivity.this);
                        alwaysDenyDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        alwaysDenyDialog.show();
                    }
                }
                break;

        }
    }

    public static void checkFingerPrintAvailability() {
        if (Application.fingerPrintIsAvailable) {
            if (Settings.getSetting(Settings.PASSWORD).equals("")) {
                fingerPrintLayout.setVisibility(View.GONE);
            } else {
                fingerPrintLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    Context context() {
        return SettingActivity.this;
    }

    @Override
    void init() {
        rasTextView = findViewById(R.id.setting_activity_rasgir_title_textView);
        premiumText1 = findViewById(R.id.setting_activity_premium_box_textview1);
        premiumText2 = findViewById(R.id.setting_activity_premium_box_textview2);
        closePremium = findViewById(R.id.setting_activity_close_premium);
        buyCompleteVersionLayout = findViewById(R.id.setting_activity_buy_complete_version_layout);
        passwordLayout = findViewById(R.id.setting_activity_password_layout);
        knowMoreBtn = findViewById(R.id.setting_activity_know_more_btn);
        menuIcon = findViewById(R.id.setting_activity_ic_menu);
        menuIconContainer = findViewById(R.id.setting_activity_ic_menu_container);
        rasgirLayout = findViewById(R.id.setting_activity_rasgir_layout);
        yadavarLayout = findViewById(R.id.setting_activity_yadavar_layout);
        wordCutLayout = findViewById(R.id.setting_activity_wordcut_layout);
        currencyLayout = findViewById(R.id.setting_activity_currency_layout);
        backupGoogleDriveLayout = findViewById(R.id.setting_activity_backup_google_layout);
        removeLastYearDataLayout = findViewById(R.id.setting_activity_remove_last_year_data_layout);
        clearDataBaseLayout = findViewById(R.id.setting_activity_clear_database_layout);
        resetSettingLayout = findViewById(R.id.setting_activity_reset_setting_layout);
        seeLicenseLayout = findViewById(R.id.setting_activity_see_license_layout);
        aboutTaxiAppsLayout = findViewById(R.id.setting_activity_about_taxiapps_layout);
        rateUsLayout = findViewById(R.id.setting_activity_rate_us_layout);
        sendMessageLayout = findViewById(R.id.setting_activity_send_message_layout);
        otherAppsLayout = findViewById(R.id.setting_activity_other_apps_layout);
        shareLinkLayout = findViewById(R.id.setting_activity_share_link_layout);
        latelyChangesLayout = findViewById(R.id.setting_activity_Lately_changes_layout);
        fingerPrintLayout = findViewById(R.id.setting_activity_fingerprint_layout);
        fingerSwitch = findViewById(R.id.setting_activity_fingerprint_switch);
        smsLayout = findViewById(R.id.setting_activity_sms_layout);
        smsSwitch = findViewById(R.id.setting_activity_sms_switch);
        currencyTextView = findViewById(R.id.setting_activity_currency_textview);
        versionText = findViewById(R.id.setting_activity_version_text_view);
        walkThroughLayout = findViewById(R.id.setting_activity_walkthrough_layout);
        appSourceText = findViewById(R.id.setting_activity_source_text_view);

        if (Application.isLicenseValid()) {
            premiumUIHandling(false);
        } else {
            premiumUIHandling(true);
        }

        Typeface yekan = Typeface.createFromAsset(getAssets(), "fonts/yekan.ttf");
        Typeface mj_dinar = Typeface.createFromAsset(getAssets(), "fonts/Mj_Dinar Two Medium.ttf");
        rasTextView.setTypeface(yekan);
        knowMoreBtn.setTypeface(mj_dinar);
        premiumText1.setTypeface(mj_dinar);
        premiumText2.setTypeface(mj_dinar);

        checkFingerPrintAvailability();
    }

    @Override
    void setListeners() {

        View.OnClickListener clickListeners = new View.OnClickListener() {

            String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER);

            @Override
            public void onClick(View view) {
                if (view.equals(passwordLayout)) {
                    if (Settings.getSetting(Settings.PASSWORD).equals("")) {
                        Intent intent = new Intent(SettingActivity.this, PasswordActivity.class);
                        intent.putExtra("PassEnteringType", PasswordActivity.PassEnteringType.UserWantToSet.value());
                        startActivityForResult(intent, 1);
//                        overridePendingTransition(R.anim.start_slide_up, R.anim.end_slide_up);
                    } else {
                        Dialog dialog = passwordDialog(SettingActivity.this);
                        dialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        dialog.show();
                    }
                }

                if (view.equals(rasgirLayout)) {
                    String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER);

                    try {
                        MessageAdd("Rasgir", TxAnalytics.MessageTypes.ADCLICK, EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, userNumber, EnumsAndConstants.SERVER_URL, new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                Log.i("MessageAdd", "Not Working");
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                Log.i("MessageAdd", "is Working");
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    switch (EnumsAndConstants.APP_SOURCE) {
                        case "google":
                            Intent intention = new Intent(Intent.ACTION_VIEW);
                            intention.setData(Uri.parse(getResources().getString(R.string.rasgir_cheque_google_play_link)));
                            startActivity(intention);
                            break;
                        case "bazaar":
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(getResources().getString(R.string.rasgir_cheque_bazaar_link)));
                            intent.setPackage("com.farsitel.bazaar");
                            startActivity(intent);
                            break;
                        case "myket":
                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
                            intent1.setData(Uri.parse(getResources().getString(R.string.rasgir_cheque_myket_link)));
                            intent1.setPackage("ir.mservices.market");
                            startActivity(intent1);
                            break;
                    }
                }

                if (view.equals(wordCutLayout)) {
                    String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER);

                    try {
                        MessageAdd("WordCut", TxAnalytics.MessageTypes.ADCLICK, EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, userNumber, EnumsAndConstants.SERVER_URL, new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                Log.i("MessageAdd", "Not Working");
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                Log.i("MessageAdd", "is Working");
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    switch (EnumsAndConstants.APP_SOURCE) {
                        case "google":
                            Intent intention = new Intent(Intent.ACTION_VIEW);
                            intention.setData(Uri.parse(getResources().getString(R.string.wordcut_google_play_link)));
                            startActivity(intention);
                            break;
                        case "bazaar":
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(getResources().getString(R.string.wordcut_bazaar_link)));
                            intent.setPackage("com.farsitel.bazaar");
                            startActivity(intent);
                            break;
                        case "myket":
                            Intent intention1 = new Intent(Intent.ACTION_VIEW);
                            intention1.setData(Uri.parse(getResources().getString(R.string.wordcut_google_play_link)));
                            intention1.setPackage("ir.mservices.market");
                            startActivity(intention1);
                            break;
                    }
                }

                if (view.equals(yadavarLayout)) {

                    try {
                        MessageAdd("Yadavar", TxAnalytics.MessageTypes.ADCLICK, EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, userNumber, EnumsAndConstants.SERVER_URL, new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                Log.i("MessageAdd", "Not Working");
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                Log.i("MessageAdd", "is Working");
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    switch (EnumsAndConstants.APP_SOURCE) {
                        case "google":
                            Intent intention = new Intent(Intent.ACTION_VIEW);
                            intention.setData(Uri.parse(getResources().getString(R.string.yadavare_cheque_google_play_link)));
                            startActivity(intention);
                            break;
                        case "bazaar":
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(getResources().getString(R.string.yadavare_cheque_bazaar_link)));
                            intent.setPackage("com.farsitel.bazaar");
                            startActivity(intent);
                            break;
                        case "myket":
//                            Intent intention1 = new Intent(Intent.ACTION_VIEW);
//                            intention1.setData(Uri.parse(getResources().getString(R.string.yadavare_cheque_google_play_link)));//TODO
//                            startActivity(intention1);
                            break;
                    }
                }

                if (view.equals(currencyLayout)) {
                    currencyDialog(SettingActivity.this).show();
                }

                if (view.equals(smsLayout)) {
                    Intent intent = new Intent(SettingActivity.this, FilterBankSmsActivity.class);
                    startActivity(intent);
                }

                if (view.equals(removeLastYearDataLayout))
                    removePastYearsData(SettingActivity.this).show();


                if (view.equals(clearDataBaseLayout))
                    resetData(SettingActivity.this).show();


                if (view.equals(resetSettingLayout))
                    resetSettings(SettingActivity.this).show();


                if (view.equals(aboutTaxiAppsLayout)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.site_link)));
                    startActivity(intent);
                }

                if (view.equals(rateUsLayout)) {
                    int rate = Settings.getSetting(Settings.USER_RATE).equals("0") ? 0 : Integer.parseInt(Settings.getSetting(Settings.USER_RATE));
                    PublicDialogs.rateUsDialog(SettingActivity.this, rate, EnumsAndConstants.APP_ID, EnumsAndConstants.APP_VERSION, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.DEVICE_ID, userNumber, EnumsAndConstants.SERVER_URL, rate1 -> {
                        Settings.setSetting(Settings.USER_RATE, String.valueOf(rate1)); // ذخیره در تنظیمات
                        Log.i("rateUsDialog", String.valueOf(rate1));
                    }).show();
                }


                if (view.equals(sendMessageLayout)) {
                    PublicModules.contactUsByEmail(SettingActivity.this, "دخل و خرج", EnumsAndConstants.APP_VERSION);
                }
//                    if (Settings.getSetting(Settings.SettingEnums.TX_USER_NUMBER.value()).equals(""))
//                        PublicDialogs.getUserNumber(SettingActivity.this, R.drawable.ic_dakhlokharj, EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, userNumber, EnumsAndConstants.SERVER_URL, new PublicDialogs.GetUserNumberCallBack() {
//                            @Override
//                            public void call(String number) {
//                                Settings.setSetting(Settings.SettingEnums.TX_USER_NUMBER.value(), number);
//                            }
//                        }).show();
//                    else
//                        PublicDialogs.sendMessageOrBugDialog(SettingActivity.this, R.drawable.ic_dakhlokharj, EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, userNumber, EnumsAndConstants.SERVER_URL).show();


                if (view.equals(otherAppsLayout)) {
                    switch (EnumsAndConstants.APP_SOURCE) {
                        case "google":
                            Intent intention = new Intent(Intent.ACTION_VIEW);
                            intention.setData(Uri.parse(getResources().getString(R.string.google_play_developer_id_link)));
                            startActivity(intention);
                            break;
                        case "bazaar":
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(getResources().getString(R.string.bazaar_developer_id_link) + EnumsAndConstants.BAZAAR_DEVELOPER_ID));
                            intent.setPackage("com.farsitel.bazaar");
                            startActivity(intent);
                            break;
                        case "myket":
                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
                            intent1.setData(Uri.parse(getResources().getString(R.string.myket_developer_id_link)));
                            intent1.setPackage("ir.mservices.market");
                            startActivity(intent1);
                            break;
                    }

                }

                if (view.equals(backupGoogleDriveLayout)) {
                    Intent intent = new Intent(SettingActivity.this, GoogleDriveBackupActivity.class);
                    startActivity(intent);
                }

                if (view.equals(shareLinkLayout)) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    String shareBody = "";
                    switch (EnumsAndConstants.APP_SOURCE) {
                        case "google":
                            sharingIntent.setType("text/plain");
                            shareBody = "یک اپلیکیشن کاملا مناسب و ساده برای مدیریت هزینه ها و درآمد های شخصی\nلینک دانلود از Google Play:\n" + EnumsAndConstants.SHARE_LINK_GOOGLE + "\n" + "تاکسی اپس تولید کننده ی اپ های موبایلی\uD83D\uDCF1";
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            startActivity(Intent.createChooser(sharingIntent, "Share using"));
                            break;
                        case "bazaar":
                            sharingIntent.setType("text/plain");
                            shareBody = "یک اپلیکیشن کاملا مناسب و ساده برای مدیریت هزینه ها و درآمد های شخصی\nلینک دانلود از بازار:\n" + EnumsAndConstants.SHARE_LINK_BAZAAR + "\n" + "تاکسی اپس تولید کننده ی اپ های موبایلی\uD83D\uDCF1";
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            startActivity(Intent.createChooser(sharingIntent, "Share using"));
                            break;
                        case "myket":
                            sharingIntent.setType("text/plain");
                            shareBody = "یک اپلیکیشن کاملا مناسب و ساده برای مدیریت هزینه ها و درآمد های شخصی\nلینک دانلود از مایکت:\n" + EnumsAndConstants.SHARE_LINK_MYKET + "\n" + "تاکسی اپس تولید کننده ی اپ های موبایلی\uD83D\uDCF1";
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            startActivity(Intent.createChooser(sharingIntent, "Share using"));
                            break;
                    }
                }

                if (view.equals(walkThroughLayout)) {
                    Intent intent = new Intent(SettingActivity.this, WalkThrough.class);
                    startActivity(intent);
                }

                if (view.equals(latelyChangesLayout)){
                    latestChange(SettingActivity.this).show();
                }

                if (view.equals(fingerPrintLayout))
                    if (fingerSwitch.isOn())
                        fingerSwitch.setOn(false, true);
                    else
                        fingerSwitch.setOn(true, true);

                if (view.equals(seeLicenseLayout)) {
                    makeTXLicense(SettingActivity.this).show();
                }
            }
        };

        View.OnTouchListener touchListeners = (view, motionEvent) -> {

            if (view.equals(closePremium)) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    closePremium.setAlpha(0.5f);
                    PublicModules.collapseAnimate(buyCompleteVersionLayout);
                } else {
                    closePremium.setAlpha(1f);
                }
            }
            if (view.equals(knowMoreBtn)) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    knowMoreBtn.setAlpha(0.5f);
                    String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                    String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                    Payment.newInstance(R.drawable.ic_dakhlokharj,
                            EnumsAndConstants.APP_ID,
                            EnumsAndConstants.APP_SOURCE,
                            EnumsAndConstants.APP_VERSION,
                            "دخل و خرج",
                            Application.currentBase64,
                            "مزایای نسخه کامل",
                            limitationText,
                            userNumber,
                            EnumsAndConstants.DEVICE_ID,
                            EnumsAndConstants.SERVER_URL,
                            EnumsAndConstants.GATEWAY_URL,
                            new TX_License.SetLicenseCallback() {
                                @Override
                                public void call(long expireDate, String license) {

                                    Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                    Settings.setSetting(Settings.LICENSE_DATA, license);

                                    try {
                                        JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                        Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                    MainActivity.getInstance().updateFragment(false);
                                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                    if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                        Transaction.appPurchased();
                                    MainActivity.getInstance().reloadMainData();

                                    if (SettingActivity.getInstance() != null)
                                        SettingActivity.getInstance().premiumUIHandling(false);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            },
                            new Payment.UserNameCallBack() {
                                @Override
                                public void getUserName(String username) {
                                    Settings.setSetting(Settings.TX_USER_NUMBER, username);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            }
                    ).show(getFragmentManager(), "tx_payment");
                } else {
                    knowMoreBtn.setAlpha(1f);
                }
            }
            if (view.equals(menuIconContainer)) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    menuIcon.setAlpha(0.5f);
                    finish();
                } else {
                    menuIcon.setAlpha(1f);
                }
            }
            return true;
        };

        closePremium.setOnTouchListener(touchListeners);
        knowMoreBtn.setOnTouchListener(touchListeners);
        menuIconContainer.setOnTouchListener(touchListeners);
        passwordLayout.setOnClickListener(clickListeners);
        rasgirLayout.setOnClickListener(clickListeners);
        yadavarLayout.setOnClickListener(clickListeners);
        wordCutLayout.setOnClickListener(clickListeners);
        currencyLayout.setOnClickListener(clickListeners);
        backupGoogleDriveLayout.setOnClickListener(clickListeners);
//        backupLayout.setOnClickListener(clickListeners);
//        restoreLayout.setOnClickListener(clickListeners);
        removeLastYearDataLayout.setOnClickListener(clickListeners);
        clearDataBaseLayout.setOnClickListener(clickListeners);
        resetSettingLayout.setOnClickListener(clickListeners);
        aboutTaxiAppsLayout.setOnClickListener(clickListeners);
        rateUsLayout.setOnClickListener(clickListeners);
        sendMessageLayout.setOnClickListener(clickListeners);
        otherAppsLayout.setOnClickListener(clickListeners);
        shareLinkLayout.setOnClickListener(clickListeners);
        latelyChangesLayout.setOnClickListener(clickListeners);
        smsLayout.setOnClickListener(clickListeners);
        fingerPrintLayout.setOnClickListener(clickListeners);
        seeLicenseLayout.setOnClickListener(clickListeners);
        walkThroughLayout.setOnClickListener(clickListeners);


        fingerSwitch.setOnSwitchStateChangeListener(isOn -> {
            if (isOn)
                Settings.setSetting(Settings.FINGER_PRINT_IS_ENABLE, "1");
            else
                Settings.setSetting(Settings.FINGER_PRINT_IS_ENABLE, "0");
        });

        smsSwitch.setOnSwitchStateChangeListener(isOn -> {
            if (isOn)
                Settings.setSetting(Settings.SMS_IS_ENABLE, "true");
            else
                Settings.setSetting(Settings.SMS_IS_ENABLE, "false");
        });

    }

    @Override
    void setAmountTypeFaces() {
        //Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        instance = this;

        init();
        setAmountTypeFaces();
        setListeners();

        try {
            versionText.setText("نسخه " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (getIntent().hasExtra("GoToGoogleDriveActivity")) {
            if (getIntent().getBooleanExtra("GoToGoogleDriveActivity", false)) {
                backupGoogleDriveLayout.callOnClick();
            }
        }

        switch (EnumsAndConstants.APP_SOURCE) {
            case "google":
                appSourceText.setText("گوگل");
                break;
            case "bazaar":
                appSourceText.setText("بازار");
                break;
            case "myket":
                appSourceText.setText("مایکت");
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Settings.getSetting(Settings.FINGER_PRINT_IS_ENABLE).equals("1"))
            fingerSwitch.setOn(true);
        else
            fingerSwitch.setOn(false);

        if (Settings.getSetting(Settings.SMS_IS_ENABLE).equals("true"))
            smsSwitch.setOn(true);
        else
            smsSwitch.setOn(false);

        currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));

    }

    private Dialog resetData(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View resetDataLayout = inflater.inflate(R.layout.pop_reset_data, null);

        final TextView removeData, cancel;
        removeData = resetDataLayout.findViewById(R.id.remove_data);
        cancel = resetDataLayout.findViewById(R.id.cancel);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 24;
                break;

            case 2:
                width = 67;
                height = 18;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog resetDataDialog = PublicDialogs.makeDialog(context, resetDataLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel)) {
                resetDataDialog.dismiss();
            }

            if (v.equals(removeData)) {
                context.deleteDatabase("dakhlokharj.db"); // delete database
                DBManager.dataBaseOpenHelper = new DataBaseOpenHelper(context); // init again
                File trnPictures = new File(Environment.getExternalStorageDirectory() + "/Dakhl_o_kharj", "TransactionPicture");
                deleteDirectory(trnPictures); // deleting transaction pictures folder
                MainActivity.getInstance().reloadMainData(); // reload kardane data y main
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged(); // refresh kardane widget hay main
                customToast("خام نمودن اطلاعات برنامه", "همه ی اطلاعات برنامه حذف و \nمجددا بارگذاری شد", true);
                resetDataDialog.dismiss();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        removeData.setOnClickListener(dialogListeners);

        return resetDataDialog;
    }

    private void deleteDirectory(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        files[i].delete();
                    }
                }
            }
            file.delete();
        }
    }

    private Dialog removePastYearsData(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View removePastYearsLayout = inflater.inflate(R.layout.pop_remove_past_years_data, null);

        final TextView removeData, cancel, description;
        removeData = removePastYearsLayout.findViewById(R.id.remove_data);
        cancel = removePastYearsLayout.findViewById(R.id.cancel);
        description = removePastYearsLayout.findViewById(R.id.pop_remove_past_years_data_description);

        description.setText("این عملیات اطلاعات داخل برنامه از سال " + new PersianDate().getShYear() + " به قبل را حذف می کند. آیا از انجام این عملیات اطمینان دارید؟");

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 24;
                break;

            case 2:
                width = 67;
                height = 18;
                break;

            case 3:
                width = 67;
                height = 16;
                break;

        }

        final Dialog rpydDialog = PublicDialogs.makeDialog(context, removePastYearsLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel))
                rpydDialog.dismiss();

            if (v.equals(removeData)) {
                DBManager.deletePastYearsData();
                customToast("حذف اطلاعات سال گذشته", "اطلاعات سال گذشته حذف شد", true);
                rpydDialog.dismiss();
            }

        };

        cancel.setOnClickListener(dialogListeners);
        removeData.setOnClickListener(dialogListeners);

        return rpydDialog;
    }

    private Dialog resetSettings(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View resetSettingsLayout = inflater.inflate(R.layout.pop_reset_settings, null);

        final TextView resetData, cancel;
        resetData = resetSettingsLayout.findViewById(R.id.reset_data);
        cancel = resetSettingsLayout.findViewById(R.id.cancel);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 31;
                break;

            case 2:
                width = 70;
                height = 25;
                break;

            case 3:
                width = 67;
                height = 21;
                break;

        }

        final Dialog resetSettingsDialog = PublicDialogs.makeDialog(context, resetSettingsLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel))
                resetSettingsDialog.dismiss();

            if (v.equals(resetData)) {

                String license = Settings.getSetting(Settings.LICENSE_DATA);
                String expireDate = Settings.getSetting(Settings.LICENSE_EXPIRE);
                String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER);
                String appAmount = Settings.getSetting(Settings.APP_AMOUNT);
                String payeeOrders = Settings.getSetting(Settings.PAYEE_ORDER_MAP);
                String accountOrders = Settings.getSetting(Settings.ACCOUNT_ORDER_MAP);

                context.getSharedPreferences(Application.AppEnums.SHARED_PREFRENCES.value(), MODE_PRIVATE).edit().clear().apply(); // delete app settings
                GoogleSignIn.getClient(context, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Drive.SCOPE_APPFOLDER)
                        .build()).signOut(); // sign out from Google account

                Settings.init(context); // init again
                Settings.setSetting(Settings.LICENSE_DATA, license);
                Settings.setSetting(Settings.LICENSE_EXPIRE, expireDate);
                Settings.setSetting(Settings.TX_USER_NUMBER, userNumber);
                Settings.setSetting(Settings.APP_AMOUNT, appAmount);
                Settings.setSetting(Settings.PAYEE_ORDER_MAP, payeeOrders);
                Settings.setSetting(Settings.ACCOUNT_ORDER_MAP, accountOrders);


                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                smsSwitch.setOn(false, true);
                fingerPrintLayout.setVisibility(View.GONE);

                customToast("ریست تنظیمات", "تنظیمات برنامه ریست شد", true);
                resetSettingsDialog.dismiss();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        resetData.setOnClickListener(dialogListeners);

        return resetSettingsDialog;
    }

    public static Dialog latestChange(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View latestChangeLayout = inflater.inflate(R.layout.pop_latest_change, null);

        int width = 0, height = 0;

        TextView close = latestChangeLayout.findViewById(R.id.pop_latest_change_close);
        TextView version = latestChangeLayout.findViewById(R.id.pop_latest_change_version_text_view);
        try {
            version.setText("بروز رسانی به نسخه " + context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 70;
                break;

            case 2:
                width = 67;
                height = 50;
                break;

            case 3:
                width = 50;
                height = 43;
                break;

        }

        final Dialog latestChangeDialog = PublicDialogs.makeDialog(context, latestChangeLayout, width, height, false);

        close.setOnClickListener(v -> latestChangeDialog.dismiss());

        return latestChangeDialog;
    }

    private Dialog currencyDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View currencyDialogLayout = inflater.inflate(R.layout.pop_currency, null);

        final TextView Rial, Toman, AED, USD, EUR, TRY, CNY, cancel;

        Rial = currencyDialogLayout.findViewById(R.id.pop_currency_rial);
        Toman = currencyDialogLayout.findViewById(R.id.pop_currency_toman);
        AED = currencyDialogLayout.findViewById(R.id.pop_currency_aed);
        USD = currencyDialogLayout.findViewById(R.id.pop_currency_usd);
        EUR = currencyDialogLayout.findViewById(R.id.pop_currency_eur);
        TRY = currencyDialogLayout.findViewById(R.id.pop_currency_try);
        CNY = currencyDialogLayout.findViewById(R.id.pop_currency_cny);
        cancel = currencyDialogLayout.findViewById(R.id.pop_currency_cancel);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 100;
                height = 64;
                break;

            case 2:
                width = 100;
                height = 55;
                break;

            case 3:
                width = 100;
                height = 47;
                break;

        }

        final Dialog currencyDialog = PublicDialogs.makeDialog(context, currencyDialogLayout, width, height, false);
        currencyDialog.getWindow().setGravity(Gravity.BOTTOM);

        currencyDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        View.OnClickListener listener = v -> {

            if (v.equals(Rial)) {
                Settings.setSetting(Settings.CASH_TYPE_INDEX, "0");
                Settings.setSetting(Settings.AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Arabic.value());
                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                MainActivity.getInstance().refreshMainTypeFace();
                currencyDialog.dismiss();
            }
            if (v.equals(Toman)) {
                Settings.setSetting(Settings.CASH_TYPE_INDEX, "1");
                Settings.setSetting(Settings.AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Arabic.value());
                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                MainActivity.getInstance().refreshMainTypeFace();
                currencyDialog.dismiss();
            }
            if (v.equals(AED)) {
                Settings.setSetting(Settings.CASH_TYPE_INDEX, "2");
                Settings.setSetting(Settings.AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Kharejik.value());
                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                MainActivity.getInstance().refreshMainTypeFace();
                currencyDialog.dismiss();
            }
            if (v.equals(USD)) {
                Settings.setSetting(Settings.CASH_TYPE_INDEX, "3");
                Settings.setSetting(Settings.AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Kharejik.value());
                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                MainActivity.getInstance().refreshMainTypeFace();
                currencyDialog.dismiss();
            }
            if (v.equals(EUR)) {
                Settings.setSetting(Settings.CASH_TYPE_INDEX, "4");
                Settings.setSetting(Settings.AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Kharejik.value());
                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                MainActivity.getInstance().refreshMainTypeFace();
                currencyDialog.dismiss();
            }
            if (v.equals(TRY)) {
                Settings.setSetting(Settings.CASH_TYPE_INDEX, "5");
                Settings.setSetting(Settings.AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Kharejik.value());
                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                MainActivity.getInstance().refreshMainTypeFace();
                currencyDialog.dismiss();
            }
            if (v.equals(CNY)) {
                Settings.setSetting(Settings.CASH_TYPE_INDEX, "6");
                Settings.setSetting(Settings.AMOUNT_TYPE_FACE, EnumsAndConstants.AmountTypeFace.Kharejik.value());
                currencyTextView.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));
                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                MainActivity.getInstance().refreshMainTypeFace();
                currencyDialog.dismiss();
            }
            if (v.equals(cancel))
                currencyDialog.dismiss();

        };

        Rial.setOnClickListener(listener);
        Toman.setOnClickListener(listener);
        AED.setOnClickListener(listener);
        USD.setOnClickListener(listener);
        EUR.setOnClickListener(listener);
        TRY.setOnClickListener(listener);
        CNY.setOnClickListener(listener);
        cancel.setOnClickListener(listener);

        return currencyDialog;
    }

    private Dialog passwordDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View passwordLayout = inflater.inflate(R.layout.pop_edit_or_delete_password, null);

        final ConstraintLayout deletePassword, resetPassword, passwordCancel;
        deletePassword = passwordLayout.findViewById(R.id.pop_edit_or_delete_password_delete_password_layout);
        resetPassword = passwordLayout.findViewById(R.id.pop_edit_or_delete_password_reset_password_layout);
        passwordCancel = passwordLayout.findViewById(R.id.pop_edit_or_delete_password_cancel_layout);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 80;
                height = 36;
                break;

            case 2:
                width = 77;
                height = 29;
                break;

            case 3:
                width = 57;
                height = 24;
                break;

        }

        final Dialog faDialog = PublicDialogs.makeDialog(context, passwordLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(deletePassword)) {
                Settings.setSetting(Settings.PASSWORD, "");
                customToast("حذف رمز", "رمز حذف شد", true);
                SettingActivity.checkFingerPrintAvailability();
                faDialog.dismiss();
            }
            if (v.equals(resetPassword)) {
                Intent intent = new Intent(SettingActivity.this, PasswordActivity.class);
                intent.putExtra("PassEnteringType", PasswordActivity.PassEnteringType.UserWantToSet.value());
                startActivityForResult(intent, 20);
                overridePendingTransition(R.anim.start_slide_up, R.anim.end_slide_up);
                faDialog.dismiss();
            }
            if (v.equals(passwordCancel)) {
                faDialog.dismiss();
            }
        };

        deletePassword.setOnClickListener(dialogListeners);
        resetPassword.setOnClickListener(dialogListeners);
        passwordCancel.setOnClickListener(dialogListeners);

        return faDialog;
    }

    private Dialog selectingBackUp(Context context, ArrayList<File> files) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.pop_backup_select, null);

        RecyclerView recyclerView = layout.findViewById(R.id.pop_backup_select_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        Button cancel = layout.findViewById(R.id.pop_backup_select_cancel);
        TextView filesCount = layout.findViewById(R.id.pop_backup_select_files_count);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 90;
                height = 70;
                break;

            case 2:
                width = 70;
                height = 60;
                break;

            case 3:
                width = 60;
                height = 55;
                break;

        }

        final Dialog backUpDialog = PublicDialogs.makeDialog(context, layout, width, height, true);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backUpDialog.dismiss();
            }
        });

        filesCount.setText(String.valueOf(files.size()) + " فایل");
        recyclerView.setAdapter(new BackUpFilesAdapter(context, files, new Backup.SelectBackUpCallBack() {
            @Override
            public void call(File file) {
                boolean recoveryDone = Backup.recoveryDataFromStorage(file);
                if (recoveryDone) {
                    customToast("بازیابی اطلاعات", "اطلاعات با موفقیت بازیابی شد", true);
                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();
                } else
                    customToast("بازیابی اطلاعات", "بازیابی اطلاعات ناموفق بود", false);

                backUpDialog.dismiss();
            }
        }));

        return backUpDialog;
    }

    public void premiumUIHandling(boolean isLimit) {

        if (!isLimit) {
            if (!Settings.getSetting(Settings.PURCHASE_STATUS).equals("0")) {
                if (Application.getLicense() != null)
                    seeLicenseLayout.setVisibility(View.VISIBLE);
                else
                    seeLicenseLayout.setVisibility(View.GONE);
            } else {
                seeLicenseLayout.setVisibility(View.GONE);
            }

            buyCompleteVersionLayout.setVisibility(View.GONE);
//            backupLayout.setVisibility(View.VISIBLE);
//            restoreLayout.setVisibility(View.VISIBLE);
        } else {
            buyCompleteVersionLayout.setVisibility(View.VISIBLE);
            seeLicenseLayout.setVisibility(View.GONE);
//            backupLayout.setVisibility(View.GONE);
//            restoreLayout.setVisibility(View.GONE);
        }

    }

    private void customToast(String title, String description, boolean isSuccess) {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom,
                findViewById(R.id.toast_layout_root));

        ImageView image = layout.findViewById(R.id.toast_img);
        if (isSuccess)
            image.setImageResource(R.drawable.ic_success);
        else
            image.setImageResource(R.drawable.ic_unsuccess);
        TextView titleText = layout.findViewById(R.id.toast_title);
        titleText.setText(title);
        TextView descriptionText = layout.findViewById(R.id.toast_description);
        descriptionText.setText(description);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 25);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    private Dialog alwaysDenyDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View popAllwaysDeny = inflater.inflate(R.layout.pop_allways_deny, null);

        final TextView goToSetting, cancel, title, desc;
        goToSetting = popAllwaysDeny.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = popAllwaysDeny.findViewById(R.id.pop_allways_deny_cancel);
        title = popAllwaysDeny.findViewById(R.id.pop_always_deny_title);
        desc = popAllwaysDeny.findViewById(R.id.pop_always_deny_description);

        title.setText("پشتیبانگیری");
        desc.setText("با فعال کردن گزینه Storage از منوی Permissions مربوط به برنامه دخل و خرج امکان پشتیبانگیری را فعال کنید.");

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 72;
                height = 19;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog rpydDialog = PublicDialogs.makeDialog(context, popAllwaysDeny, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel))
                rpydDialog.dismiss();

            if (v.equals(goToSetting)) {
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);

                rpydDialog.dismiss();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        goToSetting.setOnClickListener(dialogListeners);

        return rpydDialog;
    }

    private Dialog makeTXLicense(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_tx_payment_license, null);
        final Dialog dialog = PublicDialogs.makeDialog(context, view, Application.WIDTH, Application.HEIGHT, true);
        //
        TextView status = view.findViewById(R.id.frg_tx_payment_license_status);
        TextView date = view.findViewById(R.id.frg_tx_payment_license_date);
        TextView paidPrice = view.findViewById(R.id.frg_tx_payment_license_paid_price);
        TextView buyNumber = view.findViewById(R.id.frg_tx_payment_license_buy_number);
        TextView limitation = view.findViewById(R.id.frg_tx_payment_license_limitation);
        TextView username = view.findViewById(R.id.frg_tx_payment_license_username);
        final TextView exit = view.findViewById(R.id.frg_tx_payment_license_exit);
        //
        String license = Settings.getSetting(Settings.LICENSE_DATA);
        if (!license.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                status.setText("خرید موفق لایسنس راس گیر چک");
                date.setText(jsonObject.getString("date_fa"));
                String price = PublicModules.toEnglishDigit(jsonObject.getString("amount")).replaceAll("\\D", "");
                if (!price.equals("")) {
                    price = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(price) / 10); // separator
                    paidPrice.setText(price.concat(" تومان"));
                }
                buyNumber.setText(jsonObject.getString("pcode"));
                limitation.setText(jsonObject.getString("description"));
                if (jsonObject.getString("username") != null && !jsonObject.getString("username").equals(""))
                    username.setText(jsonObject.getString("username"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //on click listeners
        View.OnClickListener listener = v -> {
            if (v.equals(exit)) {
                exit.setTextColor(R.color.blue);
                dialog.dismiss();
            }
        };
        //set onclick listeners
        exit.setOnClickListener(listener);
        //
        return dialog;
    }

    private void restoreBackup() {
        ArrayList<File> files = new ArrayList<>();
        File parentFiles = new File(Settings.getSetting(Settings.BACKUP_DESTINATION)); // file hay backup ro mikhunim az BackUp Destination
        if (parentFiles.exists())
            if (parentFiles.listFiles() != null)
                files = new ArrayList<>(Arrays.asList(parentFiles.listFiles()));

        for (int i = 0; i < files.size(); i++) {
            if (files.get(i).getName().equals("Temp")) // bara inke file temp ro tu list neshun nade
                files.get(i).delete();
        }

        if (files.size() > 0)  // check mikonim ghablan backup gerefte ya na !
            selectingBackUp(SettingActivity.this, files).show();
        else
            PublicDialogs.makeMessageDialog(SettingActivity.this, PublicDialogs.MessageDialogTypes.NORMAL, "فایلی یافت نشد!", "شما ابتدا باید از اطلاعات خود پشتیبان گیری کنید.", "", EnumsAndConstants.APP_SOURCE).show();
    }

}