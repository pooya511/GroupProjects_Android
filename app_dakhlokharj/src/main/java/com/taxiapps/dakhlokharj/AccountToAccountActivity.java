package com.taxiapps.dakhlokharj;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcel;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dialog_fragments.DatePicker;
import model.Account;
import model.Currency;
import model.EnumsAndConstants;
import model.Transaction;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.DialogUtils;

public class AccountToAccountActivity extends BaseActivity {

    private TextView fromAccountTextView, toAccountTextView, priceTextView, fromAccountBalanceTextView, toAccountBalanceTextView, transfer, balanceWarning;
    private TextView transferDateTextView, transferTimeTextView;
    private EditText trnDescription;
    private Dialog accountDialog, keyPadDialog;

    private Account fromAccount;
    private Account toAccount;
    private double calculatedBalance;

    @Override
    Context context() {
        return this;
    }

    @Override
    void init() {
        fromAccountTextView = findViewById(R.id.act_account_to_account_from_account_dynamic_text_view);
        toAccountTextView = findViewById(R.id.act_account_to_account_to_account_dynamic_text_view);
        priceTextView = findViewById(R.id.act_account_to_account_price_dynamic_text_view);
        fromAccountBalanceTextView = findViewById(R.id.act_account_to_account_from_account_balance);
        toAccountBalanceTextView = findViewById(R.id.act_account_to_account_to_account_balance);
        trnDescription = findViewById(R.id.act_account_to_account_description_edit_text);
        transfer = findViewById(R.id.act_account_to_account_transfer_text_view);
        balanceWarning = findViewById(R.id.act_account_to_account_price_warning);
        transferDateTextView = findViewById(R.id.act_account_to_account_date_input_text_view);
        transferTimeTextView = findViewById(R.id.act_account_to_account_time_input_text_view);

        PersianDate pc = new PersianDate();
        PersianDateFormat formatter = new PersianDateFormat();
        transferDateTextView.setText(formatter.format(pc, "Y/m/d"));
        transferTimeTextView.setText(formatter.format(pc, "H:i"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AccountListActivity.REQUEST_CODE_ADD_ACCOUNT && resultCode == RESULT_OK) {
            toAccountTextView.callOnClick();
        }
    }

    @Override
    void setListeners() {
        View.OnClickListener listener = v -> {

            if (v.equals(toAccountTextView)) {
                ArrayList<Integer> exclusionIDs = new ArrayList<>();
                exclusionIDs.add(fromAccount.getID());
                ArrayList<Account> activeAccounts = Account.getAllActive();
                accountDialog = Account.accountList(this, activeAccounts, exclusionIDs, account -> {

                    toAccount = account;
                    toAccountTextView.setText(account.getAcnName());

                    checkDuplicateAccount();
                    checkAccountBalance(toAccount, toAccountBalanceTextView);

                    accountDialog.dismiss();
                });
                if (!accountDialog.isShowing()) {
                    accountDialog.show();
                }
            }

            if (v.equals(priceTextView)) {
                keyPadDialog = DialogUtils.keypadDialog(this, priceTextView.getText().toString().replaceAll("\\D", ""), number -> {
                    priceTextView.setText(Currency.CurrencyString.getCurrencyString(number, Currency.CurrencyMode.Short).separated);

                    if (checkPriceAvailability(Double.valueOf(PublicModules.toEnglishDigit(number.replaceAll("\\D", ""))))) {
                        balanceWarning.setVisibility(View.INVISIBLE);
                    } else {
                        balanceWarning.setVisibility(View.VISIBLE);
                    }

                });
                if (!keyPadDialog.isShowing()) {
                    keyPadDialog.show();
                }
            }

            if (v.equals(transfer)) {
                if (toAccount == null) {
                    errorAnimation(toAccountTextView);
                } else if (Double.valueOf(PublicModules.toEnglishDigit(priceTextView.getText().toString().replaceAll("\\D", ""))) == 0) {
                    errorAnimation(priceTextView);
                } else {
                    transfer();
                }
            }

            if (v.equals(transferDateTextView)) {
                DatePicker.newInstance(new Date().getTime(), new DatePicker.DatePickerCallBack() {
                    @Override
                    public void call(PersianDate persianCalendar) {
                        transferDateTextView.setText(PersianDateFormat.format(persianCalendar, "Y/m/d"));
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {

                    }
                }).show(getFragmentManager(), "");
            }

            if (v.equals(transferTimeTextView)) {
                DialogUtils.timePickerDialog(this, transferTimeTextView.getText().toString(), time ->
                        transferTimeTextView.setText(time)
                ).show();
            }
        };

        toAccountTextView.setOnClickListener(listener);
        priceTextView.setOnClickListener(listener);
        transfer.setOnClickListener(listener);
        transferDateTextView.setOnClickListener(listener);
        transferTimeTextView.setOnClickListener(listener);
    }

    @Override
    void setAmountTypeFaces() {
        fromAccountBalanceTextView.setTypeface(Application.amountTypeFace(AccountToAccountActivity.this));
        toAccountBalanceTextView.setTypeface(Application.amountTypeFace(AccountToAccountActivity.this));
        priceTextView.setTypeface(Application.amountTypeFace(AccountToAccountActivity.this));
    }

    private void errorAnimation(TextView targetTextView) {

        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                targetTextView.setTextColor(Color.RED);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                targetTextView.setTextColor(AccountToAccountActivity.this.getResources().getColor(R.color.passwordBtnTextColor));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        targetTextView.startAnimation(shake);
    }

    private void transfer() {
        String price = PublicModules.toEnglishDigit(priceTextView.getText().toString());
        String description = PublicModules.toEnglishDigit(trnDescription.getText().toString());

        PersianDate date = new PersianDate(Long.valueOf(PublicModules.string2TimeStamp(transferDateTextView.getText().toString() + " " + transferTimeTextView.getText().toString())));
        String transferDate = String.valueOf(date.getTime());

        Transaction withDraw = new Transaction(0, 0, Double.valueOf(price.replaceAll("\\D", "")), transferDate, 0, fromAccount.getID(),
                description, "", "", EnumsAndConstants.TransactionTypes.WithDraw.value());
        int withDrawReference = withDraw.insert();

        Transaction deposit = new Transaction(0, 0, Double.valueOf(price.replaceAll("\\D", "")), transferDate, 0, toAccount.getID(),
                description, "", "", EnumsAndConstants.TransactionTypes.Deposit.value());
        int depositReference = deposit.insert();

        withDraw.setTrnReference(depositReference);
        deposit.setTrnReference(withDrawReference);

        String[] columns = new String[]{"trnReference"};
        withDraw.update(columns);
        deposit.update(columns);

        PublicModules.customToast(this, "موفق", "انتقال انجام شد", true);
        setResult(RESULT_OK);
        finish();
    }

    private void checkDuplicateAccount() {
        if (fromAccount != null && toAccount != null) {
            if (fromAccount.getID() == toAccount.getID()) {
                toAccount = null;
                toAccountTextView.setText("انتخاب کنید");
                toAccountBalanceTextView.setVisibility(View.INVISIBLE);
            }
        }
    }

    private double checkAccountBalance(Account account, TextView targetTextView) {

        targetTextView.setVisibility(View.VISIBLE);
        double itemAccountBalance = Double.parseDouble(account.getAcnBalance());
        ArrayList<Transaction> transactions = Account.getAllTransaction(account.getID());

        if (transactions.size() == 0) {
            if (itemAccountBalance > 0) {
                targetTextView.setText(Currency.CurrencyString.getCurrencyString(itemAccountBalance, Currency.CurrencyMode.Short).separated);
                targetTextView.setTextColor(Color.parseColor("#71D009"));
            } else if (itemAccountBalance == 0) {
                targetTextView.setText(Currency.CurrencyString.getCurrencyString(itemAccountBalance, Currency.CurrencyMode.Short).separated);
                targetTextView.setTextColor(Color.parseColor("#000000"));
            }
        } else {
            for (int i = 0; i < transactions.size(); i++) {
                double trnAmount = Double.parseDouble(transactions.get(i).getTrnAmount());
                if (transactions.get(i).getTrnType() == 1 || transactions.get(i).getTrnType() == 3) {
                    itemAccountBalance = itemAccountBalance - trnAmount;
                } else {
                    itemAccountBalance = itemAccountBalance + trnAmount;
                }
            }

            if (itemAccountBalance > 0) {
                targetTextView.setText(Currency.CurrencyString.getCurrencyString(itemAccountBalance, Currency.CurrencyMode.Short).separated);
                targetTextView.setTextColor(Color.parseColor("#71D009"));
            } else if (itemAccountBalance < 0) {
                targetTextView.setText("منفی ".concat(Currency.CurrencyString.getCurrencyString(Math.abs(itemAccountBalance), Currency.CurrencyMode.Short).separated));
                targetTextView.setTextColor(Color.parseColor("#FF0000"));
            } else if (itemAccountBalance == 0) {
                targetTextView.setText(Currency.CurrencyString.getCurrencyString("0", Currency.CurrencyMode.Short).separated);
                targetTextView.setTextColor(Color.parseColor("#000000"));
            }
        }

        return itemAccountBalance;
    }

    private boolean checkPriceAvailability(double price) {
        return calculatedBalance >= price;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_to_account);

        init();
        setListeners();
        setAmountTypeFaces();

        if (getIntent().hasExtra("accountID")) {
            fromAccount = Account.getAccount(getIntent().getIntExtra("accountID", 0));
            fromAccountTextView.setText(fromAccount.getAcnName());
            calculatedBalance = checkAccountBalance(fromAccount, fromAccountBalanceTextView);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fromAccount = null;
    }
}
