package com.taxiapps.dakhlokharj;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.woxthebox.draglistview.DragListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import adapters.AccountListAdapter;
import model.Account;
import model.Settings;
import modules.PublicModules;
import utils.AppModules;

public class AccountListActivity extends BaseActivity {

    private ImageView addAccount;
    private DragListView accountList;
    private TextView textView1;
    private ConstraintLayout didntAddAccountLayout;

    public static Account targetAccount;

    private int lastPosition;
    public final static int REQUEST_CODE_ADD_ACCOUNT = 20;
    public final static int REQUEST_TRANSFER_ACCOUNT = 789047324;

    private ArrayList<Account> accounts;
    private AccountListAdapter accountListAdapter;

    private static AccountListActivity instance;

    public static AccountListActivity getInstance() {
        return instance;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD_ACCOUNT && resultCode == RESULT_OK) {
            if (accountListAdapter != null) {
                accountListAdapter.refreshData();
                targetAccount = null;
            }
            checkListContent();
        }
        if (resultCode == RESULT_CANCELED) {
            targetAccount = null;
        }

        if (requestCode == REQUEST_TRANSFER_ACCOUNT && resultCode == RESULT_OK) {
            if (accounts.size() > 0) {
                accountListAdapter.refreshData();
            }
        }
    }

    @Override
    Context context() {
        return AccountListActivity.this;
    }

    @Override
    void init() {
        addAccount = findViewById(R.id.account_list_new_account_image);
        accountList = findViewById(R.id.account_list_activity_list_view);
        textView1 = findViewById(R.id.account_list_activity_textview1);
        didntAddAccountLayout = findViewById(R.id.account_list_activity_didnt_add_account_layout);
    }

    @Override
    void setListeners() {
        View.OnClickListener listeners = v -> {
            if (v.equals(addAccount)) {
                Intent intent = new Intent(AccountListActivity.this, NewAndEditAccountActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ADD_ACCOUNT);
            }
        };
        addAccount.setOnClickListener(listeners);
    }

    @Override
    void setAmountTypeFaces() {
        // Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_list);
        instance = this;

        lastPosition = -1;
        init();
        setAmountTypeFaces();
        setListeners();

    }

    public void checkListContent() {
        accounts = Account.getAll();
        if (accounts.size() == 0) {
            didntAddAccountLayout.setVisibility(View.VISIBLE);
            textView1.setVisibility(View.GONE);
            accountList.setVisibility(View.GONE);
        } else {
            didntAddAccountLayout.setVisibility(View.GONE);
            textView1.setVisibility(View.VISIBLE);
            accountList.setVisibility(View.VISIBLE);
            initAdapter();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkListContent();
    }

    private void initAdapter() {
        ArrayList<Account> allAccounts = Account.getAll();
        ArrayList<Account> accounts = Account.sortAccountsOrder(allAccounts, Settings.loadMap(Settings.ACCOUNT_ORDER_MAP));
        accountListAdapter = new AccountListAdapter(AccountListActivity.this, accounts);
        accountList.setLayoutManager(new LinearLayoutManager(AccountListActivity.this));
        accountList.setAdapter(accountListAdapter, true);
        accountList.setDragListListener(new DragListView.DragListListener() {
            @Override
            public void onItemDragStarted(int position) {
            }

            @Override
            public void onItemDragging(int itemPosition, float x, float y) {
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition) {
                    if (accountListAdapter.accounts.get(toPosition).isExpanded) {
                        accountListAdapter.lastPosition = toPosition;
                        accountListAdapter.notifyItemChanged(fromPosition);
                        accountListAdapter.notifyItemChanged(toPosition);
                    }
                    PublicModules.customToast(AccountListActivity.this, "لیست حساب ها", "ترتیب نمایش بروزرسانی شد", true);
                }
            }
        });
        accountList.setCanDragHorizontally(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (accounts.size() > 0) {
            ArrayList<Account> newOrderedAccounts = listToArrayList(accountListAdapter.getItemList());
            Account.updateAccountOrderHashMap(newOrderedAccounts);
        }
    }

    //    @Override
    //    protected void onResume() {
    //        super.onResume();
    //        if (accounts.size() > 0) {
    ////            accounts = Account.getAll();
    //            accountListAdapter.notifyDataSetChanged();
    //        }
    //    }

    private ArrayList<Account> listToArrayList(List<Account> accountsList) {
        ArrayList<Account> res = new ArrayList<>();
        for (int i = 0; i < accountsList.size(); i++) {
            res.add(accountsList.get(i));
        }
        return res;
    }
}
