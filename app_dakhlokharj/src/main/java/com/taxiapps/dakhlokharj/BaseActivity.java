package com.taxiapps.dakhlokharj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.concurrent.ExecutionException;

import model.EnumsAndConstants;
import model.Settings;
import modules.PublicDialogs;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import utils.DialogUtils;
import utils.ForegroundCheckTask;

/**
 * Created by Parsa on 9/23/2017.
 */

public abstract class BaseActivity extends Activity {

    abstract Context context();

    abstract void init();

    abstract void setListeners();

    abstract void setAmountTypeFaces();

    public boolean startFromPassActivity;
    private static BaseActivity instance;

    public static BaseActivity getInstance() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        Application.localization(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // va az khode activity password nayomade bashe.
        // va timer ham be 10 sanie nareside bashe.
        if (context().getClass().getName().equals("PasswordActivity") || !getInstance().startFromPassActivity) {
            getInstance().startFromPassActivity = false;
        } else {
            getInstance().startFromPassActivity = true;
        }

        if (getInstance().startFromPassActivity) {
            getInstance().startFromPassActivity = false;
            if (!Settings.getSetting(Settings.PASSWORD).equals("")) {
                Intent intent = new Intent(this, PasswordActivity.class);
                intent.putExtra("PassEnteringType", PasswordActivity.PassEnteringType.UserWantToEnter.value());
                intent.putExtra("onBackDisable", true); // bayad onBackPressed() tu passwordActivity disable bashe
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() != null) {
            MainActivity.getInstance().appPauseTimer.cancel();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (!BaseActivity.appIsForeground(this))
                if (MainActivity.getInstance() != null)
                    MainActivity.getInstance().appPauseTimer.start();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static boolean appIsForeground(Context context) throws ExecutionException, InterruptedException {
        return new ForegroundCheckTask().execute(context).get();
    }

    public static void showPushPopup(final String title, final String content, final String link) {
        instance.runOnUiThread(() -> PublicDialogs.makeMessageDialog(getInstance(), PublicDialogs.MessageDialogTypes.NORMAL, title, content, link, EnumsAndConstants.APP_SOURCE).show());
    }

}
