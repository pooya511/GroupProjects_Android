package com.taxiapps.dakhlokharj;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcel;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.github.mikephil.charting.data.Entry;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.fragment.Limits;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import adapters.ExpensesAndIncomesListAdapter;
import adapters.PageAdapter;
import fragments.Chart;
import fragments.GaugeLevel;
import fragments.MostUsedItem;
import fragments.SelectByDayFragment;
import fragments.SelectBySelectingFragment;
import model.Backup;
import model.Budget;
import model.ChartConfig;
import model.Currency;
import model.EnumsAndConstants;
import model.Reports;
import model.SMS;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import pub.devrel.easypermissions.EasyPermissions;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import utils.ForegroundCheckTask;

import static TxAnalytics.TxAnalytics.AppUse;
import static TxAnalytics.TxAnalytics.PurchaseIsActive;
import static TxAnalytics.TxAnalytics.VersionCheck;

public class MainActivity extends FragmentActivity {

    private TextView gregDateTextView, shamsiDateTextView, totalExpensesTextView, totalIncomesTextView, easyAccessTextView;
    private TextView totalExpensesBudgetTextView;
    private ImageView easyAccessSettingIcon, logo, search, backBtn, nextBtn, cloudErrorImage;
    private ConstraintLayout expensesLayout, incomesLayout, expensesBudgetLayout, myAccountsLayout, payeesLayout, reportsLayout, easyAccesslayout, settingIcon;
    public ViewPager footerViewPager;
    public PageAdapter pageAdapter;
    public ImageView buyBtn;
    private Dialog smsDialog;

    private Chart chart;
    public CountDownTimer appPauseTimer = new CountDownTimer(30000, 100000) {
        @Override
        public void onTick(long l) {

        }

        @Override
        public void onFinish() {
            if (BaseActivity.getInstance() != null)
                BaseActivity.getInstance().startFromPassActivity = true;
        }
    };

    private final int SMS_PERMISSION = 1;
    private final int WRITE_PERMISSION_FOR_SMS = 2;

    private static MainActivity instance;

    public static MainActivity getInstance() {
        return instance;
    }

    private interface easyAccessCallBack {
        void call(String string);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Limits.getInstance() != null) {
            if (Limits.getInstance().mHelper != null) Limits.getInstance().mHelper.dispose();
            Limits.getInstance().mHelper = null;
        }
        //  Log.d(childClassName,"destroy");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!Limits.getInstance().mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d("sabih", "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void init() {
        Application.localization(this);
        gregDateTextView = findViewById(R.id.main_activity_greg_date_textview);
        shamsiDateTextView = findViewById(R.id.main_activity_shamsi_date_textview);
        easyAccessTextView = findViewById(R.id.easy_access_textview);
        totalExpensesTextView = findViewById(R.id.main_activity_total_expenses_textview);
        totalIncomesTextView = findViewById(R.id.main_activity_total_incems_textview);
        totalExpensesBudgetTextView = findViewById(R.id.main_activity_total_expenses_budeget_textview);
        settingIcon = findViewById(R.id.main_activity_ic_setting);
        buyBtn = findViewById(R.id.main_activity_buy_btn);
        easyAccessSettingIcon = findViewById(R.id.activity_main_ic_easy_access_setting);
        logo = findViewById(R.id.main_activity_logo);
        search = findViewById(R.id.main_activity_serach);
        expensesLayout = findViewById(R.id.main_activity_expenses_layout);
        incomesLayout = findViewById(R.id.main_activity_incomes_layout);
        expensesBudgetLayout = findViewById(R.id.main_activity_expenses_budget_layout);
        myAccountsLayout = findViewById(R.id.main_activity_my_accounts_layout);
        payeesLayout = findViewById(R.id.main_activity_payees_layout);
        reportsLayout = findViewById(R.id.main_activity_reports_layout);
        easyAccesslayout = findViewById(R.id.main_activity_easy_access_container);
        footerViewPager = findViewById(R.id.activity_main_view_pager);
        backBtn = findViewById(R.id.activity_main_back_btn);
        nextBtn = findViewById(R.id.activity_main_next_btn);
        cloudErrorImage = findViewById(R.id.act_main_cloud_error);

        if (Application.getLicense() == null)
            buyBtn.setVisibility(View.VISIBLE);
        else
            buyBtn.setVisibility(View.GONE);

        if (Settings.getSetting(Settings.EASY_ACCESS_TYPE).equals("1"))
            easyAccessTextView.setText("هزینه جدید");
        else
            easyAccessTextView.setText("درآمد جدید");

        gregDateTextView.setTypeface(Typeface.SANS_SERIF);
        gregDateTextView.setText(String.valueOf(new SimpleDateFormat("yyyy MMMM dd").format(new Date())));
        shamsiDateTextView.setText(PublicModules.timeStamp2String(new PersianDate().getTime(), "yyyy MM dd"));
        long[] currentMonth = PublicModules.getMonthInterval(0);
        String totalExpenseStr = Transaction.getSumAll(String.valueOf(currentMonth[0]), String.valueOf(currentMonth[1]), EnumsAndConstants.TransactionTypes.Expense);
        String totalIncomeStr = Transaction.getSumAll(String.valueOf(currentMonth[0]), String.valueOf(currentMonth[1]), EnumsAndConstants.TransactionTypes.Income);
        totalExpensesTextView.setText(Currency.CurrencyString.getCurrencyString(PublicModules.toEnglishDigit(totalExpenseStr).replaceAll(",", "").replaceAll("\\.", ""), Currency.CurrencyMode.Full).separated);
        totalIncomesTextView.setText(Currency.CurrencyString.getCurrencyString(PublicModules.toEnglishDigit(totalIncomeStr).replaceAll(",", "").replaceAll("\\.", ""), Currency.CurrencyMode.Full).separated);

        footerViewPager.setCurrentItem(Integer.parseInt(Settings.getSetting(Settings.LAST_WIDGET)));

    }

    private void setListeners() {

        View.OnClickListener clickListeners = view -> {

            if (view.equals(expensesLayout)) {
                Intent intent = new Intent(MainActivity.this, ExpensesOrIncomeListActivity.class);
                intent.putExtra("Type", "Expense");
                startActivity(intent);
            }
            if (view.equals(cloudErrorImage)) {
                googleBackupPopup().show();
            }
            if (view.equals(incomesLayout)) {
                Intent intent = new Intent(MainActivity.this, ExpensesOrIncomeListActivity.class);
                intent.putExtra("Type", "Income");
                startActivity(intent);
            }
            if (view.equals(expensesBudgetLayout)) {
                if (Application.isLicenseValid()) {
                    Intent intent = new Intent(MainActivity.this, ExpensesBudgetActivity.class);
                    startActivity(intent);
                } else {
                    String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                    String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                    Payment.newInstance(R.drawable.ic_dakhlokharj,
                            EnumsAndConstants.APP_ID,
                            EnumsAndConstants.APP_SOURCE,
                            EnumsAndConstants.APP_VERSION,
                            "دخل و خرج",
                            Application.currentBase64,
                            "بودجه ماهانه تنها در نسخه کامل فعال می باشد!",
                            limitationText,
                            userNumber,
                            EnumsAndConstants.DEVICE_ID,
                            EnumsAndConstants.SERVER_URL,
                            EnumsAndConstants.GATEWAY_URL,
                            new TX_License.SetLicenseCallback() {
                                @Override
                                public void call(long expireDate, String license) {

                                    Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                    Settings.setSetting(Settings.LICENSE_DATA, license);

                                    try {
                                        JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                        Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                    MainActivity.getInstance().updateFragment(false);
                                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                    if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                        Transaction.appPurchased();

                                    MainActivity.getInstance().reloadMainData();
                                    if (SettingActivity.getInstance() != null)
                                        SettingActivity.getInstance().premiumUIHandling(false);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            },
                            new Payment.UserNameCallBack() {
                                @Override
                                public void getUserName(String username) {
                                    Settings.setSetting(Settings.TX_USER_NUMBER, username);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            }
                    ).show(getFragmentManager(), "tx_payment");
                }
            }
            if (view.equals(myAccountsLayout)) {
                Intent intent = new Intent(MainActivity.this, AccountListActivity.class);
                startActivity(intent);
            }
            if (view.equals(payeesLayout)) {
                Intent intent = new Intent(MainActivity.this, PayeeListActivity.class);
                startActivity(intent);
            }
            if (view.equals(reportsLayout)) {
                Intent intent = new Intent(MainActivity.this, ReportsActivity.class);
                startActivity(intent);
            }
            if (view.equals(easyAccesslayout)) {

                EnumsAndConstants.TransactionTypes type = null;

                Intent intent = new Intent(MainActivity.this, AddAndEditExpensesAndIncomesActivity.class);
                if (Settings.getSetting(Settings.EASY_ACCESS_TYPE).equals("1")) {
                    type = EnumsAndConstants.TransactionTypes.Expense;
                    intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.NewExpense.value());
                } else if (Settings.getSetting(Settings.EASY_ACCESS_TYPE).equals("2")) {
                    type = EnumsAndConstants.TransactionTypes.Income;
                    intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.NewIncome.value());
                }

                ExpensesAndIncomesListAdapter temp = new ExpensesAndIncomesListAdapter(MainActivity.this, PublicModules.getMonthInterval(0), Integer.valueOf(Settings.getSetting(Settings.EXPENSE_ADAPTER_FILTER)), type);
                startActivity(intent);

            }
        };

        View.OnTouchListener touchListeners = (view, motionEvent) -> {
            if (view.equals(settingIcon) && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(0.5f);
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
            } else {
                settingIcon.setAlpha(1f);
            }
            if (view.equals(buyBtn) && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(0.5f);
                String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                Payment.newInstance(R.drawable.ic_dakhlokharj,
                        EnumsAndConstants.APP_ID,
                        EnumsAndConstants.APP_SOURCE,
                        EnumsAndConstants.APP_VERSION,
                        "دخل و خرج",
                        Application.currentBase64,
                        "مزایای نسخه کامل",
                        limitationText,
                        userNumber,
                        EnumsAndConstants.DEVICE_ID,
                        EnumsAndConstants.SERVER_URL,
                        EnumsAndConstants.GATEWAY_URL,
                        new TX_License.SetLicenseCallback() {
                            @Override
                            public void call(long expireDate, String license) {

                                Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                Settings.setSetting(Settings.LICENSE_DATA, license);

                                try {
                                    JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                    Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                MainActivity.getInstance().updateFragment(false);
                                MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال")) {
                                    Transaction.appPurchased();
                                }

                                MainActivity.getInstance().reloadMainData();

                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        },
                        new Payment.UserNameCallBack() {
                            @Override
                            public void getUserName(String username) {
                                Settings.setSetting(Settings.TX_USER_NUMBER, username);
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        }
                ).show(getFragmentManager(), "tx_payment");
            } else {
                buyBtn.setAlpha(1f);
            }
            if (view.equals(easyAccessSettingIcon) && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(0.5f);
                fastAccessDialog(MainActivity.this, string -> {
                    easyAccessTextView.setText(string);
                    if (string.equals("هزینه جدید"))
                        Settings.setSetting(Settings.EASY_ACCESS_TYPE, "1");
                    else
                        Settings.setSetting(Settings.EASY_ACCESS_TYPE, "2");
                }).show();
            } else {
                easyAccessSettingIcon.setAlpha(1f);
            }
            if (view.equals(logo) && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.setAlpha(0.5f);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.taxiapps.org/"));
                startActivity(intent);
            } else {
                logo.setAlpha(1f);
            }
            if (view.equals(search) && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                view.setAlpha(0.5f);
            } else {
                search.setAlpha(1f);
            }
            if (view.equals(backBtn) && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                footerViewPager.setCurrentItem(footerViewPager.getCurrentItem() - 1);
            }
            if (view.equals(nextBtn) && motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                footerViewPager.setCurrentItem(footerViewPager.getCurrentItem() + 1);
            }

            return true;
        };

        footerViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (footerViewPager.getCurrentItem() == 0) {
                    backBtn.setVisibility(View.INVISIBLE);
                } else {
                    backBtn.setVisibility(View.VISIBLE);
                }

                if (footerViewPager.getCurrentItem() == 2) {
                    nextBtn.setVisibility(View.INVISIBLE);
                } else {
                    nextBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                Settings.setSetting(Settings.LAST_WIDGET, String.valueOf(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        settingIcon.setOnTouchListener(touchListeners);
        buyBtn.setOnTouchListener(touchListeners);
        easyAccessSettingIcon.setOnTouchListener(touchListeners);
        logo.setOnTouchListener(touchListeners);
        search.setOnTouchListener(touchListeners);
        backBtn.setOnTouchListener(touchListeners);
        nextBtn.setOnTouchListener(touchListeners);
        expensesLayout.setOnClickListener(clickListeners);
        incomesLayout.setOnClickListener(clickListeners);
        expensesBudgetLayout.setOnClickListener(clickListeners);
        myAccountsLayout.setOnClickListener(clickListeners);
        payeesLayout.setOnClickListener(clickListeners);
        reportsLayout.setOnClickListener(clickListeners);
        easyAccesslayout.setOnClickListener(clickListeners);
        cloudErrorImage.setOnClickListener(clickListeners);
    }

    public List<android.support.v4.app.Fragment> initFragments() {

        List<android.support.v4.app.Fragment> fList = new ArrayList<>();

        ArrayList<ArrayList> monthReport = Reports.YearlyReport.ChartReport();

        List expense = monthReport.get(0).subList(0, 5);
        List income = monthReport.get(1).subList(0, 5);
        //   List budget = monthReport.get(2); lazemesh nadarim
        List<String> months = monthReport.get(3).subList(0, 5);

        // reverse lists to show correctly LTR
        Collections.reverse(expense);
        Collections.reverse(income);
        Collections.reverse(months);

        List<Entry> expenseData = new ArrayList();
        List<Entry> incomesData = new ArrayList();

        boolean hasEnoughData;
        float total = 0;

        for (int i = 0; i < months.size(); i++) {
            expenseData.add(new Entry(i, Float.valueOf(String.valueOf(expense.get(i)))));
            incomesData.add(new Entry(i, Float.valueOf(String.valueOf(income.get(i)))));
            total += Float.valueOf(String.valueOf(expense.get(i))) + Float.valueOf(String.valueOf(income.get(i)));
        }

        if (total > 0)
            hasEnoughData = true;
        else
            hasEnoughData = false;

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("#ff0000", "#00ff00"));

        chart = Chart.initInstance(new ChartConfig(
                new ArrayList<>(Arrays.asList(expenseData, incomesData))
                , new ArrayList<>(Arrays.asList("هزینه", "درآمد"))
                , colors, months
                , 5
                , 3
                , -20
                , true
                , hasEnoughData));

        fList.add(chart);

        fList.add(MostUsedItem.initInstance());

        fList.add(GaugeLevel.initInstance());

        return fList;

    }

    private void setTypeFaces() {
        totalExpensesTextView.setTypeface(Application.amountTypeFace(MainActivity.this));
        totalIncomesTextView.setTypeface(Application.amountTypeFace(MainActivity.this));
        totalExpensesBudgetTextView.setTypeface(Application.amountTypeFace(MainActivity.this));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (MainActivity.getInstance() != null) {
            if (BaseActivity.getInstance() != null) {
                if (MainActivity.getInstance().getClass().getName().equals("PasswordActivity") || !BaseActivity.getInstance().startFromPassActivity)
                    BaseActivity.getInstance().startFromPassActivity = false;
                else
                    BaseActivity.getInstance().startFromPassActivity = true;
            }
        }

        if (BaseActivity.getInstance() != null)
            if (BaseActivity.getInstance().startFromPassActivity) {
                BaseActivity.getInstance().startFromPassActivity = false;
                if (!Settings.getSetting(Settings.PASSWORD).equals("")) {
                    Intent intent = new Intent(this, PasswordActivity.class);
                    intent.putExtra("PassEnteringType", PasswordActivity.PassEnteringType.UserWantToEnter.value());
                    intent.putExtra("onBackDisable", true); // bayad onBackPressed() tu passwordActivity disable bashe
                    startActivity(intent);
                }
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == SMS_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            SMS.getSmsCount(this);
        }
        if (requestCode == WRITE_PERMISSION_FOR_SMS && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            SMSHandling();
            removeTempContents();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadMainData();

        checkStore();
//        appUse();// app use feedback
        apiMessages();// version check and suggest for upgrade app
        checkPushNotification(); // check mikonim az tu notification oomade ya na
        smsHandling();
        checkAppUpdated();// check mikonim barname update shode ya na

        checkActivation();// check for limitation of app
        checkAppLimitation();// handle kardan e view hay pooli

        setAppVersion();//setting -> app version
        initDatePickers();//init datePickers
        checkRecoveryFile();//check mikonim ke ba file oomade tu ya na

        getInstance().appPauseTimer.cancel();// for pass Activity

        if (Settings.getSetting(Settings.CLOUD_ERROR).equals("1") && GoogleSignIn.getLastSignedInAccount(this) == null) {
            cloudErrorImage.setVisibility(View.VISIBLE);
        } else {
            cloudErrorImage.setVisibility(View.INVISIBLE);
        }

        autoBackup();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (!BaseActivity.appIsForeground(this))
                getInstance().appPauseTimer.start();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        exitSureDialog(MainActivity.this).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Application.localization(this);
        instance = this;
        setContentView(R.layout.activity_main);

        init();
        setTypeFaces();
        setListeners();
        checkPin();// password activity
//        testSMS();
    }

    private void testSMS() {

        String bankRegex = "((?<=\\بانک |بانك )[ء-ي ا-ی]+)|(?<=بانك )(.*)";
        String trnTypeRegex = "(پايانه فروش|پایانه فروش|خريداينترنتی|خریداینترنتی|واريز|برداشت| -|واریز|خرید|انتقال|پرداخت قبض)";
        String amountRegex = "\\d.*,[0-9]+";

        ArrayList<SMS> smsList = new ArrayList<>();
        String body = "بانک سپه \nانتقال 400,000- \nحساب 602* \nمانده 2,112,550";
        Pattern namePattern = Pattern.compile(bankRegex);
        Matcher nameMatcher = namePattern.matcher(body);

        Pattern trnTypePattern = Pattern.compile(trnTypeRegex);
        Matcher trnTypeMatcher = trnTypePattern.matcher(body);

        Pattern amountPattern = Pattern.compile(amountRegex);
        Matcher amountMatcher = amountPattern.matcher(PublicModules.toEnglishDigit(body)); // digit hay farsi ro tashkhis nemide

        if (trnTypeMatcher.find() && amountMatcher.find() && nameMatcher.find()) {
            int trnType = 0;
            if (trnTypeMatcher.group(0).equals("برداشت") ||
                    trnTypeMatcher.group(0).equals("پرداخت قبض") ||
                    trnTypeMatcher.group(0).equals("انتقال") ||
                    trnTypeMatcher.group(0).equals("خرید") ||
                    trnTypeMatcher.group(0).equals("خریداینترنتی") ||
                    trnTypeMatcher.group(0).equals("خريداينترنتی") ||
                    trnTypeMatcher.group(0).equals("پایانه فروش") ||
                    trnTypeMatcher.group(0).equals("پايانه فروش")
                    ) {
                trnType = 1;
            } else if (trnTypeMatcher.group(0).equals("واریز") || trnTypeMatcher.group(0).equals("واريز")) {
                trnType = 2;
            }
            SMS sms = new SMS("Sosis", amountMatcher.group(0), trnType);
            smsList.add(sms);
        }


    }

    private void checkStore() {
        switch (EnumsAndConstants.APP_SOURCE) {
            case "bazaar":
                if (!appInstalledOrNot(MainActivity.this, "com.farsitel.bazaar"))
                    EnumsAndConstants.APP_SOURCE = "google";
                break;
            case "myket":
                if (!appInstalledOrNot(MainActivity.this, "ir.mservices.market"))
                    EnumsAndConstants.APP_SOURCE = "google";
                break;
        }

    }

    private void checkPushNotification() {
        if (getIntent().hasExtra("i")) {
            showPushPopup(getIntent().getStringExtra("popUpTitle"), getIntent().getStringExtra("popUpContent"), getIntent().getStringExtra("popUpURL"));
        }
    }

    private void smsHandling() {
        int appRunningCounter = Integer.valueOf(Settings.getSetting(Settings.APP_RUNNING_NUMBER));
        if (appRunningCounter == 0) {
            Intent intent = new Intent(this, WalkThrough.class);
            startActivity(intent);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                SMS.getSmsCount(this);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, SMS_PERMISSION);
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION_FOR_SMS);
            } else {
                SMSHandling();
            }
        }

        Settings.setSetting(Settings.APP_RUNNING_NUMBER, String.valueOf(appRunningCounter + 1));
    }

    private void checkAppUpdated() {
        if (!Settings.getSetting(Settings.APP_VERSION).equals("")) {
            if (Integer.valueOf(EnumsAndConstants.APP_VERSION.replaceAll("\\D", "")) >
                    Integer.valueOf(Settings.getSetting(Settings.APP_VERSION).replaceAll("\\D", ""))) {
                SettingActivity.latestChange(this).show();
                Settings.setSetting(Settings.APP_VERSION, EnumsAndConstants.APP_VERSION);
            }
        } else {
            Settings.setSetting(Settings.APP_VERSION, EnumsAndConstants.APP_VERSION);
            Log.i("APP_VERSION", EnumsAndConstants.APP_VERSION);
        }

    }

    private void checkPin() {
        if (!Settings.getSetting(Settings.PASSWORD).equals("")) {
            Intent intent = new Intent(this, PasswordActivity.class);
            intent.putExtra("PassEnteringType", PasswordActivity.PassEnteringType.UserWantToEnter.value());
            startActivity(intent);
        }
    }

    private void checkAppLimitation() {
        if (Settings.getSetting(Settings.PURCHASE_STATUS).equals("0")) {
            pageAdapter = PageAdapter.initInstance(getSupportFragmentManager(), initFragments());
            footerViewPager.setAdapter(pageAdapter);
            updateFragment(false);
            buyBtn.setVisibility(View.GONE);
        } else {
            if (Application.isLicenseValid()) {
                pageAdapter = PageAdapter.initInstance(getSupportFragmentManager(), initFragments());
                footerViewPager.setAdapter(pageAdapter);
                updateFragment(false);
                buyBtn.setVisibility(View.GONE);
            } else {
                pageAdapter = PageAdapter.initInstance(getSupportFragmentManager(), initFragments());
                footerViewPager.setAdapter(pageAdapter);
                updateFragment(true);
                buyBtn.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setAppVersion() {
        try {
            Settings.setSetting(Settings.APP_VERSION, String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initDatePickers() {
        new Thread(new DayFragmentInit()).start();
        new Thread(new SelectFragmentInit()).start();
    }

    private void checkRecoveryFile() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        Log.i("action", String.valueOf(action));
        Log.i("type", String.valueOf(type));
        Log.i("URI", String.valueOf(intent.getData()));

        Uri fileUri = intent.getData();

        if (Intent.ACTION_VIEW.equals(action) && fileUri != null) {
            File file = new File(fileUri.getPath());
            recoveryFromReceivedFile(this, file).show();
        }

    }

    private void autoBackup() {
        // age signin karde bud                                             // age backup e automatic enable e
        if (GoogleSignIn.getLastSignedInAccount(this) != null && !Settings.getSetting(Settings.AUTO_BACKUP).equals("0")) {

            GoogleDriveBackupActivity.mDriveResourceClient = Drive.getDriveResourceClient(this, GoogleSignIn.getLastSignedInAccount(this));

            // last backup date
            Date lastBackupDate = new Date(Long.valueOf(Settings.getSetting(Settings.BACKUP_LAST_TIME)));

            // today date
            Calendar currentDate = Calendar.getInstance();

            if (lastBackupDate.compareTo(currentDate.getTime()) < 0) {

                String backupPeriod = Settings.getSetting(Settings.BACKUP_PERIOD);
                long dayCount = (currentDate.getTimeInMillis() - lastBackupDate.getTime()) / (24 * 60 * 60 * 1000);

                switch (backupPeriod) {
                    case "Monthly":
                        if (dayCount >= 30) {
                            downloadProgress(this, "در حال پشتیبانگیری").show();
                            GoogleDriveBackupActivity.backupAndSendFileToGoogleDrive(this, GoogleDriveBackupActivity.mDriveResourceClient, true, success -> {
                                downloadProgress(this, "پشتیبانگیری موفق").show();
                            });
                        }

                        break;

                    case "Weekly":
                        if (dayCount >= 7) {
                            downloadProgress(this, "در حال پشتیبانگیری").show();
                            GoogleDriveBackupActivity.backupAndSendFileToGoogleDrive(this, GoogleDriveBackupActivity.mDriveResourceClient, true, success -> {
                                downloadProgress(this, "پشتیبانگیری موفق").show();
                            });
                        }
                        break;

                    case "Daily":
                        if (dayCount >= 1) {
                            downloadProgress(this, "در حال پشتیبانگیری").show();
                            GoogleDriveBackupActivity.backupAndSendFileToGoogleDrive(this, GoogleDriveBackupActivity.mDriveResourceClient, true, success -> {
                                downloadProgress(this, "پشتیبانگیری موفق").show();
                            });
                        }
                        break;
                }

            }

        }
    }

    public static boolean appIsForeground(Context context) throws ExecutionException, InterruptedException {
        return new ForegroundCheckTask().execute(context).get();
    }

    public static void showPushPopup(final String title, final String content, final String link) {
        getInstance().runOnUiThread(() -> PublicDialogs.makeMessageDialog(getInstance(), PublicDialogs.MessageDialogTypes.NORMAL, title, content, link, EnumsAndConstants.APP_SOURCE).show());
    }

    private void SMSHandling() {
        if (Settings.getSetting(Settings.SMS_IS_ENABLE).equals("true"))
            if (SMS.readInbox(this, false).size() > 0) {
                smsDialog = SMS.smsSelectingDialog(this, transactions -> {
                    SMS.writeToDb(transactions);
                    reloadMainData();
                    pageAdapter.notifyDataSetChanged();
                    smsDialog.dismiss();
                });
                smsDialog.show();
            }

    }

    public void updateFragment(boolean isLimited) {
        chart.setLimited(isLimited);
        GaugeLevel.getInstance().isLimited = isLimited;
        MostUsedItem.getInstance().isLimited = isLimited;
        pageAdapter.notifyDataSetChanged();
    }

    public void refreshMainTypeFace() {
        setTypeFaces();
    }

    public void reloadMainData() {

        if (Budget.getSumAll().equals("0"))
            totalExpensesBudgetTextView.setText(Currency.CurrencyString.getCurrencyString("0", Currency.CurrencyMode.Full).separated);
        else
            totalExpensesBudgetTextView.setText(Currency.CurrencyString.getCurrencyString(Budget.getSumAll(), Currency.CurrencyMode.Full).separated);

        if (Transaction.getSumThisMonth(EnumsAndConstants.TransactionTypes.Expense).equals("0"))
            totalExpensesTextView.setText(Currency.CurrencyString.getCurrencyString("0", Currency.CurrencyMode.Full).separated);
        else
            totalExpensesTextView.setText(Currency.CurrencyString.getCurrencyString(Double.valueOf(Transaction.getSumThisMonth(EnumsAndConstants.TransactionTypes.Expense)), Currency.CurrencyMode.Full).separated);

        if (Transaction.getSumThisMonth(EnumsAndConstants.TransactionTypes.Income).equals("0"))
            totalIncomesTextView.setText(Currency.CurrencyString.getCurrencyString("0", Currency.CurrencyMode.Full).separated);
        else
            totalIncomesTextView.setText(Currency.CurrencyString.getCurrencyString(Double.valueOf(Transaction.getSumThisMonth(EnumsAndConstants.TransactionTypes.Income)), Currency.CurrencyMode.Full).separated);

    }

    private void removeTempContents() {

        File temp = new File(Settings.getSetting(Settings.BACKUP_DESTINATION) + "/Temp");

        if (!temp.exists())
            temp.mkdir();

        File[] tempFiles = temp.listFiles();
        if (tempFiles != null)
            for (int i = 0; i < tempFiles.length; i++) {
                tempFiles[i].delete();
            }

    }

    public void refreshUI() {

        if (Application.isLicenseValid()) {
            buyBtn.setVisibility(View.GONE);
            updateFragment(false);
        } else {
            buyBtn.setVisibility(View.VISIBLE);
            updateFragment(true);
        }
        pageAdapter.notifyDataSetChanged();

    }

    private void checkActivation() {
        if (!Settings.getSetting(Settings.PURCHASE_STATUS).equals("-1"))
            return;
        else {
            try {
                String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER);
                PurchaseIsActive(EnumsAndConstants.APP_ID,
                        EnumsAndConstants.APP_SOURCE,
                        EnumsAndConstants.APP_VERSION,
                        EnumsAndConstants.DEVICE_ID,
                        userNumber,
                        EnumsAndConstants.SERVER_URL,
                        new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                return;
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject jsonResponse = new JSONObject(PublicModules.makeJsonString(response.body().string()));
                                        if (jsonResponse.getString("purchase_active").equals("1")) {  // yani bayad payment dashte bashe
                                            Settings.setSetting(Settings.PURCHASE_STATUS, "1");
                                        } else if (jsonResponse.getString("purchase_active").equals("0")) { // yani bayad full version bashe
                                            Settings.setSetting(Settings.PURCHASE_STATUS, "0");
                                        }

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                refreshUI();
                                            }
                                        });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void appUse() {
        try {
            AppUse(EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, EnumsAndConstants.SERVER_URL, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    Log.i("app_use", "Not working");
                }

                @Override
                public void onResponse(Response response) {
                    if (response.isSuccessful()) {
                        Log.i("app_use", "is working");
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void apiMessages() {
        try {
            VersionCheck(EnumsAndConstants.APP_ID, EnumsAndConstants.APP_SOURCE, EnumsAndConstants.APP_VERSION, EnumsAndConstants.DEVICE_ID, EnumsAndConstants.SERVER_URL, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    //TODO return false
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    String responseStr = response.body().string();
                    Log.i("VersionCheck", "is Working");
                    try {
                        final JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(responseStr));
                        switch (jsonObject.getString("popup_type")) {
                            case "message":
                                if (jsonObject.has("welcome_msg"))
                                    if (!jsonObject.getString("welcome_msg").trim().equals("")) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    serverMessageDialog(MainActivity.this, "پیام", jsonObject.getString("welcome_msg")).show();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                        //TODO return true
                                    } else {
                                        //TODO return false
                                    }
                                break;
                            case "update":
                                if (jsonObject.has("welcome_msg"))
                                    if (!jsonObject.getString("welcome_msg").trim().equals("")) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    updateDialog(MainActivity.this, "بروزرسانی", jsonObject.getString("welcome_msg"), jsonObject.getString("download_link")).show();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                        //TODO return true
                                    } else {
                                        //TODO return false
                                    }
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private TSnackbar downloadProgress(Context context, String text) {

        TSnackbar snackBar = TSnackbar.make(((Activity) context).findViewById(android.R.id.content), text, TSnackbar.LENGTH_LONG);
        snackBar.setActionTextColor(Color.WHITE);
        View snackBarView = snackBar.getView();

        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.greenLight));
        TextView textView = snackBarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setGravity(Gravity.CENTER);

        return snackBar;
    }

    private boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    private Dialog recoveryFromReceivedFile(final Context context, final File file) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View recoveryFromReceivedFileLayout = inflater.inflate(R.layout.pop_main_restore, null);

        PersianDate pCalendar = new PersianDate(file.lastModified());

        final TextView description, recovery, cancel;

        description = recoveryFromReceivedFileLayout.findViewById(R.id.pop_main_restore_description);
        recovery = recoveryFromReceivedFileLayout.findViewById(R.id.pop_main_restore_restore);
        cancel = recoveryFromReceivedFileLayout.findViewById(R.id.pop_main_restore_cancel);

        description.setText("فایل پشتیبان با نام " +
                file.getName() +
                " به تاریخ " +
                PersianDateFormat.format(pCalendar, "Y/m/d") +
                " یافت شد. آیا مایل به بازیابی اطلاعات هستید؟\n" +
                "توجه کنید که اطلاعات قبلی شما حذف خواهند شد.");

        int width = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                break;

            case 2:
                width = 63;
                break;

            case 3:
                width = 61;
                break;

        }

        final Dialog recoveryFromReceivedFileDialog = PublicDialogs.makeDialog(context, recoveryFromReceivedFileLayout, width, 0, false);

        recovery.setOnClickListener(v -> recoveryFromReceivedFileWarning(context, recoveryFromReceivedFileDialog, file).show());

        cancel.setOnClickListener(v -> recoveryFromReceivedFileDialog.dismiss());

        return recoveryFromReceivedFileDialog;
    }

    private Dialog recoveryFromReceivedFileWarning(Context context, final Dialog parentDialog, final File file) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View recoveryFromReceivedFileWarningLayout = inflater.inflate(R.layout.pop_main_restore_warning, null);

        TextView ok, cancel;
        ok = recoveryFromReceivedFileWarningLayout.findViewById(R.id.pop_main_restore_warning_ok);
        cancel = recoveryFromReceivedFileWarningLayout.findViewById(R.id.pop_main_restore_warning_cancel);

        int width = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                break;

            case 2:
                width = 63;
                break;

            case 3:
                width = 61;
                break;

        }

        final Dialog recoveryFromReceivedFileWarningDialog = PublicDialogs.makeDialog(context, recoveryFromReceivedFileWarningLayout, width, 0, false);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Backup.recoveryDataFromShare(file);
                recoveryFromReceivedFileWarningDialog.dismiss();
                parentDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recoveryFromReceivedFileWarningDialog.dismiss();
            }
        });

        return recoveryFromReceivedFileWarningDialog;
    }

    private Dialog fastAccessDialog(final Context context, final easyAccessCallBack easyAccessCallBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View fastAccessLayout = inflater.inflate(R.layout.pop_fast_access, null);

        final ConstraintLayout addIncome, addExpense, cancel;
        addIncome = fastAccessLayout.findViewById(R.id.pop_fast_access_add_new_income);
        addExpense = fastAccessLayout.findViewById(R.id.pop_fast_access_add_new_expense);
        cancel = fastAccessLayout.findViewById(R.id.pop_fast_access_cancel);

        int width = 0, height = 0;
        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 63;
                break;

            case 2:
                width = 63;
                height = 51;
                break;

            case 3:
                width = 61;
                height = 49;
                break;

        }

        final Dialog faDialog = PublicDialogs.makeDialog(context, fastAccessLayout, width, height, false);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(addIncome)) {
                easyAccessCallBack.call("درآمد جدید");
                faDialog.dismiss();
            }
            if (v.equals(addExpense)) {
                easyAccessCallBack.call("هزینه جدید");
                faDialog.dismiss();
            }
            if (v.equals(cancel)) {
                faDialog.dismiss();
            }
        };

        addIncome.setOnClickListener(dialogListeners);
        addExpense.setOnClickListener(dialogListeners);
        cancel.setOnClickListener(dialogListeners);

        return faDialog;
    }

    private Dialog serverMessageDialog(Context context, String title, String desc) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View serverMessageLayout = inflater.inflate(R.layout.pop_why_select_account, null);

        TextView titleText, descText;
        titleText = serverMessageLayout.findViewById(R.id.pop_why_select_account_title);
        descText = serverMessageLayout.findViewById(R.id.pop_why_select_account_description);

        titleText.setText(title);
        descText.setText(desc);

        TextView close;

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 30;
                break;

            case 2:
                width = 70;
                height = 20;
                break;

            case 3:
                width = 62;
                height = 17;
                break;
        }

        final Dialog serverMessage = PublicDialogs.makeDialog(context, serverMessageLayout, width, height, false);
        serverMessage.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);

        close = serverMessage.findViewById(R.id.pop_why_select_account_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serverMessage.dismiss();
            }
        });

        return serverMessage;
    }

    private Dialog updateDialog(Context context, String title, String desc, final String link) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View updateDialogLayout = inflater.inflate(R.layout.pop_allways_deny, null);

        final TextView update, cancel, titleText, descText;
        update = updateDialogLayout.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = updateDialogLayout.findViewById(R.id.pop_allways_deny_cancel);
        titleText = updateDialogLayout.findViewById(R.id.pop_always_deny_title);
        descText = updateDialogLayout.findViewById(R.id.pop_always_deny_description);

        titleText.setText(title);
        descText.setText(desc);
        update.setText("بروزرسانی");

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 72;
                height = 19;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog updateDialog = PublicDialogs.makeDialog(context, updateDialogLayout, width, height, false);
        updateDialog.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);

        View.OnClickListener dialogListeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(cancel))
                    updateDialog.dismiss();

                if (v.equals(update)) {
                    if (!link.trim().equals("")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                        startActivity(browserIntent);
                        updateDialog.dismiss();
                    } else
                        updateDialog.dismiss();
                }
            }
        };

        cancel.setOnClickListener(dialogListeners);
        update.setOnClickListener(dialogListeners);

        return updateDialog;
    }

    private Dialog exitSureDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View exitSureDialogLayout = inflater.inflate(R.layout.pop_allways_deny, null);

        final TextView exit, cancel, titleText, descText;
        exit = exitSureDialogLayout.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = exitSureDialogLayout.findViewById(R.id.pop_allways_deny_cancel);
        titleText = exitSureDialogLayout.findViewById(R.id.pop_always_deny_title);
        descText = exitSureDialogLayout.findViewById(R.id.pop_always_deny_description);

        titleText.setText("خروج");
        descText.setText("آیا برای خروج از دخل و خرج مطمئن هستید؟");
        exit.setText("خروج");
        cancel.setText("نه");
        exit.setTextColor(Color.RED);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 72;
                height = 19;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog exitDialog = PublicDialogs.makeDialog(context, exitSureDialogLayout, width, height, false);
        exitDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel))
                exitDialog.dismiss();

            if (v.equals(exit)) {
                finish();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        exit.setOnClickListener(dialogListeners);

        return exitDialog;
    }

    private Dialog googleBackupPopup() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.pop_google_backup);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        TextView goToSettings, dontShowThisMessage;
        goToSettings = dialog.getWindow().getDecorView().findViewById(R.id.pop_google_backup_go_to_setting_text);
        dontShowThisMessage = dialog.getWindow().getDecorView().findViewById(R.id.pop_google_backup_dont_show_text);

        goToSettings.setOnTouchListener((v, event) -> {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                v.setAlpha(0.4f);
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                intent.putExtra("GoToGoogleDriveActivity", true); // for showing GoogleDriveBackupActivity after going to settingActivity
                startActivity(intent);
                dialog.dismiss();
            } else {
                v.setAlpha(1f);
            }
            return true;
        });

        dontShowThisMessage.setOnTouchListener((v, event) -> {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                v.setAlpha(0.4f);
                Settings.setSetting(Settings.CLOUD_ERROR, "0");
                cloudErrorImage.setVisibility(View.GONE);
                dialog.dismiss();
            } else {
                v.setAlpha(1f);
            }
            return true;
        });


        return dialog;

    }

    class SelectFragmentInit implements Runnable {
        @Override
        public void run() {

            SelectBySelectingFragment.dayDates = new ArrayList<>();

            SelectBySelectingFragment.yearDates = new ArrayList<>();
            SelectBySelectingFragment.esfandDays = new ArrayList<>();
            SelectBySelectingFragment.firstSixMonthDayDates = new ArrayList<>();
            SelectBySelectingFragment.secondSixMonthDayDates = new ArrayList<>();

            for (int i = 1; i < 32; i++) {
                SelectBySelectingFragment.firstSixMonthDayDates.add(PublicModules.toPersianDigit(String.valueOf(i)));
                if (SelectBySelectingFragment.secondSixMonthDayDates.size() != 30)
                    SelectBySelectingFragment.secondSixMonthDayDates.add(PublicModules.toPersianDigit(String.valueOf(i)));
                if (SelectBySelectingFragment.esfandDays.size() != 29)
                    SelectBySelectingFragment.esfandDays.add(PublicModules.toPersianDigit(String.valueOf(i)));
            }

            //Default
            SelectBySelectingFragment.dayDates = SelectBySelectingFragment.firstSixMonthDayDates;

            for (int i = 1380; i < 1430; i++) {
                SelectBySelectingFragment.yearDates.add(PublicModules.toPersianDigit(String.valueOf(i)));
            }
            SelectBySelectingFragment.monthDates = new ArrayList<>(Arrays.asList(
                    "فروردین",
                    "اردیبهشت",
                    "خرداد",
                    "تیر",
                    "مرداد",
                    "شهریور",
                    "مهر",
                    "آبان",
                    "آذر",
                    "دی",
                    "بهمن",
                    "اسفند"
            ));

        }
    }

    class DayFragmentInit implements Runnable {
        @Override
        public void run() {
            SelectByDayFragment.stringDates = new ArrayList<>();
            SelectByDayFragment.dateList = new ArrayList<>();

            PersianDateFormat formatter = new PersianDateFormat("l j F Y");
            PersianDate persianCalendar = new PersianDate();
            persianCalendar.setShDay(persianCalendar.getShDay() - 500);

            for (int i = 0; i < 1000; i++) {

                persianCalendar.setShDay(persianCalendar.getShDay() + 1);
                SelectByDayFragment.dateList.add(persianCalendar.getTime());
                SelectByDayFragment.stringDates.add(PublicModules.toPersianDigit(formatter.format(persianCalendar)));

                if (i == 0) {
                    SelectByDayFragment.date = persianCalendar.getTime();
                }
            }
        }
    }

}