package com.taxiapps.dakhlokharj;

import android.Manifest;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.data.Entry;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import adapters.CategoryReportAdapter;
import adapters.MonthlyReportAdapter;
import adapters.PeriodReportAdapter;
import fragments.Chart;
import fragments.PayeeReportFragment;
import github.nisrulz.screenshott.ScreenShott;
import model.ChartConfig;
import model.Currency;
import model.EnumsAndConstants;
import model.Reports;
import model.Settings;
import model.Transaction;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import utils.AppModules;

public class ReportsItemActivity extends FragmentActivity {

    public final int PAYEE_RESULT = 1;
    public final int PERIOD_RESULT = 2;

    private ArrayList<HashMap<String, Object>> periodArray = Transaction.getAll();

    private LinearLayout searchTemp, descriptionLayout;
    private TextView description1, description2;
    private RecyclerView periodReportListView;
    private ImageView clearImageView;

    private PayeeReportSearchActivity.ReturnedItem returnedItem;
    private FrameLayout fragmentContainer;
    private final int PERMISSIONS_REQUEST_CHART_SCREENSHOT = 0;
    private final int PERMISSIONS_REQUEST_SMART_REPORT_SCREENSHOT = 1;
    private final int PERMISSIONS_REQUEST_REPORT_BY_MONTH_SCREENSHOT = 2;
    private final int PERMISSIONS_REQUEST_PERIOD_REPORT_SCREENSHOT = 3;
    private final int PERMISSIONS_REQUEST_PAYEE_REPORT_SCREENSHOT = 4;
    private final int PERMISSIONS_REQUEST_CATEGORY_REPORT_SCREENSHOT = 5;
    private final int PERMISSIONS_REQUEST_SUMMARY_REPORT_SCREENSHOT = 6;
    private FrameLayout fragmentFrame;
    private ConstraintLayout smartReportLayout, reportByMonthLayout, periodReportLayout, reportByCategoryLayout, summaryReportLayout;
    private ImageView chartImg;
    private String mostExpensiveDayOfWeek, mostExpensiveCategories, fridaysAverageExpense, unusedItemsCount;
    private ArrayList<Reports.MonthReport> monthReports;

    private ArrayList<ArrayList<String>> payeeReportItems;
    private ArrayList<HashMap<String, Object>> reportByCategoryList;
    private ArrayList<String[]> summary;
    private TextView periodSearchTextView;
    private ImageView periodSearchImageView;
    private ImageView periodSearchRemoveImageView;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CHART_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(fragmentFrame, "ChartScreenShot");
                    Dialog shareYearly = shareYearlyReportDialog(ReportsItemActivity.this);
                    shareYearly.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareYearly.show();

                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ReportsItemActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(ReportsItemActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog alwaysDenyDialog = alwaysDenyDialog(ReportsItemActivity.this);
                        alwaysDenyDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        alwaysDenyDialog.show();
                    }
                }
                break;
            case PERMISSIONS_REQUEST_SMART_REPORT_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(smartReportLayout, "SmartReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.SmartReport);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ReportsItemActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(ReportsItemActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog allwaysDenydialog = alwaysDenyDialog(ReportsItemActivity.this);
                        allwaysDenydialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        allwaysDenydialog.show();
                    }

                }
                break;
            case PERMISSIONS_REQUEST_REPORT_BY_MONTH_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(reportByMonthLayout, "ReportByMonthScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.ReportByMonth);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ReportsItemActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(ReportsItemActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog allwaysDenydialog = alwaysDenyDialog(ReportsItemActivity.this);
                        allwaysDenydialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        allwaysDenydialog.show();
                    }

                }
                break;
            case PERMISSIONS_REQUEST_PERIOD_REPORT_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(periodReportLayout, "PeriodReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.PeriodReport);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ReportsItemActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(ReportsItemActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog allwaysDenydialog = alwaysDenyDialog(ReportsItemActivity.this);
                        allwaysDenydialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        allwaysDenydialog.show();
                    }

                }
                break;

            case PERMISSIONS_REQUEST_PAYEE_REPORT_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(fragmentContainer, "PayeeReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.PayeeReports);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ReportsItemActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(ReportsItemActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog allwaysDenydialog = alwaysDenyDialog(ReportsItemActivity.this);
                        allwaysDenydialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        allwaysDenydialog.show();
                    }

                }
                break;

            case PERMISSIONS_REQUEST_CATEGORY_REPORT_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(reportByCategoryLayout, "ReportByCategoryScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.ReportByCategory);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ReportsItemActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(ReportsItemActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog allwaysDenydialog = alwaysDenyDialog(ReportsItemActivity.this);
                        allwaysDenydialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        allwaysDenydialog.show();
                    }

                }
                break;

            case PERMISSIONS_REQUEST_SUMMARY_REPORT_SCREENSHOT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takeScreenShot(summaryReportLayout, "SummaryReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.ReportSummary);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ReportsItemActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        String denyDialogTitle = "اشتراک گذاری";
                        String denyDialogDescription = "برای به اشتراگ گذاری نیاز به تأیید شما به منظور دسترسی برنامه است.";
                        Dialog denyDialog = PublicDialogs.makeMessageDialog(ReportsItemActivity.this, PublicDialogs.MessageDialogTypes.SUCCESS, denyDialogTitle, denyDialogDescription, "", EnumsAndConstants.APP_SOURCE);
                        denyDialog.getWindow().setWindowAnimations(R.style.fadeInAnimation);
                        denyDialog.show();
                    } else {
                        Dialog allwaysDenydialog = alwaysDenyDialog(ReportsItemActivity.this);
                        allwaysDenydialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                        allwaysDenydialog.show();
                    }
                }
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYEE_RESULT) {
            if (resultCode == RESULT_OK) {
                returnedItem = data.getParcelableExtra("data");
                if (returnedItem.payeeName != null) {
                    searchTemp.setVisibility(View.GONE);
                    descriptionLayout.setVisibility(View.VISIBLE);

                    PersianDateFormat formatter = new PersianDateFormat("Y/m/d");

                    String des1 = "گزارش ";
                    String des2 = "";
                    if (returnedItem.dateFrom != 0)
                        des1 = des1.concat(" از تاریخ " + formatter.format(new PersianDate(returnedItem.dateFrom)) + " ");
                    if (returnedItem.dateTo != 0)
                        des1 = des1.concat(" تا تاریخ " + formatter.format(new PersianDate(returnedItem.dateTo)) + " ");
                    if (returnedItem.isIncome && !returnedItem.isExpense)
                        des2 = des2.concat("دریافتی ها ");
                    if (returnedItem.isExpense && !returnedItem.isIncome)
                        des2 = des2.concat("پرداختی ها ");
                    if (returnedItem.isIncome && returnedItem.isExpense)
                        des2 = des2.concat("پرداختی ها و دریافتی ها ");

                    des2 = des2.concat(" برای " + returnedItem.payeeName);

                    int transactionType = 0;
                    if (returnedItem.isExpense)
                        transactionType = 1;
                    if (returnedItem.isIncome)
                        transactionType = 2;
                    if (returnedItem.isExpense && returnedItem.isIncome)
                        transactionType = 0;

                    payeeReportItems = Reports.ReportPayee.byPayee(returnedItem.payeeId, returnedItem.dateFrom, returnedItem.dateTo, transactionType);

                    description1.setText(des1);
                    description2.setText(des2);
                    clearImageView.setVisibility(View.VISIBLE);

                }

            } else if (resultCode == RESULT_CANCELED) {
                searchTemp.setVisibility(View.VISIBLE);
                descriptionLayout.setVisibility(View.GONE);
                clearImageView.setVisibility(View.GONE);

                returnedItem = null;
            }

            initFragment(returnedItem);
        }


        if (requestCode == PERIOD_RESULT) {
            if (resultCode == RESULT_OK) {
                periodArray = Transaction.getAll(String.valueOf(PeriodReportSearchActivity.getInstance().fromDateTimeStamp), String.valueOf(PeriodReportSearchActivity.getInstance().toDateTimeStamp));
                if (periodArray.size() == 0) {
                    periodReportListView.setVisibility(View.GONE); // gone sh mikonam ke backgroundesh sefid she ! goshad ham khodeti
                } else {
                    periodReportListView.setAdapter(new PeriodReportAdapter(ReportsItemActivity.this, periodArray));
                    PeriodReportAdapter.getInstance().notifyDataSetChanged();
                }

                periodSearchImageView.setVisibility(View.GONE);
                String searchData = "";
                if (PeriodReportSearchActivity.getInstance().fromDateTimeStamp != 0)
                    searchData = searchData.concat("از تاریخ " + PublicModules.timeStamp2String(new PersianDate(PeriodReportSearchActivity.getInstance().fromDateTimeStamp).getTime(), "yyyy mm dd"));
                if (PeriodReportSearchActivity.getInstance().toDateTimeStamp != 0)
                    searchData = searchData.concat("تا تاریخ " + PublicModules.timeStamp2String(new PersianDate(PeriodReportSearchActivity.getInstance().toDateTimeStamp).getTime(), "yyyy mm dd"));

                periodSearchTextView.setText(searchData);
                periodSearchTextView.setTextColor(getResources().getColor(R.color.gray));
                periodSearchRemoveImageView.setVisibility(View.VISIBLE);

            }
            if (resultCode == RESULT_CANCELED) {

                PeriodReportSearchActivity.getInstance().fromDateTimeStamp = 0;
                PeriodReportSearchActivity.getInstance().toDateTimeStamp = 0;

                periodArray = Transaction.getAll();
                periodReportListView.setVisibility(View.VISIBLE);
                periodReportListView.setAdapter(new PeriodReportAdapter(ReportsItemActivity.this, periodArray));
                PeriodReportAdapter.getInstance().notifyDataSetChanged();

                periodSearchTextView.setText("جستجو");
                periodSearchTextView.setTextColor(getResources().getColor(R.color.linkColor));
                periodSearchImageView.setVisibility(View.VISIBLE);
                periodSearchRemoveImageView.setVisibility(View.INVISIBLE);

            }
        }

    }

    private void initFragment(PayeeReportSearchActivity.ReturnedItem item) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(fragmentContainer.getId(), PayeeReportFragment.newInstance(item));
        fragmentTransaction.commit();
    }

    public enum ReportTypesEnum {

        PayeeReports(0),
        ReportByMonth(1),
        ReportByCategory(2),
        SmartReport(3),
        ReportByYear(4),
        PeriodReport(5),
        ReportSummary(6);

        private final int name;

        ReportTypesEnum(int s) {
            name = s;
        }

        public int value() {
            return this.name;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Application.localization(this);

        switch (getIntent().getExtras().getInt("reportType")) {
            case 0: // PayeeReports         natunestam az khode Enum ha estefadeh konam !
                payeeReport();
                break;

            case 1: // ReportByMonth
                monthReport();
                break;

            case 2: // ReportByCategory
                categoryReport();
                break;

            case 3: // SmartReport
                smartReport();
                break;

            case 4: // ReportByYear
                yearReport();
                break;

            case 5: // PeriodReport
                periodReport();
                break;

            case 6: // ReportSummary
                summaryReport();
                break;

        }
    }

    private void payeeReport() {
        setContentView(R.layout.activity_report_payee);

        ConstraintLayout search = findViewById(R.id.payee_report_activity_search_layout);
        fragmentContainer = findViewById(R.id.payee_report_activity_fragment_container);
        searchTemp = findViewById(R.id.payee_report_activity_search_text_layout);
        descriptionLayout = findViewById(R.id.activity_report_payee_description_layout);
        description1 = findViewById(R.id.activity_report_payee_description1);
        description2 = findViewById(R.id.activity_report_payee_description2);
        clearImageView = findViewById(R.id.payee_report_activity_clear_img);
        final LinearLayout shareLayout = findViewById(R.id.activity_report_payee_sharing_layout);

        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(ReportsItemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((ReportsItemActivity.this),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_PAYEE_REPORT_SCREENSHOT);
                } else {
                    takeScreenShot(fragmentContainer, "PayeeReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.PayeeReports);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                }
            }

        };

        shareLayout.setOnClickListener(listeners);

        initFragment(returnedItem);

        search.setOnClickListener(v -> {
            Intent intent = new Intent(ReportsItemActivity.this, PayeeReportSearchActivity.class);
            if (returnedItem != null)
                intent.putExtra("item", returnedItem);
            startActivityForResult(intent, PAYEE_RESULT);

        });

        clearImageView.setOnClickListener(v -> onActivityResult(PAYEE_RESULT, RESULT_CANCELED, null));
    }

    private void monthReport() {
        setContentView(R.layout.activity_report_by_month);

        final LinearLayout shareLayout = findViewById(R.id.activity_report_by_month_share_layout);
        ProgressBar loading = findViewById(R.id.activity_report_by_month_loading);
        reportByMonthLayout = findViewById(R.id.activity_report_by_month_layout);

        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(ReportsItemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((ReportsItemActivity.this),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_REPORT_BY_MONTH_SCREENSHOT);
                } else {
                    takeScreenShot(reportByMonthLayout, "ReportByMonthScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.ReportByMonth);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                }
            }

        };

        shareLayout.setOnClickListener(listeners);

        RecyclerView recyclerView = findViewById(R.id.activity_report_by_month_list_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(ReportsItemActivity.this));

        loading.setVisibility(View.VISIBLE);
        new Thread(() -> {
            monthReports = Reports.Monthly.recent12Months();
            runOnUiThread(() -> {
                recyclerView.setAdapter(new MonthlyReportAdapter(ReportsItemActivity.this, monthReports));
                recyclerView.smoothScrollToPosition(monthReports.size() - 1);  // for showing last element
                loading.setVisibility(View.GONE);
            });

        }).start();

    }

    private void categoryReport() {
        setContentView(R.layout.activity_report_by_category);

        final LinearLayout shareLayout = findViewById(R.id.report_by_category_activity_share_layout);
        reportByCategoryLayout = findViewById(R.id.report_by_category_activity_layout);
        RecyclerView categoryListView = findViewById(R.id.report_by_category_activity_list_view);

        reportByCategoryList = Reports.ReportCategory.byCategories();

        CategoryReportAdapter adapter = new CategoryReportAdapter(ReportsItemActivity.this, reportByCategoryList);
        categoryListView.setLayoutManager(new LinearLayoutManager(ReportsItemActivity.this));
        categoryListView.setHasFixedSize(true);
        categoryListView.setAdapter(adapter);

        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(ReportsItemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((ReportsItemActivity.this),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_CATEGORY_REPORT_SCREENSHOT);
                } else {

                    takeScreenShot(reportByCategoryLayout, "ReportByCategoryScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.ReportByCategory);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                }
            }

        };

        shareLayout.setOnClickListener(listeners);
    }

    private void smartReport() {
        setContentView(R.layout.activity_smart_report);

        LinearLayout unusedCategory = findViewById(R.id.activity_smart_report_unused_category);
        final LinearLayout list = findViewById(R.id.smart_report_activity_listview_container);
        final LinearLayout shareLayout = findViewById(R.id.smart_report_activity_share_layout);

        ArrayList<ArrayList<String>> items = Reports.SmartReport.unusedCategory();

        LayoutInflater inflater = LayoutInflater.from(ReportsItemActivity.this);

        TextView name, icon;

        final ConstraintLayout smartReportLayout = findViewById(R.id.smart_report_layout);

        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(ReportsItemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((ReportsItemActivity.this),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_SMART_REPORT_SCREENSHOT);
                } else {
                    takeScreenShot(smartReportLayout, "SmartReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.SmartReport);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                }
            }

        };

        shareLayout.setOnClickListener(listeners);

        for (int i = 0; i < items.size(); i++) {

            View view = inflater.inflate(R.layout.item_unused_categories, null);

            name = view.findViewById(R.id.item_unused_categories_title_text_view);
            icon = view.findViewById(R.id.item_unused_categories_icon_text_view);

            name.setText(items.get(i).get(0));

            icon = AppModules.setTypeFace(ReportsItemActivity.this, icon, "fontawesome_webfont.ttf");
            icon.setText(AppModules.convertIconFromString(ReportsItemActivity.this, items.get(i).get(1)));
            icon.setTextColor(Color.parseColor(items.get(i).get(2)));

            list.addView(view);

        }

        final TextView unusedCategoryTitle = findViewById(R.id.smart_report_activity_textview5);
        final TextView unusedItemCount = findViewById(R.id.smart_report_activity_categorys_without_use_textview);
        unusedItemsCount = String.valueOf(items.size());
        unusedItemCount.setText(unusedItemsCount);

        unusedCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                unusedCategoryTitle.setVisibility(View.VISIBLE);
                list.setVisibility(View.VISIBLE);

            }
        });
        mostExpensiveDayOfWeek = Reports.SmartReport.mostUsedDay();
        TextView mostExpensiveDay = findViewById(R.id.smart_report_activity_most_expensive_day_textview);
        mostExpensiveDay.setText(mostExpensiveDayOfWeek);

        mostExpensiveCategories = Reports.SmartReport.mostExpensiveSubCategory();
        TextView mostExpensiveCategory = findViewById(R.id.smart_report_activity_most_expensive_category_textview);
        mostExpensiveCategory.setText(mostExpensiveCategories);

        TextView fridaysAverage = findViewById(R.id.smart_report_activity_average_expenses_in_fridays_textview);
        fridaysAverage.setTypeface(Application.amountTypeFace(ReportsItemActivity.this));
        if (!Reports.SmartReport.fridaysExpenseAverage().equals("-"))
            fridaysAverage.setText(Reports.SmartReport.fridaysExpenseAverage());
        fridaysAverageExpense = fridaysAverage.getText().toString();
    }

    ArrayList<ArrayList> monthReport;

    private void yearReport() {

        setContentView(R.layout.activity_yearly_report);

        TextView totalExpenses, totalIncomes, averageExpense, daysWithoutExpense;
        ProgressBar loading = findViewById(R.id.activity_report_by_year_loading);
        final LinearLayout shareLayout = findViewById(R.id.yearly_report_activity_share_layout);
        Typeface typeface = Application.amountTypeFace(ReportsItemActivity.this);

        totalExpenses = findViewById(R.id.reports_activity_yearly_report_total_expenses);
        totalExpenses.setTypeface(typeface);
        totalExpenses.setText(Reports.YearlyReport.totalExpensesThisYear());

        totalIncomes = findViewById(R.id.reports_activity_yearly_report_total_incomes);
        totalIncomes.setTypeface(typeface);
        totalIncomes.setText(Reports.YearlyReport.totalIncomeThisYear());

        averageExpense = findViewById(R.id.yearly_report_activity_daily_expense_average_textview);
        averageExpense.setTypeface(typeface);
        averageExpense.setText(Reports.YearlyReport.averageExpending());

        daysWithoutExpense = findViewById(R.id.yearly_report_activity_days_number_textview);
        daysWithoutExpense.setTypeface(typeface);
        daysWithoutExpense.setText(Reports.YearlyReport.daysWithoutExpense());

        fragmentFrame = findViewById(R.id.yearly_report_activity_fragment_container);
        final android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();

        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(ReportsItemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((ReportsItemActivity.this),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_CHART_SCREENSHOT);
                } else {

                    takeScreenShot(fragmentFrame, "ChartScreenShot");
                    Dialog shareYearly = shareYearlyReportDialog(ReportsItemActivity.this);
                    shareYearly.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareYearly.show();

                }
            }
        };

        shareLayout.setOnClickListener(listeners);

        loading.setVisibility(View.VISIBLE);
        new Thread(() -> {
            monthReport = Reports.YearlyReport.ChartReport();
            List expense = monthReport.get(0);
            List income = monthReport.get(1);
            List budget = monthReport.get(2);
            ArrayList<String> months = monthReport.get(3);

            // reverse lists to show correctly LTR
            Collections.reverse(expense);
            Collections.reverse(income);
            Collections.reverse(budget);
            Collections.reverse(months);

            List<Entry> expenseData = new ArrayList();
            List<Entry> incomesData = new ArrayList();
            List<Entry> budgetData = new ArrayList();

            boolean hasEnoughData;

            for (int i = 0; i < months.size(); i++) {
                expenseData.add(new Entry(i, Float.valueOf(String.valueOf(expense.get(i)))));
                incomesData.add(new Entry(i, Float.valueOf(String.valueOf(income.get(i)))));
                budgetData.add(new Entry(i, Float.valueOf(String.valueOf(budget.get(i)))));
            }

            if (expenseData.size() > 0)
                hasEnoughData = true;
            else
                hasEnoughData = false;

            ArrayList<String> colors = new ArrayList<>(Arrays.asList("#ff0000", "#00ff00", "#ff8000"));

            Chart chart = Chart.initInstance(new ChartConfig(
                    new ArrayList<>(Arrays.asList(expenseData, incomesData, budgetData))
                    , new ArrayList<>(Arrays.asList("هزینه", "درآمد", "بودجه"))
                    , colors
                    , months
                    , 11
                    , 4
                    , -70
                    , false
                    , hasEnoughData)
            );
            chart.setLimited(false);

            runOnUiThread(() -> {
                fragmentTransaction.replace(fragmentFrame.getId(), chart);
                //    fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                loading.setVisibility(View.GONE);
            });

        }).start();

    }

    private void periodReport() {
        setContentView(R.layout.activity_period_report);
        periodArray = Transaction.getAll();

        final LinearLayout shareLayout = findViewById(R.id.activity_period_report_share_layout);
        LinearLayout search = findViewById(R.id.activity_period_report_search_layout);
        periodSearchTextView = findViewById(R.id.activity_period_report_search_text_view);
        periodSearchImageView = findViewById(R.id.activity_period_report_search_image);
        periodSearchRemoveImageView = findViewById(R.id.activity_period_report_remove_image);
        periodReportListView = findViewById(R.id.activity_period_report_list_view);
        periodReportListView.setLayoutManager(new LinearLayoutManager(ReportsItemActivity.this));
        periodReportListView.setAdapter(new PeriodReportAdapter(this, periodArray));
        periodReportLayout = findViewById(R.id.activity_period_report_layout);

        if (periodArray.size() == 0)
            periodReportListView.setVisibility(View.GONE);


        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(ReportsItemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((ReportsItemActivity.this),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_PERIOD_REPORT_SCREENSHOT);
                } else {

                    takeScreenShot(periodReportLayout, "PeriodReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.PeriodReport);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                }
            }

            if (v.equals(periodSearchRemoveImageView)) {
                onActivityResult(PERIOD_RESULT, RESULT_CANCELED, null);
            }
        };

        shareLayout.setOnClickListener(listeners);
        periodSearchRemoveImageView.setOnClickListener(listeners);

        search.setOnClickListener(v -> {
            Intent intent = new Intent(ReportsItemActivity.this, PeriodReportSearchActivity.class);
            startActivityForResult(intent, PERIOD_RESULT);
        });
    }

    private void summaryReport() {
        setContentView(R.layout.activity_summary_report);

        TextView todayDate, todayExpenseAmount, todayExpenseCurrency, todayIncomeAmount, todayIncomeCurrency;
        todayDate = findViewById(R.id.summary_report_activity_today_date_text_view);
        todayExpenseAmount = findViewById(R.id.summary_report_activity_today_expense_amount_text_view);
        todayIncomeAmount = findViewById(R.id.summary_report_activity_today_income_amount_text_view);
        final LinearLayout shareLayout = findViewById(R.id.summary_report_activity_share_layout);
        summaryReportLayout = findViewById(R.id.summary_report_activity_layout);

        View.OnClickListener listeners = v -> {
            if (v.equals(shareLayout)) {
                if (ContextCompat.checkSelfPermission(ReportsItemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((ReportsItemActivity.this),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_REQUEST_SUMMARY_REPORT_SCREENSHOT);
                } else {

                    takeScreenShot(summaryReportLayout, "SummaryReportScreenShot");
                    Dialog shareDialog = shareReportDialog(ReportsItemActivity.this, ReportTypesEnum.ReportSummary);
                    shareDialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
                    shareDialog.show();

                }
            }
        };

        shareLayout.setOnClickListener(listeners);

        TextView currentWeekExpenseAmount, currentWeekIncomeAmount;
        currentWeekExpenseAmount = findViewById(R.id.summary_report_activity_this_week_expense_amount_text_view);
        currentWeekIncomeAmount = findViewById(R.id.summary_report_activity_this_week_income_amount_text_view);

        TextView past10DaysExpenseAmount, past10DaysIncomeAmount;
        past10DaysExpenseAmount = findViewById(R.id.summary_report_activity_last_10_days_expense_amount_text_view);
        past10DaysIncomeAmount = findViewById(R.id.summary_report_activity_last_10_days_income_amount_text_view);

        TextView currentMonth, currentMonthExpenseAmount, currentMonthIncomeAmount;
        currentMonth = findViewById(R.id.summary_report_activity_this_month_month_name_text_view);
        currentMonthExpenseAmount = findViewById(R.id.summary_report_activity_this_month_expense_amount_text_view);
        currentMonthIncomeAmount = findViewById(R.id.summary_report_activity_this_month_income_amount_text_view);

        TextView currentYear, currentYearExpenseAmount, currentYearIncomeAmount;
        currentYear = findViewById(R.id.summary_report_activity_this_year_text_view);
        currentYearExpenseAmount = findViewById(R.id.summary_report_activity_this_year_expense_amount_text_view);
        currentYearIncomeAmount = findViewById(R.id.summary_report_activity_this_year_income_amount_text_view);

//            PersianCalendar pCalendar = new PersianCalendar();
//            todayDate.setText(pCalendar.getPersianShortDate());
//            currentMonth.setText(pCalendar.getPersianMonthName());
//            currentYear.setText(String.valueOf(pCalendar.getPersianYear()));

        summary = Reports.Summary.summaryReport();

        Typeface typeface = Application.amountTypeFace(ReportsItemActivity.this);

        todayExpenseAmount.setTypeface(typeface);
        todayExpenseAmount.setText(summary.get(0)[0]);
        todayIncomeAmount.setTypeface(typeface);
        todayIncomeAmount.setText(summary.get(0)[1]);
        todayDate.setText(summary.get(0)[2]);

        currentWeekExpenseAmount.setTypeface(typeface);
        currentWeekExpenseAmount.setText(summary.get(1)[0]);
        currentWeekIncomeAmount.setTypeface(typeface);
        currentWeekIncomeAmount.setText(summary.get(1)[1]);

        past10DaysExpenseAmount.setTypeface(typeface);
        past10DaysExpenseAmount.setText(summary.get(2)[0]);
        past10DaysIncomeAmount.setTypeface(typeface);
        past10DaysIncomeAmount.setText(summary.get(2)[1]);

        currentMonthExpenseAmount.setTypeface(typeface);
        currentMonthExpenseAmount.setText(summary.get(3)[0]);
        currentMonthIncomeAmount.setTypeface(typeface);
        currentMonthIncomeAmount.setText(summary.get(3)[1]);
        currentMonth.setText(summary.get(3)[2]);

        currentYearExpenseAmount.setTypeface(typeface);
        currentYearExpenseAmount.setText(summary.get(4)[0]);
        currentYearIncomeAmount.setTypeface(typeface);
        currentYearIncomeAmount.setText(summary.get(4)[1]);
        currentYear.setText(summary.get(4)[2]);
    }

    public void takeScreenShot(View view, String name) {

        Bitmap bitmap_hiddenview = ScreenShott.getInstance().takeScreenShotOfView(view);

        try {

            File file = ScreenShott.getInstance().saveScreenshotToPicturesFolder(ReportsItemActivity.this, bitmap_hiddenview, name);
            if (name.equals("ChartScreenShot")) {
                Settings.setSetting(String.valueOf(Settings.CHART_SCREENSHOT_PATH), file.getAbsolutePath());
            }
            if (name.equals("SmartReportScreenShot")) {
                Settings.setSetting(String.valueOf(Settings.SMART_REPORT_SCREENSHOT_PATH), file.getAbsolutePath());
            }
            if (name.equals("ReportByMonthScreenShot")) {
                Settings.setSetting(String.valueOf(Settings.REPORT_BY_MONTH_SCREENSHOT_PATH), file.getAbsolutePath());
            }
            if (name.equals("PeriodReportScreenShot")) {
                Settings.setSetting(String.valueOf(Settings.PERIOD_REPORT_SCREENSHOT_PATH), file.getAbsolutePath());
            }
            if (name.equals("PayeeReportScreenShot")) {
                Settings.setSetting(String.valueOf(Settings.PAYEE_REPORT_SCREENSHOT_PATH), file.getAbsolutePath());
            }
            if (name.equals("ReportByCategoryScreenShot")) {
                Settings.setSetting(String.valueOf(Settings.CATEGORY_REPORT_SCREENSHOT_PATH), file.getAbsolutePath());
            }
            if (name.equals("SummaryReportScreenShot")) {
                Settings.setSetting(String.valueOf(Settings.SUMMARY_REPORT_SCREENSHOT_PATH), file.getAbsolutePath());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void shareChart(boolean isWithText) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        String path = "file://" + Settings.getSetting(Settings.CHART_SCREENSHOT_PATH);
        Uri screenshotUri = Uri.parse(path);
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        if (isWithText) {
            String text = "۱ - جمع کل هزینه ها: " + Reports.YearlyReport.totalExpensesThisYear() + "\n" +
                    "۲ - جمع کل درآمد ها:  " + Reports.YearlyReport.totalIncomeThisYear() + "\n" +
                    "۳ - میانگین هزینه در روز:  " + Reports.YearlyReport.averageExpending() + " در روز" + "\n" +
                    "۴ - تعداد روز های بدون هزینه:  " + Reports.YearlyReport.daysWithoutExpense() + " روز" + "\n\n" +
                    "گزارش از اپلیکیشن دخل و خرج";

            intent.putExtra(Intent.EXTRA_TEXT, text);
        }
        intent.setType("image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(intent, "Share by"));

    }

    public void shareImage(String imagePath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        String path = "file://" + imagePath;
        Uri screenshotUri = Uri.parse(path);
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        intent.setType("image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(intent, "Share by"));
    }

    public void shareText(String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(intent, "Share by"));
    }

    private Dialog shareYearlyReportDialog(final Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View shareYearlyReportLayout = inflater.inflate(R.layout.pop_share_yearly_report, null);

        final ConstraintLayout shareImg, shareImgAndSummary, cancel;
        TextView icon1, icon2;
        chartImg = shareYearlyReportLayout.findViewById(R.id.pop_share_yearly_report_img);
        shareImg = shareYearlyReportLayout.findViewById(R.id.pop_share_yearly_report_share_img);
        shareImgAndSummary = shareYearlyReportLayout.findViewById(R.id.pop_share_yearly_report_share_img_and_summary);
        cancel = shareYearlyReportLayout.findViewById(R.id.pop_share_yearly_report_cancel);

        icon1 = shareYearlyReportLayout.findViewById(R.id.pop_share_yearly_report_share_img_ic);
        icon2 = shareYearlyReportLayout.findViewById(R.id.pop_share_yearly_report_share_img_and_summary_ic);

        icon1 = AppModules.setTypeFace(ReportsItemActivity.this, icon1, "fontawesome_webfont.ttf");
        icon2 = AppModules.setTypeFace(ReportsItemActivity.this, icon2, "fontawesome_webfont.ttf");

        icon1.setText(R.string.fa_picture_o);
        icon2.setText(R.string.fa_file_text);

        Drawable screenShot = Drawable.createFromPath(Settings.getSetting(String.valueOf(Settings.CHART_SCREENSHOT_PATH)));
        chartImg.setBackground(screenShot);


        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 82;
                height = 68;
                break;

            case 2:
                width = 65;
                height = 58;
                break;

            case 3:
                width = 55;
                height = 52;
                break;

        }

        final Dialog faDialog = PublicDialogs.makeDialog(context, shareYearlyReportLayout, width, height, false);

        View.OnClickListener dialogListeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(shareImg)) {
                    shareChart(false);
                    faDialog.dismiss();
                }
                if (v.equals(shareImgAndSummary)) {
                    shareChart(true);
                    faDialog.dismiss();
                }
                if (v.equals(cancel)) {
                    faDialog.dismiss();
                }
            }
        };

        shareImg.setOnClickListener(dialogListeners);
        shareImgAndSummary.setOnClickListener(dialogListeners);
        cancel.setOnClickListener(dialogListeners);

        return faDialog;
    }

    private Dialog shareReportDialog(final Context context, final ReportTypesEnum reportTypesEnum) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View shareReportLayout = inflater.inflate(R.layout.pop_report_share, null);

        final ConstraintLayout shareImg, shareText, cancel;
        TextView icon1, icon2;
        shareImg = shareReportLayout.findViewById(R.id.pop_report_share_img);
        shareText = shareReportLayout.findViewById(R.id.pop_report_share_text);
        cancel = shareReportLayout.findViewById(R.id.pop_report_share_cancel);

        icon1 = shareReportLayout.findViewById(R.id.pop_report_share_ic1);
        icon2 = shareReportLayout.findViewById(R.id.pop_report_share_ic2);

        icon1 = AppModules.setTypeFace(ReportsItemActivity.this, icon1, "fontawesome_webfont.ttf");
        icon2 = AppModules.setTypeFace(ReportsItemActivity.this, icon2, "fontawesome_webfont.ttf");

        icon1.setText(R.string.fa_picture_o);
        icon2.setText(R.string.fa_file_text);


        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 82;
                height = 34;
                break;

            case 2:
                width = 75;
                height = 29;
                break;

            case 3:
                width = 60;
                height = 23;
                break;

        }

        final Dialog faDialog = PublicDialogs.makeDialog(context, shareReportLayout, width, height, false);

        View.OnClickListener dialogListeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(shareImg)) {
                    if (reportTypesEnum == ReportTypesEnum.SmartReport) {
                        shareImage(Settings.getSetting(Settings.SMART_REPORT_SCREENSHOT_PATH));
                    }
                    if (reportTypesEnum == ReportTypesEnum.ReportByMonth) {
                        shareImage(Settings.getSetting(Settings.REPORT_BY_MONTH_SCREENSHOT_PATH));
                    }
                    if (reportTypesEnum == ReportTypesEnum.PeriodReport) {
                        shareImage(Settings.getSetting(Settings.PERIOD_REPORT_SCREENSHOT_PATH));
                    }
                    if (reportTypesEnum == ReportTypesEnum.PayeeReports) {
                        shareImage(Settings.getSetting(Settings.PAYEE_REPORT_SCREENSHOT_PATH));
                    }
                    if (reportTypesEnum == ReportTypesEnum.ReportByCategory) {
                        shareImage(Settings.getSetting(Settings.CATEGORY_REPORT_SCREENSHOT_PATH));
                    }
                    if (reportTypesEnum == ReportTypesEnum.ReportSummary) {
                        shareImage(Settings.getSetting(Settings.SUMMARY_REPORT_SCREENSHOT_PATH));
                    }
                    faDialog.dismiss();
                }
                if (v.equals(shareText)) {
                    if (reportTypesEnum == ReportTypesEnum.SmartReport) {
                        String text = "۱ - پرهزینه ترین روز هفته:  " + mostExpensiveDayOfWeek + "\n" +
                                "۲ - پرهزینه ترین دسته بندی:  " + mostExpensiveCategories + "\n" +
                                "۳ - میانگین هزینه کرد در جمعه ها:  " + fridaysAverageExpense + "\n" +
                                "۴ - دسته بندی های بدون استفاده:  " + PublicModules.toPersianDigit(unusedItemsCount) + " مورد" + "\n\n" +
                                "گزارش از اپلیکیشن دخل و خرج";
                        shareText(text);
                    }
                    if (reportTypesEnum == ReportTypesEnum.ReportByMonth) {
                        String text = "";
                        for (int i = 0; i < monthReports.size(); i++) {
                            Reports.MonthReport monthReport = monthReports.get(i);

                            text = text.concat(PublicModules.toPersianDigit(String.valueOf(i + 1)) + " -  " + monthReport.month + "\n");
                            text = text.concat("     هزینه:  " + monthReport.expense + "\n");
                            text = text.concat("     درآمد:  " + monthReport.income + "\n");
                        }
                        text = text.concat("\n\n" + "گزارش از اپلیکیشن دخل و خرج");
                        shareText(text);
                    }
                    if (reportTypesEnum == ReportTypesEnum.PeriodReport) {
                        String text = "";
                        for (int i = 0; i < periodArray.size(); i++) {

                            text = text.concat("- " + periodArray.get(i).get("lsdName").toString() + "\n");

                            text = text.concat(PublicModules.toPersianDigit(PublicModules.timeStamp2String(Long.parseLong(String.valueOf(periodArray.get(i).get("trnDate"))), "yyyy/MM/dd HH:mm")) + "\n");

                            if ((i + 1) != periodArray.size()) {
                                text = text.concat(Currency.CurrencyString.getCurrencyString(String.valueOf(periodArray.get(i).get("trnAmount")), Currency.CurrencyMode.Short).separated + "\n");
                            } else {
                                text = text.concat(Currency.CurrencyString.getCurrencyString(String.valueOf(periodArray.get(i).get("trnAmount")), Currency.CurrencyMode.Short).separated);
                            }
                        }
                        text = text.concat("\n\n" + "گزارش از اپلیکیشن دخل و خرج");
                        shareText(text);


                    }
                    if (reportTypesEnum == ReportTypesEnum.PayeeReports) {
                        String text = "گزارش برای " + returnedItem.payeeName + "\n\n";
                        for (int i = 0; i < payeeReportItems.size(); i++) {

                            text = text.concat("- " + payeeReportItems.get(i).get(0) + "\n");

                            text = text.concat(PublicModules.toPersianDigit(payeeReportItems.get(i).get(2)) + "\n");
                            if ((i + 1) != payeeReportItems.size()) {
                                text = text.concat(payeeReportItems.get(i).get(1) + "\n");
                            } else {
                                text = text.concat(payeeReportItems.get(i).get(1));
                            }
                        }
                        text = text.concat("\n\n" + "گزارش از اپلیکیشن دخل و خرج");
                        shareText(text);
                    }

                    if (reportTypesEnum == ReportTypesEnum.ReportByCategory) {
                        String text = "";
                        for (int i = 0; i < reportByCategoryList.size(); i++) {
                            text = text.concat(PublicModules.toPersianDigit(String.valueOf(i + 1)) + " -  " + reportByCategoryList.get(i).get("lstName") + "\n");

                            text = text.concat("هزینه: " + Currency.CurrencyString.getCurrencyString(String.valueOf(reportByCategoryList.get(i).get("EX")), Currency.CurrencyMode.Short).separated + "\n");
                        }
                        text = text.concat("\n\n" + "گزارش از اپلیکیشن دخل و خرج");
                        shareText(text);
                    }

                    if (reportTypesEnum == ReportTypesEnum.ReportSummary) {
                        String text = "";
                        text = text.concat(PublicModules.toPersianDigit(String.valueOf(1)) + " -  امروز" + "\n");
                        text = text.concat("        هزینه: " + Currency.CurrencyString.getCurrencyString(summary.get(0)[0], null).separated) + "\n";
                        text = text.concat("        درآمد: " + Currency.CurrencyString.getCurrencyString(summary.get(0)[1], null).separated) + "\n";

                        text = text.concat(PublicModules.toPersianDigit(String.valueOf(2)) + " -  هفته جاری" + "\n");
                        text = text.concat("        هزینه: " + Currency.CurrencyString.getCurrencyString(summary.get(1)[0], null).separated) + "\n";
                        text = text.concat("        درآمد: " + Currency.CurrencyString.getCurrencyString(summary.get(1)[1], null).separated) + "\n";

                        text = text.concat(PublicModules.toPersianDigit(String.valueOf(3)) + " -  ۱۰ روز گذشته" + "\n");
                        text = text.concat("        هزینه: " + Currency.CurrencyString.getCurrencyString(summary.get(2)[0], null).separated) + "\n";
                        text = text.concat("        درآمد: " + Currency.CurrencyString.getCurrencyString(summary.get(2)[1], null).separated) + "\n";

                        text = text.concat(PublicModules.toPersianDigit(String.valueOf(4)) + " -  ماه جاری" + "\n");
                        text = text.concat("        هزینه: " + Currency.CurrencyString.getCurrencyString(summary.get(3)[0], null).separated) + "\n";
                        text = text.concat("        درآمد: " + Currency.CurrencyString.getCurrencyString(summary.get(3)[1], null).separated) + "\n";

                        text = text.concat(PublicModules.toPersianDigit(String.valueOf(5)) + " -  سال جاری" + "\n");
                        text = text.concat("        هزینه: " + Currency.CurrencyString.getCurrencyString(summary.get(4)[0], null).separated) + "\n";
                        text = text.concat("        درآمد: " + Currency.CurrencyString.getCurrencyString(summary.get(4)[1], null).separated) + "\n\n";

                        text = text.concat("گزارش از اپلیکیشن دخل و خرج");

                        shareText(text);
                    }
                    faDialog.dismiss();
                }
                if (v.equals(cancel)) {
                    faDialog.dismiss();
                }
            }
        };

        shareImg.setOnClickListener(dialogListeners);
        shareText.setOnClickListener(dialogListeners);
        cancel.setOnClickListener(dialogListeners);

        return faDialog;
    }

    private Dialog alwaysDenyDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View popAllwaysDeny = inflater.inflate(R.layout.pop_allways_deny, null);

        final TextView goToSetting, cancel;
        goToSetting = popAllwaysDeny.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = popAllwaysDeny.findViewById(R.id.pop_allways_deny_cancel);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 72;
                height = 19;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog rpydDialog = PublicDialogs.makeDialog(context, popAllwaysDeny, width, height, false);

        View.OnClickListener dialogListeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(cancel))
                    rpydDialog.dismiss();

                if (v.equals(goToSetting)) {
                    Intent intent = new Intent();
                    intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);

                    rpydDialog.dismiss();
                }
            }
        };

        cancel.setOnClickListener(dialogListeners);
        goToSetting.setOnClickListener(dialogListeners);

        return rpydDialog;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (!BaseActivity.appIsForeground(this))
                MainActivity.getInstance().appPauseTimer.start();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.getInstance().appPauseTimer.cancel();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (MainActivity.getInstance().getClass().getName().equals("PasswordActivity") || !BaseActivity.getInstance().startFromPassActivity)
            BaseActivity.getInstance().startFromPassActivity = false;
        else
            BaseActivity.getInstance().startFromPassActivity = true;

        if (BaseActivity.getInstance().startFromPassActivity) {
            BaseActivity.getInstance().startFromPassActivity = false;
            if (!Settings.getSetting(Settings.PASSWORD).equals("")) {
                Intent intent = new Intent(this, PasswordActivity.class);
                intent.putExtra("PassEnteringType", PasswordActivity.PassEnteringType.UserWantToEnter.value());
                intent.putExtra("onBackDisable", true); // bayad onBackPressed() tu passwordActivity disable bashe
                startActivity(intent);
            }
        }
    }

}