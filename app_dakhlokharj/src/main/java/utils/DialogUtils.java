package utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.taxiapps.dakhlokharj.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import model.Currency;
import model.Settings;
import modules.PublicDialogs;
import modules.PublicModules;

/**
 * Created by Parsa on 2017-10-14.
 */

public class DialogUtils {

    private static TextWatcher textWatcher;

    public interface TimePickerCallBack {
        void call(String time);
    }

    public interface keyPadCallBack {
        void call(String number);
    }

    public interface RemoveTrnCallBack {
        void call();
    }

    public static Dialog keypadDialog(final Context context, String number, final keyPadCallBack keyPadCallBack) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_keypad, null);
        int width;
        int height;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 83;
                height = 75;
                break;
            case 2:
                width = 64;
                height = 60;
                break;
            case 3:
                width = 64;
                height = 60;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }
        final Dialog dialog = PublicDialogs.makeDialog(context, view, width, height, true);

        Button number0 = view.findViewById(R.id.keypad_btn0);
        Button number1 = view.findViewById(R.id.keypad_btn1);
        Button number2 = view.findViewById(R.id.keypad_btn2);
        Button number3 = view.findViewById(R.id.keypad_btn3);
        Button number4 = view.findViewById(R.id.keypad_btn4);
        Button number5 = view.findViewById(R.id.keypad_btn5);
        Button number6 = view.findViewById(R.id.keypad_btn6);
        Button number7 = view.findViewById(R.id.keypad_btn7);
        Button number8 = view.findViewById(R.id.keypad_btn8);
        Button number9 = view.findViewById(R.id.keypad_btn9);
        Button number000 = view.findViewById(R.id.keypad_btn000);
        TextView accept = view.findViewById(R.id.keypad_btn_ok);
        TextView cancel = view.findViewById(R.id.keypad_btn_exit);
        Button result = view.findViewById(R.id.keypad_btn_equal);
        Button addition = view.findViewById(R.id.keypad_btn_plus);
        Button subtraction = view.findViewById(R.id.keypad_btn_minus);
        Button multiplication = view.findViewById(R.id.keypad_btn_multiply);
        Button division = view.findViewById(R.id.keypad_btn_devide);
        Button percent = view.findViewById(R.id.keypad_btn_percent);
        Button negate = view.findViewById(R.id.keypad_btn_negate);
        final Button reset = view.findViewById(R.id.keypad_btn_clear);
        Button backSpace = view.findViewById(R.id.keypad_btn_backspace);
        final EditText userNumber = view.findViewById(R.id.pop_keypad_user_number_entered);
        TextView currency = view.findViewById(R.id.pop_keypad_currency);

        currency.setText(Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full));

        if (number.equals("") || number.equals("۰")) {
            userNumber.setText("0");
        } else {
            number = PublicModules.toEnglishDigit(number);
            number = Currency.CurrencyString.getCurrencyString(number, null).separated;
            userNumber.setText(number);
        }

        boolean isRialOrToman = Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("0") || Settings.getSetting(Settings.CASH_TYPE_INDEX).equals("1");
        number000.setBackgroundResource(isRialOrToman ? R.drawable.img_keypad_btn000 : R.drawable.img_keypad_btn_dot);

        View.OnClickListener clickListener = new View.OnClickListener() {
            private String firstNumber = "";
            int clickCount = 0;
            String operation = "";
            boolean equalClicked;

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.keypad_btn0:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("0");
                        } else {
                            if (userNumber.length() >= 1) {
                                if (equalClicked) {
                                    userNumber.setText("0");
                                    equalClicked = false;
                                } else {
                                    userNumber.append("0");
                                }
                            }
                        }
                        break;
                    case R.id.keypad_btn1:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("1");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("1");
                                equalClicked = false;
                            } else {
                                userNumber.append("1");
                            }
                        }
                        break;
                    case R.id.keypad_btn2:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("2");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("2");
                                equalClicked = false;
                            } else {
                                userNumber.append("2");
                            }
                        }
                        break;
                    case R.id.keypad_btn3:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("3");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("3");
                                equalClicked = false;
                            } else {
                                userNumber.append("3");
                            }
                        }
                        break;
                    case R.id.keypad_btn4:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("4");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("4");
                                equalClicked = false;
                            } else {
                                userNumber.append("4");
                            }
                        }
                        break;
                    case R.id.keypad_btn5:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("5");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("5");
                                equalClicked = false;
                            } else {
                                userNumber.append("5");
                            }
                        }
                        break;
                    case R.id.keypad_btn6:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("6");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("6");
                                equalClicked = false;
                            } else {
                                userNumber.append("6");
                            }
                        }
                        break;
                    case R.id.keypad_btn7:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("7");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("7");
                                equalClicked = false;
                            } else {
                                userNumber.append("7");
                            }
                        }
                        break;
                    case R.id.keypad_btn8:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("8");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("8");
                                equalClicked = false;
                            } else {
                                userNumber.append("8");
                            }
                        }
                        break;
                    case R.id.keypad_btn9:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("9");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("9");
                                equalClicked = false;
                            } else {
                                userNumber.append("9");
                            }
                        }
                        break;
                    case R.id.keypad_btn000:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText(isRialOrToman ? "000" : ".");
                        } else {
                            if (equalClicked) {
                                userNumber.setText(isRialOrToman ? "000" : ".");
                                equalClicked = false;
                            } else {
                                String userInput = userNumber.getText().toString();
                                if (isRialOrToman) {
                                    userNumber.setText(userInput + "000");
                                } else {
                                    if (!userInput.contains(".")) {
                                        userNumber.setText(userInput + ".");
                                    }
                                }
                            }
                        }
                        break;
                    case R.id.keypad_btn_plus:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "+";
                        break;
                    case R.id.keypad_btn_minus:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "-";
                        break;
                    case R.id.keypad_btn_multiply:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "*";
                        break;
                    case R.id.keypad_btn_devide:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "/";
                        break;
                    case R.id.keypad_btn_percent:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        double res = Double.valueOf(PublicModules.toEnglishDigit(firstNumber).replace("/", ".")) / 100;
                        userNumber.setText(String.valueOf(res));
                        firstNumber = "";
                        break;
                    case R.id.keypad_btn_negate:
                        String input = userNumber.getText().toString();
                        if (!input.equals("") && !input.equals("0")) {
                            if (input.charAt(0) == '-') {
                                userNumber.setText(input.substring(1, input.length()));
                            } else {
                                userNumber.setText("-" + input);
                            }
                        }
                        break;
                    case R.id.keypad_btn_equal:
                        double result;
                        if (!firstNumber.equals("")) {
                            equalClicked = true;
                            switch (operation) {
                                case "+":
                                    result = Double.valueOf(firstNumber.replace("/", ".")) + Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.valueOf(result));
                                    firstNumber = "";
                                    break;

                                case "-":
                                    result = Double.valueOf(firstNumber.replace("/", ".")) - Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.valueOf(result));
                                    firstNumber = "";
                                    break;

                                case "*":
                                    result = Double.valueOf(firstNumber.replace("/", ".")) * Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.valueOf(result));
                                    firstNumber = "";

                                    break;

                                case "/":
                                    double resD = Double.valueOf(firstNumber.replace("/", ".")) / Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.valueOf(resD));
                                    break;

                            }
                        }
                        break;
                    case R.id.keypad_btn_exit:
                        dialog.dismiss();
                        break;
                    case R.id.keypad_btn_ok:
                        if (!userNumber.getText().toString().equals(""))
                            keyPadCallBack.call(userNumber.getText().toString());
                        else
                            keyPadCallBack.call("0");
                        dialog.dismiss();
                        break;
                    case R.id.keypad_btn_clear:
                        userNumber.setText("0");
                        break;
                    case R.id.keypad_btn_backspace:
                        if (userNumber.length() >= 1) {
                            String str = userNumber.getText().toString();
                            str = str.substring(0, userNumber.length() - 1);
                            userNumber.setText(str);
                        }
                        break;
                }
            }
        };

        number0.setOnClickListener(clickListener);
        number1.setOnClickListener(clickListener);
        number2.setOnClickListener(clickListener);
        number3.setOnClickListener(clickListener);
        number4.setOnClickListener(clickListener);
        number5.setOnClickListener(clickListener);
        number6.setOnClickListener(clickListener);
        number7.setOnClickListener(clickListener);
        number8.setOnClickListener(clickListener);
        number9.setOnClickListener(clickListener);
        number000.setOnClickListener(clickListener);
        accept.setOnClickListener(clickListener);
        cancel.setOnClickListener(clickListener);
        reset.setOnClickListener(clickListener);
        backSpace.setOnClickListener(clickListener);
        result.setOnClickListener(clickListener);
        addition.setOnClickListener(clickListener);
        subtraction.setOnClickListener(clickListener);
        multiplication.setOnClickListener(clickListener);
        division.setOnClickListener(clickListener);
        percent.setOnClickListener(clickListener);
        negate.setOnClickListener(clickListener);

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String userInput = PublicModules.toEnglishDigit(userNumber.getText().toString());
                userNumber.removeTextChangedListener(textWatcher);

                if (!userInput.isEmpty()) {
                    if (isRialOrToman) {
                        userInput = userInput.replaceAll("\\D", "");
                        userInput = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(userInput.replace(",", "")));
                    } else {
                        userInput = userInput.replace(",", "");
                        if (userInput.contains(".")) {
                            if (userInput.replace(".", "").length() > 1) {
                                String fractionalDigits = userInput.substring(userInput.indexOf("."), userInput.length());
                                if (fractionalDigits.length() > 3) {
                                    userInput = userInput.substring(0, userInput.length() - 1);
                                }
                                try {
                                    userInput = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(userInput.replace(",", "")));
                                } catch (NumberFormatException e) {
                                    userInput = userInput.substring(0, userInput.length() - 1);
                                    userInput = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(userInput.replace(",", "")));
                                }

                                if (!userInput.contains(".")) {
                                    userInput = userInput.concat(".");
                                }
                            }
                        } else {
                            userInput = String.format(Locale.US,"%,.0f", Double.valueOf(userInput));
                        }
                    }
                }
                userNumber.setText(userInput);
                userNumber.addTextChangedListener(textWatcher);
            }
        };

        userNumber.addTextChangedListener(textWatcher);

        return dialog;
    }

    public static Dialog timePickerDialog(Context context, String currentTime, final TimePickerCallBack callBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.pop_time_picker, null);

        final TextView ok, cancel, nowTime;
        ok = view.findViewById(R.id.pop_time_picker_ok);
        cancel = view.findViewById(R.id.pop_time_picker_cancel);
        nowTime = view.findViewById(R.id.pop_time_picker_select_today);

        int width = 0;
        int height = 0;
        switch (PublicModules.screenSize(context)) {
            case 1:
                width = 75;
                height = 41;
                break;
            case 2:
                width = 65;
                height = 28;
                break;
            case 3:
                width = 55;
                height = 22;
                break;
        }

        final List<String> hours = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            if (i < 10)
                hours.add(PublicModules.toPersianDigit("0" + i));
            else
                hours.add(PublicModules.toPersianDigit(String.valueOf(i)));
        }

        final List<String> minutes = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            if (i < 10)
                minutes.add(PublicModules.toPersianDigit("0" + i));
            else
                minutes.add(PublicModules.toPersianDigit(String.valueOf(i)));
        }

        Typeface wheelTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/yekan.ttf");

        final WheelPicker wheelPickerHour = view.findViewById(R.id.wheel_picker_hour);
        final WheelPicker wheelPickerMinute = view.findViewById(R.id.wheelPicker_minute);

        wheelPickerHour.setData(hours);
        wheelPickerHour.setIndicator(true);
        wheelPickerHour.setSelectedItemTextColor(Color.BLACK);
        wheelPickerHour.setTypeface(wheelTypeface);

        wheelPickerMinute.setData(minutes);
        wheelPickerMinute.setIndicator(true);
        wheelPickerMinute.setSelectedItemTextColor(Color.BLACK);
        wheelPickerMinute.setTypeface(wheelTypeface);

        String[] time = currentTime.split(":");

        for (int i = 0; i < hours.size(); i++) {

            if (Integer.valueOf(hours.get(i)) == Integer.valueOf(time[0]))
                wheelPickerHour.setSelectedItemPosition(i);

        }

        for (int i = 0; i < minutes.size(); i++) {

            if (Integer.valueOf(minutes.get(i)) == Integer.valueOf(time[1].trim()))
                wheelPickerMinute.setSelectedItemPosition(i);

        }

        final Dialog timePickerDialog = PublicDialogs.makeDialog(context, view, width, height, false);

        View.OnClickListener timePickerDialogListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(ok)) {

                    String res = hours.get(wheelPickerHour.getCurrentItemPosition()) + ":" + minutes.get(wheelPickerMinute.getCurrentItemPosition());
                    callBack.call(res);
                    timePickerDialog.dismiss();

                }
                if (v.equals(cancel)) {
                    timePickerDialog.dismiss();
                }
                if (v.equals(nowTime)) {

                    Calendar calendar = Calendar.getInstance();
                    int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                    int currentMinute = calendar.get(Calendar.MINUTE);

                    for (int i = 0; i < hours.size(); i++) {

                        if (Integer.valueOf(hours.get(i)) == currentHour)
                            wheelPickerHour.setSelectedItemPosition(i);


                    }

                    for (int i = 0; i < minutes.size(); i++) {

                        if (Integer.valueOf(minutes.get(i)) == currentMinute)
                            wheelPickerMinute.setSelectedItemPosition(i);

                    }

                }
            }
        };

        ok.setOnClickListener(timePickerDialogListener);
        cancel.setOnClickListener(timePickerDialogListener);
        nowTime.setOnClickListener(timePickerDialogListener);

        return timePickerDialog;
    }

    public static Dialog removeTransactionDialog(Context context, final RemoveTrnCallBack callBack) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View dlgLayout = inflater.inflate(R.layout.pop_remove_transaction, null);

        int width = 0, height = 0;

        switch (PublicModules.screenSize(context)) {

            case 1:
                width = 84;
                height = 22;
                break;

            case 2:
                width = 55;
                height = 16;
                break;

            case 3:
                width = 45;
                height = 13;
                break;


        }


        final Dialog removeDlg = PublicDialogs.makeDialog(context, dlgLayout, width, height, false);

        TextView remove, cancel;
        remove = dlgLayout.findViewById(R.id.pop_remove_transaction_delete);
        cancel = dlgLayout.findViewById(R.id.pop_remove_transaction_cancel);

        remove.setOnClickListener(v -> {
            callBack.call();
            removeDlg.dismiss();
        });

        cancel.setOnClickListener(v -> removeDlg.dismiss());

        return removeDlg;
    }

}