package utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Build;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.jaredrummler.android.device.DeviceName;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

import de.cketti.mailto.EmailIntentBuilder;
import model.Account;
import model.Bank;
import model.EnumsAndConstants;
import model.Group;
import model.GroupDetail;
import model.Payee;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

/**
 * Created by Parsa on 9/20/2017.
 */

public class AppModules {

    public static String toDB(Object obj, EnumsAndConstants.Types type) {
        String res = "";
        if (obj == null)
            res = "NULL";
        else if (type == EnumsAndConstants.Types.STRING)
            res = obj == "" ? "NULL" : "'" + String.valueOf(obj) + "'";
        else if (type == EnumsAndConstants.Types.INTEGER)
            res = obj == "" ? "NULL" : PublicModules.toEnglishDigit(String.valueOf(obj));
        else if (type == EnumsAndConstants.Types.LONG)
            res = obj == "" ? "NULL" : PublicModules.toEnglishDigit(String.valueOf(obj));
        else if (type == EnumsAndConstants.Types.DOUBLE)
            res = obj == "" ? "NULL" : PublicModules.toEnglishDigit(String.format(Locale.US, "%.2f", Double.parseDouble(String.valueOf(obj))).replace("٫", "."));
        else if (type == EnumsAndConstants.Types.BOOLEAN)
            res = obj == "" ? "NULL" : obj.toString().equals("true") ? "1" : "0";
        else if (type == EnumsAndConstants.Types.FLOAT)
            res = obj == "" ? "NULL" : PublicModules.toEnglishDigit(String.valueOf(obj));
        return res;
    }

    public static String makeStringFromArrayListStrings(ArrayList<String> list, String seperator) {

        String result = "";

        for (int i = 0; i < list.size(); i++) {
            result += list.get(i) + seperator;
        }

        return result.substring(0, result.length() - 1);
    }

    public static String replaceLast(String text, String search, String replace) {
        int pos = text.lastIndexOf(search);
        if (pos < 0) {
            return text;
        }
        return text.substring(0, pos) + replace + text.substring(pos + search.length());
    }

    public static String replaceFirst(String text, String search, String replace) {
        int pos = text.indexOf(search);
        if (pos < 0) {
            return text;
        }
        return text.substring(0, pos) + replace + text.substring(pos + search.length());
    }

    public static String dayWeekToString(String number) {

        switch (number) {

            case "0":
                return "شنبه";
            case "1":
                return "یکشنبه";
            case "2":
                return "دو شنبه";
            case "3":
                return "سه شنبه";
            case "4":
                return "چهار شنبه";
            case "5":
                return "پنجشنبه";
            case "6":
                return "جمعه";
            default:
                return "?";

        }

    }

    public static long calendarOrdinality(PersianDate pCalendar) {

        pCalendar.setMinute(0);

        PersianDate persianCalendar = new PersianDate();
        persianCalendar.setShMonth(1);
        persianCalendar.setShDay(1);

        persianCalendar.setMinute(0);

        long compare = ((pCalendar.getTime() - persianCalendar.getTime()) / (24 * 60 * 60) / 1000);

        return compare;

    }

    public static TextView setTypeFace(Context context, TextView textView, String fontName) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
        textView.setTypeface(typeface);
        return textView;
    }

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void showSoftKeyboard(Context context, View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static int convertIconFromString(Context context, String icon) {
        return context.getResources().getIdentifier(icon.replaceAll("-", "_"), "string", context.getPackageName());
    }

    public static ArrayList<Payee> filterPayee(String searchedPhrase, ArrayList<Payee> itemsToSearchIn) {

        ArrayList<Payee> filteredPayees = new ArrayList<>();
        for (int i = 0; i < itemsToSearchIn.size(); i++) {
            Payee payee = itemsToSearchIn.get(i);

            if (payee.getPyeName().contains(searchedPhrase)) {
                filteredPayees.add(payee);
            }

        }
        return filteredPayees;
    }

    public static ArrayList<Bank> filterBank(String searchedPhrase, ArrayList<Bank> itemsToSearchIn) {

        ArrayList<Bank> filteredBanks = new ArrayList<>();
        for (int i = 0; i < itemsToSearchIn.size(); i++) {
            Bank bank = itemsToSearchIn.get(i);

            if (bank.getBnkTitle().contains(searchedPhrase)) {
                filteredBanks.add(bank);
            }

        }
        return filteredBanks;
    }

    public static ArrayList<Account> filterAccount(String searchedPhrase, ArrayList<Account> itemsToSearchIn) {

        ArrayList<Account> filteredAccounts = new ArrayList<>();
        for (int i = 0; i < itemsToSearchIn.size(); i++) {
            Account account = itemsToSearchIn.get(i);

            if (account.getAcnName().contains(searchedPhrase)) {
                filteredAccounts.add(account);
            }

        }
        return filteredAccounts;
    }

    public static AnimationSet fadeInAndFadeOut() {

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //fade in
        fadeIn.setDuration(1000);

        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //fade out
        fadeOut.setStartOffset(1000);
        fadeOut.setDuration(1000);

        AnimationSet animation = new AnimationSet(false); //change to false
        animation.addAnimation(fadeIn);
        animation.addAnimation(fadeOut);

        return animation;
    }

    public static Bitmap getImageFromFilePath(String destination) {

        Bitmap resBitmap = null;

        File imageFile = new File(destination);
        if (imageFile.exists())
            resBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

        return resBitmap;
    }
}
