package fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.taxiapps.dakhlokharj.R;

import adapters.ExpensesAndIncomesListAdapter;
import model.EnumsAndConstants;
import model.Transaction;

/**
 * Created by Parsa on 2017-11-29.
 */

public class ExpensesAndIncomes extends Fragment {

    private ExpandableListView expandableListView;
    private ExpensesAndIncomesListAdapter mAdapter = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static ExpensesAndIncomes newInstance(long[] month, int filterValue, EnumsAndConstants.TransactionTypes types) {

        Bundle args = new Bundle();
        args.putLongArray("month", month);
        args.putInt("filterType", filterValue);
        args.putInt("transactionType", types.value());

        ExpensesAndIncomes fragment = new ExpensesAndIncomes();
        fragment.setArguments(args);

        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view;
        long[] monthInterval = getArguments().getLongArray("month");
        int filter = getArguments().getInt("filterType");
        int transactionType = getArguments().getInt("transactionType");

        EnumsAndConstants.TransactionTypes type;
        if (transactionType == EnumsAndConstants.TransactionTypes.Expense.value())
            type = EnumsAndConstants.TransactionTypes.Expense;
        else
            type = EnumsAndConstants.TransactionTypes.Income;

        if (Transaction.getAll(String.valueOf(monthInterval[0]), String.valueOf(monthInterval[1]), type).size() == 0)
            view = inflater.inflate(R.layout.frg_empty, null);
        else {
            view = inflater.inflate(R.layout.frg_expense_or_income, null);

            expandableListView = view.findViewById(R.id.expenses_or_income_list_activity_expandable_listview);

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;

            mAdapter = new ExpensesAndIncomesListAdapter(getActivity(), monthInterval, filter, type);

            expandableListView.setIndicatorBoundsRelative(width - GetDipsFromPixel(30), width - GetDipsFromPixel(5));
            expandableListView.setAdapter(mAdapter);

        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // expanding all groups
        if (mAdapter != null) {

            if (mAdapter.getGroupCount() > 0)
                for (int i = 0; i < mAdapter.getGroupCount(); i++) {
                    expandableListView.expandGroup(i);
                }
        }
    }

    private int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

}