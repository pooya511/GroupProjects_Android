package fragments;


import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aigestudio.wheelpicker.WheelPicker;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class SelectBySelectingFragment extends Fragment {

    private View fragmentLayout;
    private static WheelPicker day, month, year;

    private static int resDay, resMonth, resYear;

    public static List<String> monthDates, esfandDays, firstSixMonthDayDates, secondSixMonthDayDates, yearDates, dayDates;

    public static SelectBySelectingFragment newInstance(long currentDate) {

        Bundle args = new Bundle();
        args.putLong("currentDate", currentDate);
        SelectBySelectingFragment fragment = new SelectBySelectingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        long currentDate = getArguments().getLong("currentDate");

        fragmentLayout = inflater.inflate(R.layout.frg_select_by_selecting, container, false);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yekan.ttf");

        day = fragmentLayout.findViewById(R.id.frg_select_by_selecting_day);
        day.setTypeface(typeface);
        month = fragmentLayout.findViewById(R.id.frg_select_by_selecting_month);
        month.setTypeface(typeface);
        year = fragmentLayout.findViewById(R.id.frg_select_by_selecting_year);
        year.setTypeface(typeface);

        day.setData(dayDates);
        month.setData(monthDates);
        year.setData(yearDates);

        setDate(currentDate);

        if (month.getCurrentItemPosition() <= 5) {
            dayDates = firstSixMonthDayDates;
            day.setData(dayDates);
        } else if (month.getCurrentItemPosition() >= 6 && month.getCurrentItemPosition() < 11) {
            dayDates = secondSixMonthDayDates;
            day.setData(dayDates);
        } else if (month.getCurrentItemPosition() == 11) {
            dayDates = esfandDays;
            day.setData(dayDates);
        }

        month.setOnWheelChangeListener(new WheelPicker.OnWheelChangeListener() {
            @Override
            public void onWheelScrolled(int offset) {

            }

            @Override
            public void onWheelSelected(int position) {

                if (position <= 5) {
                    if (day.getCurrentItemPosition() + 2 == firstSixMonthDayDates.size())
                        if (dayDates.size() == 30) {
                            dayDates = firstSixMonthDayDates;
                        }
                    day.setData(dayDates);
                } else if (position >= 6 && position < 11) {
                    dayDates = secondSixMonthDayDates;
                    day.setData(dayDates);
                } else if (position == 11) {
                    dayDates = esfandDays;
                    day.setData(dayDates);
                }

            }

            @Override
            public void onWheelScrollStateChanged(int state) {

            }
        });

        day.setOnItemSelectedListener((picker, data, position) -> resDay = Integer.parseInt(PublicModules.toEnglishDigit(dayDates.get(position))));

        month.setOnItemSelectedListener((picker, data, position) -> {

            switch (String.valueOf(data)) {

                case "فروردین":
                    resMonth = 1;
                    break;

                case "اردیبهشت":
                    resMonth = 2;
                    break;

                case "خرداد":
                    resMonth = 3;
                    break;

                case "تیر":
                    resMonth = 4;
                    break;

                case "مرداد":
                    resMonth = 5;
                    break;

                case "شهریور":
                    resMonth = 6;
                    break;

                case "مهر":
                    resMonth = 7;
                    break;

                case "آبان":
                    resMonth = 8;
                    break;

                case "آذر":
                    resMonth = 9;
                    break;

                case "دی":
                    resMonth = 10;
                    break;

                case "بهمن":
                    resMonth = 11;
                    break;

                case "اسفند":
                    resMonth = 12;
                    break;

            }
        });

        year.setOnItemSelectedListener((picker, data, position) -> resYear = Integer.parseInt(PublicModules.toEnglishDigit(yearDates.get(position))));

        return fragmentLayout;
    }

    public static void setDate(long targetDate) {

        PersianDate persianCalendar = new PersianDate(targetDate);
        PersianDateFormat formatter = new PersianDateFormat("F");

        resDay = persianCalendar.getShDay();
        resMonth = persianCalendar.getShMonth(); // persianCalendar e oskole . tof tu kale mohammad amin ba in librarish . dard o bala sina zamani bokhore tu saresh
        resYear = persianCalendar.getShYear();

        // day
        for (int i = 0; i < dayDates.size(); i++) {
            if (PublicModules.toEnglishDigit(dayDates.get(i)).equals(String.valueOf(resDay))) {
                day.setSelectedItemPosition(i);
                resDay = Integer.parseInt(PublicModules.toEnglishDigit(dayDates.get(i)));
                break;
            }
        }

        // month
        for (int i = 0; i < monthDates.size(); i++) {
            if (monthDates.get(i).equals(formatter.format(persianCalendar).replace("اردی\u200Cبهشت", "اردیبهشت"))) {
                month.setSelectedItemPosition(i);
                resMonth = i + 1;
                break;
            }
        }

        // year
        for (int i = 0; i < yearDates.size(); i++) {
            if (PublicModules.toEnglishDigit(yearDates.get(i)).equals(String.valueOf(resYear))) {
                year.setSelectedItemPosition(i);
                resYear = Integer.parseInt(PublicModules.toEnglishDigit(yearDates.get(i)));
                break;
            }
        }

    }

    public static PersianDate getDate() {

        resDay = day.getCurrentItemPosition() + 1;

        PersianDate persianCalendar = new PersianDate();
        persianCalendar.setShYear(resYear);
        persianCalendar.setShDay(resDay);
        persianCalendar.setShMonth(resMonth);

//        Log.i("res getDate()", String.valueOf(resDay) + " " + String.valueOf(resMonth) + " " + String.valueOf(resYear));

        return persianCalendar;
    }

}
