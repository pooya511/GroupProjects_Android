package fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.AddAndEditExpensesAndIncomesActivity;
import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.MainActivity;
import com.taxiapps.dakhlokharj.R;
import com.taxiapps.dakhlokharj.SettingActivity;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import model.Currency;
import model.EnumsAndConstants;
import model.GroupDetail;
import model.Settings;
import model.Transaction;
import modules.PublicModules;
import utils.AppModules;

public class MostUsedItem extends Fragment {

    private ArrayList<HashMap<String, Object>> top3Data;

    private TextView rightIcon, leftIcon, centerIcon;
    private TextView rightName, leftName, centerName;
    private TextView rightUsedTimes, leftUsedTimes, centerUsedTimes;
    private LinearLayout leftItem, centerItem, rightItem;
    public boolean isLimited = true;
    private boolean hasEnoughData;

    public MostUsedItem() {
        top3Data = Transaction.getTop3Detail();
        if (top3Data.size() < 3)
            hasEnoughData = false;
        else
            hasEnoughData = true;
    }

    private static MostUsedItem instance;

    public static MostUsedItem getInstance() {
        if (instance == null) {
            instance = new MostUsedItem();
            return instance;
        }
        return instance;
    }

    public static MostUsedItem initInstance() {
        return getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        if (isLimited) {
            view = inflater.inflate(R.layout.frg_widget_limited, container, false);
            ImageView imageView = view.findViewById(R.id.frg_widget_limited_image_view);
            imageView.setBackground(getResources().getDrawable(R.drawable.img_most_used_limited));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                    String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                    Payment.newInstance(R.drawable.ic_dakhlokharj,
                            EnumsAndConstants.APP_ID,
                            EnumsAndConstants.APP_SOURCE,
                            EnumsAndConstants.APP_VERSION,
                            "دخل و خرج",
                            Application.currentBase64,
                            "مزایای نسخه کامل",
                            limitationText,
                            userNumber,
                            EnumsAndConstants.DEVICE_ID,
                            EnumsAndConstants.SERVER_URL,
                            EnumsAndConstants.GATEWAY_URL,
                            new TX_License.SetLicenseCallback() {
                                @Override
                                public void call(long expireDate, String license) {

                                    Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                    Settings.setSetting(Settings.LICENSE_DATA, license);

                                    try {
                                        JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                        Settings.setSetting(Settings.APP_AMOUNT , jsonObject.getString("amount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                    MainActivity.getInstance().updateFragment(false);
                                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                    if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                        Transaction.appPurchased();

                                    MainActivity.getInstance().reloadMainData();
//
                                    if (SettingActivity.getInstance() != null)
                                        SettingActivity.getInstance().premiumUIHandling(false);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            },
                            new Payment.UserNameCallBack() {
                                @Override
                                public void getUserName(String username) {
                                    Settings.setSetting(Settings.TX_USER_NUMBER, username);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            }
                    ).show(getActivity().getFragmentManager(), "tx_payment");
                }
            });
        } else {

            if (hasEnoughData) {
                view = inflater.inflate(R.layout.frg_most_used_item, container, false);

                final GroupDetail rightGroupDetail = new GroupDetail(Integer.valueOf(String.valueOf(top3Data.get(0).get(GroupDetail.GroupDetailCol.id.value()))), Integer.valueOf(String.valueOf(top3Data.get(0).get(GroupDetail.GroupDetailCol.lstID.value()))), String.valueOf(top3Data.get(0).get(GroupDetail.GroupDetailCol.lsdName.value())), String.valueOf(top3Data.get(0).get(GroupDetail.GroupDetailCol.lsdIcon.value())), String.valueOf(top3Data.get(0).get(GroupDetail.GroupDetailCol.lsdColor.value())));
                final GroupDetail centerGroupDetail = new GroupDetail(Integer.valueOf(String.valueOf(top3Data.get(1).get(GroupDetail.GroupDetailCol.id.value()))), Integer.valueOf(String.valueOf(top3Data.get(1).get(GroupDetail.GroupDetailCol.lstID.value()))), String.valueOf(top3Data.get(1).get(GroupDetail.GroupDetailCol.lsdName.value())), String.valueOf(top3Data.get(1).get(GroupDetail.GroupDetailCol.lsdIcon.value())), String.valueOf(top3Data.get(1).get(GroupDetail.GroupDetailCol.lsdColor.value())));
                final GroupDetail leftGroupDetail = new GroupDetail(Integer.valueOf(String.valueOf(top3Data.get(2).get(GroupDetail.GroupDetailCol.id.value()))), Integer.valueOf(String.valueOf(top3Data.get(2).get(GroupDetail.GroupDetailCol.lstID.value()))), String.valueOf(top3Data.get(2).get(GroupDetail.GroupDetailCol.lsdName.value())), String.valueOf(top3Data.get(2).get(GroupDetail.GroupDetailCol.lsdIcon.value())), String.valueOf(top3Data.get(2).get(GroupDetail.GroupDetailCol.lsdColor.value())));

                rightIcon = view.findViewById(R.id.frg_most_used_item_right_item_icon);
                centerIcon = view.findViewById(R.id.frg_most_used_item_center_item_icon);
                leftIcon = view.findViewById(R.id.frg_most_used_item_left_item_icon);

                leftItem = view.findViewById(R.id.frg_most_used_item_left_item);
                centerItem = view.findViewById(R.id.frg_most_used_item_center_item);
                rightItem = view.findViewById(R.id.frg_most_used_item_right_item);

                rightIcon = AppModules.setTypeFace(getContext(), rightIcon, "fontawesome_webfont.ttf");
                centerIcon = AppModules.setTypeFace(getContext(), centerIcon, "fontawesome_webfont.ttf");
                leftIcon = AppModules.setTypeFace(getContext(), leftIcon, "fontawesome_webfont.ttf");

                rightIcon.setTextColor(Color.parseColor(rightGroupDetail.getLsdColor()));
                centerIcon.setTextColor(Color.parseColor(centerGroupDetail.getLsdColor()));
                leftIcon.setTextColor(Color.parseColor(String.valueOf(leftGroupDetail.getLsdColor())));

                rightName = view.findViewById(R.id.frg_most_used_item_right_item_name);
                centerName = view.findViewById(R.id.frg_most_used_item_center_item_name);
                leftName = view.findViewById(R.id.frg_most_used_item_left_item_name);

                rightUsedTimes = view.findViewById(R.id.frg_most_used_item_right_item_times);
                centerUsedTimes = view.findViewById(R.id.frg_most_used_item_center_item_times);
                leftUsedTimes = view.findViewById(R.id.frg_most_used_item_left_item_times);

                rightIcon.setText(AppModules.convertIconFromString(getContext(), rightGroupDetail.getLsdIcon()));
                centerIcon.setText(AppModules.convertIconFromString(getContext(), centerGroupDetail.getLsdIcon()));
                leftIcon.setText(AppModules.convertIconFromString(getContext(), leftGroupDetail.getLsdIcon()));

                rightName.setText(rightGroupDetail.getLsdName());
                centerName.setText(centerGroupDetail.getLsdName());
                leftName.setText(leftGroupDetail.getLsdName());

                rightUsedTimes.setText(String.valueOf(top3Data.get(0).get("_COUNT")));
                centerUsedTimes.setText(String.valueOf(top3Data.get(1).get("_COUNT")));
                leftUsedTimes.setText(String.valueOf(top3Data.get(2).get("_COUNT")));

                final Intent intent = new Intent(getActivity(), AddAndEditExpensesAndIncomesActivity.class);
                intent.putExtra("Type", AddAndEditExpensesAndIncomesActivity.IntentTypes.Widget.value());

                leftItem.setOnClickListener(v -> {
                    intent.putExtra("groupDetailID", leftGroupDetail.getID()); // baray enteghal groupdetail be safhe AddAndEditExpensesAndIncomesActivity az khodesh nemitunim estefade konim (dastane parcelabel e ast) pas az ID sh estefadeh mikonim
                    intent.putExtra("trnType", String.valueOf(top3Data.get(2).get(Transaction.TransactionCol.trnType.value())));
                    startActivity(intent);
                });

                centerItem.setOnClickListener(v -> {
                    intent.putExtra("groupDetailID", centerGroupDetail.getID()); // comment balayi
                    intent.putExtra("trnType", String.valueOf(top3Data.get(1).get(Transaction.TransactionCol.trnType.value())));
                    startActivity(intent);
                });

                rightItem.setOnClickListener(v -> {
                    intent.putExtra("groupDetailID", rightGroupDetail.getID()); // comment balayi
                    intent.putExtra("trnType", String.valueOf(top3Data.get(0).get(Transaction.TransactionCol.trnType.value())));
                    startActivity(intent);
                });
            } else {
                view = inflater.inflate(R.layout.frg_widget_limited, container, false);
                ImageView imageView = view.findViewById(R.id.frg_widget_limited_image_view);
                imageView.setBackground(getResources().getDrawable(R.drawable.img_most_used_blure_no_data));
            }
        }


        return view;
    }

}
