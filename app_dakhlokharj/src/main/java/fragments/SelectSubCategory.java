package fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.R;

import adapters.SubCategoryAdapter;
import model.Group;
import model.GroupDetail;
import utils.AppModules;

/**
 * Created by Parsa on 2017-12-13.
 */

public class SelectSubCategory extends Fragment {

    private Group group;
    private SelectedSubCategoryCallBack categoryCallBack;
    public static TextView edit;
    public static boolean isEditMode;

    public interface SelectedSubCategoryCallBack extends Parcelable {
        void call(GroupDetail groupDetail);
    }

    public static SelectSubCategory newInstance(Group group, SelectedSubCategoryCallBack callBack) {
        Bundle args = new Bundle();
        args.putParcelable("Group", group);
        args.putParcelable("CallBack", callBack);
        SelectSubCategory fragment = new SelectSubCategory();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        group = getArguments().getParcelable("Group");
        categoryCallBack = getArguments().getParcelable("CallBack");

        final View layout = inflater.inflate(R.layout.pop_sub_categories, null);

        if (!group.getLstName().equals("new")) {

            TextView categoryName, iconCategory;
            final ImageView back, clearImg;
            final EditText searchEditText;

            final RecyclerView subCategoriesList = layout.findViewById(R.id.pop_sub_categories_list_view);
            subCategoriesList.setLayoutManager(new LinearLayoutManager(getActivity()));
            subCategoriesList.setHasFixedSize(true);

            categoryName = layout.findViewById(R.id.pop_sub_categories_category_name);
            iconCategory = layout.findViewById(R.id.pop_sub_categories_category_icon);
            edit = layout.findViewById(R.id.pop_sub_categories_edit_text_view);
            back = layout.findViewById(R.id.pop_sub_categories_back);
            searchEditText = layout.findViewById(R.id.pop_sub_categories_search_edittext);
            clearImg = layout.findViewById(R.id.pop_sub_categories_clear_btn);

            searchEditText.requestFocus();

            categoryName.setText(group.getLstName());

            iconCategory = AppModules.setTypeFace(getActivity(), iconCategory, "fontawesome_webfont.ttf");
            iconCategory.setText(AppModules.convertIconFromString(getActivity(), group.getLstIcon()));
            iconCategory.setTextColor(Color.parseColor(group.getLstColor()));

            subCategoriesList.setAdapter(new SubCategoryAdapter(getActivity(), group.getID(), categoryCallBack, null, isEditMode));

            TextWatcher textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (s.length() > 2) {
                        SubCategoryAdapter.getInstance().setFiltredData(String.valueOf(s));
                    }
                    if (s.length() == 0) {
                        SubCategoryAdapter.getInstance().reloadData();
                    }

                    if (s.length() > 0) {
                        clearImg.setVisibility(View.VISIBLE);
                    } else {
                        clearImg.setVisibility(View.GONE);
                    }

                }
            };

            searchEditText.addTextChangedListener(textWatcher);

            View.OnClickListener dialogListeners = v -> {

                if (v.equals(edit))
                    if (edit.getText().toString().equals("ویرایش") && !SubCategoryAdapter.getInstance().showMinusIcon) {
                        SubCategoryAdapter.getInstance().editStart();
                        isEditMode = true;
                    } else {
                        SubCategoryAdapter.getInstance().editEnd();
                        isEditMode = false;
                    }


                if (v.equals(back)) {
                    getFragmentManager().popBackStack();
                    SubCategoryAdapter.getInstance().editEnd();
                }

                if (v.equals(clearImg)) {
                    searchEditText.setText("");
                }


            };

            edit.setOnClickListener(dialogListeners);
            back.setOnClickListener(dialogListeners);
            clearImg.setOnClickListener(dialogListeners);

        }


        return layout;
    }

}