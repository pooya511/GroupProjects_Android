package fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.taxiapps.dakhlokharj.R;

import adapters.CategoryAdapter;
import model.EnumsAndConstants;
import model.Group;

/**
 * Created by Parsa on 2017-12-13.
 */

public class SelectCategory extends Fragment {

    private EnumsAndConstants.TransactionTypes type;
    private SelectedCategoryCallBack selectedCategoryCallBack;
    public static TextView edit;
    public static boolean isEditMode;

    public interface SelectedCategoryCallBack extends Parcelable {
        void call(Group group);
    }

    public static SelectCategory newInstance(EnumsAndConstants.TransactionTypes type, SelectedCategoryCallBack categoryCallBack) {
        Bundle args = new Bundle();
        args.putInt("Type", type.value());
        args.putParcelable("CallBack", categoryCallBack);
        SelectCategory fragment = new SelectCategory();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        type = getArguments().getInt("Type") == 1 ? EnumsAndConstants.TransactionTypes.Expense : EnumsAndConstants.TransactionTypes.Income;
        selectedCategoryCallBack = getArguments().getParcelable("CallBack");

        View layout = inflater.inflate(R.layout.pop_categories, null);

        final RecyclerView listView = layout.findViewById(R.id.pop_categories_list_view);
        listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        listView.setHasFixedSize(true);
        final ImageView clearImg = layout.findViewById(R.id.pop_categories_clear_btn);
        final EditText searchEditText = layout.findViewById(R.id.pop_categories_search_edittext);
        edit = layout.findViewById(R.id.pop_categories_edit_text);

        listView.setAdapter(new CategoryAdapter(getActivity(), type, selectedCategoryCallBack, null, isEditMode));
//        ArrayList<Group> allGroups = Group.getAll(type.value());

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 2) {
//                    ArrayList<Group> filteredGroups = AppModules.filterGroup(String.valueOf(s), allGroups);
//                    listView.setAdapter(new CategoryAdapter(getActivity(), type, selectedCategoryCallBack, filteredGroups, isEditMode));
//                    CategoryAdapter.getInstance().notifyDataSetChanged();
                    CategoryAdapter.getInstance().setFiltredData(String.valueOf(s));
                }
                if (s.length() == 0) {
                    CategoryAdapter.getInstance().reloadData();
                }
                if (s.length() > 0) {
                    clearImg.setVisibility(View.VISIBLE);
                } else {
                    clearImg.setVisibility(View.GONE);
                }


            }
        };

        searchEditText.addTextChangedListener(textWatcher);

        View.OnClickListener dialogListeners = v -> {

            if (v.equals(edit)) {
                if (edit.getText().toString().equals("ویرایش") && !CategoryAdapter.getInstance().showMinusIcon) { // handel kardan e button edit va save
                    CategoryAdapter.getInstance().editStart();
                    isEditMode = true;
                } else {
                    CategoryAdapter.getInstance().editEnd();
                    isEditMode = false;
                }

            }
            if (v.equals(clearImg)) {
                searchEditText.setText("");
                searchEditText.clearFocus();
            }

        };

        edit.setOnClickListener(dialogListeners);
        clearImg.setOnClickListener(dialogListeners);

        return layout;
    }

}