package fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.PayeeReportSearchActivity;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;

import adapters.PayeeReportAdapter;
import model.Reports;

public class PayeeReportFragment extends Fragment {

    private TextView payeeName;
    private RecyclerView listView;
    private LinearLayout sharingLayout;

    public static PayeeReportFragment newInstance(PayeeReportSearchActivity.ReturnedItem item) {

        Bundle args = new Bundle();
        args.putParcelable("Item", item);
        PayeeReportFragment fragment = new PayeeReportFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view;
        TextView nothingToShow;
        ImageView fragmentEmptyImg;

        ArrayList<ArrayList<String>> resData;

        sharingLayout = getActivity().findViewById(R.id.activity_report_payee_sharing_layout);

        if (getArguments().getParcelable("Item") != null) {

            PayeeReportSearchActivity.ReturnedItem item = getArguments().getParcelable("Item");

            int transactionType = 0;
            if (item.isExpense)
                transactionType = 1;
            if (item.isIncome)
                transactionType = 2;
            if (item.isExpense && item.isIncome)
                transactionType = 0;

            resData = Reports.ReportPayee.byPayee(item.payeeId, item.dateFrom, item.dateTo, transactionType);

            if (resData.size() != 0) {

                view = inflater.inflate(R.layout.frg_payee_report_expense_list, null);

                payeeName = view.findViewById(R.id.frg_payee_report_expense_list_payee_name_text);
                listView = view.findViewById(R.id.frg_payee_report_expense_list_list_view);

                payeeName.setText(item.payeeName);
                sharingLayout.setVisibility(View.VISIBLE);
                listView.setLayoutManager(new LinearLayoutManager(getActivity()));
                listView.setAdapter(new PayeeReportAdapter(getActivity(), resData));

            } else {

                view = inflater.inflate(R.layout.frg_empty, null);
                nothingToShow = view.findViewById(R.id.frg_empty_text);
                fragmentEmptyImg = view.findViewById(R.id.frg_empty_img);

                sharingLayout.setVisibility(View.INVISIBLE);
                nothingToShow.setText("هیچ هزینه ای وجود ندارد!");
                fragmentEmptyImg.setImageResource(R.drawable.ic_ribbon);

            }

        } else {

            view = inflater.inflate(R.layout.frg_empty, null);
            nothingToShow = view.findViewById(R.id.frg_empty_text);
            fragmentEmptyImg = view.findViewById(R.id.frg_empty_img);

            sharingLayout.setVisibility(View.INVISIBLE);
            nothingToShow.setText("هیچ طرف حسابی انتخاب نکردید");
            fragmentEmptyImg.setImageResource(R.drawable.ic_ribbon);

        }

        return view;
    }


}