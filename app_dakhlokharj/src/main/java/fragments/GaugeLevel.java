package fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.MainActivity;
import com.taxiapps.dakhlokharj.R;
import com.taxiapps.dakhlokharj.SettingActivity;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import model.Currency;
import model.EnumsAndConstants;
import model.Settings;
import model.Transaction;
import modules.PublicModules;

/**
 * Created by Parsa on 2017-11-13.
 */

public class GaugeLevel extends android.support.v4.app.Fragment {

    private ImageView gaugeHand;
    private TextView percentTextView, remainingTextView, descriptionTextView;

    private double percent;
    private double remaining;

    public boolean isLimited = true;

    private static GaugeLevel instance;
    private double expense;
    private double income;

    private boolean hasEnoughData;

    public static GaugeLevel getInstance() {
        if (instance == null) {
            instance = new GaugeLevel();
            return instance;
        }
        return instance;
    }

    public static GaugeLevel initInstance() {
        return getInstance();
    }

    private void initializeGauge(final double percent) {

        if (!getInstance().isLimited) {
            if (!Double.isNaN(percent)) {
                if (percent > 0.5) {
                    final double x1 = 270 * 0.5;
                    final double x2 = 270 * (percent - 0.5);
                    gaugeHand.animate().rotationBy((float) x1).setDuration((long) (1000 * (0.5 / percent))).setInterpolator(new LinearInterpolator()).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            gaugeHand.animate().rotationBy((float) x2).setDuration((long) (1500 * ((percent - 0.5) / percent))).start();
                        }
                    }).start();

                } else if (percent < 0.5) {
                    double x = 270 * percent;
                    gaugeHand.animate().rotationBy((float) x).setDuration(1000).start();
                } else if (percent == 0) {

                }
            }
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        long[] thisMonth = PublicModules.getMonthInterval(0);

        expense = Double.valueOf(Transaction.getSumAll(String.valueOf(thisMonth[0]), String.valueOf(thisMonth[1]), EnumsAndConstants.TransactionTypes.Expense));
        income = Double.valueOf(Transaction.getSumAll(String.valueOf(thisMonth[0]), String.valueOf(thisMonth[1]), EnumsAndConstants.TransactionTypes.Income));

        if (expense > 0)
            hasEnoughData = true;
        else
            hasEnoughData = false;

        View v;
        if (getInstance().isLimited) {
            v = inflater.inflate(R.layout.frg_widget_limited, container, false);
            ImageView imageView = v.findViewById(R.id.frg_widget_limited_image_view);
            imageView.setBackground(getResources().getDrawable(R.drawable.img_gauge_limited));
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                    String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                    Payment.newInstance(R.drawable.ic_dakhlokharj,
                            EnumsAndConstants.APP_ID,
                            EnumsAndConstants.APP_SOURCE,
                            EnumsAndConstants.APP_VERSION,
                            "دخل و خرج",
                            Application.currentBase64,
                            "مزایای نسخه کامل",
                            limitationText,
                            userNumber,
                            EnumsAndConstants.DEVICE_ID,
                            EnumsAndConstants.SERVER_URL,
                            EnumsAndConstants.GATEWAY_URL,
                            new TX_License.SetLicenseCallback() {
                                @Override
                                public void call(long expireDate, String license) {

                                    Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                    Settings.setSetting(Settings.LICENSE_DATA, license);

                                    try {
                                        JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                        Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                    MainActivity.getInstance().updateFragment(false);
                                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                    if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                        Transaction.appPurchased();
                                    MainActivity.getInstance().reloadMainData();
//
                                    if (SettingActivity.getInstance() != null)
                                        SettingActivity.getInstance().premiumUIHandling(false);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            },
                            new Payment.UserNameCallBack() {
                                @Override
                                public void getUserName(String username) {
                                    Settings.setSetting(Settings.TX_USER_NUMBER, username);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            }
                    ).show(getActivity().getFragmentManager(), "tx_payment");
                }
            });
        } else {

            if (getInstance().hasEnoughData) {
                v = inflater.inflate(R.layout.frg_gauge_level, container, false);

                gaugeHand = v.findViewById(R.id.frg_gauge_gauge_hand);
                percentTextView = v.findViewById(R.id.frg_gauge_percent_text_view);
                remainingTextView = v.findViewById(R.id.frg_gauge_remaining_text_view);
                descriptionTextView = v.findViewById(R.id.frg_gauge_description_text_view);

                percent = expense / income;
                remaining = expense - income;

                percent = Math.max(0, percent);
                percent = Math.min(1, percent);

                Typeface typeface = Application.amountTypeFace(getActivity());
                remainingTextView.setTypeface(typeface);

                if (expense == 0 && income == 0) {
                    remainingTextView.setText("هزینه و درآمدی در ماه جاری ندارید");
                    percentTextView.setText("");
                } else if (expense / income > 1) {
                    descriptionTextView.setTextColor(Color.RED);
                    remainingTextView.setText("مانده ماه: " + Currency.CurrencyString.getCurrencyString(remaining, Currency.CurrencyMode.Short).separated + " - ");

                    remainingTextView.setTextColor(Color.RED);
                    percentTextView.setText("+ 100 %");
                } else {
                    percentTextView.setText(PublicModules.toPersianDigit(Currency.CurrencyString.getCurrencyString(percent * 100, null).separated) + " %");
                    remainingTextView.setText(Currency.CurrencyString.getCurrencyString(Math.abs(remaining), Currency.CurrencyMode.Short).separated);
                    remainingTextView.setTextColor(getResources().getColor(R.color.green));
                }
            } else {
                v = inflater.inflate(R.layout.frg_widget_limited, container, false);
                ImageView imageView = v.findViewById(R.id.frg_widget_limited_image_view);
                imageView.setBackground(getResources().getDrawable(R.drawable.img_gauge_blure_no_data));
            }

        }

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (gaugeHand != null)
            if (!getInstance().isLimited && getInstance().hasEnoughData) {
                initializeGauge(percent);
            }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gaugeHand != null)
            if (!getInstance().isLimited && getInstance().hasEnoughData) {
                percent = 0;
                initializeGauge(percent);
            }
    }

}
