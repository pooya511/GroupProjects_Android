package fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.taxiapps.dakhlokharj.Application;
import com.taxiapps.dakhlokharj.MainActivity;
import com.taxiapps.dakhlokharj.R;
import com.taxiapps.dakhlokharj.SettingActivity;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.ChartConfig;
import model.ChartDataSet;
import model.Currency;
import model.EnumsAndConstants;
import model.Settings;
import model.Transaction;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import utils.AppModules;

public class Chart extends Fragment {

    private LineChart lineChart;
    private ChartConfig chartConfig;
    private boolean isLimited = true;

    public boolean isLimited() {
        return isLimited;
    }

    public void setLimited(boolean limited) {
        isLimited = limited;
    }

    public Chart() {
        // Required empty public constructor
    }

    public static Chart initInstance(ChartConfig chartConfig) {

        Bundle bundle = new Bundle(2);
        bundle.putParcelable("chartConfig", chartConfig);
        Chart chart = new Chart();
        chart.setArguments(bundle);

        return chart;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        chartConfig = (ChartConfig) getArguments().get("chartConfig");

        View view;
        if (isLimited()) {
            view = inflater.inflate(R.layout.frg_widget_limited, container, false);
            ImageView imageView = view.findViewById(R.id.frg_widget_limited_image_view);
            imageView.setBackground(getResources().getDrawable(R.drawable.img_chart_limited));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    TXDialogs txDialogs = new TXDialogs();
//                    txDialogs.makeTXLimitDialog(getActivity(), "مزایای نسخه کامل").show();
                    String limitationText = "-ورود نامحدود آیتم های هزینه و درآمد \n -پشتیبانگیری از اطلاعات\n -امکان ثبت بودجه ثابت\n -نسخه کامل محدودیت زمانی ندارد و قابل انتقال به گوشی دیگر می باشد";
                    String userNumber = Settings.getSetting(Settings.TX_USER_NUMBER).equals("") ? "" : Settings.getSetting(Settings.TX_USER_NUMBER);
                    Payment.newInstance(R.drawable.ic_dakhlokharj,
                            EnumsAndConstants.APP_ID,
                            EnumsAndConstants.APP_SOURCE,
                            EnumsAndConstants.APP_VERSION,
                            "دخل و خرج",
                            Application.currentBase64,
                            "مزایای نسخه کامل",
                            limitationText,
                            userNumber,
                            EnumsAndConstants.DEVICE_ID,
                            EnumsAndConstants.SERVER_URL,
                            EnumsAndConstants.GATEWAY_URL,
                            new TX_License.SetLicenseCallback() {
                                @Override
                                public void call(long expireDate, String license) {

                                    Settings.setSetting(Settings.LICENSE_EXPIRE, String.valueOf(expireDate));
                                    Settings.setSetting(Settings.LICENSE_DATA, license);

                                    try {
                                        JSONObject jsonObject = new JSONObject(PublicModules.makeJsonString(license));
                                        Settings.setSetting(Settings.APP_AMOUNT, jsonObject.getString("amount"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    MainActivity.getInstance().buyBtn.setVisibility(View.GONE);
                                    MainActivity.getInstance().updateFragment(false);
                                    MainActivity.getInstance().pageAdapter.notifyDataSetChanged();

                                    if (Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("تومان") || Currency.CurrencyString.getCurrency(Currency.CurrencyMode.Full).equals("ریال"))
                                        Transaction.appPurchased();
                                    MainActivity.getInstance().reloadMainData();
//
                                    if (SettingActivity.getInstance() != null)
                                        SettingActivity.getInstance().premiumUIHandling(false);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            },
                            new Payment.UserNameCallBack() {
                                @Override
                                public void getUserName(String username) {
                                    Settings.setSetting(Settings.TX_USER_NUMBER, username);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            }
                    ).show(getActivity().getFragmentManager() , "tx_payment");
                }
            });
        } else {

            if (chartConfig.isHasEnoughData()) {
                view = inflater.inflate(R.layout.frg_chart, container, false);

                lineChart = view.findViewById(R.id.chart);
                lineChart.getXAxis().setDrawGridLines(true);
                lineChart.getXAxis().setGridColor(Color.parseColor("#E2E2E2"));
                lineChart.getAxisLeft().setDrawGridLines(true);
                lineChart.getAxisLeft().setGridColor(Color.parseColor("#E2E2E2"));
                lineChart.getAxisRight().setDrawGridLines(false);
                lineChart.setBackgroundColor(Color.TRANSPARENT);
                lineChart.getXAxis().setLabelRotationAngle(chartConfig.getLabelRotationAngle());
                lineChart.getXAxis().setLabelCount(chartConfig.getBottomLabelCount(), true);
                lineChart.setExtraBottomOffset(-15);
                lineChart.getLegend().setYOffset(10);
                lineChart.setExtraLeftOffset(12);

                List<ILineDataSet> dataSets = new ArrayList<>();

                for (int i = 0; i < chartConfig.getEntries().size(); i++) {
                    dataSets.add(new ChartDataSet(chartConfig.getEntries().get(i), chartConfig.getLabels().get(i), chartConfig.getColors().get(i), chartConfig.isDrawCircles()));
                }

                LineData lineData = new LineData(dataSets);
                lineChart.setData(lineData);

                lineChart.getXAxis().setValueFormatter((value, axis) -> {
                    if (chartConfig.getAxisLabels() == null) {
                        PersianDate pCalendar = new PersianDate((long) value);
                        return PublicModules.timeStamp2String(pCalendar.getTime(), "yy MM");
                    } else
                        return chartConfig.getAxisLabels().get((int) value);
                });
                lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                lineChart.getAxisLeft().setDrawLabels(true);
                lineChart.getAxisLeft().setValueFormatter((value, axis) -> Currency.CurrencyString.getCurrencyString(value, Currency.CurrencyMode.Short).separated);
                lineChart.getAxisRight().setValueFormatter((value, axis) -> "  ");
                lineChart.getAxisRight().setDrawAxisLine(false);
                lineChart.getAxisLeft().setDrawAxisLine(false);
                lineChart.getXAxis().setDrawAxisLine(false);
                lineChart.getAxisLeft().setAxisMinimum(0.0f);
                lineChart.setDoubleTapToZoomEnabled(false);
                lineChart.getDescription().setText("");
                lineChart.getData().setValueTextColor(Color.parseColor("#626262"));

                Typeface typeface = Application.amountTypeFace(getActivity());
                lineChart.getAxisLeft().setTypeface(typeface);
                lineChart.getAxisLeft().setTextSize(9);
                lineChart.getXAxis().setTypeface(typeface);
                lineChart.getXAxis().setTextSize(9);
                lineChart.getLegend().setTypeface(typeface);
                lineChart.getLegend().setTextSize(9);
                lineChart.getAxisLeft().setLabelCount(chartConfig.getLeftLabelCount(), false);
            } else {
                view = inflater.inflate(R.layout.frg_widget_limited, container, false);
                ImageView imageView = view.findViewById(R.id.frg_widget_limited_image_view);
                imageView.setBackground(getResources().getDrawable(R.drawable.img_chart_blure_no_data));
            }
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments().getBoolean("limit") && lineChart != null) {
            lineChart.animate();
            lineChart.animateX(2000);
            lineChart.animateY(2000);
        }
    }
}