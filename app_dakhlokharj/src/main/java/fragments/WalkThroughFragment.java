package fragments;

import android.app.Fragment;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.R;

/**
 * Created by Parsa on 2018-01-31.
 */

public class WalkThroughFragment extends android.support.v4.app.Fragment {

    public static WalkThroughFragment newInstance(int resId, String description, boolean showButton) {
        Bundle args = new Bundle();
        args.putBoolean("showButton", showButton);
        args.putString("description", description);
        args.putInt("resId", resId);
        WalkThroughFragment fragment = new WalkThroughFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.frg_guid_walkthrough, null);

        ImageView imageView = view.findViewById(R.id.frg_guid_walkthrough_image);
        TextView description = view.findViewById(R.id.frg_guid_walkthrough_description);
        Button startApp = view.findViewById(R.id.frg_guid_walkthrough_start_app);

        imageView.setBackgroundResource(getArguments().getInt("resId"));
        description.setText(getArguments().getString("description"));

        if (getArguments().getBoolean("showButton")) {
            startApp.animate().alpha(1f).setDuration(2000);
        } else
            startApp.setAlpha(0);

        startApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); ++i) {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                getActivity().finish();
            }
        });

        return view;
    }
}
