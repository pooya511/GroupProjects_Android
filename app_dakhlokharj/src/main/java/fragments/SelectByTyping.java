package fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.taxiapps.dakhlokharj.R;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import model.EnumsAndConstants;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class SelectByTyping extends Fragment {

    private View fragmentLayout;
    private ImageView clearImage;
    private static EditText dateField;

    private static String date = "";

    public static SelectByTyping newInstance(long currentDate) {

        Bundle args = new Bundle();
        args.putLong("currentDate", currentDate);
        SelectByTyping fragment = new SelectByTyping();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        long currentDate = getArguments().getLong("currentDate");

        fragmentLayout = inflater.inflate(R.layout.frg_select_by_typing, null, false);

        clearImage = fragmentLayout.findViewById(R.id.frg_select_by_typing_clear_image_view);
        dateField = fragmentLayout.findViewById(R.id.frg_select_by_typing_date_edit_text);

        setDate(currentDate);

        clearImage.setOnClickListener(v -> {
            dateField.setText("13");
            dateField.setSelection(dateField.getText().toString().length());
        });

        MaskedTextChangedListener dateTextWatcher = new MaskedTextChangedListener("[0000]/[00]/[00]", false, dateField, null, (b, s) -> {

            String text = String.valueOf(s);

            // year validation
            if (text.length() == 2) {
                if (Integer.valueOf(text.substring(0, 2)) > 13 || Integer.valueOf(text.substring(0, 2)) < 13) {
                    dateField.setText(text.substring(0, 0));
                    dateField.append("13");
                }
            }

            // month validation
            if (text.length() == 6) {

                if (Integer.valueOf(text.substring(4, 6)) > 12) {

                    dateField.setText(text.substring(0, 4));
                    dateField.append("12");

                }

                if (Integer.valueOf(text.substring(4, 6)) == 0) {

                    dateField.setText(text.substring(0, 4));
                    dateField.append("01");

                }

            }

            // day validation
            if (text.length() == 8) {

                if (Integer.valueOf(text.substring(6, 8)) > 31) {

                    if (Integer.valueOf(text.substring(4, 6)) >= 7) {
                        if (Integer.valueOf(text.substring(4, 6)) == 12) {

                            dateField.setText(text.substring(0, 6));
                            dateField.append("29");

                        } else {

                            dateField.setText(text.substring(0, 6));
                            dateField.append("30");

                        }


                    } else {
                        dateField.setText(text.substring(0, 6));
                        dateField.append("31");
                    }

                }

            }

            dateField.setSelection(dateField.getText().toString().length());
        });


        dateField.setOnKeyListener((v, keyCode, event) -> {

            String userText = dateField.getText().toString();

            if (keyCode == KeyEvent.KEYCODE_DEL) {
                dateField.setText(userText.substring(0, userText.length()));
                dateField.setSelection(dateField.getText().toString().length());
            }
            return false;

        });

        dateField.addTextChangedListener(dateTextWatcher);
        dateField.setOnFocusChangeListener(dateTextWatcher);

        updateDate();

        return fragmentLayout;
    }

    public static void setDate(long targetDate) {

//        PersianCalendar pc = new PersianCalendar(targetDate);
        PersianDate persianDate = new PersianDate(targetDate);
        String res = new PersianDateFormat("Y/m/j").format(persianDate);
        dateField.setText(res);
        dateField.setSelection(dateField.getText().toString().length());

    }

    private static void updateDate() {
        date = dateField.getText().toString();
    }

    public static PersianDate getDate(Context context) {

        updateDate();

        if (date.length() != 0) {

            String[] dateArray = date.split("/");

            try {

                PersianDate pCalendar = new PersianDate();
                pCalendar.setShYear(Integer.valueOf(dateArray[0]));
                pCalendar.setShDay(Integer.valueOf(dateArray[2]));
                pCalendar.setShMonth(Integer.valueOf(dateArray[1]));

                if (PublicModules.dateIsValid(String.valueOf(pCalendar.getShYear()), String.valueOf(pCalendar.getShMonth()), String.valueOf(pCalendar.getShDay()))) {
                    return pCalendar;
                } else {
                    PublicDialogs.makeMessageDialog(context, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "تاریخ وارد شده معتبر نیست!", "", EnumsAndConstants.APP_SOURCE).show();
                    return null;
                }

            } catch (ArrayIndexOutOfBoundsException e) {

                PublicDialogs.makeMessageDialog(context, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "تاریخ وارد شده معتبر نیست!", "", EnumsAndConstants.APP_SOURCE).show();
                return null;
            }

        } else {
            PublicDialogs.makeMessageDialog(context, PublicDialogs.MessageDialogTypes.ERROR, "خطا", "تاریخی وارد نکردید!", "", EnumsAndConstants.APP_SOURCE).show();
            return null;
        }
    }

}