package fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aigestudio.wheelpicker.WheelPicker;
import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.List;

import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;
import utils.AppModules;

/**
 * Created by Parsa on 2017-11-01.
 */

public class SelectByDayFragment extends Fragment {

    private static View fragmentLayout;
    private static WheelPicker wheelDatePicker;

    public static List<String> stringDates;
    public static List<Long> dateList;
    public static long date;

    public static SelectByDayFragment newInstance(long currentDate) {

        Bundle args = new Bundle();
        args.putLong("currentDate", currentDate);

        SelectByDayFragment fragment = new SelectByDayFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        long currentDate = getArguments().getLong("currentDate");

        fragmentLayout = inflater.inflate(R.layout.frg_select_by_day, container, false);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yekan.ttf");

        wheelDatePicker = fragmentLayout.findViewById(R.id.wheel_date_picker);
        wheelDatePicker.setTypeface(typeface);
        wheelDatePicker.setData(stringDates);
        wheelDatePicker.setSelectedItemTextColor(Color.BLACK);
        wheelDatePicker.setOnItemSelectedListener((picker, data, position) -> date = dateList.get(position));

        setWheelDatePicker(currentDate);

        return fragmentLayout;
    }

    public static void setWheelDatePicker(long targetDate) {

        PersianDateFormat formatter = new PersianDateFormat("l j F Y");
        String formattedDate = formatter.format(new PersianDate(targetDate));

        for (int i = 0; i < stringDates.size(); i++) {
            if (formattedDate.equals(PublicModules.toEnglishDigit(stringDates.get(i)))) {
                wheelDatePicker.setSelectedItemPosition(i);
                date = dateList.get(i);
                break;
            }
        }


    }

    public static PersianDate getDate() {
        return new PersianDate(date);
    }

}