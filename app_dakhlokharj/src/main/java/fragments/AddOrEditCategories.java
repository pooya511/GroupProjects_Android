package fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taxiapps.dakhlokharj.R;

import java.util.ArrayList;
import java.util.Arrays;

import adapters.CategoryAdapter;
import adapters.SubCategoryAdapter;
import model.CircleButtonComponent;
import model.Group;
import model.GroupDetail;
import utils.AppModules;

/**
 * Created by Parsa on 2017-12-17.
 */

public class AddOrEditCategories extends Fragment {

    private String currentColor = "";
    private TextView beforeView = null;
    private CircleButtonComponent beforeCircleBtn;
    private EditText nameEditText;

    private CategoryItem categoryItem;

    private static ArrayList<String> colorArray = new ArrayList<>(Arrays.asList("#236E96", "#15B2D3", "#FFD700", "#F3872F", "#FF598F", "#00FFFF", "#6495ED", "#8A2BE2", "#FFC0CB", "#FF1493", "#FFFF00",
            "#B22222", "#B8860B", "#006400", "#32CD32", "#20B2AA", "#00CED1", "#1E90FF", "#4169E1", "#F5DEB3", "#BC8F8F", "#FF00FF",
            "#FF4500", "#FFD700", "#008000", "#00FF7F", "#2F4F4F", "#48D1CC", "#191970", "#4B0082", "#8B4513", "#FF69B4", "#D2691E",
            "#FF0000", "#9ACD32", "#00FF00", "#2E8B57", "#008B8B", "#5F9EA0", "#00008B", "#8B008B", "#00BFFF", "#808080", "#000000"));

    private static ArrayList<String> iconArray = new ArrayList<>(Arrays.asList("fa-cutlery","fa-home", "fa-building-o", "fa-tint", "fa-shopping-cart", "fa-user-o", "fa-futbol-o", "fa-handshake-o", "fa-money", "fa-user-secret", "fa-free-code-camp", "fa-file", "fa-car", "fa-inbox", "fa-glass", "fa-bolt", "fa-phone", "fa-mobile", "fa-globe", "fa-medkit", "fa-sun-o", "fa-diamond", "fa-television", "fa-suitcase", "fa-scissors", "fa-umbrella", "fa-pencil", "fa-stethoscope", "fa-gift", "fa-archive", "fa-video-camera", "fa-eye",
            "fa-briefcase", "fa-bar-chart", "fa-university", "fa-shopping-basket", "fa-apple", "fa-comment-o", "fa-money", "fa-heartbeat", "fa-motorcycle", "fa-diamond", "fa-question-circle", "fa-car", "fa-map-o", "fa-user-o", "fa-star", "fa-youtube-play", "fa-renren", "fa-fa", "fa-ge", "fa-line-chart", "fa-trophy", "fa-th-large", "fa-users", "fa-btc", "fa-user-circle-o", "fa-dollar", "fa-crosshairs", "fa-send-o", "fa-scissors", "fa-server",
            "fa-user-secret", "fa-file", "fa-eraser", "fa-headphones", "fa-times-circle-o", "fa-inbox", "fa-edge", "fa-ioxhost", "fa-hourglass-half", "fa-keyboard-o", "fa-free-code-camp", "fa-clock-o", "fa-female", "fa-paw",
            "fa-forumbee", "fa-bell", "fa-battery-4", "fa-sign-language", "fa-smile-o", "fa-th", "fa-registered", "fa-tree", "fa-binoculars", "fa-frown-o", "fa-eur", "fa-mobile-phone", "fa-cart-plus", "fa-ship", "fa-cc-visa", "fa-deafness", "fa-hand-scissors-o", "fa-birthday-cake", "fa-dot-circle-o", "fa-handshake-o", "fa-rouble", "fa-dropbox", "fa-institution", "fa-mixcloud", "fa-lightbulb-o", "fa-flag-o", "fa-cogs",
            "fa-train", "fa-gg", "fa-newspaper-o", "fa-hdd-o", "fa-file-zip-o", "fa-bitbucket", "fa-globe", "fa-apple", "fa-gear", "fa-simplybuilt", "fa-fax", "fa-chrome", "fa-envelope-o", "fa-signal", "fa-fighter-jet", "fa-slideshare", "fa-tags", "fa-bar-chart-o", "fa-qq", "fa-asterisk", "fa-gavel", "fa-rocket", "fa-comments",
            "fa-file-photo-o", "fa-book", "fa-wikipedia-w", "fa-stack-overflow", "fa-subway", "fa-modx", "fa-shopping-bag", "fa-file-picture-o", "fa-instagram", "fa-space-shuttle", "fa-leanpub", "fa-laptop", "fa-thumbs-o-down", "fa-shopping-cart", "fa-sun-o", "fa-indent",
            "fa-universal-access", "fa-editStart", "fa-black-tie", "fa-soundcloud", "fa-link", "fa-address-book-o", "fa-trash", "fa-video-camera", "fa-camera-retro", "fa-television", "fa-bed", "fa-bullhorn", "fa-folder-open", "fa-blind", "fa-user-plus", "fa-minus", "fa-shield", "fa-try", "fa-gift", "fa-mortar-board", "fa-film", "fa-tripadvisor", "fa-pie-chart", "fa-plane",
            "fa-life-bouy", "fa-plug", "fa-eye", "fa-camera", "fa-life-saver", "fa-music", "fa-bolt", "fa-yelp", "fa-bank", "fa-windows", "fa-coffee", "fa-phone", "fa-bell-slash", "fa-umbrella", "fa-safari", "fa-tag", "fa-viadeo", "fa-thermometer-three-quarters", "fa-balance-scale", "fa-fire-extinguisher"));

    public static AddOrEditCategories newInstance(CategoryItem categoryItem) {

        // currentColor = "#000000";

        Bundle args = new Bundle();

        if (categoryItem != null)
            args.putParcelable("Item", categoryItem);
        //    else
        //args.putParcelable("Item", new CategoryItem());

        AddOrEditCategories fragment = new AddOrEditCategories();
        fragment.setArguments(args);
        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.pop_add_and_edit_categories_and_subcategories, null);

        LinearLayout colorLayout = view.findViewById(R.id.pop_add_and_edit_categories_and_subcategories_color_container);
        final HorizontalScrollView horizontalScrollView = view.findViewById(R.id.pop_add_and_edit_categories_and_subcategories_horizontal_scroll_view);
        final GridView iconListView = view.findViewById(R.id.pop_add_and_edit_categories_and_subcategories_grid_view);
        nameEditText = view.findViewById(R.id.pop_add_and_edit_categories_and_subcategories_category_name);
        TextView saveTextView = view.findViewById(R.id.pop_add_and_edit_categories_and_subcategories_save);
        final TextView errorTextView = view.findViewById(R.id.pop_add_and_edit_categories_and_subcategories_error_text_view);

        categoryItem = getArguments().getParcelable("Item");
        if (categoryItem.iconText != null && categoryItem.iconColor != null && !categoryItem.name.equals("new")) {
            nameEditText.setText(categoryItem.name);
            currentColor = categoryItem.iconColor;
        }

        iconListView.setAdapter(new IconAdapter(getActivity(), iconArray, categoryItem));

        TextView backText = view.findViewById(R.id.pop_add_and_edit_categories_and_subcategories_back);
        backText.setOnClickListener(v -> {
            if (CategoryAdapter.getInstance() != null)
                CategoryAdapter.getInstance().editEnd();
            if (SubCategoryAdapter.getInstance() != null)
                SubCategoryAdapter.getInstance().editEnd();
            getFragmentManager().popBackStack();

        });

        for (int i = 0; i < colorArray.size(); i++) {

            final CircleButtonComponent circleButton = new CircleButtonComponent(getActivity());
            circleButton.setLayoutParams(new LinearLayout.LayoutParams(65, 65));
            circleButton.setColor(colorArray.get(i));
            circleButton.setId(i * 65);

            circleButton.setOnClickListener(v -> {
                if (beforeCircleBtn == null)
                    beforeCircleBtn = circleButton;
                else {
                    beforeCircleBtn.setLayoutParams(new LinearLayout.LayoutParams(65, 65));
                    beforeCircleBtn = circleButton;
                }

                circleButton.setLayoutParams(new LinearLayout.LayoutParams(75, 75));
                currentColor = circleButton.getColor();

                IconAdapter.getInstance().categoryItem.iconColor = currentColor;
                IconAdapter.getInstance().notifyDataSetChanged();

                horizontalScrollView.smoothScrollTo((int) (circleButton.getId() - (3.15 * 65)), 0);
                if (beforeView != null)
                    beforeView.setTextColor(getResources().getColor(R.color.gray));
            });

            colorLayout.addView(circleButton);

        }

        // click shodan Icon ha
        iconListView.setOnItemClickListener((parent, view1, position, id) -> {

            if (currentColor.equals(""))
                currentColor = "#000000";

            TextView textView = (TextView) view1;
            textView.setTextColor(Color.parseColor(currentColor));

            Log.i("Position", String.valueOf(position));
            IconAdapter.getInstance().categoryItem.iconText = iconArray.get(position);
            IconAdapter.getInstance().categoryItem.iconColor = currentColor;
            IconAdapter.getInstance().notifyDataSetChanged();

            if (beforeView == null)
                beforeView = textView;
            else {
                beforeView.setTextColor(getActivity().getResources().getColor(R.color.gray));
                beforeView = textView;
            }

        });

        // save kardan
        saveTextView.setOnClickListener(v -> {

            if (nameEditText.getText().toString().equals("")) {
                errorTextView.startAnimation(AppModules.fadeInAndFadeOut());
                errorTextView.setText("لطفا نام را وارد کنید");
            } else if (currentColor.equals("")) {
                errorTextView.setText("لطفا یک رنگ انتخاب کنید");
                errorTextView.startAnimation(AppModules.fadeInAndFadeOut());
            } else if (categoryItem.iconText == null) {
                errorTextView.setText("لطفا یک آیکون انتخاب کنید");
                errorTextView.startAnimation(AppModules.fadeInAndFadeOut());
            } else {

                // for category
                if (categoryItem.categoryItemType.toString().equals("Category")) {

                    if (categoryItem.name.equals("new")) {

                        Group newGroup = new Group(0, nameEditText.getText().toString(), categoryItem.iconText, categoryItem.iconColor, 0, categoryItem.lstType);
                        newGroup.insert();
                        CategoryAdapter.getInstance().editEnd();
                        CategoryAdapter.getInstance().updateGroupOrder();
                        getFragmentManager().popBackStack();

                    } else {

                        Group updatedGroup = new Group(categoryItem.ID, nameEditText.getText().toString(), categoryItem.iconText, categoryItem.iconColor, categoryItem.order, categoryItem.lstType);
//                    Log.i("GroupID", String.valueOf(group.getID()));
//                    Log.i("GroupName", group.getLstName());
//                    Log.i("GroupIconText", group.getLstIcon());
//                    Log.i("GroupIconColor", group.getLstColor());
//                    Log.i("GroupOrder", String.valueOf(group.getLstOrder()));
//                    Log.i("GroupListType", String.valueOf(group.getLstType()));
                        updatedGroup.update(new String[]{Group.GroupCol.lstName.value(), Group.GroupCol.lstIcon.value(), Group.GroupCol.lstColor.value()});
                        CategoryAdapter.getInstance().editEnd();
                        getFragmentManager().popBackStack();

                    }

                }

                //for subcategory
                if (categoryItem.categoryItemType.toString().equals("SubCategory")) {

                    if (categoryItem.name.equals("new")) {

                        GroupDetail newGroupDetail = new GroupDetail(0, categoryItem.lstID, nameEditText.getText().toString(), categoryItem.iconText, categoryItem.iconColor);
//                    Log.i("GroupDetailID", String.valueOf(groupDetail.getID()));
//                    Log.i("GroupDetailLstID", String.valueOf(groupDetail.getLstID()));
//                    Log.i("GroupDetailName", String.valueOf(groupDetail.getLsdName()));
//                    Log.i("GroupDetailIcon", String.valueOf(groupDetail.getLsdIcon()));
//                    Log.i("GroupDetailColor", String.valueOf(groupDetail.getLsdColor()));
                        newGroupDetail.insert();
                        SubCategoryAdapter.getInstance().editEnd();
                        getFragmentManager().popBackStack();

                    } else {

                        GroupDetail updatedGroupDetail = new GroupDetail(categoryItem.ID, categoryItem.lstID, nameEditText.getText().toString(), categoryItem.iconText, categoryItem.iconColor);
//                    Log.i("GroupDetailID", String.valueOf(groupDetail.getID()));
//                    Log.i("GroupDetailLstID", String.valueOf(groupDetail.getLstID()));
//                    Log.i("GroupDetailName", String.valueOf(groupDetail.getLsdName()));
//                    Log.i("GroupDetailIcon", String.valueOf(groupDetail.getLsdIcon()));
//                    Log.i("GroupDetailColor", String.valueOf(groupDetail.getLsdColor()));
                        updatedGroupDetail.update(new String[]{GroupDetail.GroupDetailCol.lsdName.value(), GroupDetail.GroupDetailCol.lsdIcon.value(), GroupDetail.GroupDetailCol.lsdColor.value()});
                        SubCategoryAdapter.getInstance().editEnd();
                        getFragmentManager().popBackStack();

                    }

                }

            }

        });

        return view;
    }

    private static class IconAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<String> iconArray;
        private TextView iconTextView;
        public CategoryItem categoryItem;

        private static IconAdapter instance = null;

        public static IconAdapter getInstance() {
            if (instance == null) {
                try {
                } catch (Exception e) {
                  //  Toast.makeText(context, "dadash yadet raft init koni classo", Toast.LENGTH_SHORT).show();
                }
            }
            return instance;
        }

        public IconAdapter(Context context, ArrayList<String> iconArray, CategoryItem categoryItem) {
            this.context = context;
            this.iconArray = iconArray;
            this.categoryItem = categoryItem;
            instance = this;
        }

        @Override
        public int getCount() {
            return iconArray.size();
        }

        @Override
        public Object getItem(int position) {
            return iconArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            iconTextView = new TextView(context);
            iconTextView.setGravity(Gravity.CENTER);
            iconTextView = AppModules.setTypeFace(context, iconTextView, "fontawesome_webfont.ttf");
            iconTextView.setTextColor(context.getResources().getColor(R.color.gray));
            iconTextView.setTextSize(20f);

            if (categoryItem.iconText != null && categoryItem.iconText.equals(iconArray.get(position))){
                iconTextView.setTextColor(Color.parseColor(categoryItem.iconColor));
            }

            try {
                iconTextView.setText(AppModules.convertIconFromString(context, iconArray.get(position)));
            } catch (Resources.NotFoundException e) {
                Log.i("Icon", iconArray.get(position));
            }

            return iconTextView;
        }

    }

    public static class CategoryItem implements Parcelable {

        public int ID;
        public int lstID;
        public int lstType;
        public int order;
        public String iconColor;
        public String iconText;
        public String name;
        public CategoryItemType categoryItemType;

        protected CategoryItem(Parcel in) {
            ID = in.readInt();
            order = in.readInt();
            iconColor = in.readString();
            iconText = in.readString();
            name = in.readString();
        }

        public static final Creator<CategoryItem> CREATOR = new Creator<CategoryItem>() {
            @Override
            public CategoryItem createFromParcel(Parcel in) {
                return new CategoryItem(in);
            }

            @Override
            public CategoryItem[] newArray(int size) {
                return new CategoryItem[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(ID);
            dest.writeInt(order);
            dest.writeString(iconColor);
            dest.writeString(iconText);
            dest.writeString(name);
        }

        public enum CategoryItemType {

            CATEGORY("Category"),
            SUBCATEGORY("SubCategory");

            private final String value;

            CategoryItemType(String s) {
                value = s;
            }

            public String toString() {
                return this.value;
            }

        }

        // for List
        public CategoryItem(int ID, String name, String iconText, String iconColor, int order, int lstType, CategoryItemType type) {
            this.ID = ID;
            this.iconColor = iconColor;
            this.iconText = iconText;
            this.name = name;
            this.order = order;
            this.categoryItemType = type;
            this.lstType = lstType;
        }


        // for List Detail
        public CategoryItem(int ID, int lstID, String name, String iconText, String iconColor, CategoryItemType type) {
            this.ID = ID;
            this.lstID = lstID;
            this.iconColor = iconColor;
            this.iconText = iconText;
            this.name = name;
            this.categoryItemType = type;
        }

        public CategoryItem() {

        }


    }
}