package air.gamediesel.rasgir;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import DB.DBManager;
import OOP.Factor;
import OOP.FactorGroup;
import OOP.FactorRasResult;
import Other.C_Setting;
import Other.DialogUtils;
import Other.Modules;
import adapters.FactorListAdapter;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

import static Other.Modules.hideSoftKeyboard;
import static Other.Modules.persianDigits;

public class AddFactorActivityFinal extends BaseActivity {

    private TextView dialogText, factorCount, factorDate, factorPriceSum, currency, groupName, groupNameTextView , dayCounts , beforeOrAfter;
    private LinearLayout priceLayout;
    Button okBtn, autoNameBtn, newGroup, itselfGroup;
    ImageView CreatePdfBtn, shareBtn , saveBtn;
    EditText enteredName;
    public FactorGroup factorGroup;
    ArrayList<FactorGroup> factorGroupsList = new ArrayList<>();
    LinearLayout homeLayout, formulaLayout , beforeAfterBox;
    DBManager db;
    private ArrayList<Factor> factors;
    boolean hasNoPrice;
    private final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 0;
    private FactorRasResult factorRasResult;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    private int getDaysToMonthsForward(long date, int monthCount) {
        PersianDate date1 = new PersianDate(Modules.startOfDay(date));
        int newMonth = (date1.getShMonth() + monthCount);
        int newYear = date1.getShYear();
        if (newMonth > 12) {
            newYear = date1.getShYear() + (int) Math.floor(newMonth / 12);
            newMonth = newMonth % 12;
        }
        PersianDate date2 = new PersianDate();
        date2.setHour(23);
        date2.setMinute(59);
        date2.setSecond(59);
        date2.setShYear(newYear);
        date2.setShMonth(newMonth);
        date2.setShDay(date1.getShDay());
        long compare = date2.getTime() - date1.getTime();
        return Math.round(compare / 86400000);
    }

    private FactorRasResult calculateFactors(ArrayList<Factor> factors) {

        FactorRasResult factorRasResult = new FactorRasResult();

        double settleLimitDayCountByPrice = 0;
        double settleAverageDateByPrice = 0;
        double settleAverageDate_ts;
        double limitDate_ts;
        double daysForward ;

        for (int i = 0; i < factors.size(); i++) {
            if (factors.get(i).isActive()) {
                daysForward =
                        factors.get(i).getSettlementType() == 1 ? factors.get(i).getSettlementDuration() :
                                factors.get(i).getSettlementType() == 2 ? getDaysToMonthsForward(factors.get(i).getDate(), factors.get(i).getSettlementDuration()) :
                                        0;
                factorRasResult.priceSum += factors.get(i).getPrice();
                factorRasResult.factorCount++;
                settleLimitDayCountByPrice += daysForward * Math.max(1, factors.get(i).getPrice());
                settleAverageDateByPrice += factors.get(i).getDate() * Math.max(1, factors.get(i).getPrice());
            }
        }
        factorRasResult.dayCount = (int) Math.floor(settleLimitDayCountByPrice / Math.max(1, factorRasResult.priceSum));
        settleAverageDate_ts = settleAverageDateByPrice / Math.max(1, factorRasResult.priceSum);

        limitDate_ts = settleAverageDate_ts + (factorRasResult.dayCount * (double) 86400000);
        factorRasResult.limitDate = (long) limitDate_ts;
        factorRasResult.averageDate = (long) settleAverageDate_ts;

        return factorRasResult;
    }

    private void shareFactorPdf() {
        try {
            PersianDate persianCalendar1 = new PersianDate();
            factorGroup.setExportDate(persianCalendar1.getTime());
            if (factorGroup.getId() != 0) {
                db.updateFactorGroup(factorGroup);
            }
            Modules.createFactorPdf(AddFactorActivityFinal.this, factorGroup, factors, factorRasResult);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri pdfUri = Uri.parse("file://" + C_Setting.getStringValue(C_Setting.GET_FACTOR_PDF_PATH));
        Log.i("getPdfPath", "file://" + C_Setting.getStringValue(C_Setting.GET_FACTOR_PDF_PATH));
        sharingIntent.setType("application/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, pdfUri);
        startActivity(Intent.createChooser(sharingIntent, "Share PDF using"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    shareFactorPdf();
                } else {
                    DialogUtils.makeMessageDialog(AddFactorActivityFinal.this, DialogUtils.MessageDialogTypes.ERROR, "عدم دسترسی", "برای ارسال فایل باید دسترسی به حافظه خارجی را فراهم کنید .", "").show();
                }
                break;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_factor_activity_final);
        //
        db = new DBManager(AddFactorActivityFinal.this);
        factorGroupsList = db.selectAllFactorGroups();
        //
        saveBtn = findViewById(R.id.add_factor_final_save_button);
        CreatePdfBtn = findViewById(R.id.factor_create_pdf_btn);
        factorCount = findViewById(R.id.add_factor_activity_final_factor_count);
        factorPriceSum = findViewById(R.id.add_factor_activity_final_factor_price_sum);
        factorDate = findViewById(R.id.add_factor_activity_final_factor_date);
        homeLayout = findViewById(R.id.add_factor_activity_final_home_button);
        priceLayout = findViewById(R.id.add_factor_activity_final_check_price_layout);
        currency = findViewById(R.id.add_factor_activity_final_currency);
        formulaLayout = findViewById(R.id.add_factor_final_formula_layout);
        shareBtn = findViewById(R.id.share_factors);
        groupName = findViewById(R.id.add_factor_activity_final_group_name);
        groupNameTextView = findViewById(R.id.add_factor_activity_final_group_name_text_view);
        dayCounts = findViewById(R.id.add_factor_activity_final_number_of_days);
        beforeOrAfter = findViewById(R.id.add_factor_activity_final_before_after);
        beforeAfterBox = findViewById(R.id.add_factor_activity_final_before_after_box);
        //
        if (getIntent().getStringExtra("groupName") != null) {
            groupName.setText(PublicModules.toPersianDigit(getIntent().getStringExtra("groupName")));
            groupNameTextView.setVisibility(View.VISIBLE);
        }
        //
        currency.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));
        //
        hasNoPrice = getIntent().getBooleanExtra("hasNoPrice", false);
        if (hasNoPrice) {
            priceLayout.setVisibility(View.GONE);
        } else {
            priceLayout.setVisibility(View.VISIBLE);
        }
        //
        factors = (ArrayList<Factor>) getIntent().getSerializableExtra("FactorList");
        factorRasResult = calculateFactors(factors);
        factorCount.setText(PublicModules.toPersianDigit(String.valueOf(factorRasResult.factorCount)));
        String price = String.valueOf(String.valueOf(factorRasResult.priceSum));
        price = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(price));
        factorPriceSum.setText(PublicModules.toPersianDigit(price));
        final PersianDate persianCalendar = new PersianDate(factorRasResult.limitDate);
        factorDate.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));

        dayCounts.setText(PublicModules.toPersianDigit(String.valueOf(factorRasResult.dayCount).replaceAll("-", "")));
        if (factorRasResult.dayCount < 0) {
            beforeOrAfter.setText("قبل");
        }
        if (factorRasResult.dayCount > 0) {
            beforeOrAfter.setText("بعد");
        }
        if (factorRasResult.dayCount == 0) {
            beforeAfterBox.setVisibility(View.GONE);
        }
        //
        if (FactorListAdapter.factorGroupId == 0) {
            String name = "گروه " + (factorGroupsList.size() + 1);
            factorGroup = new FactorGroup(name, factorRasResult.limitDate, hasNoPrice);
        } else {
            factorGroup = db.selectFactorGruopById(FactorListAdapter.factorGroupId);
            factorGroup.setDeadLineDate(factorRasResult.limitDate);
        }
        //TODO onActivityResult va code permission pak beshe
        CreatePdfBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtils.loadingDialog(AddFactorActivityFinal.this).show();
                if (ContextCompat.checkSelfPermission(AddFactorActivityFinal.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    PublicDialogs.permissionExplainDialog(AddFactorActivityFinal.this, "ذخیره PDF", new PublicDialogs.PermissionExplainCallBack() {
                        @Override
                        public void ok() {
                            String[] permissions = {android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                            Permissions.check(AddFactorActivityFinal.this, permissions, null, null, new PermissionHandler() {
                                @Override
                                public void onGranted() {
                                    shareFactorPdf();
                                }

                                @Override
                                public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                                    PublicDialogs.permissionExplainDialog(AddFactorActivityFinal.this, "ذخیره PDF", new PublicDialogs.PermissionExplainCallBack() {
                                        @Override
                                        public void ok() {
                                            //do nothing
                                        }
                                    }).show();
                                }

                                @Override
                                public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {
                                }

                                @Override
                                public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                                    new PublicDialogs.AlwaysDeny(AddFactorActivityFinal.this, "خروجی", "با فعال کردن گزینه Storage از منوی Permissions مربوط به برنامه یادآورچک امکان خروجی گرفتن را فعال کنید.").show();
                                    return true;
                                }
                            });
                        }
                    }).show();
                } else {
                    shareFactorPdf();
                }
            }
        });
        //TODO
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AddFactorActivity1.changeItself) {
                    final Dialog parentDialog = new Dialog(AddFactorActivityFinal.this);
                    parentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    parentDialog.setContentView(R.layout.dialog_save_check_group);
                    parentDialog.setCanceledOnTouchOutside(false);
                    parentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    parentDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                    parentDialog.show();
                    //
                    newGroup = parentDialog.findViewById(R.id.dialog_save_check_group_new_group);
                    itselfGroup = parentDialog.findViewById(R.id.dialog_save_check_group_itself);
                    //
                    newGroup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog dialog = new Dialog(AddFactorActivityFinal.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_enter_group_name);
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

                            dialogText = dialog.findViewById(R.id.dialog_enter_group_name_text);
                            dialogText.setText("لطفا نام گروه های فاکتور خود را وارد کنید.");
                            dialog.show();
                            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {
                                    Modules.hideSoftKeyboard(AddFactorActivityFinal.this);
                                }
                            });
                            //
                            okBtn = dialog.findViewById(R.id.dialog_group_name_ok_button);
                            autoNameBtn = dialog.findViewById(R.id.dialog_group_name_auto_button);
                            enteredName = dialog.findViewById(R.id.dialog_group_name_edit_text);
                            enteredName.setImeOptions(EditorInfo.IME_ACTION_DONE);
                            enteredName.setHint("گروه " + PublicModules.toPersianDigit(String.valueOf((factorGroupsList.size() + 1))));
                            //
                            enteredName.requestFocus();
                            Modules.showSoftKeyboard(AddFactorActivityFinal.this);
                            //
                            okBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String name = enteredName.getText().toString();
                                    if (enteredName.getText().toString().equals("")) {
                                        DialogUtils.makeMessageDialog(AddFactorActivityFinal.this, DialogUtils.MessageDialogTypes.ERROR, "خطا", "نام گروه نمی تواند خالی باشد", "").show();
                                    } else {
                                        factorGroup.setName(PublicModules.toEnglishDigit(name));
                                        factorGroup.setExportDate(0);
                                        int id = db.insertFactorGroup(factorGroup);
                                        for (int i = 0; i < factors.size(); i++) {
                                            factors.get(i).setGroupId(id);
                                            db.insertFactor(factors.get(i));
                                        }
                                        groupName.setText(PublicModules.toPersianDigit(name));
                                        groupNameTextView.setVisibility(View.VISIBLE);
                                        dialog.dismiss();
                                        AddFactorActivity1.arrayString = AddFactorActivity1.factorSum(factors);
                                        parentDialog.dismiss();
                                        hideSoftKeyboard(AddFactorActivityFinal.this);
                                    }
                                }
                            });
                            //
                            autoNameBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String name = "گروه " + (factorGroupsList.size() + 1);
                                    factorGroup.setExportDate(0);
                                    factorGroup.setName(PublicModules.toEnglishDigit(name));
                                    int id = db.insertFactorGroup(factorGroup);
                                    for (int i = 0; i < factors.size(); i++) {
                                        factors.get(i).setGroupId(id);
                                        db.insertFactor(factors.get(i));
                                    }
                                    groupName.setText(PublicModules.toPersianDigit(name));
                                    groupNameTextView.setVisibility(View.VISIBLE);
                                    dialog.dismiss();
                                    AddFactorActivity1.arrayString = AddFactorActivity1.factorSum(factors);
                                    parentDialog.dismiss();
                                    hideSoftKeyboard(AddFactorActivityFinal.this);
                                }
                            });
                        }

                    });
                    //
                    itselfGroup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            db.updateFactorGroup(factorGroup);
                            //
                            db.deleteFactorByfctId(factorGroup.getId());
                            for (int i = 0; i < factors.size(); i++) {
                                factors.get(i).setGroupId(factorGroup.getId());
                                db.insertFactor(factors.get(i));
                            }
                            groupName.setText(PublicModules.toPersianDigit(factorGroup.getName()));
                            groupNameTextView.setVisibility(View.VISIBLE);
                            parentDialog.dismiss();
                            AddFactorActivity1.arrayString = AddFactorActivity1.factorSum(factors);
                        }
                    });
                } else {
                    final Dialog parentDialog = new Dialog(AddFactorActivityFinal.this);
                    parentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    parentDialog.setContentView(R.layout.dialog_enter_group_name);
                    parentDialog.setCanceledOnTouchOutside(false);
                    parentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    parentDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

                    dialogText = parentDialog.findViewById(R.id.dialog_enter_group_name_text);
                    dialogText.setText("لطفا نام گروه های فاکتور خود را وارد کنید.");
                    parentDialog.show();
                    parentDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            Modules.hideSoftKeyboard(AddFactorActivityFinal.this);
                        }
                    });
                    //
                    okBtn = parentDialog.findViewById(R.id.dialog_group_name_ok_button);
                    autoNameBtn = parentDialog.findViewById(R.id.dialog_group_name_auto_button);
                    enteredName = parentDialog.findViewById(R.id.dialog_group_name_edit_text);
                    enteredName.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    enteredName.setHint("گروه " + PublicModules.toPersianDigit(String.valueOf((factorGroupsList.size() + 1))));
                    //
                    enteredName.requestFocus();
                    Modules.showSoftKeyboard(AddFactorActivityFinal.this);
                    //
                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name = enteredName.getText().toString();
                            if (enteredName.getText().toString().equals("")) {
                                DialogUtils.makeMessageDialog(AddFactorActivityFinal.this, DialogUtils.MessageDialogTypes.ERROR, "خطا", "نام گروه نمی تواند خالی باشد", "").show();
                            } else {
                                factorGroup.setName(PublicModules.toEnglishDigit(name));
                                int id = db.insertFactorGroup(factorGroup);
                                for (int i = 0; i < factors.size(); i++) {
                                    factors.get(i).setGroupId(id);
                                    db.insertFactor(factors.get(i));
                                }
                                groupName.setText(PublicModules.toPersianDigit(name));
                                groupNameTextView.setVisibility(View.VISIBLE);
                                parentDialog.dismiss();
                                AddFactorActivity1.arrayString = AddFactorActivity1.factorSum(factors);
                                hideSoftKeyboard(AddFactorActivityFinal.this);
                            }
                        }
                    });
                    //
                    autoNameBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name = "گروه " + (factorGroupsList.size() + 1);
                            int id = db.insertFactorGroup(factorGroup);
                            for (int i = 0; i < factors.size(); i++) {
                                factors.get(i).setGroupId(id);
                                db.insertFactor(factors.get(i));
                            }
                            groupName.setText(PublicModules.toPersianDigit(name));
                            groupNameTextView.setVisibility(View.VISIBLE);
                            parentDialog.dismiss();
                            AddFactorActivity1.arrayString = AddFactorActivity1.factorSum(factors);
                            hideSoftKeyboard(AddFactorActivityFinal.this);
                        }
                    });
                }
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(AddFactorActivityFinal.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_share_text);
                dialog.setCanceledOnTouchOutside(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

                final LinearLayout shareTextResult = dialog.findViewById(R.id.dialog_share_text_result);
                final LinearLayout shareTextResultAndList = dialog.findViewById(R.id.dialog_share_text_result_and_list);
                final LinearLayout shareTextDialogCancel = dialog.findViewById(R.id.dialog_share_text_cancel);

                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v.equals(shareTextResult)) {
                            String factorSum = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(factorRasResult.priceSum)));
                            PersianDate checkDate = new PersianDate(factorRasResult.limitDate);
                            Intent sendIntent = new Intent();
                            String export = "تعداد فاکتور ها: " + String.valueOf(factorRasResult.factorCount) + " فقره \n"
                                    + "جمع مبلغ: " + factorSum + " ریال\n"
                                    + "تاریخ چک: " + Modules.getPersianShortDate(checkDate) + "\n";
                            String fixedExport = persianDigits(export);
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, fixedExport);
                            sendIntent.setType("text/plain");
                            startActivity(sendIntent);
                            dialog.dismiss();
                        }
                        if (v.equals(shareTextResultAndList)) {
                            String settlementDate = "";
                            String factorSum = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(factorRasResult.priceSum)));
                            PersianDate checkDate = new PersianDate(factorRasResult.limitDate);
                            Intent sendIntent = new Intent();
                            String export = "تعداد فاکتور ها: " + String.valueOf(factorRasResult.factorCount) + " فقره \n"
                                    + "جمع مبلغ: " + factorSum + " ریال\n"
                                    + "تاریخ چک: " + Modules.getPersianShortDate(checkDate) + "\n"
                                    + "\n"
                                    + "لیست فاکتور ها :";
                            for (int i = 0; i < factors.size(); i++) {
                                if (factors.get(i).getSettlementType() == 1) {
                                    settlementDate = String.valueOf(factors.get(i).getSettlementDuration() + " روز");
                                } else {
                                    settlementDate = String.valueOf(factors.get(i).getSettlementDuration() + " ماه");
                                }
                                String formattedPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(factors.get(i).getPrice())));
                                PersianDate persianCalendar = new PersianDate(factors.get(i).getDate());
                                export = export + "\n\n"
                                        + String.valueOf(i + 1) + "- "
                                        + "مبلغ : "
                                        + (formattedPrice + "ریال \n")
                                        + ("تاریخ فاکتور : ")
                                        + (Modules.getPersianShortDate(persianCalendar)) + " - "
                                        + "مدت تسویه : " + settlementDate;
                            }
                            String fixedExport = persianDigits(export);
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, fixedExport);
                            sendIntent.setType("text/plain");
                            startActivity(sendIntent);
                            dialog.dismiss();
                        }
                        if (v.equals(shareTextDialogCancel)) {
                            dialog.dismiss();
                        }
                    }
                };
                shareTextResult.setOnClickListener(listener);
                shareTextResultAndList.setOnClickListener(listener);
                shareTextDialogCancel.setOnClickListener(listener);
                dialog.show();
            }
        });
        //
        homeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AddFactorActivity1.arrayString.equals(AddFactorActivity1.factorSum(factors))) {
                    Intent intent = new Intent(AddFactorActivityFinal.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("isLogin", true);
                    startActivity(intent);
                } else {
                    DialogUtils.backToHomeDialog(AddFactorActivityFinal.this).show();
                }
            }
        });
        //
        formulaLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(AddFactorActivityFinal.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_factor_formula);
                dialog.setCanceledOnTouchOutside(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
            }
        });
    }
}
