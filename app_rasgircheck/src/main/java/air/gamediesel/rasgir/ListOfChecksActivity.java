package air.gamediesel.rasgir;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.List;

import DB.DBManager;
import OOP.CheckGroup;
import adapters.CheckListAdapter;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class ListOfChecksActivity extends BaseActivity {

    private static List<CheckGroup> checkGroups;
    private Button newBtn;
    private RecyclerView recyclerView;

    private static ListOfChecksActivity instance;
    public CheckListAdapter adapter;
    private DBManager db;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkGroups = db.selectAllCheckGroups();
        adapter.refresh();
    }

    public static ListOfChecksActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_checks);
        instance = this;
        db = new DBManager(this);
        //
        recyclerView = findViewById(R.id.list_of_check_recycler_view);
        //this is adapter constructor
        adapter = new CheckListAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        //
        newBtn = findViewById(R.id.list_of_check_new_button);
        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfChecksActivity.this, AddCheckActivity1.class);
                startActivity(intent);
            }
        });

    }

}
