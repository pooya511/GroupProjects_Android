package air.gamediesel.rasgir;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taxiapps.txpayment.dialogFragment.Payment;

import java.util.ArrayList;

import DB.DBManager;
import OOP.Factor;
import Other.C_Setting;
import Other.Constants;
import Other.DialogUtils;
import Other.Modules;
import adapters.FactorAdapter;
import adapters.FactorListAdapter;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class AddFactorActivity1 extends BaseActivity {

    private ArrayList<Factor> factorArray;
    private FactorAdapter factorAdapter;
    private Button nextPageBtn;
    private DBManager db;
    private RecyclerView recycleView;
    private LinearLayoutManager layoutManager;
    public static boolean changeItself;
    private boolean isEmpty, hasNoPrice;
    private ImageView report;
    private TextView sumCurrency;

    public static String arrayString; // برای تشخیص دادن این که دیتا ها تغییری کرده اند یا نه استفاده میشه
    // توسط متد factor sum استفاده میشه

    public static String factorSum(ArrayList<Factor> factors) {

        String fctSum = "";

        for (int i = 0; i < factors.size(); i++) {
            fctSum = String.valueOf(factors.get(i).getPrice()).concat("," + String.valueOf(factors.get(i).getDate()));
        }

        return fctSum;

    }

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_factor_page1);
        //
        db = new DBManager(this);
        factorArray = db.selectAllFactors();
        for (int i = 0; i < factorArray.size(); i++) {
            Log.i("type", String.valueOf(factorArray.get(i).getSettlementType()));
        }
        //
        ArrayList<Factor> factors = (ArrayList<Factor>) getIntent().getSerializableExtra("factorArray");
        hasNoPrice = getIntent().getBooleanExtra("hasNoPrice", false);
        factorArray = factors;
        if (factorArray == null) {
            factorArray = new ArrayList<>();
        }
        if (factorArray.isEmpty()) {
            Factor factor = new Factor(true);
            if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("روز")) {
                factor.setSettlementType(1);
            } else if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("ماه")) {
                factor.setSettlementType(2);
            }
            factorArray.add(factor);
            //
            if (C_Setting.getBooleanValue(C_Setting.RASGIRI_FACTOR_BEDUNE_MABLAGH)) {
                hasNoPrice = true;
            }
            changeItself = false;
        } else {
            changeItself = true;
        }
        //
        arrayString = factorSum(factorArray);
        //
        recycleView = findViewById(R.id.recycler_view_factor);
        factorAdapter = new FactorAdapter(this, factorArray, hasNoPrice);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recycleView.setLayoutManager(layoutManager);
        recycleView.setHasFixedSize(true);
        recycleView.setAdapter(factorAdapter);
        OverScrollDecoratorHelper.setUpOverScroll(recycleView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        //
        LinearLayout addLayout = findViewById(R.id.add_factor_page1_plus_layout);
        addLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setAlpha(0.4f);
                    if (factorArray.size() < 4) {
                        Factor factor = new Factor(true);
                        if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("روز")) {
                            factor.setSettlementType(1);
                        } else if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("ماه")) {
                            factor.setSettlementType(2);
                        }
                        factor.setGroupId(FactorListAdapter.factorGroupId);
                        factorArray.add(factor);
                        factorAdapter.updateFooter(factorArray);
                        recycleView.scrollToPosition(factorArray.size() - 1);
                    } else {
                        if (MyApplication.isLicenseValid()) {
                            Factor factor = new Factor(true);
                            if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("روز")) {
                                factor.setSettlementType(1);
                            } else if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("ماه")) {
                                factor.setSettlementType(2);
                            }
                            factorArray.add(factor);
                            factorAdapter.updateFooter(factorArray);
                            recycleView.scrollToPosition(factorArray.size() - 1);
                        } else {
                            final String userName = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER).equals("") ? "" : C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                            Payment.newInstance(R.drawable.rasgir_icon,
                                    Constants.APP_ID,
                                    Constants.APP_SOURCE,
                                    Constants.APP_VERSION,
                                    "راس گیر چک",
                                    MyApplication.currentBase64,
                                    "مزایای نسخه کامل",
                                    "- ورود بیش از ۴ عدد چک در راس گیری \n - ورود بیش از ۴ عدد فاکتور",
                                    userName,
                                    Constants.DEVICE_ID,
                                    Constants.SERVER_URL,
                                    Constants.GATEWAY_URL,
                                    new com.taxiapps.txpayment.payment.TX_License.SetLicenseCallback() {
                                        @Override
                                        public void call(long expireDate, String license) {
                                            C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE, expireDate);
                                            C_Setting.setStringValue(C_Setting.LICENSE_DATA, license);

                                            MainActivity.setUIPremium();
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    },
                                    new Payment.UserNameCallBack() {
                                        @Override
                                        public void getUserName(String username) {
                                            C_Setting.setStringValue(C_Setting.TX_USER_NUMBER, username);
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    }
                            ).show(getFragmentManager(), "tx_payment");
                        }
                    }
                    return true;
                } else {
                    v.setAlpha(1f);
                    return false;
                }
            }
        });
        //
        nextPageBtn = findViewById(R.id.add_factor_next_page1_button);
        nextPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEmpty = false;
                for (int i = 0; i < factorArray.size(); i++) {
                    if (!hasNoPrice) {
                        if (factorArray.get(i).getPrice() == 0
                                || factorArray.get(i).getDate() == 0
                                ) {
                            isEmpty = true;
                            Modules.customToast(AddFactorActivity1.this , "خطا" , "ابتدا فیلد های خالی را پر کنید." , false);
                        }
                    }
                }
                if (!isEmpty) {
                    Intent intent = new Intent(AddFactorActivity1.this, AddFactorActivityFinal.class);
                    intent.putExtra("FactorList", factorArray);
                    intent.putExtra("hasNoPrice", hasNoPrice);
                    if (getIntent().getStringExtra("groupName") != null) {
                        intent.putExtra("groupName", getIntent().getStringExtra("groupName"));
                    }
                    startActivity(intent);
                }
            }
        });
        //
        report = findViewById(R.id.add_factor_page1_report_image);
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double maximumPrice = factorArray.get(0).getPrice();
                for (int i = 0; i < factorArray.size(); i++) {  // برای به دست آوردن مبلغ ماکزیموم
                    if (factorArray.get(i).getPrice() > maximumPrice) {
                        maximumPrice = factorArray.get(i).getPrice();
                    }
                }
                //
                double minimumPrice = factorArray.get(0).getPrice();
                for (int i = 0; i < factorArray.size(); i++) { // برای به دست آوردن مبلغ مینیموم
                    if (factorArray.get(i).getPrice() < minimumPrice) {
                        minimumPrice = factorArray.get(i).getPrice();
                    }
                }
                //
                long nearestDate = 0;
                for (int i = 0; i < factorArray.size(); i++) {           // برای به دست آوردن نزدیک ترین تاریخ
                    if (factorArray.get(i).getDate() != 0) {
                        nearestDate = nearestDate == 0 ? factorArray.get(i).getDate() : nearestDate;
                        if (factorArray.get(i).getDate() < nearestDate) {
                            nearestDate = factorArray.get(i).getDate();
                        }
                    }
                }
                //
                long hindmostDate = 0;
                for (int i = 0; i < factorArray.size(); i++) {            // برای به دست آوردن دور ترین تاریخ
                    if (factorArray.get(i).getDate() != 0) {
                        hindmostDate = hindmostDate == 0 ? factorArray.get(i).getDate() : hindmostDate;
                        if (factorArray.get(i).getDate() > hindmostDate) {
                            hindmostDate = factorArray.get(i).getDate();
                        }
                    }
                }
                DialogUtils.reportChecksAndFactors(AddFactorActivity1.this, nearestDate, hindmostDate, maximumPrice, minimumPrice).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (arrayString.equals(factorSum(factorArray))) {
            super.onBackPressed();
        } else {
            DialogUtils.backDialog(AddFactorActivity1.this).show();
        }
    }
}
