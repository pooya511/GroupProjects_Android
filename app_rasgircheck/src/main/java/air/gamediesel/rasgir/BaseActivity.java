package air.gamediesel.rasgir;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.fragment.Limits;

import Other.DialogUtils;
import Other.C_Setting;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends Activity {

    private boolean startFromPassActivity = false;
    private static int resumed = 0;
    private static int paused = 0;

    private static Context context;

    private static BaseActivity instance;

    public static BaseActivity getInstance() {
        return instance;
    }

    final static int flagsHide = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

    abstract String childClassName();

    public static void runOnUi(final String title, final String content, final String url) {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.makeMessageDialog(context, DialogUtils.MessageDialogTypes.NORMAL, title, content, url).show();
            }
        });
    }

    CountDownTimer appPauseTimer = new CountDownTimer(10000, 100000) {
        @Override
        public void onTick(long l) {

        }

        @Override
        public void onFinish() {
            startFromPassActivity = true;
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Log.d("hichi", "onActivityResult(" + requestCode + "," + resultCode + "," + data);
//
//        // Pass on the activity result to the helper for handling
//        if (!TXDialogs.mHelper.handleActivityResult(requestCode, resultCode, data)) {
//            super.onActivityResult(requestCode, resultCode, data);
//        } else {
//            Log.d("hichi", "onActivityResult handled by IABUtil.");
//        }
//    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(Payment.TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!Limits.getInstance().mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(Payment.TAG, "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        context = BaseActivity.this;
        Log.d(childClassName(), "onCreate");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(flagsHide);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(childClassName(), "onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(childClassName(), "onRestart");

        // check mikonim ke app foreground bashe.
        // va az khode actiivty password nayomade bashe.
        // va timer ham be 60 sanie nareside bashe.
        if (appIsForeground() || childClassName().equals("PasswordActivity") || !startFromPassActivity)
            startFromPassActivity = false;
        else
            startFromPassActivity = true;

        if (startFromPassActivity) {
            startFromPassActivity = false;
            if (C_Setting.getStringValue(C_Setting.PIN_CODE) != null && !C_Setting.getStringValue(C_Setting.PIN_CODE).equals("")) {
                Intent intent = new Intent(this, PasswordActivity.class);
                intent.putExtra("userWantToEnter", true);
                intent.putExtra("onBackDisable", true); // bayad onBackPressed() tu passwordActivity disable bashe
                startActivity(intent);
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumed++;
        appPauseTimer.cancel();
        Log.d(childClassName(), "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(childClassName(), "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        paused++;
        appPauseTimer.start();

        Log.d(childClassName(), "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(childClassName(), "onDestroy");
        if (Limits.getInstance() != null)
            if (Limits.getInstance().mHelper != null) {
                Limits.getInstance().mHelper.dispose();
                Limits.getInstance().mHelper = null;
            }
    }

    /**
     * check mikonim barname forgrounde ya backgrounde.
     * age forground bashe resume ha bishtare .
     * age background bashe pause ha bishtare
     *
     * @return
     */
    public static boolean appIsForeground() {
        return resumed > paused;
    }

}
