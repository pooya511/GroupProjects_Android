package air.gamediesel.rasgir;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.List;

import OOP.FactorGroup;
import adapters.FactorListAdapter;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class ListOfFactorsActivity extends BaseActivity {

    private List<FactorGroup> factorGroups ;
    private Button newBtn;

    FactorListAdapter adapter;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_factors);
        //
        RecyclerView recyclerView = findViewById(R.id.list_of_factor_recycler_view);
        adapter = new FactorListAdapter(this,factorGroups);//this is adapter constructor
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        OverScrollDecoratorHelper.setUpOverScroll(recyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        //
        newBtn =  findViewById(R.id.list_of_factor_new_button);
        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfFactorsActivity.this, AddFactorActivity1.class);
                startActivity(intent);
                FactorListAdapter.factorGroupId = 0 ;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.refresh();
    }

}
