package air.gamediesel.rasgir;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import DB.DBManager;
import OOP.Check;
import OOP.CheckGroup;
import OOP.CheckRasResult;
import Other.C_Setting;
import Other.DialogUtils;
import Other.Modules;
import adapters.CheckAdapter;
import adapters.CheckListAdapter;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import me.grantland.widget.AutofitTextView;
import modules.PublicDialogs;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

import static Other.Modules.hideSoftKeyboard;
import static Other.Modules.persianDigits;

public class AddCheckActivityFinal extends BaseActivity {

    ImageView saveBtn, createPdfBtn, shareBtn, plusBtn, minusBtn;
    Button okBtn, autoNameBtn, newGroup, itselfGroup;
    EditText enteredName;
    TextView dialogText, numberOfChecks, priceOfResult, dayCounts, beforeOrAfter, dateOfChecks, darsadeBahre;
    AutofitTextView bahre, kasreBahreAzMablagh;
    TextView jameMablaghVahedTextView, kasreBahreAzMablaghTitleTextView, groupNameTextView, groupName;
    public CheckGroup checkGroup;
    CheckAdapter checkAdapter;
    public ArrayList<CheckGroup> checkGroupsList = new ArrayList<>();
    DBManager db;
    LinearLayout homeLayout, formulaRelativeLayout, shareTextResult, shareTextResultAndList, shareTextDialogCancel, beforeOrAfterBox;
    double percent = 3;
    private CheckListAdapter checkListAdapter;
    private ArrayList<Check> checks;
    boolean isAdvanced = true;
    private ArrayList<Check> isActiveChecks;
    private CheckRasResult chkRasResult;
    String vahedPul = C_Setting.getStringValue(C_Setting.APP_CURRENCY);

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    public static CheckRasResult calculateRasCheck(ArrayList<Check> checks) {
        CheckRasResult checkRasResult = new CheckRasResult();
        double dateByPriceSum = 0;
        ArrayList<Long> startDates = new ArrayList();
        //
        for (int i = 0; i < checks.size(); i++) {
            if (checks.get(i).isCheckIsActive()) {
                checkRasResult.priceSum += checks.get(i).getPrice();
                startDates.add(checks.get(i).getOffsetDate());
                long dateDifferenceByDate = (checks.get(i).getDate() - checks.get(i).getOffsetDate()) / 86400000;
                dateByPriceSum += dateDifferenceByDate * Math.max(1, checks.get(i).getPrice());
                checkRasResult.checkCount++;
            }
        }

        long min = Modules.getMinimum(startDates);
        checkRasResult.startDate = min;
//        PersianDate pc = new PersianDate(checkRasResult.startDate);
        checkRasResult.dayCount = (int) Math.floor(dateByPriceSum / Math.max(checkRasResult.priceSum, 1));
        long headDate = checkRasResult.startDate + (checkRasResult.dayCount * (long) 86400000);
        long headDateResult = Modules.startOfDay(headDate);
        checkRasResult.headDate = headDateResult;
//        pc.setTime(checkRasResult.headDate);
        return checkRasResult;
    }

    private long calculateBahre(long price, double percent, int dayCount) {
        double monthCount = (float) dayCount / (float) 30;
        double percentage = price * percent / 100;
        return Math.round(monthCount * percentage);
    }

    private long calculateKasreBahreAzMablagh(long price, double bahre) {
        return Math.round(price - bahre);
    }

    private void shareChequesPdf() {
        try {
            PersianDate persianCalendar = new PersianDate();
            checkGroup.setCheckExportDate(persianCalendar.getTime());
            db.updateCheckGroup(checkGroup);
            Modules.createChequePdf(AddCheckActivityFinal.this, chkRasResult, checkGroup, isActiveChecks, percent, calculateBahre(chkRasResult.priceSum, percent, chkRasResult.dayCount));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri pdfUri = Uri.parse("file://" + C_Setting.getStringValue(C_Setting.GET_CHECK_PDF_PATH));
        Log.d("getPdfPath", "file://" + C_Setting.getStringValue(C_Setting.GET_CHECK_PDF_PATH));
        sharingIntent.setType("application/*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, pdfUri);
        startActivity(Intent.createChooser(sharingIntent, "Share PDF using"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_check_page_final);
        db = new DBManager(AddCheckActivityFinal.this);
        checkAdapter = new CheckAdapter();
        checkListAdapter = new CheckListAdapter();
        checkGroupsList = db.selectAllCheckGroups();
        if (getIntent().hasExtra("isAdvanced")) {
            isAdvanced = getIntent().getBooleanExtra("isAdvanced", false);
        }
        // ----------
        long offsetDate;
        if (isAdvanced) {
            offsetDate = 0;
        } else {
            offsetDate = (long) getIntent().getSerializableExtra("offsetDate");
        }
        if (CheckListAdapter.checkGroupId == 0) {
            String name = "گروه " + (checkGroupsList.size() + 1);
            checkGroup = new CheckGroup(name, offsetDate, isAdvanced, percent);
        } else {
            checkGroup = db.selectCheckGroupById(CheckListAdapter.checkGroupId);
            checkGroup.setGroupOffset(offsetDate);
        }
//        checkGroup.setId(CheckListAdapter.checkGroupId);
        // ----------
        checks = (ArrayList<Check>) getIntent().getSerializableExtra("CheckList");
        //
        isActiveChecks = new ArrayList<>();
        for (int i = 0; i < checks.size(); i++) {
            if (checks.get(i).isCheckIsActive()) {
                isActiveChecks.add(checks.get(i));
            }
        }
        //
        numberOfChecks = findViewById(R.id.number_of_checks);
        priceOfResult = findViewById(R.id.price_of_result);
        dayCounts = findViewById(R.id.add_check_final_page_number_of_days);
        saveBtn = findViewById(R.id.save_checks_group);
        createPdfBtn = findViewById(R.id.create_checks_pdf);
        shareBtn = findViewById(R.id.share_checks);
        formulaRelativeLayout = findViewById(R.id.add_check_final_formula_layout);
        homeLayout = findViewById(R.id.add_check_final_activity_home_layout);
        beforeOrAfter = findViewById(R.id.add_check_final_page_before_after);
        darsadeBahre = findViewById(R.id.bahre_value);
        plusBtn = findViewById(R.id.plus_button);
        minusBtn = findViewById(R.id.minus_button);
        dateOfChecks = findViewById(R.id.add_check_final_page_date_of_checks);
        bahre = findViewById(R.id.price_of_bahre);
        kasreBahreAzMablagh = findViewById(R.id.kasre_bahre_az_mablagh);
        jameMablaghVahedTextView = findViewById(R.id.price_of_result_vahed_pul);
        kasreBahreAzMablaghTitleTextView = findViewById(R.id.kasre_bahre_az_mablagh_title);
        groupName = findViewById(R.id.add_check_activity_final_group_name);
        groupNameTextView = findViewById(R.id.add_check_activity_final_group_name_text_view);
        beforeOrAfterBox = findViewById(R.id.add_check_final_page_before_after_box);
        //
        if (getIntent().getStringExtra("groupName") != null) {
            groupName.setText(PublicModules.toPersianDigit(getIntent().getStringExtra("groupName")));
            groupNameTextView.setVisibility(View.VISIBLE);
        }
        //
        if (C_Setting.getBooleanValue(C_Setting.JAME_BAHRE_BA_MABLAGH)) {
            kasreBahreAzMablaghTitleTextView.setText("جمع بهره با مبلغ کل:");
        }
        //
        jameMablaghVahedTextView.setText(vahedPul);
        //
        chkRasResult = calculateRasCheck(checks);
        numberOfChecks.setText(PublicModules.toPersianDigit(String.valueOf(chkRasResult.checkCount)));
        String priceSum = String.valueOf(chkRasResult.priceSum);
        priceSum = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(priceSum));
        priceOfResult.setText(PublicModules.toPersianDigit(priceSum));
        dayCounts.setText(PublicModules.toPersianDigit(String.valueOf(chkRasResult.dayCount).replaceAll("-", "")));
        if (chkRasResult.dayCount < 0) {
            beforeOrAfter.setText("قبل");
        } else if (chkRasResult.dayCount > 0) {
            beforeOrAfter.setText("بعد");
        } else if (chkRasResult.dayCount == 0) {
            beforeOrAfterBox.setVisibility(View.GONE);
        }
        final PersianDate pc = new PersianDate(chkRasResult.headDate);
        dateOfChecks.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(pc)));
        if (checkGroup.getCheckInterestPercent() != 0) {
            percent = checkGroup.getCheckInterestPercent();
        }
        darsadeBahre.setText(PublicModules.toPersianDigit(String.format("%1$,.1f", percent)));
        long longResultBahre = calculateBahre(chkRasResult.priceSum, percent, chkRasResult.dayCount);
        String bahreStr = String.valueOf(longResultBahre);
        bahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(bahreStr)) + " " + vahedPul;
        bahre.setText(PublicModules.toPersianDigit(bahreStr));
        if (C_Setting.getBooleanValue(C_Setting.JAME_BAHRE_BA_MABLAGH)) {
            long priceMenhayBahre = chkRasResult.priceSum + longResultBahre;
            String priceMenhayBahreStr = String.valueOf(priceMenhayBahre);
            priceMenhayBahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(priceMenhayBahreStr)) + " " + vahedPul;
            kasreBahreAzMablagh.setText(PublicModules.toPersianDigit(priceMenhayBahreStr));
        } else {
            long priceMenhayBahre = calculateKasreBahreAzMablagh(chkRasResult.priceSum, longResultBahre);
            String priceMenhayBahreStr = String.valueOf(priceMenhayBahre);
            priceMenhayBahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(priceMenhayBahreStr)) + " " + vahedPul;
            kasreBahreAzMablagh.setText(PublicModules.toPersianDigit(priceMenhayBahreStr));
        }
        //
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.equals(saveBtn)) {
                    if (AddCheckActivity1.changeItself) {
                        final Dialog parentDialog = new Dialog(AddCheckActivityFinal.this);
                        parentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        parentDialog.setContentView(R.layout.dialog_save_check_group);
                        parentDialog.setCanceledOnTouchOutside(false);
                        parentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        parentDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                        parentDialog.show();
                        //
                        newGroup = parentDialog.findViewById(R.id.dialog_save_check_group_new_group);
                        itselfGroup = parentDialog.findViewById(R.id.dialog_save_check_group_itself);
                        //
                        newGroup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final Dialog dialog = new Dialog(AddCheckActivityFinal.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.dialog_enter_group_name);
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

                                dialogText = dialog.findViewById(R.id.dialog_enter_group_name_text);
                                dialogText.setText("لطفا نام گروه چک های خود را وارد کنید.");
                                dialog.show();
                                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialogInterface) {
                                        Modules.hideSoftKeyboard(AddCheckActivityFinal.this);
                                    }
                                });
                                //
                                okBtn = dialog.findViewById(R.id.dialog_group_name_ok_button);
                                autoNameBtn = dialog.findViewById(R.id.dialog_group_name_auto_button);
                                enteredName = dialog.findViewById(R.id.dialog_group_name_edit_text);
                                enteredName.setImeOptions(EditorInfo.IME_ACTION_DONE);
                                enteredName.setHint("گروه " + PublicModules.toPersianDigit(String.valueOf((checkGroupsList.size() + 1))));
                                //
                                enteredName.requestFocus();
                                Modules.showSoftKeyboard(AddCheckActivityFinal.this);
                                //
                                okBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String name = enteredName.getText().toString();
                                        if (enteredName.getText().toString().equals("")) {
                                            DialogUtils.makeMessageDialog(AddCheckActivityFinal.this, DialogUtils.MessageDialogTypes.ERROR, "خطا", "نام گروه نمی تواند خالی باشد", "").show();
                                        } else {
                                            checkGroup.setName(PublicModules.toEnglishDigit(name));
                                            checkGroup.setCheckExportDate(0);
                                            int id = db.insertCheckGroup(checkGroup);
                                            for (int i = 0; i < checks.size(); i++) {
                                                checks.get(i).setGroupId(id);
                                                db.insertCheck(checks.get(i));
                                            }
                                            groupName.setText(PublicModules.toPersianDigit(name));
                                            groupNameTextView.setVisibility(View.VISIBLE);
                                            dialog.dismiss();
                                            parentDialog.dismiss();
                                            AddCheckActivity1.arrayString = AddCheckActivity1.checkSum(checks);
                                            hideSoftKeyboard(AddCheckActivityFinal.this);
                                        }
                                    }
                                });
                                //
                                autoNameBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String name = "گروه " + (checkGroupsList.size() + 1);
                                        checkGroup.setName(PublicModules.toEnglishDigit(name));
                                        checkGroup.setCheckExportDate(0);
                                        int id = db.insertCheckGroup(checkGroup);
                                        for (int i = 0; i < checks.size(); i++) {
                                            checks.get(i).setGroupId(id);
                                            db.insertCheck(checks.get(i));
                                        }
                                        groupName.setText(PublicModules.toPersianDigit(name));
                                        groupNameTextView.setVisibility(View.VISIBLE);
                                        dialog.dismiss();
                                        parentDialog.dismiss();
                                        AddCheckActivity1.arrayString = AddCheckActivity1.checkSum(checks);
                                        hideSoftKeyboard(AddCheckActivityFinal.this);
                                    }
                                });
                            }

                        });
                        //
                        itselfGroup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                db.updateCheckGroup(checkGroup);
                                //
                                db.deleteCheckByChkId(checkGroup.getChkId());
                                for (int i = 0; i < checks.size(); i++) {
                                    checks.get(i).setGroupId(checkGroup.getChkId());
                                    db.insertCheck(checks.get(i));
                                }
                                //
                                groupName.setText(PublicModules.toPersianDigit(checkGroup.getName()));
                                groupNameTextView.setVisibility(View.VISIBLE);
                                parentDialog.dismiss();
                                AddCheckActivity1.arrayString = AddCheckActivity1.checkSum(checks);
                            }
                        });
                    } else {

                        final Dialog parentDialog = new Dialog(AddCheckActivityFinal.this);
                        parentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        parentDialog.setContentView(R.layout.dialog_enter_group_name);
                        parentDialog.setCanceledOnTouchOutside(false);
                        parentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        parentDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

                        dialogText = parentDialog.findViewById(R.id.dialog_enter_group_name_text);
                        dialogText.setText("لطفا نام گروه چک های خود را وارد کنید.");
                        parentDialog.show();
                        parentDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                Modules.hideSoftKeyboard(AddCheckActivityFinal.this);
                            }
                        });
                        //
                        okBtn = parentDialog.findViewById(R.id.dialog_group_name_ok_button);
                        autoNameBtn = parentDialog.findViewById(R.id.dialog_group_name_auto_button);
                        enteredName = parentDialog.findViewById(R.id.dialog_group_name_edit_text);
                        enteredName.setImeOptions(EditorInfo.IME_ACTION_DONE);
                        enteredName.setHint("گروه " + PublicModules.toPersianDigit(String.valueOf((checkGroupsList.size() + 1))));
                        //
                        enteredName.requestFocus();
                        Modules.showSoftKeyboard(AddCheckActivityFinal.this);
                        //
                        okBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String name = enteredName.getText().toString();
                                if (enteredName.getText().toString().equals("")) {
                                    DialogUtils.makeMessageDialog(AddCheckActivityFinal.this, DialogUtils.MessageDialogTypes.ERROR, "خطا", "نام گروه نمی تواند خالی باشد", "").show();
                                } else {
                                    checkGroup.setName(PublicModules.toEnglishDigit(name));
                                    int id = db.insertCheckGroup(checkGroup);
                                    for (int i = 0; i < checks.size(); i++) {
                                        checks.get(i).setGroupId(id);
                                        db.insertCheck(checks.get(i));
                                    }
                                    groupName.setText(PublicModules.toPersianDigit(name));
                                    groupNameTextView.setVisibility(View.VISIBLE);
                                    parentDialog.dismiss();
                                    AddCheckActivity1.arrayString = AddCheckActivity1.checkSum(checks);
                                    hideSoftKeyboard(AddCheckActivityFinal.this);
                                }
                            }
                        });
                        //
                        autoNameBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String name = "گروه " + (checkGroupsList.size() + 1);
                                checkGroup.setName(PublicModules.toEnglishDigit(name));
                                int id = db.insertCheckGroup(checkGroup);
                                for (int i = 0; i < checks.size(); i++) {
                                    checks.get(i).setGroupId(id);
                                    db.insertCheck(checks.get(i));
                                }
                                groupName.setText(PublicModules.toPersianDigit(name));
                                groupNameTextView.setVisibility(View.VISIBLE);
                                parentDialog.dismiss();
                                AddCheckActivity1.arrayString = AddCheckActivity1.checkSum(checks);
                                hideSoftKeyboard(AddCheckActivityFinal.this);
                            }
                        });
                    }
                }
                if (v.equals(formulaRelativeLayout)) {
                    Dialog dialog = new Dialog(AddCheckActivityFinal.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_add_check_final_formula);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.show();
                }
                //TODO onActivityResult va code permission pak beshe
                if (v.equals(createPdfBtn)) {
                    DialogUtils.loadingDialog(AddCheckActivityFinal.this).show();
                    if (ContextCompat.checkSelfPermission(AddCheckActivityFinal.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        PublicDialogs.permissionExplainDialog(AddCheckActivityFinal.this, "ذخیره PDF", new PublicDialogs.PermissionExplainCallBack() {
                            @Override
                            public void ok() {
                                String[] permissions = {android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                Permissions.check(AddCheckActivityFinal.this, permissions, null, null, new PermissionHandler() {
                                    @Override
                                    public void onGranted() {
                                        shareChequesPdf();
                                    }

                                    @Override
                                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                                        PublicDialogs.permissionExplainDialog(AddCheckActivityFinal.this, "ذخیره PDF", new PublicDialogs.PermissionExplainCallBack() {
                                            @Override
                                            public void ok() {
                                                //do nothing
                                            }
                                        }).show();
                                    }

                                    @Override
                                    public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {
                                    }

                                    @Override
                                    public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                                        new PublicDialogs.AlwaysDeny(AddCheckActivityFinal.this, "خروجی", "با فعال کردن گزینه Storage از منوی Permissions مربوط به برنامه یادآورچک امکان خروجی گرفتن را فعال کنید.").show();
                                        return true;
                                    }
                                });
                            }
                        }).show();
                    } else {
                        shareChequesPdf();
                    }
                }
                //TODO
                if (v.equals(shareBtn)) {
                    final Dialog dialog = new Dialog(AddCheckActivityFinal.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_share_text);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

                    shareTextResult = dialog.findViewById(R.id.dialog_share_text_result);
                    shareTextResultAndList = dialog.findViewById(R.id.dialog_share_text_result_and_list);
                    shareTextDialogCancel = dialog.findViewById(R.id.dialog_share_text_cancel);
                    View.OnClickListener listener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (v.equals(shareTextResult)) {
                                String chequesSum = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(chkRasResult.priceSum)));
                                PersianDate startDate = new PersianDate(chkRasResult.startDate);
                                PersianDate headDate = new PersianDate(chkRasResult.headDate);
                                String rasdateString = "";
                                if (chkRasResult.dayCount < 0) {
                                    rasdateString = Math.abs(chkRasResult.dayCount) + " روز" + " قبل ،";
                                } else if (chkRasResult.dayCount > 0) {
                                    rasdateString = Math.abs(chkRasResult.dayCount) + " روز" + " بعد ،";
                                } else if (chkRasResult.dayCount == 0) {
                                    rasdateString = Math.abs(chkRasResult.dayCount) + " روز";
                                }

                                Intent sendIntent = new Intent();
                                String export = "تعداد چک ها: " + String.valueOf(chkRasResult.checkCount) + " فقره \n"
                                        + "جمع مبلغ: " + chequesSum + " ریال\n"
                                        + "تاریخ مبدأ چک ها: " + Modules.getPersianShortDate(startDate) + "\n"
                                        + "رأس به روز: " + rasdateString + "  معادل: " + Modules.getPersianShortDate(headDate);
                                String fixedExport = persianDigits(export);
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, fixedExport);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                                dialog.dismiss();
                            }
                            if (v.equals(shareTextResultAndList)) {
                                String chequesSum = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(chkRasResult.priceSum)));
                                PersianDate startDate = new PersianDate(chkRasResult.startDate);
                                PersianDate headDate = new PersianDate(chkRasResult.headDate);
                                String rasdateString = "";
                                if (chkRasResult.dayCount < 0) {
                                    rasdateString = Math.abs(chkRasResult.dayCount) + " روز" + " قبل ،";
                                } else if (chkRasResult.dayCount > 0) {
                                    rasdateString = Math.abs(chkRasResult.dayCount) + " روز" + " بعد ،";
                                } else if (chkRasResult.dayCount == 0) {
                                    rasdateString = Math.abs(chkRasResult.dayCount) + " روز";
                                }
                                Intent sendIntent = new Intent();
                                String export = "تعداد چک ها: " + String.valueOf(chkRasResult.checkCount) + " فقره \n"
                                        + "جمع مبلغ: " + chequesSum + " ریال\n"
                                        + "تاریخ مبدأ چک ها: " + Modules.getPersianShortDate(startDate) + "\n"
                                        + "رأس به روز: " + rasdateString + "  معادل: " + Modules.getPersianShortDate(headDate)
                                        + "\n\n"
                                        + "لیست چک ها :";
                                for (int i = 0; i < checks.size(); i++) {
                                    String formattedPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(checks.get(i).getPrice())));
                                    PersianDate persianCalendar = new PersianDate(checks.get(i).getDate());
                                    export = export + "\n\n"
                                            + String.valueOf(i + 1) + "- "
                                            + "مبلغ : "
                                            + (formattedPrice + "ریال \n")
                                            + ("تاریخ چک : ")
                                            + (Modules.getPersianShortDate(persianCalendar));
                                }
                                String fixedExport = persianDigits(export);
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, fixedExport);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                                dialog.dismiss();
                            }
                            if (v.equals(shareTextDialogCancel)) {
                                dialog.dismiss();
                            }
                        }
                    };
                    shareTextResult.setOnClickListener(listener);
                    shareTextResultAndList.setOnClickListener(listener);
                    shareTextDialogCancel.setOnClickListener(listener);
                    dialog.show();
                }
            }
        };
        //
        saveBtn.setOnClickListener(listener);
        formulaRelativeLayout.setOnClickListener(listener);
        createPdfBtn.setOnClickListener(listener);
        shareBtn.setOnClickListener(listener);


        final CountDownTimer countDownTimer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

            }
        };

        plusBtn.setOnTouchListener(new View.OnTouchListener() {
            Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        countDownTimer.start();
                        mHandler = new Handler();
                        mHandler.postDelayed(plusAction, 160);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(plusAction);
                        mHandler = null;
                        break;
                }
                return false;
            }

            Runnable plusAction = new Runnable() {
                @Override
                public void run() {
                    if (percent <= 7.9) {
                        percent = percent + 0.1;
                        Log.i("percent ziad shod", String.valueOf(percent));
                    }
                    darsadeBahre.setText(PublicModules.toPersianDigit(String.format("%1$,.1f", percent)));
                    long result = calculateBahre(chkRasResult.priceSum, percent, chkRasResult.dayCount);
                    String bahreStr = String.valueOf(result);
                    bahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(bahreStr)) + " " + vahedPul;
                    bahre.setText(PublicModules.toPersianDigit(bahreStr));
                    long priceMenhayBahre = calculateKasreBahreAzMablagh(chkRasResult.priceSum, result);
                    String priceMenhayBahreStr = String.valueOf(priceMenhayBahre);
                    priceMenhayBahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(priceMenhayBahreStr)) + " " + vahedPul;
                    kasreBahreAzMablagh.setText(PublicModules.toPersianDigit(priceMenhayBahreStr));
                    checkGroup.setCheckInterestPercent(percent);
                    mHandler.postDelayed(this, 160);
                }
            };
        });
        minusBtn.setOnTouchListener(new View.OnTouchListener() {
            Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        countDownTimer.start();
                        mHandler = new Handler();
                        mHandler.postDelayed(minusAction, 160);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(minusAction);
                        mHandler = null;
                        break;
                }

                return false;
            }

            Runnable minusAction = new Runnable() {
                @Override
                public void run() {
                    if (percent >= 0) {
                        percent = percent - 0.1;
                        Log.i("percent kam shod", String.valueOf(percent));
                    }
                    darsadeBahre.setText(PublicModules.toPersianDigit(String.format("%1$,.1f", percent).replaceAll("-", "")));
                    long result = calculateBahre(chkRasResult.priceSum, percent, chkRasResult.dayCount);
                    String bahreStr = String.valueOf(result);
                    bahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(bahreStr)) + " " + vahedPul;
                    bahre.setText(PublicModules.toPersianDigit(bahreStr));
                    long priceMenhayBahre = calculateKasreBahreAzMablagh(chkRasResult.priceSum, result);
                    String priceMenhayBahreStr = String.valueOf(priceMenhayBahre);
                    priceMenhayBahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(priceMenhayBahreStr)) + " " + vahedPul;
                    kasreBahreAzMablagh.setText(PublicModules.toPersianDigit(priceMenhayBahreStr));
                    checkGroup.setCheckInterestPercent(percent);
                    mHandler.postDelayed(this, 160);
                }
            };
        });
        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (percent <= 7.9) {
                    percent = percent + 0.1;
                    Log.i("percent ziad shod", String.valueOf(percent));
                }
                darsadeBahre.setText(PublicModules.toPersianDigit(String.format("%1$,.1f", percent)));
                long result = calculateBahre(chkRasResult.priceSum, percent, chkRasResult.dayCount);
                String bahreStr = String.valueOf(result);
                bahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(bahreStr)) + " " + vahedPul;
                bahre.setText(PublicModules.toPersianDigit(bahreStr));
                long priceMenhayBahre = calculateKasreBahreAzMablagh(chkRasResult.priceSum, result);
                String priceMenhayBahreStr = String.valueOf(priceMenhayBahre);
                priceMenhayBahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(priceMenhayBahreStr)) + " " + vahedPul;
                kasreBahreAzMablagh.setText(PublicModules.toPersianDigit(priceMenhayBahreStr));
                checkGroup.setCheckInterestPercent(percent);
            }
        });
        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (percent >= 0) {
                    percent = percent - 0.1;
                    Log.i("percent kam shod", String.valueOf(percent));
                }
                darsadeBahre.setText(PublicModules.toPersianDigit(String.format("%1$,.1f", percent).replaceAll("-", "")));
                long result = calculateBahre(chkRasResult.priceSum, percent, chkRasResult.dayCount);
                String bahreStr = String.valueOf(result);
                bahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(bahreStr)) + " " + vahedPul;
                bahre.setText(PublicModules.toPersianDigit(bahreStr));
                long priceMenhayBahre = calculateKasreBahreAzMablagh(chkRasResult.priceSum, result);
                String priceMenhayBahreStr = String.valueOf(priceMenhayBahre);
                priceMenhayBahreStr = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(priceMenhayBahreStr)) + " " + vahedPul;
                kasreBahreAzMablagh.setText(PublicModules.toPersianDigit(priceMenhayBahreStr));
                checkGroup.setCheckInterestPercent(percent);
            }
        });
        //
        homeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AddCheckActivity1.arrayString.equals(AddCheckActivity1.checkSum(checks))) {
                    Intent intent = new Intent(AddCheckActivityFinal.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("isLogin", true);
                    startActivity(intent);
                } else {
                    DialogUtils.backToHomeDialog(AddCheckActivityFinal.this).show();
                }
            }
        });


    }


}
