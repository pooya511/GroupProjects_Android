package air.gamediesel.rasgir;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

import OOP.LoanResult;
import Other.C_Setting;
import Other.DialogUtils;
import Other.Modules;
import modules.PublicModules;
import org.w3c.dom.Text;

public class CalculateLoanActivity extends BaseActivity {

    private EditText mablagheTasHilat, nerkheSudeSalane, modateBazpardakht;
    private TextWatcher textWatcher;
    private Button calculateBtn;
    private TextView mablaghResult, profitResult, totalProfit;
    private TextView mabTasVahed, mabGhstVahed, sudVahed, aslVahed;
    private TextWatcher persianTextWatcher;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    private LoanResult installmentCalculator(double price, double profitPercent, int months) {

        double x1, x2, x3, x4, x5;
        double totalProfit, totalPrice;
        LoanResult loanResult = new LoanResult();
        if (profitPercent == 0) {
            loanResult.installment = Math.round(price / months);
            loanResult.profit = 0;
            loanResult.finalPrice = price;
            return loanResult;
        }
        x1 = (price / 12) * (profitPercent / 100);
        x2 = Math.pow((1 + (profitPercent / 1200)), months);
        x3 = x2 - 1;
        x4 = x1 * x2 / x3;
        x5 = Math.round(x4);
        totalPrice = x5 * months;
        totalProfit = totalPrice - price;
        loanResult.installment = x5;
        loanResult.profit = totalProfit;
        loanResult.finalPrice = totalPrice;
        return loanResult;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_loan);
        //
        mablagheTasHilat = findViewById(R.id.mablaghe_tasHilat);
        nerkheSudeSalane = findViewById(R.id.nerkhe_sude_salane);
        modateBazpardakht = findViewById(R.id.modate_bazpardakht);
        mablaghResult = findViewById(R.id.loan_activity_mablagh_result);
        profitResult = findViewById(R.id.loan_activity_sud_result);
        totalProfit = findViewById(R.id.loan_activity_totalprofit);
        mabTasVahed = findViewById(R.id.rial_text_view);
        mabGhstVahed = findViewById(R.id.mablagh_rial_text_view);
        sudVahed = findViewById(R.id.sud_rial_text_view);
        aslVahed = findViewById(R.id.asl_va_sud_rial_text_view);
        //
        mabTasVahed.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));
        mabGhstVahed.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));
        sudVahed.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));
        aslVahed.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));

        nerkheSudeSalane.addTextChangedListener(new PersianTextWatcher(nerkheSudeSalane));
        modateBazpardakht.addTextChangedListener(new PersianTextWatcher(modateBazpardakht));

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String userInput = mablagheTasHilat.getText().toString();
                mablagheTasHilat.removeTextChangedListener(textWatcher);

                for (int i = 0; i < userInput.length(); i++) {
                    if (userInput.charAt(i) == ',') {
                        userInput = userInput.replaceAll(",", "");
                    }
                }
                //
                // for thousand separator
                if (!userInput.isEmpty()) {
                    userInput = NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(userInput));
                }
                //
                mablagheTasHilat.setText(PublicModules.toPersianDigit(userInput));
                //
                mablagheTasHilat.setSelection(userInput.length());
                mablagheTasHilat.addTextChangedListener(textWatcher);
            }
        };
        mablagheTasHilat.addTextChangedListener(textWatcher);
        //
        calculateBtn = findViewById(R.id.calculate_btn);
        calculateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mablagheTasHilat.getText().toString().equals("")
                        && !nerkheSudeSalane.getText().toString().equals("")
                        && !modateBazpardakht.getText().toString().equals("")
                        ) {
                    String mt = PublicModules.toEnglishDigit(mablagheTasHilat.getText().toString().replaceAll(",", ""));
                    String ns = PublicModules.toEnglishDigit(nerkheSudeSalane.getText().toString().replaceAll(",", ""));
                    String mb = PublicModules.toEnglishDigit(modateBazpardakht.getText().toString().replaceAll(",", ""));
                    LoanResult loanResult = installmentCalculator(Double.parseDouble(mt)
                            , Double.parseDouble(ns)
                            , Integer.parseInt(mb));
                    //
                    String installment = String.valueOf(loanResult.installment);
                    installment = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(installment));
                    mablaghResult.setTextColor(Color.BLACK);
                    mablaghResult.setText(PublicModules.toPersianDigit(installment));
                    //
                    String profit = String.valueOf(loanResult.profit);
                    profit = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(profit));
                    profitResult.setTextColor(Color.BLACK);
                    profitResult.setText(PublicModules.toPersianDigit(profit));
                    //
                    String totalPrice = String.valueOf(loanResult.finalPrice);
                    totalPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(totalPrice));
                    totalProfit.setTextColor(Color.BLACK);
                    totalProfit.setText(PublicModules.toPersianDigit(totalPrice));
                } else {
                    Modules.customToast(CalculateLoanActivity.this, "خطا", "لطفا همه ی فیلد ها را پر کنید.", false);
                }

            }
        });
        LinearLayout formulaLayout = findViewById(R.id.loan_formula);
        formulaLayout.setOnClickListener(v -> {
            Dialog dialog = new Dialog(CalculateLoanActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_loan_formula);
            dialog.setCanceledOnTouchOutside(true);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            //
        });
    }

    //TODO badan bere too module
    class PersianTextWatcher implements TextWatcher {

        private TextView targetTextView;

        public PersianTextWatcher(TextView targetTextView) {
            this.targetTextView = targetTextView;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            targetTextView.removeTextChangedListener(this);
            String targetStr = s.toString();
            targetTextView.setText(PublicModules.toPersianDigit(targetStr));
            targetTextView.addTextChangedListener(this);
        }
    }
}