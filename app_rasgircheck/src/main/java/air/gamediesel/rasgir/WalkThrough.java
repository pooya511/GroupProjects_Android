package air.gamediesel.rasgir;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import adapters.WalkThroughAdapter;
import fragment.WalkThroughFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Parsa on 2018-01-31.
 */

public class WalkThrough extends FragmentActivity {

    private ViewPager viewPager;
    private TextView startApp;
    private View page1, page2, page3, page4, page5 , page6 , page7;

    private void init() {

        viewPager = findViewById(R.id.activity_guid_walkthrough_viewpager);
        startApp = findViewById(R.id.activity_guid_walkthrough_start_app);
        page1 = findViewById(R.id.activity_guid_walkthrough_page1);
        page2 = findViewById(R.id.activity_guid_walkthrough_page2);
        page3 = findViewById(R.id.activity_guid_walkthrough_page3);
        page4 = findViewById(R.id.activity_guid_walkthrough_page4);
        page5 = findViewById(R.id.activity_guid_walkthrough_page5);
        page6 = findViewById(R.id.activity_guid_walkthrough_page6);
        page7 = findViewById(R.id.activity_guid_walkthrough_page7);

    }

    private void setListener() {

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                indicatorHandling(position + 1);

                if (position + 1 == viewPager.getAdapter().getCount())
                    startApp.setVisibility(View.INVISIBLE);
                else
                    startApp.setVisibility(View.VISIBLE);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(startApp)) {
                    for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    if (getIntent().getExtras().getBoolean("fromMain")){
                        Intent intent = new Intent(WalkThrough.this , MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    if (getIntent().getExtras().getBoolean("fromSetting")){
                        finish();
                    }
                }

            }
        };

        startApp.setOnClickListener(listener);

    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guid_walkthrough);

        init();
        setListener();

        viewPager.setAdapter(new WalkThroughAdapter(getSupportFragmentManager(), initFragments()));
        indicatorHandling(1); // init avalie

    }

    @Override
    public void onBackPressed() {

    }

    private static ArrayList<Fragment> initFragments() {

        ArrayList<Fragment> fragments = new ArrayList<>();

        fragments.add(WalkThroughFragment.newInstance(R.drawable.usage_guide_page1,
                "به سادگی چک ها را وارد برنامه کنید و تیک سبز رنگ آن هایی را که نمی خواهید رأس گیری شوند بردارید. در پایین صفحه تعداد و جمع کل نیز نمایش داده میشود" ,
                false));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.usage_guide_page2,
                "اگر تاریخ مرجع رأس گیری شما امروز نیست می توانید آن را تغییر دهید در غیر اینصورت به مرحله بعد بروید" ,
                false));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.usage_guide_page3,
                "در صفحه نهایی می توانید بهره را تغییر دهید رسید چاپی خروجی بگیرید و اطلاعات را به اشتراک بگذارید" ,
                false));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.usage_guide_page4,
                "در رأس گیری فاکتور ها می توانید مدت زمان تسویه هر فاکتور را به ماه و یا به روز مشخص کنید. همچنین با برداشتن تیک هر فاکتور،آن فاکتور در محاسبه نهایی و جمع کل نخواهد بود" ,
                false));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.usage_guide_page5,
                "در رأس گیری پیشرفته می توانید برای گروهی از چک ها تاریخ مرجع متفاوتی در نظر بگیرید. این امکان برای حسابدار ها در محاسبات پیچیده تر کاربرد دارد"
                ,
                false));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.usage_guide_page6,
                "در قسمت تنظیمات می توانید واحد پول برنامه را تغییر دهید. همچنین رمز ورود بگذارید و یا رأس گیری پیشرفته و نمایش خودکار مبلغ و تاریخ را فعال نمایید" ,
                false));

        fragments.add(WalkThroughFragment.newInstance(R.drawable.usage_guide_page7,
                "" ,
                true));

        return fragments;
    }

    private void indicatorHandling(int currentPage) {

        switch (currentPage) {
            case 1:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page6.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page7.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 2:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page6.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page7.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 3:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page6.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page7.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 4:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page6.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page7.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

            case 5:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page6.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page7.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

                case 6:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page6.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                page7.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                break;

                case 7:
                page1.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page2.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page3.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page4.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page5.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page6.setBackgroundResource(R.drawable.sh_walkthrough_empty_indicator);
                page7.setBackgroundResource(R.drawable.sh_walkthrough_full_indicator);
                break;
        }

    }

}