package air.gamediesel.rasgir;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.multidex.MultiDexApplication;


import java.util.Date;

import DB.DBManager;
import Other.C_Setting;
import Other.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MyApplication extends MultiDexApplication {

    public static String currentBase64;

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANYekanMobileRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        //
        C_Setting.init(this); // initialize shared prefrences

        try {
            Constants.APP_VERSION = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (Constants.APP_SOURCE.equals("bazaar")) {
            currentBase64 = Constants.BAZAAR_PAYMENT_BASE64;
        }
        if (Constants.APP_SOURCE.equals("myket")) {
            currentBase64 = Constants.MYKET_PAYMENT_BASE64;
        }
    }


    public static boolean isLicenseValid() {
        if (getLicense() != null) {
            long expireDate = C_Setting.getLongValue(C_Setting.LICENSE_EXPIRE);
            long currentDate = new Date().getTime();
            if (currentDate < expireDate) { // اگر تاریخ انقضا نگذشته بود
                return true; // لایسنس صحیح
            } else {
                return false; // لایسنس منقضی
            }
        } else {
            return false; // لایسنس ندارد
        }
    }

    private static String getLicense() {
        if (C_Setting.getStringValue(C_Setting.LICENSE_DATA) != null && !C_Setting.getStringValue(C_Setting.LICENSE_DATA).equals("")) {
            return C_Setting.getStringValue(C_Setting.LICENSE_DATA);
        } else {
            return null;
        }
    }

}
