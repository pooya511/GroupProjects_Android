package air.gamediesel.rasgir;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.suke.widget.SwitchButton;
import com.taxiapps.txpayment.dialogFragment.Payment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.NetworkInterface;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import Other.C_Setting;
import Other.Constants;
import Other.DialogUtils;
import Other.KeyPadDialog;
import Other.Modules;
import TxAnalytics.TxAnalytics;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;
import modules.PublicModules;

import static TxAnalytics.TxAnalytics.MessageAdd;

public class SettingActivity extends BaseActivity {

    private ConstraintLayout switchLayout1, switchLayout2, switchLayout3, switchLayout4, defaultExpireDate, vahedPul, password, guidLayout, fingerLayout;
    private ConstraintLayout yadavarLayout, aboutLayout, rateLayout, sendMessageOrBug, otherApp, telegramChannel, shareToFriends, latestChange, appIntro;
    private static LinearLayout snackLayout;
    private ConstraintLayout licenseLayout;
    private com.suke.widget.SwitchButton switch1, switch2, switch3, switch4, fingerEnable;
    private TextView modatPishfarz, vahedPulTextView, pinTextView, licenseTextView, modatPishfarzType, appSource;
    private ScrollView scrollView;
    private TextView versionText;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (C_Setting.getStringValue(C_Setting.PIN_CODE) != null && !C_Setting.getStringValue(C_Setting.PIN_CODE).equals("")) {
            pinTextView.setText("تغییر یا حذف رمز ورود");
            if (SplashScreenActivity.fingerPrintIsAvailable) {
                fingerLayout.setVisibility(View.VISIBLE);
            } else {
                fingerLayout.setVisibility(View.GONE);
            }
        } else {
            pinTextView.setText("فعال کردن رمز ورود");
            fingerLayout.setVisibility(View.GONE);
        }
    }

    /**
     * for guid handling and making
     * unique id for devices
     *
     * @return result
     */
    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String macPart = "00" + Integer.toHexString(b & 0xFF);
                    macPart = macPart.length() > 2 ? macPart.substring(macPart.length() - 2) : macPart;
                    res1.append(macPart + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        //
        scrollView = findViewById(R.id.setting_activity_scrollview);
        switchLayout1 = findViewById(R.id.switch_layout_1);
        switchLayout2 = findViewById(R.id.switch_layout_2);
        switchLayout3 = findViewById(R.id.switch_layout_3);
        switchLayout4 = findViewById(R.id.switch_layout_4);
        defaultExpireDate = findViewById(R.id.setting_activity_mpdate_pishfarz);
        vahedPul = findViewById(R.id.setting_activity_namayesh_vahed_pul_layout);
        password = findViewById(R.id.setting_activity_set_password);
        guidLayout = findViewById(R.id.setting_activity_guid);
        yadavarLayout = findViewById(R.id.setting_activity_yadavar_layout);
        aboutLayout = findViewById(R.id.setting_activity_about_taxi_apps);
        switch1 = findViewById(R.id.switch_1);
        switch2 = findViewById(R.id.switch_2);
        switch3 = findViewById(R.id.switch_3);
        switch4 = findViewById(R.id.switch_4);
        modatPishfarz = findViewById(R.id.setting_activity_modate_pishfarz_text_view);
        vahedPulTextView = findViewById(R.id.setting_activity_namayesh_vahed_pul_text_view);
        pinTextView = findViewById(R.id.setting_activity_pin_text_view);
        rateLayout = findViewById(R.id.setting_activity_rate_us);
        sendMessageOrBug = findViewById(R.id.setting_activity_send_message);
        otherApp = findViewById(R.id.setting_activity_other_app);
        telegramChannel = findViewById(R.id.setting_activity_telegram_channel);
        shareToFriends = findViewById(R.id.setting_activity_share_to_friends);
        latestChange = findViewById(R.id.setting_activity_latest_changes);
        licenseTextView = findViewById(R.id.setting_activity_license_textview);
        licenseLayout = findViewById(R.id.setting_activity_license_layout);
        modatPishfarzType = findViewById(R.id.setting_activity_modate_pishfarz_type_text_view);
        snackLayout = findViewById(R.id.setting_activity_snack_layout);
        appIntro = findViewById(R.id.setting_app_intro);
        fingerLayout = findViewById(R.id.setting_activity_finger_layout);
        fingerEnable = findViewById(R.id.finger_switch);
        appSource = findViewById(R.id.app_source);
        versionText = findViewById(R.id.versionText);

        try {
            versionText.setText(PublicModules.toPersianDigit(getPackageManager().getPackageInfo(getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        switch (Constants.APP_SOURCE) {
            case "google":
                appSource.setText("گوگل");
                break;
            case "bazaar":
                appSource.setText("بازار");
                break;
        }
        //
        OverScrollDecoratorHelper.setUpOverScroll(scrollView);
        //
        if (C_Setting.getStringValue(C_Setting.PIN_CODE) != null && !C_Setting.getStringValue(C_Setting.PIN_CODE).equals("")) {
            pinTextView.setText("تغییر یا حذف رمز ورود");
        } else {
            pinTextView.setText("فعال کردن رمز ورود");
        }

        if (C_Setting.getBooleanValue(C_Setting.ADVANCED_RASGIRI_CHECK)) {
            switch1.setChecked(true);
        } else {
            switch1.setChecked(false);
        }

        if (C_Setting.getBooleanValue(C_Setting.RASGIRI_FACTOR_BEDUNE_MABLAGH)) {
            switch2.setChecked(true);
        } else {
            switch2.setChecked(false);
        }

        if (C_Setting.getBooleanValue(C_Setting.NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH)) {
            switch3.setChecked(true);
        } else {
            switch3.setChecked(false);
        }

        if (C_Setting.getBooleanValue(C_Setting.JAME_BAHRE_BA_MABLAGH)) {
            switch4.setChecked(true);
        } else {
            switch4.setChecked(false);
        }

        if (C_Setting.getBooleanValue(C_Setting.ENTER_WITH_FINGER_PRINT)) {
            fingerEnable.setChecked(true);
        } else {
            fingerEnable.setChecked(false);
        }

        modatPishfarz.setText(PublicModules.toPersianDigit(String.valueOf(C_Setting.getIntegerValue(C_Setting.MODAT_TASVIEH_PISHFARZ))));
        modatPishfarzType.setText(C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ));
        //
        vahedPulTextView.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));

        if (C_Setting.getStringValue(C_Setting.LICENSE_DATA) != null && !C_Setting.getStringValue(C_Setting.LICENSE_DATA).equals("")) {
            licenseTextView.setText("مشاهده لایسنس");
        }
        //
        final View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(switchLayout1)) {
                    if (switch1.isChecked()) {
                        switch1.setChecked(false);
                    } else {
                        switch1.setChecked(true);
                    }
                }
                if (v.equals(switchLayout2)) {
                    if (switch2.isChecked()) {
                        switch2.setChecked(false);
                    } else {
                        switch2.setChecked(true);
                    }
                }
                if (v.equals(switchLayout3)) {
                    if (switch3.isChecked()) {
                        switch3.setChecked(false);
                    } else {
                        switch3.setChecked(true);
                    }
                }
                if (v.equals(switchLayout4)) {
                    if (switch4.isChecked()) {
                        switch4.setChecked(false);
                    } else {
                        switch4.setChecked(true);
                    }
                }
                if (v.equals(fingerLayout)) {
                    if (fingerEnable.isChecked())
                        fingerEnable.setChecked(false);
                    else
                        fingerEnable.setChecked(true);
                }
                if (v.equals(defaultExpireDate)) {
                    final Dialog dialog = new Dialog(SettingActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_setting_default_days);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                    dialog.show();
                    //
                    final LinearLayout layoutValue = dialog.findViewById(R.id.dialog_setting_default_days_value_layout);
                    final TextView dayOrMonth = dialog.findViewById(R.id.dialog_setting_day_or_month_text_view);
                    final TextView day = dialog.findViewById(R.id.dialog_setting_day_text_view);
                    final TextView month = dialog.findViewById(R.id.dialog_setting_month_text_view);
                    final TextView accept = dialog.findViewById(R.id.dialog_setting_accept_text_view);
                    final TextView userValue = dialog.findViewById(R.id.dialog_setting_default_days_value_edit_text);
                    //
                    userValue.setText(PublicModules.toPersianDigit(String.valueOf(C_Setting.getIntegerValue(C_Setting.MODAT_TASVIEH_PISHFARZ))));
                    dayOrMonth.setText(C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ));
                    if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("روز")) {
                        day.setBackgroundResource(R.drawable.factor_item_day_month_button_shape_selected);
                        day.setTextColor(Color.WHITE);
                        month.setBackgroundResource(R.drawable.factor_item_day_month_shape_no_selected);
                        month.setTextColor(Color.BLACK);
                    } else if (C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ).equals("ماه")) {
                        month.setBackgroundResource(R.drawable.factor_item_day_month_button_shape_selected);
                        month.setTextColor(Color.WHITE);
                        day.setBackgroundResource(R.drawable.factor_item_day_month_shape_no_selected);
                        day.setTextColor(Color.BLACK);
                    }
                    //
                    final ImageView dismiss = dialog.findViewById(R.id.dialog_setting_dismiss_image);
                    //
                    View.OnClickListener dialogListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (v.equals(layoutValue)) {

                                new KeyPadDialog(SettingActivity.this, PublicModules.toEnglishDigit( userValue.getText().toString()), DialogUtils.keyPadTypes.NORMAL_NUMBER, new KeyPadDialog.KeyPadCallBack() {
                                    @Override
                                    public void call(String price) {
                                        userValue.setText(PublicModules.toPersianDigit(price));
                                    }
                                }).show();

//                                DialogUtils.keyPadDialog(SettingActivity.this, Integer.parseInt(userValue.getText().toString()), DialogUtils.keyPadTypes.NORMAL_NUMBER, new Modules.KeyPadCallBack() {
//                                    @Override
//                                    public void call(String price) {
//                                        userValue.setText(price);
//                                    }
//                                });
                            }
                            if (v.equals(day)) {
                                C_Setting.setStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ, "روز");
                                dayOrMonth.setText(C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ));
                                day.setBackgroundResource(R.drawable.factor_item_day_month_button_shape_selected);
                                day.setTextColor(Color.WHITE);
                                month.setBackgroundResource(R.drawable.factor_item_day_month_shape_no_selected);
                                month.setTextColor(Color.BLACK);
                            }
                            if (v.equals(month)) {
                                C_Setting.setStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ, "ماه");
                                dayOrMonth.setText(C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ));
                                day.setBackgroundResource(R.drawable.factor_item_day_month_shape_no_selected);
                                day.setTextColor(Color.BLACK);
                                month.setBackgroundResource(R.drawable.factor_item_day_month_button_shape_selected);
                                month.setTextColor(Color.WHITE);
                            }
                            if (v.equals(dismiss)) {
                                dialog.dismiss();
                            }
                            if (v.equals(accept)) {
                                C_Setting.setStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ, dayOrMonth.getText().toString());
                                C_Setting.setIntegerValue(C_Setting.MODAT_TASVIEH_PISHFARZ, Integer.parseInt(PublicModules.toEnglishDigit(userValue.getText().toString())));
                                modatPishfarz.setText(PublicModules.toPersianDigit(String.valueOf(C_Setting.getIntegerValue(C_Setting.MODAT_TASVIEH_PISHFARZ))));
                                modatPishfarzType.setText(C_Setting.getStringValue(C_Setting.TYPE_MODAT_TASVIEH_PISHFARZ));
                                dialog.dismiss();
                            }
                        }
                    };
                    //
                    layoutValue.setOnClickListener(dialogListener);
                    day.setOnClickListener(dialogListener);
                    month.setOnClickListener(dialogListener);
                    dayOrMonth.setOnClickListener(dialogListener);
                    dismiss.setOnClickListener(dialogListener);
                    accept.setOnClickListener(dialogListener);
                }
                if (v.equals(vahedPul)) {
                    if (vahedPulTextView.getText().toString().equals("ریال")) {
                        vahedPulTextView.setText("تومان");
                        C_Setting.setStringValue(C_Setting.APP_CURRENCY, "تومان");
                    } else {
                        vahedPulTextView.setText("ریال");
                        C_Setting.setStringValue(C_Setting.APP_CURRENCY, "ريال");
                    }
                }
                if (v.equals(password)) {
                    if (C_Setting.getStringValue(C_Setting.PIN_CODE) != null && !C_Setting.getStringValue(C_Setting.PIN_CODE).equals("")) {

                        final Dialog dialog = new Dialog(SettingActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_pin_code_message);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                        //
                        dialog.show();
                        //
                        final LinearLayout edit = dialog.findViewById(R.id.dialog_pin_code_edit_text_view);
                        final LinearLayout remove = dialog.findViewById(R.id.dialog_pin_code_remove_text_view);
                        final LinearLayout cancel = dialog.findViewById(R.id.dialog_pin_code_cancel_text_view);
                        View.OnTouchListener listener = new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (v.equals(edit) && event.getAction() == MotionEvent.ACTION_DOWN) {
                                    v.setAlpha(0.5f);
                                    Intent intent = new Intent(SettingActivity.this, PasswordActivity.class);
                                    intent.putExtra("userWantToEdit", true);
                                    startActivity(intent);
                                    dialog.dismiss();
                                } else {
                                    v.setAlpha(1);
                                }

                                if (v.equals(remove) && event.getAction() == MotionEvent.ACTION_DOWN) {
                                    v.setAlpha(0.5f);
                                    C_Setting.setStringValue(C_Setting.PIN_CODE, "");
                                    Modules.customToast(SettingActivity.this, "رمز عبور", "عملیات حذف پین با موفقیت انجام شد.", true);
                                    pinTextView.setText("فعال کردن رمز ورود");
                                    fingerLayout.setVisibility(View.GONE);
                                    dialog.dismiss();
                                } else {
                                    v.setAlpha(1);
                                }

                                if (v.equals(cancel) && event.getAction() == MotionEvent.ACTION_DOWN) {
                                    v.setAlpha(0.5f);
                                    dialog.dismiss();
                                } else {
                                    v.setAlpha(1);
                                }
                                return true;
                            }
                        };
                        edit.setOnTouchListener(listener);
                        remove.setOnTouchListener(listener);
                        cancel.setOnTouchListener(listener);
                    } else {
                        Intent intent = new Intent(SettingActivity.this, PasswordActivity.class);
                        intent.putExtra("userWantToRemove", false);
                        startActivity(intent);
                    }
                }
                if (v.equals(guidLayout)) {

                    final Dialog dialog = new Dialog(SettingActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_guid_);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                    dialog.show();
                    //
                    TextView guid = dialog.findViewById(R.id.dialog_guid_text_view);
                    Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
                    guid.setTypeface(typeface);
                    TextView ok = dialog.findViewById(R.id.dialog_ok_text_view);
                    //
                    String id = Modules.macAddressHandling(SettingActivity.this);
                    guid.setText(id);
                    //
                    ok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                v.setAlpha(0.5f);
                                dialog.dismiss();
                            } else {
                                v.setAlpha(1);
                            }
                            return true;
                        }
                    });
                }
                if (v.equals(licenseLayout)) {
                    if (C_Setting.getStringValue(C_Setting.LICENSE_DATA) != null && !C_Setting.getStringValue(C_Setting.LICENSE_DATA).equals("")) {
                        makeTXLicense(SettingActivity.this).show();
                    } else {
                        if (Modules.isNetworkAvailable(SettingActivity.this)) {
                            final String userName = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER).equals("") ? "" : C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                            Payment.newInstance(R.drawable.rasgir_icon,
                                    Constants.APP_ID,
                                    Constants.APP_SOURCE,
                                    Constants.APP_VERSION,
                                    "راس گیر چک",
                                    MyApplication.currentBase64,
                                    "مزایای نسخه کامل",
                                    "- ورود بیش از ۴ عدد چک در راس گیری \n - ورود بیش از ۴ عدد فاکتور",
                                    userName,
                                    Constants.DEVICE_ID,
                                    Constants.SERVER_URL,
                                    Constants.GATEWAY_URL,
                                    new com.taxiapps.txpayment.payment.TX_License.SetLicenseCallback() {
                                        @Override
                                        public void call(long expireDate, String license) {
                                            C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE, expireDate);
                                            C_Setting.setStringValue(C_Setting.LICENSE_DATA, license);

                                            MainActivity.setUIPremium();
                                            setUIPremium();
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    },
                                    new Payment.UserNameCallBack() {
                                        @Override
                                        public void getUserName(String username) {
                                            C_Setting.setStringValue(C_Setting.TX_USER_NUMBER, username);
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    }
                            ).show(getFragmentManager(), "tx_payment");
                        } else {
                            DialogUtils.makeMessageDialog(SettingActivity.this, DialogUtils.MessageDialogTypes.NORMAL, "اینترنت", "لطفا به اینترنت متصل شوید.", "").show();
                        }
                    }
                }
                if (v.equals(yadavarLayout)) {

                    try {
                        String userNumber = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                        MessageAdd("Yadavar",
                                TxAnalytics.MessageTypes.ADCLICK,
                                Constants.APP_ID,
                                Constants.APP_SOURCE,
                                Constants.APP_VERSION,
                                Constants.DEVICE_ID,
                                userNumber,
                                Constants.SERVER_URL,
                                new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                Log.i("MessageAdd", "Not Working");
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                Log.i("MessageAdd", "is Working");
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (Constants.APP_SOURCE.equals("google")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.taxiapps.yadavarecheque"));
                        startActivity(intent);
                    }if (Constants.APP_SOURCE.equals("myket")) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.taxiapps.yadavarecheque"));
//                        startActivity(intent);
                    } else if (Constants.APP_SOURCE.equals("bazaar")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://cafebazaar.ir/app/?id=com.taxiapps.yadavarecheque&ref=share"));
                        startActivity(intent);
                    }
                }
                if (v.equals(aboutLayout)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.taxiapps.org/%D8%AA%D8%A7%DA%A9%D8%B3%DB%8C-%D8%A7%D9%8E%D9%BE%D8%B3/"));
                    startActivity(intent);
                }
                if (v.equals(rateLayout)) {
                    DialogUtils.rateUsDialog(SettingActivity.this).show();
                }
                if (v.equals(sendMessageOrBug)) {
                    PublicModules.contactUsByEmail(SettingActivity.this , "راس گیر چک" , Constants.APP_VERSION);
                }
                if (v.equals(otherApp)) {
                    switch (Constants.APP_SOURCE) {
                        case "google":
                            Intent intention = new Intent(Intent.ACTION_VIEW);
                            intention.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Sayeh+Co."));
                            startActivity(intention);
                            break;
                        case "bazaar":
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("https://cafebazaar.ir/developer/173176965686/" + Constants.BAZAAR_DEVELOPER_ID));
                            intent.setPackage("com.farsitel.bazaar");
                            startActivity(intent);
                            break;
                    }
                }
                if (v.equals(shareToFriends)) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            "اپلیکیشن \"راس گیر چک سایه\" مناسب و ضروری برای اهالی کسب و کار. اپی که کارش راس گیری و محاسبه بهره چک و تاریخ راس فاکتور هاست. بر رویGoogle Play و  AppStore موجود است و می تونی از لینک زیر دانلودش کنی: " +
                                    " \n https://play.google.com/store/apps/details?id=air.gamediesel.rasgir&rdid=air.gamediesel.rasgir");
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
                if (v.equals(latestChange)) {
                    DialogUtils.latestChange(SettingActivity.this).show();
                }
                if (v.equals(telegramChannel)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/taxiapps"));
                    startActivity(intent);
                }
                if (v.equals(appIntro)) {
                    Intent intent = new Intent(SettingActivity.this, WalkThrough.class);
                    intent.putExtra("fromSetting", true);
                    startActivity(intent);
//                    finish();
                }
            }
        };
        //
        switchLayout1.setOnClickListener(listener);
        switchLayout2.setOnClickListener(listener);
        switchLayout3.setOnClickListener(listener);
        switchLayout4.setOnClickListener(listener);
        defaultExpireDate.setOnClickListener(listener);
        vahedPul.setOnClickListener(listener);
        vahedPulTextView.setOnClickListener(listener);
        password.setOnClickListener(listener);
        guidLayout.setOnClickListener(listener);
        yadavarLayout.setOnClickListener(listener);
        aboutLayout.setOnClickListener(listener);
        rateLayout.setOnClickListener(listener);
        sendMessageOrBug.setOnClickListener(listener);
        otherApp.setOnClickListener(listener);
        telegramChannel.setOnClickListener(listener);
        shareToFriends.setOnClickListener(listener);
        latestChange.setOnClickListener(listener);
        licenseLayout.setOnClickListener(listener);
        appIntro.setOnClickListener(listener);
        fingerLayout.setOnClickListener(listener);
        //
        switch1.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (view.isChecked()) {
                    C_Setting.setBooleanValue(C_Setting.ADVANCED_RASGIRI_CHECK, true);
                } else {
                    C_Setting.setBooleanValue(C_Setting.ADVANCED_RASGIRI_CHECK, false);
                }
            }
        });
        //
        switch2.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (view.isChecked()) {
                    C_Setting.setBooleanValue(C_Setting.RASGIRI_FACTOR_BEDUNE_MABLAGH, true);
                } else {
                    C_Setting.setBooleanValue(C_Setting.RASGIRI_FACTOR_BEDUNE_MABLAGH, false);
                }
            }
        });
        //
        switch3.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (view.isChecked()) {
                    C_Setting.setBooleanValue(C_Setting.NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH, true);
                } else {
                    C_Setting.setBooleanValue(C_Setting.NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH, false);
                }
            }
        });
        //
        switch4.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (view.isChecked()) {
                    C_Setting.setBooleanValue(C_Setting.JAME_BAHRE_BA_MABLAGH, true);
                } else {
                    C_Setting.setBooleanValue(C_Setting.JAME_BAHRE_BA_MABLAGH, false);
                }
            }
        });
        //
        fingerEnable.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (view.isChecked()) {
                    C_Setting.setBooleanValue(C_Setting.ENTER_WITH_FINGER_PRINT, true);
                } else {
                    C_Setting.setBooleanValue(C_Setting.ENTER_WITH_FINGER_PRINT, false);
                }
            }
        });
    }

    private Dialog makeTXLicense(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.tx_payment_license, null);
        final Dialog dialog = DialogUtils.makeDialog(context, view, true, SplashScreenActivity.WIDTH, SplashScreenActivity.HEIGHT);
        //
        TextView status = view.findViewById(R.id.tx_payment_license_status);
        TextView date = view.findViewById(R.id.tx_payment_license_date);
        TextView paidPrice = view.findViewById(R.id.tx_payment_license_paid_price);
        TextView buyNumber = view.findViewById(R.id.tx_payment_license_buy_number);
        TextView limitation = view.findViewById(R.id.tx_payment_license_limitation);
        TextView username = view.findViewById(R.id.tx_payment_license_username);
        final TextView exit = view.findViewById(R.id.tx_payment_license_exit);
        //
        String license = C_Setting.getStringValue(C_Setting.LICENSE_DATA);
        if (!license.equals("")) {
            try {
                JSONObject jsonObject = new JSONObject(Modules.makeJsonString(C_Setting.getStringValue(C_Setting.LICENSE_DATA)));
                status.setText("خرید موفق لایسنس راس گیر چک");
                date.setText(jsonObject.getString("date_fa"));
                String price = Modules.persianDigitToEnglishDigit(jsonObject.getString("amount")).replaceAll(",", "").replace("ریال", "").replace("تومان", "").replace("٬", "");
                if (!price.equals("")) {
                    price = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(price) / 10); // separator
                    paidPrice.setText(price.concat(" تومان"));
                }
                buyNumber.setText(jsonObject.getString("pcode"));
                limitation.setText(jsonObject.getString("description"));
                if (jsonObject.getString("username") != null && !jsonObject.getString("username").equals(""))
                    username.setText(jsonObject.getString("username"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //on click listeners
        View.OnClickListener listener = new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if (v.equals(exit)) {
                    exit.setTextColor(R.color.blue);
                    dialog.dismiss();
                }
            }
        };
        //set onclick listeners
        exit.setOnClickListener(listener);
        //
        return dialog;
    }

    private void setUIPremium() {
        licenseTextView.setText("مشاهده لایسنس");
    }
}