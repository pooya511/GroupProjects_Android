package air.gamediesel.rasgir;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.ArrayList;

import OOP.Check;
import Other.DialogUtils;
import Other.Modules;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

public class AddCheckActivity2 extends BaseActivity {

    private Button changeButton, calculateBtn;
    private TextView dateText;

    private DialogFragment datePicker;
    private PersianDate persianCalender;
    private ArrayList<Check> checks;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    private void setOffsetDates(ArrayList<Check> checks, PersianDate persianDate) {
        for (int i = 0; i < checks.size(); i++) {
            checks.get(i).setOffsetDate(Modules.startOfDay(persianDate.getTime()));
            Log.i("check", String.valueOf(checks.get(i).getOffsetDate()));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_check_page2);
        persianCalender = new PersianDate();
        //
        dateText = findViewById(R.id.add_check_activity2_date_text);
        checks = (ArrayList<Check>) getIntent().getSerializableExtra("CheckList");
        //
        if (checks.get(0).getOffsetDate() != 0) {
            persianCalender = new PersianDate(checks.get(0).getOffsetDate());
            dateText.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalender)));
        } else {
            dateText.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalender)));
        }
        //
        changeButton = findViewById(R.id.change_button);
        changeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    changeButton.setAlpha(0.5f);
                    datePicker = dialogFragment.DatePicker.newInstance(stringToDate(PublicModules.toEnglishDigit(String.valueOf(dateText.getText()))), "تاریخ چک را وارد کنید", new dialogFragment.DatePicker.DatePickerCallBack() {
                        @Override
                        public void call(PersianDate persianCalendar) {
                            dateText.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
                            setOffsetDates(checks, persianCalendar);
                            datePicker.dismiss();
                        }

                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }
                    });
                    datePicker.show(getFragmentManager(), "");
                } else {
                    changeButton.setAlpha(1.0f);
                }
                return true;
            }
        });
        //
        calculateBtn = findViewById(R.id.next_button_check_page2);
        calculateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddCheckActivity2.this, AddCheckActivityFinal.class);
                ArrayList<Check> checks = (ArrayList<Check>) getIntent().getSerializableExtra("CheckList");
                intent.putExtra("CheckList", checks);
                intent.putExtra("offsetDate", persianCalender.getTime());
                intent.putExtra("isAdvanced", false);
                if (getIntent().getStringExtra("groupName") != null) {
                    intent.putExtra("groupName", getIntent().getStringExtra("groupName"));
                }
                startActivity(intent);
            }
        });
        //
        setOffsetDates(checks, persianCalender);
    }

    private long stringToDate(String s) {
        String[] dateSplitArray = s.split("/");
        PersianDate newDate = new PersianDate();
        newDate.setShDay(Integer.valueOf(dateSplitArray[2]));
        newDate.setShMonth(Integer.valueOf(dateSplitArray[1]));
        newDate.setShYear(Integer.valueOf(dateSplitArray[0]));
        return newDate.getTime();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
