package air.gamediesel.rasgir;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcel;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import DB.DBManager;
import OOP.CheckGroup;
import OOP.FactorGroup;
import Other.C_Setting;
import Other.Constants;
import Other.DialogUtils;
import Other.Modules;
import Other.PopUpWelcomeManager;
import adapters.CheckListAdapter;
import adapters.FactorListAdapter;
import modules.PublicDialogs;
import modules.PublicModules;

import static TxAnalytics.TxAnalytics.AppUse;
import static TxAnalytics.TxAnalytics.VersionCheck;

public class MainActivity extends BaseActivity {

    private DBManager db;
    private ArrayList<CheckGroup> checkGroupList;
    private ArrayList<FactorGroup> factorGroupList;

    public static ImageView buyLayout;
    public static TextView buyText;
    private TextView versionText;

    @Override
    String childClassName() {
        return "MainActivity";
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkGroupList = db.selectAllCheckGroups();
        factorGroupList = db.selectAllFactorGroups();
        //

        if (MyApplication.isLicenseValid()) {
            if (buyLayout != null) {
                buyText.setVisibility(View.INVISIBLE);
                buyLayout.setVisibility(View.INVISIBLE);
            }
        } else {
            if (buyLayout != null) {
                buyText.setVisibility(View.VISIBLE);
                buyLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        db = new DBManager(this);
        super.onCreate(savedInstanceState);

        switch (Constants.APP_SOURCE) {
            case "bazaar":
                if (!appInstalledOrNot(MainActivity.this, "com.farsitel.bazaar"))
                    Constants.APP_SOURCE = "google";
                break;
            case "myket":
                if (!appInstalledOrNot(MainActivity.this, "ir.mservices.market"))
                    Constants.APP_SOURCE = "google";
                break;
        }

        Constants.DEVICE_ID = Modules.macAddressHandling(this);
        try {
            AppUse(Constants.APP_ID,
                    Constants.APP_SOURCE,
                    Constants.APP_VERSION,
                    Constants.DEVICE_ID,
                    Constants.SERVER_URL,
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            Log.i("app use ", "Not working");
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (response.isSuccessful()) {
                                Log.i("app use ", "is working");
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        //
        apiMessages();
        //
        if (C_Setting.getIntegerValue(C_Setting.APP_USE) == 0) {     // کارایی که برنامه در اولین اجرا باید انجام بده
            db.replaceOldDatabases(this, new TX_License.SetLicenseCallback() {
                @Override
                public void call(long expireDate, String license) {
                    C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE, expireDate);
                    C_Setting.setStringValue(C_Setting.LICENSE_DATA, license);

                    setUIPremium();
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {

                }
            }); // جایگزین کردن دیتابیس ها و انتقال لایسنس
            Intent intent = new Intent(MainActivity.this, WalkThrough.class); // نمایش راهنما
            intent.putExtra("fromMain", true);
            startActivity(intent);
            finish();
        }
        //
        int count = C_Setting.getIntegerValue(C_Setting.APP_USE);
        C_Setting.setIntegerValue(C_Setting.APP_USE, count + 1);
        //
        if (C_Setting.getStringValue(C_Setting.PIN_CODE) != null && !C_Setting.getStringValue(C_Setting.PIN_CODE).equals("")) {
            if (!(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("isLogin"))) {
                Intent intent = new Intent(this, PasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("userWantToEnter", true);
                intent.putExtra("isRoot", true);
                startActivity(intent);
                return;
            }
        }
        //
        if (getIntent().hasExtra("i")) {
            runOnUi(getIntent().getStringExtra("popUpTitle"), getIntent().getStringExtra("popUpContent"), getIntent().getStringExtra("popUpURL"));
        } else {
            final PopUpWelcomeManager welcomeManager = new PopUpWelcomeManager();
            new CountDownTimer(2000, 100000) {

                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    welcomeManager.showPopUp(C_Setting.getIntegerValue(C_Setting.APP_USE), MainActivity.this);
                }
            }.start();
        }
        //
        setContentView(R.layout.activity_main);
        buyLayout = findViewById(R.id.buy_complete_version_layout);
        buyText = findViewById(R.id.buy_text);
        versionText = findViewById(R.id.versionText);
        //
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    this.getPackageName(), 0);
            String version = info.versionName;
            versionText.setText(PublicModules.toPersianDigit("نسخه " + version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //
        if (MyApplication.isLicenseValid()) {
            buyText.setVisibility(View.INVISIBLE);
            buyLayout.setVisibility(View.INVISIBLE);
        } else {
            buyText.setVisibility(View.VISIBLE);
            buyLayout.setVisibility(View.VISIBLE);
        }
        //
        checkGroupList = db.selectAllCheckGroups();
        factorGroupList = db.selectAllFactorGroups();
        //
        ImageView calculateLoan = findViewById(R.id.calculateLoanButton);
        calculateLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CalculateLoanActivity.class);
                startActivity(intent);
            }
        });
        // }
        ImageView settingButton = findViewById(R.id.settingButton);
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });
        //
        final ImageView rasgiriChandCheck = findViewById(R.id.manyCheckButton);
        rasgiriChandCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkGroupList.size() != 0) {
                    Intent intent = new Intent(MainActivity.this, ListOfChecksActivity.class);
                    startActivity(intent);
                } else {
                    FactorListAdapter.factorGroupId = 0;
                    Intent intent = new Intent(MainActivity.this, AddCheckActivity1.class);
                    startActivity(intent);
                }
            }
        });
        //
        ImageView chandFactor = findViewById(R.id.manyFactorButton);
        chandFactor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (factorGroupList.size() != 0) {
                    Intent intent = new Intent(MainActivity.this, ListOfFactorsActivity.class);
                    startActivity(intent);
                } else {
                    CheckListAdapter.checkGroupId = 0;
                    Intent intent = new Intent(MainActivity.this, AddFactorActivity1.class);
                    startActivity(intent);
                }
            }
        });
        //
        buyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Modules.isNetworkAvailable(MainActivity.this)) {
                    final String userName = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER).equals("") ? "" : C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                    Payment.newInstance(R.drawable.rasgir_icon,
                            Constants.APP_ID,
                            Constants.APP_SOURCE,
                            Constants.APP_VERSION,
                            "راس گیر چک",
                            MyApplication.currentBase64,
                            "مزایای نسخه کامل",
                            "- ورود بیش از ۴ عدد چک در راس گیری \n - ورود بیش از ۴ عدد فاکتور",
                            userName,
                            Constants.DEVICE_ID,
                            Constants.SERVER_URL,
                            Constants.GATEWAY_URL,
                            new com.taxiapps.txpayment.payment.TX_License.SetLicenseCallback() {
                                @Override
                                public void call(long expireDate, String license) {
                                    C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE, expireDate);
                                    C_Setting.setStringValue(C_Setting.LICENSE_DATA, license);

                                    setUIPremium();
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            },
                            new Payment.UserNameCallBack() {
                                @Override
                                public void getUserName(String username) {
                                    C_Setting.setStringValue(C_Setting.TX_USER_NUMBER, username);
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            }
                    ).show(getFragmentManager(), "tx_payment");
                } else {
                    DialogUtils.makeMessageDialog(MainActivity.this, DialogUtils.MessageDialogTypes.NORMAL, "اینترنت", "لطفا به اینترنت متصل شوید.", "").show();
                }
            }
        });
        ImageView taxiAppsSite = findViewById(R.id.taxi_apps_site_view);
        taxiAppsSite.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setAlpha(0.5f);
                    if (Modules.isNetworkAvailable(MainActivity.this)) {
                        String url = "http://www.taxiapps.org/";
                        Intent intention = new Intent(Intent.ACTION_VIEW);
                        intention.setData(Uri.parse(url));
                        startActivity(intention);
                    } else {
                        DialogUtils.makeMessageDialog(MainActivity.this, DialogUtils.MessageDialogTypes.NORMAL, "اینترنت", "لطفا به اینترنت متصل شوید.", "").show();
                    }
                } else {
                    v.setAlpha(1f);
                }
                return true;
            }
        });

        ImageView tikoWebSite = findViewById(R.id.rasgir_web_layout);
        tikoWebSite.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setAlpha(0.5f);
                    if (Modules.isNetworkAvailable(MainActivity.this)) {
                        String url = "http://tikoweb.com";
                        Intent intention = new Intent(Intent.ACTION_VIEW);
                        intention.setData(Uri.parse(url));
                        startActivity(intention);
                    } else {
                        DialogUtils.makeMessageDialog(MainActivity.this, DialogUtils.MessageDialogTypes.NORMAL, "اینترنت", "لطفا به اینترنت متصل شوید.", "").show();
                    }
                } else {
                    v.setAlpha(1f);
                }
                return true;
            }
        });

    }

    @Override
    public void onBackPressed() {
        exitSureDialog(MainActivity.this).show();
    }

    private void apiMessages() {
        try {
            VersionCheck(Constants.APP_ID,
                    Constants.APP_SOURCE,
                    Constants.APP_VERSION,
                    Constants.DEVICE_ID,
                    Constants.SERVER_URL,
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            //TODO return false
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            String responseStr = response.body().string();
                            Log.i("responseStr", responseStr);
                            try {
                                final JSONObject jsonObject = new JSONObject(Modules.makeJsonString(responseStr));
                                switch (jsonObject.getString("popup_type")) {
                                    case "message":
                                        if (jsonObject.has("welcome_msg"))
                                            if (!jsonObject.getString("welcome_msg").trim().equals("")) {
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            serverMessageDialog(MainActivity.this, "پیام", jsonObject.getString("welcome_msg")).show();
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });
                                                //TODO return true
                                            } else {
                                                //TODO return false
                                            }
                                        break;
                                    case "update":
                                        if (jsonObject.has("welcome_msg"))
                                            if (!jsonObject.getString("welcome_msg").trim().equals("")) {
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            updateDialog(MainActivity.this, "بروزرسانی", jsonObject.getString("welcome_msg"), jsonObject.getString("download_link")).show();
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });
                                                //TODO return true
                                            } else {
                                                //TODO return false
                                            }
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Dialog serverMessageDialog(Context context, String title, String desc) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View serverMessageLayout = inflater.inflate(R.layout.dialog_server_message, null);

        TextView titleText, descText;
        titleText = serverMessageLayout.findViewById(R.id.dialog_server_message_title);
        descText = serverMessageLayout.findViewById(R.id.dialog_server_message_description);

        titleText.setText(title);
        descText.setText(desc);

        TextView close;

        int width = 0, height = 0;

        switch (Modules.screenSize(context)) {

            case 1:
                width = 84;
                height = 30;
                break;

            case 2:
                width = 70;
                height = 20;
                break;

            case 3:
                width = 62;
                height = 17;
                break;
        }

        final Dialog serverMessage = new Dialog(context);
        serverMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        serverMessage.setContentView(serverMessageLayout);
        Window dialogWindow = serverMessage.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //
        int ScreenSizeResults[] = Modules.getWidthAndHeight(context, width, height);
        lp.width = ScreenSizeResults[0];
        lp.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;

        serverMessage.setCanceledOnTouchOutside(true);
        dialogWindow.setAttributes(lp);
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        serverMessage.getWindow().setWindowAnimations(R.style.slideDownDialogAnimation);

        close = serverMessage.findViewById(R.id.dialog_server_message_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serverMessage.dismiss();
            }
        });

        return serverMessage;
    }

    private Dialog updateDialog(Context context, String title, String desc, final String link) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View updateDialogLayout = inflater.inflate(R.layout.dialog_update, null);

        final TextView update, cancel, titleText, descText;
        update = updateDialogLayout.findViewById(R.id.dialog_update_update);
        cancel = updateDialogLayout.findViewById(R.id.dialog_update_cancel);
        titleText = updateDialogLayout.findViewById(R.id.dialog_update_title);
        descText = updateDialogLayout.findViewById(R.id.dialog_update_description);

        titleText.setText(title);
        descText.setText(desc);
        update.setText("بروزرسانی");

        int width = 0, height = 0;

        switch (Modules.screenSize(context)) {

            case 1:
                width = 84;
                height = 25;
                break;

            case 2:
                width = 72;
                height = 19;
                break;

            case 3:
                width = 65;
                height = 16;
                break;

        }

        final Dialog updateDialog = new Dialog(context);
        updateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        updateDialog.setContentView(updateDialogLayout);
        Window dialogWindow = updateDialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //
        int ScreenSizeResults[] = Modules.getWidthAndHeight(context, width, height);
        lp.width = ScreenSizeResults[0];
        lp.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;

        updateDialog.setCanceledOnTouchOutside(true);
        dialogWindow.setAttributes(lp);
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogWindow.setWindowAnimations(R.style.slideDownDialogAnimation);

        View.OnClickListener dialogListeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(cancel))
                    updateDialog.dismiss();

                if (v.equals(update)) {
                    if (!link.trim().equals("")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                        startActivity(browserIntent);
                        updateDialog.dismiss();
                    } else
                        updateDialog.dismiss();
                }
            }
        };

        cancel.setOnClickListener(dialogListeners);
        update.setOnClickListener(dialogListeners);

        return updateDialog;
    }

    public static void setUIPremium() {
        if (MainActivity.buyLayout != null) {
            MainActivity.buyText.setVisibility(View.INVISIBLE);
            MainActivity.buyLayout.setVisibility(View.INVISIBLE);
        }
    }

    private Dialog exitSureDialog(Context context) {

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_allways_deny);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setWindowAnimations(R.style.slideUpDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final TextView exit, cancel, titleText, descText;
        exit = dialog.findViewById(R.id.pop_allways_deny_go_to_setting);
        cancel = dialog.findViewById(R.id.pop_allways_deny_cancel);
        titleText = dialog.findViewById(R.id.pop_always_deny_title);
        descText = dialog.findViewById(R.id.pop_always_deny_description);

        titleText.setText("خروج");
        descText.setText("آیا برای خروج از رأس گیر مطمئن هستید؟");
        exit.setText("خروج");
        cancel.setText("نه");
        exit.setTextColor(Color.RED);


        View.OnClickListener dialogListeners = v -> {

            if (v.equals(cancel)){
                dialog.dismiss();
            }

            if (v.equals(exit)) {
                finish();
            }
        };

        cancel.setOnClickListener(dialogListeners);
        exit.setOnClickListener(dialogListeners);

        return dialog;
    }

    private boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

}
