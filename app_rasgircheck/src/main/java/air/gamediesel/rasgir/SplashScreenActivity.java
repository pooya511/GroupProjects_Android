package air.gamediesel.rasgir;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.mtramin.rxfingerprint.RxFingerprint;

import Other.Modules;

public class SplashScreenActivity extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    public static int WIDTH;
    public static int HEIGHT;

    public static boolean fingerPrintIsAvailable;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

        // for payment Dialogs size
        switch (Modules.screenSize(SplashScreenActivity.this)) {
            case 1:
                WIDTH = 80;
                HEIGHT = 65;
                break;
            case 2:
                WIDTH = 55;
                HEIGHT = 50;
                break;
            case 3:
                WIDTH = 42;
                HEIGHT = 37;
                break;
            default:
                WIDTH = 0;
                HEIGHT = 0;
                break;
        }
        //
        if (RxFingerprint.isAvailable(this))
            fingerPrintIsAvailable = true;
        else
            fingerPrintIsAvailable = false;
    }
}
