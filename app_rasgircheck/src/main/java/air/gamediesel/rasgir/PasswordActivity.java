package air.gamediesel.rasgir;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mtramin.rxfingerprint.RxFingerprint;
import com.mtramin.rxfingerprint.data.FingerprintAuthenticationResult;

import Other.C_Setting;
import Other.DialogUtils;
import Other.Modules;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class PasswordActivity extends BaseActivity {

    private Dialog fingerDialog;
    public static Disposable disposable;

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    ImageButton number1ImageButton, number0ImageButton;
    ImageButton number2ImageButton, number3ImageButton, number4ImageButton, number5ImageButton;
    ImageButton number6ImageButton, number7ImageButton, number8ImageButton, number9ImageButton;
    ImageButton numberResetImageButton, numberBackSpaceImageButton;

    TextView number1TextView, number2TextView, number3TextView, number4TextView, number5TextView;
    TextView number6TextView, number7TextView, number8TextView, number9TextView, number0TextView;
    TextView title, limitText;
    ImageView numberResetTextView, numberBackSpaceTextView, pass1, pass2, pass3, pass4;

    static LinearLayout snackLayout;

    private String password = "";
    private String firstPassword = "";
    private boolean weHave2Pass = false;
    private boolean hasPass = false;
    private CountDownTimer countDownTimer;
    private boolean userWantToRemove;
    private boolean userWantToEnter;
    private boolean userWantToEdit;
    private int counter = 0;
    private CountDownTimer limitCountDown;
    private boolean userHasPermission = true; // کاربر بعد از 3 بار و تا 60 ثانیه نتونه پین بزنه
    private LinearLayout fingerLayout;

    public static void disposeScanner(Disposable disposable) {
        disposable.dispose();
    }

    private String passHandling(String str, String number) {

        if (userHasPermission) {

            if (str.length() <= 4) {
                str = str + number;
            }
            if (str.length() == 0) {
                pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
            }
            if (str.length() == 1) {
                pass1.setBackgroundResource(R.drawable.ic_brightness_1);
                pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
            }
            if (str.length() == 2) {
                pass1.setBackgroundResource(R.drawable.ic_brightness_1);
                pass2.setBackgroundResource(R.drawable.ic_brightness_1);
                pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
            }
            if (str.length() == 3) {
                pass1.setBackgroundResource(R.drawable.ic_brightness_1);
                pass2.setBackgroundResource(R.drawable.ic_brightness_1);
                pass3.setBackgroundResource(R.drawable.ic_brightness_1);
                pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
            }
            if (str.length() == 4) {
                pass1.setBackgroundResource(R.drawable.ic_brightness_1);
                pass2.setBackgroundResource(R.drawable.ic_brightness_1);
                pass3.setBackgroundResource(R.drawable.ic_brightness_1);
                pass4.setBackgroundResource(R.drawable.ic_brightness_1);
                if (hasPass) {
                    if (userWantToRemove) { // کاربر میخواد پینشو پاک کنه
                        String userPass = C_Setting.getStringValue(C_Setting.PIN_CODE);
                        if (str.equals(userPass)) {
                            hasPass = false;
                            pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            C_Setting.setStringValue(C_Setting.PIN_CODE, null);
                            //
                            Modules.customToast(PasswordActivity.this , "رمز عبور" , "عملیات حذف پین با موفقیت انجام شد." , true);
                            //
                            countDownTimer = new CountDownTimer(500, 10000) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                }

                                @Override
                                public void onFinish() {
                                    pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                    pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                    pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                    pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                }
                            };
                            countDownTimer.start();
                            finish();
                        } else {
                            counter++;
                            if (counter == 3) {
                                limitText.setVisibility(View.VISIBLE);
                                limitCountDown.start();
                                userHasPermission = false;
                            }
                            pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            str = "";
                            Modules.customToast(PasswordActivity.this , "خطا" , "رمز ورودی معتبر نیست." , false);
                        }
                    } else if (userWantToEnter) { // کاربر میخواد وارد برنامه شه
                        String userPass = C_Setting.getStringValue(C_Setting.PIN_CODE);
                        if (str.equals(userPass)) {
                            if (getIntent().getExtras().getBoolean("isRoot")) {
                                Intent intent = new Intent(this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("isLogin", true);
                                startActivity(intent);
                                finish();
                            } else {
                                super.onBackPressed();
                            }
                        } else {
                            counter++;
                            if (counter == 3) {
                                limitText.setVisibility(View.VISIBLE);
                                limitCountDown.start();
                                userHasPermission = false;
                            }
                            pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            str = "";
                            //
                            Modules.customToast(PasswordActivity.this , "خطا" , "رمز ورودی معتبر نیست." , false);
                            //
                        }
                    } else if (userWantToEdit) { // زمانی که کاربر میخواد ادیت کنه و فانکشن عادی کار میکنه
                        countDownTimer = new CountDownTimer(500, 10000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            }
                        };
                        countDownTimer.start();
                        //
                        if (!weHave2Pass) {
                            firstPassword = str;
                            str = "";
                            weHave2Pass = true;
                            title.setText("رمز را مجددا وارد نمایید.");
                        } else {
                            if (firstPassword.equals(str)) {
                                C_Setting.setStringValue(C_Setting.PIN_CODE, str);
                                Modules.customToast(PasswordActivity.this , "رمز عبور" , "عملیات فعالسازی پین با موفقیت انجام شد." , true);
                                finish();
                            } else {
                                str = "";
                                pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                                Modules.customToast(PasswordActivity.this , "خطا" , "لطفا رمز اول خود را وارد کنید." , false);
                            }
                        }
                    }
                } else { // برای فعالسازی پین استفاده میشه
                    countDownTimer = new CountDownTimer(500, 10000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                        }
                    };
                    countDownTimer.start();
                    //
                    if (!weHave2Pass) {
                        firstPassword = str;
                        str = "";
                        weHave2Pass = true;
                        title.setText("رمز را مجددا وارد نمایید.");
                    } else {
                        if (firstPassword.equals(str)) {
                            C_Setting.setStringValue(C_Setting.PIN_CODE, str);
                            Modules.customToast(PasswordActivity.this , "رمز عبور" , "عملیات فعالسازی پین با موفقیت انجام شد." , true);
                            finish();
                        } else {
                            str = "";
                            pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                            Modules.customToast(PasswordActivity.this , "خطا" , "لطفا رمز اول خود را وارد کنید." , false);
                        }
                    }
                }
            }
        }
        return str;
    }

    private void fingerPrintHandling() {

        disposable = RxFingerprint.authenticate(this)
                .subscribe(new io.reactivex.functions.Consumer<FingerprintAuthenticationResult>() {
                    @Override
                    public void accept(@NonNull FingerprintAuthenticationResult fingerprintAuthenticationResult) throws Exception {
                        switch (fingerprintAuthenticationResult.getResult()) {
                            case FAILED:
                                Modules.customToast(PasswordActivity.this , "خطا" , "اثر انگشت یافت نشد !" , false);
                                break;
                            case AUTHENTICATED:
                                fingerDialog.dismiss();
                                pass1.setBackgroundResource(R.drawable.ic_brightness_1);
                                pass2.setBackgroundResource(R.drawable.ic_brightness_1);
                                pass3.setBackgroundResource(R.drawable.ic_brightness_1);
                                pass4.setBackgroundResource(R.drawable.ic_brightness_1);
                                new CountDownTimer(500, 100000) {

                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        if (getIntent().getExtras().getBoolean("isRoot")) {
                                            Intent intent = new Intent(PasswordActivity.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            intent.putExtra("isLogin", true);
                                            startActivity(intent);
                                            finish();
                                            disposeScanner(disposable);
                                        } else if (getIntent().getExtras().getBoolean("userWantToEnter")) {
                                            PasswordActivity.super.onBackPressed();
                                        }
                                    }
                                }.start();
                                break;
                        }
                    }
                }, new io.reactivex.functions.Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        Log.e("ERROR", "authenticate", throwable);
                    }
                });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        title = findViewById(R.id.password_title);
        //
        userWantToRemove = getIntent().getBooleanExtra("userWantToRemove", false);
        userWantToEnter = getIntent().getBooleanExtra("userWantToEnter", false);
        userWantToEdit = getIntent().getBooleanExtra("userWantToEdit", false);
        //
        fingerLayout = findViewById(R.id.password_activity_finger_print_layout);
        if (SplashScreenActivity.fingerPrintIsAvailable && C_Setting.getBooleanValue(C_Setting.ENTER_WITH_FINGER_PRINT) && userWantToEnter) {
            fingerLayout.setVisibility(View.VISIBLE);
        } else
            fingerLayout.setVisibility(View.GONE);
        //
        if (C_Setting.getBooleanValue(C_Setting.ENTER_WITH_FINGER_PRINT) && userWantToEnter) {
            fingerDialog = DialogUtils.fingerDialog(PasswordActivity.this);
            fingerDialog.show();
            //
            fingerPrintHandling();
        }
        //
        limitCountDown = new CountDownTimer(31000, 1000) {
            @Override
            public void onTick(long l) {
                limitText.setText("تا " +
                        String.valueOf(l / 1000) +
                        " ثانیه دیگر نمی توانید رمز را وارد کنید."
                );

            }

            @Override
            public void onFinish() {
                limitText.setVisibility(View.GONE);
                userHasPermission = true;
                counter = 0;
            }
        };
        //
        if (C_Setting.getStringValue(C_Setting.PIN_CODE) != null && !C_Setting.getStringValue(C_Setting.PIN_CODE).equals("")) {
            hasPass = true;
            title.setText("لطفا رمز خود را وارد کنید.");
        }
        //
        limitText = findViewById(R.id.limitation_text);
        //
        number1ImageButton = findViewById(R.id.password_input_number1_layout);
        number2ImageButton = findViewById(R.id.password_input_number2_layout);
        number3ImageButton = findViewById(R.id.password_input_number3_layout);
        number4ImageButton = findViewById(R.id.password_input_number4_layout);
        number5ImageButton = findViewById(R.id.password_input_number5_layout);
        number6ImageButton = findViewById(R.id.password_input_number6_layout);
        number7ImageButton = findViewById(R.id.password_input_number7_layout);
        number8ImageButton = findViewById(R.id.password_input_number8_layout);
        number9ImageButton = findViewById(R.id.password_input_number9_layout);
        number0ImageButton = findViewById(R.id.password_input_number0_layout);
        numberResetImageButton = findViewById(R.id.password_input_reset_layout);
        numberBackSpaceImageButton = findViewById(R.id.password_input_back_space_layout);
        //
        number1TextView = findViewById(R.id.pasword_input_number1_text_view);
        number2TextView = findViewById(R.id.pasword_input_number2_text_view);
        number3TextView = findViewById(R.id.pasword_input_number3_text_view);
        number4TextView = findViewById(R.id.pasword_input_number4_text_view);
        number5TextView = findViewById(R.id.pasword_input_number5_text_view);
        number6TextView = findViewById(R.id.pasword_input_number6_text_view);
        number7TextView = findViewById(R.id.pasword_input_number7_text_view);
        number8TextView = findViewById(R.id.pasword_input_number8_text_view);
        number9TextView = findViewById(R.id.pasword_input_number9_text_view);
        number0TextView = findViewById(R.id.pasword_input_number0_text_view);
        numberResetTextView = findViewById(R.id.pasword_input_reset_image_view);
        numberBackSpaceTextView = findViewById(R.id.pasword_input_back_space_image_view);
        //
        pass1 = findViewById(R.id.password_pass1_image_view);
        pass2 = findViewById(R.id.password_pass2_image_view);
        pass3 = findViewById(R.id.password_pass3_image_view);
        pass4 = findViewById(R.id.password_pass4_image_view);
        //
        View.OnTouchListener touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.equals(number1ImageButton)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "1");
                        number1ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number1TextView.setTextColor(Color.BLACK);
                    } else {
                        number1ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number1TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number2ImageButton)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "2");
                        number2ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number2TextView.setTextColor(Color.BLACK);
                    } else {
                        number2ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number2TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number3ImageButton)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "3");
                        number3ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number3TextView.setTextColor(Color.BLACK);
                    } else {
                        number3ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number3TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number4ImageButton)) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "4");
                        number4ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number4TextView.setTextColor(Color.BLACK);
                    } else {
                        number4ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number4TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number5ImageButton)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "5");
                        number5ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number5TextView.setTextColor(Color.BLACK);
                    } else {
                        number5ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number5TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number6ImageButton)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "6");
                        number6ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number6TextView.setTextColor(Color.BLACK);
                    } else {
                        number6ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number6TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number7ImageButton)) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "7");
                        number7ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number7TextView.setTextColor(Color.BLACK);
                    } else {
                        number7ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number7TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number8ImageButton)) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "8");
                        number8ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number8TextView.setTextColor(Color.BLACK);
                    } else {
                        number8ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number8TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number9ImageButton)) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "9");
                        number9ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number9TextView.setTextColor(Color.BLACK);
                    } else {
                        number9ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number9TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(number0ImageButton)) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = passHandling(password, "0");
                        number0ImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        number0TextView.setTextColor(Color.BLACK);
                    } else {
                        number0ImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        number0TextView.setTextColor(Color.parseColor("#02ABB2"));
                    }
                }
                if (v.equals(numberResetImageButton)) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        password = "";
                        pass1.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                        pass2.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                        pass3.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                        pass4.setBackgroundResource(R.drawable.ic_radio_button_unchecked);
                        numberResetImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        numberResetTextView.setBackgroundResource(R.drawable.ic_replay_black);
                    } else {
                        numberResetImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        numberResetTextView.setBackgroundResource(R.drawable.ic_replay);
                    }
                }
                if (v.equals(numberBackSpaceImageButton)) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        if (password.length() >= 1) {
                            password = password.substring(0, password.length() - 1);
                        } else if (password.length() < 1) {
                            password = "";
                        }
                        passHandling(password, "");
                        numberBackSpaceImageButton.setBackgroundResource(R.drawable.password_input_selected);
                        numberBackSpaceTextView.setBackgroundResource(R.drawable.ic_chevron_left_black);
                    } else {
                        numberBackSpaceImageButton.setBackgroundResource(R.drawable.password_inputs_default);
                        numberBackSpaceTextView.setBackgroundResource(R.drawable.ic_chevron_left_mamadeshun);
                    }
                }

                return true;
            }
        };
        number1ImageButton.setOnTouchListener(touchListener);
        number1ImageButton.setOnTouchListener(touchListener);
        number2ImageButton.setOnTouchListener(touchListener);
        number3ImageButton.setOnTouchListener(touchListener);
        number4ImageButton.setOnTouchListener(touchListener);
        number5ImageButton.setOnTouchListener(touchListener);
        number6ImageButton.setOnTouchListener(touchListener);
        number7ImageButton.setOnTouchListener(touchListener);
        number8ImageButton.setOnTouchListener(touchListener);
        number9ImageButton.setOnTouchListener(touchListener);
        number0ImageButton.setOnTouchListener(touchListener);
        numberResetImageButton.setOnTouchListener(touchListener);
        numberBackSpaceImageButton.setOnTouchListener(touchListener);
        //
        fingerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fingerDialog = DialogUtils.fingerDialog(PasswordActivity.this);
                fingerDialog.show();
                //
                fingerPrintHandling();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getExtras().getBoolean("onBackDisable")) { // برای زمانی که از بک گراند میاد نباید بتونه بک کنه . اینجا داریم همینو چک میکنیم .

        } else {
            super.onBackPressed();
        }
    }

}
