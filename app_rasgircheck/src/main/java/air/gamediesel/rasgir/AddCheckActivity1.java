package air.gamediesel.rasgir;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taxiapps.txpayment.dialogFragment.Payment;

import java.util.ArrayList;

import OOP.Check;
import Other.C_Setting;
import Other.Constants;
import Other.DialogUtils;
import Other.Modules;
import adapters.CheckAdapter;
import adapters.CheckListAdapter;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class AddCheckActivity1 extends BaseActivity {

    private ArrayList<Check> checkArray;
    private CheckAdapter checkAdapter;
    private Button nextBtn;
    private boolean isEmpty;
    public static boolean changeItself;
    private LinearLayoutManager layoutManager;
    private RecyclerView recycleView;
    private boolean checkGroupIsAdvanced;
    private ImageView report;
    private TextView sumCurrency, paymentText1, paymentText2, paymentText3;

    public static String arrayString;

    public static String checkSum(ArrayList<Check> checks) {

        String chkSum = "";

        for (int i = 0; i < checks.size(); i++) {
            chkSum = String.valueOf(checks.get(i).getPrice()).concat("," + String.valueOf(checks.get(i).getDate()));
        }

        return chkSum;

    }

    @Override
    String childClassName() {
        return getLocalClassName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_check_page1);
        //
        ArrayList<Check> checks = (ArrayList<Check>) getIntent().getSerializableExtra("checkArray");
        checkGroupIsAdvanced = getIntent().getBooleanExtra("checkGroupIsAdvanced", false);
        checkArray = checks;
        if (checkArray == null) {
            checkArray = new ArrayList<>();
        }
        if (checkArray.isEmpty()) {
            checkArray.add(new Check(true));
            if (C_Setting.getBooleanValue(C_Setting.ADVANCED_RASGIRI_CHECK)) {
                checkGroupIsAdvanced = true;
            }
            changeItself = false;
        } else {
            changeItself = true;
        }
        //
        arrayString = checkSum(checkArray);
        //
        nextBtn = findViewById(R.id.next_button_check_page1);
        //
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(nextBtn)) {
                    isEmpty = false;
                    for (int i = 0; i < checkArray.size(); i++) {
                        if (checkArray.get(i).getPrice() == 0
                                || checkArray.get(i).getDate() == 0) {
                            isEmpty = true;
                            Modules.customToast(AddCheckActivity1.this, "خطا", "ابتدا فیلد های خالی را پر کنید.", false);
                        }
                    }
                    if (!isEmpty) {
                        if (checkGroupIsAdvanced) {
                            Intent intent = new Intent(AddCheckActivity1.this, AddCheckActivityFinal.class);
                            intent.putExtra("CheckList", checkArray);
                            if (getIntent().getStringExtra("groupName") != null) {
                                intent.putExtra("groupName", getIntent().getStringExtra("groupName"));
                            }
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(AddCheckActivity1.this, AddCheckActivity2.class);
                            intent.putExtra("CheckList", checkArray);
                            if (getIntent().getStringExtra("groupName") != null) {
                                intent.putExtra("groupName", getIntent().getStringExtra("groupName"));
                            }
                            startActivity(intent);
                        }
                    }
                }
            }
        };
        //
        nextBtn.setOnClickListener(listener);
        //
        recycleView = findViewById(R.id.recycler_view);
        checkAdapter = new CheckAdapter(this, checkArray, checkGroupIsAdvanced);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recycleView.setLayoutManager(layoutManager);
        recycleView.setHasFixedSize(true);
        recycleView.setAdapter(checkAdapter);
        OverScrollDecoratorHelper.setUpOverScroll(recycleView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);

        // ---------------------------------------------------
        LinearLayout addLayout = findViewById(R.id.add_check_page1_plus_layout);
        addLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setAlpha(0.4f);
                    if (checkArray.size() < 4) {
                        Check check = new Check(true);
                        checkArray.add(check);
                        checkAdapter.updateFooter(checkArray);
                        recycleView.scrollToPosition(checkArray.size() - 1);
                    } else {
                        if (MyApplication.isLicenseValid()) {
                            Check check = new Check(true);
                            check.setGroupId(CheckListAdapter.checkGroupId);
                            checkArray.add(check);
                            checkAdapter.updateFooter(checkArray);
                            recycleView.scrollToPosition(checkArray.size() - 1);
                        } else {
                            final String userName = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER).equals("") ? "" : C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                            Payment.newInstance(R.drawable.rasgir_icon,
                                    Constants.APP_ID,
                                    Constants.APP_SOURCE,
                                    Constants.APP_VERSION,
                                    "راس گیر چک",
                                    MyApplication.currentBase64,
                                    "مزایای نسخه کامل",
                                    "- ورود بیش از ۴ عدد چک در راس گیری \n - ورود بیش از ۴ عدد فاکتور",
                                    userName,
                                    Constants.DEVICE_ID,
                                    Constants.SERVER_URL,
                                    Constants.GATEWAY_URL,
                                    new com.taxiapps.txpayment.payment.TX_License.SetLicenseCallback() {
                                        @Override
                                        public void call(long expireDate, String license) {
                                            C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE, expireDate);
                                            C_Setting.setStringValue(C_Setting.LICENSE_DATA, license);

                                            MainActivity.setUIPremium();
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    },
                                    new Payment.UserNameCallBack() {
                                        @Override
                                        public void getUserName(String username) {
                                            C_Setting.setStringValue(C_Setting.TX_USER_NUMBER, username);
                                        }

                                        @Override
                                        public int describeContents() {
                                            return 0;
                                        }

                                        @Override
                                        public void writeToParcel(Parcel dest, int flags) {

                                        }
                                    }
                            ).show(getFragmentManager(), "tx_payment");
                        }
                    }
                    return true;
                } else {
                    v.setAlpha(1f);
                    return false;
                }
            }
        });
        report = findViewById(R.id.add_check_page1_report_image);
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double maximumPrice = checkArray.get(0).getPrice();
                for (int i = 1; i < checkArray.size(); i++) {  // برای به دست آوردن مبلغ ماکزیموم
                    if (checkArray.get(i).getPrice() > maximumPrice) {
                        maximumPrice = checkArray.get(i).getPrice();
                    }

                }
                //
                double minimumPrice = checkArray.get(0).getPrice();
                for (int i = 1; i < checkArray.size(); i++) { // برای به دست آوردن مبلغ مینیموم
                    if (checkArray.get(i).getPrice() < minimumPrice) {
                        minimumPrice = checkArray.get(i).getPrice();
                    }
                }
                //
                long nearestDate = 0;
                for (int i = 0; i < checkArray.size(); i++) {           // برای به دست آوردن نزدیک ترین تاریخ
                    if (checkArray.get(i).getDate() != 0) {
                        nearestDate = nearestDate == 0 ? checkArray.get(i).getDate() : nearestDate;
                        if (checkArray.get(i).getDate() < nearestDate) {
                            nearestDate = checkArray.get(i).getDate();
                        }
                    }
                }
                //
                long hindmostDate = 0;
                for (int i = 0; i < checkArray.size(); i++) { // برای به دست آوردن دور ترین تاریخ
                    if (checkArray.get(i).getDate() != 0) {
                        hindmostDate = hindmostDate == 0 ? checkArray.get(i).getDate() : hindmostDate;
                        if (checkArray.get(i).getDate() > hindmostDate) {
                            hindmostDate = checkArray.get(i).getDate();
                        }
                    }
                }
                DialogUtils.reportChecksAndFactors(AddCheckActivity1.this, nearestDate, hindmostDate, maximumPrice, minimumPrice).show();
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (arrayString.equals(checkSum(checkArray))) {
            super.onBackPressed();
            finish();
        } else {
            DialogUtils.backDialog(AddCheckActivity1.this).show();
        }
    }

}
