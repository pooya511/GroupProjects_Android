package dialogFragment;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import Other.Modules;
import air.gamediesel.rasgir.R;
import fragment.DatePickerFrg;
import saman.zamani.persiandate.PersianDate;

/**
 * Created by Parsa on 2018-02-18.
 */

public class DatePicker extends DialogFragment {

    private TextView ok, cancel, todayDate, descriptionTextView;

    private DatePickerCallBack callBack;
    private PersianDate pCalendar;
    private Fragment dateFragment;

    private FrameLayout fragmentFrame;

    private String description;

    public interface DatePickerCallBack extends Parcelable {
        void call(PersianDate persianDate);
    }

    public static DatePicker newInstance(long currentDate, String description, DatePickerCallBack callBack) {
        Bundle args = new Bundle();
        args.putParcelable("CallBack", callBack);
        args.putLong("CurrentDate", currentDate);
        args.putString("Description", description);
        DatePicker fragment = new DatePicker();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        Window dialogWindow = getDialog().getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        callBack = getArguments().getParcelable("CallBack");
        description = getArguments().getParcelable("Description");

        if (getArguments().getLong("CurrentDate") == 0)
            pCalendar = new PersianDate();
        else
            pCalendar = new PersianDate(getArguments().getLong("CurrentDate"));

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View datePickerLayout = inflater.inflate(R.layout.pop_date_picker, null);

        fragmentFrame = datePickerLayout.findViewById(R.id.pop_date_picker_main_fragment_container);
        ok = datePickerLayout.findViewById(R.id.pop_date_picker_ok);
        cancel = datePickerLayout.findViewById(R.id.pop_date_picker_cancel);
        todayDate = datePickerLayout.findViewById(R.id.pop_time_picker_select_today);
        descriptionTextView = datePickerLayout.findViewById(R.id.pop_date_picker_description_text_view);

        FragmentManager fm = getChildFragmentManager();

        dateFragment = DatePickerFrg.newInstance(pCalendar.getTime());

        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(fragmentFrame.getId(), dateFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        descriptionTextView.setText(description);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.call(DatePickerFrg.getDate());
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        todayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFrg.setDate(new PersianDate().getTime());
            }
        });

        return datePickerLayout;
    }
}
