package Other;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;

import java.util.ArrayList;

import OOP.PopUpWelcome;

public class PopUpWelcomeManager {

    public void showPopUp(int appUse, final Context context) {
        int index = appUse % PopUpWelcome.welcome_mgr_cycle;
        final ArrayList<PopUpWelcome> popUpWelcomes = PopUpWelcome.getPopUpArray(context);
        for (int i = 0; i < popUpWelcomes.size(); i++) {
            for (int j = 0; j < popUpWelcomes.get(i).identifiers.size(); j++) {
                if (popUpWelcomes.get(i).identifiers.get(j) == index) {
                    if (popUpWelcomes.get(i).dialog != null) {
                        popUpWelcomes.get(i).dialog.show();
                    }
                    if (popUpWelcomes.get(i).dialogFragment != null) {
                        final int finalI = i;
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                popUpWelcomes.get(finalI).dialogFragment.show(((Activity) context).getFragmentManager(), "tx_payment");
                            }
                        });
                    }
                    return;
                }
            }
        }
    }
}
