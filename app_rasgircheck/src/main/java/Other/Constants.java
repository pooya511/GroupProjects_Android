package Other;

public class Constants {

    public static final String MY_PREFRENCE = "pref";
    public static String SERVER_URL = "http://api.taxiapps.ir/api/taxiapps";
    public static String GATEWAY_URL = "http://api.taxiapps.ir/BankWithUser";
    public static String APP_ID = "rasgir2";
    public static String APP_VERSION = "";
    public static String APP_SOURCE = "google";
    public static String DEVICE_ID = "";
    public static String GOOGLE_DEVELOPER_ID = "00206505599730386804";
    public static String BAZAAR_DEVELOPER_ID = "taxiapps";
    public static String OS = "android";

    public static String BAZAAR_PAYMENT_BASE64 = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCvirQTwWQkYtNLUruu8bqjchR/xM6eCzzVIhE8BFa8dPBlRok3RZNbbynq8h8gyftgDemSUOi7ZIXpjirXzSVl8phCO4fJLRWB+rdYBkBY6skBHoH/SevdAfPEOB4xnPJ/0zmi5zh/AP5yuRf9Mr8aHlJiTSmONi31UsSYBr2q2h5F49MBF697lz1sWGPlZ3T9sU0kbNt8y0lR9UzrCkfEd5k5IC+eRnQvgo4a2/cCAwEAAQ==";
    public static String MYKET_PAYMENT_BASE64 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCaML+GkyPih0HmHxoq7QQiEl24+YJGi7sJOLVyda3S6tNYHFCeyOp4DBbkJwuwRoUso+udcApry5zB9F+Qks7kHmQkS3ZVN97AoLt0H3MTZzWmbLhPKQqZSPKBXsSWAMg2kYZPSxk7XNKwoHB2s2OaJdQOhMSyLnXzbFn+U2SYywIDAQAB";

    // ----------------------- API Constants

    public static final String SUCCESS = "0";
    public static final String UNKNOWN_ERROR = "-10";
    public static final String PRODUCT_INVALID = "-20";
    public static final String TRANSACTION_INVALID = "-30";
    public static final String COUPON_INVALID = "-40";
    public static final String COUPON_EXPIRED = "-41";
    public static final String COUPON_INVALID_APP = "-42";
    public static final String COUPON_EXPIRE_FAILED = "-48";
    public static final String APP_INVALID = "-60";
    public static final String BACKUP_SIZE_LIMIT = "-70";
    public static final String SMS_NUMBER_INVALID = "-80";
    public static final String SMS_METHOD1_DISABLED = "-81";
    public static final String SMS_UNKNOWN_ERROR = "-82";
    public static final String USER_NOT_EXISTS = "-90";
    public static final String LICENSE_NOT_EXISTS = "-50";
    public static final String USER_LICENSE_NOT_FOUND = "-51";

    // ----------------------- Error API Constants
    public static final String INFO_PAYMENT_VERIFY_ERROR = "پرداخت ناموفق بوده است.در صورت کسر مبلغ وجه شما حداکثر تا ۱ ساعت به کارت بانکی برگشت داده می شود. شما می توانید هم اکنون مجددا تلاش کنید";
    public static final String INFO_PAYMENT_STATUS_CANCELED = "شما فرآیند خرید را لغو کردید. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_ISSUERDOWN = "در حال حاضر بانک صادر کننده پاسخگو نیست. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_UNKNOWN = "خطای نامشخص. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
}
