package Other;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.audiofx.Equalizer;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.text.NumberFormat;
import java.util.Locale;

import air.gamediesel.rasgir.R;
import modules.PublicModules;

public class KeyPadDialog extends Dialog {

    private final TextWatcher textWatcher;
    private final TextWatcher textWatcher2;

    public interface KeyPadCallBack {
        void call(String price);
    }

    public KeyPadDialog(@NonNull Context context, String currPrice, final DialogUtils.keyPadTypes keyPadTypes, final KeyPadCallBack keyPadCallBack) {
        super(context);
        this.setCancelable(true);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.setContentView(R.layout.pop_keypad);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT); //TODO

        Button number0 = this.findViewById(R.id.keypad_btn0);
        Button number1 = this.findViewById(R.id.keypad_btn1);
        Button number2 = this.findViewById(R.id.keypad_btn2);
        Button number3 = this.findViewById(R.id.keypad_btn3);
        Button number4 = this.findViewById(R.id.keypad_btn4);
        Button number5 = this.findViewById(R.id.keypad_btn5);
        Button number6 = this.findViewById(R.id.keypad_btn6);
        Button number7 = this.findViewById(R.id.keypad_btn7);
        Button number8 = this.findViewById(R.id.keypad_btn8);
        Button number9 = this.findViewById(R.id.keypad_btn9);
        Button number000 = this.findViewById(R.id.keypad_btn000);
        TextView accept = this.findViewById(R.id.keypad_btn_ok);
        final TextView cancel = this.findViewById(R.id.keypad_btn_exit);
        Button result = this.findViewById(R.id.keypad_btn_equal);
        Button addition = this.findViewById(R.id.keypad_btn_plus);
        Button subtraction = this.findViewById(R.id.keypad_btn_minus);
        Button multiplication = this.findViewById(R.id.keypad_btn_multiply);
        Button division = this.findViewById(R.id.keypad_btn_devide);
        Button percent = this.findViewById(R.id.keypad_btn_percent);
        Button negate = this.findViewById(R.id.keypad_btn_negate);


        TextView currency = this.findViewById(R.id.pop_keypad_currency);
        currency.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));
        final EditText userNumber = this.findViewById(R.id.pop_keypad_user_number_entered);
        switch (keyPadTypes) {
            case NORMAL_NUMBER:
                currency.setVisibility(View.INVISIBLE);
                break;
            case PRICE:
                currency.setVisibility(View.VISIBLE);
                break;
        }

        if (currPrice.equals("") || currPrice.equals("۰")) {
            userNumber.setText("0");
        } else {
            String myPrice = String.valueOf(currPrice).replaceAll("\\D", "");
            myPrice = PublicModules.toEnglishDigit(myPrice);
            switch (keyPadTypes) {
                case PRICE:
                    myPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(myPrice));
                    break;
            }
            userNumber.setText(myPrice);
        }
        final Button reset = this.findViewById(R.id.keypad_btn_clear);
        Button backSpace = this.findViewById(R.id.keypad_btn_backspace);
        //
        View.OnClickListener clickListener = new View.OnClickListener() {
            private String firstNumber = "";
            int clickCount = 0;
            String operation = "";
            boolean equalClicked;

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.keypad_btn0:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("0");
                        } else {
                            if (userNumber.length() >= 1) {
                                if (equalClicked) {
                                    userNumber.setText("0");
                                    equalClicked = false;
                                } else {
                                    userNumber.append("0");
                                }
                            }
                        }
                        break;
                    case R.id.keypad_btn1:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("1");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("1");
                                equalClicked = false;
                            } else {
                                userNumber.append("1");
                            }
                        }
                        break;
                    case R.id.keypad_btn2:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("2");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("2");
                                equalClicked = false;
                            } else {
                                userNumber.append("2");
                            }
                        }
                        break;
                    case R.id.keypad_btn3:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("3");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("3");
                                equalClicked = false;
                            } else {
                                userNumber.append("3");
                            }
                        }
                        break;
                    case R.id.keypad_btn4:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("4");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("4");
                                equalClicked = false;
                            } else {
                                userNumber.append("4");
                            }
                        }
                        break;
                    case R.id.keypad_btn5:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("5");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("5");
                                equalClicked = false;
                            } else {
                                userNumber.append("5");
                            }
                        }
                        break;
                    case R.id.keypad_btn6:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("6");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("6");
                                equalClicked = false;
                            } else {
                                userNumber.append("6");
                            }
                        }
                        break;
                    case R.id.keypad_btn7:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("7");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("7");
                                equalClicked = false;
                            } else {
                                userNumber.append("7");
                            }
                        }
                        break;
                    case R.id.keypad_btn8:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("8");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("8");
                                equalClicked = false;
                            } else {
                                userNumber.append("8");
                            }
                        }
                        break;
                    case R.id.keypad_btn9:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("9");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("9");
                                equalClicked = false;
                            } else {
                                userNumber.append("9");
                            }
                        }
                        break;
                    case R.id.keypad_btn000:
                        clickCount++;
                        if (clickCount == 1) {
                            userNumber.setText("000");
                        } else {
                            if (equalClicked) {
                                userNumber.setText("000");
                                equalClicked = false;
                            } else {
                                userNumber.append("000");
                            }
                        }
                        break;
                    case R.id.keypad_btn_plus:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "+";
                        break;
                    case R.id.keypad_btn_minus:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "-";
                        break;
                    case R.id.keypad_btn_multiply:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "*";
                        break;
                    case R.id.keypad_btn_devide:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        userNumber.setText("");
                        operation = "/";
                        break;
                    case R.id.keypad_btn_percent:
                        firstNumber = userNumber.getText().toString().replaceAll(",", "");
                        double res = Double.valueOf(firstNumber.replace("/", ".")) / 100;
                        userNumber.setText(String.valueOf(res));
                        firstNumber = "";
                        break;
                    case R.id.keypad_btn_negate:
                        if (keyPadTypes.equals(DialogUtils.keyPadTypes.PRICE)) {
                            String input = userNumber.getText().toString();
                            if (!input.equals("") && !input.equals("0")) {
                                if (input.charAt(0) == '-')
                                    userNumber.setText(input.substring(1, input.length()));
                                else
                                    userNumber.setText("-" + input);
                            }
                        }
                        break;
                    case R.id.keypad_btn_equal:
                        double result;
                        if (!firstNumber.equals("")) {
                            equalClicked = true;
                            switch (operation) {
                                case "+":
                                    result = Double.valueOf(firstNumber.replace("/", ".")) + Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.format("%.0f", result));
                                    firstNumber = "";
                                    break;

                                case "-":
                                    result = Double.valueOf(firstNumber.replace("/", ".")) - Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.format("%.0f", result));
                                    firstNumber = "";
                                    break;

                                case "*":
                                    result = Double.valueOf(firstNumber.replace("/", ".")) * Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.format("%.0f", result));
                                    firstNumber = "";

                                    break;

                                case "/":
                                    double resD = Double.valueOf(firstNumber.replace("/", ".")) / Double.valueOf(userNumber.getText().toString().replace(",", ""));
                                    userNumber.setText(String.format("%.0f", resD));
                                    break;

                            }
                        }
                        break;
                    case R.id.keypad_btn_exit:
                        dismiss();
                        break;
                    case R.id.keypad_btn_ok:
                        if (!userNumber.getText().toString().equals("")) {
                            keyPadCallBack.call(userNumber.getText().toString());
                        } else
                            keyPadCallBack.call("0");
                        dismiss();
                        break;
                    case R.id.keypad_btn_clear:
                        userNumber.setText("0");
                        break;
                    case R.id.keypad_btn_backspace:
                        if (userNumber.length() >= 1) {
                            String str = userNumber.getText().toString();
                            str = str.substring(0, userNumber.length() - 1);
                            userNumber.setText(str);
                        }
                        break;
                }
            }
        };

        number0.setOnClickListener(clickListener);
        number1.setOnClickListener(clickListener);
        number2.setOnClickListener(clickListener);
        number3.setOnClickListener(clickListener);
        number4.setOnClickListener(clickListener);
        number5.setOnClickListener(clickListener);
        number6.setOnClickListener(clickListener);
        number7.setOnClickListener(clickListener);
        number8.setOnClickListener(clickListener);
        number9.setOnClickListener(clickListener);
        number000.setOnClickListener(clickListener);
        accept.setOnClickListener(clickListener);
        cancel.setOnClickListener(clickListener);
        reset.setOnClickListener(clickListener);
        backSpace.setOnClickListener(clickListener);
        result.setOnClickListener(clickListener);
        addition.setOnClickListener(clickListener);
        subtraction.setOnClickListener(clickListener);
        multiplication.setOnClickListener(clickListener);
        division.setOnClickListener(clickListener);
        percent.setOnClickListener(clickListener);
        negate.setOnClickListener(clickListener);


        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String userInput = userNumber.getText().toString();
                userNumber.removeTextChangedListener(textWatcher);
                userInput = userInput.replaceAll("\\D", "");

                if (!userInput.isEmpty()) {
                    userInput = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(userInput.replace("/", "."))).replace(".", "/");
                }
                userNumber.setText(userInput);
                userNumber.addTextChangedListener(textWatcher);
            }
        };

        textWatcher2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                userNumber.removeTextChangedListener(textWatcher2);
                String str = s.toString();
                if (str.length() > 4) {
                    str = str.substring(0, 5);
                }
                userNumber.setText(str);
                userNumber.addTextChangedListener(textWatcher2);
            }
        };
        //
        if (keyPadTypes.equals(DialogUtils.keyPadTypes.PRICE)) {
            userNumber.addTextChangedListener(textWatcher);
        } else {
            userNumber.addTextChangedListener(textWatcher2);
        }

    }
}
