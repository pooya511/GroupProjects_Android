package Other;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

public class C_Setting {

    public static final String ADVANCED_RASGIRI_CHECK = "rasgiriCheck";
    public static final String RASGIRI_FACTOR_BEDUNE_MABLAGH = "rasgiriFactorBeduneMablagh";
    public static final String NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH = "namayeshKhodkarMablaghVaTarikh";
    public static final String JAME_BAHRE_BA_MABLAGH = "jameBahareBaMablagh";
    public static final String MODAT_TASVIEH_PISHFARZ = "modatTasviehPishfarz";
    public static final String TYPE_MODAT_TASVIEH_PISHFARZ = "typeModatTasviehPishfarz";
    public static final String ENTER_WITH_FINGER_PRINT = "enterWithFingerPrint";
    public static final String APP_CURRENCY = "appCurrency";
    public static final String PIN_CODE = "userPin";
    public static final String USER_RATE= "userRate";
    public static final String TX_USER_NUMBER = "userPhoneNumber";
    public static final String IS_PREMIUM = "isPremium";
    public static final String LICENSE_DATA = "licenseData";
    public static final String LICENSE_EXPIRE= "licenseExp";
    public static final String APP_USE = "appUse";

    public static final String GET_CHECK_PDF_COUNTER = "checkPDFCounter";
    public static final String GET_CHECK_PDF_DATE = "checkPDFDate";
    public static final String GET_CHECK_PDF_PATH = "getCheckPdfPath";


    public static final String GET_FACTOR_PDF_COUNTER = "factorPDFCounter";
    public static final String GET_FACTOR_PDF_DATE = "factorPDFDate";
    public static final String GET_FACTOR_PDF_PATH = "getFactorPdfPath";

    public static final String WE_HAVE_WRITE_EXTERNAL_STORAGE_PERMISSION = "weHaveWriteExternalStoragePermission" ;


    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public static void init(Context context) {
        sharedPreferences = context.getSharedPreferences(Constants.MY_PREFRENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (!sharedPreferences.contains(ADVANCED_RASGIRI_CHECK)) {
            editor.putBoolean(ADVANCED_RASGIRI_CHECK, false);
            editor.apply();
        }
        if (!sharedPreferences.contains(RASGIRI_FACTOR_BEDUNE_MABLAGH)) {
            editor.putBoolean(RASGIRI_FACTOR_BEDUNE_MABLAGH, false);
            editor.apply();
        }
        if (!sharedPreferences.contains(NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH)) {
            editor.putBoolean(NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH, false);
            editor.apply();
        }
        if (!sharedPreferences.contains(JAME_BAHRE_BA_MABLAGH)) {
            editor.putBoolean(JAME_BAHRE_BA_MABLAGH, false);
            editor.apply();
        }
        if (!sharedPreferences.contains(MODAT_TASVIEH_PISHFARZ)) {
            editor.putInt(MODAT_TASVIEH_PISHFARZ, 30 );
            editor.apply();
        }
        if (!sharedPreferences.contains(TYPE_MODAT_TASVIEH_PISHFARZ)){
            editor.putString(TYPE_MODAT_TASVIEH_PISHFARZ,"روز");
            editor.apply();
        }
        if (!sharedPreferences.contains(TX_USER_NUMBER)){
            editor.putString(TX_USER_NUMBER, "");
            editor.apply();
        }
        if (!sharedPreferences.contains(IS_PREMIUM)){
            editor.putBoolean(IS_PREMIUM,false);
            editor.apply();
        }
        if (!sharedPreferences.contains(LICENSE_DATA)){
            editor.putString(LICENSE_DATA, null);
            editor.apply();
        }
        if (!sharedPreferences.contains(APP_USE)){
            editor.putInt(APP_USE, 0);
            editor.apply();
        }
        if (!sharedPreferences.contains(USER_RATE)){
            editor.putString(USER_RATE, null);
            editor.apply();
        }
        if (!sharedPreferences.contains(GET_CHECK_PDF_DATE)){
            editor.putString(GET_CHECK_PDF_DATE, "");
            editor.apply();
        }
        if (!sharedPreferences.contains(WE_HAVE_WRITE_EXTERNAL_STORAGE_PERMISSION)){
            editor.putBoolean(WE_HAVE_WRITE_EXTERNAL_STORAGE_PERMISSION, false);
            editor.apply();
        }
        if (!sharedPreferences.contains(GET_FACTOR_PDF_DATE)){
            editor.putString(GET_FACTOR_PDF_DATE, null);
            editor.apply();
        }
        if (!sharedPreferences.contains(GET_CHECK_PDF_COUNTER)){
            editor.putInt(GET_CHECK_PDF_COUNTER, 1);
            editor.apply();
        }
        if (!sharedPreferences.contains(GET_CHECK_PDF_PATH)){
            editor.putString(GET_CHECK_PDF_PATH, null);
            editor.apply();
        }
        if (!sharedPreferences.contains(GET_FACTOR_PDF_PATH)){
            editor.putString(GET_FACTOR_PDF_PATH, null);
            editor.apply();
        }
        if (!sharedPreferences.contains(GET_FACTOR_PDF_COUNTER)){
            editor.putString(GET_FACTOR_PDF_COUNTER, null);
            editor.apply();
        }
        if (!sharedPreferences.contains(LICENSE_EXPIRE)){
            editor.putLong(LICENSE_EXPIRE, 0);
            editor.apply();
        }
        if (!sharedPreferences.contains(ENTER_WITH_FINGER_PRINT)){
            editor.putBoolean(ENTER_WITH_FINGER_PRINT,false);
            editor.apply();
        }
        if (!sharedPreferences.contains(PIN_CODE)){
            editor.putString(PIN_CODE,null);
            editor.apply();
        }
        if (!sharedPreferences.contains(APP_CURRENCY)){
            editor.putString(APP_CURRENCY, "ریال");
            editor.apply();
        }
    }

    public static String getStringValue(String key) {
        return sharedPreferences.getString(key, "");
    }

    public static void setStringValue(String key, String value) {
        editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static boolean getBooleanValue(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public static void setBooleanValue(String key, boolean value) {
        editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void setIntegerValue(String key , int integer){
        editor = sharedPreferences.edit();
        editor.putInt(key,integer);
        editor.apply();
    }

    public static int getIntegerValue(String key){
        return sharedPreferences.getInt(key,0);
    }

    public static void setLongValue(String key , long longValue){
        editor = sharedPreferences.edit();
        editor.putLong(key,longValue);
        editor.apply();
    }

    public static long getLongValue(String key){
        return sharedPreferences.getLong(key,0);
    }

}
