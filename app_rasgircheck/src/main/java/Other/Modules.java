package Other;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.taxiapps.txpayment.dialogFragment.Payment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import OOP.Check;
import OOP.CheckGroup;
import OOP.CheckRasResult;
import OOP.Factor;
import OOP.FactorGroup;
import OOP.FactorRasResult;
import OOP.ProductContent;
import air.gamediesel.rasgir.R;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

import static air.gamediesel.rasgir.SettingActivity.getMacAddress;

public class Modules {

    public interface DateCallBack {
        void call(PersianDate persianCalendar);
    }

    public interface KeyPadCallBack {
        void call(String price);
    }

    public static String reverseString(String str) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(str);

        stringBuilder = stringBuilder.reverse();

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < stringBuilder.length(); i++) {
            result = result.append(stringBuilder.charAt(i));
        }

        return result.toString();
    }

    // --------------------------------------------------------

    public static int screenSize(Context context) {

        int screenSize = context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return 1;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return 2;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return 3;
            default:
                return 4; // alaki
        }
    }

    // --------------------------------------------------------

    public static String toPersianDigit(String persianDigit) {

        return persianDigit.replace("0", "۰").replace("1", "۱").replace("2", "۲").replace("3", "۳").replace("4", "۴").replace("5", "۵")
                .replace("6", "۶").replace("7", "۷").replace("8", "۸").replace("9", "۹");

    }

    // --------------------------------------------------------

    public static int[] getWidthAndHeight(Context context, int widthPercent, int heightPercent) {
        int widthAndHeight[] = new int[2];
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        widthAndHeight[0] = widthPercent * displayMetrics.widthPixels / 100;
        widthAndHeight[1] = heightPercent * displayMetrics.heightPixels / 100;
        return widthAndHeight;
    }

    // --------------------------------------------------------

    public static ArrayList<ProductContent> getProductContent(String str) throws JSONException {
        ArrayList<ProductContent> productContentArray = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(Modules.makeJsonString(str));
        String value = jsonObject.getString("products");
        JSONObject jsonObject1 = new JSONObject(Modules.makeJsonString(value));
        String key1 = "0";
        String value1 = jsonObject1.getString(key1);
        if (key1.equals("0")) {
            ProductContent productContent1 = new ProductContent();
            JSONObject jsonObject2 = new JSONObject(Modules.makeJsonString(value1));
            Iterator iterator2 = jsonObject2.keys();
            while (iterator2.hasNext()) {
                String key2 = String.valueOf(iterator2.next());
                String value2 = jsonObject2.getString(key2);
                switch (key2) {
                    case "product":
                        productContent1.setProduct(value2);
                        Payment.getInstance().SKU_PREMIUM = value2;
                        break;
                    case "text":
                        productContent1.setTitle(value2);
                        break;
                    case "text_color":
                        productContent1.setTitleColor(value2);
                        break;
                    case "price":
                        productContent1.setPrice(value2);
                        break;
                    case "description":
                        productContent1.setDescription(value2);
                        break;
                    case "hasCoupon":
                        if (value2.equals("true")) {
                            productContent1.setHasCoupon(true);
                        } else {
                            productContent1.setHasCoupon(false);
                        }
                        break;
                    case "description_color":
                        productContent1.setDescriptionColor(value2);
                        break;
                }
            }
            productContentArray.add(productContent1);
        }
        if (jsonObject1.has("1")) {
            key1 = "1";
            value1 = jsonObject1.getString(key1);
            if (key1.equals("1")) {
                ProductContent productContent2 = new ProductContent();
                JSONObject jsonObject2 = new JSONObject(Modules.makeJsonString(value1));
                Iterator iterator2 = jsonObject2.keys();
                while (iterator2.hasNext()) {
                    String key2 = String.valueOf(iterator2.next());
                    String value2 = jsonObject2.getString(key2);
                    switch (key2) {
                        case "product":
                            productContent2.setProduct(value2);
                            break;
                        case "text":
                            productContent2.setTitle(value2);
                            break;
                        case "text_color":
                            productContent2.setTitleColor(value2);
                            break;
                        case "price":
                            productContent2.setPrice(value2);
                            break;
                        case "description":
                            productContent2.setDescription(value2);
                            break;
                        case "hasCoupon":
                            if (value2.equals("true")) {
                                productContent2.setHasCoupon(true);
                            } else {
                                productContent2.setHasCoupon(false);
                            }
                            break;
                        case "description_color":
                            productContent2.setDescriptionColor(value2);
                            break;
                    }
                }
                productContentArray.add(productContent2);
            }
        }
        return productContentArray;
    }

    // --------------------------------------------------------

    public static long getMinimum(ArrayList<Long> dates) {
        long min = 0;
        if (dates.size() != 0) {
            min = dates.get(0);
            for (int j = 0; j < dates.size(); j++) {
                if (dates.get(j) < min) {
                    min = dates.get(j);
                }
            }
        }
        return min;
    }

    // --------------------------------------------------------

    public static long startOfDay(long date) {
        PersianDate pc = new PersianDate(date);
        pc.setHour(0);
        pc.setMinute(0);
        pc.setSecond(0);
        return pc.getTime();
    }

    // --------------------------------------------------------

    public static JSONObject getLicenseContent(String responseStr) {
        JSONObject jsonObject;
        JSONObject jsonResult = new JSONObject();
        Log.i("LicenseContent response", responseStr);
        try {
            jsonObject = new JSONObject(responseStr);
            String licenses = jsonObject.getString("licenses");
            jsonObject = new JSONObject(licenses);
            Iterator result = jsonObject.keys();
            while (result.hasNext()) {
                Object key = result.next();
                if (jsonObject.getJSONObject(key.toString()).getString("appid").equals(Constants.APP_ID)) {
                    jsonResult = jsonObject.getJSONObject(key.toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    // --------------------------------------------------------

    public static String makeJsonString(String responseStr) {
        if (responseStr.charAt(0) == '\"' && responseStr.charAt(responseStr.length() - 1) == '\"') {
            responseStr = responseStr.substring(1, responseStr.length() - 1);// از بین بردن " های اول و آخر
        }
        responseStr = responseStr.replaceAll("\\\\", "");
        return responseStr;
    }

    // --------------------------------------------------------

    public static boolean inputIsValid(String str, int targetLength, boolean isPinCode) {
        if (isPinCode) {
            if (!str.equals("") && str.length() > 11 && str.length() < 16) {
                return true;
            } else {
                return false;
            }
        } else {
            if (str.equals("") || str.length() != targetLength &&
                    str.charAt(0) != 0 && str.charAt(1) != 9
                    ) {
                return false;
            } else {
                return true;
            }
        }

    }

    // --------------------------------------------------------

    public static void hideSoftKeyboard(Context context) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

    }


    public static void showSoftKeyboard(Context context) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    // --------------------------------------------------------

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // ---------------------------------------------------------

    public static File createChequePdf(Context context, CheckRasResult checkRas, CheckGroup checkGroup, ArrayList<Check> checks, double percent, long bahrePrice) throws DocumentException, IOException {
        PersianDate persianCalendar = new PersianDate(checkGroup.getCheckExportDate());

        File myFile = null;
        File ourPdfOutput = new File(Environment.getExternalStorageDirectory(), "Document/Rasgir_Reports/Cheques");

        if (!ourPdfOutput.exists()) {
            ourPdfOutput.mkdirs();
            //     Toast.makeText(context, ourPdfOutput.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        }

        if (C_Setting.getStringValue(C_Setting.GET_CHECK_PDF_DATE).equals("")) {
            myFile = new File(ourPdfOutput.getAbsolutePath(), "check_export_".concat(Modules.getPersianShortDate(persianCalendar)).replaceAll("/", "") + (".pdf"));
            C_Setting.setStringValue(C_Setting.GET_CHECK_PDF_DATE, Modules.getPersianShortDate(persianCalendar));
            C_Setting.setIntegerValue(C_Setting.GET_CHECK_PDF_COUNTER, C_Setting.getIntegerValue(C_Setting.GET_CHECK_PDF_COUNTER) + 1);
            C_Setting.setStringValue(C_Setting.GET_CHECK_PDF_PATH, myFile.getAbsolutePath());
        } else if (C_Setting.getStringValue(C_Setting.GET_CHECK_PDF_DATE).equals(Modules.getPersianShortDate(persianCalendar))) {
            myFile = new File(ourPdfOutput.getAbsolutePath(), "check_export_".concat(Modules.getPersianShortDate(persianCalendar)).replaceAll("/", "") + "(" + String.valueOf(C_Setting.getIntegerValue(C_Setting.GET_CHECK_PDF_COUNTER)) + ")" + (".pdf"));
            C_Setting.setStringValue(C_Setting.GET_CHECK_PDF_PATH, myFile.getAbsolutePath());
            C_Setting.setIntegerValue(C_Setting.GET_CHECK_PDF_COUNTER, C_Setting.getIntegerValue(C_Setting.GET_CHECK_PDF_COUNTER) + 1);
        } else {
            C_Setting.setStringValue(C_Setting.GET_CHECK_PDF_DATE, "");
            C_Setting.setIntegerValue(C_Setting.GET_CHECK_PDF_COUNTER, 1);
        }

        try {
            BaseFont urName = BaseFont.createFont("assets/fonts/yekan.ttf",
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            BaseFont roboto = BaseFont.createFont("assets/fonts/Roboto-Medium.ttf",
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            BaseColor baseColor = BaseColor.GRAY;

            Font yekan = new Font(urName, 12);
            Font robotoFont = new Font(roboto, 14);

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(myFile));
            document.open();

//-----------------------------------------------------------------------------------------------------

            PdfPTable tableTitle = new PdfPTable(2);
            tableTitle.setWidthPercentage(100);
            tableTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableTitle.setExtendLastRow(false);

            Font titleFont = new Font(urName, 16);

            PdfPCell titleCell = new PdfPCell(new Phrase("خروجی لیست چک ها", titleFont));
            titleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            titleCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            titleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            titleCell.setPaddingBottom(7f);
            titleCell.setFixedHeight(25f);
            titleCell.setBorder(0);

            InputStream ims = context.getAssets().open("rasgir_logo_pdf.png");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());

            PdfPCell ImageCell = new PdfPCell(image);
            ImageCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            ImageCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            ImageCell.setFixedHeight(60f);
            ImageCell.setBorder(0);

            PdfPTable tableDateTitle = new PdfPTable(2);
            tableDateTitle.setWidthPercentage(100);
            tableDateTitle.setWidths(new float[]{15.5f, 2.5f});
            tableDateTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableDateTitle.setExtendLastRow(false);

            PdfPCell titleDateCell = new PdfPCell(new Phrase("تاریخ گزارش : ", yekan));
            titleDateCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            titleDateCell.setVerticalAlignment(Element.ALIGN_LEFT);
            titleDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            titleDateCell.setFixedHeight(25f);
            titleDateCell.setBorder(0);

            PdfPCell cell = new PdfPCell(new Phrase(Modules.getPersianShortDate(persianCalendar), yekan));
            cell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            cell.setVerticalAlignment(Element.ALIGN_LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setFixedHeight(25f);
            cell.setBorder(0);

//-----------------------------------------------------------------------------------------------------

            PdfPTable table1 = new PdfPTable(4);
            table1.setWidthPercentage(100);
            table1.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            table1.setExtendLastRow(false);

            Font tableTitlesFont = new Font(urName, 13);

            PdfPCell chequesNumberTitleCell = new PdfPCell(new Phrase("تعداد چک ها", tableTitlesFont));
            chequesNumberTitleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequesNumberTitleCell.setVerticalAlignment(Element.ALIGN_CENTER);
            chequesNumberTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequesNumberTitleCell.setFixedHeight(25f);

            PdfPCell chequesSumTitleCell = new PdfPCell(new Phrase("جمع مبلغ چک ها", tableTitlesFont));
            chequesSumTitleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequesSumTitleCell.setVerticalAlignment(Element.ALIGN_CENTER);
            chequesSumTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequesSumTitleCell.setFixedHeight(25f);

            PdfPCell rasDateTitleCell = new PdfPCell(new Phrase("رأس به روز", tableTitlesFont));
            rasDateTitleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            rasDateTitleCell.setVerticalAlignment(Element.ALIGN_CENTER);
            rasDateTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            rasDateTitleCell.setFixedHeight(25f);

            PdfPCell mabdaRasgiriTitleCell = new PdfPCell(new Phrase("مبدأ رأس گیری", tableTitlesFont));
            mabdaRasgiriTitleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            mabdaRasgiriTitleCell.setVerticalAlignment(Element.ALIGN_CENTER);
            mabdaRasgiriTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            mabdaRasgiriTitleCell.setFixedHeight(25f);

            PdfPCell chequesNumberCell = new PdfPCell(new Phrase(String.valueOf(checkRas.checkCount), yekan));
            chequesNumberCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequesNumberCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            chequesNumberCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequesNumberCell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
            chequesNumberCell.setFixedHeight(20f);

            String chequesSum = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(checkRas.priceSum)));

            PdfPCell chequesSumCell = new PdfPCell(new Phrase(chequesSum + " ريال", yekan));
            chequesSumCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequesSumCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            chequesSumCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequesSumCell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
            chequesSumCell.setFixedHeight(20f);


            String rasdateCellString = Math.abs(checkRas.dayCount) + " روز" + (checkRas.dayCount < 0 ? " قبل،" : " بعد،") + " معادل ";

            PdfPCell rasDateCell = new PdfPCell(new Phrase(rasdateCellString, yekan));
            rasDateCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            rasDateCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            rasDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            rasDateCell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
            rasDateCell.setFixedHeight(20f);

            persianCalendar = new PersianDate(checkRas.startDate);
            PdfPCell mabdaRasgiriCell = new PdfPCell(new Phrase(Modules.getPersianShortDate(persianCalendar), yekan));
            mabdaRasgiriCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            mabdaRasgiriCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            mabdaRasgiriCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            mabdaRasgiriCell.setBorder(Rectangle.TOP | Rectangle.RIGHT | Rectangle.LEFT);
            mabdaRasgiriCell.setFixedHeight(20f);

            PdfPCell cell1 = new PdfPCell(new Phrase("     ", yekan));
            cell1.setVerticalAlignment(Element.ALIGN_TOP);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
            cell1.setFixedHeight(22f);

            PdfPCell cell2 = new PdfPCell(new Phrase("     ", yekan));
            cell2.setVerticalAlignment(Element.ALIGN_TOP);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setFixedHeight(22f);
            cell2.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);

            persianCalendar = new PersianDate(checkRas.headDate);
            PdfPCell cell3 = new PdfPCell(new Phrase(Modules.getPersianShortDate(persianCalendar), yekan));
            cell3.setVerticalAlignment(Element.ALIGN_TOP);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setFixedHeight(22f);
            cell3.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);

            PdfPCell cell4 = new PdfPCell(new Phrase("     ", yekan));
            cell4.setVerticalAlignment(Element.ALIGN_TOP);
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell4.setFixedHeight(22f);
            cell4.setBorder(Rectangle.BOTTOM | Rectangle.LEFT | Rectangle.RIGHT);
//------------------------------------------------------------------------------------------------------------

            PdfPTable table2 = new PdfPTable(4);
            table2.setWidthPercentage(100);
            table2.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            table2.setWidths(new int[]{11, 6, 4, 2});
            table2.setExtendLastRow(false);

            PdfPCell numberTitle = new PdfPCell(new Phrase("شماره", tableTitlesFont));
            numberTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            numberTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            numberTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            numberTitle.setBorder(Rectangle.BOTTOM);
            numberTitle.setBorderWidth(1.5f);
            numberTitle.setPaddingBottom(0f);
            numberTitle.setFixedHeight(25f);

            PdfPCell chequeDateTitle = new PdfPCell(new Phrase("تاریخ چک", tableTitlesFont));
            chequeDateTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequeDateTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            chequeDateTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequeDateTitle.setBorder(Rectangle.BOTTOM);
            chequeDateTitle.setBorderWidth(1.5f);
            chequeDateTitle.setPaddingBottom(0f);
            chequeDateTitle.setFixedHeight(25f);

            PdfPCell chequePriceTitle = new PdfPCell(new Phrase("مبلغ چک", tableTitlesFont));
            chequePriceTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequePriceTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            chequePriceTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequePriceTitle.setBorder(Rectangle.BOTTOM);
            chequePriceTitle.setBorderWidth(1.5f);
            chequePriceTitle.setPaddingBottom(0f);
            chequePriceTitle.setFixedHeight(25f);

            PdfPCell descriptionTitle = new PdfPCell(new Phrase("توضیحات", tableTitlesFont));
            descriptionTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            descriptionTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            descriptionTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            descriptionTitle.setBorder(Rectangle.BOTTOM);
            descriptionTitle.setBorderWidth(1.5f);
            descriptionTitle.setPaddingBottom(0f);
            descriptionTitle.setFixedHeight(25f);

//------------------------------------------------------------------------------------------------------------

            PdfPTable table3 = new PdfPTable(4);
            table3.setWidthPercentage(100);
            table3.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            table3.setExtendLastRow(false);

            PdfPCell chequesPriceSumTitle = new PdfPCell(new Phrase("جمع مبلغ چک ها", tableTitlesFont));
            chequesPriceSumTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequesPriceSumTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            chequesPriceSumTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequesPriceSumTitle.setFixedHeight(25f);

            PdfPCell profitPercentTitle = new PdfPCell(new Phrase("درصد بهره", tableTitlesFont));
            profitPercentTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            profitPercentTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            profitPercentTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            profitPercentTitle.setFixedHeight(25f);

            PdfPCell profitPriceTitle = new PdfPCell(new Phrase("مبلغ بهره", tableTitlesFont));
            profitPriceTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            profitPriceTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            profitPriceTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            profitPriceTitle.setFixedHeight(25f);

            PdfPCell totalSumTitle ;

            if (C_Setting.getBooleanValue(C_Setting.JAME_BAHRE_BA_MABLAGH))
                totalSumTitle = new PdfPCell(new Phrase("جمع بهره با مبلغ", tableTitlesFont));
            else
                totalSumTitle = new PdfPCell(new Phrase("کسر بهره از مبلغ", tableTitlesFont));

            totalSumTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            totalSumTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            totalSumTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            totalSumTitle.setFixedHeight(25f);

            PdfPCell chequesPriceSum = new PdfPCell(new Phrase(chequesSum + " ريال", yekan));
            chequesPriceSum.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequesPriceSum.setVerticalAlignment(Element.ALIGN_CENTER);
            chequesPriceSum.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequesPriceSum.setFixedHeight(25f);

            DecimalFormat doubleFormat = new DecimalFormat(".##");

            PdfPCell profitPercent = new PdfPCell(new Phrase("% " + String.valueOf(doubleFormat.format(percent)), yekan));
            profitPercent.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            profitPercent.setVerticalAlignment(Element.ALIGN_CENTER);
            profitPercent.setHorizontalAlignment(Element.ALIGN_CENTER);
            profitPercent.setFixedHeight(25f);

            String profitPriceValue = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(bahrePrice)));

            PdfPCell profitPrice = new PdfPCell(new Phrase(profitPriceValue + " ریال", yekan));
            profitPrice.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            profitPrice.setVerticalAlignment(Element.ALIGN_CENTER);
            profitPrice.setHorizontalAlignment(Element.ALIGN_CENTER);
            profitPrice.setFixedHeight(25f);

            String totalSumValue;

            if (C_Setting.getBooleanValue(C_Setting.JAME_BAHRE_BA_MABLAGH))
                totalSumValue = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(checkRas.priceSum + bahrePrice).replaceAll(",", "")));
            else
                totalSumValue = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(String.valueOf(checkRas.priceSum - bahrePrice).replaceAll(",", "")));

            PdfPCell totalSum = new PdfPCell(new Phrase(totalSumValue + " ریال", yekan));
            totalSum.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            totalSum.setVerticalAlignment(Element.ALIGN_CENTER);
            totalSum.setHorizontalAlignment(Element.ALIGN_CENTER);
            totalSum.setFixedHeight(25f);

//-----------------------------------------------------------------------------------------------------------

            PdfPTable tableSignature = new PdfPTable(2);
            tableSignature.setWidthPercentage(100);
            tableSignature.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableSignature.setExtendLastRow(false);

            PdfPCell signature1 = new PdfPCell(new Phrase("محل امضای دریافت کننده", yekan));
            signature1.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signature1.setVerticalAlignment(Element.ALIGN_CENTER);
            signature1.setHorizontalAlignment(Element.ALIGN_CENTER);
            signature1.setFixedHeight(25f);
            signature1.setBorder(0);

            PdfPCell signature2 = new PdfPCell(new Phrase("محل امضای پرداخت کننده", yekan));
            signature2.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signature2.setVerticalAlignment(Element.ALIGN_CENTER);
            signature2.setHorizontalAlignment(Element.ALIGN_CENTER);
            signature2.setFixedHeight(25f);
            signature2.setBorder(0);

            PdfPCell signaturePlace1 = new PdfPCell(new Phrase("   ", yekan));
            signaturePlace1.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signaturePlace1.setVerticalAlignment(Element.ALIGN_CENTER);
            signaturePlace1.setHorizontalAlignment(Element.ALIGN_CENTER);
            signaturePlace1.setBorder(0);
            signaturePlace1.setFixedHeight(45f);

            PdfPCell signaturePlace2 = new PdfPCell(new Phrase("   ", yekan));
            signaturePlace2.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signaturePlace2.setVerticalAlignment(Element.ALIGN_CENTER);
            signaturePlace2.setHorizontalAlignment(Element.ALIGN_CENTER);
            signaturePlace2.setBorder(0);
            signaturePlace2.setFixedHeight(45f);

//------------------------------------------------------------------------------------------------------------

            PdfPTable finishDescriptions = new PdfPTable(1);
            tableSignature.setWidthPercentage(100);
            tableSignature.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableSignature.setExtendLastRow(false);

            Font descriptionFont = new Font(urName, 12);
            descriptionFont.setColor(BaseColor.GRAY);

            PdfPCell finishDescription1 = new PdfPCell(new Phrase("برای دانلود نرم افزار موبایلی و تحت وب رأس گیر چک سایه ما را در ", descriptionFont));
            finishDescription1.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            finishDescription1.setVerticalAlignment(Element.ALIGN_CENTER);
            finishDescription1.setHorizontalAlignment(Element.ALIGN_CENTER);
            finishDescription1.setBorder(0);
//            finishDescription1.setFixedHeight(45f);

            PdfPCell finishDescription2 = new PdfPCell(new Phrase("کافه بازار اندروید و یا اپ استور آیفون جستجو نمایید.", descriptionFont));
            finishDescription2.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            finishDescription2.setVerticalAlignment(Element.ALIGN_CENTER);
            finishDescription2.setHorizontalAlignment(Element.ALIGN_CENTER);
            finishDescription2.setPaddingTop(10f);
            finishDescription2.setBorder(0);
            finishDescription2.setFixedHeight(45f);

            PdfPCell siteAddress = new PdfPCell(new Phrase("www.TaxiApps.org", robotoFont));
            siteAddress.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            siteAddress.setVerticalAlignment(Element.ALIGN_CENTER);
            siteAddress.setHorizontalAlignment(Element.ALIGN_CENTER);
            siteAddress.setBorder(0);
            siteAddress.setFixedHeight(45f);


//------------------------------------------------------------------------------------------------------------

            table1.addCell(chequesNumberTitleCell);
            table1.addCell(chequesSumTitleCell);
            table1.addCell(rasDateTitleCell);
            table1.addCell(mabdaRasgiriTitleCell);
            table1.addCell(chequesNumberCell);
            table1.addCell(chequesSumCell);
            table1.addCell(rasDateCell);
            table1.addCell(mabdaRasgiriCell);
            table1.addCell(cell1);
            table1.addCell(cell2);
            table1.addCell(cell3);
            table1.addCell(cell4);


            tableTitle.addCell(titleCell);
            tableTitle.addCell(ImageCell);
            tableDateTitle.addCell(titleDateCell);
            tableDateTitle.addCell(cell);

            table2.addCell(numberTitle);
            table2.addCell(chequeDateTitle);
            table2.addCell(chequePriceTitle);
            table2.addCell(descriptionTitle);
            table2.setHeaderRows(1);

            for (int i = 0; i < checks.size(); i++) {

                PdfPCell number = new PdfPCell(new Phrase(String.valueOf(i + 1), yekan));

                persianCalendar = new PersianDate(checks.get(i).getDate());
                PdfPCell chequeDate = new PdfPCell(new Phrase(Modules.getPersianShortDate(persianCalendar), yekan));

                String checkPrice = String.valueOf(checks.get(i).getPrice());
                checkPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(checkPrice));

                PdfPCell chequePrice = new PdfPCell(new Phrase(checkPrice + " ریال", yekan));
                PdfPCell description = new PdfPCell(new Phrase("    ", yekan));

                if (i != checks.size() - 1) {
                    number.setBorder(Rectangle.BOTTOM);
                    number.setBorderWidth(1f);
                    number.setBorderColor(baseColor);
                    chequeDate.setBorder(Rectangle.BOTTOM);
                    chequeDate.setBorderWidth(1f);
                    chequeDate.setBorderColor(baseColor);
                    chequePrice.setBorder(Rectangle.BOTTOM);
                    chequePrice.setBorderWidth(1f);
                    chequePrice.setBorderColor(baseColor);
                    description.setBorder(Rectangle.BOTTOM);
                    description.setBorderWidth(1f);
                    description.setBorderColor(baseColor);
                } else {
                    number.setBorder(0);
                    chequeDate.setBorder(0);
                    chequePrice.setBorder(0);
                    description.setBorder(0);
                }

                number.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                number.setVerticalAlignment(Element.ALIGN_CENTER);
                number.setHorizontalAlignment(Element.ALIGN_CENTER);
                number.setPaddingBottom(0f);
                number.setFixedHeight(25f);
                table2.addCell(number);


                chequeDate.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                chequeDate.setVerticalAlignment(Element.ALIGN_CENTER);
                chequeDate.setHorizontalAlignment(Element.ALIGN_CENTER);
                chequeDate.setPaddingBottom(0f);
                chequeDate.setFixedHeight(25f);
                table2.addCell(chequeDate);


                chequePrice.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                chequePrice.setVerticalAlignment(Element.ALIGN_CENTER);
                chequePrice.setHorizontalAlignment(Element.ALIGN_CENTER);
                chequePrice.setPaddingBottom(0f);
                chequePrice.setFixedHeight(25f);
                table2.addCell(chequePrice);

                description.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                description.setVerticalAlignment(Element.ALIGN_CENTER);
                description.setHorizontalAlignment(Element.ALIGN_CENTER);
                description.setPaddingBottom(0f);
                description.setFixedHeight(25f);
                table2.addCell(description);

            }   // برای جدول چک ها

            table3.addCell(chequesPriceSumTitle);
            table3.addCell(profitPercentTitle);
            table3.addCell(profitPriceTitle);
            table3.addCell(totalSumTitle);
            table3.addCell(chequesPriceSum);
            table3.addCell(profitPercent);
            table3.addCell(profitPrice);
            table3.addCell(totalSum);

            tableSignature.addCell(signature1);
            tableSignature.addCell(signature2);
            tableSignature.addCell(signaturePlace1);
            tableSignature.addCell(signaturePlace2);

            finishDescriptions.addCell(finishDescription1);
            finishDescriptions.addCell(finishDescription2);
            finishDescriptions.addCell(siteAddress);

            tableTitle.setHorizontalAlignment(Element.ALIGN_LEFT);

            document.add(tableTitle);
            document.add(tableDateTitle);
            document.add(new Paragraph("\n"));
            document.add(table1);
            document.add(new Paragraph("\n\n"));
            document.add(table2);
            document.add(new Paragraph("\n\n"));
            document.add(table3);
            document.add(new Paragraph("\n\n"));
            document.add(tableSignature);
            document.add(new Paragraph("\n\n"));
            document.add(finishDescriptions);


            //---------------
            document.close();

            return myFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // ----------------------------------------------------------

    public static File createFactorPdf(Context context, FactorGroup factorGroup, ArrayList<Factor> factors, FactorRasResult factorRasResult) throws DocumentException, IOException {
        PersianDate persianCalendar = new PersianDate(factorGroup.getExportDate());

        File myFile = null;
        File ourPdfOutput = new File(Environment.getExternalStorageDirectory(), "Document/Rasgir_Reports/Factors");

        if (!ourPdfOutput.exists()) {
            ourPdfOutput.mkdirs();
         //   Toast.makeText(context, ourPdfOutput.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        }

        if (C_Setting.getStringValue(C_Setting.GET_FACTOR_PDF_DATE) == null || C_Setting.getStringValue(C_Setting.GET_FACTOR_PDF_DATE).equals("")) {
            myFile = new File(ourPdfOutput.getAbsolutePath(), "factor_export_".concat(Modules.getPersianShortDate(persianCalendar)).replaceAll("/", "") + (".pdf"));
            C_Setting.setStringValue(C_Setting.GET_FACTOR_PDF_DATE, Modules.getPersianShortDate(persianCalendar));
            C_Setting.setStringValue(C_Setting.GET_FACTOR_PDF_PATH, myFile.getAbsolutePath());
            C_Setting.setIntegerValue(C_Setting.GET_FACTOR_PDF_COUNTER, C_Setting.getIntegerValue(C_Setting.GET_FACTOR_PDF_COUNTER) + 1);
        } else if (C_Setting.getStringValue(C_Setting.GET_FACTOR_PDF_DATE).equals(Modules.getPersianShortDate(persianCalendar))) {
            myFile = new File(ourPdfOutput.getAbsolutePath(), "factor_export_".concat(Modules.getPersianShortDate(persianCalendar)).replaceAll("/", "") + "(" + String.valueOf(C_Setting.getIntegerValue(C_Setting.GET_FACTOR_PDF_COUNTER)) + ")" + (".pdf"));
            C_Setting.setStringValue(C_Setting.GET_FACTOR_PDF_PATH, myFile.getAbsolutePath());
            C_Setting.setIntegerValue(C_Setting.GET_FACTOR_PDF_COUNTER, C_Setting.getIntegerValue(C_Setting.GET_FACTOR_PDF_COUNTER) + 1);
        } else {
            C_Setting.setStringValue(C_Setting.GET_FACTOR_PDF_DATE, null);
            C_Setting.setIntegerValue(C_Setting.GET_FACTOR_PDF_COUNTER, 1);
        }


        try {
            BaseFont urName = BaseFont.createFont("assets/fonts/yekan.ttf",
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            BaseFont roboto = BaseFont.createFont("assets/fonts/Roboto-Medium.ttf",
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            BaseColor baseColor = BaseColor.GRAY;

            Font yekan = new Font(urName, 12);
            Font robotoFont = new Font(roboto, 14);

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(myFile));
            document.open();

//-----------------------------------------------------------------------------------------------------

            PdfPTable tableTitle = new PdfPTable(2);
            tableTitle.setWidthPercentage(100);
            tableTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableTitle.setExtendLastRow(false);

            Font titleFont = new Font(urName, 16);

            PdfPCell titleCell = new PdfPCell(new Phrase("خروجی لیست فاکتور ها", titleFont));
            titleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            titleCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            titleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            titleCell.setPaddingBottom(7f);
            titleCell.setFixedHeight(25f);
            titleCell.setBorder(0);

            InputStream ims = context.getAssets().open("rasgir_logo_pdf.png");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());

            PdfPCell ImageCell = new PdfPCell(image);
            ImageCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            ImageCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            ImageCell.setFixedHeight(60f);
            ImageCell.setBorder(0);

            PdfPTable tableDateTitle = new PdfPTable(2);
            tableDateTitle.setWidthPercentage(100);
            tableDateTitle.setWidths(new float[]{15.5f, 2.5f});
            tableDateTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableDateTitle.setExtendLastRow(false);

            PdfPCell titleDateCell = new PdfPCell(new Phrase("تاریخ گزارش : ", yekan));
            titleDateCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            titleDateCell.setVerticalAlignment(Element.ALIGN_LEFT);
            titleDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            titleDateCell.setFixedHeight(25f);
            titleDateCell.setBorder(0);

            PdfPCell cell = new PdfPCell(new Phrase(Modules.getPersianShortDate(persianCalendar), yekan));
            cell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            cell.setVerticalAlignment(Element.ALIGN_LEFT);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setFixedHeight(25f);
            cell.setBorder(0);

//-----------------------------------------------------------------------------------------------------
            Font tableTitlesFont = new Font(urName, 13);

            PdfPTable factorsTable = new PdfPTable(5);
            factorsTable.setWidthPercentage(100);
            factorsTable.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            factorsTable.setWidths(new int[]{11, 7, 7, 7, 3});
            factorsTable.setExtendLastRow(false);

            PdfPCell numberTitle = new PdfPCell(new Phrase("شماره", tableTitlesFont));
            numberTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            numberTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            numberTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            numberTitle.setBorder(Rectangle.BOTTOM);
            numberTitle.setBorderWidth(1.5f);
            numberTitle.setPaddingBottom(0f);
            numberTitle.setFixedHeight(25f);

            PdfPCell factorDateTitle = new PdfPCell(new Phrase("تاریخ فاکتور", tableTitlesFont));
            factorDateTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            factorDateTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            factorDateTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            factorDateTitle.setBorder(Rectangle.BOTTOM);
            factorDateTitle.setBorderWidth(1.5f);
            factorDateTitle.setPaddingBottom(0f);
            factorDateTitle.setFixedHeight(25f);

            PdfPCell factorPriceTitle = new PdfPCell(new Phrase("مبلغ فاکتور", tableTitlesFont));
            factorPriceTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            factorPriceTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            factorPriceTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            factorPriceTitle.setBorder(Rectangle.BOTTOM);
            factorPriceTitle.setBorderWidth(1.5f);
            factorPriceTitle.setPaddingBottom(0f);
            factorPriceTitle.setFixedHeight(25f);

            PdfPCell settlementDateTitle = new PdfPCell(new Phrase("مدت تسویه", tableTitlesFont));
            settlementDateTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            settlementDateTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            settlementDateTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            settlementDateTitle.setBorder(Rectangle.BOTTOM);
            settlementDateTitle.setBorderWidth(1.5f);
            settlementDateTitle.setPaddingBottom(0f);
            settlementDateTitle.setFixedHeight(25f);

            PdfPCell descriptionTitle = new PdfPCell(new Phrase("توضیحات", tableTitlesFont));
            descriptionTitle.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            descriptionTitle.setVerticalAlignment(Element.ALIGN_CENTER);
            descriptionTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            descriptionTitle.setBorder(Rectangle.BOTTOM);
            descriptionTitle.setBorderWidth(1.5f);
            descriptionTitle.setPaddingBottom(0f);
            descriptionTitle.setFixedHeight(25f);

//-----------------------------------------------------------------------------------------------------------

            PdfPTable summaryTable = new PdfPTable(3);
            summaryTable.setWidthPercentage(100);
            summaryTable.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            summaryTable.setExtendLastRow(false);

            PdfPCell factorNumberTitleCell = new PdfPCell(new Phrase("تعداد فاکتور ها", tableTitlesFont));
            factorNumberTitleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            factorNumberTitleCell.setVerticalAlignment(Element.ALIGN_CENTER);
            factorNumberTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            factorNumberTitleCell.setFixedHeight(25f);

            PdfPCell factorSumTitleCell = new PdfPCell(new Phrase("جمع مبلغ فاکتور ها", tableTitlesFont));
            factorSumTitleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            factorSumTitleCell.setVerticalAlignment(Element.ALIGN_CENTER);
            factorSumTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            factorSumTitleCell.setFixedHeight(25f);

            PdfPCell chequeDateTitleCell = new PdfPCell(new Phrase("تاریخ چک معادل", tableTitlesFont));
            chequeDateTitleCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequeDateTitleCell.setVerticalAlignment(Element.ALIGN_CENTER);
            chequeDateTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequeDateTitleCell.setFixedHeight(25f);

            PdfPCell factorNumberCell = new PdfPCell(new Phrase(String.valueOf(factorRasResult.factorCount), yekan));
            factorNumberCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            factorNumberCell.setVerticalAlignment(Element.ALIGN_CENTER);
            factorNumberCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            factorNumberCell.setFixedHeight(30f);

            String factorSumVlue = String.valueOf(factorRasResult.priceSum);
            factorSumVlue = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(factorSumVlue));

            PdfPCell factorSumCell = new PdfPCell(new Phrase(factorSumVlue + " ریال", yekan));
            factorSumCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            factorSumCell.setVerticalAlignment(Element.ALIGN_CENTER);
            factorSumCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            factorSumCell.setFixedHeight(30f);

            persianCalendar = new PersianDate(factorRasResult.limitDate);

            PdfPCell chequeDateCell = new PdfPCell(new Phrase(Modules.getPersianShortDate(persianCalendar), yekan));
            chequeDateCell.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            chequeDateCell.setVerticalAlignment(Element.ALIGN_CENTER);
            chequeDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            chequeDateCell.setFixedHeight(30f);

//-----------------------------------------------------------------------------------------------------------

            PdfPTable tableSignature = new PdfPTable(2);
            tableSignature.setWidthPercentage(100);
            tableSignature.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableSignature.setExtendLastRow(false);

            PdfPCell signature1 = new PdfPCell(new Phrase("محل امضای دریافت کننده", yekan));
            signature1.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signature1.setVerticalAlignment(Element.ALIGN_CENTER);
            signature1.setHorizontalAlignment(Element.ALIGN_CENTER);
            signature1.setFixedHeight(25f);
            signature1.setBorder(0);

            PdfPCell signature2 = new PdfPCell(new Phrase("محل امضای پرداخت کننده", yekan));
            signature2.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signature2.setVerticalAlignment(Element.ALIGN_CENTER);
            signature2.setHorizontalAlignment(Element.ALIGN_CENTER);
            signature2.setFixedHeight(25f);
            signature2.setBorder(0);

            PdfPCell signaturePlace1 = new PdfPCell(new Phrase("   ", yekan));
            signaturePlace1.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signaturePlace1.setVerticalAlignment(Element.ALIGN_CENTER);
            signaturePlace1.setHorizontalAlignment(Element.ALIGN_CENTER);
            signaturePlace1.setBorder(0);
            signaturePlace1.setFixedHeight(45f);

            PdfPCell signaturePlace2 = new PdfPCell(new Phrase("   ", yekan));
            signaturePlace2.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            signaturePlace2.setVerticalAlignment(Element.ALIGN_CENTER);
            signaturePlace2.setHorizontalAlignment(Element.ALIGN_CENTER);
            signaturePlace2.setBorder(0);
            signaturePlace2.setFixedHeight(45f);

//------------------------------------------------------------------------------------------------------------

            PdfPTable finishDescriptions = new PdfPTable(1);
            tableSignature.setWidthPercentage(100);
            tableSignature.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            tableSignature.setExtendLastRow(false);

            Font descriptionFont = new Font(urName, 12);
            descriptionFont.setColor(BaseColor.GRAY);

            PdfPCell finishDescription1 = new PdfPCell(new Phrase("برای دانلود نرم افزار موبایلی و تحت وب رأس گیر چک سایه ما را در ", descriptionFont));
            finishDescription1.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            finishDescription1.setVerticalAlignment(Element.ALIGN_CENTER);
            finishDescription1.setHorizontalAlignment(Element.ALIGN_CENTER);
            finishDescription1.setBorder(0);
//            finishDescription1.setFixedHeight(45f);

            PdfPCell finishDescription2 = new PdfPCell(new Phrase("کافه بازار اندروید و یا اپ استور آیفون جستجو نمایید.", descriptionFont));
            finishDescription2.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            finishDescription2.setVerticalAlignment(Element.ALIGN_CENTER);
            finishDescription2.setHorizontalAlignment(Element.ALIGN_CENTER);
            finishDescription2.setPaddingTop(10f);
            finishDescription2.setBorder(0);
            finishDescription2.setFixedHeight(45f);

            PdfPCell siteAddress = new PdfPCell(new Phrase("www.TaxiApps.org", robotoFont));
            siteAddress.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            siteAddress.setVerticalAlignment(Element.ALIGN_CENTER);
            siteAddress.setHorizontalAlignment(Element.ALIGN_CENTER);
            siteAddress.setBorder(0);
            siteAddress.setFixedHeight(45f);


//------------------------------------------------------------------------------------------------------------

            tableTitle.addCell(titleCell);
            tableTitle.addCell(ImageCell);
            tableDateTitle.addCell(titleDateCell);
            tableDateTitle.addCell(cell);


            factorsTable.addCell(numberTitle);
            factorsTable.addCell(factorDateTitle);
            factorsTable.addCell(factorPriceTitle);
            factorsTable.addCell(settlementDateTitle);
            factorsTable.addCell(descriptionTitle);
            factorsTable.setHeaderRows(1);

            for (int i = 0; i < factors.size(); i++) {

                PdfPCell number = new PdfPCell(new Phrase(String.valueOf(i + 1), yekan));

                persianCalendar = new PersianDate(factors.get(i).getDate());
                PdfPCell factorDate = new PdfPCell(new Phrase(Modules.getPersianShortDate(persianCalendar), yekan));

                String factorPriceVlue = String.valueOf(factors.get(i).getPrice());
                factorPriceVlue = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(factorPriceVlue));

                PdfPCell factorPrice = new PdfPCell(new Phrase(factorPriceVlue + " ریال", yekan));
                PdfPCell settlementDate = new PdfPCell(new Phrase(String.valueOf(factors.get(i).getSettlementDuration()) + (factors.get(i).getSettlementType() == 1 ? " روز" : " ماه"), yekan));
                PdfPCell description = new PdfPCell(new Phrase("    ", yekan));

                if (i != factors.size() - 1) {
                    number.setBorder(Rectangle.BOTTOM);
                    number.setBorderWidth(1f);
                    number.setBorderColor(baseColor);
                    factorDate.setBorder(Rectangle.BOTTOM);
                    factorDate.setBorderWidth(1f);
                    factorDate.setBorderColor(baseColor);
                    factorPrice.setBorder(Rectangle.BOTTOM);
                    factorPrice.setBorderWidth(1f);
                    factorPrice.setBorderColor(baseColor);
                    settlementDate.setBorder(Rectangle.BOTTOM);
                    settlementDate.setBorderWidth(1f);
                    settlementDate.setBorderColor(baseColor);
                    description.setBorder(Rectangle.BOTTOM);
                    description.setBorderWidth(1f);
                    description.setBorderColor(baseColor);
                } else {
                    number.setBorder(0);
                    factorDate.setBorder(0);
                    factorPrice.setBorder(0);
                    settlementDate.setBorder(0);
                    description.setBorder(0);
                }

                number.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                number.setVerticalAlignment(Element.ALIGN_CENTER);
                number.setHorizontalAlignment(Element.ALIGN_CENTER);
                number.setFixedHeight(25f);
                number.setPaddingBottom(0f);
                factorsTable.addCell(number);


                factorDate.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                factorDate.setVerticalAlignment(Element.ALIGN_CENTER);
                factorDate.setHorizontalAlignment(Element.ALIGN_CENTER);
                factorDate.setFixedHeight(25f);
                factorDate.setPaddingBottom(0f);
                factorsTable.addCell(factorDate);

                factorPrice.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                factorPrice.setVerticalAlignment(Element.ALIGN_CENTER);
                factorPrice.setHorizontalAlignment(Element.ALIGN_CENTER);
                factorPrice.setFixedHeight(25f);
                factorPrice.setPaddingBottom(0f);
                factorsTable.addCell(factorPrice);

                settlementDate.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                settlementDate.setVerticalAlignment(Element.ALIGN_CENTER);
                settlementDate.setHorizontalAlignment(Element.ALIGN_CENTER);
                settlementDate.setFixedHeight(25f);
                settlementDate.setPaddingBottom(0f);
                factorsTable.addCell(settlementDate);

                description.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
                description.setVerticalAlignment(Element.ALIGN_CENTER);
                description.setHorizontalAlignment(Element.ALIGN_CENTER);
                description.setFixedHeight(25f);
                description.setPaddingBottom(0f);
                factorsTable.addCell(description);

            }

            summaryTable.addCell(factorNumberTitleCell);
            summaryTable.addCell(factorSumTitleCell);
            summaryTable.addCell(chequeDateTitleCell);
            summaryTable.addCell(factorNumberCell);
            summaryTable.addCell(factorSumCell);
            summaryTable.addCell(chequeDateCell);


            tableSignature.addCell(signature1);
            tableSignature.addCell(signature2);
            tableSignature.addCell(signaturePlace1);
            tableSignature.addCell(signaturePlace2);

            finishDescriptions.addCell(finishDescription1);
            finishDescriptions.addCell(finishDescription2);
            finishDescriptions.addCell(siteAddress);

            tableTitle.setHorizontalAlignment(Element.ALIGN_LEFT);

            document.add(tableTitle);
            document.add(tableDateTitle);
            document.add(new Paragraph("\n"));
            document.add(factorsTable);
            document.add(new Paragraph("\n\n"));
            document.add(summaryTable);
            document.add(new Paragraph("\n\n"));
            document.add(tableSignature);
            document.add(new Paragraph("\n\n"));
            document.add(finishDescriptions);


            //---------------
            document.close();

            return myFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //-----------------------------------------------------------

    private static final Map<String, String> PERSIAN_DIGITS = new HashMap<>();

    static {
        PERSIAN_DIGITS.put("0", "۰");
        PERSIAN_DIGITS.put("1", "۱");
        PERSIAN_DIGITS.put("2", "۲");
        PERSIAN_DIGITS.put("3", "۳");
        PERSIAN_DIGITS.put("4", "۴");
        PERSIAN_DIGITS.put("5", "۵");
        PERSIAN_DIGITS.put("6", "۶");
        PERSIAN_DIGITS.put("7", "۷");
        PERSIAN_DIGITS.put("8", "۸");
        PERSIAN_DIGITS.put("9", "۹");
    }

    public static String persianDigits(String s) {
        StringBuffer sb = new StringBuffer();
        Matcher m = DIGIT_OR_LINK_PATTERN.matcher(s);
        while (m.find()) {
            String t = m.group(1);
            if (t.length() == 1) {
                // Digit.
                t = PERSIAN_DIGITS.get(t);
            }
            m.appendReplacement(sb, t);
        }
        m.appendTail(sb);
        return sb.toString();
    }

    private static final Pattern DIGIT_OR_LINK_PATTERN =
            Pattern.compile("(\\d|<[^>]*>)",
                    Pattern.DOTALL | Pattern.MULTILINE);

    //-----------------------------------------------------------

    public static String persianDigitToEnglishDigit(String str) {
        return str.replace("۰", "0").replace("۱", "1").replaceAll("۲", "2").replaceAll("۳", "3").replaceAll("۴", "4").replaceAll("۵", "5")
                .replaceAll("۶", "6").replaceAll("۷", "7").replaceAll("۸", "8").replaceAll("۹", "9");
    }

    //-----------------------------------------------------------

    public static boolean dateIsValid(String year, String month, String day) {
        boolean isFirst6Month;
        int yearInt = 0;
        int monthInt = 0;
        int dayInt = 0;

        if (!year.equals("") && !month.equals("") && !day.equals("")) {
            yearInt = Integer.parseInt(year);
            monthInt = Integer.parseInt(month);
            dayInt = Integer.parseInt(day);
        }

        if (yearInt < 1300 || yearInt > 1499) {
            return false;
        }

        Log.i("Year Integer", String.valueOf(yearInt));
        Log.i("is leap year", String.valueOf(new PersianDate().isLeap(yearInt)));

        if (monthInt > 0 && monthInt <= 6) {
            isFirst6Month = true;
        } else if (monthInt > 6 && monthInt <= 12) {
            isFirst6Month = false;
        } else {
            return false;
        }
        if (isFirst6Month) {
            if (dayInt <= 0 || dayInt > 31) {
                return false;
            }
        } else {
            if (dayInt <= 0 || dayInt > 30) {
                return false;
            } else {
                PersianDate persianCalendar = new PersianDate();
                persianCalendar.setShDay(1);
                persianCalendar.setShMonth(1);
                persianCalendar.setShYear(yearInt);
                Log.i("persian short date", Modules.getPersianShortDate(persianCalendar));
                Log.i("is leap year", String.valueOf(persianCalendar.isLeap()));
                if (monthInt == 12 && !persianCalendar.isLeap()) {
                    if (dayInt <= 0 || dayInt > 29) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }


    public static String macAddressHandling(Context context) {
        String letters = "abcdefghijklmnopqrstuvwxyz0123456789";
        String lettersReverse = "012345abcdef6789ghmrijklstuvwnopqxyz";
        lettersReverse = lettersReverse.toUpperCase();
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String macAddress = getMacAddress();
        macAddress = macAddress.toLowerCase();
        Log.i("mac address", macAddress);
        macAddress = macAddress.replaceAll(":", "");
        Log.i("mac address 2", macAddress);
        for (int i = 0; i < macAddress.length(); i++) {
            int target = letters.indexOf(macAddress.charAt(i));
            StringBuilder stringBuilder = new StringBuilder(macAddress);
            stringBuilder.setCharAt(i, lettersReverse.charAt(target));
            macAddress = stringBuilder.toString();
        }
        Log.i("mac address", macAddress);
        return macAddress;
    }


    public static void customToast(Context context , String title, String description, boolean isSuccess) {

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom,
                (ViewGroup) ((Activity)context).findViewById(R.id.toast_layout_root));

        ImageView image = layout.findViewById(R.id.toast_img);
        if (isSuccess)
            image.setImageResource(R.drawable.ic_success);
        else
            image.setImageResource(R.drawable.ic_unsuccess);
        TextView titleText = layout.findViewById(R.id.toast_title);
        titleText.setText(title);
        TextView descriptionText = layout.findViewById(R.id.toast_description);
        descriptionText.setText(description);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 25);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static String getPersianShortDate (PersianDate persianDate){
        return new PersianDateFormat("Y/m/d").format(persianDate);
    }

}


