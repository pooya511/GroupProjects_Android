package Other;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurelhubert.simpleratingbar.SimpleRatingBar;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import TxAnalytics.TxAnalytics;
import air.gamediesel.rasgir.MainActivity;
import air.gamediesel.rasgir.MyApplication;
import air.gamediesel.rasgir.PasswordActivity;
import air.gamediesel.rasgir.R;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

import static TxAnalytics.TxAnalytics.MessageAdd;
import static TxAnalytics.TxAnalytics.RateUs;

public class DialogUtils {

    private static PersianDate finalDate;
    private static TextWatcher textWatcher;
    private static PersianDate persianCalender;

    public enum MessageDialogTypes {

        ERROR("Error"),
        NORMAL("Normal"),
        SUCCESS("Success"),
        WARNING("Warning");

        private final String name;

        private MessageDialogTypes(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }
    }

    public enum keyPadTypes {

        PRICE("Price"),
        NORMAL_NUMBER("normalNumber");

        private final String name;

        private keyPadTypes(String s) {
            name = s;
        }

        public String toString() {
            return this.name;
        }

    }

    public static Dialog backDialog(final Context context) {

        //
        final Dialog backDialog = new Dialog(context);
        backDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        backDialog.setContentView(R.layout.dialog_back);
        backDialog.setCanceledOnTouchOutside(false);
        backDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        backDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        final TextView nope = backDialog.findViewById(R.id.dialog_back_no_text);
        final TextView back = backDialog.findViewById(R.id.dialog_back_back_text);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.equals(nope)) {
                    backDialog.dismiss();
                }
                if (view.equals(back)) {
                    ((Activity) context).finish();
                }
            }
        };
        nope.setOnClickListener(listener);
        back.setOnClickListener(listener);
        //
        return backDialog;
    }

    public static Dialog backToHomeDialog(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_back, null);
        int width;
        int height;
        switch (Modules.screenSize(context)) {
            case 1:
                width = 85;
                height = 16;
                break;
            case 2:
                width = 50;
                height = 20;
                break;
            case 3:
                width = 50;
                height = 8;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }
        final Dialog backToHome = makeDialog(context, view, false, width, height);
        //
        final TextView nope = view.findViewById(R.id.dialog_back_no_text);
        final TextView home = view.findViewById(R.id.dialog_back_back_text);
        //
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.equals(nope)) {
                    backToHome.dismiss();
                }
                if (view.equals(home)) {
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("isLogin", true);
                    context.startActivity(intent);
                }
            }
        };
        //
        nope.setOnClickListener(listener);
        home.setOnClickListener(listener);
        //
        return backToHome;
    }

    public static Dialog makeDialog(Context context, View view, boolean setCanceledOnTouchOutside, int width, int height) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //
        int ScreenSizeResults[] = Modules.getWidthAndHeight(context, width, height);
        lp.width = ScreenSizeResults[0];
        lp.height = ScreenSizeResults[1];
        //
        dialogWindow.setAttributes(lp);
        dialog.setCanceledOnTouchOutside(setCanceledOnTouchOutside);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    public static Dialog makeMessageDialog(final Context context, DialogUtils.MessageDialogTypes mesType, String title, String description, final String link) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_error_msg, null);
        int width;
        int height;
        switch (Modules.screenSize(context)) {
            case 1:
                width = 85;
                height = 22;
                break;
            case 2:
                width = 65;
                height = 15;
                break;
            case 3:
                width = 50;
                height = 11;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }
        final Dialog dialog = makeDialog(context, view, false, width, height);
        //
        final TextView titleTextView = view.findViewById(R.id.dialog_error_msg_title);
        final TextView descriptionTextView = view.findViewById(R.id.dialog_error_msg_description);
        switch (mesType) {
            case SUCCESS:
                titleTextView.setTextColor(Color.parseColor("#01a9d6"));
//                titleLayout.setBackgroundColor(Color.parseColor("#b3f0ff"));
                break;
            case ERROR:
                titleTextView.setTextColor(Color.parseColor("#f44336"));
//                titleLayout.setBackgroundColor(Color.parseColor("#ffb3b3"));
                break;
            default:
                titleTextView.setTextColor(Color.BLACK);
                break;
        }
        titleTextView.setText(title);
        descriptionTextView.setText(description);
        final LinearLayout closeButton = view.findViewById(R.id.dialog_rate_us_cancel);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (!link.equals("")) {
                    Intent intention = new Intent(Intent.ACTION_VIEW);
                    intention.setData(Uri.parse("http://" + link));
                    context.startActivity(intention);
                }
            }
        });
        //
        return dialog;
    }

    public static Dialog rateUsDialog(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_rate_us);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
        //
        final EditText commentEditText = dialog.findViewById(R.id.dialog_rate_us_comment_edittext);
        final SimpleRatingBar ratingBar = dialog.findViewById(R.id.dialog_rate_us_ratingbar);
        final LinearLayout submit = dialog.findViewById(R.id.dialog_rate_us_submit);
        final LinearLayout cancel = dialog.findViewById(R.id.dialog_rate_us_cancel);
        //
        if (C_Setting.getStringValue(C_Setting.USER_RATE) != null && !C_Setting.getStringValue(C_Setting.USER_RATE).equals("")) {
            ratingBar.setRating(Integer.parseInt(C_Setting.getStringValue(C_Setting.USER_RATE)));
        }
        //
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.equals(cancel)) {
                    dialog.dismiss();
                }
                if (view.equals(submit)) {
                    if (Modules.isNetworkAvailable(context)) {
                        final String stars = String.valueOf(ratingBar.getRating());
                        Log.i("Rating Bar", stars);
                        try {
                            String userNumber = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                            RateUs(stars,
                                    Constants.APP_ID,
                                    Constants.APP_SOURCE,
                                    Constants.APP_VERSION,
                                    Constants.DEVICE_ID,
                                    userNumber,
                                    Constants.SERVER_URL, new Callback() {

                                        private String rateLink;

                                        @Override
                                        public void onFailure(Request request, IOException e) {
                                            Log.i("rate", "Not working");
                                        }

                                        @Override
                                        public void onResponse(Response response) throws IOException {
                                            if (response.isSuccessful()) {
                                                try {
                                                    Log.i("rate", "is working");
                                                    String responseStr = response.body().string();
                                                    Log.i("rating responseStr", responseStr);
                                                    JSONObject responseJson = new JSONObject(Modules.makeJsonString(responseStr));
                                                    final String retMessage = responseJson.getString("ret_msg");
                                                    rateLink = "";
                                                    if (!responseJson.getString("rate_link").equals("")) {
                                                        rateLink = responseJson.getString("rate_link");
                                                    }
                                                    C_Setting.setStringValue(C_Setting.USER_RATE, stars); // ذخیره در تنظیمات
                                                    ((Activity) context).runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            makeMessageDialog(context, MessageDialogTypes.NORMAL, "موفق", retMessage, rateLink).show();
                                                        }
                                                    });
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }
                                    });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String comment;
                        if (!commentEditText.getText().toString().equals("")) {
                            comment = commentEditText.getText().toString();
                            try {
                                String userNumber = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                                MessageAdd(comment,
                                        TxAnalytics.MessageTypes.MESSAGE,
                                        Constants.APP_ID,
                                        Constants.APP_SOURCE,
                                        Constants.APP_VERSION,
                                        Constants.DEVICE_ID,
                                        userNumber,
                                        Constants.SERVER_URL,
                                        new Callback() {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                Log.i("message sending", "not working");
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if (response.isSuccessful()) {
                                                    Log.i("message sending", "is working");
                                                }
                                            }
                                        });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        dialog.dismiss();
                    } else {
                        makeMessageDialog(context, MessageDialogTypes.ERROR, "خطای اینترنت", "لطفا به اینترنت متصل شوید", "").show();
                    }
                }
            }
        };
        //
        submit.setOnClickListener(listener);
        cancel.setOnClickListener(listener);
        //
        return dialog;
    }

    public static Dialog latestChange(Context context) {
        Dialog latestChangeDialog = new Dialog(context);
        latestChangeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        latestChangeDialog.setContentView(R.layout.pop_latest_change);
        latestChangeDialog.setCanceledOnTouchOutside(true);
        latestChangeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView close = latestChangeDialog.findViewById(R.id.pop_latest_change_close);
        TextView version = latestChangeDialog.findViewById(R.id.pop_latest_change_version_text_view);
        version.setText("بروز رسانی به نسخه " + PublicModules.toPersianDigit(Constants.APP_VERSION));

        close.setOnClickListener(v -> latestChangeDialog.dismiss());

        return latestChangeDialog;
    }

    public static Dialog reportChecksAndFactors(final Context context, long nearestDate, long hindmostDate, double maximumPrice, double minimumPrice) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_report_check_and_factor);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
        //
        final TextView nearest = dialog.findViewById(R.id.dialog_report_nearest);
        final TextView hindmost = dialog.findViewById(R.id.dialog_report_hindmost);
        final TextView maximum = dialog.findViewById(R.id.dialog_report_maximum_price);
        final TextView minimum = dialog.findViewById(R.id.dialog_report_minimum_price);
        //
        PersianDate persianCalendar;
        if (nearestDate != 0) {
            persianCalendar = new PersianDate(nearestDate);
            nearest.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
        } else {
            nearest.setText("_");
        }
        if (hindmostDate != 0) {
            persianCalendar = new PersianDate(hindmostDate);
            hindmost.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
        } else {
            hindmost.setText("_");
        }
        //
        String minPrice = String.valueOf(minimumPrice);
        minPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(minPrice));
        minimum.setText(PublicModules.toPersianDigit(minPrice.concat(" " + C_Setting.getStringValue(C_Setting.APP_CURRENCY))));
        //
        String maxPrice = String.valueOf(maximumPrice);
        maxPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(maxPrice));
        maximum.setText(PublicModules.toPersianDigit(maxPrice.concat(" " + C_Setting.getStringValue(C_Setting.APP_CURRENCY))));
        //
        Button close = dialog.findViewById(R.id.dialog_report_button);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        //
        return dialog;
    }

    public static Dialog telegram(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_telegram);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
        //
        final TextView channel = dialog.findViewById(R.id.dialog_telegram_see_channel);
        final TextView close = dialog.findViewById(R.id.dialog_telegram_cancel);
        //

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.equals(close)) {
                    dialog.dismiss();
                }
                if (view.equals(channel)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/taxiapps"));
                    context.startActivity(intent);
                    dialog.dismiss();
                }
            }
        };
        //
        close.setOnClickListener(listener);
        channel.setOnClickListener(listener);
        //
        return dialog;
    }

    public static Dialog fingerDialog(Context context) {

        final Dialog fingerDialog = new Dialog(context);
        fingerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fingerDialog.setContentView(R.layout.dialog_finger_layout);
        fingerDialog.setCanceledOnTouchOutside(false);
        fingerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        fingerDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);

        fingerDialog.show();
        //
        TextView cancel = fingerDialog.findViewById(R.id.dialog_finger_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PasswordActivity.disposeScanner(PasswordActivity.disposable);
                fingerDialog.dismiss();
            }
        });
        //
        return fingerDialog;
    }

    public static Dialog loadingDialog(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_load, null);
        final Dialog dialog = makeDialog(context, view, false, 100, 100);
        new CountDownTimer(3000, 10000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                dialog.dismiss();
            }
        }.start();
        return dialog;
    }

}
