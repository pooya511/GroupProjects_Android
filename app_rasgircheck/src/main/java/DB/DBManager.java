package DB;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

import OOP.Check;
import OOP.CheckGroup;
import OOP.Factor;
import OOP.FactorGroup;
import Other.Constants;
import Other.Modules;

public class DBManager {

    private DataBaseOpenHelper dataBaseOpenHelper;
    private SQLiteDatabase database;

    public DBManager(Context context) {
        this.dataBaseOpenHelper = new DataBaseOpenHelper(context);
    }

    /**
     * open the dataBase connection for write
     */
    private void openForWrite() {
        this.database = dataBaseOpenHelper.getWritableDatabase();
    }

    /**
     * open the dataBase connection for read
     */
    private void openForRead() {
        this.database = dataBaseOpenHelper.getReadableDatabase();
    }

    /**
     * close the dataBase Connection
     */
    private void close() {
        if (database != null) {
            this.database.close();
        }
    }


    public void insertCheck(Check check) {
        openForWrite();
        long checkDate = check.getDate();
        long checkOffsetDate = check.getOffsetDate();
        int checkIsActive;
        if (check.isCheckIsActive()) {
            checkIsActive = 1;
        } else {
            checkIsActive = 0;
        }
        // Log.i("check price insert",check.getPrice() );
        ContentValues cv = new ContentValues(1);
        cv.put(Check.CHECK_PRICE_COLUMN, check.getPrice());
        cv.put(Check.CHECK_GROUP_ID_COLUMN, check.getGroupId());
        cv.put(Check.CHECK_DATE_COLUMN, checkDate);
        cv.put(Check.CHECK_OFFSET_DATE_COLUMN, checkOffsetDate);
        cv.put(Check.CHECK_IS_ACTIVE_COLUMN, checkIsActive);
        database.insert(Check.CHECK_TABLE, null, cv);
        Cursor cursor = database.rawQuery("SELECT MAX(" + Check.CHECK_ID_COLUMN + ") as ID FROM T_Checks_Details", null);
        if (cursor.moveToNext()) {
            int insertedID = cursor.getInt(0);
            check.setId(insertedID);
        }
        close();
    }

    public ArrayList<Check> selectAllChecks() {
        openForRead();
        ArrayList<Check> checkArrayList = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM " + Check.CHECK_TABLE, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Check check = new Check();
            check.setId(cursor.getInt(0));
            check.setGroupId(cursor.getInt(1));
            check.setPrice(cursor.getDouble(2));
            check.setDate(cursor.getLong(3));
            check.setOffsetDate(cursor.getLong(4));
            int isActive = cursor.getInt(5);
            if (isActive == 1) {
                check.setCheckIsActive(true);
            } else {
                check.setCheckIsActive(false);
            }
            checkArrayList.add(check);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return checkArrayList;
    }

    public void updateCheck(Check oldCheck, Check newCheck) {
        openForWrite();
        ContentValues cv = new ContentValues();
        long checkDate = newCheck.getDate();
        long checkOffsetDate = newCheck.getOffsetDate();
        int checkIsActive;
        if (newCheck.isCheckIsActive()) {
            checkIsActive = 1;
        } else {
            checkIsActive = 0;
        }
        cv.put(Check.CHECK_PRICE_COLUMN, newCheck.getPrice());
        cv.put(Check.CHECK_GROUP_ID_COLUMN, newCheck.getGroupId());
        cv.put(Check.CHECK_DATE_COLUMN, checkDate);
        cv.put(Check.CHECK_OFFSET_DATE_COLUMN, checkOffsetDate);
        cv.put(Check.CHECK_IS_ACTIVE_COLUMN, checkIsActive);
        database.update(Check.CHECK_TABLE, cv, Check.CHECK_ID_COLUMN + " = ?", new String[]{String.valueOf(oldCheck.getId())});
        close();
    }

    public ArrayList<Check> selectCheckByGroupId(int id) {
        openForRead();
        ArrayList<Check> checks = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM " + Check.CHECK_TABLE + " WHERE " + Check.CHECK_GROUP_ID_COLUMN + " = " + id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Check check = new Check();
            check.setId(cursor.getInt(0));
            check.setGroupId(cursor.getInt(1));
            check.setPrice(cursor.getDouble(2));
            check.setDate(cursor.getLong(3));
            check.setOffsetDate(cursor.getLong(4));
            int isActive = cursor.getInt(5);
            if (isActive == 1) {
                check.setCheckIsActive(true);
            } else {
                check.setCheckIsActive(false);
            }
            checks.add(check);
            cursor.moveToNext();
        }
        close();
        return checks;
    }

    public void deleteCheckByChkId(int checkGroupId) {
        openForWrite();
        database.execSQL("DELETE FROM " + Check.CHECK_TABLE + " WHERE chkID = " + checkGroupId);
        close();
    }

    public void deleteCheck(Check check) {
        openForWrite();
        database.delete(Check.CHECK_TABLE, Check.CHECK_ID_COLUMN + " = ?", new String[]{String.valueOf(check.getId())});
        close();
    }

    public CheckGroup selectCheckGroupById(int id) {
        openForRead();
        CheckGroup checkGroup = new CheckGroup();
        Cursor cursor = database.rawQuery("SELECT * FROM T_Checks AS A LEFT JOIN (SELECT chkID,SUM(chdPrice) AS chkTotalPrice,COUNT(*) AS chkCheckCount FROM T_Checks_Details WHERE chdIsActive=1 GROUP BY chkID) AS B ON A.ID = B.chkID  WHERE ID = " + id, null);
        cursor.moveToFirst();
        checkGroup.setId(cursor.getInt(0));
        checkGroup.setName(cursor.getString(1));
        checkGroup.setGroupOffset(cursor.getLong(2));
        boolean isAdvanced;
        {
            if (cursor.getInt(3) == 0) {
                isAdvanced = false;
            } else {
                isAdvanced = true;
            }
        }
        checkGroup.setCheckIsAdvanced(isAdvanced);
        checkGroup.setCheckExportDate(cursor.getLong(4));
        checkGroup.setCheckInterestPercent(cursor.getDouble(5));
        checkGroup.setChkId(cursor.getInt(7));
        checkGroup.setChkTotalPrice(cursor.getDouble(8));
        checkGroup.setChkCheckCount(cursor.getInt(9));
        cursor.close();
        close();
        return checkGroup;
    }

    public int insertCheckGroup(CheckGroup checkGroup) {
        openForWrite();
        long checkGroupOffsetDate = checkGroup.getGroupOffset();
        long checkExportDate = checkGroup.getCheckExportDate();
        ContentValues cv = new ContentValues();
        cv.put(CheckGroup.CHECK_GROUP_NAME, checkGroup.getName());
        cv.put(CheckGroup.CHECK_GROUP_OFFSET_COLUMN, checkGroupOffsetDate);
        int isAdvanced;
        if (checkGroup.isCheckIsAdvanced()) {
            isAdvanced = 1;
        } else {
            isAdvanced = 0;
        }
        cv.put(CheckGroup.CHECK_GROUP_ADVANCED_COLUMN, isAdvanced);
        cv.put(CheckGroup.CHECK_GROUP_EXPORT_DATE, checkExportDate);
        cv.put(CheckGroup.CHECK_GROUP_INTEREST_PERCENT, checkGroup.getCheckInterestPercent());
        database.insert(CheckGroup.CHECK_GROUP_TABLE, null, cv);
        Cursor cursor = database.rawQuery("SELECT MAX(" + CheckGroup.CHECK_GROUP_ID + ") as ID FROM " + CheckGroup.CHECK_GROUP_TABLE, null);
        if (cursor.moveToNext()) {
            int insertedID = cursor.getInt(0);
            checkGroup.setId(insertedID);
        }
        cursor.close();
        close();
        return checkGroup.getId();
    }

    public ArrayList<CheckGroup> selectAllCheckGroups() {
        openForRead();
        ArrayList<CheckGroup> checkGroups = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM T_Checks AS A LEFT JOIN (SELECT chkID,SUM(chdPrice) AS chkTotalPrice,COUNT(*) AS chkCheckCount FROM T_Checks_Details WHERE chdIsActive=1 GROUP BY chkID) AS B ON A.ID = B.chkID", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CheckGroup checkGroup = new CheckGroup();
            checkGroup.setId(cursor.getInt(0));
            checkGroup.setName(cursor.getString(1));
            checkGroup.setGroupOffset(cursor.getLong(2));
            boolean isAdvanced;
            {
                if (cursor.getInt(3) == 0) {
                    isAdvanced = false;
                } else {
                    isAdvanced = true;
                }
            }
            checkGroup.setCheckIsAdvanced(isAdvanced);
            checkGroup.setCheckExportDate(cursor.getLong(4));
            checkGroup.setCheckInterestPercent(cursor.getDouble(5));
            checkGroup.setChkId(cursor.getInt(7));
            checkGroup.setChkTotalPrice(cursor.getDouble(8));
            checkGroup.setChkCheckCount(cursor.getInt(9));
            checkGroups.add(checkGroup);
            cursor.moveToNext();
        }
        close();
        return checkGroups;
    }

    public void deleteCheckGroup(CheckGroup checkGroup) {
        openForWrite();
        database.delete(CheckGroup.CHECK_GROUP_TABLE, CheckGroup.CHECK_GROUP_ID + " = ?", new String[]{String.valueOf(checkGroup.getId())});
        close();
    }

    public void updateCheckGroup(CheckGroup newCheckGroup) {
        openForWrite();
        ContentValues cv = new ContentValues();
        long checkGroupOffsetDate = newCheckGroup.getGroupOffset();
        long checkExportDate = newCheckGroup.getCheckExportDate();
        cv.put(CheckGroup.CHECK_GROUP_NAME, newCheckGroup.getName());
        cv.put(CheckGroup.CHECK_GROUP_OFFSET_COLUMN, checkGroupOffsetDate);
        int isAdvanced;
        if (newCheckGroup.isCheckIsAdvanced()) {
            isAdvanced = 1;
        } else {
            isAdvanced = 0;
        }
        cv.put(CheckGroup.CHECK_GROUP_ADVANCED_COLUMN, isAdvanced);
        cv.put(CheckGroup.CHECK_GROUP_EXPORT_DATE, checkExportDate);
        cv.put(CheckGroup.CHECK_GROUP_INTEREST_PERCENT, newCheckGroup.getCheckInterestPercent());
        database.update(CheckGroup.CHECK_GROUP_TABLE, cv, CheckGroup.CHECK_GROUP_ID + " = ?", new String[]{String.valueOf(newCheckGroup.getId())});
        close();
    }

    public void insertFactor(Factor factor) {
        openForWrite();
        ContentValues cv = new ContentValues();
        long date = factor.getDate();
        cv.put(Factor.FACTOR_GROUP_ID_COLUMN, factor.getGroupId());
        cv.put(Factor.FACTOR_PRICE_COLUMN, factor.getPrice());
        cv.put(Factor.FACTOR_DATE_COLUMN, date);
        cv.put(Factor.FACTOR_SETTLEMENT_DURATION_COLUMN, factor.getSettlementDuration());
        cv.put(Factor.FACTOR_SETTLEMENT_TYPE_COLUMN, factor.getSettlementType());
        int isActive;
        if (factor.isActive()) {
            isActive = 1;
        } else {
            isActive = 0;
        }
        cv.put(Factor.FACTOR_IS_ACTIVE_COLUMN, isActive);
        database.insert(Factor.FACTOR_TABLE, null, cv);
        Cursor cursor = database.rawQuery("SELECT MAX(" + Factor.FACTOR_ID_COLUMN + ") as ID FROM " + Factor.FACTOR_TABLE, null);
        if (cursor.moveToNext()) {
            int insertedID = cursor.getInt(0);
            factor.setId(insertedID);
        }
        close();
    }

    public ArrayList<Factor> selectAllFactors() {
        openForRead();
        ArrayList<Factor> factorArrayList = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM " + Factor.FACTOR_TABLE, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Factor factor = new Factor();
            factor.setId(cursor.getInt(0));
            factor.setGroupId(cursor.getInt(1));
            factor.setPrice(cursor.getLong(2));
            factor.setDate(cursor.getLong(3));
            factor.setSettlementDuration(cursor.getInt(4));
            factor.setSettlementType(cursor.getInt(5));
            int isActive = cursor.getInt(6);
            if (isActive == 0) {
                factor.setIsActive(false);
            } else {
                factor.setIsActive(true);
            }
            factorArrayList.add(factor);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return factorArrayList;
    }

    public ArrayList<Factor> selectFactorByGroupId(int id) {
        openForRead();
        ArrayList<Factor> factorArrayList = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM " + Factor.FACTOR_TABLE + " WHERE " + Factor.FACTOR_GROUP_ID_COLUMN + " = " + id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Factor factor = new Factor();
            factor.setId(cursor.getInt(0));
            factor.setGroupId(cursor.getInt(1));
            factor.setPrice(cursor.getLong(2));
            factor.setDate(cursor.getLong(3));
            factor.setSettlementDuration(cursor.getInt(4));
            factor.setSettlementType(cursor.getInt(5));
            if (cursor.getInt(6) == 1) {
                factor.setIsActive(true);
            } else {
                factor.setIsActive(false);
            }
            factorArrayList.add(factor);
            cursor.moveToNext();
        }
        close();
        return factorArrayList;
    }

    public void deleteFactor(Factor factor) {
        openForWrite();
        database.delete(Factor.FACTOR_TABLE, Factor.FACTOR_ID_COLUMN + " = ?", new String[]{String.valueOf(factor.getId())});
        close();
    }

    public FactorGroup selectFactorGruopById(int id) {
        openForRead();
        FactorGroup factorGroup = new FactorGroup();
        Cursor cursor = database.rawQuery("SELECT * FROM T_Factors AS A "
                + " LEFT JOIN (SELECT fctID,SUM(fcdPrice) AS fcdTotalPrice,COUNT(*) AS fcdFactorCount "
                + " FROM T_Factors_Details WHERE fcdIsActive=1 GROUP BY fctID) AS B ON A.ID = B.fctID WHERE ID=" + id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            factorGroup.setId(cursor.getInt(0));
            factorGroup.setName(cursor.getString(1));
            factorGroup.setDeadLineDate(cursor.getLong(2));
            factorGroup.setExportDate(cursor.getLong(3));
            boolean noPrice;
            if (cursor.getInt(5) == 0) {
                noPrice = false;
            } else {
                noPrice = true;
            }
            factorGroup.setNoPrice(noPrice);
            factorGroup.setFcdTotalPrice(cursor.getDouble(7));
            factorGroup.setFcdFactorCount(cursor.getInt(8));
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return factorGroup;
    }

    public void updateFactor(Factor oldFactor, Factor newFactor) {
        openForWrite();
        ContentValues cv = new ContentValues();
        long date = newFactor.getDate();
        cv.put(Factor.FACTOR_GROUP_ID_COLUMN, newFactor.getGroupId());
        cv.put(Factor.FACTOR_PRICE_COLUMN, newFactor.getPrice());
        cv.put(Factor.FACTOR_DATE_COLUMN, date);
        cv.put(Factor.FACTOR_SETTLEMENT_DURATION_COLUMN, newFactor.getSettlementDuration());
        cv.put(Factor.FACTOR_SETTLEMENT_TYPE_COLUMN, newFactor.getSettlementType());
        int isActive;
        if (newFactor.isActive()) {
            isActive = 1;
        } else {
            isActive = 0;
        }
        cv.put(Factor.FACTOR_IS_ACTIVE_COLUMN, isActive);
        database.update(Factor.FACTOR_TABLE, cv, Factor.FACTOR_ID_COLUMN + " = ?", new String[]{String.valueOf(oldFactor.getId())});
        close();
    }

    public int insertFactorGroup(FactorGroup factorGroup) {
        openForWrite();
        ContentValues cv = new ContentValues();
        long deadLine = factorGroup.getDeadLineDate();
        long export = factorGroup.getExportDate();
        cv.put(FactorGroup.FACTOR_GROUP_NAME_COLUMN, factorGroup.getName());
        cv.put(FactorGroup.FACTOR_GROUP_DEAD_LINE_COLUMN, deadLine);
        cv.put(FactorGroup.FACTOR_GROUP_EXPORT_DATE_COLUMN, export);
        int noPrice;
        if (factorGroup.isNoPrice()) {
            noPrice = 1;
        } else {
            noPrice = 0;
        }
        cv.put(FactorGroup.FACTOR_GROUP_NO_PRICE_COLUMN, noPrice);
        database.insert(FactorGroup.FACTOR_GROUP_TABLE, null, cv);
        Cursor cursor = database.rawQuery("SELECT MAX(" + FactorGroup.FACTOR_GROUP_ID_COLUMN + ") as ID FROM " + FactorGroup.FACTOR_GROUP_TABLE, null);
        if (cursor.moveToNext()) {
            int insertedID = cursor.getInt(0);
            factorGroup.setId(insertedID);
        }
        close();
        return factorGroup.getId();
    }

    public ArrayList<FactorGroup> selectAllFactorGroups() {
        openForRead();
        ArrayList<FactorGroup> factorGroupArrayList = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM T_Factors AS A "
                + " LEFT JOIN (SELECT fctID,SUM(fcdPrice) AS fcdTotalPrice,COUNT(*) AS fcdFactorCount "
                + " FROM T_Factors_Details WHERE fcdIsActive=1 GROUP BY fctID) AS B ON A.ID = B.fctID", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FactorGroup factorGroup = new FactorGroup();
            factorGroup.setId(cursor.getInt(0));
            factorGroup.setName(cursor.getString(1));
            factorGroup.setDeadLineDate(cursor.getLong(2));
            factorGroup.setExportDate(cursor.getLong(3));
            boolean noPrice;
            if (cursor.getInt(5) == 0) {
                noPrice = false;
            } else {
                noPrice = true;
            }
            factorGroup.setNoPrice(noPrice);
            factorGroup.setFctId(cursor.getInt(6));
            factorGroup.setFcdTotalPrice(cursor.getDouble(7));
            factorGroup.setFcdFactorCount(cursor.getInt(8));
            factorGroupArrayList.add(factorGroup);
            cursor.moveToNext();
        }
        close();
        return factorGroupArrayList;
    }

    public void deleteFactorGroup(FactorGroup factorGroup) {
        openForWrite();
        database.delete(FactorGroup.FACTOR_GROUP_TABLE, FactorGroup.FACTOR_GROUP_ID_COLUMN + " = ?", new String[]{String.valueOf(factorGroup.getId())});
        close();
    }

    public void updateFactorGroup(FactorGroup newFactorGroup) {
        openForWrite();
        ContentValues cv = new ContentValues();
        long deadLine = newFactorGroup.getDeadLineDate();
        long export = newFactorGroup.getExportDate();
        cv.put(FactorGroup.FACTOR_GROUP_NAME_COLUMN, newFactorGroup.getName());
        cv.put(FactorGroup.FACTOR_GROUP_DEAD_LINE_COLUMN, deadLine);
        cv.put(FactorGroup.FACTOR_GROUP_EXPORT_DATE_COLUMN, export);
        int noPrice;
        if (newFactorGroup.isNoPrice()) {
            noPrice = 1;
        } else {
            noPrice = 0;
        }
        cv.put(FactorGroup.FACTOR_GROUP_NO_PRICE_COLUMN, noPrice);
        database.update(FactorGroup.FACTOR_GROUP_TABLE, cv, FactorGroup.FACTOR_GROUP_ID_COLUMN + " = ?", new String[]{String.valueOf(newFactorGroup.getId())});
        close();
    }

    public void deleteFactorByfctId(int factorGroupId) {
        openForWrite();
        database.execSQL("DELETE FROM " + Factor.FACTOR_TABLE + " WHERE fctID = " + factorGroupId);
        close();
    }

    public void deleteAllRecords() {
        openForWrite();
        database.execSQL("DELETE FROM " + Factor.FACTOR_TABLE);
        close();
    }


    public void replaceOldDatabases(Context context, TX_License.SetLicenseCallback callback) {

        PackageManager m = context.getPackageManager();
        String s = context.getPackageName();
        PackageInfo p = null;
        try {
            p = m.getPackageInfo(s, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        s = p.applicationInfo.dataDir;
        Log.i("path", s);


        // ----- cheque database handling


        File oldChequeDataBaseDirectory = new File(s + "/gamediesel.rasgir/Local Store/cheques.db");
        if (oldChequeDataBaseDirectory.exists()) {
            SQLiteDatabase oldCheckDataBase = SQLiteDatabase.openDatabase(oldChequeDataBaseDirectory.toString(), null, SQLiteDatabase.OPEN_READONLY);
            Cursor checkGroupCursor = oldCheckDataBase.rawQuery("SELECT * FROM t_groups", null);
            checkGroupCursor.moveToFirst();
            openForWrite();
            while (!checkGroupCursor.isAfterLast()) {
                CheckGroup checkGroup = new CheckGroup();
                checkGroup.setName(Modules.reverseString(checkGroupCursor.getString(1)));
                checkGroup.setChkCheckCount(Integer.parseInt(checkGroupCursor.getString(2)));
                checkGroup.setChkTotalPrice(Double.parseDouble(checkGroupCursor.getString(3).replace(",", "")));
                // ---- date handling
                String[] date = checkGroupCursor.getString(4).split(",");
                Log.i("date", date[1]);
                PersianCalendar pCalendar = new PersianCalendar();
                pCalendar.parse(date[1]);
                pCalendar.setPersianDate(pCalendar.getPersianYear(), pCalendar.getPersianMonth() - 1, pCalendar.getPersianDay());
                checkGroup.setGroupOffset(pCalendar.getTimeInMillis());
                //
                int chkId = insertCheckGroup(checkGroup);
                //
                Cursor checkCursor = oldCheckDataBase.rawQuery("SELECT * FROM [" + checkGroupCursor.getString(1) + "]", null);
                checkCursor.moveToFirst();
                while (!checkCursor.isAfterLast()) {
                    Check check = new Check();
                    Log.i(checkCursor.getColumnName(0), String.valueOf(checkCursor.getString(0)));
                    Log.i(checkCursor.getColumnName(1), checkCursor.getString(1));
                    check.setGroupId(chkId);
                    check.setPrice(Double.parseDouble(checkCursor.getString(0).replaceAll(",", "")));
                    // ------ date handling
                    PersianCalendar persianCalendar = new PersianCalendar();
                    String[] year_month_day = checkCursor.getString(1).split("/");
                    if (Modules.dateIsValid(year_month_day[0], year_month_day[1], year_month_day[2])) {
                        persianCalendar.parse(checkCursor.getString(1));
                    }
                    persianCalendar.setPersianDate(persianCalendar.getPersianYear(), persianCalendar.getPersianMonth() - 1, persianCalendar.getPersianDay());
                    check.setDate(persianCalendar.getTimeInMillis());
                    //
                    check.setOffsetDate(checkGroup.getGroupOffset());
                    check.setCheckIsActive(true);
                    //
                    insertCheck(check);
                    checkCursor.moveToNext();
                }
                //
                checkCursor.close();
                checkGroupCursor.moveToNext();
            }
            checkGroupCursor.close();
            close();
        } else {

        }

        // ----- factor database handling

        File oldFactorDataBaseDirectory = new File(s + "/gamediesel.rasgir/Local Store/factors.db");
        if (oldFactorDataBaseDirectory.exists()) {
            SQLiteDatabase oldFactorDataBase = SQLiteDatabase.openDatabase(oldFactorDataBaseDirectory.toString(), null, SQLiteDatabase.OPEN_READONLY);
            Cursor factorGroupCursor = oldFactorDataBase.rawQuery("SELECT * FROM t_groups", null);

            factorGroupCursor.moveToFirst();
            openForWrite();
            while (!factorGroupCursor.isAfterLast()) {
                FactorGroup factorGroup = new FactorGroup();
                factorGroup.setName(Modules.reverseString(factorGroupCursor.getString(1)));
                factorGroup.setFcdFactorCount(Integer.parseInt(factorGroupCursor.getString(2)));
                factorGroup.setFcdTotalPrice(Double.parseDouble(factorGroupCursor.getString(3).replace(",", "")));
                // ----- date handling
                PersianCalendar perCalendar = new PersianCalendar();
                perCalendar.parse(factorGroupCursor.getString(4));
                perCalendar.setPersianDate(perCalendar.getPersianYear(), perCalendar.getPersianMonth() - 1, perCalendar.getPersianDay());
                // ------
                int fctId = insertFactorGroup(factorGroup);
                //
                Cursor factorCursor = oldFactorDataBase.rawQuery("SELECT * FROM [" + factorGroupCursor.getString(1) + "]", null);
                factorCursor.moveToFirst();
                while (!factorCursor.isAfterLast()) {
                    Factor factor = new Factor();
                    //
                    Log.i(factorCursor.getColumnName(0), factorCursor.getString(0));
                    Log.i(factorCursor.getColumnName(1), factorCursor.getString(1));
                    //
                    factor.setGroupId(fctId);
                    factor.setPrice(Double.parseDouble(factorCursor.getString(0)));
                    // --- date handling
                    PersianCalendar pCal = new PersianCalendar();
                    String[] YEAR_MONTH_DAY = factorCursor.getString(1).split("/");
                    if (Modules.dateIsValid(YEAR_MONTH_DAY[0], YEAR_MONTH_DAY[1], YEAR_MONTH_DAY[2])) {
                        pCal.parse(factorCursor.getString(1));
                    }
                    pCal.setPersianDate(pCal.getPersianYear(), pCal.getPersianMonth() - 1, pCal.getPersianDay());
                    factor.setDate(pCal.getTimeInMillis());
                    //
                    factor.setSettlementDuration(Integer.parseInt(factorGroupCursor.getString(5)));
                    factor.setSettlementType(1);
                    factor.setIsActive(true);
                    //
                    insertFactor(factor);
                    //
                    factorCursor.moveToNext();
                }
                factorCursor.close();
                factorGroupCursor.moveToNext();
            }

            factorGroupCursor.close();
            close();
        } else {

        }
        File oldDataTableDirectory = new File(s + "/gamediesel.rasgir/Local Store/data.db");
        if (oldDataTableDirectory.exists()) {
            SQLiteDatabase oldDataDataBase = SQLiteDatabase.openDatabase(oldDataTableDirectory.toString(), null, SQLiteDatabase.OPEN_READONLY);
            Cursor dataCursor = oldDataDataBase.rawQuery("SELECT * FROM t_ver", null);
            dataCursor.moveToFirst();
            while (!dataCursor.isAfterLast()) {
                String version = dataCursor.getString(0);
                if (version.equals("1")) {
                    PersianCalendar persianCalendar = new PersianCalendar();
                    String timeZone = persianCalendar.getPersianLongDateAndTime();
                    String license = "{\\\"status\\\":\\\"0\\\""
                            + ",\\\"license_product\\\":\\\"full_lifetime\\\""
                            + ",\\\"license_valid_days\\\":\\\"863913600000\\\""
                            + ",\\\"app_fa\\\":\\\"راسگیر چک سایه\\\""
                            + ",\\\"appid\\\":\\\"" + Constants.APP_ID + "\\\""
                            + ",\\\"source\\\":\\\"" + Constants.APP_SOURCE + "\\\""
                            + ",\\\"version\\\":\\\"" + Constants.APP_VERSION + "\\\""
                            + ",\\\"pcode\\\":\\\"111111111111111\\\""
                            + ",\\\"digitalreceipt\\\":\\\"\\\""
                            + ",\\\"amount\\\":\\\"99000\\\""
                            + ",\\\"_status\\\":\\\"0\\\""
                            + ",\\\"verify\\\":\\\"0\\\""
                            + ",\\\"coupon\\\":\\\"\\\""
                            + ",\\\"description\\\":\\\"تک کاربره بدون محدودیت زمانی\\\""
                            + ",\\\"date_fa\\\":\\\"" + timeZone + "\\\""
                            + ",\\\"username\\\":\\\"\\\"}";

                    try {
                        TX_License.AppLicense appLicense = TX_License.returnedLicense(license);
                        callback.call(appLicense.getExpireDate(), appLicense.getLicense());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                dataCursor.moveToNext();
            }
            dataCursor.close();

        } else {

        }


    }


}
