package adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import DB.DBManager;
import OOP.Factor;
import OOP.FactorGroup;
import Other.C_Setting;
import Other.DialogUtils;
import Other.Modules;
import air.gamediesel.rasgir.AddFactorActivity1;
import air.gamediesel.rasgir.R;
import modules.PublicModules;

;

public class FactorListAdapter extends RecyclerView.Adapter<FactorListAdapter.ViewHolder> {

    private final Context context;
    private final LayoutInflater inflater;
    private List<FactorGroup> factorGroups;
    private DBManager db;
    public static int factorGroupId;

    public void refresh() {
        factorGroups = db.selectAllFactorGroups();
        notifyDataSetChanged();
    }


    public FactorListAdapter(Context context, List<FactorGroup> factorGroups) {
        this.context = context;
        this.factorGroups = factorGroups;
        inflater = LayoutInflater.from(context);
        db = new DBManager(context);
        this.factorGroups = db.selectAllFactorGroups();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.factor_list_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final FactorGroup obj = factorGroups.get(position);
        holder.itemNumber.setText(PublicModules.toPersianDigit(String.valueOf(position + 1)));
        if (position == factorGroups.size() - 1) {
            holder.seprator.setVisibility(View.GONE);
        } else {
            holder.seprator.setVisibility(View.VISIBLE);
        }
        if (obj.getExportDate()!=0){
            holder.printerIcon.setVisibility(View.VISIBLE);
        }else{
            holder.printerIcon.setVisibility(View.GONE);
        }
        //
        holder.numberOfChecks.setText(PublicModules.toPersianDigit(String.valueOf(obj.getFcdFactorCount())));
        //
        holder.currency.setText(C_Setting.getStringValue(C_Setting.APP_CURRENCY));
        //
        holder.groupName.setText(PublicModules.toPersianDigit(obj.getName()));
        //
        String price = String.valueOf(obj.getFcdTotalPrice());
        price = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(price));
        holder.sumOfCheckPrices.setText(PublicModules.toPersianDigit(price));
        //
    }

    @Override
    public int getItemCount() {
        return factorGroups.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView itemNumber;
        private TextView sumOfCheckPrices;
        private TextView groupName;
        private TextView numberOfChecks;
        private TextView currency;
        private LayoutInflater layoutInflater;
        private View seprator;
        private ImageView printerIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            //
            seprator = itemView.findViewById(R.id.factor_list_item_layout_line_separator);
            itemView.setBackgroundResource(R.drawable.check_list_item_clicked);
            itemNumber = itemView.findViewById(R.id.factor_list_item_layout_group_number);
            sumOfCheckPrices = itemView.findViewById(R.id.factor_list_item_layout_group_sum_result_text);
            groupName = itemView.findViewById(R.id.factor_list_item_layout_group_name);
            numberOfChecks = itemView.findViewById(R.id.factor_list_item_layout_check_count);
            currency = itemView.findViewById(R.id.factor_list_item_layout_currency_textview);
            printerIcon = itemView.findViewById(R.id.factor_list_printer_icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<FactorGroup> factorGroupArrayList = db.selectAllFactorGroups();
                    FactorGroup factorGroup = factorGroupArrayList.get(getAdapterPosition());
                    factorGroupId = factorGroup.getId();
                    ArrayList<Factor> factors = db.selectFactorByGroupId(factorGroup.getId());
                    Intent intent = new Intent(v.getContext(), AddFactorActivity1.class);
                    intent.putExtra("factorArray", factors);
                    intent.putExtra("hasNoPrice", factorGroup.isNoPrice());
                    intent.putExtra("groupName", factorGroup.getName());
                    context.startActivity(intent);
                }
            });
            //
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ArrayList<FactorGroup> factorGroupArrayList = db.selectAllFactorGroups();
                    final FactorGroup factorGroup = factorGroupArrayList.get(getAdapterPosition());
                    //
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_item_long_clicked_layout);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                    dialog.show();
                    //
                    final LinearLayout edit = dialog.findViewById(R.id.dialog_on_long_clicked_edit_text_view);
                    final LinearLayout remove = dialog.findViewById(R.id.dialog_on_long_clicked_remove_text_view);
                    final LinearLayout cancel = dialog.findViewById(R.id.dialog_on_long_clicked_cancel_text_view);
                    //
                    View.OnClickListener listener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (view.equals(edit)) {
                                final Dialog editDialog = new Dialog(context);
                                editDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                editDialog.setContentView(R.layout.dialog_edit_item_name_layout);
                                editDialog.setCanceledOnTouchOutside(false);
                                editDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                editDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                                editDialog.show();
                                //
                                TextView title = editDialog.findViewById(R.id.dialog_edit_item_name_title);
                                //
                                final EditText newName = editDialog.findViewById(R.id.dialog_edit_item_name_new_name);
                                newName.setText(PublicModules.toPersianDigit(factorGroup.getName()));
                                newName.setSelection(0, factorGroup.getName().length());
                                Modules.showSoftKeyboard(context);
                                newName.requestFocus();
                                //
                                final ImageView close = editDialog.findViewById(R.id.dialog_edit_item_name_close);
                                final TextView doneBtn = editDialog.findViewById(R.id.dialog_edit_item_name_done);
                                //
                                View.OnClickListener editViewListeners = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (view.equals(doneBtn)) {
                                            if (newName.getText().toString() != null && !newName.getText().toString().equals("")) {
                                                factorGroup.setName(newName.getText().toString());
                                                db.updateFactorGroup(factorGroup);
                                                refresh();
                                                notifyDataSetChanged();
                                                editDialog.dismiss();
                                                dialog.dismiss();
                                                Modules.hideSoftKeyboard(context);
                                                //
                                                Modules.customToast(context , "فاکتور" , "نام گروه فاکتور ها ویرایش شد" , true);
                                            }
                                        }
                                        if (view.equals(newName)) {
                                            newName.selectAll();
                                            Modules.hideSoftKeyboard(context);
                                        }

                                        if (view.equals(close)) {
                                            editDialog.dismiss();
                                        }
                                    }
                                };
                                //
                                title.setOnClickListener(editViewListeners);
                                newName.setOnClickListener(editViewListeners);
                                close.setOnClickListener(editViewListeners);
                                doneBtn.setOnClickListener(editViewListeners);
                            }
                            if (view.equals(remove)) {
                                final Dialog removeDialog = new Dialog(context);
                                removeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                removeDialog.setContentView(R.layout.dialog_remove_items_layout);
                                removeDialog.setCanceledOnTouchOutside(false);
                                removeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                removeDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                                removeDialog.show();
                                //
                                final LinearLayout ok = removeDialog.findViewById(R.id.done_linear);
                                final LinearLayout cancel = removeDialog.findViewById(R.id.cancel_linear);
                                //
                                View.OnClickListener removeListener = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (view.equals(ok)) {
                                            String message = "گروه " + factorGroup.getName().replace("گروه", "") + " حذف شد !";
                                            db.deleteFactorGroup(factorGroup);
                                            refresh();
                                            notifyDataSetChanged();
                                            removeDialog.dismiss();
                                            dialog.dismiss();
                                            //
                                            Modules.customToast(context , "گروه فاکتور" , message , true);
                                        }
                                        if (view.equals(cancel)) {
                                            removeDialog.dismiss();
                                        }
                                    }
                                };
                                //
                                ok.setOnClickListener(removeListener);
                                cancel.setOnClickListener(removeListener);
                            }
                            if (view.equals(cancel)) {
                                dialog.dismiss();
                            }
                        }
                    };
                    edit.setOnClickListener(listener);
                    remove.setOnClickListener(listener);
                    cancel.setOnClickListener(listener);
                    //
                    return true;
                }
            });
        }
    }
}
