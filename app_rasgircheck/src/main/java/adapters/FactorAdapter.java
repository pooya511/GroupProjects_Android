package adapters;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Parcel;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import OOP.Factor;
import Other.C_Setting;
import Other.DialogUtils;
import Other.KeyPadDialog;
import Other.Modules;
import air.gamediesel.rasgir.R;
import dialogFragment.DatePicker;
import me.grantland.widget.AutofitTextView;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

public class FactorAdapter extends RecyclerView.Adapter<FactorAdapter.ViewHolder> {

    private final Context context;
    private final LayoutInflater inflater;
    private List<Factor> factorList = Collections.emptyList();
    private TextView numberTxtView1, numberTxtView2;
    private AutofitTextView resultOfSelectedFactor;
    private PersianDate persianCalendar;
    private DialogFragment datePicker;
    int clickCount;
    boolean hasNoPrice;

    public FactorAdapter(Context context, List<Factor> factorList, boolean hasNoPrice) {
        this.context = context;
        this.factorList = factorList;
        this.hasNoPrice = hasNoPrice;
        inflater = LayoutInflater.from(context);
        //
        numberTxtView1 = ((Activity) context).findViewById(R.id.add_factor_page1_number1);
        numberTxtView2 = ((Activity) context).findViewById(R.id.add_factor_page1_number2);
        resultOfSelectedFactor = ((Activity) context).findViewById(R.id.add_factor_page1_resultOfChecks);
        //
        updateFooter(factorList);
    }

    public void updateFooter(List<Factor> factors) {
        long result = 0;
        int count = 0;
        for (int i = 0; i < factors.size(); i++) {
            double price = factors.get(i).getPrice();
            if (factors.get(i).isActive()) {
                if (price == 0)
                    result += 0;
                else
                    result += price;
                count++;
            }
        }
        resultOfSelectedFactor.setText(PublicModules.toPersianDigit(NumberFormat.getNumberInstance(Locale.US).format(Long.parseLong(String.valueOf(result))) + C_Setting.getStringValue(C_Setting.APP_CURRENCY)));
        numberTxtView1.setText(PublicModules.toPersianDigit(String.valueOf(count)));
        numberTxtView2.setText(PublicModules.toPersianDigit(String.valueOf(factors.size())));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (hasNoPrice) {
            View view = inflater.inflate(R.layout.factor_item_no_price_layout, parent, false);
            return new ViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.factor_item_layout, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Factor obj = factorList.get(position);
        persianCalendar = new PersianDate();
        clickCount = 0;
        if (position == 0) {
            holder.firstBorder.setVisibility(View.VISIBLE);
        } else {
            holder.firstBorder.setVisibility(View.GONE);
        }
        //
        holder.itemNumberField.setText(PublicModules.toPersianDigit(String.valueOf(position + 1)));
        //
        if (!hasNoPrice) {
            if (obj.getPrice() != 0) {
                String myPrice = String.valueOf(obj.getPrice());
                myPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(myPrice));
                holder.priceField.setText(PublicModules.toPersianDigit(myPrice));
            } else {
                holder.priceField.setText("");
            }
        }
        //
        if (obj.isActive()) {
            holder.selectBtn.setBackgroundResource(R.drawable.ic_check_square);
            //  Log.i("position", String.valueOf(position) + " is active");
            updateFooter(factorList);
        } else {
            holder.selectBtn.setBackgroundResource(R.drawable.ic_square_o);
            //  Log.i("position", String.valueOf(position) + " is not active");
            updateFooter(factorList);
        }
        //
        if (obj.getDate() != 0) {
            persianCalendar = new PersianDate(obj.getDate());
            holder.dateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
        } else {
            holder.dateField.setText("");
        }
        //
        if (obj.getSettlementDuration() == 0) {
            int defaultValue = C_Setting.getIntegerValue(C_Setting.MODAT_TASVIEH_PISHFARZ);
            obj.setSettlementDuration(defaultValue);
            holder.daysNumberField.setText(PublicModules.toPersianDigit(String.valueOf(obj.getSettlementDuration())));
        } else {
            holder.daysNumberField.setText(PublicModules.toPersianDigit(String.valueOf(obj.getSettlementDuration())));
        }
        //
        if (obj.getSettlementType() == 1) {
            holder.dayOrMonthTextField.setText("روز");
            holder.day.setBackgroundResource(R.drawable.factor_item_day_month_button_shape_selected);
            holder.day.setTextColor(Color.WHITE);
            holder.month.setBackgroundResource(R.drawable.factor_item_day_month_shape_no_selected);
            holder.month.setTextColor(Color.BLACK);
        } else if (obj.getSettlementType() == 2) {
            holder.dayOrMonthTextField.setText("ماه");
            holder.day.setBackgroundResource(R.drawable.factor_item_day_month_shape_no_selected);
            holder.day.setTextColor(Color.BLACK);
            holder.month.setBackgroundResource(R.drawable.factor_item_day_month_button_shape_selected);
            holder.month.setTextColor(Color.WHITE);
        }
        //
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(holder.selectBtn)) {
                    if (obj.isActive()) {
                        obj.setIsActive(false);
                        updateFooter(factorList);
                        holder.selectBtn.setBackgroundResource(R.drawable.ic_square_o);

                    } else {
                        obj.setIsActive(true);
                        updateFooter(factorList);
                        holder.selectBtn.setBackgroundResource(R.drawable.ic_check_square);
                    }
                }
                if (v.equals(holder.removeBtn)) {
                    if (factorList.size() > 1) {
                        factorList.remove(position);
                        updateFooter(factorList);
                        notifyItemRemoved(position);
                        notifyDataSetChanged();
                    } else {
                        obj.setPrice(0);
                        obj.setDate(0);
                        obj.setSettlementDuration(0);
                        notifyItemChanged(position);
                    }
                }
                if (v.equals(holder.month)) {
                    holder.dayOrMonthTextField.setText("ماه");
                    obj.setSettlementType(2);
                    notifyItemChanged(position);
                }
                if (v.equals(holder.day)) {
                    holder.dayOrMonthTextField.setText("روز");
                    obj.setSettlementType(1);
                    notifyItemChanged(position);
                }
                if (v.equals(holder.priceField)) {

                    new KeyPadDialog(context, PublicModules.toEnglishDigit(holder.priceField.getText().toString()), DialogUtils.keyPadTypes.PRICE ,new KeyPadDialog.KeyPadCallBack() {
                        @Override
                        public void call(String price) {
                            if (price != null && !price.equals("")) {
                                double myPrice = Double.parseDouble(price.replaceAll(",", ""));
                                obj.setPrice(myPrice);
                            }
                            holder.priceField.setText(PublicModules.toPersianDigit(price));
                            //
                            updateFooter(factorList);
                            //
                            if (C_Setting.getBooleanValue(C_Setting.NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH)) {
                                datePicker = DatePicker.newInstance(obj.getDate(), "تاریخ فاکتور را وارد کنید", new DatePicker.DatePickerCallBack() {
                                    @Override
                                    public void call(PersianDate persianCalendar) {
                                        holder.dateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
                                        obj.setDate(Modules.startOfDay(persianCalendar.getTime()));
                                        datePicker.dismiss();
                                    }

                                    @Override
                                    public int describeContents() {
                                        return 0;
                                    }

                                    @Override
                                    public void writeToParcel(Parcel dest, int flags) {

                                    }
                                });
                                datePicker.show(((Activity) context).getFragmentManager(), "");
                            }
                        }
                    }).show();
                }

                if (v.equals(holder.dateField)) {
                    datePicker = DatePicker.newInstance(obj.getDate(), "تاریخ فاکتور را وارد کنید", new DatePicker.DatePickerCallBack() {
                        @Override
                        public void call(PersianDate persianCalendar) {
                            holder.dateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
                            obj.setDate(Modules.startOfDay(persianCalendar.getTime()));
                            datePicker.dismiss();
                        }

                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }
                    });
                    datePicker.show(((Activity) context).getFragmentManager(), "");
                }
                if (v.equals(holder.daysNumberField)) {
                    int settelment;
                    if (obj.getSettlementDuration() == 0) {
                        settelment = C_Setting.getIntegerValue(C_Setting.MODAT_TASVIEH_PISHFARZ);
                    } else {
                        settelment = obj.getSettlementDuration();
                    }

                    new KeyPadDialog(context, PublicModules.toEnglishDigit(holder.dayOrMonthTextField.getText().toString()), DialogUtils.keyPadTypes.NORMAL_NUMBER, new KeyPadDialog.KeyPadCallBack() {
                        @Override
                        public void call(String price) {
                            obj.setSettlementDuration(Integer.parseInt(price.replaceAll(",", "")));
                            holder.daysNumberField.setText(PublicModules.toPersianDigit(String.valueOf(obj.getSettlementDuration())));
                        }
                    }).show();
//
//                    DialogUtils.keyPadDialog(context, settelment, DialogUtils.keyPadTypes.NORMAL_NUMBER, new Modules.KeyPadCallBack() {
//                        @Override
//                        public void call(String number) {
//                            obj.setSettlementDuration(Integer.parseInt(number.replaceAll(",", "")));
//                            holder.daysNumberField.setText(String.valueOf(obj.getSettlementDuration()));
//                        }
//                    });
                }
            }
        };
        holder.removeBtn.setOnClickListener(listener);
        holder.selectBtn.setOnClickListener(listener);
        holder.month.setOnClickListener(listener);
        holder.day.setOnClickListener(listener);
        if (!hasNoPrice) {
            holder.priceField.setOnClickListener(listener);
        }
        holder.dateField.setOnClickListener(listener);
        holder.daysNumberField.setOnClickListener(listener);
    }


    @Override
    public int getItemCount() {
        return factorList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView itemNumberField;
        private TextView priceField;
        private TextView dateField;
        private TextView daysNumberField;
        private TextView dayOrMonthTextField;
        private TextView day;
        private TextView month;
        private ImageButton selectBtn;
        private ImageButton removeBtn;
        private View firstBorder;

        public ViewHolder(View itemView) {
            super(itemView);
            if (hasNoPrice) {
                firstBorder = itemView.findViewById(R.id.factor_item_layout_first_border);
                itemNumberField = itemView.findViewById(R.id.add_factor_page1_item_number);
                dateField = itemView.findViewById(R.id.add_factor_page1_date_field);
                daysNumberField = itemView.findViewById(R.id.add_factor_page1_day_number);
                dayOrMonthTextField = itemView.findViewById(R.id.add_factor_page1_day_or_month_text);
                day = itemView.findViewById(R.id.add_factor_page1_day_text_view);
                month = itemView.findViewById(R.id.add_factor_page1_month_text_view);
                selectBtn = itemView.findViewById(R.id.add_factor_page1_select_image_btn);
                removeBtn = itemView.findViewById(R.id.add_factor_page1_remove_image_btn);
            } else {
                firstBorder = itemView.findViewById(R.id.factor_item_layout_first_border);
                itemNumberField = itemView.findViewById(R.id.add_factor_page1_item_number);
                priceField = itemView.findViewById(R.id.add_factor_page1_price_field);
                dateField = itemView.findViewById(R.id.add_factor_page1_date_field);
                daysNumberField = itemView.findViewById(R.id.add_factor_page1_day_number);
                dayOrMonthTextField = itemView.findViewById(R.id.add_factor_page1_day_or_month_text);
                day = itemView.findViewById(R.id.add_factor_page1_day_text_view);
                month = itemView.findViewById(R.id.add_factor_page1_month_text_view);
                selectBtn = itemView.findViewById(R.id.add_factor_page1_select_image_btn);
                removeBtn = itemView.findViewById(R.id.add_factor_page1_remove_image_btn);
            }

        }
    }
}
