package adapters;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import android.os.Parcel;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
//import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerController;
//import com.mohamadamin.persianmaterialdatetimepicker.date.MonthAdapter;
//import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import OOP.Check;
import Other.DialogUtils;
import Other.KeyPadDialog;
import Other.Modules;
import Other.C_Setting;
import air.gamediesel.rasgir.R;
import dialogFragment.DatePicker;
import me.grantland.widget.AutofitTextView;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

public class CheckAdapter extends RecyclerView.Adapter<CheckAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    public List<Check> checkList = Collections.emptyList();
    private TextView number1TxtView, number2TxtView;
    private AutofitTextView resultOfSelectedCheck;
    private PersianDate persianCalender;
    private DialogFragment datePicker;
    boolean checkGroupIsAdvanced;


    public CheckAdapter() {
    }

    public CheckAdapter(Context context, List<Check> checkList, boolean checkGroupIsAdvanced) {
        this.context = context;
        this.checkList = checkList;
        this.checkGroupIsAdvanced = checkGroupIsAdvanced;
        inflater = LayoutInflater.from(context);
        //
        number1TxtView = ((Activity) context).findViewById(R.id.number1);
        number2TxtView = ((Activity) context).findViewById(R.id.number2);
        resultOfSelectedCheck = ((Activity) context).findViewById(R.id.add_check_page1_resultOfChecks);
        //
        updateFooter(checkList);

    }

    public void updateFooter(List<Check> checks) {
        long result = 0;
        int count = 0;
        for (int i = 0; i < checks.size(); i++) {
            double price = checks.get(i).getPrice();
            if (checks.get(i).isCheckIsActive()) {
                if (price == 0)
                    result += 0;
                else
                    result += price;
                count++;
            }
        }
        resultOfSelectedCheck.setText(PublicModules.toPersianDigit(NumberFormat.getNumberInstance(Locale.US).format(Math.round(Double.parseDouble(String.valueOf(result)))) + C_Setting.getStringValue(C_Setting.APP_CURRENCY)));

        number1TxtView.setText(PublicModules.toPersianDigit(String.valueOf(count)));
        number2TxtView.setText(PublicModules.toPersianDigit(String.valueOf(checks.size())));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /**
         * set the check layout Items
         */
        if (checkGroupIsAdvanced) {
            View view = inflater.inflate(R.layout.check_item_advanced_layout, parent, false);
            return new ViewHolder(view);
        } else {
            View view = inflater.inflate(R.layout.check_item_layout, parent, false);
            return new ViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Check obj = checkList.get(position);
        persianCalender = new PersianDate();
        if (position == 0) {
            holder.firstBorder.setVisibility(View.VISIBLE);
        } else {
            holder.firstBorder.setVisibility(View.GONE);
        }
        //
        /**
         * set the field
         */
        holder.idField.setText(PublicModules.toPersianDigit(String.valueOf(position + 1)));
        //
        if (obj.getPrice() != 0) {
            String myPrice = String.valueOf(obj.getPrice());
            myPrice = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(myPrice));
            holder.priceField.setText(PublicModules.toPersianDigit(myPrice));
        } else {
            holder.priceField.setText("");
        }
        //
        if (obj.getDate() != 0) {
            persianCalender = new PersianDate(obj.getDate());
            holder.dateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalender)));
        } else {
            holder.dateField.setText("");
        }
        //
        if (obj.getOffsetDate() != 0 && checkGroupIsAdvanced) {
            persianCalender = new PersianDate(obj.getOffsetDate());
            holder.originDateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalender)));
        }
        //
        if (obj.isCheckIsActive()) {
            holder.selectBtn.setBackgroundResource(R.drawable.ic_check_square);
        } else {
            holder.selectBtn.setBackgroundResource(R.drawable.ic_square_o);
        }
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(holder.removeBtn)) {
                    if (checkList.size() > 1) {
                        checkList.remove(obj);
                        updateFooter(checkList);
                        notifyItemRemoved(position);
                        notifyDataSetChanged();
                    } else {
                        obj.setPrice(0);
                        obj.setDate(0);
                        notifyItemChanged(position);
                    }
                }
                if (v.equals(holder.selectBtn)) {
                    if (obj.isCheckIsActive()) {
                        obj.setCheckIsActive(false);
                        updateFooter(checkList);
                        holder.selectBtn.setBackgroundResource(R.drawable.ic_square_o);
                        notifyDataSetChanged();
                    } else {
                        obj.setCheckIsActive(true);
                        updateFooter(checkList);
                        holder.selectBtn.setBackgroundResource(R.drawable.ic_check_square);
                        notifyDataSetChanged();
                    }
                }

            }
        };
        holder.removeBtn.setOnClickListener(listener);
        holder.selectBtn.setOnClickListener(listener);
        /**
         * price field handler
         */
        holder.priceField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new KeyPadDialog(context, PublicModules.toEnglishDigit(holder.priceField.getText().toString()), DialogUtils.keyPadTypes.PRICE, new KeyPadDialog.KeyPadCallBack() {
                    @Override
                    public void call(String price) {
                        if (price != null && !price.equals("")) {
                            double myPrice = Double.parseDouble(price.replaceAll(",", ""));
                            obj.setPrice(myPrice);
                        }
                        holder.priceField.setText(PublicModules.toPersianDigit(price));
                        //
                        updateFooter(checkList);
                        //
                        if (C_Setting.getBooleanValue(C_Setting.NAMAYESH_KHODKAR_MABLAGH_VA_TARIKH)) {
                            datePicker = DatePicker.newInstance(obj.getDate(), "تاریخ چک را وارد کنید", new DatePicker.DatePickerCallBack() {
                                @Override
                                public void call(PersianDate persianCalendar) {
                                    holder.dateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
                                    obj.setDate(Modules.startOfDay(persianCalendar.getTime()));
                                    datePicker.dismiss();
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            });
                            datePicker.show(((Activity) context).getFragmentManager(), "");
                        }
                    }
                }).show();
            }
        });
        /**
         * date field handler
         */
        holder.dateField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker = DatePicker.newInstance(obj.getDate(), "تاریخ چک را وارد کنید", new DatePicker.DatePickerCallBack() {
                    @Override
                    public void call(PersianDate persianCalendar) {
                        holder.dateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
                        obj.setDate(Modules.startOfDay(persianCalendar.getTime()));
                        notifyDataSetChanged();
                        datePicker.dismiss();
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {

                    }
                });
                datePicker.show(((Activity) context).getFragmentManager(), "");
            }
        });
        //
        if (checkGroupIsAdvanced) {
            holder.originDateField.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    datePicker = DatePicker.newInstance(obj.getOffsetDate(), "تاریخ چک را وارد کنید", new DatePicker.DatePickerCallBack() {
                        @Override
                        public void call(PersianDate persianCalendar) {
                            holder.originDateField.setText(PublicModules.toPersianDigit(Modules.getPersianShortDate(persianCalendar)));
                            obj.setOffsetDate(Modules.startOfDay(persianCalendar.getTime()));
                            notifyDataSetChanged();
                            datePicker.dismiss();
                        }

                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }
                    });
                    datePicker.show(((Activity) context).getFragmentManager(), "");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return checkList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView priceField;
        private TextView dateField;
        private TextView originDateField;
        private TextView idField;
        private ImageButton selectBtn;
        private ImageButton removeBtn;
        private View firstBorder;

        public ViewHolder(View itemView) {
            super(itemView);
            if (checkGroupIsAdvanced) {
                firstBorder = itemView.findViewById(R.id.check_item_layout_first_border);
                idField = itemView.findViewById(R.id.advanced_check_item_number);
                priceField = itemView.findViewById(R.id.advanced_check_price_field);
                dateField = itemView.findViewById(R.id.advanced_check_date_field);
                originDateField = itemView.findViewById(R.id.advanced_check_origin_date_field);
                selectBtn = itemView.findViewById(R.id.advanced_check_select_image_btn);
                removeBtn = itemView.findViewById(R.id.advanced_check_remove_image_btn);
            } else {
                firstBorder = itemView.findViewById(R.id.check_item_layout_first_border);
                idField = itemView.findViewById(R.id.item_number);
                priceField = itemView.findViewById(R.id.price_field);
                dateField = itemView.findViewById(R.id.date_field);
                selectBtn = itemView.findViewById(R.id.select_image_btn);
                removeBtn = itemView.findViewById(R.id.remove_image_btn);
            }
        }

    }

}