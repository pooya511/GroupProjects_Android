package adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import DB.DBManager;
import OOP.Check;
import OOP.CheckGroup;
import OOP.CheckRasResult;
import Other.C_Setting;
import Other.DialogUtils;
import Other.Modules;
import air.gamediesel.rasgir.AddCheckActivity1;
import air.gamediesel.rasgir.AddCheckActivityFinal;
import air.gamediesel.rasgir.R;
import me.grantland.widget.AutofitTextView;
import modules.PublicModules;
import saman.zamani.persiandate.PersianDate;

public class CheckListAdapter extends RecyclerView.Adapter<CheckListAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<CheckGroup> checkGroupArrayList;
    private DBManager db;
    public static int checkGroupId;

    public CheckListAdapter() {
    }

    public void refresh() {
        checkGroupArrayList = db.selectAllCheckGroups();
        notifyDataSetChanged();
    }

    public CheckListAdapter(Context context) {
        this.context = context;

        db = new DBManager(context);
        inflater = LayoutInflater.from(context);
        C_Setting.init(context);
        this.checkGroupArrayList = db.selectAllCheckGroups();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.check_list_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CheckGroup checkGroup = checkGroupArrayList.get(position);

        final ArrayList<Check> checks = db.selectCheckByGroupId(checkGroup.getId());

        CheckRasResult checkRasResult = AddCheckActivityFinal.calculateRasCheck(checks);

        PersianDate persianDate = new PersianDate(checkRasResult.headDate);
        holder.rasDate.setText(PublicModules.toPersianDigit("راس: " + Modules.getPersianShortDate(persianDate)));

        if (position == checkGroupArrayList.size() - 1) {
            holder.separator.setVisibility(View.GONE);
        } else {
            holder.separator.setVisibility(View.VISIBLE);
        }
        if (checkGroup.getCheckExportDate() != 0) {
            holder.printerImage.setVisibility(View.VISIBLE);
        }
        holder.itemNumber.setText(PublicModules.toPersianDigit(String.valueOf(position + 1)));
        holder.groupName.setText(PublicModules.toPersianDigit(checkGroup.getName()));
        //
        String sum = String.valueOf(checkGroup.getChkTotalPrice());
        sum = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(sum));
        holder.sumOfCheckPrices.setText(PublicModules.toPersianDigit(sum.substring(0, sum.length()) + " " + C_Setting.getStringValue(C_Setting.APP_CURRENCY)));
        //
        holder.numberOfChecks.setText(PublicModules.toPersianDigit(String.valueOf(checkGroup.getChkCheckCount())));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkGroupId = checkGroup.getId();
                Intent intent = new Intent(v.getContext(), AddCheckActivity1.class);
                intent.putExtra("checkArray", checks);
                intent.putExtra("checkGroupIsAdvanced", checkGroup.isCheckIsAdvanced());
                intent.putExtra("groupName", checkGroup.getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return checkGroupArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView itemNumber;
        private AutofitTextView sumOfCheckPrices, rasDate;
        private TextView groupName;
        private TextView numberOfChecks;
        private LayoutInflater layoutInflater;
        private View separator;
        private ImageView printerImage;

        public ViewHolder(View itemView) {
            super(itemView);
            //
            separator = itemView.findViewById(R.id.check_list_item_layout_line_separator);
            itemView.setBackgroundResource(R.drawable.check_list_item_clicked);
            itemNumber = itemView.findViewById(R.id.check_list_item_layout_group_number);
            sumOfCheckPrices = itemView.findViewById(R.id.check_list_item_layout_group_sum_result_text);
            rasDate = itemView.findViewById(R.id.check_list_item_layout_group_ras_date_text);
            groupName = itemView.findViewById(R.id.check_list_item_layout_group_name);
            numberOfChecks = itemView.findViewById(R.id.check_list_item_layout_check_count);
            printerImage = itemView.findViewById(R.id.check_list_printer_icon);
            //
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ArrayList<CheckGroup> checkGroupArrayList = db.selectAllCheckGroups();
                    final CheckGroup checkGroup = checkGroupArrayList.get(getAdapterPosition());
                    //
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_item_long_clicked_layout);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                    dialog.show();
                    //
                    final LinearLayout edit = dialog.findViewById(R.id.dialog_on_long_clicked_edit_text_view);
                    final LinearLayout remove = dialog.findViewById(R.id.dialog_on_long_clicked_remove_text_view);
                    final LinearLayout cancel = dialog.findViewById(R.id.dialog_on_long_clicked_cancel_text_view);
                    //
                    View.OnClickListener listener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (view.equals(edit)) {
                                final Dialog editDialog = new Dialog(context);
                                editDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                editDialog.setContentView(R.layout.dialog_edit_item_name_layout);
                                editDialog.setCanceledOnTouchOutside(false);
                                editDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                editDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                                editDialog.show();
                                //
                                TextView title = editDialog.findViewById(R.id.dialog_edit_item_name_title);
                                //
                                final EditText newName = editDialog.findViewById(R.id.dialog_edit_item_name_new_name);
                                newName.setText(PublicModules.toPersianDigit(checkGroup.getName()));
                                newName.setSelection(0, checkGroup.getName().length());
                                Modules.showSoftKeyboard(context);
                                newName.requestFocus();
                                //
                                final ImageView close = editDialog.findViewById(R.id.dialog_edit_item_name_close);
                                final TextView doneBtn = editDialog.findViewById(R.id.dialog_edit_item_name_done);
                                //
                                View.OnClickListener editViewListeners = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (view.equals(doneBtn)) {
                                            if (newName.getText().toString() != null && !newName.getText().toString().equals("")) {
                                                checkGroup.setName(newName.getText().toString());
                                                db.updateCheckGroup(checkGroup);
                                                refresh();
                                                notifyDataSetChanged();
                                                editDialog.dismiss();
                                                dialog.dismiss();
                                                Modules.hideSoftKeyboard(context);
                                                //
                                                Modules.customToast(context, "گروه چک", "نام گروه چک ها ویرایش شد", true);
                                            }
                                        }
                                        if (view.equals(newName)) {
                                            newName.selectAll();
                                        }

                                        if (view.equals(close)) {
                                            editDialog.dismiss();
                                            Modules.hideSoftKeyboard(context);
                                        }
                                    }
                                };
                                //
                                title.setOnClickListener(editViewListeners);
                                newName.setOnClickListener(editViewListeners);
                                close.setOnClickListener(editViewListeners);
                                doneBtn.setOnClickListener(editViewListeners);
                            }
                            if (view.equals(remove)) {
                                final Dialog removeDialog = new Dialog(context);
                                removeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                removeDialog.setContentView(R.layout.dialog_remove_items_layout);
                                removeDialog.setCanceledOnTouchOutside(false);
                                removeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                removeDialog.getWindow().setWindowAnimations(R.style.normalSlideUpDialogAnimation);
                                removeDialog.show();
                                //
                                final LinearLayout ok = removeDialog.findViewById(R.id.done_linear);
                                final LinearLayout cancel = removeDialog.findViewById(R.id.cancel_linear);
                                //
                                View.OnClickListener removeListener = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (view.equals(ok)) {
                                            String message = "گروه " + checkGroup.getName().replace("گروه", "") + " حذف شد !";
                                            db.deleteCheckGroup(checkGroup);
                                            refresh();
                                            notifyDataSetChanged();
                                            removeDialog.dismiss();
                                            dialog.dismiss();
                                            //
                                            Modules.customToast(context, "گروه چک", message, true);
                                        }
                                        if (view.equals(cancel)) {
                                            removeDialog.dismiss();
                                        }
                                    }
                                };
                                //
                                ok.setOnClickListener(removeListener);
                                cancel.setOnClickListener(removeListener);
                            }
                            if (view.equals(cancel)) {
                                dialog.dismiss();
                            }
                        }
                    };
                    edit.setOnClickListener(listener);
                    remove.setOnClickListener(listener);
                    cancel.setOnClickListener(listener);
                    //
                    return true;
                }
            });
        }
    }
}
