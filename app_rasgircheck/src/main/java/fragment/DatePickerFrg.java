package fragment;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aigestudio.wheelpicker.WheelPicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Other.Modules;
import air.gamediesel.rasgir.R;
import saman.zamani.persiandate.PersianDate;

/**
 * Created by Parsa on 2018-02-18.
 */

public class DatePickerFrg extends Fragment {

    private View fragmentLayout;
    private static WheelPicker day, month, year;

    private static int resDay, resMonth, resYear;
    private static List<String> monthDates, esfandDays, firstSixMonthDayDates, secondSixMonthDayDates, yearDates, dayDates;

    public static DatePickerFrg newInstance(long date) {

        dayDates = new ArrayList<>();

        yearDates = new ArrayList<>();
        esfandDays = new ArrayList<>();
        firstSixMonthDayDates = new ArrayList<>();
        secondSixMonthDayDates = new ArrayList<>();

        for (int i = 1; i < 32; i++) {
            firstSixMonthDayDates.add(Modules.toPersianDigit(String.valueOf(i)));
            if (secondSixMonthDayDates.size() != 30)
                secondSixMonthDayDates.add(Modules.toPersianDigit(String.valueOf(i)));
            if (esfandDays.size() != 29)
                esfandDays.add(Modules.toPersianDigit(String.valueOf(i)));
        }

        //Default
        dayDates = firstSixMonthDayDates;

        for (int i = 1380; i < 1430; i++) {
            yearDates.add(Modules.toPersianDigit(String.valueOf(i)));
        }
        monthDates = new ArrayList<>(Arrays.asList(
                "فروردین",
                "اردیبهشت",
                "خرداد",
                "تیر",
                "مرداد",
                "شهریور",
                "مهر",
                "آبان",
                "آذر",
                "دی",
                "بهمن",
                "اسفند"
        ));

        Bundle args = new Bundle();
        args.putLong("Date", date);
        DatePickerFrg fragment = new DatePickerFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        long currentDate = getArguments().getLong("Date");

        fragmentLayout = inflater.inflate(R.layout.frg_select_by_selecting, container, false);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yekan.ttf");

        day = fragmentLayout.findViewById(R.id.frg_select_by_selecting_day);
        day.setTypeface(typeface);
        month = fragmentLayout.findViewById(R.id.frg_select_by_selecting_month);
        month.setTypeface(typeface);
        year = fragmentLayout.findViewById(R.id.frg_select_by_selecting_year);
        year.setTypeface(typeface);

        day.setData(dayDates);
        month.setData(monthDates);
        year.setData(yearDates);

        setDate(currentDate);

        if (month.getCurrentItemPosition() <= 5) {
            dayDates = firstSixMonthDayDates;
            day.setData(dayDates);
        } else if (month.getCurrentItemPosition() >= 6 && month.getCurrentItemPosition() < 11) {
            dayDates = secondSixMonthDayDates;
            day.setData(dayDates);
        } else if (month.getCurrentItemPosition() == 11) {
            dayDates = esfandDays;
            day.setData(dayDates);
        }

        month.setOnWheelChangeListener(new WheelPicker.OnWheelChangeListener() {
            @Override
            public void onWheelScrolled(int offset) {

            }

            @Override
            public void onWheelSelected(int position) {

                if (position <= 5) {
                    if (day.getCurrentItemPosition() + 2 == firstSixMonthDayDates.size())
                        if (dayDates.size() == 30) {
                            dayDates = firstSixMonthDayDates;
                        }
                    day.setData(dayDates);
                } else if (position >= 6 && position < 11) {
                    dayDates = secondSixMonthDayDates;
                    day.setData(dayDates);
                } else if (position == 11) {
                    dayDates = esfandDays;
                    day.setData(dayDates);
                }

            }

            @Override
            public void onWheelScrollStateChanged(int state) {

            }
        });

        day.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                resDay = Integer.parseInt(Modules.persianDigitToEnglishDigit(dayDates.get(position)));
            }
        });

        month.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {

                switch (String.valueOf(data)) {

                    case "فروردین":
                        resMonth = 1;
                        break;

                    case "اردیبهشت":
                        resMonth = 2;
                        break;

                    case "خرداد":
                        resMonth = 3;
                        break;

                    case "تیر":
                        resMonth = 4;
                        break;

                    case "مرداد":
                        resMonth = 5;
                        break;

                    case "شهریور":
                        resMonth = 6;
                        break;

                    case "مهر":
                        resMonth = 7;
                        break;

                    case "آبان":
                        resMonth = 8;
                        break;

                    case "آذر":
                        resMonth = 9;
                        break;

                    case "دی":
                        resMonth = 10;
                        break;

                    case "بهمن":
                        resMonth = 11;
                        break;

                    case "اسفند":
                        resMonth = 12;
                        break;

                }
            }
        });

        year.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                resYear = Integer.parseInt(Modules.persianDigitToEnglishDigit(yearDates.get(position)));
            }
        });

        return fragmentLayout;
    }

    public static void setDate(long targetDate) {

        PersianDate persianCalendar = new PersianDate(targetDate);

        resDay = persianCalendar.getShDay();
        resMonth = persianCalendar.getShMonth(); // persianCalendar e oskole . tof tu kale mohammad amin ba in librarish
        resYear = persianCalendar.getShYear();     // negaran nabash dadash avazesh kardam

        Log.i("res setDate()", String.valueOf(resDay) + " " + String.valueOf(resMonth) + " " + String.valueOf(resYear));

        for (int i = 0; i < dayDates.size(); i++) {

            if (Modules.persianDigitToEnglishDigit(dayDates.get(i)).equals(String.valueOf(resDay))) {
                day.setSelectedItemPosition(i);
                resDay = Integer.parseInt(Modules.persianDigitToEnglishDigit(dayDates.get(i)));
            }

        }

        for (int i = 0; i < monthDates.size(); i++) {

            if (monthDates.get(i).equals(persianCalendar.monthName())) {
                month.setSelectedItemPosition(i);
                resMonth = i + 1;
            }

        }

        for (int i = 0; i < yearDates.size(); i++) {

            if (Modules.persianDigitToEnglishDigit(yearDates.get(i)).equals(String.valueOf(resYear))) {
                year.setSelectedItemPosition(i);
                resYear = Integer.parseInt(Modules.persianDigitToEnglishDigit(yearDates.get(i)));
            }

        }

    }

    public static PersianDate getDate() {

        resDay = day.getCurrentItemPosition() + 1;

        PersianDate persianCalendar = new PersianDate();
        persianCalendar.setShDay(resDay);
        persianCalendar.setShMonth(resMonth);
        persianCalendar.setShYear(resYear);

        Log.i("res getDate()", String.valueOf(resDay) + " " + String.valueOf(resMonth) + " " + String.valueOf(resYear));

        return persianCalendar;
    }

}
