package OOP;

import java.io.Serializable;

public class Factor implements Serializable {

    public static final String FACTOR_TABLE = "T_Factors_Details" ;
    public static final String FACTOR_ID_COLUMN = "ID" ;
    public static final String FACTOR_GROUP_ID_COLUMN = "fctID" ;
    public static final String FACTOR_PRICE_COLUMN = "fcdPrice" ;
    public static final String FACTOR_DATE_COLUMN = "fcdDate" ;
    public static final String FACTOR_SETTLEMENT_DURATION_COLUMN = "fcdSettlementDuration" ;
    public static final String FACTOR_SETTLEMENT_TYPE_COLUMN = "fcdSettlementType" ;
    public static final String FACTOR_IS_ACTIVE_COLUMN = "fcdIsActive" ;

    private int id;
    private int groupId;
    private double price;
    private long date;
    private int settlementDuration;
    private int settlementType;
    private boolean isActive;

    public Factor() {
    }

    public Factor(boolean isActive,int settlementType) {
        this.isActive = isActive;
        this.settlementType = settlementType ;
    }

    public Factor(boolean isActive) {
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getSettlementDuration() {
        return settlementDuration;
    }

    public void setSettlementDuration(int settlementDuration) {
        this.settlementDuration = settlementDuration;
    }

    public int getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(int settlementType) {
        this.settlementType = settlementType;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
