package OOP;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FactorGroup {

    public static final String FACTOR_GROUP_TABLE = "T_Factors" ;
    public static final String FACTOR_GROUP_ID_COLUMN = "ID" ;
    public static final String FACTOR_GROUP_NAME_COLUMN = "fctName" ;
    public static final String FACTOR_GROUP_NO_PRICE_COLUMN = "fctNoPrice" ;
    public static final String FACTOR_GROUP_DEAD_LINE_COLUMN = "fctDeadLine" ;
    public static final String FACTOR_GROUP_EXPORT_DATE_COLUMN = "fctExportDate" ;

    private int id;
    private String name;
    private long deadLineDate;
    private long exportDate;
    private boolean noPrice;
    private ArrayList<Factor> factorsGroup;
    private int fctId;
    private double fcdTotalPrice;
    private int fcdFactorCount;

    public FactorGroup() {
    }

    public FactorGroup( String name,long deadLineDate,boolean hasNoPrice) {
        this.deadLineDate = deadLineDate;
        this.name= name;
        this.noPrice = hasNoPrice ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDeadLineDate() {
        return deadLineDate;
    }

    public void setDeadLineDate(long deadLineDate) {
        this.deadLineDate = deadLineDate;
    }

    public long getExportDate() {
        return exportDate;
    }

    public void setExportDate(long exportDate) {
        this.exportDate = exportDate;
    }

    public boolean isNoPrice() {
        return noPrice;
    }

    public void setNoPrice(boolean noPrice) {
        this.noPrice = noPrice;
    }

    public int getFctId() {
        return fctId;
    }

    public void setFctId(int fcdId) {
        this.fctId = fcdId;
    }

    public double getFcdTotalPrice() {
        return fcdTotalPrice;
    }

    public void setFcdTotalPrice(double fcdTotalPrice) {
        this.fcdTotalPrice = fcdTotalPrice;
    }

    public int getFcdFactorCount() {
        return fcdFactorCount;
    }

    public void setFcdFactorCount(int fcdCheckCount) {
        this.fcdFactorCount = fcdCheckCount;
    }
}
