package OOP;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class CheckGroup{

    public static final String CHECK_GROUP_TABLE = "T_Checks" ;
    public static final String CHECK_GROUP_ID = "ID" ;
    public static final String CHECK_GROUP_NAME = "chkName" ;
    public static final String CHECK_GROUP_OFFSET_COLUMN = "chkGroupOffset" ;
    public static final String CHECK_GROUP_ADVANCED_COLUMN = "chkIsAdvanced" ;
    public static final String CHECK_GROUP_EXPORT_DATE = "chkExportDate" ;
    public static final String CHECK_GROUP_INTEREST_PERCENT = "chkInterestPercent" ;


    private int id;
    private ArrayList<Check> checksGroup;
    private String name;
    private long groupOffset;
    private boolean checkIsAdvanced;
    private long checkExportDate;
    private double checkInterestPercent;
    private int chkId;
    private double chkTotalPrice;
    private int chkCheckCount;

    public CheckGroup() {
    }

    public CheckGroup(String name, long groupOffset, boolean checkIsAdvanced, double checkInterestPercent) {
        this.name = name;
        this.groupOffset = groupOffset;
        this.checkIsAdvanced = checkIsAdvanced;
        this.checkInterestPercent = checkInterestPercent;
    }

    public ArrayList<Check> getChecksGroup() {
        return checksGroup;
    }

    public void setChecksGroup(ArrayList<Check> checksGroup) {
        this.checksGroup = checksGroup;
    }

    public int getChkId() {
        return chkId;
    }

    public void setChkId(int chkId) {
        this.chkId = chkId;
    }

    public double getChkTotalPrice() {
        return chkTotalPrice;
    }

    public void setChkTotalPrice(double chkTotalPrice) {
        this.chkTotalPrice = chkTotalPrice;
    }

    public int getChkCheckCount() {
        return chkCheckCount;
    }

    public void setChkCheckCount(int chkCheckCount) {
        this.chkCheckCount = chkCheckCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getGroupOffset() {
        return groupOffset;
    }

    public void setGroupOffset(long groupOffset) {
        this.groupOffset = groupOffset;
    }

    public boolean isCheckIsAdvanced() {
        return checkIsAdvanced;
    }

    public void setCheckIsAdvanced(boolean checkIsAdvanced) {
        this.checkIsAdvanced = checkIsAdvanced;
    }

    public long getCheckExportDate() {
        return checkExportDate;
    }

    public void setCheckExportDate(long checkExportDate) {
        this.checkExportDate = checkExportDate;
    }

    public double getCheckInterestPercent() {
        return checkInterestPercent;
    }

    public void setCheckInterestPercent(double checkInterestPercent) {
        this.checkInterestPercent = checkInterestPercent;
    }
}
