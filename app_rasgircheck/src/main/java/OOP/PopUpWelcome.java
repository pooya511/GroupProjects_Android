package OOP;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Parcel;

import com.taxiapps.txpayment.dialogFragment.Payment;

import java.util.ArrayList;

import Other.C_Setting;
import Other.Constants;
import Other.DialogUtils;
import air.gamediesel.rasgir.MainActivity;
import air.gamediesel.rasgir.MyApplication;
import air.gamediesel.rasgir.R;

public class PopUpWelcome {

    public ArrayList<Integer> identifiers;
    public Dialog dialog;
    public DialogFragment dialogFragment;

    public static int welcome_mgr_cycle = 50;
    private static DialogUtils dialogUtils = new DialogUtils();

    public PopUpWelcome(ArrayList<Integer> identifiers, Dialog dialog) {
        this.identifiers = identifiers;
        this.dialog = dialog;
    }

    public PopUpWelcome(ArrayList<Integer> identifiers, DialogFragment dialogFragment) {
        this.identifiers = identifiers;
        this.dialogFragment = dialogFragment;
    }

    public static ArrayList<PopUpWelcome> getPopUpArray(Context context) {
        ArrayList<PopUpWelcome> popUpWelcomes = new ArrayList<>();
        ArrayList<Integer> purchasePopUp = new ArrayList<Integer>() {{
            add(4);
            add(12);
            add(18);
            add(28);
            add(38);
            add(48);
        }};
        ArrayList<Integer> ratePopUp = new ArrayList<Integer>() {{
            add(5);
            add(20);
            add(25);
            add(35);
            add(40);
            add(47);
        }};
        ArrayList<Integer> telegramPopUp = new ArrayList<Integer>() {{
            add(3);
            add(26);
            add(42);
            add(49);
        }};
        if (!MyApplication.isLicenseValid()) { // چک میکنیم لایسنس نداشته باشه

            final String userName = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER).equals("") ? "" : C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
            DialogFragment payment = Payment.newInstance(R.drawable.rasgir_icon,
                    Constants.APP_ID,
                    Constants.APP_SOURCE,
                    Constants.APP_VERSION,
                    "راس گیر چک",
                    MyApplication.currentBase64,
                    "مزایای نسخه کامل",
                    "- ورود بیش از ۴ عدد چک در راس گیری \n - ورود بیش از ۴ عدد فاکتور",
                    userName,
                    Constants.DEVICE_ID,
                    Constants.SERVER_URL,
                    Constants.GATEWAY_URL,
                    new com.taxiapps.txpayment.payment.TX_License.SetLicenseCallback() {
                        @Override
                        public void call(long expireDate, String license) {
                            C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE, expireDate);
                            C_Setting.setStringValue(C_Setting.LICENSE_DATA, license);

                            MainActivity.setUIPremium();
                        }

                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }
                    },
                    new Payment.UserNameCallBack() {
                        @Override
                        public void getUserName(String username) {
                            C_Setting.setStringValue(C_Setting.TX_USER_NUMBER, username);
                        }

                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }
                    }
            );
            popUpWelcomes.add(new PopUpWelcome(purchasePopUp, payment));
        }
        if (C_Setting.getStringValue(C_Setting.USER_RATE) == null || C_Setting.getStringValue(C_Setting.USER_RATE).equals("")) { // چک میکنیم قبلا امتیاز نداده باشه
            popUpWelcomes.add(new PopUpWelcome(ratePopUp, dialogUtils.rateUsDialog(context)));
        }
        popUpWelcomes.add(new PopUpWelcome(telegramPopUp, dialogUtils.telegram(context)));
        return popUpWelcomes;
    }

}
