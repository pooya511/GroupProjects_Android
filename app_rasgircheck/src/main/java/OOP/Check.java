package OOP;

import java.io.Serializable;
import java.util.Date;

public class Check implements Serializable{

    public static final String CHECK_TABLE = "T_Checks_Details";
    public static final String CHECK_PRICE_COLUMN = "chdPrice";
    public static final String CHECK_DATE_COLUMN = "chdDate";
    public static final String CHECK_OFFSET_DATE_COLUMN = "chdOffsetDate";
    public static final String CHECK_IS_ACTIVE_COLUMN = "chdIsActive";
    public static final String CHECK_ID_COLUMN = "ID";
    public static final String CHECK_GROUP_ID_COLUMN = "chkID";

    private int id;
    private int groupId;
    private double price;
    private long date;
    private long offsetDate;
    private boolean checkIsActive;

    public Check() {
    }

    public Check(double price, long date,long offsetDate,boolean checkIsActive) {
        this.price = price;
        this.date = date;
        this.offsetDate = offsetDate;
        this.checkIsActive = checkIsActive;
    }

    public Check(double price, long date) {
        this.price = price;
        this.date = date;
    }

    public Check(double price) {
        this.price = price;
    }

    public Check(boolean checkIsActive) {
        this.checkIsActive = checkIsActive;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getOffsetDate() {
        return offsetDate;
    }

    public void setOffsetDate(long offsetDate) {
        this.offsetDate = offsetDate;
    }

    public boolean isCheckIsActive() {
        return checkIsActive;
    }

    public void setCheckIsActive(boolean checkIsActive) {
        this.checkIsActive = checkIsActive;
    }
}
