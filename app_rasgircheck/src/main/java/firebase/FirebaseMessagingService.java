package firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import Other.C_Setting;
import Other.Constants;
import Other.Modules;
import TxAnalytics.TxAnalytics;
import air.gamediesel.rasgir.BaseActivity;
import air.gamediesel.rasgir.MainActivity;
import air.gamediesel.rasgir.R;

import static TxAnalytics.TxAnalytics.MessageAdd;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private static String i;
    private String customField;
    private JSONObject custom;
    private JSONObject urlDetails;
    private String title = "", message = "", url = "";

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        try {
            customField = remoteMessage.getData().get("custom");
            custom = new JSONObject(Modules.makeJsonString(customField));
            i = custom.getString("i");
            if (custom.has("a")) {
                urlDetails = custom.getJSONObject("a");
                title = urlDetails.getString("tx_pop_title");
                message = urlDetails.getString("tx_pop_msg");
                url = urlDetails.getString("tx_pop_url");
            }
            String userNumber = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
            MessageAdd(i,
                    TxAnalytics.MessageTypes.PUSH,
                    Constants.APP_ID,
                    Constants.APP_SOURCE,
                    Constants.APP_VERSION,
                    Constants.DEVICE_ID,
                    userNumber,
                    Constants.SERVER_URL, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                }

                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        Log.i("Response", response.body().toString());
                        Log.i("MessageAdd", "message sent");
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (BaseActivity.getInstance() != null) {
            if (!BaseActivity.getInstance().isFinishing()) {
                if (BaseActivity.appIsForeground()) {
                    if (message.equals("") || title.equals("")) {
                        BaseActivity.runOnUi(remoteMessage.getData().get("title"), remoteMessage.getData().get("alert"), "");
                    } else {
                        BaseActivity.runOnUi(remoteMessage.getData().get("title"), remoteMessage.getData().get("alert"), url);
                    }
                } else {
                    makeNotification(remoteMessage);
                }
            }
        } else
            makeNotification(remoteMessage);

    }

    private void makeNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, MainActivity.class);
        NotificationCompat.Builder notiBuilder = new NotificationCompat.Builder(this);
        //
        Log.i("i", i);
        intent.putExtra("i", i);
        intent.putExtra("popUpTitle", remoteMessage.getData().get("title"));
        intent.putExtra("popUpContent", remoteMessage.getData().get("alert"));
        intent.putExtra("popUpURL", url);
        notiBuilder.setContentTitle(remoteMessage.getData().get("title"));
        notiBuilder.setContentText(remoteMessage.getData().get("alert"));
        //
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        notiBuilder.setAutoCancel(true);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notiBuilder.setSound(uri);
        notiBuilder.setSmallIcon(R.drawable.rasgir_icon);
        notiBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notiBuilder.build());
    }

}
