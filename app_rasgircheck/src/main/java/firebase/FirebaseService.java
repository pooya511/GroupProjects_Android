package firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import Other.C_Setting;
import Other.Constants;
import Other.Modules;

import static TxAnalytics.TxAnalytics.UpdateClientPushID;

public class FirebaseService extends FirebaseInstanceIdService{

    private static final String REG_TOKEN = "REG_TOKEN";

    @Override
    public void onTokenRefresh() {
        String recentToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN,recentToken);
        if (recentToken != null && !recentToken.equals("")){
            try {
                String userNumber = C_Setting.getStringValue(C_Setting.TX_USER_NUMBER);
                UpdateClientPushID(recentToken,
                        Constants.APP_ID,
                        Constants.APP_SOURCE,
                        Constants.APP_VERSION,
                        Constants.DEVICE_ID,
                        Constants.OS,
                        userNumber,
                        Constants.SERVER_URL,
                        new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {

                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        if (response.isSuccessful()){
                            String responseStr = response.body().string() ;
                            try {
                                JSONObject responseJson = new JSONObject(Modules.makeJsonString(responseStr));
                                if (responseJson.getString("status").equals(Constants.SUCCESS)){
                                    Log.i("UpdateClientPushID","OK");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
