package com.taxiapps.txpayment.model;

/**
 * Created by Parsa on 2018-02-21.
 */

public class Constants {

    public static final int LIMITS_ID = 1;
    public static final int MAIN_ID = 2;
    public static final int INFO_ID = 3;
    public static final int PIN_VERIFY_M1_ID = 4;
    public static final int PIN_VERIFY_M2_ID = 5;
    public static final int RESULT_ID = 6;
    public static final int LICENSE_ID = 7;

    public static final int PAYMENT_FAILED_ID = -20;
    public static final int BAZAAR_FAILED_ID = -10;

    public static final int BAZAAR_NOT_INSTALLED = -1;
    public static final int BAZAAR_LOGIN_ERROR = -2;
    public static final int BAZAAR_CONNECTION_ERROR = -3;
    public static final int NETWORK_ERROR = -4;
    public static final int USER_IS_PREMIUM = -5;
    public static final int INPUT_IS_INVALID = -6;

    public static final String SUCCESS = "0";
    public static final String UNKNOWN_ERROR = "-10";
    public static final String PRODUCT_INVALID = "-20";
    public static final String TRANSACTION_INVALID = "-30";
    public static final String COUPON_INVALID = "-40";
    public static final String COUPON_EXPIRED = "-41";
    public static final String COUPON_INVALID_APP = "-42";
    public static final String COUPON_EXPIRE_FAILED = "-48";
    public static final String APP_INVALID = "-60";
    public static final String BACKUP_SIZE_LIMIT = "-70";
    public static final String SMS_NUMBER_INVALID = "-80";
    public static final String SMS_METHOD1_DISABLED = "-81";
    public static final String SMS_UNKNOWN_ERROR = "-82";
    public static final String USER_NOT_EXISTS = "-90";
    public static final String LICENSE_NOT_EXISTS = "-50";
    public static final String USER_LICENSE_NOT_FOUND = "-51";

    public static final String INFO_PAYMENT_VERIFY_ERROR = "پرداخت ناموفق بوده است.در صورت کسر مبلغ وجه شما حداکثر تا ۱ ساعت به کارت بانکی برگشت داده می شود. شما می توانید هم اکنون مجددا تلاش کنید";
    public static final String INFO_PAYMENT_STATUS_CANCELED = "شما فرآیند خرید را لغو کردید. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_ISSUERDOWN = "در حال حاضر بانک صادر کننده پاسخگو نیست. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";
    public static final String INFO_PAYMENT_STATUS_UNKNOWN = "خطای نامشخص. شما همچنین می توانید از طریق انتقال وجه کارت به کارت و دریافت پین کد لایسنس نسخه کامل برنامه را فعالسازی نمایید";


    public static final String PAYMENT_CANCELED_BY_USER = "پرداخت توسط کاربر لغو شد";
    public static final String BANK_NOT_RESPONDING = "بانک پساخگو نیست. بعدا تلاش کنید";
    public static final String PAYMENT_CONFIRMATION_ERROR = "خطا در تایید پرداخت";
    public static final String TRANSACTION_CONFIRMATION_ERROR = "خطا در تایید تراکنش";

}
