package com.taxiapps.txpayment.payment;

import android.os.Parcelable;

import com.taxiapps.txpayment.dialogFragment.Payment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class TX_License {

    public interface SetLicenseCallback extends Parcelable {
        void call(long expireDate, String license);
    }

    public interface RemoveLicenseCallback {
        void call();
    }

    public static AppLicense returnedLicense(String license) throws JSONException {
        JSONObject licenseJson = new JSONObject(Payment.makeJsonString(license));
        //
        long validDays = Long.parseLong(licenseJson.getString("license_valid_days")) / (1000 * 60 * 60 * 24);
        Calendar expireDate = Calendar.getInstance();  // تاریخ امروز
        expireDate.add(Calendar.DAY_OF_YEAR, (int) validDays); // 9999 روز دیگه چه روزی میشه

//        C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE,calendar.getTime().getTime());
//        C_Setting.setStringValue(C_Setting.LICENSE_DATA,license);
        return new AppLicense(license, expireDate.getTimeInMillis());
    }

    public void removeLicense(RemoveLicenseCallback callback) {
        callback.call();
//        C_Setting.setStringValue(C_Setting.LICENSE_DATA,null);
//        C_Setting.setLongValue(C_Setting.LICENSE_EXPIRE,0);
    }

    //TODO 2 methode payin bayad tu classe app bashe
//    public boolean isLicenseValid() {
//        if (getLicense() != null) {
//            long expireDate = C_Setting.getLongValue(C_Setting.LICENSE_EXPIRE);
//            long currentDate = new Date().getTime();
//            if (currentDate < expireDate) { // اگر تاریخ انقضا نگذشته بود
//                return true; // لایسنس صحیح
//            } else {
//                return false; // لایسنس منقضی
//            }
//        } else {
//            return false; // لایسنس ندارد
//        }
//    }
//
//    public String getLicense() {
//        if (C_Setting.getStringValue(C_Setting.LICENSE_DATA) != null && !C_Setting.getStringValue(C_Setting.LICENSE_DATA).equals("")) {
//            return C_Setting.getStringValue(C_Setting.LICENSE_DATA);
//        } else {
//            return null;
//        }
//    }

    public static class AppLicense {

        private String license;
        private long expireDate;

        public AppLicense() {
        }

        public AppLicense(String license, long expireDate) {
            this.license = license;
            this.expireDate = expireDate;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public long getExpireDate() {
            return expireDate;
        }

        public void setExpireDate(long expireDate) {
            this.expireDate = expireDate;
        }
    }
}
