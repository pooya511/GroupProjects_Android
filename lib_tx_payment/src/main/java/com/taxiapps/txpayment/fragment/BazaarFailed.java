package com.taxiapps.txpayment.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.model.Constants;

/**
 * Created by Parsa on 2018-02-21.
 */

public class BazaarFailed extends BaseFragment {

    private Button ok;
    private TextView title;
    private TextView description;

    @Override
    void validation(ResponseCallBack callBack) {

    }

    public static BazaarFailed newInstance() {

        Bundle args = new Bundle();

        BazaarFailed fragment = new BazaarFailed();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View layout = inflater.inflate(R.layout.frg_tx_payment_bazaar_login_error,null);
        title = layout.findViewById(R.id.frg_tx_payment_limits_red_title);
        description = layout.findViewById(R.id.tx_payment_bazaar_login_error_description);

        ok = layout.findViewById(R.id.tx_payment_bazaar_login_error_ok_button);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Payment.getInstance().dismiss();
            }
        });

        switch (Limits.bazaarErrorType){
            case Constants.BAZAAR_NOT_INSTALLED:
                if (Payment.getInstance().appSource.equals("bazaar")){
                    title.setText("خطا!");
                    description.setText("شما برای خرید نسخه کامل باید اپلیکیشن بازار را روی گوشی خود نصب کنید");
                }
                if (Payment.getInstance().appSource.equals("myket")){
                    title.setText("خطا!");
                    description.setText("شما برای خرید نسخه کامل باید اپلیکیشن مایکت را روی گوشی خود نصب کنید");
                }
                break;
            case Constants.BAZAAR_LOGIN_ERROR:
                if (Payment.getInstance().appSource.equals("bazaar")){
                    title.setText("خطا در ورود به حساب");
                    description.setText("شما برای خرید نسخه کامل باید به حساب خود در بازار وارد شوید");
                }
                if (Payment.getInstance().appSource.equals("myket")){
                    title.setText("خطا در ورود به حساب");
                    description.setText("شما برای خرید نسخه کامل باید به حساب خود در مایکت وارد شوید");
                }
                break;
            case Constants.BAZAAR_CONNECTION_ERROR:
                if (Payment.getInstance().appSource.equals("bazaar")){
                    title.setText("خطا در ارتباط با بازار");
                    description.setText("در حال حاضر سیستم سیستم پرداخت بازار پاسخگو نیست. لطفا بعدا تلاش کنید");
                }
                if (Payment.getInstance().appSource.equals("myket")){
                    title.setText("خطا در ارتباط با مایکت");
                    description.setText("در حال حاضر سیستم سیستم پرداخت مایکت پاسخگو نیست. لطفا بعدا تلاش کنید");
                }
                break;
        }

        return layout;
    }
}
