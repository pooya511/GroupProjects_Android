package com.taxiapps.txpayment.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Parsa on 2018-02-21.
 */

public class License extends BaseFragment {

    private TextView status, date, paidPrice, buyNumber, limitation, userName, exit;

    public static License newInstance() {

        Bundle args = new Bundle();

        License fragment = new License();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View licenseView = inflater.inflate(R.layout.frg_tx_payment_license, null);

        Payment.getInstance().headerDescription.setText(R.string.license_header_description);

        status = licenseView.findViewById(R.id.frg_tx_payment_license_status);
        date = licenseView.findViewById(R.id.frg_tx_payment_license_date);
        paidPrice = licenseView.findViewById(R.id.frg_tx_payment_license_paid_price);
        buyNumber = licenseView.findViewById(R.id.frg_tx_payment_license_buy_number);
        limitation = licenseView.findViewById(R.id.frg_tx_payment_license_limitation);
        userName = licenseView.findViewById(R.id.frg_tx_payment_license_username);
        exit = licenseView.findViewById(R.id.frg_tx_payment_license_exit);

        TX_License.AppLicense appLicense = Result.getInstance().getLicense();

        try {
            JSONObject jsonLicense = new JSONObject(Payment.makeJsonString(appLicense.getLicense()));

            String statusStr = "خرید موفق لایسنس " + Payment.getInstance().app_fa;
            status.setText(statusStr);

            date.setText(jsonLicense.getString("date_fa"));

            String price = persianDigitToEnglishDigit(jsonLicense.getString("amount").replaceAll("\\D", ""));
            if (!price.equals("")) {
                price = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(price) / 10); // separator
                paidPrice.setText(price.concat(" تومان"));
            }

            buyNumber.setText(jsonLicense.getString("pcode"));
            limitation.setText(jsonLicense.getString("description"));

            if (jsonLicense.has("username"))
                userName.setText(jsonLicense.getString("username"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO refresh ui Permium
                Payment.getInstance().dismiss();
            }
        });

        return licenseView;
    }

    @Override
    void validation(ResponseCallBack callBack) {

    }

    private static String persianDigitToEnglishDigit(String str) {
        return str.replace("۰", "0").replace("۱", "1").replaceAll("۲", "2").replaceAll("۳", "3").replaceAll("۴", "4").replaceAll("۵", "5")
                .replaceAll("۶", "6").replaceAll("۷", "7").replaceAll("۸", "8").replaceAll("۹", "9");
    }

}
