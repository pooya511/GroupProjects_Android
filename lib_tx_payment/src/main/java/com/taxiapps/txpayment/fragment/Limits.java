package com.taxiapps.txpayment.fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.mohamadamin.persianmaterialdatetimepicker.utils.TimeZones;
import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.model.Constants;
import com.taxiapps.txpayment.payment.TX_License;
import com.taxiapps.txpayment.payment.util.IabHelper;
import com.taxiapps.txpayment.payment.util.IabResult;
import com.taxiapps.txpayment.payment.util.Inventory;
import com.taxiapps.txpayment.payment.util.SkuDetails;

import org.json.JSONException;

/**
 * Created by Parsa on 2018-02-21.
 */

public class Limits extends BaseFragment {

    public IabHelper mHelper;
    public IabHelper.QueryInventoryFinishedListener mGotInventoryListener;
    public IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;

    private Button activation;
    private TextView featuresList , title;

    private ProgressBar loading;

    private static Limits instance;

    public static Limits getInstance() {
        return instance;
    }

    public static int bazaarErrorType;

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }


    public static Limits newInstance(String title , String featuresList) {

        Bundle args = new Bundle();
        args.putString("featuresList", featuresList);
        args.putString("title", title);

        Limits fragment = new Limits();
        fragment.setArguments(args);

        instance = fragment;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Payment.getInstance().headerDescription.setText(R.string.limit_header_description);

        View limitsLayout = inflater.inflate(R.layout.frg_tx_payment_limits, null);
        featuresList = limitsLayout.findViewById(R.id.frg_tx_payment_limits_text_list);
        title = limitsLayout.findViewById(R.id.frg_tx_payment_limits_title);
        activation = limitsLayout.findViewById(R.id.frg_tx_payment_limits_activate_button);
        loading = limitsLayout.findViewById(R.id.frg_tx_payment_limits_progressbar);

        featuresList.setText(getArguments().getString("featuresList"));
        title.setText(getArguments().getString("title"));
        activation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                loading.setVisibility(View.VISIBLE);
                validation(new ResponseCallBack() {
                    @Override
                    public void success() {
                        loading.setVisibility(View.GONE);
                        Payment.getInstance().initTargetFragment(Constants.MAIN_ID); // boro be safhe main
                    }

                    @Override
                    public void failed(int result) {
                        loading.setVisibility(View.GONE);
                        switch (result) {
                            case Constants.BAZAAR_NOT_INSTALLED:
                                bazaarErrorType = Constants.BAZAAR_NOT_INSTALLED;
                                Payment.getInstance().initTargetFragment(Constants.BAZAAR_FAILED_ID);
                                break;
                            case Constants.BAZAAR_LOGIN_ERROR:
                                bazaarErrorType = Constants.BAZAAR_LOGIN_ERROR;
                                Payment.getInstance().initTargetFragment(Constants.BAZAAR_FAILED_ID);
                                break;
                            case Constants.BAZAAR_CONNECTION_ERROR:
                                bazaarErrorType = Constants.BAZAAR_CONNECTION_ERROR;
                                Payment.getInstance().initTargetFragment(Constants.BAZAAR_FAILED_ID);
                                break;
                            case Constants.NETWORK_ERROR:
                                Payment.getInstance().customToast(getActivity(), "اینترنت", "لطفا به اینترنت متصل شوید.", false).show();
                                break;

                        }
                    }
                });
            }
        });

        return limitsLayout;
    }

    @Override
    void validation(final ResponseCallBack callBack) {

        if (isNetworkAvailable(getActivity())) {
            switch (Payment.getInstance().appSource) {
                case "google":
                    callBack.success();
                    break;

                case "bazaar":
                    if (appInstalledOrNot(getActivity(), "com.farsitel.bazaar")) {
                        mHelper = new IabHelper(getActivity(), Payment.getInstance().base64EncodedPublicKey);
                        Log.d(Payment.TAG, "Starting setup.");
                        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                            public void onIabSetupFinished(IabResult result) {
                                Log.d(Payment.TAG, "Setup finished.");
                                if (!result.isSuccess()) {
                                    // Oh noes, there was a problem.
                                    Log.d(Payment.TAG, "Problem setting up In-app Billing: " + result);
                                    callBack.failed(Constants.BAZAAR_CONNECTION_ERROR);
                                }
                                // Hooray, IAB is fully set up!
                                mHelper.queryInventoryAsync(mGotInventoryListener);
                            }
                        });
                        mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
                            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                                Log.d(Payment.TAG, "Query inventory finished.");
                                if (result.isFailure()) {
                                    Log.d(Payment.TAG, "Failed to query inventory: " + result);
                                    Payment.getInstance().initTargetFragment(Constants.BAZAAR_FAILED_ID);
                                    callBack.failed(Constants.BAZAAR_LOGIN_ERROR);
                                    return;
                                } else {
                                    Log.d(Payment.TAG, "Query inventory was successful.");
                                    // does the user have the premium upgrade?
                                    Payment.getInstance().mIsPremium = inventory.hasPurchase(Payment.getInstance().SKU_PREMIUM);
                                    // update UI

                                    Log.d(Payment.TAG, "User is " + (Payment.getInstance().mIsPremium ? "PREMIUM" : "NOT PREMIUM"));

                                    if (Payment.getInstance().mIsPremium) {
                                        SkuDetails skuDetails = inventory.getSkuDetails(Payment.getInstance().SKU_PREMIUM);

                                        PersianCalendar pCalendar = new PersianCalendar();

                                        String license = "{\\\"status\\\":\\\"0\\\""
                                                + ",\\\"license_product\\\":\\\"full_lifetime\\\""
                                                + ",\\\"license_valid_days\\\":\\\"863913600000\\\""
                                                + ",\\\"app_fa\\\":\\\"" + Payment.getInstance().app_fa + "\\\""
                                                + ",\\\"appid\\\":\\\"" + Payment.getInstance().appID + "\\\""
                                                + ",\\\"source\\\":\\\"" + Payment.getInstance().appSource + "\\\""
                                                + ",\\\"version\\\":\\\"" + Payment.getInstance().appVersion + "\\\""
                                                + ",\\\"pcode\\\":\\\"_\\\""
                                                + ",\\\"digitalreceipt\\\":\\\"\\\""
                                                + ",\\\"amount\\\":\\\"" + skuDetails.getPrice() + "\\\""
                                                + ",\\\"_status\\\":\\\"0\\\""
                                                + ",\\\"verify\\\":\\\"0\\\""
                                                + ",\\\"coupon\\\":\\\"\\\""
                                                + ",\\\"description\\\":\\\"" + skuDetails.getDescription() + "\\\""
                                                + ",\\\"date_fa\\\":\\\"" + pCalendar.getPersianLongDateAndTime() + "\\\""
                                                + ",\\\"username\\\":\\\"" + Payment.getInstance().userName + "\\\"}";

                                        //
                                        try {
                                            Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                            Result.getInstance().setLicense(TX_License.returnedLicense(license));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        callBack.failed(Constants.USER_IS_PREMIUM);
                                    } else
                                        callBack.success();

                                }
                                Log.d(Payment.TAG, "Initial inventory query finished; enabling main UI.");
                            }
                        };
                    } else { // bazaar nadare
                        Payment.getInstance().initTargetFragment(Constants.BAZAAR_FAILED_ID);
                        callBack.failed(Constants.BAZAAR_NOT_INSTALLED);
                    }
                    break;

                    case "myket":
                    if (appInstalledOrNot(getActivity(), "ir.mservices.market")) {
                        mHelper = new IabHelper(getActivity(), Payment.getInstance().base64EncodedPublicKey);
                        Log.d(Payment.TAG, "Starting setup.");
                        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                            public void onIabSetupFinished(IabResult result) {
                                Log.d(Payment.TAG, "Setup finished.");
                                if (!result.isSuccess()) {
                                    // Oh noes, there was a problem.
                                    Log.d(Payment.TAG, "Problem setting up In-app Billing: " + result);
                                    callBack.failed(Constants.BAZAAR_CONNECTION_ERROR);
                                }
                                // Hooray, IAB is fully set up!
                                mHelper.queryInventoryAsync(mGotInventoryListener);
                            }
                        });
                        mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
                            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                                Log.d(Payment.TAG, "Query inventory finished.");
                                if (result.isFailure()) {
                                    Log.d(Payment.TAG, "Failed to query inventory: " + result);
                                    Payment.getInstance().initTargetFragment(Constants.BAZAAR_FAILED_ID);
                                    callBack.failed(Constants.BAZAAR_LOGIN_ERROR);
                                    return;
                                } else {
                                    Log.d(Payment.TAG, "Query inventory was successful.");
                                    // does the user have the premium upgrade?
                                    Payment.getInstance().mIsPremium = inventory.hasPurchase(Payment.getInstance().SKU_PREMIUM);
                                    // update UI

                                    Log.d(Payment.TAG, "User is " + (Payment.getInstance().mIsPremium ? "PREMIUM" : "NOT PREMIUM"));

                                    if (Payment.getInstance().mIsPremium) {
                                        SkuDetails skuDetails = inventory.getSkuDetails(Payment.getInstance().SKU_PREMIUM);

                                        PersianCalendar pCalendar = new PersianCalendar();

                                        String license = "{\\\"status\\\":\\\"0\\\""
                                                + ",\\\"license_product\\\":\\\"full_lifetime\\\""
                                                + ",\\\"license_valid_days\\\":\\\"863913600000\\\""
                                                + ",\\\"app_fa\\\":\\\"" + Payment.getInstance().app_fa + "\\\""
                                                + ",\\\"appid\\\":\\\"" + Payment.getInstance().appID + "\\\""
                                                + ",\\\"source\\\":\\\"" + Payment.getInstance().appSource + "\\\""
                                                + ",\\\"version\\\":\\\"" + Payment.getInstance().appVersion + "\\\""
                                                + ",\\\"pcode\\\":\\\"_\\\""
                                                + ",\\\"digitalreceipt\\\":\\\"\\\""
                                                + ",\\\"amount\\\":\\\"" + skuDetails.getPrice() + "\\\""
                                                + ",\\\"_status\\\":\\\"0\\\""
                                                + ",\\\"verify\\\":\\\"0\\\""
                                                + ",\\\"coupon\\\":\\\"\\\""
                                                + ",\\\"description\\\":\\\"" + skuDetails.getDescription() + "\\\""
                                                + ",\\\"date_fa\\\":\\\"" + pCalendar.getPersianLongDateAndTime() + "\\\""
                                                + ",\\\"username\\\":\\\"" + Payment.getInstance().userName + "\\\"}";

                                        //
                                        try {
                                            Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                            Result.getInstance().setLicense(TX_License.returnedLicense(license));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        callBack.failed(Constants.USER_IS_PREMIUM);
                                    } else
                                        callBack.success();

                                }
                                Log.d(Payment.TAG, "Initial inventory query finished; enabling main UI.");
                            }
                        };
                    } else { // bazaar nadare
                        Payment.getInstance().initTargetFragment(Constants.BAZAAR_FAILED_ID);
                        callBack.failed(Constants.BAZAAR_NOT_INSTALLED);
                    }
                    break;
            }

        } else {
            callBack.failed(Constants.NETWORK_ERROR);
        }
    }
}
