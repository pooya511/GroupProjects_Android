package com.taxiapps.txpayment.fragment;

import android.app.Fragment;

/**
 * Created by Parsa on 2018-02-21.
 */

public abstract class BaseFragment extends Fragment {

    public interface ResponseCallBack {
        void success();
        void failed(int result);
    }

    abstract void validation(ResponseCallBack callBack);


}
