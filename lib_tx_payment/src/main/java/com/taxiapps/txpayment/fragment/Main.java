package com.taxiapps.txpayment.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.model.Constants;
import com.taxiapps.txpayment.model.Product;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import static TxAnalytics.TxAnalytics.CouponGet;
import static TxAnalytics.TxAnalytics.GetProducts;

/**
 * Created by Parsa on 2018-02-21.
 */

public class Main extends BaseFragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ProductAdapter productAdapter;

    public boolean isFree;
    public String coupon;

    public static Main newInstance() {

        Bundle args = new Bundle();

        Main fragment = new Main();
        instance = fragment;
        fragment.setArguments(args);
        return fragment;
    }

    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View mainLayout = inflater.inflate(R.layout.frg_tx_payment_main, null);

        Payment.getInstance().headerDescription.setText(R.string.main_header_description);

        recyclerView = mainLayout.findViewById(R.id.frg_tx_payment_main_recyclerview);
        progressBar = mainLayout.findViewById(R.id.frg_tx_payment_main_progressbar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        try {
            progressBar.setVisibility(View.VISIBLE);
            GetProducts(Payment.getInstance().appID,
                    Payment.getInstance().appSource,
                    Payment.getInstance().deviceID,
                    Payment.getInstance().userName,
                    Payment.getInstance().serverUrl,
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            Log.i("GetProducts", "failed");
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (response.isSuccessful()) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                                String responseStr = response.body().string();
                                try {
                                    ArrayList<Product> products = getProductContent(responseStr);
                                    productAdapter = new ProductAdapter(getActivity(), products, inflater);
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            recyclerView.setAdapter(productAdapter);
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mainLayout;
    }

    private static ArrayList<Product> getProductContent(String str) throws JSONException {
        ArrayList<Product> productContentArray = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(Payment.makeJsonString(str));
        String value = jsonObject.getString("products");
        JSONObject jsonObject1 = new JSONObject(Payment.makeJsonString(value));
        String key1 = "0";
        String value1 = jsonObject1.getString(key1);
        if (key1.equals("0")) {
            Product productContent1 = new Product();
            JSONObject jsonObject2 = new JSONObject(Payment.makeJsonString(value1));
            Iterator iterator2 = jsonObject2.keys();
            while (iterator2.hasNext()) {
                String key2 = String.valueOf(iterator2.next());
                String value2 = jsonObject2.getString(key2);
                switch (key2) {
                    case "product":
                        productContent1.setProduct(value2);
                        Payment.getInstance().SKU_PREMIUM = value2;
                        break;
                    case "text":
                        productContent1.setText(value2);
                        break;
                    case "text_color":
                        productContent1.setTextColor(value2);
                        break;
                    case "price":
                        productContent1.setPrice(Integer.parseInt(value2));
                        break;
                    case "description":
                        productContent1.setDescription(value2);
                        break;
                    case "hasCoupon":
                        if (value2.equals("true")) {
                            productContent1.setHasCoupon(true);
                        } else {
                            productContent1.setHasCoupon(false);
                        }
                        break;
                    case "description_color":
                        productContent1.setDescriptionColor(value2);
                        break;
                }
            }
            productContentArray.add(productContent1);
        }
        if (jsonObject1.has("1")) {
            key1 = "1";
            value1 = jsonObject1.getString(key1);
            if (key1.equals("1")) {
                Product productContent2 = new Product();
                JSONObject jsonObject2 = new JSONObject(Payment.makeJsonString(value1));
                Iterator iterator2 = jsonObject2.keys();
                while (iterator2.hasNext()) {
                    String key2 = String.valueOf(iterator2.next());
                    String value2 = jsonObject2.getString(key2);
                    switch (key2) {
                        case "product":
                            productContent2.setProduct(value2);
                            break;
                        case "text":
                            productContent2.setText(value2);
                            break;
                        case "text_color":
                            productContent2.setTextColor(value2);
                            break;
                        case "price":
                            productContent2.setPrice(Integer.parseInt(value2));
                            break;
                        case "description":
                            productContent2.setDescription(value2);
                            break;
                        case "hasCoupon":
                            if (value2.equals("true")) {
                                productContent2.setHasCoupon(true);
                            } else {
                                productContent2.setHasCoupon(false);
                            }
                            break;
                        case "description_color":
                            productContent2.setDescriptionColor(value2);
                            break;
                    }
                }
                productContentArray.add(productContent2);
            }
        }
        return productContentArray;
    }

    @Override
    void validation(ResponseCallBack callBack) {

    }

    public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        public static final int HAS_COUPON = 1;
        public static final int HAS_NOT_COUPON = 2;
        public static final int RECOVERY = 3;

        private Context context;
        private ArrayList<Product> products;
        private LayoutInflater inflater;

        public ProductAdapter(Context context, ArrayList<Product> products, LayoutInflater inflater) {
            this.context = context;
            this.products = products;
            this.inflater = inflater.from(context);

            for (Product product : products) {
                if (product.isHasCoupon())
                    product.setProductType(HAS_COUPON);
                else
                    product.setProductType(HAS_NOT_COUPON);
            }

            if (Payment.getInstance().appSource.equals("google")) {
                Product recoveryProduct = new Product(); // static recovery product
                recoveryProduct.setProductType(RECOVERY);
                products.add(recoveryProduct);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case HAS_COUPON:
                    return new HasCouponViewHolder(inflater.inflate(R.layout.itm_purchase_main_has_coupon, parent, false));
                case HAS_NOT_COUPON:
                    return new HasNotCouponViewHolder(inflater.inflate(R.layout.itm_purchase_main_has_not_coupon, parent, false));
                case RECOVERY:
                    return new RecoveryViewHolder(inflater.inflate(R.layout.itm_purchase_recovery, parent, false));
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            final Product product = products.get(position);

            switch (holder.getItemViewType()) {
                case HAS_COUPON:
                    final HasCouponViewHolder hasCouponViewHolder = (HasCouponViewHolder) holder;

                    hasCouponViewHolder.title.setText(products.get(position).getText().replace("n", "\n"));
                    hasCouponViewHolder.title.setTextColor(Color.parseColor(products.get(position).getTextColor()));

                    hasCouponViewHolder.description.setText(products.get(position).getDescription());
                    hasCouponViewHolder.description.setTextColor(Color.parseColor(products.get(position).getDescriptionColor()));

                    hasCouponViewHolder.activation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final String userCoupon = hasCouponViewHolder.pinCode.getText().toString();
                            if (pinCodeInputValidation(userCoupon)) {
                                final Dialog spinnerDialog = spinnerDialog(getActivity());
                                spinnerDialog.show();
                                try {
                                    CouponGet(userCoupon,
                                            Payment.getInstance().serverUrl,
                                            new Callback() {
                                                @Override
                                                public void onFailure(Request request, IOException e) {
                                                    spinnerDialog.dismiss();
                                                }

                                                @Override
                                                public void onResponse(Response response) throws IOException {
                                                    spinnerDialog.dismiss();
                                                    String responseStr = response.body().string();
                                                    Log.i("coupon_get_response", responseStr);
                                                    coupon = userCoupon;
                                                    try {
                                                        JSONObject responseJson = new JSONObject(responseStr);
                                                        if (responseJson.getString("status").equals(Constants.SUCCESS)) {
                                                            checkCoupon(responseJson, product);
                                                        } else if (responseJson.getString("status").equals(Constants.COUPON_EXPIRED)) {
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Payment.getInstance().customToast(getActivity(), "خطا!", "کوپن شما منقضی شده است", false).show();
                                                                }
                                                            });
                                                        } else if (responseJson.getString("status").equals(Constants.COUPON_INVALID)) {
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Payment.getInstance().customToast(getActivity(), "خطا!", "کوپن یافت نشد", false).show();
                                                                }
                                                            });
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Payment.getInstance().customToast(getActivity(), "خطا!", "کوپن خالی است.", false).show();
                                    }
                                });
                                hasCouponViewHolder.pinCode.setText("");
                            }
                        }
                    });

                    hasCouponViewHolder.itemView.setOnClickListener(new View.OnClickListener()

                    {
                        @Override
                        public void onClick(View v) {
                            if (hasCouponViewHolder.expandLayout.getVisibility() == View.VISIBLE)
                                hasCouponViewHolder.expandLayout.setVisibility(View.GONE);
                            else
                                hasCouponViewHolder.expandLayout.setVisibility(View.VISIBLE);
                        }
                    });

                    break;
                case HAS_NOT_COUPON:
                    HasNotCouponViewHolder hasNotCouponViewHolder = (HasNotCouponViewHolder) holder;

                    hasNotCouponViewHolder.title.setText(products.get(position).getText().replace("n", "\n"));
                    hasNotCouponViewHolder.title.setTextColor(Color.parseColor(products.get(position).

                            getTextColor()));

                    hasNotCouponViewHolder.description.setText(products.get(position).

                            getDescription());
                    hasNotCouponViewHolder.description.setTextColor(Color.parseColor(products.get(position).

                            getDescriptionColor()));

                    hasNotCouponViewHolder.price.setText(

                            thousandSeparator(products.get(position).getPrice() / 10));

                    hasNotCouponViewHolder.itemView.setOnClickListener(new View.OnClickListener()

                    {
                        @Override
                        public void onClick(View v) {
                            Payment.getInstance().initTargetFragment(Constants.INFO_ID);
                            Info.getInstance().setInfoType(HAS_NOT_COUPON);
                            Info.getInstance().setTargetProduct(product);
                        }
                    });
                    break;
                case RECOVERY:
                    RecoveryViewHolder recoveryViewHolder = (RecoveryViewHolder) holder;

                    recoveryViewHolder.itemView.setOnClickListener(new View.OnClickListener()

                    {
                        @Override
                        public void onClick(View v) {
                            Payment.getInstance().initTargetFragment(Constants.INFO_ID);
                            Info.getInstance().setInfoType(RECOVERY);
                            Info.getInstance().setTargetProduct(product);
                        }
                    });

                    break;
            }
        }

        @Override
        public int getItemViewType(int position) {
            super.getItemViewType(position);
            switch (products.get(position).getProductType()) {
                case HAS_COUPON:
                    return HAS_COUPON;
                case HAS_NOT_COUPON:
                    return HAS_NOT_COUPON;
                case RECOVERY:
                    return RECOVERY;
                default:
                    return 0;
            }
        }

        @Override
        public int getItemCount() {
            return products.size();
        }

        class HasCouponViewHolder extends RecyclerView.ViewHolder {

            private TextView title, description, activation;
            private EditText pinCode;
            private RelativeLayout expandLayout;

            public HasCouponViewHolder(View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.enter_pincode_layout_close_title);
                description = itemView.findViewById(R.id.enter_pincode_layout_close_description);
                pinCode = itemView.findViewById(R.id.enter_pincode_layout_edittext);
                activation = itemView.findViewById(R.id.enter_pincode_layout_activation);
                expandLayout = itemView.findViewById(R.id.tx_payment_main_pincode_layout_open);
            }
        }

        class HasNotCouponViewHolder extends RecyclerView.ViewHolder {

            private TextView title, price, description;

            public HasNotCouponViewHolder(View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.buy_complete_version_layout_title);
                price = itemView.findViewById(R.id.buy_complete_version_layout_price);
                description = itemView.findViewById(R.id.buy_complete_version_layout_description);
            }
        }

        class RecoveryViewHolder extends RecyclerView.ViewHolder {

            public RecoveryViewHolder(View itemView) {
                super(itemView);
            }
        }


    }

    public String thousandSeparator(int value) {
        return NumberFormat.getInstance(Locale.US).format(value);
    }

    private boolean pinCodeInputValidation(String str) {
        if (str.matches("(?<!\\d)\\d{11,16}(?!\\d)")) {
            return true;
        } else {
            return false;
        }
    }

    public Dialog spinnerDialog(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View spinnerDialogLayout = inflater.inflate(R.layout.pop_spinner, null);

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(spinnerDialogLayout);
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //
        lp.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        lp.width = ConstraintLayout.LayoutParams.WRAP_CONTENT;


        dialogWindow.setAttributes(lp);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    private void checkCoupon(JSONObject responseJson, Product product) {
        try {
            if (!responseJson.getString("appid").equals(Payment.getInstance().appID) && !responseJson.getString("appid").equals("*")) {
                Log.i("appID", "Not ok");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Payment.getInstance().customToast(getActivity(), "خطا!", "این کوپن برای برنامه ی دیگری می باشد.", false).show();
                    }
                });
                // khata
                //in coupon bara barname digari mibashad
            } else {
                Log.i("appID", "ok");

                if (!responseJson.getString("value").equals("100%")) {//coupon 100% k bere to safe result
                    isFree = false;
                    Payment.getInstance().initTargetFragment(Constants.INFO_ID);
                    Info.getInstance().setInfoType(ProductAdapter.HAS_COUPON);
                    Info.getInstance().setTargetProduct(product);

                } else {//coupon bakhshi darsade k bayad bere too dargah
                    isFree = true;
                    Payment.getInstance().initTargetFragment(Constants.INFO_ID);
                    Info.getInstance().setInfoType(ProductAdapter.HAS_COUPON);
                    Info.getInstance().setTargetProduct(product);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
