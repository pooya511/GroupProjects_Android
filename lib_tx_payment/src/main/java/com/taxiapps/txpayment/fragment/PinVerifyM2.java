package com.taxiapps.txpayment.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.model.Constants;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static TxAnalytics.TxAnalytics.CheckPINVerifyMethod2;
import static TxAnalytics.TxAnalytics.GeneratePINVerifyMethod2;
import static TxAnalytics.TxAnalytics.ReactivateLicense;

/**
 * Created by Parsa on 2018-02-21.
 */

public class PinVerifyM2 extends BaseFragment {

    private TextView pinVerifyM2Description;
    private TextView pinVerifyM2Pin;
    private TextView waitingForSmsCountDownTextView;
    private TextView backTextView;
    private ProgressBar pinVerifyM2PinProgressBar;
    private LinearLayout waitingForSmsLayout;
    private Button sendSmsBtn;

    private CountDownTimer getLicenseTimer, waitingForSmsCountDownTimer;

    public String lineNumber;
    public String pinCode;

    public static PinVerifyM2 newInstance() {

        Bundle args = new Bundle();

        PinVerifyM2 fragment = new PinVerifyM2();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View pinVerifyM2Layout = inflater.inflate(R.layout.frg_tx_payment_pin_verify_m2, null);

        Payment.getInstance().headerDescription.setText(R.string.pin_verify_header_description);

        pinVerifyM2Description = pinVerifyM2Layout.findViewById(R.id.frg_tx_payment_pin_verify_m2_description);
        pinVerifyM2Pin = pinVerifyM2Layout.findViewById(R.id.frg_tx_payment_pin_verify_m2_pin_textview);
        waitingForSmsCountDownTextView = pinVerifyM2Layout.findViewById(R.id.frg_tx_payment_pin_verify_m2_countdown);
        backTextView = pinVerifyM2Layout.findViewById(R.id.frg_tx_payment_pin_verify_m2_exit);
        pinVerifyM2PinProgressBar = pinVerifyM2Layout.findViewById(R.id.frg_tx_payment_pin_verify_m2_pin_progressbar);
        waitingForSmsLayout = pinVerifyM2Layout.findViewById(R.id.frg_tx_payment_pin_verify_m2_waiting_for_sms);
        sendSmsBtn = pinVerifyM2Layout.findViewById(R.id.frg_tx_payment_pin_verify_m2_send_sms_button);

        pinVerifyM2Pin.setTypeface(Typeface.SANS_SERIF);

        try {
            GeneratePINVerifyMethod2(Payment.getInstance().appID,
                    Payment.getInstance().appSource,
                    Payment.getInstance().appVersion,
                    Payment.getInstance().deviceID,
                    Info.getInstance().userNumber,
                    Payment.getInstance().serverUrl,
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {

                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (response.isSuccessful()) {
                                String responseStr = response.body().string();
                                Log.i("GeneratePINVerify2", responseStr);
                                JSONObject responseJson;
                                try {
                                    responseJson = new JSONObject(Payment.makeJsonString(responseStr));
                                    pinCode = responseJson.getString("pin");
                                    lineNumber = responseJson.getString("linenumber");

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pinVerifyM2Description.setText("لطفا پین کد زیر را از شماره " + Info.getInstance().userNumber + " به شماره " + lineNumber + " پیامک بفرمایید.");
                                            pinVerifyM2Pin.setText(pinCode);
                                            pinVerifyM2Pin.setVisibility(View.VISIBLE);
                                            pinVerifyM2PinProgressBar.setVisibility(View.GONE);
                                            waitingForSmsLayout.setVisibility(View.VISIBLE);
                                            getLicenseTimer.start();
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        getLicenseTimer = new CountDownTimer(100000, 5000) {
            @Override
            public void onTick(long millisUntilFinished) {
                try {
                    CheckPINVerifyMethod2(Payment.getInstance().appID,
                            Payment.getInstance().appSource,
                            Payment.getInstance().appVersion,
                            Payment.getInstance().deviceID,
                            Info.getInstance().userNumber,
                            Payment.getInstance().serverUrl,
                            new Callback() {
                                @Override
                                public void onFailure(Request request, IOException e) {

                                }

                                @Override
                                public void onResponse(Response response) throws IOException {
                                    if (response.isSuccessful()) {
                                        String responseStr = response.body().string();
                                        Log.i("CheckPINVerifyMethod2", responseStr);
                                        try {
                                            JSONObject responseJson = new JSONObject(Payment.makeJsonString(responseStr));
                                            if (responseJson.getString("status").equals(Constants.SUCCESS)) {
                                                if (responseJson.getString("approved").equals("1")) {
                                                    getLicenseTimer.cancel();
                                                    ReactivateLicense(String.valueOf(PinVerifyM1.licenseID),
                                                            Payment.getInstance().appID,
                                                            Payment.getInstance().appSource,
                                                            Payment.getInstance().appVersion,
                                                            Payment.getInstance().deviceID,
                                                            Info.getInstance().userNumber,
                                                            Payment.getInstance().serverUrl,
                                                            new Callback() {
                                                                @Override
                                                                public void onFailure(Request request, IOException e) {

                                                                }

                                                                @Override
                                                                public void onResponse(Response response) throws IOException {
                                                                    if (response.isSuccessful()) {
                                                                        try {
                                                                            String responseStr = response.body().string();
                                                                            Log.i("ReactivateLicensRespons", responseStr);
                                                                            JSONObject licenseJson = new JSONObject(responseStr);
                                                                            if (licenseJson.length() != 0) {
                                                                                Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                                                                Result.getInstance().setLicense(TX_License.returnedLicense(responseStr));
                                                                            } else { // khatay bazyabi e license
                                                                                Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                                                PaymentFailed.getInstance().setErrorMessage("لایسنسی برای این برنامه یافت نشد !");
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {

            }
        };

        waitingForSmsCountDownTimer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                waitingForSmsCountDownTextView.setText(millisUntilFinished / 1000 + " ثانیه");
            }

            @Override
            public void onFinish() {
                waitingForSmsLayout.setVisibility(View.GONE);
            }
        }.start();

        View.OnClickListener listeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(sendSmsBtn)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("smsto:"));
                    intent.setType("vnd.android-dir/mms-sms");
                    intent.putExtra("address", lineNumber);
                    intent.putExtra("sms_body", pinCode);
                    getActivity().startActivity(intent);
                }
                if (v.equals(backTextView)) {
                    getFragmentManager().popBackStack();
                    waitingForSmsCountDownTimer.cancel();
                }

            }
        };
        sendSmsBtn.setOnClickListener(listeners);
        backTextView.setOnClickListener(listeners);

        return pinVerifyM2Layout;
    }

    @Override
    void validation(ResponseCallBack callBack) {

    }
}
