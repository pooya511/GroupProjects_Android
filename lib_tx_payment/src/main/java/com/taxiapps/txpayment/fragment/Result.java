package com.taxiapps.txpayment.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.model.Constants;
import com.taxiapps.txpayment.payment.TX_License;

/**
 * Created by Parsa on 2018-02-21.
 */

public class Result extends BaseFragment {

    private TX_License.AppLicense license;

    private static Result instance;

    public static Result getInstance() {
        return instance;
    }

    public static Result newInstance() {

        Bundle args = new Bundle();

        Result fragment = new Result();
        fragment.setArguments(args);

        instance = fragment;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View paymentResultLayout = inflater.inflate(R.layout.frg_tx_payment_result, null);

        Payment.getInstance().setLicenseCallback.call(getInstance().getLicense().getExpireDate(), getInstance().getLicense().getLicense());

        Payment.getInstance().headerDescription.setText(R.string.result_header_description);

        Button seeLicenseButton = paymentResultLayout.findViewById(R.id.frg_tx_payment_result_see_license_button);

        seeLicenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Payment.getInstance().initTargetFragment(Constants.LICENSE_ID);
            }
        });


        return paymentResultLayout;
    }

    @Override
    void validation(ResponseCallBack callBack) {

    }

    public TX_License.AppLicense getLicense() {
        return license;
    }

    public void setLicense(TX_License.AppLicense license) {
        this.license = license;
    }
}
