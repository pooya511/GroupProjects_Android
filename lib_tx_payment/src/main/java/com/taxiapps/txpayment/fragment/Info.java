package com.taxiapps.txpayment.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.model.Constants;
import com.taxiapps.txpayment.model.Product;
import com.taxiapps.txpayment.payment.TX_License;
import com.taxiapps.txpayment.payment.util.IabHelper;
import com.taxiapps.txpayment.payment.util.IabResult;
import com.taxiapps.txpayment.payment.util.Purchase;
import com.taxiapps.txpayment.payment.util.SkuDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

import static TxAnalytics.TxAnalytics.GetUserLicenses;
import static TxAnalytics.TxAnalytics.PurchaseByCoupon;
import static TxAnalytics.TxAnalytics.PurchaseVerify;
import static TxAnalytics.TxAnalytics.getBankRedirectURL;
import static com.taxiapps.txpayment.dialogFragment.Payment.TAG;

/**
 * Created by Parsa on 2018-02-21.
 */

public class Info extends BaseFragment {

    private EditText userInput;
    private TextView errorMessage, back;
    private Button continuee;

    private int infoType;

    private Product targetProduct;
    private String description;
    private TextView descriptionTextView;

    public String userNumber;

    public static Info instance;

    public static Info newInstance() {

        Bundle args = new Bundle();

        Info fragment = new Info();
        fragment.setArguments(args);

        instance = fragment;

        return fragment;
    }

    public static Info getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View infoLayout = inflater.inflate(R.layout.frg_tx_payment_info, null);

        Payment.getInstance().headerDescription.setText(R.string.info_header_description);

        userInput = infoLayout.findViewById(R.id.frg_tx_payment_info_number_edittext);
        errorMessage = infoLayout.findViewById(R.id.frg_tx_payment_info_error_textview);
        continuee = infoLayout.findViewById(R.id.frg_tx_payment_info_continue_button);
        back = infoLayout.findViewById(R.id.frg_tx_payment_info_back);
        descriptionTextView = infoLayout.findViewById(R.id.frg_tx_payment_info_title_text);

        switch (getInstance().getInfoType()) {
            case Main.ProductAdapter.HAS_COUPON:
                description = "برای فعالسازی با پین کد لطفا شماره موبایل خود را وارد کنید.";
                back.setVisibility(View.INVISIBLE);
                break;
            case Main.ProductAdapter.HAS_NOT_COUPON:
                description = "لطفا اطلاعات را به درستی وارد کنید. برای بازیابی خرید بر روی گوشی دیگر به آن ها نیاز خواهید داشت.";
                break;
            case Main.ProductAdapter.RECOVERY:
                description = "لطفا شماره موبایلی توسط آن برنامه را خریداری نموده اید را وارد نمایید.";
                break;
        }
        descriptionTextView.setText(description);

        continuee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation(new ResponseCallBack() {
                    @Override
                    public void success() {
                        userNumber = userInput.getText().toString();
                        Payment.getInstance().userNameCallBack.getUserName(userNumber);
                        switch (Payment.getInstance().appSource) {
                            case "google":
                                switch (getInstance().getInfoType()) {
                                    case Main.ProductAdapter.HAS_COUPON:
                                        if (Main.getInstance().isFree) {
                                            purchaseByCoupon(Main.getInstance().coupon, getInstance().getTargetProduct().getProduct());
                                        } else {
                                            makeTXWebViewDialog(getActivity(),
                                                    getBankRedirectURL(getInstance().getTargetProduct().getProduct(),
                                                            Main.getInstance().coupon,
                                                            Payment.getInstance().gateWayUrl,
                                                            Payment.getInstance().appID,
                                                            Payment.getInstance().appSource,
                                                            Payment.getInstance().appVersion,
                                                            Payment.getInstance().deviceID,
                                                            Info.getInstance().userNumber)
                                            ).show();
                                        }
                                        break;
                                    case Main.ProductAdapter.HAS_NOT_COUPON:
                                        makeTXWebViewDialog(getActivity(),
                                                getBankRedirectURL(getInstance().getTargetProduct().getProduct(),
                                                        "",
                                                        Payment.getInstance().gateWayUrl,
                                                        Payment.getInstance().appID,
                                                        Payment.getInstance().appSource,
                                                        Payment.getInstance().appVersion,
                                                        Payment.getInstance().deviceID,
                                                        Info.getInstance().userNumber)
                                        ).show();
                                        break;
                                    case Main.ProductAdapter.RECOVERY:
                                        getUserLicense();
                                        break;
                                }
                                break;
                            case "bazaar":
                                Limits.getInstance().mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
                                    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                                        Log.d(TAG, "onIabPurchaseFinished");
                                        Log.d(TAG, String.valueOf(result.isFailure()));
                                        if (purchase != null)
                                            Log.d(TAG, purchase.getSku());
                                        if (result.isFailure()) {
                                            Log.d(TAG, "Error purchasing: " + result);
                                            return;
                                        } else if (purchase.getSku().equals(Payment.getInstance().SKU_PREMIUM)) {
                                            // give user access to premium content and update the UI
                                            JSONObject jsonObject;
                                            PersianCalendar persianCalendar = null;
                                            String orderId = "";
                                            try {
                                                jsonObject = new JSONObject(Payment.makeJsonString(purchase.getOriginalJson()));
                                                persianCalendar = new PersianCalendar(jsonObject.getLong("purchaseTime"));

                                                orderId = jsonObject.getString("orderId");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                SkuDetails skuDetails = new SkuDetails(purchase.getOriginalJson());
                                                String license = "{\\\"status\\\":\\\"0\\\""
                                                        + ",\\\"license_product\\\":\\\"full_lifetime\\\""
                                                        + ",\\\"license_valid_days\\\":\\\"863913600000\\\""
                                                        + ",\\\"app_fa\\\":\\\"" + Payment.getInstance().app_fa + "\\\""
                                                        + ",\\\"appid\\\":\\\"" + Payment.getInstance().appID + "\\\""
                                                        + ",\\\"source\\\":\\\"" + Payment.getInstance().appSource + "\\\""
                                                        + ",\\\"version\\\":\\\"" + Payment.getInstance().appVersion + "\\\""
                                                        + ",\\\"pcode\\\":\\\"" + orderId + "\\\""
                                                        + ",\\\"digitalreceipt\\\":\\\"\\\""
                                                        + ",\\\"amount\\\":\\\"" + skuDetails.getPrice() + "\\\""
                                                        + ",\\\"_status\\\":\\\"0\\\""
                                                        + ",\\\"verify\\\":\\\"0\\\""
                                                        + ",\\\"coupon\\\":\\\"\\\""
                                                        + ",\\\"description\\\":\\\"" + skuDetails.getDescription() + "\\\""
                                                        + ",\\\"date_fa\\\":\\\"" + persianCalendar.getPersianLongDateAndTime() + "\\\""
                                                        + ",\\\"username\\\":\\\" \\\"}"; //+ Settings.getSetting(Settings.SettingEnums.TX_USER_NUMBER.value()) +
                                                //
                                                Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                                Result.getInstance().setLicense(TX_License.returnedLicense(license));

                                                //makeTXResult(context).show();
                                                //TX_License.setLicense(license);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            //
                                            //  makeTXLicense(context);
                                        }
                                    }
                                };
                                Limits.getInstance().mHelper.launchPurchaseFlow(getActivity(), Payment.getInstance().SKU_PREMIUM, 0, Limits.getInstance().mPurchaseFinishedListener, "payload-string");
                                break;

                            case "myket":
                                Limits.getInstance().mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
                                    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                                        Log.d(TAG, "onIabPurchaseFinished");
                                        Log.d(TAG, String.valueOf(result.isFailure()));
                                        if (purchase != null)
                                            Log.d(TAG, purchase.getSku());
                                        if (result.isFailure()) {
                                            Log.d(TAG, "Error purchasing: " + result);
                                            return;
                                        } else if (purchase.getSku().equals(Payment.getInstance().SKU_PREMIUM)) {
                                            // give user access to premium content and update the UI
                                            JSONObject jsonObject;
                                            PersianCalendar persianCalendar = null;
                                            String orderId = "";
                                            try {
                                                jsonObject = new JSONObject(Payment.makeJsonString(purchase.getOriginalJson()));
                                                persianCalendar = new PersianCalendar(jsonObject.getLong("purchaseTime"));

                                                orderId = jsonObject.getString("orderId");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                SkuDetails skuDetails = new SkuDetails(purchase.getOriginalJson());
                                                String license = "{\\\"status\\\":\\\"0\\\""
                                                        + ",\\\"license_product\\\":\\\"full_lifetime\\\""
                                                        + ",\\\"license_valid_days\\\":\\\"863913600000\\\""
                                                        + ",\\\"app_fa\\\":\\\"" + Payment.getInstance().app_fa + "\\\""
                                                        + ",\\\"appid\\\":\\\"" + Payment.getInstance().appID + "\\\""
                                                        + ",\\\"source\\\":\\\"" + Payment.getInstance().appSource + "\\\""
                                                        + ",\\\"version\\\":\\\"" + Payment.getInstance().appVersion + "\\\""
                                                        + ",\\\"pcode\\\":\\\"" + orderId + "\\\""
                                                        + ",\\\"digitalreceipt\\\":\\\"\\\""
                                                        + ",\\\"amount\\\":\\\"" + skuDetails.getPrice() + "\\\""
                                                        + ",\\\"_status\\\":\\\"0\\\""
                                                        + ",\\\"verify\\\":\\\"0\\\""
                                                        + ",\\\"coupon\\\":\\\"\\\""
                                                        + ",\\\"description\\\":\\\"" + skuDetails.getDescription() + "\\\""
                                                        + ",\\\"date_fa\\\":\\\"" + persianCalendar.getPersianLongDateAndTime() + "\\\""
                                                        + ",\\\"username\\\":\\\" \\\"}"; //+ Settings.getSetting(Settings.SettingEnums.TX_USER_NUMBER.value()) +
                                                //
                                                Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                                Result.getInstance().setLicense(TX_License.returnedLicense(license));

                                                //makeTXResult(context).show();
                                                //TX_License.setLicense(license);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            //
                                            //  makeTXLicense(context);
                                        }
                                    }
                                };
                                Limits.getInstance().mHelper.launchPurchaseFlow(getActivity(), Payment.getInstance().SKU_PREMIUM, 0, Limits.getInstance().mPurchaseFinishedListener, "payload-string");
                                break;
                        }
                    }

                    @Override
                    public void failed(int result) {
                        if (result == Constants.INPUT_IS_INVALID)           //bara tamizi code ...
                            errorMessage.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        return infoLayout;
    }

    private void getUserLicense() {
        final Dialog spinnerDialogInfo = Main.getInstance().spinnerDialog(getActivity());
        spinnerDialogInfo.show();
        try {
            GetUserLicenses(Payment.getInstance().appID,
                    Payment.getInstance().appSource,
                    Info.getInstance().userNumber,
                    Payment.getInstance().serverUrl,
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            spinnerDialogInfo.dismiss();
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (response.isSuccessful()) {
                                spinnerDialogInfo.dismiss();
                                String responseStr = response.body().string();
                                Log.i(TAG, responseStr);
                                final JSONObject jsonObject = getLicenseContent(Payment.makeJsonString(responseStr)); // license e app e morede nazar ro migire

                                final String errorMsgBody = "هیچ لایسنسی با شماره " + getInstance().userNumber + " یافت نشد";
                                if (jsonObject.toString().equals("{}")) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
//                                    DialogUtils.makeMessageDialog(context, DialogUtils.MessageDialogTypes.ERROR, "خطا", errorMsgBody, "").show();
                                            Payment.getInstance().customToast(getActivity(), "خطا!", errorMsgBody, false).show();
                                        }
                                    });
                                } else {
                                    try {
                                        if (jsonObject.getString("appid").equals(Payment.getInstance().appID)) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
//                                            makeTXVerifyM1(context).show();
                                                    try {
                                                        if (jsonObject.getInt("active_count") < jsonObject.getInt("active_limit")
                                                                && !String.valueOf(jsonObject.getInt("id")).equals("")) {
                                                            Payment.getInstance().initTargetFragment(Constants.PIN_VERIFY_M1_ID);
                                                            PinVerifyM1.licenseCount = jsonObject.getInt("active_count");
                                                            PinVerifyM1.licenseLimit = jsonObject.getInt("active_limit");
                                                            PinVerifyM1.licenseID = jsonObject.getInt("id");
                                                        } else {
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Payment.getInstance().restoreError(getActivity()).show();
                                                                }
                                                            });
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });

                                        } else {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
//                                            DialogUtils.makeMessageDialog(context, DialogUtils.MessageDialogTypes.ERROR, "خطا", errorMsgBody, "").show();
                                                    Payment.getInstance().customToast(getActivity(), "خطا!", errorMsgBody, false).show();
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void purchaseByCoupon(String coupon, String product) {
        final Dialog spinnerDialogInfo = Main.getInstance().spinnerDialog(getActivity());
        spinnerDialogInfo.show();
        try {
            PurchaseByCoupon(coupon,
                    product,
                    Payment.getInstance().appID,
                    Payment.getInstance().appSource,
                    Payment.getInstance().appVersion,
                    Payment.getInstance().deviceID,
                    Info.getInstance().userNumber,
                    Payment.getInstance().serverUrl,
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            Log.i(TAG, "purchase failed");
                            spinnerDialogInfo.dismiss();
                        }

                        @Override
                        public void onResponse(final Response response) throws IOException {
                            if (response.isSuccessful()) {
                                spinnerDialogInfo.dismiss();
                                Log.i(TAG, "purchase Success");
                                try {
                                    String responseStr = response.body().string();
                                    Log.i(TAG, responseStr);
                                    Log.i("PurchaseByCoupon free", responseStr);
                                    JSONObject responseJson = new JSONObject(Payment.makeJsonString(responseStr));
                                    if (responseJson.getString("status").equals(Constants.SUCCESS)) { // بدون خطا از سرور
                                        Log.i(TAG, "بدون خطا از سرور");
                                        if (responseJson.getString("_status").equals(Constants.SUCCESS)) { //تراکنش انجام شده
                                            if (responseJson.getString("verify").equals(Constants.SUCCESS)) {
                                                Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                                Result.getInstance().setLicense(TX_License.returnedLicense(responseStr));
                                            } else { // ناموفق
                                                Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                PaymentFailed.getInstance().setErrorMessage("عملیات فعالسازی با پین کد با خطا مواجه شد");

                                                Log.i(TAG, " ناموفق");
                                            }
                                        }
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    void validation(ResponseCallBack callBack) {
        String userInputStr = userInput.getText().toString();
        if (userInputStr.matches("(^[0][9].{9}$)")) {
            errorMessage.setVisibility(View.INVISIBLE);
            callBack.success();
        } else {
            callBack.failed(Constants.INPUT_IS_INVALID);
        }
    }

    public Dialog makeTXWebViewDialog(final Context context, String URL) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_payment_web_view);
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();

        lp.width = ConstraintLayout.LayoutParams.MATCH_PARENT;
        lp.height = ConstraintLayout.LayoutParams.MATCH_PARENT;

        dialogWindow.setAttributes(lp);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //
        final WebView myWebView = dialog.findViewById(R.id.web_view);
        myWebView.getSettings().setSupportZoom(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setDomStorageEnabled(true);
        myWebView.loadUrl(URL);
        myWebView.setWebChromeClient(new WebChromeClient());
        //       myWebView.evaluateJavascript("alert('parsa')", null);
        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public void onPageStarted(final WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                view.evaluateJavascript("tx_reference_input.value", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(final String s) {
                        Log.i("s", s);
                        if (!s.equals("null")) {
                            String pcode = s.replace("\"", "");
                            Log.i("pcode", pcode);
                            if (s.length() > 0) {
                                Log.i("onReceiveValue", s);
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        myWebView.clearCache(true);
                                        myWebView.onPause();
                                        myWebView.removeAllViews();
                                        myWebView.destroyDrawingCache();
                                        myWebView.destroy();
                                    }
                                });
                                try {
                                    PurchaseVerify(pcode,
                                            Payment.getInstance().serverUrl,
                                            new Callback() {
                                                String responseStr;

                                                @Override
                                                public void onFailure(Request request, IOException e) {

                                                }

                                                @Override
                                                public void onResponse(Response response) throws IOException {
                                                    if (response.isSuccessful()) {
                                                        responseStr = response.body().string();
                                                        Log.i("Response: ", responseStr);
                                                        if (responseStr.charAt(0) == '\"' && responseStr.charAt(responseStr.length() - 1) == '\"') {
                                                            responseStr = responseStr.substring(1, responseStr.length() - 1);// از بین بردن " های اول و آخر
                                                        }
                                                        responseStr = responseStr.replaceAll("\\\\", ""); // // از بین بردن / های اضافی
                                                /*
                                                ------------------------------------------
                                                 */
                                                        try {
                                                            JSONObject jsonObject = new JSONObject(responseStr);
                                                            if (jsonObject.getString("status").equals(Constants.SUCCESS)) { // بدون خطا از سرور
                                                                Log.i(TAG, "بدون خطا از سرور");
                                                                if (jsonObject.getString("_status").equals("OK")) { //تراکنش انجام شده
                                                                    Log.i(TAG, "تراکنش انجام شده");
                                                                    if (jsonObject.getString("verify").equals(Constants.SUCCESS)) {
                                                                        Log.i(TAG, "پرداخت موفق");
//                                                                C_Setting.setStringValue(C_Setting.LICENSE_DATA, responseStr);

                                                                        ((Activity) context).runOnUiThread(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                try {
                                                                                    Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                                                                    Result.getInstance().setLicense(TX_License.returnedLicense(responseStr));
                                                                                } catch (JSONException e) {
                                                                                    e.printStackTrace();
                                                                                }
                                                                                dialog.dismiss();
                                                                            }
                                                                        });
                                                                    } else { //پرداخت ناموفق
                                                                        Log.i(TAG, "پرداخت ناموفق");
                                                                        ((Activity) context).runOnUiThread(new Runnable() {
                                                                            @Override
                                                                            public void run() {
                                                                                Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                                                PaymentFailed.getInstance().setErrorMessage(Constants.INFO_PAYMENT_VERIFY_ERROR);
                                                                                dialog.dismiss();
                                                                            }
                                                                        });
                                                                    }
                                                                } else if (jsonObject.getString("_status").equals("Canceled By User")) {
                                                                    ((Activity) context).runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                                            PaymentFailed.getInstance().setErrorMessage(Constants.PAYMENT_CANCELED_BY_USER);
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                                } else if (jsonObject.getString("_status").equals("IssuerDown_Slm")) {
                                                                    ((Activity) context).runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                                            PaymentFailed.getInstance().setErrorMessage(Constants.BANK_NOT_RESPONDING);
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                                } else {
                                                                    ((Activity) context).runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            Payment.getInstance().customToast(getActivity(), "پرداخت ناموفق", Constants.INFO_PAYMENT_STATUS_UNKNOWN, false).show();
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                                }
                                                            } else if (jsonObject.getString("status").equals(Constants.TRANSACTION_INVALID)) { // تراکنش پیدا نشد و سرور خطا میدهد
                                                                ((Activity) context).runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                                        PaymentFailed.getInstance().setErrorMessage(Constants.PAYMENT_CONFIRMATION_ERROR);
                                                                        dialog.dismiss();
                                                                    }
                                                                });
                                                                Log.i(TAG, "خطا در پرداخت لطفا دوباره تلاش کنید ");
                                                            } else {
                                                                ((Activity) context).runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Payment.getInstance().customToast(getActivity(), "خطا!", "خطا نامشخص", false).show();
                                                                    }
                                                                });
                                                                Log.i(TAG, "خطای نا مشخص");
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        ((Activity) context).runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                                PaymentFailed.getInstance().setErrorMessage(Constants.TRANSACTION_CONFIRMATION_ERROR);
                                                                dialog.dismiss();
                                                            }
                                                        });
                                                        Log.i(TAG, "خطا در تایید تراکنش");
                                                    }
                                                }
                                            });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else if (s.length() == 0) {
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                        PaymentFailed.getInstance().setErrorMessage(Constants.TRANSACTION_CONFIRMATION_ERROR);
                                        dialog.dismiss();
                                    }
                                });
                            }
                        }
                    }
                });
            }
        };
        myWebView.setWebViewClient(webViewClient);
        //
        return dialog;
    }

    public static JSONObject getLicenseContent(String responseStr) {
        JSONObject jsonObject;
        JSONObject jsonResult = new JSONObject();
        Log.i("LicenseContent response", responseStr);
        try {
            jsonObject = new JSONObject(responseStr);
            String licenses = jsonObject.getString("licenses");
            jsonObject = new JSONObject(licenses);
            Iterator result = jsonObject.keys();
            while (result.hasNext()) {
                Object key = result.next();
                if (jsonObject.getJSONObject(key.toString()).getString("appid").equals(Payment.getInstance().appID)) {
                    jsonResult = jsonObject.getJSONObject(key.toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonResult;
    }

    public int getInfoType() {
        return infoType;
    }

    public void setInfoType(int infoType) {
        this.infoType = infoType;
    }

    public Product getTargetProduct() {
        return targetProduct;
    }

    public void setTargetProduct(Product targetProduct) {
        this.targetProduct = targetProduct;
    }
}
