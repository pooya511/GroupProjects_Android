package com.taxiapps.txpayment.fragment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;
import com.taxiapps.txpayment.model.Constants;
import com.taxiapps.txpayment.payment.TX_License;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static TxAnalytics.TxAnalytics.CheckPINVerifyMethod1;
import static TxAnalytics.TxAnalytics.GeneratePINVerifyMethod1;
import static TxAnalytics.TxAnalytics.ReactivateLicense;

/**
 * Created by Parsa on 2018-02-21.
 */

public class PinVerifyM1 extends BaseFragment {

    private CountDownTimer countDownTimer, progressBarCountDown, smsNotReceivedCountDown;
    private ProgressBar pinVerifyM1progressbar;
    private TextView pinVerifyM1Back, resendSms, smsDidntRecived, countDown;
    private EditText activationCodeEdittext;
    private Button activationButton;

    public static int licenseCount;
    public static int licenseLimit;
    public static int licenseID;

    public static PinVerifyM1 newInstance() {

        Bundle args = new Bundle();

        PinVerifyM1 fragment = new PinVerifyM1();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View pinVerifyM1Layout = inflater.inflate(R.layout.frg_tx_payment_pin_verify_m1, null);

        Payment.getInstance().headerDescription.setText(R.string.pin_verify_header_description);

        activationCodeEdittext = pinVerifyM1Layout.findViewById(R.id.frg_tx_payment_pin_verify_m1_activation_code_edittext);
        countDown = pinVerifyM1Layout.findViewById(R.id.frg_tx_payment_pin_verify_m1_countdown);
        smsDidntRecived = pinVerifyM1Layout.findViewById(R.id.frg_tx_payment_pin_verify_m1_sms_not_recived);
        resendSms = pinVerifyM1Layout.findViewById(R.id.frg_tx_payment_pin_verify_m1_resend_sms);
        pinVerifyM1Back = pinVerifyM1Layout.findViewById(R.id.tx_payment_pin_verify_m1_back);
        pinVerifyM1progressbar = pinVerifyM1Layout.findViewById(R.id.frg_tx_payment_pin_verify_m1_progressbar);
        activationButton = pinVerifyM1Layout.findViewById(R.id.tx_payment_pin_verify_m1_activation_button);

        progressBarCountDown = new CountDownTimer(2000, 1000000) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                pinVerifyM1progressbar.setVisibility(View.GONE);
                countDownTimer = new CountDownTimer(60000, 1000) {

                    int threeSecondTimerStart = 0;

                    public void onTick(long millisUntilFinished) {
                        threeSecondTimerStart++;
                        if (threeSecondTimerStart == 3)
                            smsDidntRecived.setVisibility(View.VISIBLE);
                        countDown.setText("ارسال مجدد " + millisUntilFinished / 1000 + " ثانیه");
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        countDown.setVisibility(View.GONE);
                        resendSms.setVisibility(View.VISIBLE);
                    }

                }.start();
            }
        }.start();

        generatePINVerifyMethod1();

        View.OnClickListener listeners = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.equals(smsDidntRecived)) {
                    Payment.getInstance().initTargetFragment(Constants.PIN_VERIFY_M2_ID);
                }
                if (v.equals(pinVerifyM1Back)) {
                    getFragmentManager().popBackStack();
                }
                if (v.equals(activationButton)) {
                    activationLicense();
                }
                if (v.equals(resendSms)) {
                    pinVerifyM1progressbar.setVisibility(View.VISIBLE);
                    smsDidntRecived.setVisibility(View.GONE);
                    countDown.setText("");
                    countDown.setVisibility(View.VISIBLE);
                    resendSms.setVisibility(View.GONE);
                    progressBarCountDown.start();
                    generatePINVerifyMethod1();
                }

            }
        };
        smsDidntRecived.setOnClickListener(listeners);
        pinVerifyM1Back.setOnClickListener(listeners);
        activationButton.setOnClickListener(listeners);
        resendSms.setOnClickListener(listeners);

        return pinVerifyM1Layout;
    }

    private void generatePINVerifyMethod1() {
        try {
            GeneratePINVerifyMethod1(Payment.getInstance().appID,
                    Payment.getInstance().appSource,
                    Payment.getInstance().appVersion,
                    Payment.getInstance().deviceID,
                    Info.getInstance().userNumber,
                    Payment.getInstance().serverUrl,
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {

                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (response.isSuccessful()) {
                                String responseStr = response.body().string();
                                Log.i(Payment.TAG, responseStr);
                                try {
                                    JSONObject responseJson = new JSONObject(Payment.makeJsonString(responseStr));
                                    if (responseJson.getString("status").equals(Constants.SUCCESS)) {
                                        activationLicense();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void activationLicense() {
        if (!activationCodeEdittext.getText().toString().equals("")) {
            try {
                CheckPINVerifyMethod1(activationCodeEdittext.getText().toString(),
                        Payment.getInstance().appID,
                        Payment.getInstance().appSource,
                        Payment.getInstance().appVersion,
                        Payment.getInstance().deviceID,
                        Info.getInstance().userNumber,
                        Payment.getInstance().serverUrl,
                        new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {

                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful()) {
                                    String responseStr = response.body().string();
                                    Log.i("CheckPINVerify1Response", responseStr);
                                    try {
                                        final JSONObject responseJson = new JSONObject(Payment.makeJsonString(responseStr));
                                        if (responseJson.getString("status").equals(Constants.SUCCESS)) {
                                            if (responseJson.getString("approved").equals("1")) {
                                                ReactivateLicense(String.valueOf(licenseID),
                                                        Payment.getInstance().appID,
                                                        Payment.getInstance().appSource,
                                                        Payment.getInstance().appVersion,
                                                        Payment.getInstance().deviceID,
                                                        Info.getInstance().userNumber,
                                                        Payment.getInstance().serverUrl,
                                                        new Callback() {
                                                            @Override
                                                            public void onFailure(Request request, IOException e) {

                                                            }

                                                            @Override
                                                            public void onResponse(Response response) throws IOException {
                                                                if (response.isSuccessful()) {
                                                                    try {
                                                                        String responseStr = response.body().string();
                                                                        Log.i("ReactivateLicensRespons", responseStr);
                                                                        JSONObject licenseJson = new JSONObject(responseStr);
                                                                        if (licenseJson.length() != 0) {
                                                                            Payment.getInstance().initTargetFragment(Constants.RESULT_ID);
                                                                            Result.getInstance().setLicense(TX_License.returnedLicense(responseStr));
                                                                        } else {
                                                                            // خطای بازیابی لایسنس
                                                                            Payment.getInstance().initTargetFragment(Constants.PAYMENT_FAILED_ID);
                                                                            PaymentFailed.getInstance().setErrorMessage("خطای فعالسازی مجدد لایسنس");
                                                                        }
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                        });
                                            } else {
                                                // کد فعالسازی صحیح نیست .
                                                // پاپ آپ بسته شود .
//                                        DialogUtils.makeMessageDialog(getActivity(), DialogUtils.MessageDialogTypes.ERROR, "خطا", "کد فعالسازی صحیح نیست .", "").show();
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Payment.getInstance().customToast(getActivity(), "خطا", "کد فعالسازی صحیح نیست.", false).show();
                                                    }
                                                });
                                            }
                                        } else {
//                                    DialogUtils.makeMessageDialog(context, DialogUtils.MessageDialogTypes.ERROR, "خطا", "خطا در تایید پیامک !", "").show();
                                            // خطا در تایید پیامک
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Payment.getInstance().customToast(getActivity(), "خطا", "خطا در تایید پیامک !", false).show();
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    void validation(ResponseCallBack callBack) {

    }
}
