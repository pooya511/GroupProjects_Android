package com.taxiapps.txpayment.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.dialogFragment.Payment;

/**
 * Created by Parsa on 2018-02-21.
 */

public class PaymentFailed extends BaseFragment {

    private Button exit;
    private TextView errorMessageTextView;

    private String errorMessage;

    private static PaymentFailed instance;

    public static PaymentFailed getInstance() {
        return instance;
    }

    public static PaymentFailed newInstance() {

        Bundle args = new Bundle();

        PaymentFailed fragment = new PaymentFailed();
        fragment.setArguments(args);

        instance = fragment;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View layout = inflater.inflate(R.layout.frg_tx_payment_unsuccess, null);

        exit = layout.findViewById(R.id.tx_payment_un_success_exit);
        errorMessageTextView = layout.findViewById(R.id.tx_payment_un_success_error_message);

        if (!errorMessage.equals(""))
            errorMessageTextView.setText(getInstance().getErrorMessage());

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Payment.getInstance().dismiss();
            }
        });

        return layout;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    void validation(ResponseCallBack callBack) {

    }


}

