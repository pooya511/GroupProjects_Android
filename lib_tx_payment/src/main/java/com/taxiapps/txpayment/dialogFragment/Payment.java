package com.taxiapps.txpayment.dialogFragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taxiapps.txpayment.R;
import com.taxiapps.txpayment.fragment.BazaarFailed;
import com.taxiapps.txpayment.fragment.Info;
import com.taxiapps.txpayment.fragment.License;
import com.taxiapps.txpayment.fragment.Limits;
import com.taxiapps.txpayment.fragment.Main;
import com.taxiapps.txpayment.fragment.PaymentFailed;
import com.taxiapps.txpayment.fragment.PinVerifyM1;
import com.taxiapps.txpayment.fragment.PinVerifyM2;
import com.taxiapps.txpayment.fragment.Result;
import com.taxiapps.txpayment.model.Constants;
import com.taxiapps.txpayment.payment.TX_License;

import modules.PublicModules;

/**
 * Created by Parsa on 2018-02-21.
 */

public class Payment extends DialogFragment {

    public interface UserNameCallBack extends Parcelable {
        void getUserName(String username);
    }

    public String SKU_PREMIUM = "prdPremium";
    public String SKU_PRICE = "skuPrice";
    public boolean mIsPremium;
    public final int RC_REQUEST = 0;
    public String base64EncodedPublicKey;
    public static final String TAG = "tx_payment";

    // components
    private FrameLayout fragmentContainer;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    public TextView headerDescription;

    // variables
    public String appID;
    public String appSource;
    public String appVersion;
    public String app_fa; // for cafe bazaar
    public String deviceID;
    public String titleText;
    public String userName;
    public String limitText;
    public String serverUrl;
    public String gateWayUrl;

    // callbacks
    public TX_License.SetLicenseCallback setLicenseCallback;
    public UserNameCallBack userNameCallBack;

    private static Payment instance;

    public static Payment getInstance() {
        return instance;
    }

    public static Payment newInstance(int iconResourceId,
                                      String appID,
                                      String appSource,
                                      String appVersion,
                                      String app_fa,
                                      String base64EncodedPublicKey,
                                      String titleText,
                                      String limitText,
                                      String userName,
                                      String deviceID,
                                      String serverUrl,
                                      String gateWayUrl,
                                      TX_License.SetLicenseCallback setLicenseCallback,
                                      UserNameCallBack userNameCallBack
    ) {

        Bundle args = new Bundle();
        args.putInt("iconResourceId", iconResourceId);
        args.putString("base64EncodedPublicKey", base64EncodedPublicKey);
        args.putString("appID", appID);
        args.putString("appSource", appSource);
        args.putString("appVersion", appVersion);
        args.putString("app_fa", app_fa);
        args.putString("titleText", titleText);
        args.putString("limitText", limitText);
        args.putString("userName", userName);
        args.putString("deviceID", deviceID);
        args.putString("serverUrl", serverUrl);
        args.putString("gateWayUrl", gateWayUrl);
        args.putParcelable("SetLicenseCallback", setLicenseCallback);
        args.putParcelable("userNameCallBack", userNameCallBack);

        Payment fragment = new Payment();
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("ResourceType")
    public void initTargetFragment(int fragmentId) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.start_slide_left, R.anim.end_slide_left);

        switch (fragmentId) {
            case Constants.LIMITS_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), Limits.newInstance(titleText, limitText), "LIMITS_ID");
                break;
            case Constants.MAIN_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), Main.newInstance(), "MAIN_ID");
                break;
            case Constants.INFO_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), Info.newInstance(), "INFO_ID");
                break;
            case Constants.PIN_VERIFY_M1_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), PinVerifyM1.newInstance(), "PIN_VERIFY_M1_ID");
                break;
            case Constants.PIN_VERIFY_M2_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), PinVerifyM2.newInstance(), "PIN_VERIFY_M2_ID");
                break;
            case Constants.RESULT_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), Result.newInstance(), "RESULT_ID");
                break;
            case Constants.LICENSE_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), License.newInstance(), "LICENSE_ID");
                break;
            case Constants.PAYMENT_FAILED_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), PaymentFailed.newInstance(), "PAYMENT_FAILED_ID");
                break;
            case Constants.BAZAAR_FAILED_ID:
                fragmentTransaction.replace(fragmentContainer.getId(), BazaarFailed.newInstance(), "BAZAAR_FAILED_ID");
                break;
        }

        fragmentTransaction.addToBackStack(null);
        if (getActivity() != null)
            if (!getActivity().isFinishing()){
                fragmentTransaction.commitAllowingStateLoss();
            }

    }

    @Override
    public void onStart() {
        super.onStart();
        Window dialogWindow = getDialog().getWindow();
        dialogWindow.setBackgroundDrawableResource(android.R.color.transparent);
        getDialog().setCanceledOnTouchOutside(false);

        int width;
        int height;
        switch (screenSize(getActivity())) {
            case 1:
                width = 80;
                height = 75;
                break;
            case 2:
                width = 65;
                height = 65;
                break;
            case 3:
                width = 65;
                height = 65;
                break;
            default:
                width = 0;
                height = 0;
                break;
        }

        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        //
        int ScreenSizeResults[] = getWidthAndHeight(getActivity(), width, height);
        lp.width = ScreenSizeResults[0];
        lp.height = ScreenSizeResults[1];

        dialogWindow.setAttributes(lp);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        base64EncodedPublicKey = getArguments().getString("base64EncodedPublicKey");
        appID = getArguments().getString("appID");
        appSource = getArguments().getString("appSource");
        appVersion = getArguments().getString("appVersion");
        app_fa = getArguments().getString("app_fa");
        titleText = getArguments().getString("titleText");
        limitText = getArguments().getString("limitText");
        userName = getArguments().getString("userName");
        deviceID = getArguments().getString("deviceID");
        serverUrl = getArguments().getString("serverUrl");
        gateWayUrl = getArguments().getString("gateWayUrl");
        setLicenseCallback = getArguments().getParcelable("SetLicenseCallback");
        userNameCallBack = getArguments().getParcelable("userNameCallBack");

        View parentView = inflater.inflate(R.layout.tx_payment_parent, null);

        //find
        fragmentContainer = parentView.findViewById(R.id.tx_payment_parent_fragment_container);
        headerDescription = parentView.findViewById(R.id.tx_payment_description);
        ImageView logo = parentView.findViewById(R.id.tx_payment_parent_logo);

        //init
        logo.setBackgroundResource(getArguments().getInt("iconResourceId"));
        headerDescription.setText(R.string.limit_header_description);
        fragmentManager = getChildFragmentManager();

        initTargetFragment(Constants.LIMITS_ID);

        return parentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    public static String makeJsonString(String responseStr) {
        if (responseStr.charAt(0) == '\"' && responseStr.charAt(responseStr.length() - 1) == '\"') {
            responseStr = responseStr.substring(1, responseStr.length() - 1);// از بین بردن " های اول و آخر
        }
        responseStr = responseStr.replaceAll("\\\\", "");
        return responseStr;
    }

    private int screenSize(Context context) {

        int screenSize = context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return 1;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return 2;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return 3;
            default:
                return 4; // alaki
        }
    }

    private int[] getWidthAndHeight(Context context, int widthPercent, int heightPercent) {

        int widthAndHeight[] = new int[2];
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        widthAndHeight[0] = widthPercent * displayMetrics.widthPixels / 100;
        widthAndHeight[1] = heightPercent * displayMetrics.heightPixels / 100;
        return widthAndHeight;
    }

    public Toast customToast(Context context, String title, String description, boolean isSuccess) {

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom,
                (ViewGroup) ((Activity) context).findViewById(R.id.toast_layout_root));

        ImageView image = layout.findViewById(R.id.toast_img);
        if (isSuccess)
            image.setImageResource(R.drawable.ic_success);
        else
            image.setImageResource(R.drawable.ic_unsuccess);
        TextView titleText = layout.findViewById(R.id.toast_title);
        titleText.setText(title);
        TextView descriptionText = layout.findViewById(R.id.toast_description);
        descriptionText.setText(description);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 25);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        return toast;
    }

    public Dialog restoreError (Context context){
        final Dialog restoreError = new Dialog(context);
        restoreError.requestWindowFeature(Window.FEATURE_NO_TITLE);
        restoreError.setContentView(R.layout.pop_payment_restore_error);
        Window dialogWindow = restoreError.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.width = ConstraintLayout.LayoutParams.WRAP_CONTENT;

        TextView ok = dialogWindow.getDecorView().findViewById(R.id.pop_payment_restore_error_ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restoreError.dismiss();
                Payment.getInstance().dismiss();
            }
        });

        restoreError.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        restoreError.getWindow().setWindowAnimations(R.style.fadeInAnimation);

        return restoreError;

    }

}
